({
	getACRS : function(component,event) {

		var recordId = component.get("v.recordId");
		//var caseId = component.get("v.caseId");
		var caseAcctId = component.get("v.caseAcctId");
		
		console.log("in helper getACRS: caseId:"+recordId);
		/*if(recordId) {
			caseId = recordId;
		}*/
		var callCtrl = component.get("c.getACRList");
		callCtrl.setParams({"caseId":recordId,
							"caseAcctId":caseAcctId});
		callCtrl.setCallback(this,function(resp) {

			var callbackStatus = resp.getState();
			if(callbackStatus == "SUCCESS") {

				var acrRecorList = resp.getReturnValue();
				if(acrRecorList != null) {
					component.set("v.acrList",acrRecorList);
				} else {
					component.set("v.messageStr","No Related Records.");
				}
			} else if(callbackStatus == "ERROR") {
				var errors = resp.getError();
				if(errors) {
					if(errors[0] && errors[0].message) {
						component.set("v.messageStr","Error in retriving related Contacts");						
					}
				}
			}
		});
		$A.enqueueAction(callCtrl);
	}
})