({
	init : function(component, event, helper) {


		// status var decleration
		var getAcctStatus;
		console.log("getAccount:");
		var acctId = component.get("v.recordId");
		var opptyRetriveStatus;
		
		var getOppty = component.get("c.isValidAcct");
		console.log("oppty acctId:"+acctId);
		getOppty.setParams({"acctId":acctId});
		getOppty.setCallback(this,function(resp) {

			opptyRetriveStatus = resp.getState();
			console.log("Oppty status : "+opptyRetriveStatus);
			if(opptyRetriveStatus == "SUCCESS") {
				
				console.log("validate resp :"+resp.getReturnValue());
				var returnObj = resp.getReturnValue();
				
				console.log("returnObj opportunity:"+returnObj.message
						+"acct flag:"+returnObj.confirmFlag
						+"acct:"+returnObj.accountId);
				var respMessage = returnObj.message;
				var confirmflag = returnObj.confirmFlag;
				// set account
				component.set("v.wrapperRecord",returnObj);
				component.set("v.ccCheckFlag",confirmflag);
				component.set("v.responseStr",respMessage);
				
				if(confirmflag) {
					console.log("confirmflag "+confirmflag);	

					
					var confirmMsg = component.find("confirmId");
					
					component.set("v.responseStrType","Confirmation");
					$A.util.removeClass(confirmMsg,"slds-hide");
					$A.util.addClass(confirmMsg,"slds-show");

					var confirmBtnId = component.find("confirmBtnId");
					$A.util.removeClass(confirmBtnId,"slds-hide");
					$A.util.addClass(confirmBtnId,"slds-show");

					var cancelBtnId = component.find("cancelBtnId");
					$A.util.removeClass(cancelBtnId,"slds-hide")
					$A.util.addClass(cancelBtnId,"slds-show");

					var okBtnId = component.find("okBtnId");
					$A.util.removeClass(okBtnId,"slds-show");
					$A.util.addClass(okBtnId,"slds-hide");
				} else {

					
					var confirmId = component.find("confirmId");
					$A.util.removeClass(confirmId,"slds-hide");
					$A.util.addClass(confirmId,"slds-show");
					
					var confirmBtnId = component.find("confirmBtnId");
					$A.util.removeClass(confirmBtnId,"slds-show");
					$A.util.addClass(confirmBtnId,"slds-hide");

					var cancelBtnId = component.find("cancelBtnId");
					$A.util.removeClass(cancelBtnId,"slds-show")
					$A.util.addClass(cancelBtnId,"slds-hide");

					var okBtnId = component.find("okBtnId");
					$A.util.removeClass(okBtnId,"slds-hide");
					$A.util.addClass(okBtnId,"slds-show");
				}
				console.log("display msg");			
				$A.get('e.force:refreshView').fire();	
			}
			else if(opptyRetriveStatus == "ERROR") {
				console.log("error in account retrival");
				component.set("v.btnName","OK");
	        }
		});
		$A.enqueueAction(getOppty);
	},
	confirmCCCheck : function(component,event) {

		component.set("v.confirmDialog",true);
		var wrapperRecord = component.get("v.wrapperRecord");
		var calloutAction = component.get("c.creditCheck");
		console.log("account:"+wrapperRecord.accountId
			+"vv Id :"+wrapperRecord.ccVerificationId);
		calloutAction.setParams({"acctId":wrapperRecord.accountId,
								"cvId":wrapperRecord.ccVerificationId});
		calloutAction.setCallback(this,function(resp) {

			calloutStatus = resp.getState();
			console.log("confirmCCCheck Oppty status : "+calloutStatus);
			if(calloutStatus == "SUCCESS") {
				
				var calloutResponse = resp.getReturnValue();
				component.set("v.ccCheckFlag",false);
				component.set("v.responseStr",calloutResponse);
				component.set("v.responseStrType","Information");

				var confirmMsg = component.find("confirmId");
				$A.util.removeClass(confirmMsg,"slds-show");
				$A.util.removeClass(confirmMsg,"slds-hide");
				console.log("confirm btn in confirmMsg");
				var confirmBtnId = component.find("confirmBtnId");
				$A.util.removeClass(confirmBtnId,"slds-show");
				$A.util.addClass(confirmBtnId,"slds-hide");

				var cancelBtnId = component.find("cancelBtnId");
				$A.util.removeClass(cancelBtnId,"slds-show")
				$A.util.addClass(cancelBtnId,"slds-hide");

				var okBtnId = component.find("okBtnId");
				$A.util.removeClass(okBtnId,"slds-hide");
				$A.util.addClass(okBtnId,"slds-show");
				
				/*var infoId = component.find("infoId");
				$A.util.removeClass(infoId,"slds-hide");
				$A.util.removeClass(infoId,"slds-show");*/

				$A.get('e.force:refreshView').fire();
				console.log("confirm btn in confirmMsg after refresh");
			}
			else if(opptyRetriveStatus == "ERROR") {
				console.log("error in account retrival");
	        }
		});
		$A.enqueueAction(calloutAction);
		//component.set("v.ccCheckFlag",true);
		//helper.toggleClassInverse(component,'backGroundSectionIddrop','slds-backdrop--');
		//helper.toggleClassInverse(component,'recordSectionId','slds-fade-in-');
	},
	cancleCCCheck : function(component,event) {

		//component.set("v.ccCheckFlag",false);
		$A.get("e.force:closeQuickAction").fire();
	}

})