({
	init : function(component, event, helper) {

		var caseId = component.get("v.recordId");
		helper.getCase(component,event,caseId);
	},
	closeCase : function(component,event,helper) {

		helper.closeCase(component,event);
	},
	cancle : function(component,event,helper) {

		helper.cancle(component,event);
	}
})