({
	getCase : function(component,event,caseId) {

		var getCase = component.get("c.getCase");
		getCase.setParams({"caseId":caseId});
		getCase.setCallback(this,function(res) {

			if(res.getState() == "SUCCESS") {

				var caseRecord = res.getReturnValue();
				if(caseRecord !== null && caseRecord.Id !== null) {

					component.set("v.caseObj",caseRecord);

				} else {

					component.set("v.caseObj",null);
				}
			} else {

				component.set("v.responseMsg","Error in Retriving Case Record.");
				var recSection =  component.find("recordSectionId");
				$A.util.removeClass(recSection,"slds-show");
				var infoId = component.find("infoId");
				$A.util.removeClass(infoId,"slds-hide");
				$A.util.removeClass(infoId,"slds-show");
			}
		});
		$A.enqueueAction(getCase);
	},
	closeCase : function(component,event) {

		var caseObj = component.get("v.caseObj");
		var closeCase = component.get("c.saveCase");
		closeCase.setParams({"caseObj": caseObj});
		closeCase.setCallback(this,function(resp) {

			if(resp.getState() == "ERROR") {

				var respMsg = resp.getReturnValue();
				component.set("v.responseMsg",respMsg);
				var recSection =  component.find("recordSectionId");
				$A.util.removeClass(recSection,"slds-show");
				var infoId = component.find("infoId");
				$A.util.removeClass(infoId,"slds-hide");
				$A.util.removeClass(infoId,"slds-show");
				var infoId = component.find("infoId");
				$A.util.removeClass(infoId,"slds-hide");
				$A.util.removeClass(infoId,"slds-show");
			}
		});
	}
})