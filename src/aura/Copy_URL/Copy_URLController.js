({
	init : function(component, event, helper) {

		var currentRecordId = component.get("v.recordId");
		console.log("currentRecordId::"+currentRecordId);

		
		var action = component.get("c.getClasicHost");
		console.log("action::"+action);
		action.setCallback(this,function(resp) {

			console.log("resp.getState:"+resp.getState());
			if(resp.getState() == "SUCCESS") {

				var sURL = resp.getReturnValue();
				console.log("sURL:"+sURL);
				sURL = sURL+"/"+currentRecordId;
				console.log("sURL:"+sURL);
				component.set("v.retutnUrl",sURL);
				
				
				var copyTextareaBtn = document.querySelector('.js-textareacopybtn');

            	copyTextareaBtn.addEventListener('click', function(event) {
              	var copyTextarea = document.querySelector('.js-copytextarea');
              	copyTextarea.select();
            
              try {
                var successful = document.execCommand('copy');
                var msg = successful ? 'successful' : 'unsuccessful';
                console.log('Copying text command was ' + msg);
              } catch (err) {
                console.log('Oops, unable to copy');
              }
            });

    		
			}
		});
		$A.enqueueAction(action);
	},
	closepopup : function(component, event, helper) {

		$A.get("e.force:closeQuickAction").fire();
	}
})