({
	init : function(component, event, helper) {

		var opptyId = component.get("v.recordId");
		console.log("opptyId::"+opptyId);

		var responseStr;
		var action = component.get("c.validateForJIRACreation");
		action.setParams({"opptyId":opptyId});
		action.setCallback(this,function(resp) {

			if(resp.getState() == "SUCCESS") {

				console.log("response is success::"+resp.getReturnValue());
				responseStr = resp.getReturnValue();

				if(responseStr !== null && responseStr.startsWith("https")) {

					//"url": returnURL
					var jiraURL = responseStr.replace("OPPTYID",opptyId);
					var urlEvent = $A.get("e.force:navigateToURL");
					urlEvent.setParams({
						"url": jiraURL
					});
					$A.get("e.force:closeQuickAction").fire();
					console.log("close event fired");
					urlEvent.fire();
					console.log("url event fired");

					var infoId = component.find("infoId");
					$A.util.removeClass(infoId,"slds-show");
					$A.util.removeClass(infoId,"slds-hide");
				} else {

					component.set("v.responseMsg",responseStr);
					var infoId = component.find("infoId");
					
					$A.util.removeClass(infoId,"slds-hide");
					$A.util.removeClass(infoId,"slds-show");					
				
				}				
				
			}
			else if(resp.getState()== "ERROR") {
				console.log("error in retriving org Id.");
	        }
		});
		$A.enqueueAction(action);

		
		
	}
})