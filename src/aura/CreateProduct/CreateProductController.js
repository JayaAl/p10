({
	showModalBox : function(component, event, helper) {

		//Toggle CSS styles for opening Modal
		helper.toggleClass(component,'backGroundSectionIddrop','slds-backdrop--');
		helper.toggleClass(component,'recordSectionId','slds-fade-in-');
		
		component.set("v.errorMsgArray",null);
 		var validateCmp = component.find("validateId");
		$A.util.removeClass(validateCmp,"slds-show");
		$A.util.addClass(validateCmp,"slds-hide");

		var actionType = event.getParam("actionType");;
		// retrive data from event and do action
		console.log("In showModalBox function");
		helper.loadData(component,event);
		if(actionType == "create") {
			
			//var offerSel = component.get("v.offerSelected");
			//console.log("showModalBox : offerSel:"+offerSel);
			//helper.mediumData(component,offerSel,event);
		}
		/*helper.loadData(component,event);
		var offerSel = component.get("v.offerSelected");
		console.log("showModalBox : offerSel 2:"+offerSel);
		helper.mediumData(component,offerSel,event);*/
	},
 	hideModalBox : function(component, event, helper) {

 		//Toggle CSS styles for hiding Modal
		helper.toggleClassInverse(component,'backGroundSectionIddrop','slds-backdrop--');
		helper.toggleClassInverse(component,'recordSectionId','slds-fade-in-');
 	},
 	offerChanged: function(component, event, helper) {

		var offerSelected = component.get("v.newProduct.Offering_Type__c");
		console.log("offerSelected:"+offerSelected);
		var isOfferChanged = "yes";
		helper.mediumData(component, offerSelected, event,isOfferChanged);
	},
	mediumChanged: function(component,event,helper){

		console.log('Medium selected :'
				+component.get("v.newProduct.Medium__c"));
	},
	saveRecord : function(component, event, helper) {
	 	
	 	var errorRecorded = [];

	 	console.log("in saveRecord");
	 	// validate Input
	 	errorRecorded = helper.validateInput(component);
	 	console.log("back to save record:"+errorRecorded);
	 	// code for saving record.
	 	if(errorRecorded.length >0) {

	 		component.set("v.errorMsgArray",errorRecorded);
	 		var validateCmp = component.find("validateId");
			$A.util.removeClass(validateCmp,"slds-hide");
			$A.util.addClass(validateCmp,"slds-show");

	 	} else {
		 	// code for saving record.	 	
		 	var oppty = component.get("v.opptyRec");
		 	var newProduct = component.get("v.newProduct");
		 	var actionType = component.get("v.actType");
		 	
		 	console.log("****************oppty:"+oppty+"newProduct:"
		 			+newProduct.Offering_Type__c+newProduct.Medium__c
		 			+"");
		 	// in case of create use default if the actuals are undefiend
		 	if(newProduct.Offering_Type__c == "undefined"
		 		|| newProduct.Offering_Type__c == null
		 		|| newProduct.Offering_Type__c == "") {

		 		newProduct.Offering_Type__c = component.get("v.offerSelected");
		 	}
		 	if(newProduct.Medium__c == "undefined"
		 		|| newProduct.Medium__c == null
		 		|| newProduct.Medium__c == "") {

		 		newProduct.Medium__c = component.get("v.mediumSelected");	
		 	}
		 	console.log("**************** after adding default:"
		 		+newProduct.Offering_Type__c+newProduct.Medium__c
		 			+"");
		 	var callCtrl = component.get("c.savePLI");
		 	var serialProduct = $A.util.json.encode(newProduct);
		 	console.log("serialProduct: "+serialProduct);
		 	callCtrl.setParams({"oppty":oppty,
		 						"pliStr":"["+serialProduct+"]",
		 						"actType":actionType});
			callCtrl.setCallback(this,function(resp) {

				var callbackStatus = resp.getState();
				console.log("callbackStatus:"+callbackStatus);
				if(callbackStatus == "SUCCESS") {
					var result = resp.getReturnValue();
					console.log('record Created');
					if(result.startsWith("00k")) {
						// refresh
						//Toggle CSS styles for hiding Modal
						helper.toggleClassInverse(component,'backGroundSectionIddrop','slds-backdrop--');
						helper.toggleClassInverse(component,'recordSectionId','slds-fade-in-');
					} else {

						alert("Error:"+result);
					}
				} else if(callbackStatus == "ERROR"){
	                     var errors = resp.getError();
	                     console.log("errors before errors:"+errors);
		                if (errors) {
		                	console.log("Error message: " +
		                                 errors[0].message);
		                    if (errors[0] && errors[0].message) {
		                        console.log("Error message: " +
		                                 errors[0].message);
		                    }
		                } else {
		                    console.log("Unknown error");
		                }
	            }
			});
			$A.enqueueAction(callCtrl);
			// refresh list 
			//component.refreshPLIList();
			console.log("opptyId:"+oppty.Id);
			var refreshList = $A.get("e.c:ParentRefresh");
	        refreshList.setParams({ "opptyId": oppty.Id});
	        refreshList.fire();
			 //Toggle CSS styles for hiding Modal
			//helper.toggleClassInverse(component,'backGroundSectionIddrop','slds-backdrop--');
			//helper.toggleClassInverse(component,'recordSectionId','slds-fade-in-');
			//$A.get('e.force:refreshView').fire();
		}
	}
})