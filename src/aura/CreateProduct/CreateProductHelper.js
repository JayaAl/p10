({
	toggleClass: function(component,componentId,className) {
		var modal = component.find(componentId);
		$A.util.removeClass(modal,className+'hide');
		$A.util.addClass(modal,className+'open');
		var dateP = component.find('datePicker-openIcon');
		$A.util.addClass(dateP,'dateopenIcon');
	},

	toggleClassInverse: function(component,componentId,className) {
		var modal = component.find(componentId);
		$A.util.addClass(modal,className+'hide');
		$A.util.removeClass(modal,className+'open');
		$A.get('e.force:refreshView').fire();
	},
	loadData : function(component,event) {

		var offerTypeList = [];

		var pliId = event.getParam("rId");
		var parentId = event.getParam("pId");
		var oppty = event.getParam("oppty");
		var actionType = event.getParam("actionType");

		component.set("v.errorMsgArray",null);
	 	var validateCmp = component.find("validateId");
		$A.util.removeClass(validateCmp,"slds-show");
		$A.util.addClass(validateCmp,"slds-hide");

		component.set("v.offerSelected","--none--");
		component.set("v.mediumSelected","--none--");
		component.set("v.newProduct",null);

		if(pliId !== null && pliId !== "" && pliId !== undefined) {
			console.log("in loadData : pliId:"+pliId);
			component.set("v.pliRecId",pliId);	
		} 
		component.set("v.actType",actionType);
		component.set("v.opptyRec",oppty);

		
		// retrive record details and prepopulate in input fields.
		// retrive pli record from apex
		console.log("LoadDate b4 Calling getPLI:");
		var getPLIData = component.get("c.getPLI");

		if(pliId !== null && pliId !== "" && pliId !== undefined) {

			getPLIData.setParams({"pliId":pliId,
								"oppty":oppty});			
		} else {
			// intiate new rec for input
			getPLIData.setParams({"pliId": "",
							"oppty":oppty});
		}
		// calling getpli
		getPLIData.setCallback(this, function(response) {

			var noneVar = "--none--";
			var state = response.getState();
			console.log('getPLIData : STATE'+response.getReturnValue());
				if (state === "SUCCESS") {
					
					component.set("v.newProduct",response.getReturnValue());					
					if(actionType === "edit" || actionType === "clone") {

						var pliOfferType = component.get("v.newProduct.Offering_Type__c");
						var pliMedium = component.get("v.newProduct.Medium__c");
						console.log("loadData:pliOfferType:"+pliOfferType);
						component.set("v.offerSelected",pliOfferType);
						component.set("v.mediumSelected",pliMedium);
					} else {

						component.set("v.offerSelected",noneVar);
						component.set("v.mediumSelected",noneVar);

						component.set("v.newProduct.Offering_Type__c",noneVar);
						component.set("v.newProduct.Medium__c",noneVar);
						console.log("after initial values are set for medium an OT in case of create.");	
					}

					var getOfferType = component.get("c.getOfferingTypeValues");
					getOfferType.setCallback(this,function(offerTypes){

						var callbackStatus = offerTypes.getState();
						var existingOffer;
						console.log("Offer callbackStatus of OfferTypeValues:"
										+callbackStatus);
						
						
						if(callbackStatus == "SUCCESS") {

							var typeResult = new Array();
							console.log("*****existingOffer:"+existingOffer);
							
							if(actionType == "edit" || actionType == "clone") {

								existingOffer = component.get("v.newProduct.Offering_Type__c");
								var offerTypeListReturned = offerTypes.getReturnValue();
								offerTypeList.push(existingOffer);
								offerTypeList.push(noneVar);
								console.log("!!!!!!!!offerTypeList :"+offerTypeList);
								

								offerTypeListReturned.forEach(function(typeValue) {

									if(existingOffer !== typeValue) {

										offerTypeList.push(typeValue);
									}
								});
								//component.set("v.newProduct.Offering_Type__c",existingOffer);
								console.log("@@@@@@@offerTypeList:"+offerTypeList);
								typeResult.push.apply(typeResult,offerTypeList);
								console.log("#####offerTypist;"+typeResult);
							}
							else {

								existingOffer = noneVar;
								typeResult.push(noneVar);
								typeResult.push.apply(typeResult,offerTypes.getReturnValue());
							}
							console.log("b4$$$$$ setting offer typeResult:"+typeResult);
							component.set("v.offerTypeOptions",typeResult);
							
							// add medium options
							this.mediumData(component,existingOffer,event);
						} else {
							// error Display
						}	
					});
					$A.enqueueAction(getOfferType);

				}else if (state === "ERROR") {
	                /*var errors = response.getError();
	                if (errors) {
	                	// logs error to the console
	                    $A.log("Errors", errors);
	                    if (errors[0] && errors[0].message) {
	                    	// 
	                        $A.reportError("Error message: " +
	                                 errors[0].message);
	                    }
	                } else {
	                    $A.reportError("Unknown error");
	                }*/
                }
			});
		$A.enqueueAction(getPLIData);
		
		
		
		
	},
	mediumData : function(component,offerSelected,event,isOfferChanged) {

		//var offerSelected = component.get("v.offerSelected");
		console.log("medium Function :offerSelected:"+offerSelected);
		var pliMedium ="";
		var mediumOptions = [];//["Display"];
		var actionType = component.get("v.actType");

		console.log("in MediumData: actionType:"+actionType);
		console.log("isOfferChanged:"+isOfferChanged);

		// re set values 
		component.set("v.mediumOptions",mediumOptions);
		// retrive Medium options from cls
		var retrivedMediums = component.get("c.getMediumValues");
		retrivedMediums.setCallback(this,function(resp){

			var callbackStatus = resp.getState();

			if(callbackStatus === "SUCCESS") {

				var resultMedium = resp.getReturnValue();
				if(actionType == "edit" || actionType == "clone") {

					pliMedium = component.get("v.newProduct.Medium__c");
					mediumOptions.push(pliMedium);
					mediumOptions.push("--none--");

					if(offerSelected === "Standard"
						|| offerSelected === "Brand Station Platform"
						|| offerSelected === "Sponsorship") {

						resultMedium.forEach(function(medium) {

							if(medium != pliMedium) {

								mediumOptions.push(medium);
							}
						});
					} else if(offerSelected == "Sponsored Listening") {
							if(pliMedium !== "Display") {
								mediumOptions.push("Display");
							}
					}
				}
				else {
					mediumOptions.push("--none--");
					mediumOptions.push.apply(mediumOptions,resp.getReturnValue());

				}
				// run the below code only if its a value change
				if(isOfferChanged === "yes") {

					mediumOptions = [];
					mediumOptions.push("--none--");
					if(offerSelected === "Standard"
						|| offerSelected === "Brand Station Platform"
						|| offerSelected === "Sponsorship") {

						mediumOptions.push.apply(mediumOptions,resp.getReturnValue());

					} else if(offerSelected == "Sponsored Listening") {
						
							mediumOptions.push("Display");						
						}
						
				}
				console.log("after else medium Options : "+mediumOptions);
				//mediumOptions = resp.getReturnValue();
				component.set("v.mediumOptions",mediumOptions);
				$A.get('e.force:refreshView').fire();
			} else {
				// error
			}
		});		
		$A.enqueueAction(retrivedMediums);
		
			
		console.log(" out of enqueue mediumOptions:"+mediumOptions);
		
	},
	validateInput : function(component) {

		var validErrorList = [];
		var newProduct = component.get("v.newProduct");
		console.log("in validateInput"+
			newProduct.UnitPrice+
			newProduct.ServiceDate+
			newProduct.End_Date__c);

		if(newProduct.Offering_Type__c === "" 
			|| newProduct.Offering_Type__c === null
			|| typeof(newProduct.Offering_Type__c) == "undefined"
			|| newProduct.Offering_Type__c === "--none--") {

			validErrorList.push("Offering Type is required.");
            //component.set("v.offerErrCls","errorHighlig");
		}
		
		if(newProduct.ServiceDate === "" 
			|| newProduct.ServiceDate === null
			|| typeof(newProduct.ServiceDate) == "undefined") {

			validErrorList.push("Start Date is required.");
			//component.set("v.startDateErrCls","errorDateHighlig");
		}
		if(newProduct.End_Date__c === "" 
			|| newProduct.End_Date__c === null
			|| typeof(newProduct.End_Date__c) == "undefined") {

			validErrorList.push("End Date is required.");
            //component.set("v.endDateErrCls","errorDateHighlig");
		}
		console.log("in validate :"+validErrorList);
		//component.set("v.errorMsgArray",validErrorList);
		//console.log("in validate :"+component.get("v.errorMsgArray"));
		return validErrorList;
	}

})