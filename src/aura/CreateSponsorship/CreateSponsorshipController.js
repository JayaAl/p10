({
	init : function(component, event, helper) {

		var opptyId = component.get("v.recordId");
		console.log("opptyId::"+opptyId);

		var orgId;
		var returnURL;
		var action = component.get("c.determineCurrentInstance");
		action.setCallback(this,function(resp) {

			if(resp.getState() == "SUCCESS") {

				orgId = resp.getReturnValue();

				if (orgId) {
					
					returnURL = "https://matterhorn-stage.savagebeast.com/sfopportunity?opportunity_id="+opptyId; 
				} else { 
					returnURL = "https://matterhorn.savagebeast.com/sfopportunity?opportunity_id="+opptyId;
				}
				//component.set("v.retutnUrl",returnURL);
				var urlEvent = $A.get("e.force:navigateToURL");
				urlEvent.setParams({
					"url": returnURL
				});
				
				$A.get("e.force:closeQuickAction").fire();
				console.log("close event fired");
				urlEvent.fire();
				console.log("url event fired");
				//component.destory;
			}
			else if(resp.getState()== "ERROR") {
				console.log("error in retriving org Id.");
	        }
		});
		$A.enqueueAction(action);

		
		
	}
})