({
	editRecord : function(component) {

		console.log("b4 helper callout: "+component.get("v.pliId"));
		console.log("callout : : "+$A.get("e.force:editRecord"));
        var editRecordEvent = $A.get("e.force:editRecord");
        editRecordEvent.setParams({
            "recordId": component.get("v.pliId")
        });
        editRecordEvent.fire();
	}
})