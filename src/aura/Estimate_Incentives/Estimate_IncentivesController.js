({
	myInitAction : function(component, event, helper) {
		console.log("myInitAction");
        var opptyId = component.get("v.recordId");
        var sId;
        var orgId;
        console.log("record Id retrived:"+opptyId);
        var action = component.get("c.getPSessionID");
        action.setCallback(this,function(resp)	{
        	
			var sIdState = resp.getState();
        	if(component.isValid() && sIdState === "SUCCESS")	{
    			sId = resp.getReturnValue();        
                console.log("sId:"+resp.getReturnValue());
	        }
		})
    	$A.enqueueAction(action);
        
        var orgIdAction = component.get("c.getPOrgId");
        orgIdAction.setCallback(this,function(resp)	{
			
            if(component.isValid() &&  resp.getState() === "SUCCESS")	{
            	orgId = resp.getReturnValue();
            console.log("orgId:"+orgId.getReturnValue());
        	}
		}) 
        $A.enqueueAction(orgIdAction);
        
        // build the url
        //https://www.xactlycorp.com/xintegrations/jsp/incent_estimator.jsp?opId={!Opportunity.Id}&sfSessionId={!API.Session_ID}&sfServerUrl={!API.Partner_Server_URL_150}&sfPartnerServerUrl={!API.Partner_Server_URL_150}&sfOrgId={!Organization.Id}"
		//var urlStr = "https://www.xactlycorp.com/xintegrations/jsp/incent_estimator.jsp?opId="+opptyId+"&sfSessionId="+sId+"sfOrgId="+orgId; //&sfServerUrl="++"&sfPartnerServerUrl="++"&
        
    	var urlEvent = $A.get("e.force:navigateToURL");
    	urlEvent.setParams({
     	 "url": "https://www.google.com/maps/place"
    	});
   		 urlEvent.fire();
	}
})