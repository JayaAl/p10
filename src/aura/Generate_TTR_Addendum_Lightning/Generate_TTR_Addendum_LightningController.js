({
	init : function(component, event, helper) {

		var opptyId = component.get("v.recordId");
		var serverUrl = component.get("v.serverUrl");
		var action = component.get("c.buildCongaURL");

		console.log("opptyId:"+opptyId+"end of opptyId");
		console.log("serverUrl:"+serverUrl);
		action.setParams({"opptyId":opptyId,
						"serverURL":serverUrl});
		action.setCallback(this,function(resp) {

			console.log("resp.getState:"+resp.getState());
			if(resp.getState() == "SUCCESS") {

				respStr = resp.getReturnValue();
				console.log('respStr:'+respStr);
				if(respStr !== null && respStr.startsWith("https")) {
					
					//$A.get("e.force:closeQuickAction").fire();
					component.set("v.serverUrl",respStr);
					//window.open(respStr,"TTR From Generation",700,500, true);
					//var dismissActionPanel = $A.get("e.force:closeQuickAction");
					//dismissActionPanel.fire();
					var congaId = component.find("congaId");
					$A.util.removeClass(congaId,"slds-hide");
					$A.util.addClass(congaId,"slds-show");
					
					//component.destory();
				} else {

					console.log('IN ELSE respStr:'+respStr);
					component.set("v.responseMsg",respStr);
					var infoId = component.find("msgId");
					$A.util.removeClass(infoId,"slds-hide");
					$A.util.addClass(infoId,"slds-show");
				}
			}
		// calling getOpportunityRecord
		});
		$A.enqueueAction(action);
	}
})