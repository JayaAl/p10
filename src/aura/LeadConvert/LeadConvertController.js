({
	init : function(component, event, helper) {

		console.log("init");
		//helper.getLeadData(component,event);

	},
	createNewAcctOption : function(component, event, helper) {

		// if the value in the createnew account is true disable the lookup component
		var createNewActFlag = component.find("createNewAct").getElement().checked;
		if(createNewActFlag) {

			clonePageUrl += "&cProds=1";
		} 
	},
	ownerChanged : function(component, event,helper) {

	},
	proceedLeadConversion :function(component, event, helper) {

		helper.cancelWorkflow(component,event);

	},
	cancelConverion : function(component, event, helper) {

		$A.get("e.force:closeQuickAction").fire();
	}
})