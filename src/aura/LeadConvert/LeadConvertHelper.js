({
	/*getLeadData : function(component,event) {

		console.log("in getLeadData");
		var leadId = component.get("v.recordId");
		var callCtrl = component.get("c.getLead");
		callCtrl.setParams({"leadId":leadId});

		var getLeadStatus = component.get("c.statusList");
		console.log("leadId:"+leadId);
		getLeadStatus.setCallback(this,function(resp) {

			var statusCallState = resp.getState();
			if(statusCallState === "SUCCESS") {

				var statusList = resp.getReturnValue();
				component.set("v.convertedStatusOptions",statusList);
				console.log("statusList:"+statusList);
				callCtrl.setCallback(this,function(resp) {

					var callbackStatus = resp.getState();
					console.log('getLead :callbackStatus:'+callbackStatus);
					if(callbackStatus === "SUCCESS") {

						var retrivedLead = resp.getReturnValue();
						component.set("v.leadRecord",retrivedLead);
					}else if(callbackStatus === "ERROR"){
		                    //alert('Error in calling server side action');
		                console.log('error in Lead data retrival');
		            }
				});
				$A.enqueueAction(callCtrl);
			}
		});
		$A.enqueueAction(getLeadStatus);		
	},
*/
	cancelWorkflow :  function (component,event) {

		var leadId = component.get("v.recordId");
		var callCtrl = component.get("c.saveLead");
		callCtrl.setParams({"leadId":leadId});

		callCtrl.setCallback(this,function(resp) {
			var callbackStatus = resp.getState();
			console.log('cancelWorkflow :callbackStatus:'+callbackStatus);
			if(callbackStatus === "SUCCESS") {
				var cancelWFLeadId = resp.getReturnValue();
				console.log('cancelWFLeadId:'+cancelWFLeadId);
				$A.get("e.force:closeQuickAction").fire();
				/*var leadConvertUrl = '/lead/leadconvert.jsp?nooppti=1&id='
									+cancelWFLeadId+'&RetURL=/'+cancelWFLeadId;
				component.set("v.serverUrl",leadConvertUrl);
				
				var lcURL = component.find("urlId");
				$A.util.removeClass(lcURL,"slds-hide");
				$A.util.addClass(lcURL,"slds-show");
				*/
			}else if(callbackStatus === "ERROR"){
				//alert('Error in calling server side action');
				console.log('error in Lead update action');
			}
		});
		$A.enqueueAction(callCtrl);
	}
})