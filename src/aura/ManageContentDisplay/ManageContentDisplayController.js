({
	initPage : function(component, event, helper) {


		// determine object
		var lookupFieldName;

		var opptyId = component.get("v.recordId");
		var viewAllFlag = component.get("v.isViewAll");
		console.log("v:::::::"+viewAllFlag);
		component.set("v.isViewAll",viewAllFlag);
		/*
			This is only for sfdc support purpose
		*/
		var rId = event.getParam("recordId");
		var v = event.getParam("isViewAll");
		console.log("rId:"+rId+" v:"+v);
		// retrive event fired
		var selectedSearchFolder = "All";
		var eventSource = event.getSource();
		console.log("event source: "+eventSource);
		// retrive prefix
		console.log("****** b4 helper");
		helper.determineController(component,event,opptyId);
		lookupFieldName = component.get("v.recordPrefix");
		console.log("******lookupFieldName:"+lookupFieldName);
		if(eventSource.toString().includes("ManageContentFilter")) {

			selectedSearchFolder = event.getParam("filter");
		}
		console.log("selectedSearchFolder:"+selectedSearchFolder);
		var getContent = component.get("c.getExistingContent");
		getContent.setParams({"selectedSearchFolder":selectedSearchFolder,
							"lookupFieldName":lookupFieldName,
							"currentRecordId":opptyId,
							"viewAll":viewAllFlag});
		getContent.setCallback(this,function(resp) {

			var contentRetriveStatus = resp.getState();
			console.log('initPage :contentRetriveStatus:'+contentRetriveStatus);
			if(contentRetriveStatus == "SUCCESS") {

				var returnList = resp.getReturnValue();
				console.log('initPage :resp:'+$A.util.isEmpty(returnList));
				if(! $A.util.isEmpty(returnList)) {
                    
					console.log('initPage:resp is not empty');
					component.set("v.contentVersionList",returnList);
				} else {
					console.log('getPlis:resp is empty');
					component.set("v.contentVersionList","");
				}
				
			} else if(contentRetriveStatus == "ERROR"){
                    //alert('Error in calling server side action');
                console.log('error in data retrival');
            }
		});
		$A.enqueueAction(getContent);
	},
	viewAllRec : function(component, event, helper) {

		console.log("in viewAllRec function");
		/*
		working : */
		var evt = $A.get("e.force:navigateToComponent");
		console.log("in viewAllRec function2");
        evt.setParams({
            componentDef : "c:ManageContentDisplay",
            componentAttributes: {
                recordId : component.get("v.recordId"),
                isViewAll : true
            }
        });
        console.log("in viewAllRec function3");
        evt.fire();
		
        ///var urlEvt = $A.get("e.force:navigateToURL");
        /*
        	/apex/LightningContentViewAll?id='
        		+component.get("v.recordId")
    			});
        */
        ///urlEvent.setParams({"url":'/006/o'
    		///	});
        ///urlEvent.fire();
       /* var evt = $A.get("e.c:ManageContentEvent");
        evt.setParams({
        	"recordId" : component.get("v.recordId"),
            "isViewAll" : true
        });
        evt.fire();*/
	}
})