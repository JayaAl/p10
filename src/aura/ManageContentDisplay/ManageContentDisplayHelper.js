({
	determineController : function(component,event,recordId) {

		var lookupFieldName;
		console.log("recordId:"+recordId);
		if(recordId !== null) {
			var recordPrefix = recordId.substring(0,3);
			console.log("recordPrefix:"+recordPrefix);
			switch (recordPrefix) {
			    case "006":
			        lookupFieldName = "Opportunity__c";
			        break;
			    case "001":
			        lookupFieldName = "Account__c";
			        break;		    
			}
			console.log("lookuoFieldName:"+lookupFieldName);
			component.set("v.recordPrefix",lookupFieldName);
		}
	}
})