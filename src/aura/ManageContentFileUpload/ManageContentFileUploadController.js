({  
	initPage : function(component, event, helper) {

		// get recordId and record Type
		var recordId = component.get("v.recordId"); 
		helper.determineController(component,event,recordId);
		var recordSObj = component.get("v.recordObj");
    	// get folder Options
    	var getFolderOptions = component.get("c.folderOptions");
    	var getWorkspaceOptions = component.get("c.workspaceOptions");
    	getWorkspaceOptions.setParams({
    				sObjectType:recordSObj,
    				currentRecordId: recordId
    	});
    	getWorkspaceOptions.setCallback(this,function(resp) {

    		var callbackStatus = resp.getState();
    		console.log("workspaceCallback is : "+callbackStatus);
    		if(callbackStatus == "SUCCESS") {

    			console.log("workspaceCallback is Success");
    			var workspaceOptions = resp.getReturnValue();
    			var workspaceOptionsList = [];
    			var workspaceOptionsIdList = [];
    			for(k in workspaceOptions) {

    				workspaceOptionsIdList.push(workspaceOptions[k]);
    				workspaceOptionsList.push(k);
    				console.log("workspaceCallback values returned : "
    						+workspaceOptions[k]);
    			}
    			component.set("v.workspaceOptions",workspaceOptionsList);
    			component.set("v.workspaceOptionMap",workspaceOptions);
    			component.set("v.selectedWorkspace",workspaceOptionsList[0]);
    			component.set("v.selectedWorkspaceID",workspaceOptionsIdList[0]);
    		}
    	});
    	$A.enqueueAction(getWorkspaceOptions);
    	
    	getFolderOptions.setCallback(this,function(resp){

			var callbackStatus = resp.getState();
    		if(callbackStatus == "SUCCESS") {

    			var folderOptions = resp.getReturnValue();
    			component.set("v.folderOptions",folderOptions);
    			component.set("v.selectedFolder",folderOptions[0]);
    		}
		});
		$A.enqueueAction(getFolderOptions);
    	// get workspace Options
    },
    workspaceChanged : function(component, event, helper) {

    	var workspaceMap = component.get("v.workspaceOptionMap");
    	var selectedWorkspace = component.get("v.selectedWorkspace");
    	for(k in workspaceMap) {

    		if(selectedWorkspace === workspaceMap[k]) {

    			component.set("v.selectedWorkspaceID",workspaceMap[k]);
    		}
    	}

    },
    folderChanged : function(component, event, helper) {

    	console.log("workspaceId SElected :"+component.get("v.selectedWorkspaceID"));
    	console.log("folder Selected :"+component.get("v.selectedFolder"));
    },
    save : function(component, event, helper) {

    	// adding enable and disble uploading
		var modal = component.find("uploading");
		$A.util.removeClass(modal,"slds-hide");
		$A.util.addClass(modal,"slds-show");

    	console.log("workspaceId SElected :"
    			+component.get("v.selectedWorkspaceID"));
    	console.log("folder Selected :"+component.get("v.selectedFolder"));
        helper.save(component,event);
    },
    waiting: function(component, event, helper) {
    	//$A.util.addClass(component.find("uploading").getElement(), "uploading");
    	//$A.util.removeClass(component.find("uploading").getElement(), "notUploading");
    	// adding enable and disble uploading
		var modal = component.find("uploading");
		$A.util.removeClass(modal,"slds-hide");
		$A.util.addClass(modal,"slds-show");

    },
    
    doneWaiting: function(component, event, helper) {
    	//$A.util.removeClass(component.find("uploading").getElement(), "uploading");
    	//$A.util.addClass(component.find("uploading").getElement(), "notUploading");
    	var modal = component.find("uploading");
		$A.util.removeClass(modal,"slds-show");
		$A.util.addClass(modal,"slds-hide");
    }
})