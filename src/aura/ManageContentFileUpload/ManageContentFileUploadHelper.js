({
   MAX_FILE_SIZE: 750 000, /* 1 000 000 * 3/4 to account for base64 */

    save : function(component,event) {

    	console.log("save function in helper");
        var fileInput = component.find("file").getElement();
    	var file = fileInput.files[0];
   		console.log("save function");
        if (file.size > this.MAX_FILE_SIZE) {
            alert('File size cannot exceed ' + this.MAX_FILE_SIZE + ' bytes.\n' +
    		  'Selected file size: ' + file.size);
    	    return;
        }
    	console.log("save function file sizein limit");
        var fileReader = new FileReader();
        
	var self = this;
       	fileReader.onload = function() {
            var fileContents = fileReader.result;
    	    var base64Mark = 'base64,';
            var dataStart = fileContents.indexOf(base64Mark) + base64Mark.length;
			console.log("save function onload function");
            fileContents = fileContents.substring(dataStart);
        	console.log("save function b4 calling self load");
    	    self.upload(component, file, fileContents);
        };
		console.log("save function readAsDataURL");
        fileReader.readAsDataURL(file);

       
    },
   upload: function(component, file, fileContents) {

   		var recordId = component.get("v.recordId");
    	var selectedFolder = component.get("v.selectedFolder");
    	var selectedWorkspaceId = component.get("v.selectedWorkspaceID");
    	console.log("selectedFolder:"+selectedFolder);
        var action = component.get("c.saveTheFile");

        //get prefix of the record Id
        // is gettign set in init it self
        // this.determineController(component,event,recordId);
        var recordSObj = component.get("v.recordObj");
		console.log("upload func b4 saveTheFile call param set");
        console.log("selectedWorkspaceId:"+selectedWorkspaceId
        			+" pId:"+component.get("v.recordId")
        			+"filename:"+file.name
                    +"base64Data:"+encodeURIComponent(fileContents)
                    +"content type:"+file.type);
        action.setParams({
            parentId: component.get("v.recordId"),
            fileName: file.name,
            base64Data: encodeURIComponent(fileContents), 
            contentType: file.type,
            selectedFolder: selectedFolder,
            selectedWorkspaceId: selectedWorkspaceId,
            sObjType: recordSObj
        });
		console.log("upload func b4 saveTheFile callout");

		// return promise
		action.setCallback(this, function(response) {
            var resp = response.getState();
            console.log("status for upload : "+resp);
            if(resp == "SUCCESS") {
            	var attachId = response.getReturnValue();
            	console.log("callout successfull :"+attachId);

            	if(attachId.substring(0,3) == "069"
                    || attachId.substring(0,3) == "068") {
	            	// fire refresh event
			        console.log("save function readAsDataURL");
			        var filter = "All";
			        var recordId = component.get("v.recordId");
			        var refreshList = $A.get("e.c:ManageContentEvent");
			        refreshList.setParams({ "recId": recordId,
			    						"filter":filter});
			        console.log("b4 firing event");
			        refreshList.fire();
			        console.log("event fired");
			    } else {

			    	alert(attachId);
                    var recordId = component.get("v.recordId");
                    var refreshList = $A.get("e.c:ManageContentEvent");
                    refreshList.setParams({ "recId": recordId,
                                        "filter":filter});
                    console.log("b4 firing event");
                    refreshList.fire();
                    console.log("event fired");
			    }

            }
        });
        $A.enqueueAction(action);
    },
    determineController : function(component,event,recordId) {

		var lookupFieldName;
		console.log("recordId:"+recordId);
		if(recordId !== null) {
			var recordPrefix = recordId.substring(0,3);
			console.log("recordPrefix:"+recordPrefix);
			switch (recordPrefix) {
			    case "006":
			        lookupFieldName = "Opportunity";
			        break;
			    case "001":
			        lookupFieldName = "Account";
			        break;		    
			}
			console.log("lookuoFieldName:"+lookupFieldName);
			component.set("v.recordObj",lookupFieldName);
		}
	}
})