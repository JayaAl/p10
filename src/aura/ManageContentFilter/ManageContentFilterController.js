({
	initPage : function(component, event, helper) {

		var opptyId = component.get("v.recordId");
		// get folder Options
    	var getFolderOptions = component.get("c.folderOptions");
    	getFolderOptions.setCallback(this,function(resp){

			var callbackStatus = resp.getState();
			console.log("####ilter FolderOptions status = "+callbackStatus);
    		if(callbackStatus == "SUCCESS") {

    			var folderOptions = [];
    			folderOptions.push("All");
    			var retrivedFolderOptions = resp.getReturnValue();
    			retrivedFolderOptions.forEach(function(folder) {
    				folderOptions.push(folder);
    			});
    			
    			component.set("v.folderOptions",folderOptions);
    			component.set("v.selectedFolder",folderOptions[0]);
    		}
		});
		$A.enqueueAction(getFolderOptions);
		
	},
	folderChanged : function(component, event, helper) {

		// refresh retrival results
		// refresh the list display 
        var refreshList = $A.get("e.c:ManageContentEvent");
        console.log("b4 firing managecontentevent : f:"+component.get("v.selectedFolder")
        			+" rec :"+component.get("v.recordId"));
        refreshList.setParams({ "recId": component.get("v.recordId"),
    						"filter":component.get("v.selectedFolder")});
        refreshList.fire();
	}
})