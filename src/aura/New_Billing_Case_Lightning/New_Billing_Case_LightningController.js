({
	initPage : function(component, event, helper) {

		
		var opptyId = component.get("v.recordId");
		console.log('opptyId:'+opptyId);
		var callCtrl = component.get("c.getPrepopulateData");
		callCtrl.setParams({"opptyId":opptyId});
		callCtrl.setCallback(this,function(resp) {

			var callbackStatus = resp.getState();
			if(callbackStatus == "SUCCESS") {

				var result = resp.getReturnValue();
				//var caseResult = JSON.parse(result);
				console.log('response:'+result.caseObj);
				console.log('response start date:'+result[0].caseObj.Campaign_Start_Date2__c);
				//console.log('non json parse response:'+caseResult);
				//if(!$A.util.isEmpty(resultresult) && !$A.util.isUndefined(result)) {

					component.set("v.caseWrapper",result.caseObj);
					//console.log('value set:'+caseResult[0].Subject);
				//}
				var optyId = result[0].caseObj.Opportunity__c;
				var caseType = result[0].caseObj.Type;
				var caseSub = result[0].caseObj.Subject;
				var status = result[0].caseObj.Status;
				console.log('case Data :'+optyId+' type:'+caseType);
				var newCaseRec = {'sobjectType':'Case',
									'Opportunity__c':optyId,
									'Type':caseType,
									'Subject':caseSub,
									'Status':status};
				component.set("v.caseRec",newCaseRec);
			} else if(state == "ERROR"){
                    alert('Error in calling server side action');
            }
		});
		$A.enqueueAction(callCtrl);
	}

	// Prepare a new record from template
    /*    
    var caseRecordMap [];
		var caseRecDetails;
    component.find("caseRecordCreator").getNewRecord(
            		"Case", // sObject type (entity API name)
            		caseRecDetails.RecordTypeId,      // record type
            		caseRecordMap,      // default record values
            		false,     // skip cache?
            		$A.getCallback(function() {
	                	var rec = component.get("v.caseRec");
	                	var error = component.get("v.newRecordError");
	                	if(error || (rec === null)) {
	                    	console.log("Error initializing record template: " + error);
	                	}
	                	else {
	                    	console.log("Record template initialized: " + rec.sobjectType);
	                	}
            		})
        );*/
	
})