({
	getForecastData : function(component,event) {

		var acctOwnerId = event.getParam("acctOwnerId");
		var acctName = event.getParam("acctName");
		var acctStatus = event.getParam("acctStatus");
		console.log("In Search Results : acctOwnerId:"+acctOwnerId 
										+" acctName:"+acctName
										+" acctStatus:"+acctStatus);
		var getNonguaranteedForecast = component.get("c.getNonguaranteedList");
		getNonguaranteedForecast.setParams({"acctName":acctName,
											"acctStatus":acctStatus,
											"acctOwnerId":acctOwnerId});
		getNonguaranteedForecast.setCallback(this,function(resp) {

			var respStatus = resp.getState();
			var pageSize = component.get("v.pageSize");
			console.log("respStatus:"+respStatus);
			if(respStatus == "SUCCESS") {

				var forecastList = resp.getReturnValue();

				if(!$A.util.isEmpty(forecastList) 
					&& !$A.util.isUndefined(forecastList)) {

					console.log("forecastList:"+forecastList[0].advertisier);
					component.set("v.nonGuaranteedWrapperList",forecastList);
					console.log("list:"
						+component.get("v.nonGuaranteedWrapperList"));	

					var totalSize = component.get("v.nonGuaranteedWrapperList").length;

					component.set("v.totalSize",totalSize);
					component.set("v.first",0);
					component.set("v.last",pageSize-1);

					var tempPageList = [];
					for(var i=0; i< pageSize && i< forecastList.length; i++) {
						console.log("forecastList:"+forecastList[i].Id);
						if(forecastList[i] != null) {
							tempPageList.push(forecastList[i]);
						}
					}
					component.set("v.wrapperPerPageList",tempPageList);
					component.set("v.totalSize",forecastList.length);

					console.log("getForecastData:res totalSize:"+totalSize
							+"pageSize:"+pageSize);
					
					this.setHasNext(component,totalSize,pageSize,pageSize-1);
        			this.setHasPrevious(component,totalSize,pageSize,0);

				} else {

					console.log('getforecastData:resp is empty');
					
					component.set("v.nonGuaranteedWrapperList",null);
					component.set("v.totalSize",0);
					this.setHasNext(component,0,pageSize,pageSize-1);
        			this.setHasNext(component,0,pageSize,0);
				}
				
			} else if(respStatus == "ERROR"){
				
				console.log("Error in retriving forecast results resp:"+resp);
		    }
		});
		$A.enqueueAction(getNonguaranteedForecast);
	},
	next  : function(component,event) {

		var wrapperList = component.get("v.nonGuaranteedWrapperList");
		var first = component.get("v.first");
		var last = component.get("v.last");
		var pageSize = component.get("v.pageSize");
		var totalSize = component.get("v.totalSize");
		var tempPageList = [];

		first = first+pageSize;
		last = last+pageSize;

		if(last >= totalSize) {

			last = last-1;
		} 

		console.log("In next display:"+first+":"+last
					+":"+pageSize+":"+totalSize);
		for(var i=first; i<=last; i++) {

			if(wrapperList[i] != null) {
				tempPageList.push(wrapperList[i]);
			}
			console.log("current prod:"+wrapperList[i]);
		}
		component.set("v.wrapperPerPageList",tempPageList);
		component.set("v.first",first);
		component.set("v.last",last);

		console.log("tempPageList:"+tempPageList);

		this.setHasNext(component,totalSize,pageSize,last);
        this.setHasPrevious(component,totalSize,pageSize,first);
	},
	previous : function(component,event) {

		var wrapperList = component.get("v.nonGuaranteedWrapperList");
		var first = component.get("v.first");
		var last = component.get("v.last");
		var pageSize = component.get("v.pageSize");
		var tempPageList = [];

		var totalSize = wrapperList.length;

		last = last-pageSize;
		first = first-pageSize;

		if(last == first) {

			last = pageSize-1;
		}

		for(var i=first; i<=last; i++) {

			if(wrapperList[i] != null) {
				tempPageList.push(wrapperList[i]);
			}
		}
		component.set("v.wrapperPerPageList",tempPageList);
		component.set("v.first",first);
		component.set("v.last",last);
		console.log("In previous display:"+first+":"+last+":"+pageSize);
		
        this.setHasNext(component,totalSize,pageSize,last);
        this.setHasPrevious(component,totalSize,pageSize,first);
	},
	setHasNext : function(component,totalSize,pageSize,last) {

		console.log("#### In hasNext totalSize"+totalSize
			+"pageSize"+pageSize
			+"last"+last);
		if(totalSize > pageSize-1 && last < totalSize-1) {

			console.log("#### In hasNext"+false);
			component.set("v.hasNext",false);
		} else {

			console.log("#### In hasNext"+true);
			component.set("v.hasNext",true);
		}
	},
	setHasPrevious : function(component,totalSize,pageSize,first) {

		console.log("#### In hasPrevious"+first);
		if(first == 0) {

			component.set("v.hasPrevious",true);
		} else {

			component.set("v.hasPrevious",false);
		}
	}
})