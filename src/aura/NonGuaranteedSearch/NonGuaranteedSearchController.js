({
	initPage : function(component, event, helper) {

		console.log("on Search Page");
		helper.getCurrentUser(component,event,helper);
	},
	search : function(component, event, helper) {

		var searchEvent = $A.get("e.c:NonGuaranteedSearchEvent");
        //searchEvent.setParams({});
        var accountOwnerId = component.get("v.accountOwnerId");
        console.log("accountOwnerId in search:"+accountOwnerId);
        searchEvent.setParams({"acctStatus": "",
        					"acctOwnerId":accountOwnerId,
        					"acctName":""});
        searchEvent.fire();
        console.log("Search event fired");
	},
	lookupSelectedEvent : function(component,event,helper) {

		console.log("in lookupSelectedEvent::::::");
		var accountOwner = event.getParam("lookupEvent");
        component.set("v.accountOwnerId" , accountOwner.Id);
        console.log("component event handler accountOwner:"+accountOwner.Id);
	}
})