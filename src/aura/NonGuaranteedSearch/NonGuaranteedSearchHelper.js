({
	getCurrentUser : function(component,helper,event) {

		var getUserId = component.get("c.getCurrentUserId");
		getUserId.setCallback(this,function(resp) {

			console.log("User Retrival on Load:"+resp.getState());
			if(resp.getState() == "SUCCESS") {

				var currentUser = resp.getReturnValue();
				console.log("user retrived:"+currentUser);
				component.set("v.accountOwnerId",currentUser.Id);
				
			} else {
				console.log("Error in retriving current user Id.");
			}
			
		});
		$A.enqueueAction(getUserId);
	}
})