({
	init : function(component, event, helper) {

		var opptyId = component.get("v.recordId");
		console.log("opptyId::"+opptyId);

		var clonePageUrl;
		var action = component.get("c.determineClonePage");
		action.setParams({"opptyId":opptyId});
		action.setCallback(this,function(resp) {

			opptyClone = resp.getState();
			console.log("Oppty status : "+opptyClone);
			if(opptyClone == "SUCCESS") {
				
				
				clonePageUrl = resp.getReturnValue();
				console.log("clonePageUrl :"+clonePageUrl);

				
				if(clonePageUrl == "006") {

					clonePageUrl = "/"+clonePageUrl+"/o";
				}
				else {

					clonePageUrl = "/apex/"+clonePageUrl;
			    }
			    console.log("clonePageUrl:"+clonePageUrl);
			    component.set("v.redirectUrl",clonePageUrl);
			}
			else if(opptyClone == "ERROR") {
				console.log("error in opportunity clone");
	        }
		});
		$A.enqueueAction(action);
	},

	productClone : function(component,event,helper) {

		var cloneProducts = component.find("cloneOptionId").getElement().checked;
		var clonePageUrl = component.get("v.redirectUrl");

		if(cloneProducts) {

			clonePageUrl += "&cProds=1";
		} 
		component.set("v.redirectUrl",clonePageUrl);
	},

	cloneOpportunity : function(component,event,helper) {

		var clonePageUrl = component.get("v.redirectUrl");
		var urlEvent = $A.get("e.force:navigateToURL");
		urlEvent.setParams({

			      	"url": clonePageUrl
			    });
		console.log("clonePageUrl:"+clonePageUrl);
		urlEvent.fire();	
	}

})