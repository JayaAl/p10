({
	initPage : function(component, event, helper) {
		console.log("in initpage");
		
		helper.getPlis(component,event);
		// get oppty details
		helper.getOpptyRec(component,event);
		var oppty = component.get("v.oppty");
		console.log("oppty:"+oppty);
		
	},
	next : function(component,event,helper) {

		helper.next(component,event,helper);
	},
	previous : function(component,event,helper) {

		helper.previous(component,event,helper);
	},
	selectedRecord: function(component, event, helper) {
        //Get data via "data-data" attribute from button (button itself or icon's parentNode)
        var checkCmp = component.find("chekboxId");
        console.log("selected PLI : "+checkCmp.get("v.value")
				+"selected PLI Id : "+checkCmp.get("v.text"));
		
	},
	offerSelected: function(component, event, helper) {

		var offerSelected = event.source.get("v.label");
		console.log("Radio offerSelected:"+offerSelected);
	},
	showAddProduct : function(component, event, helper) {
		
		var userEvent = $A.get("e.c:popup");
        userEvent.setParams({ "rId": "",
        				"pId":undefined,
        				"oppty":component.get("v.oppty"),
        				"actionType":"create"});
        userEvent.fire();
        console.log("back to related list");
	},
	editProduct : function(component,event,helper) {
		console.log(" In editProduct rec:");
		helper.editProduct(component,event);
		//helper.getPlis(component,event);
		//helper.getOpptyRec(component,event);
		var oppty = component.get("v.oppty");
		///$A.get('e.force:refreshView').fire();
	}, 
	delProduct : function(component,event,helper) {
		helper.delProduct(component,event);
		// refresh list
		helper.getPlis(component,event);
		helper.getOpptyRec(component,event);
		var oppty = component.get("v.oppty");
		$A.get('e.force:refreshView').fire();
	},
	
})