({
	getOpptyRec : function(component,event) {

		var opptyId = component.get("v.recordId");
		// retrive oppty details
		var getOppty = component.get("c.getOppty");
		getOppty.setParams({"opptyId":opptyId});
		getOppty.setCallback(this,function(resp) {

			var respStatus = resp.getState();
			if(respStatus == "SUCCESS") {

				var opptyRecord = resp.getReturnValue();
				component.set("v.oppty",opptyRecord);
			} else if(callbackStatus == "ERROR"){
                    //alert('Error in calling server side action');
            }
		});
		$A.enqueueAction(getOppty);
	},
	delProduct : function(component,event) {

		var pliId = event.target.getAttribute("data-data") 
					|| event.target.parentNode.getAttribute("data-data");
		var delPLI = component.get("c.deletePLI");
		delPLI.setParams({"pliId":pliId});
		delPLI.setCallback(this,function(resp) {

			var respStatus = resp.getState();
			if(respStatus == "SUCCESS") {
				console.log(resp.getReturnValue());
			} else if(callbackStatus == "ERROR"){
                console.log(resp.getReturnValue());
            }
		});
		$A.enqueueAction(delPLI);

	},
	editProduct : function(component,event) {

		var oppty = component.get("v.oppty");
		console.log("oppty:"+oppty);
		var pliId = event.target.getAttribute("data-data") 
					|| event.target.parentNode.getAttribute("data-data");
		console.log("In edit product pliId : "+pliId);
		var actionType = event.target.getAttribute("data-actionType")
							|| event.target.parentNode.getAttribute("data-actionType");
		console.log("In editProduct actionType:"+actionType);
		var editRecord = $A.get("e.c:popup");
        editRecord.setParams({ "rId": pliId,
        					"pId":component.get("v.recordId"),
        					"oppty":component.get("v.oppty"),
        					"actionType":actionType});
        editRecord.fire();
	},
	getPlis : function(component,event) {

		var opptyId = component.get("v.recordId");

		console.log('getPlis: opptyId:'+opptyId);
		var callCtrl = component.get("c.getPLIList");
		callCtrl.setParams({"opptyId":opptyId});
		callCtrl.setCallback(this,function(resp) {

			var pageSize = component.get("v.pageSize");
			var callbackStatus = resp.getState();
			console.log('getPlis :callbackStatus:'+callbackStatus);
			if(callbackStatus == "SUCCESS") {

				var returnList = resp.getReturnValue();
				console.log('getPlis :resp:'+$A.util.isEmpty(returnList));
				if(! $A.util.isEmpty(returnList)) {

					console.log('getPlis:resp is not empty');
					var result = resp.getReturnValue();
					
					component.set("v.pliList",returnList);
					var totalSize = component.get("v.pliList").length;

					component.set("v.totalSize",totalSize);
					component.set("v.first",0);
					component.set("v.last",pageSize-1);

					var tempPageList = [];
					for(var i=0; i< pageSize && i< returnList.length; i++) {
						console.log("returnList:"+returnList[i].Id);
						if(returnList[i] != null) {
							tempPageList.push(returnList[i]);
						}
					}
					component.set("v.pliPerPageList",tempPageList);
					component.set("v.totalSize",returnList.length);

					console.log("getPlis:res totalSize:"+totalSize
							+"pageSize:"+pageSize);
					
					this.setHasNext(component,totalSize,pageSize,pageSize-1);
        			this.setHasPrevious(component,totalSize,pageSize,0);

				} else {

					console.log('getPlis:resp is empty');
					
					component.set("v.pliList",null);
					component.set("v.totalSize",0);
					this.setHasNext(component,0,pageSize,pageSize-1);
        			this.setHasNext(component,0,pageSize,0);
				}
				
			} else if(callbackStatus == "ERROR"){
                    //alert('Error in calling server side action');
                console.log('error in data retrival');
            }
		});
		$A.enqueueAction(callCtrl);
	},
	next  : function(component,event) {

		var opptyId = component.get("v.recordId");
		var pliList = component.get("v.pliList");
		var first = component.get("v.first");
		var last = component.get("v.last");
		var pageSize = component.get("v.pageSize");
		var totalSize = component.get("v.totalSize");
		var tempPageList = [];

		first = first+pageSize;
		last = last+pageSize;

		if(last >= totalSize) {

			last = last-1;
		} 

		console.log("In next display:"+first+":"+last+":"+pageSize+":"+totalSize);
		for(var i=first; i<=last; i++) {

			if(pliList[i] != null) {
				tempPageList.push(pliList[i]);
			}
			console.log("current prod:"+pliList[i]);
		}
		component.set("v.pliPerPageList",tempPageList);
		component.set("v.first",first);
		component.set("v.last",last);

		console.log("tempPageList:"+tempPageList);

		/*var refreshList = $A.get("e.c:ParentRefresh");
        refreshList.setParams({ "opptyId": opptyId});
        refreshList.fire();*/
        this.setHasNext(component,totalSize,pageSize,last);
        this.setHasPrevious(component,totalSize,pageSize,first);
	},
	previous : function(component,event) {

		var opptyId = component.get("v.recordId");
		var pliList = component.get("v.pliList");
		var first = component.get("v.first");
		var last = component.get("v.last");
		var pageSize = component.get("v.pageSize");
		var tempPageList = [];

		var totalSize = pliList.length;

		last = last-pageSize;
		first = first-pageSize;

		if(last == first) {

			last = pageSize-1;
		}

		for(var i=first; i<=last; i++) {

			if(pliList[i] != null) {
				tempPageList.push(pliList[i]);
			}
		}
		component.set("v.pliPerPageList",tempPageList);
		component.set("v.first",first);
		component.set("v.last",last);
		console.log("In previous display:"+first+":"+last+":"+pageSize);
		/*var refreshList = $A.get("e.c:ParentRefresh");
        refreshList.setParams({ "opptyId": opptyId});
        refreshList.fire();*/
        this.setHasNext(component,totalSize,pageSize,last);
        this.setHasPrevious(component,totalSize,pageSize,first);
	},
	setHasNext : function(component,totalSize,pageSize,last) {

		console.log("#### In hasNext totalSize"+totalSize
			+"pageSize"+pageSize
			+"last"+last);
		if(totalSize > pageSize-1 && last < totalSize-1) {

			console.log("#### In hasNext"+false);
			component.set("v.hasNext",false);
		} else {

			console.log("#### In hasNext"+true);
			component.set("v.hasNext",true);
		}
	},
	setHasPrevious : function(component,totalSize,pageSize,first) {

		console.log("#### In hasPrevious"+first);
		if(first == 0) {

			component.set("v.hasPrevious",true);
		} else {

			component.set("v.hasPrevious",false);
		}
	}
})