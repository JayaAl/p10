({
	init : function(component, event, helper) {

	//Find the text value of the component with aura:id set to "address"
    var opptyId = component.get("v.recordId");
    console.log("opptyId:"+opptyId);
    //var folderId = component.get("v.folderId");
    //var folderUrl1 = component.get("v.folderurl1");
    //if(opptyId !== null && folderId !== null && folderUrl1 !== null) {
    	
    	var getOpptyDetails = component.get("c.getOpportunityRecord");
    	getOpptyDetails.setParams({"opptyId":opptyId});
		getOpptyDetails.setCallback(this,function(resp) {

		if(resp.getState() == "SUCCESS") {

			console.log("Opportunity:"+resp.getReturnValue().DAMCampaignFolderID__c);
			var damFolderId = resp.getReturnValue().DAMCampaignFolderID__c;
			
			if(damFolderId == null || damFolderId == "") {
				console.log("damFolderIDIDID");
				var damSync = component.get("c.damSync");
				console.log("damSync");
				damSync.setParams({"opptyId":opptyId});
				console.log("damSync opptyId:"+opptyId);
				damSync.setCallback(this,function(response) {
					console.log("damSync opptyId:"+response.getState());
					if(response.getState() == "SUCCESS") {
						console.log("response:"+response.getReturnValue());
						component.set("v.respStr",response.getReturnValue());
					}
				});$A.enqueueAction(damSync);
			} else {
				component.set("v.respStr","Dam folder already synced");
				
			}
		}
		});
		$A.enqueueAction(getOpptyDetails);
	/*} else {
		component.set("v.responseMsg","Already Synced.");
	}*/
	}

})