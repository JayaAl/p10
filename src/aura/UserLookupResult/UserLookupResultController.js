({
	selectUser : function(component, event, helper){
        console.log("in selectUser");
        // get the selected User from list  
        var getSelectUser = component.get("v.obj");
        console.log("User SElected:"+getSelectUser+"###Name:"+getSelectUser);
        // call the event
        var compEvent = component.getEvent("selectEvent");
        console.log("compEvent:"+compEvent);
        // set the Selected User to the event attribute.  
        compEvent.setParams({"lookupEvent" : getSelectUser});  
        console.log("compEvent:"+compEvent);
        // fire the event  
        compEvent.fire();
        console.log("event fired after user select");
    }
})