({
	searchHelper : function(component,event,getInputkeyWord,searchObj) {
	  // call the apex class method 
     var action = component.get("c.getUsers");
      // set param to method  
        action.setParams({
            'searchKey': getInputkeyWord,
            'lookupObj': searchObj
          });
      // set a callBack    
        action.setCallback(this, function(response) {
            var state = response.getState();
            if (state === "SUCCESS") {
                var storeResponse = response.getReturnValue();
              // if storeResponse size is equal 0 ,display No Result Found... message on screen.
                if (storeResponse.length == 0) {
                    component.set("v.message", 'No Result Found...');
                } else {
                    component.set("v.message", 'Search Result...');
                }
                
                // set searchResult list with return value from server.
                console.log("storeResponse:"+storeResponse);
                component.set("v.listOfSearchRecords", storeResponse);
            }
 
        });
      // enqueue the Action  
        $A.enqueueAction(action);
    
	},
  getCurrentUser : function(component,event,helper) {

  	console.log("reset user to default user");
    var getCurrentUser = component.get("c.getCurrentUser");
    getCurrentUser.setCallback(this,function(resp) {

      console.log("User Retrival on Load:"+resp.getState());
      if(resp.getState() == "SUCCESS") {

        var currentUser = resp.getReturnValue();
        console.log("reset user retrived:"+currentUser.Id);
        component.set("v.selectedRecord",currentUser);
        // call the event
        var compEvent = component.getEvent("selectEvent");
        console.log("compEvent:"+compEvent);
        // set the Selected User to the event attribute.  
        compEvent.setParams({"lookupEvent" : currentUser});  
        console.log("compEvent:"+compEvent);
        // fire the event  
        compEvent.fire();
        console.log("event fired after user select");
      } else {
        console.log("Error in retriving current user Id.");
      }
      
    });
    $A.enqueueAction(getCurrentUser);
  }
})