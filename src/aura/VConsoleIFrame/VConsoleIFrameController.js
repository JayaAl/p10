({
	doinit : function(component, event, helper) {
        //component.set("v.showPleaseSelect",true);
        var newURL = "https://customerservice.savagebeast.com/customerservice/listenerSearch.jsp?username=";
        if (component.get("v.useAlternateEmail") === true){
            newURL += component.get("v.pandoraEmail");
        } else {
            newURL += component.get("v.caseEmail");
        }
        //alert(newURL);
        component.set("v.frameSource",newURL);
        component.set("v.showPleaseSelect",false);
        component.set("v.showIFrame",true);
        component.set("v.useAlternateEmail",false);
        component.set("v.showNewTabMessage",false);
        
        var action = component.get("c.getCaseDetails");
        action.setParams({ caseId : component.get("v.recordId") });
        
        action.setCallback(this, function(response) {
            
            var state = response.getState();
            if (state === "SUCCESS"){
                var returnEmail = response.getReturnValue();
                var emailTokens = returnEmail.split(";");
                
                component.set("v.caseEmail",emailTokens[0]);
                
                //alert(emailTokens);
                if (emailTokens.length > 1){
                    component.set("v.pandoraEmail",emailTokens[1]);
                } else {
                    component.find("secondaryToggle").set("v.disabled",true);
                }
            }
        });
        $A.enqueueAction(action);
    },
    
    cbToggleChanged : function(component,event,helper) {
        if (component.get("v.useAlternateEmail") === true){
            component.set("v.useAlternateEmail",false);
        } else {
            component.set("v.useAlternateEmail",true);
        }
    },
    
    ntToggleChanged : function(component,event,helper) {
        if (component.get("v.newTabToggled") === true){
            component.set("v.newTabToggled",false);
        } else {
            component.set("v.newTabToggled",true);
        }
    },
    
    customerServiceClicked : function(component,event, helper) {
       
        
        var newURL = "https://customerservice.savagebeast.com/customerservice/listenerSearch.jsp?username=";
        if (component.get("v.useAlternateEmail") === true){
            newURL += component.get("v.pandoraEmail");
        } else {
            newURL += component.get("v.caseEmail");
        }
        //alert(newURL);
        if (component.get("v.newTabToggled") === true){
            component.set("v.showPleaseSelect",false);
            window.open(newURL,"_blank");
            component.set("v.showIFrame",false);
        	component.set("v.showNewTabMessage",true);
        } else {
            component.set("v.showPleaseSelect",false);
        	component.set("v.showNewTabMessage",false);
            component.set("v.frameSource",newURL);
            component.set("v.showIFrame",true);
        }
            
	},
 
 	charonClicked : function(component,event, helper) {
     
        var newURL = "https://charonadmin.savagebeast.com/admin/subscription/view?listenerEmail=";
        if (component.get("v.useAlternateEmail") === true){
            newURL += component.get("v.pandoraEmail");
        } else {
            newURL += component.get("v.caseEmail");
        }
        //alert(newURL);
        if (component.get("v.newTabToggled") === true){
            component.set("v.showPleaseSelect",false);
            window.open(newURL,"_blank");
            component.set("v.showIFrame",false);
        	component.set("v.showNewTabMessage",true);
        } else {
            component.set("v.showPleaseSelect",false);
        	component.set("v.showNewTabMessage",false);
            component.set("v.frameSource",newURL);
            component.set("v.showIFrame",true);
        }
    },
    
    googlePlayClicked : function(component,event, helper) {
        component.set("v.showPleaseSelect",false);
        component.set("v.showIFrame",false);
        window.open("https://play.google.com/apps/publish/?dev_acc=06848580295610995628#OrderManagementPlace","_blank");
        component.set("v.showNewTabMessage",true);
    },
    
    emailLookupClicked : function(component,event, helper) {
        component.set("v.showPleaseSelect",false);
        component.set("v.showIFrame",false);
        var newURL = "https://play.google.com/apps/publish/?dev_acc=06848580295610995628#OrderManagementPlace:search=";
        if (component.get("v.useAlternateEmail") === true){
            newURL += component.get("v.pandoraEmail");
        } else {
            newURL += component.get("v.caseEmail");
        }
        newURL += "&omsd=2008-10-22&omed=";
        
        var curDate = new Date();
        var curDay = curDate.getDate();
        var curMonth = curDate.getMonth()+1;
        var curYear = curDate.getFullYear();
        
        if(curDay<10) {
            curDay = "0" + curDay;
        } 
        if(curMonth<10) {
            curMonth = "0" + curMonth;
        } 
        
        newURL += curYear + "-" + curMonth + "-" + curDay;

        window.open(newURL,"_blank");
        component.set("v.showNewTabMessage",true);
    }
})