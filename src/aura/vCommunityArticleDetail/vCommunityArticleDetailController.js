({
    doInit : function(component, event, helper) {
        helper.loadArticle(component, event);
        helper.loadArticleTopic(component, event);
    }    
})