({
    loadArticle : function(component, event) {
        var myAction = component.get("c.getArticle");
        myAction.setParams({
            articleId : component.get("v.articleId")
        });
        myAction.setCallback(this, function(a) {
            component.set("v.articleRecord", a.getReturnValue());
            
            // Set article body
            if(a.getReturnValue() != null){
                if(a.getReturnValue().Body__c != null){
                    $("#divArticleBody").html(a.getReturnValue().Body__c);
                    $("#divArticleBody").on("click",".vArticle-item-title",function(){
                        $(".vArticle-item-desc", $(this).parent()).toggle();
                        $(this).toggleClass("vArticle-item-title-on");
                    });
                }
            }
        });
        $A.enqueueAction(myAction);
	},
    loadArticleTopic : function(component, event) {
        var myAction = component.get("c.getArticleTopic");
        myAction.setParams({
            articleId : component.get("v.articleId")
        });
        myAction.setCallback(this, function(a) {
            component.set("v.selectedTopicRecord", a.getReturnValue());
        });
        $A.enqueueAction(myAction);
	}
})