({
    loadArticle : function(component, event) {
        var myAction = component.get("c.getArticle");
        myAction.setParams({
            articleId : component.get("v.articleId")
        });
        myAction.setCallback(this, function(a) {
            component.set("v.articleRecord", a.getReturnValue());
            //$("#divArticleBody").html(a.getReturnValue().Body__c);
        });
        $A.enqueueAction(myAction);
	}
})