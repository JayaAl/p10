({
    loadSessionId : function(component, event) {
        var myAction = component.get("c.getSessionId");
        myAction.setCallback(this, function(a) {
            component.set("v.sessionId", a.getReturnValue());
        });
        $A.enqueueAction(myAction);
	},
    loadArticle : function(component, event) {
        var articleId = event.currentTarget.getAttribute('data-idValue');
        
        var myAction = component.get("c.getArticle");
        myAction.setParams({
            articleId : articleId
        });
        myAction.setCallback(this, function(a) {
            component.set("v.selectedArticleRecord", a.getReturnValue());
        });
        $A.enqueueAction(myAction);
	},
    saveCaseDeflection : function(component, event, deflected) {
        var myAction = component.get("c.saveCaseDeflection");
        myAction.setParams({"articleId" : component.get("v.selectedArticleRecord").Id,
                            "sessionId" : component.get("v.sessionId"),
                            "language" : component.get("v.language"),
                            "searchString" : component.get("v.searchString"),
                            "deflected" : deflected
        });        
        myAction.setCallback(this, function(a) {
            component.set("v.caseDeflectionId", a.getReturnValue());
            
            if(deflected)
            {
                // clear
                component.set("v.isSearchCompleted", false);
                component.set("v.selectedArticleRecord", null);
                component.set("v.articleList", null);
                component.set("v.totalArticles", 0);
                component.set("v.caseDeflectionId", null);
                component.find("searchKeyword").set("v.value", '');
            }
        });
            
        $A.enqueueAction(myAction);
    },
    updateCaseDeflection : function(component, event, caseId) {
        var myAction = component.get("c.updateCaseDeflection");
        myAction.setParams({"caseDeflectionId" : component.get("v.caseDeflectionId"),
                            "caseId" : caseId
        });
        myAction.setCallback(this, function(a) {
            
        });
            
        $A.enqueueAction(myAction);
    },
    searchArticle : function(component, event) {
        var searchString = component.get("v.searchString");
        //if(searchString != null && searchString.length >= 2)
        //{
            var myAction = component.get("c.getSearchedArticles");
            myAction.setParams({
                searchString : component.get("v.searchString"),
                language : component.get("v.language"),
                sortBy : component.get("v.articleSortBy"),
                maxNumberOfRecords : component.get("v.articleMaxNumberOfRecords")
            });
            myAction.setCallback(this, function(a) {
                component.set("v.articleList", a.getReturnValue());
                
                if(a.getReturnValue() != null)
                {
                    component.set("v.totalArticles", a.getReturnValue().length);
                }
                
                component.set("v.isSearchCompleted", true);
            });
            $A.enqueueAction(myAction);
        //}
        //else
        //{
        //    component.set("v.articleList", null);
        //    component.set("v.totalArticles", 0);
        //}
    },
    saveCase : function(component, event) {
        var validated = true;
                
        var suppliedName = "";
        if(typeof(component.find("suppliedName").get("v.value")) != "undefined")
            suppliedName = component.find("suppliedName").get("v.value");
        
        var suppliedEmail = "";
        if(typeof(component.find("suppliedEmail").get("v.value")) != "undefined")
            suppliedEmail = component.find("suppliedEmail").get("v.value");
        
        var subject = "";
        if(typeof(component.find("subject").get("v.value")) != "undefined")
            subject = component.find("subject").get("v.value");
        
        var description = "";
        if(typeof(component.find("description").get("v.value")) != "undefined")
            description = component.find("description").get("v.value");
        
        if(typeof(component.find("attachfile").get("v.value")) != "undefined")
        {
            if(component.find("attachfile").get("v.value") == "Yes")
            	component.set("v.isAttachFileRequested", true);
        }
        
        $("#pageError").hide();
        $("#suppliedNameError").hide();
        $("#suppliedEmailError").hide();
        $("#subjectError").hide();
        $("#descriptionError").hide();
        
        if(suppliedName == ""){
            validated = false;
        	$("#suppliedNameError").show();
        }
        if(suppliedEmail == ""){
            validated = false;
        	$("#suppliedEmailError").show();
        }
        if(subject == ""){
            validated = false;
        	$("#subjectError").show();
        }
        //if(description == ""){
        //    validated = false;
        //	$("#descriptionError").show();
        //}
        //
        
        if(validated){
            var myAction = component.get("c.saveContactNextBigSoundCase");
            myAction.setParams({"recordTypeId" : component.get("v.recordTypeId"),
                                "channel" : 'Next Big Sound',
                                "subject" : subject,
                                "description" : description,
                                "suppliedName" : suppliedName,
                                "suppliedEmail" : suppliedEmail,
                                "ipAddress" : component.get("v.ipAddress"),
                                "osName" : component.get("v.osName"),
                                "browser" : component.get("v.browserName")
            });
            
            myAction.setCallback(this, function(a) {
                if(a.getReturnValue() == null){
                    component.find('notifLib').showNotice({
                        "variant": "error",
                        "header": "Error",
                        "message": "Error occurred"
                    });
                }
                else if(a.getReturnValue().length != 18){
                    component.find('notifLib').showNotice({
                        "variant": "error",
                        "header": "Error",
                        "message": a.getReturnValue()
                    });
                }
                else{
                    var newCaseId = a.getReturnValue();
                    component.set("v.newCaseId", newCaseId);
                    
                    this.updateCaseDeflection(component, event, newCaseId);
                    component.set("v.isContactSupportCompleted", true);
                }
            });
            
            $A.enqueueAction(myAction);
        }
    }
})