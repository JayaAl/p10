({
    loadSessionId : function(component, event) {
        var myAction = component.get("c.getSessionId");
        myAction.setCallback(this, function(a) {
            component.set("v.sessionId", a.getReturnValue());
        });
        $A.enqueueAction(myAction);
	},
	loadReasons : function(component, event){
        var myAction = component.get("c.getCaseReasonSelectOptions");
        myAction.setCallback(this, function(a){
            component.set("v.reasonOptions", a.getReturnValue());
        });
        $A.enqueueAction(myAction);
    },
    loadSubreasons : function(component, event){
        var reason = "";
        if(typeof(component.find("reason").get("v.value")) != "undefined")
            reason = component.find("reason").get("v.value");
        
        var myAction = component.get("c.getCaseSubReasonSelectOptions");
        myAction.setParams({"reason" : reason});
        myAction.setCallback(this, function(a){
            component.set("v.subreasonOptions", a.getReturnValue());
        });
        $A.enqueueAction(myAction);
    },
	loadDevices : function(component, event){
        var myAction = component.get("c.getCaseDeviceSelectOptions");
        myAction.setCallback(this, function(a){
            component.set("v.deviceOptions", a.getReturnValue());
        });
        $A.enqueueAction(myAction);
    },
    loadArticle : function(component, event) {
        var articleId = event.currentTarget.getAttribute('data-idValue');
        
        var myAction = component.get("c.getArticle");
        myAction.setParams({
            articleId : articleId
        });
        myAction.setCallback(this, function(a) {
            component.set("v.selectedArticleRecord", a.getReturnValue());
            
            $("#divArticleBody").html(""); //reset
            
            // Set article body
            if(a.getReturnValue() != null){
                if(a.getReturnValue().Body__c != null){
                    $("#divArticleBody").html(a.getReturnValue().Body__c);
                    $("#divArticleBody").on("click",".vArticle-item-title",function(){
                        $(".vArticle-item-desc", $(this).parent()).toggle();
                        $(this).toggleClass("vArticle-item-title-on");
                    });
                }
            }
        });
        $A.enqueueAction(myAction);
	},
    saveCaseDeflection : function(component, event, deflected) {
        var myAction = component.get("c.saveCaseDeflection");
        myAction.setParams({"articleId" : component.get("v.selectedArticleRecord").Id,
                            "sessionId" : component.get("v.sessionId"),
                            "language" : component.get("v.language"),
                            "searchString" : component.get("v.searchString"),
                            "deflected" : deflected
        });        
        myAction.setCallback(this, function(a) {
            component.set("v.caseDeflectionId", a.getReturnValue());
            
            if(deflected)
            {
                // clear
                component.set("v.isSearchCompleted", false);
                component.set("v.selectedArticleRecord", null);
                component.set("v.articleList", null);
                component.set("v.totalArticles", 0);
                component.set("v.caseDeflectionId", null);
                component.find("searchKeyword").set("v.value", '');
            }
        });
            
        $A.enqueueAction(myAction);
    },
    updateCaseDeflection : function(component, event, caseId) {
        var myAction = component.get("c.updateCaseDeflection");
        myAction.setParams({"caseDeflectionId" : component.get("v.caseDeflectionId"),
                            "caseId" : caseId
        });
        myAction.setCallback(this, function(a) {
            
        });
            
        $A.enqueueAction(myAction);
    },
    searchArticle : function(component, event) {
        var searchString = component.get("v.searchString");
        if(searchString != null && searchString.length >= 2)
        {
            var myAction = component.get("c.getSearchedArticles");
            myAction.setParams({
                searchString : component.get("v.searchString"),
                language : component.get("v.language"),
                sortBy : component.get("v.articleSortBy"),
                maxNumberOfRecords : component.get("v.articleMaxNumberOfRecords")
            });
            myAction.setCallback(this, function(a) {
                component.set("v.articleList", a.getReturnValue());
                
                if(a.getReturnValue() != null)
                {
                    component.set("v.totalArticles", a.getReturnValue().length);
                }
                
                component.set("v.isSearchCompleted", true);
            });
            $A.enqueueAction(myAction);
        }
        else
        {
            component.set("v.articleList", null);
            component.set("v.totalArticles", 0);
        }
    },
    saveCase : function(component, event) {
        component.set("v.errorMessage", '');
 
        var validated = true;
                
        var suppliedName = "";
        if(typeof(component.find("suppliedName").get("v.value")) != "undefined")
            suppliedName = component.find("suppliedName").get("v.value");
        
        var suppliedEmail = "";
        if(typeof(component.find("suppliedEmail").get("v.value")) != "undefined")
            suppliedEmail = component.find("suppliedEmail").get("v.value");
        
        var confirmSuppliedEmail = "";
        if(typeof(component.find("confirmSuppliedEmail").get("v.value")) != "undefined")
            confirmSuppliedEmail = component.find("confirmSuppliedEmail").get("v.value");
        
        var reason = "";
        if(typeof(component.find("reason").get("v.value")) != "undefined")
            reason = component.find("reason").get("v.value");
        
        var subreason = "";
        if(typeof(component.find("subreason").get("v.value")) != "undefined")
            subreason = component.find("subreason").get("v.value");
        
        var pandoraAccountEmail = "";
        if(typeof(component.find("pandoraAccountEmail").get("v.value")) != "undefined")
            pandoraAccountEmail = component.find("pandoraAccountEmail").get("v.value");
        
        var device = "";
        if(typeof(component.find("device").get("v.value")) != "undefined")
            device = component.find("device").get("v.value");
        
        var subject = "";
        if(typeof(component.find("subject").get("v.value")) != "undefined")
            subject = component.find("subject").get("v.value");
        
        var description = "";
        if(typeof(component.find("description").get("v.value")) != "undefined")
            description = component.find("description").get("v.value");
        
        if(typeof(component.find("description").get("v.value")) != "undefined")
        {
            if(component.find("attachfile").get("v.value") == "Yes")
            	component.set("v.isAttachFileRequested", true);
        }
        
        $("#pageError").hide();
        $("#suppliedNameError").hide();
        $("#suppliedEmailError").hide();
        $("#confirmSuppliedEmailError").hide();
        $("#confirmSuppliedEmailNotMatchedError").hide();
        $("#pandoraAccountEmailError").hide();
        $("#subjectError").hide();
        $("#descriptionError").hide();
        $("#reasonError").hide();
        $("#subreasonError").hide();
        $("#submitError").hide();
        
        var emailformatregex = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
        
        if(suppliedName == ""){
            validated = false;
        	$("#suppliedNameError").show();
            $("#suppliedNameError").html($A.get("$Label.c.vCommunitySupport_Contact_Thisissrequiredfield"));
            $("#submitError").show();
        }
        else if(!(/[a-zA-Z0-9]+\s+[a-zA-Z0-9]+/g.test(suppliedName))){
            validated = false;
        	$("#suppliedNameError").show();
            $("#suppliedNameError").html($A.get("$Label.c.vCommunitySupport_Contact_InvalidFullname"));
            $("#submitError").show();
        }
        if(suppliedEmail == ""){
            validated = false;
        	$("#suppliedEmailError").show();
            $("#suppliedEmailError").html($A.get("$Label.c.vCommunitySupport_Contact_Thisissrequiredfield"));
            $("#submitError").show();
        }
        else if(!(emailformatregex.test(String(suppliedEmail).toLowerCase()))){
            validated = false;
        	$("#suppliedEmailError").show();
            $("#suppliedEmailError").html($A.get("$Label.c.vCommunitySupport_Contact_InvalidEmail"));
            $("#submitError").show();
        }
        if(confirmSuppliedEmail == ""){
            validated = false;
        	$("#confirmSuppliedEmailError").show();
            $("#submitError").show();
        }
        else if(!(emailformatregex.test(String(confirmSuppliedEmail).toLowerCase()))){
            validated = false;
        	$("#confirmSuppliedEmailError").show();
            $("#confirmSuppliedEmailError").html($A.get("$Label.c.vCommunitySupport_Contact_InvalidEmail"));
            $("#submitError").show();
        }
        if(suppliedEmail != "" && confirmSuppliedEmail != ""){
            if(suppliedEmail != confirmSuppliedEmail){
                validated = false;
        		$("#confirmSuppliedEmailNotMatchedError").show();
                $("#submitError").show();
            }
        }
        if(pandoraAccountEmail != ""){
            if(!(emailformatregex.test(String(pandoraAccountEmail).toLowerCase()))){
                validated = false;
                $("#pandoraAccountEmailError").show();
                $("#pandoraAccountEmailError").html($A.get("$Label.c.vCommunitySupport_Contact_InvalidEmail"));
                $("#submitError").show();
            }
        }
        if(subject == ""){
            validated = false;
        	$("#subjectError").show();
            $("#submitError").show();
        }
        if(description == ""){
            validated = false;
        	$("#descriptionError").show();
            $("#submitError").show();
        }
        if(reason == ""){
            validated = false;
        	$("#reasonError").show();
            $("#submitError").show();
        }
        if(subreason == "" && (component.get("v.subreasonOptions").length > 0)){
            validated = false;
        	$("#subreasonError").show();
            $("#submitError").show();
        }
        
        if(validated){
            var myAction = component.get("c.saveContactSupportCase");
            myAction.setParams({"recordTypeId" : component.get("v.recordTypeId"),
                                "channel" : 'Support',
                                "subject" : subject,
                                "description" : description,
                                "suppliedName" : suppliedName,
                                "suppliedEmail" : suppliedEmail,
                                "pandoraAccountEmail" : pandoraAccountEmail,
                                "reason" : reason,
                                "subreason" : subreason,
                                "device" : device,
                                "ipAddress" : component.get("v.ipAddress"),
                                "osName" : component.get("v.osName"),
                                "browser" : component.get("v.browserName")
            });
            
            myAction.setCallback(this, function(a) {
                if(a.getReturnValue() == null){
                    component.find('notifLib').showNotice({
                        "variant": "error",
                        "header": "Error",
                        "message": $A.get("$Label.c.vCommunitySupport_Contact_ErrorMessage")
                    });
                }
                else if(a.getReturnValue().length != 18){
                    console.log(a.getReturnValue());
                    
                    component.find('notifLib').showNotice({
                        "variant": "error",
                        "header": "Error",
                        "message": $A.get("$Label.c.vCommunitySupport_Contact_ErrorMessage")
                    });
                }
                else{
                    var newCaseId = a.getReturnValue();
                    component.set("v.newCaseId", newCaseId);
                    
                    this.updateCaseDeflection(component, event, newCaseId);
                    component.set("v.isContactSupportCompleted", true);
                }
            });
            
            $A.enqueueAction(myAction);
        }
    }
})