({
    doInit : function(component, event, helper) {
		helper.loadTopicList(component, event);
    },
    doAfterScriptsLoaded : function(component, event, helper) {
        helper.setupMenu(component, event);
    },
    toggleNavMenu : function(component, event, helper) {
        var cmpLogo = component.find("divLogo");
        $A.util.toggleClass(cmpLogo, "vFeaturedArticleNav-on");
        
        var cmpNav = component.find("divNav");
        $A.util.toggleClass(cmpNav, "vFeaturedArticleNav-on");
        
        var cmpOverlay = component.find("divOverlay");
        $A.util.toggleClass(cmpOverlay, "vFeaturedArticleNav-on");
        
        var cmpNavToggle = component.find("divNavToggle");
        $A.util.toggleClass(cmpNavToggle, "vFeaturedArticleNav-hamburger-on");
    },
    toggleTopicArticleMenu : function(component, event, helper){
        $A.util.toggleClass(event.target, "menutopicexpandfa");
        
        var dataId = event.target.getAttribute("data-idValue");
        $("."+dataId+'_submenu').toggleClass("menuhiddenfa");
    }
})