({
    loadTopicList : function(component, event) {
        var myAction = component.get("c.getNavigationalTopics");
        myAction.setParams({
            language : component.get("v.language")
        });
        myAction.setCallback(this, function(a) {
            component.set("v.topicList", a.getReturnValue());
        });
        $A.enqueueAction(myAction);
	},
    setupMenu : function(component, event){
        $A.util.removeClass(component.find("menuHome"), "menuhiddenfa");
        $A.util.removeClass(component.find("menuNextBigSound"), "menuhiddenfa");
        $A.util.removeClass(component.find("menuHomeIcon"), "menuactivefa");
        $A.util.removeClass(component.find("menuHomeIcon"), "menubackfa");
        $A.util.removeClass(component.find("menuHomeText"), "menuactivetextfa");
        $A.util.removeClass(component.find("menuContactIcon"), "menuactivefa");
        $A.util.removeClass(component.find("menuContactText"), "menuactivetextfa");
        $A.util.removeClass(component.find("menuSelectedTopicIcon"), "menuactivefa");
        $A.util.removeClass(component.find("menuSelectedTopicIcon"), "menubackfa");
        $A.util.removeClass(component.find("menuSelectedTopicText"), "menuactivetextfa");
        $("li.topiclistitemfa").removeClass("menuhiddenfa");
        $("li.topiclistsubitemfa").addClass("menuhiddenfa");
        $(".menutopicfa").removeClass("menutopicexpandfa");
        $(".menutopicfa").removeClass("menuactivefa");
        $(".menutopicfa").removeClass("menubackfa");
        $(".menutopictextfa").removeClass("menuactivetextfa");
        
        $A.util.addClass(component.find("menuHomeIcon"), "menuactivefa");
        $A.util.addClass(component.find("menuHomeText"), "menuactivetextfa");
    }
})