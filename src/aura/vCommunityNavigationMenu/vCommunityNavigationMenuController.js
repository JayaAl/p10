({
    doInit : function(component, event, helper) {
        helper.loadUrlVariables(component, event);
        helper.loadTopicList(component, event);
    },
    doHandleRouteChange : function(component, event, helper) {
        // Reset
        component.set("v.selectedTopicRecord", null);
        component.set("v.selectedArticleRecord", null);
        component.set("v.isTopicBackMenuClicked", false);
        
        helper.loadUrlVariables(component, event);

        var selectedTopicId = component.get("v.selectedTopicId");
        var articleUrlName = component.get("v.articleUrlName");
        var activeMenu = component.get("v.activeMenu");
        
        if(selectedTopicId != null && selectedTopicId != ''){
            helper.loadTopic(component, event); 
        }
        else if(articleUrlName != null && articleUrlName != ''){
            helper.loadArticleTopic(component, event, helper);
        }
        else{
            helper.setupMenu(component, event);
        }
    },
    doRender : function(component, event, helper) {
        
    },
    doAfterScriptsLoaded : function(component, event, helper) {
        component.set("v.isScriptsLoaded", true);
    },
    displayAllTopicMenu : function(component, event, helper){
        component.set("v.isTopicBackMenuClicked", true);
        helper.loadTopicList(component, event);
    },
    toggleNavMenu : function(component, event, helper) {
        var cmpLogo = component.find("divLogo");
        $A.util.toggleClass(cmpLogo, "vNav-on");
        
        var cmpNav = component.find("divNav");
        $A.util.toggleClass(cmpNav, "vNav-on");
        
        var cmpOverlay = component.find("divOverlay");
        $A.util.toggleClass(cmpOverlay, "vNav-on");
        
        var cmpNavToggle = component.find("divNavToggle");
        $A.util.toggleClass(cmpNavToggle, "vNav-hamburger-on");
    },
    toggleTopicArticleMenu : function(component, event, helper){
        var articleUrlName = component.get("v.articleUrlName");
        var isTopicBackMenuClicked = component.get("v.isTopicBackMenuClicked");
        
        if(!isTopicBackMenuClicked && articleUrlName != '') // display all topic menu
        {
            component.set("v.isTopicBackMenuClicked", true);
            helper.setupMenu(component, event);
            console.log(articleUrlName);
        }
        else // toggle topic articles
        {
            $A.util.toggleClass(event.target, "menutopicexpand");
        
            var dataId = event.target.getAttribute("data-idValue");
            $("."+dataId+'_submenu').toggleClass("menuhidden");
        }
    }
})