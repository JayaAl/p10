({
    loadTopic : function(component, event) {
        var myAction = component.get("c.getTopic");
        myAction.setParams({
            topicId : component.get("v.selectedTopicId")
        });
        myAction.setCallback(this, function(a) {
            component.set("v.selectedTopicRecord", a.getReturnValue());
            this.setupMenu(component, event);
        });
        $A.enqueueAction(myAction);
    },
    loadArticleTopic : function(component, event, helper) {
        var myAction = component.get("c.getArticleTopicByUrlName");
        myAction.setParams({
            urlName : component.get("v.articleUrlName")
        });
        myAction.setCallback(this, function(a) {
            component.set("v.selectedTopicRecord", a.getReturnValue());
            this.setupMenu(component, event);
        });
        $A.enqueueAction(myAction);
    },
    loadArticle : function(component, event) {
        var myAction = component.get("c.getArticleByUrlName");
        myAction.setParams({
            urlName : component.get("v.articleUrlName")
        });
        myAction.setCallback(this, function(a) {
            component.set("v.selectedArticleRecord", a.getReturnValue());
        });
        $A.enqueueAction(myAction);
    },
    loadTopicList : function(component, event) {
        var myAction = component.get("c.getNavigationalTopics");
        myAction.setParams({
            language : component.get("v.language")
        });
        myAction.setCallback(this, function(a) {
            component.set("v.topicList", a.getReturnValue());
        });
        $A.enqueueAction(myAction);
    },
    loadUrlVariables : function(component, event){
        var pageURL = decodeURIComponent(window.location.pathname);
        var urlVars = pageURL.split('/');
        
        component.set("v.activeMenu", '');
        component.set("v.selectedTopicId", '');
        component.set("v.articleUrlName", '');
        
        // Set active menu
        if(urlVars[urlVars.length - 1] == ''){ //home
            component.set("v.activeMenu", "Home");
        }
        else if(urlVars[urlVars.length - 1] == 'contactsupport'){ //contact
            component.set("v.activeMenu", "Contact");
        }
                
        // Get topic id
        if(pageURL.includes('/topic/')){
            if(urlVars[urlVars.length - 1] != 'topic'){
                var topicId = '';
                
                if(urlVars[urlVars.length - 1].startsWith('0TO')){
                    topicId = urlVars[urlVars.length - 1];
                }
                else if(urlVars[urlVars.length - 2].startsWith('0TO')){
                    topicId = urlVars[urlVars.length - 2];
                }
                
                component.set("v.selectedTopicId", topicId);
            }
        }
        
        // Get article url name
        if(pageURL.includes('/article/')){
            if(urlVars[urlVars.length - 1] != 'article'){
                component.set("v.articleUrlName", urlVars[urlVars.length - 1]);
            }
        }
        
        //console.log('Active Menu: ' + component.get("v.activeMenu"));
        //console.log('Topic Id: ' + component.get("v.selectedTopicId"));
        //console.log('Article Url Name: ' + component.get("v.articleUrlName"));
    },
    setupMenu : function(component, event){
        var scriptsHasLoaded = component.get("v.isScriptsLoaded"); // Need to check if jquery has loaded
        
        if(scriptsHasLoaded)
        {
            
        var isTopicBackMenuClicked = component.get("v.isTopicBackMenuClicked");
        var selectedTopicRecord = component.get("v.selectedTopicRecord");
        var articleUrlName = component.get("v.articleUrlName");
        var activeMenu = component.get("v.activeMenu");
        
        $A.util.removeClass(component.find("menuHome"), "menuhidden");
        $A.util.removeClass(component.find("menuNextBigSound"), "menuhidden");
        $A.util.removeClass(component.find("menuHomeIcon"), "menuactive");
        $A.util.removeClass(component.find("menuHomeIcon"), "menuback");
        $A.util.removeClass(component.find("menuHomeText"), "menuactivetext");
        $A.util.removeClass(component.find("menuContactIcon"), "menuactive");
        $A.util.removeClass(component.find("menuContactText"), "menuactivetext");
        $A.util.removeClass(component.find("menuSelectedTopicIcon"), "menuactive");
        $A.util.removeClass(component.find("menuSelectedTopicIcon"), "menuback");
        $A.util.removeClass(component.find("menuSelectedTopicText"), "menuactivetext");
        $("li.topiclistitem").removeClass("menuhidden");
        $("li.topiclistsubitem").addClass("menuhidden");
        $(".menutopic").removeClass("menutopicexpand");
        $(".menutopic").removeClass("menuactive");
        $(".menutopic").removeClass("menuback");
        $(".menutopictext").removeClass("menuactivetext");
        
        if(!isTopicBackMenuClicked)
        {   
            if(selectedTopicRecord != null)
            {
                component.set("v.showAllTopicMenu", false);  
                
                $("li.topiclistsubitem").addClass("menuhidden");
                $("li.topiclistitem").not(".li_topic_"+selectedTopicRecord.topicId).addClass("menuhidden");
                $(".topic_"+selectedTopicRecord.topicId+"_submenu").removeClass("menuhidden");
                
                if(articleUrlName == "")
                {
                    $A.util.addClass(component.find("menuHomeIcon"), "menuback");
                    $(".topic_"+selectedTopicRecord.topicId).addClass("menuactive");
                    $(".topic_"+selectedTopicRecord.topicId+"_text").addClass("menuactivetext");
                    
                    console.log($(".topic_"+selectedTopicRecord.topicId));
                    console.log($(".topic_"+selectedTopicRecord.topicId+"_text"));
                }
                else
                {
                    $(".topic_"+selectedTopicRecord.topicId).addClass("menuback");
                    $A.util.addClass(component.find("menuHome"), "menuhidden");
                    $A.util.addClass(component.find("menuSelectedTopicIcon"), "menuback");
                }
                    
                $A.util.addClass(component.find("menuNextBigSound"), "menuhidden");
            }
            else
            {
                component.set("v.showAllTopicMenu", true);  
            }
        }
        else
        {
            component.set("v.showAllTopicMenu", true);
            $("li.topiclistitem").removeClass("menuhidden");
            
            if(activeMenu != "Home")
                $A.util.addClass(component.find("menuHomeIcon"), "menuback");
            
            if(selectedTopicRecord != null)
            {
                $(".topic_"+selectedTopicRecord.topicId).addClass("menutopicexpand");
                $(".topic_"+selectedTopicRecord.topicId+"_submenu").removeClass("menuhidden");
            }
        }
        
        if(activeMenu == "Home")
        {
            $A.util.addClass(component.find("menuHomeIcon"), "menuactive");
            $A.util.addClass(component.find("menuHomeText"), "menuactivetext");
        }
        else if(activeMenu == "Contact")
        {
            $A.util.addClass(component.find("menuContactIcon"), "menuactive");
            $A.util.addClass(component.find("menuContactText"), "menuactivetext");
        }
        }
    }
})