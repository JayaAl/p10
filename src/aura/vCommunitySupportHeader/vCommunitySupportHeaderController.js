({
    search : function(component, event, helper) {
        
        
        var searchString;

        // get browser
        var browser = navigator.appName;
        var nAgt = navigator.userAgent;
        var verOffset;
        // Opera
        if ((verOffset = nAgt.indexOf('Opera')) != -1) {
            browser = 'Opera';
        }
        // Opera Next
        if ((verOffset = nAgt.indexOf('OPR')) != -1) {
            browser = 'Opera';
        }
        // Edge
        else if ((verOffset = nAgt.indexOf('Edge')) != -1) {
            browser = 'Microsoft Edge';
        }
        // MSIE
        else if ((verOffset = nAgt.indexOf('MSIE')) != -1) {
            browser = 'MSIE';
        }
        // Chrome
        else if ((verOffset = nAgt.indexOf('Chrome')) != -1) {
            browser = 'Chrome';
        }
        // Safari
        else if ((verOffset = nAgt.indexOf('Safari')) != -1) {
            browser = 'Safari';
        }
        // Firefox
        else if ((verOffset = nAgt.indexOf('Firefox')) != -1) {
            browser = 'Firefox';
        }
        // MSIE 11+
        else if (nAgt.indexOf('Trident/') != -1) {
            browser = 'MSIE';
        }
        // Other browsers
        else if ((nameOffset = nAgt.lastIndexOf(' ') + 1) < (verOffset = nAgt.lastIndexOf('/'))) {
            browser = nAgt.substring(nameOffset, verOffset);
        }

        if(event.getParams().keyCode === 13)
        {
             searchString = component.find("searchKeyword").get("v.value");
            console.log("searchString:"+searchString+" Browser:"+browser);
            var ieStrLength;

            if(!searchString && browser == "MSIE") {
                searchString = component.find("searchKeyword").getElement();
                ieStrLength = searchString.value.length;
            }
            //searchString = "test str length";
            //console.log("searchstr length:"+searchString.toString().length);
            //var searchString = component.find("searchKeyword").get("v.value");
            if(searchString != null && ((searchString.length >= 2) 
                            || (browser == "MSIE" && searchString.value.length >= 2)))
            {
                if(browser == "MSIE") {
                    var url = "/support/s/global-search/"+searchString.value;
                    console.log("url:"+url);
                    window.open(url,"_self");
                } else {

                    var urlEvent = $A.get("e.force:navigateToURL");
                    urlEvent.setParams({
                      "url": "/global-search/"+searchString
                    });
                    console.log("b4 firing url event");
                    urlEvent.fire();
                }
                var analyticsInteraction = $A.get("e.forceCommunity:analyticsInteraction");
                analyticsInteraction.setParams({
                        hitType : 'event',
                        eventCategory : 'Button',
                        eventAction : 'click',
                        eventLabel : 'Support Search',
                        eventValue: 200
                });
                analyticsInteraction.fire();
            } 
        }    
    }
})