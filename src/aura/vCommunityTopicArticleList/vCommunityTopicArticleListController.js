({
    doInit : function(component, event, helper) {
        helper.loadTopic(component, event);
        helper.loadArticleList(component, event);
    }
})