({
    loadTopic : function(component, event) {
        var myAction = component.get("c.getTopic");
        myAction.setParams({
            topicId : component.get("v.topicId")
        });
        myAction.setCallback(this, function(a) {
            component.set("v.topicRecord", a.getReturnValue());
        });
        $A.enqueueAction(myAction);
	},
    loadArticleList : function(component, event) {
        var myAction = component.get("c.getTopicArticles");
        myAction.setParams({
            topicId : component.get("v.topicId"),
            language : component.get("v.language"),
            sortBy : component.get("v.sortBy")
        });
        myAction.setCallback(this, function(a) {
            component.set("v.articleList", a.getReturnValue());
            
            if(a.getReturnValue() != null)
            {
                component.set("v.totalArticles", a.getReturnValue().length);
                
                if(a.getReturnValue().length > 0)
                	component.set("v.hasArticle", true);
            }
        });
        $A.enqueueAction(myAction);
	}
})