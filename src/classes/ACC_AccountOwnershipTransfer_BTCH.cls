/*
 * @author: Lakshman(sfdcace@gmail.com)
 * @desc: This class is used to update last 6 months revenu of closed won opportunities on Account.
 * @date: 16-11-2015
 * @reference: ACC_AccountOwnershipTransfer_SCH.cls
 */
global class ACC_AccountOwnershipTransfer_BTCH implements Database.Batchable<sObject>{
    String query; //we will set this in the constructor so that we can change the query in future without deleting the schedule job
    global ACC_AccountOwnershipTransfer_BTCH() { 
        // Query all Accounts, and all Opportunities associated to those Accounts through either the std relationship (AccountId), or the Opportunities1__r relationship (Agency__c)
        query =  ' Select Id, Last_Close_Date_W__c, Opportunity_Amount_6M__c, ';
        query += ' (Select Amount from Opportunities where StageName = \'Closed Won\' AND Owner.Territory__c = \'Inside Sales\' AND CloseDate >= LAST_N_MONTHS:5 AND Amount!= null), ';
        query += ' (Select Amount from Opportunities1__r where StageName = \'Closed Won\' AND Owner.Territory__c = \'Inside Sales\' AND CloseDate >= LAST_N_MONTHS:5 AND Amount!= null) ';
        query += ' from Account ';
    }
    global Database.QueryLocator start(Database.BatchableContext BC){
        return Database.getQueryLocator(query);
    }
    
    global void execute(Database.BatchableContext BC, List<sObject> scope){
        List<Account> accountsToUpdate = new List<Account>();
        for(sobject s : scope){
            Account objAccount = (Account)s;
            Account acc = new Account(Id = objAccount.Id);
            Decimal revenueSum = 0.0;
            if(! objAccount.Opportunities.isEmpty()) { // Loop through the Opportunities related records
                for(Opportunity opp: objAccount.Opportunities) {
                    revenueSum += opp.Amount;
                }
            }
            if(! objAccount.Opportunities.isEmpty()) { // Loop through the Opportunities1__r related records (Agency__c lookup on the Opportunity)
                for(Opportunity opp: objAccount.Opportunities1__r) {
                    revenueSum += opp.Amount;
                }
            }
            if(acc.Opportunity_Amount_6M__c != revenueSum){ // Only add those Accounts that need to be added to the accountsToUpdate collection
                acc.Opportunity_Amount_6M__c = revenueSum;
                accountsToUpdate.add(acc);
            }
        }
        
        if(!accountsToUpdate.isEmpty()) {
            //we dont want to run DMA_UpdateOrAddDMATrigger so lets set the helper flag to false
            ACC_Static_Helper.runDMATrigger = false;
            Database.update( accountsToUpdate, false );
        }
    }
    
    global void finish(Database.BatchableContext BC){
    }
}