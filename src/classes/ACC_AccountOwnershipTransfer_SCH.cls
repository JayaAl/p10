global class ACC_AccountOwnershipTransfer_SCH implements Schedulable {
    global void execute(SchedulableContext SC) {
        ACC_AccountOwnershipTransfer_BTCH aot = new ACC_AccountOwnershipTransfer_BTCH(); 
        database.executebatch(aot);

    }
}