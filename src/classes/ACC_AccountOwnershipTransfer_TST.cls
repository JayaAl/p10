@isTest(seealldata=false)
public class ACC_AccountOwnershipTransfer_TST {
    public static String CRON_EXP = '0 0 0 15 3 ? 2022';
    static testMethod void myTestSCHBTCH() {
        Account testAccount = UTIL_TestUtil.generateAccount();
        testAccount.Name = 'Opp 6m Rollup Test';
        testAccount.Type = 'Advertiser';
        insert testAccount;

         //Insert Opportunity
        Opportunity testOpp = UTIL_TestUtil.generateOpportunity(testAccount.Id);
        testOpp = UTIL_TestUtil.closeWinOpportunity(testOpp);
        insert testOpp;
        
        Test.startTest();
            // Schedule the test job
            String jobId = System.schedule('ScheduleApexClassTest',
                            CRON_EXP, 
                            new ACC_AccountOwnershipTransfer_SCH());
        Test.stopTest();
    }
}