@isTest
public class ACC_ActivityHistoryRelatedListCntrlTest {

    static testMethod void testControllerLoad(){
        dataPrep();
        Test.startTest();
        	ApexPages.currentPage().getParameters().put('id', testAgency.Id);
        	ACC_ActivityHistoryRelatedListController testEXT = new ACC_ActivityHistoryRelatedListController(new ApexPages.StandardController(testAccount));
        Test.stopTest();
    }
    
    static testMethod void testNext(){
        dataPrep();
        Test.startTest();
        	ApexPages.currentPage().getParameters().put('id', testAgency.Id);
        	ACC_ActivityHistoryRelatedListController testEXT = new ACC_ActivityHistoryRelatedListController(new ApexPages.StandardController(testAccount));
            testEXT.next();
        Test.stopTest();
    }
    static testMethod void testPrevious(){
        dataPrep();
        Test.startTest();
        	ApexPages.currentPage().getParameters().put('id', testAgency.Id);
        	ACC_ActivityHistoryRelatedListController testEXT = new ACC_ActivityHistoryRelatedListController(new ApexPages.StandardController(testAccount));
            // testEXT.previous();
        Test.stopTest();
    }
    static testMethod void testLast(){
        dataPrep();
        Test.startTest();
        	ApexPages.currentPage().getParameters().put('id', testAgency.Id);
        	ACC_ActivityHistoryRelatedListController testEXT = new ACC_ActivityHistoryRelatedListController(new ApexPages.StandardController(testAccount));
             //testEXT.last();
        Test.stopTest();
    }
    static testMethod void testBeginning(){
        dataPrep();
        Test.startTest();
        	ApexPages.currentPage().getParameters().put('id', testAgency.Id);
        	ACC_ActivityHistoryRelatedListController testEXT = new ACC_ActivityHistoryRelatedListController(new ApexPages.StandardController(testAccount));
            testEXT.beginning();
        Test.stopTest();
    }
    static testMethod void testGetDisableNext(){
        dataPrep();
        Test.startTest();
        	ApexPages.currentPage().getParameters().put('id', testAgency.Id);
        	ACC_ActivityHistoryRelatedListController testEXT = new ACC_ActivityHistoryRelatedListController(new ApexPages.StandardController(testAccount));
            testEXT.getDisableNext();
        Test.stopTest();
    }
    static testMethod void testGetDisablePrevious(){
        dataPrep();
        Test.startTest();
        	ApexPages.currentPage().getParameters().put('id', testAgency.Id);
        	ACC_ActivityHistoryRelatedListController testEXT = new ACC_ActivityHistoryRelatedListController(new ApexPages.StandardController(testAccount));
            testEXT.getDisablePrevious();
        Test.stopTest();
    }
    
    
    
    private static Account testAccount;
    private static Account testAgency;
    private static Opportunity testOpp;
    private static List<Task> tasks;
    
    private static void dataPrep(){
        //Insert Account
        testAccount = UTIL_TestUtil.generateAccount();
        testAccount.Name = 'Advertiser999Test';
        testAccount.Type = 'Advertiser';
        
        //Insert Agency
        testAgency = UTIL_TestUtil.generateAccount();
        testAgency.Name = 'Agency54321Test';
        testAgency.Type = 'Ad Agency';
        
        Insert new List<Account>{testAccount, testAgency};
        
        //Insert Opportunity
        testOpp = UTIL_TestUtil.generateOpportunity(testAccount.Id);
        testOpp.Agency__c = testAgency.Id;
        testOpp.confirm_direct_relationship__c = false;
        insert testOpp;
        
        tasks = new List<Task>();
        //Add Task for Account
        tasks.add(UTIL_TestUtil.generateCompletedTask(testAccount));
        //Add task for Agency
        tasks.add(UTIL_TestUtil.generateCompletedTask(testAgency));
        
        //Add task for Opportunity about 10
        for(Integer idx =0; idx < 501; idx ++ ) {
            tasks.add(UTIL_TestUtil.generateCompletedTask(testOpp));
        }
        insert tasks;
    }
}