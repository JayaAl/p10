/**
* @name: ACC_ActivityHistoryRelatedListController 
* @desc: View All Activities page on Account to show Activities from Both Agency and Account prospective for an Opportunity
        * Added pagination(ESS-22724)
* @author: Lakshman (sfdcace@gmail.com)
* @date: 01-08-2014
**/
public class ACC_ActivityHistoryRelatedListController {
       
    public List<ActivitiesWrapper> listAW {get;set;}
    public Boolean hasMoreThan1K {get;set;}
    /*Pagination variables*/
    
    //List to show the limited records on the page
    public List<ActivitiesWrapper> activitiesToShow{get;set;}
    
    //Navigation variables
    Integer counter = 0;//TO track the number of records parsed
    public String limitSize {get;set;}//Number of records to be displayed
    Integer totalSize =0; //To Store the total number of records available
    
    public ACC_ActivityHistoryRelatedListController(ApexPages.StandardController controller) {

        Account account  = [Select Type, Id from Account where Id =: ApexPages.currentPage().getParameters().get('id')];//get Account Id
        listAW = new List<ActivitiesWrapper>();
        limitSize = '10';
        
        Set<Id> setParentIds = new Set<Id>();
        List<ActivityHistory> listAH = new List<ActivityHistory>();
        setParentIds.add(account.Id); // Always add the current Account's Id
        
        if(account.Type == 'Ad Agency'){ //Query Opps with the Account as Agency
            for(Opportunity opp : [Select Id from Opportunity where Agency__c =: account.Id ]) {
                setParentIds.add(opp.Id);
            }
        }
        
        // Query Account related Activity History 
        List <Account> accountTemp = [
            SELECT Id, Name, (
                SELECT Subject, Id, WhatId, What.Name, WhoId, Who.Name, ActivityDate, IsTask,
                OwnerId, Owner.Name, LastModifiedDate 
                FROM ActivityHistories
                WHERE LastModifiedDate >= LAST_N_MONTHS:12
                ORDER BY LastModifiedDate DESC
            ) 
            FROM Account 
            WHERE Id IN : setParentIds
        ];
        for(Account a : accountTemp){
            for(ActivityHistory ah:a.ActivityHistories){
                listAH.add(ah);
            }
        }

        // Query Opportunity related Activity History
        List<Opportunity> oppTemp = [
            SELECT Id, Name, (
                SELECT Subject, Id, WhatId, What.Name, WhoId, Who.Name, ActivityDate, IsTask,
                OwnerId, Owner.Name, LastModifiedDate 
                FROM ActivityHistories 
                WHERE LastModifiedDate >= LAST_N_MONTHS:12 
                ORDER BY LastModifiedDate DESC
            )
            FROM Opportunity 
            WHERE Id IN : setParentIds
        ];
        for(Opportunity o : oppTemp){
            for(ActivityHistory ah:o.ActivityHistories){
                listAH.add(ah);
            }
        }
        
        for(ActivityHistory ah:listAH){
            listAW.add(new ActivitiesWrapper(ah, ah.What.Name, ah.Who.Name));
        }
        
        if(!listAW.isEmpty()) {
            listAW.sort(); //call custom sort to sort by Last Modified Date
        }
        hasMoreThan1K = false;
        
        activitiesToShow = new List<ActivitiesWrapper>();
        doPagination ();
    }
    
    //Navigation methods
    
    public void doPagination () {
        // set pagination variables
        counter = 0;
        totalSize = listAW.size();
        activitiesToShow.clear();
        //Intial adding of Activities to activitiesToShow
        //check the total records are more than Integer.valueOf(limitSize) and assign the records
        if((counter+Integer.valueOf(limitSize)) <= totalSize){
            for(Integer i=0;i<Integer.valueOf(limitSize);i++){
                activitiesToShow.add(listAW.get(i));
            }
        } else{
            for(Integer i=0;i<totalSize;i++){
                activitiesToShow.add(listAW.get(i));
            }
        }
        system.debug(logginglevel.info,'THIS IS THE ISSUE' + activitiesToShow.SIZE());
    }
    public void beginning(){
        activitiesToShow.clear();
        counter=0;
        if((counter + Integer.valueOf(limitSize)) <= totalSize){
            for(Integer i=0;i<Integer.valueOf(limitSize);i++){
                activitiesToShow.add(listAW.get(i));
            }
        } else{
            for(Integer i=0;i<totalSize;i++){
                activitiesToShow.add(listAW.get(i));
            }
        }
        
    }
    
    public void next(){
        activitiesToShow.clear();
        counter=counter+Integer.valueOf(limitSize);
        if((counter+Integer.valueOf(limitSize)) <= totalSize){
            for(Integer i=counter-1;i<(counter+Integer.valueOf(limitSize));i++){
                activitiesToShow.add(listAW.get(i));
            }
        } else{
            for(Integer i=counter;i<totalSize;i++){
                activitiesToShow.add(listAW.get(i));
            }
        }
    }
    
    public void previous(){
        activitiesToShow.clear();
        counter=counter-Integer.valueOf(limitSize);       
        for(Integer i=counter;i<(counter+Integer.valueOf(limitSize)); i++){
            activitiesToShow.add(listAW.get(i));
        }
    }
    
    public void last (){
        activitiesToShow.clear();
        if(math.mod(totalSize , Integer.valueOf(limitSize)) == 0){
            counter = Integer.valueOf(limitSize) * ((totalSize/Integer.valueOf(limitSize))-1);
        } else if (math.mod(totalSize , Integer.valueOf(limitSize)) != 0){
            counter = Integer.valueOf(limitSize) * ((totalSize/Integer.valueOf(limitSize)));
        }
        
        for(Integer i=counter-1;i<totalSize-1;i++){
            activitiesToShow.add(listAW.get(i));
        }
        
    }
    
    public Boolean getDisableNext(){
        
        if((counter + Integer.valueOf(limitSize)) >= totalSize )
            return true ;
        else
            return false ;
    }
    
    public Boolean getDisablePrevious(){
        
        if(counter == 0)
            return true ;
        else
            return false ;
    } 
    
    
    
}