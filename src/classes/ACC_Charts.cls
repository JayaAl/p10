/**
 * @name: ACC_Charts
 * @desc: Used to display following VF reports inline on Account Layout:
          1. Closed Revenue for Current Year By Month (Bar Chart) Closed won
          2. Pipeline Revenue For Current Year By Month (Bar Chart) Not closed/won or closed/lost
          3. Closed Revenue for Year by Budget Source (Pie Chart)
          4. Closed Revenue For Year by Industry category (Pie Chart)

 * @author: Lakshman(sfdcace@gmail.com)
 */

public class ACC_Charts {
    public String accountId = ApexPages.currentPage().getParameters().get('id');
    public AggregateResult[] result;
    
    
    
    
    
    public ACC_Charts(ApexPages.standardController controller){
        result = [SELECT SUM(Amount__c) monthlyRev, COUNT_DISTINCT(Opportunity_Split__r.Opportunity__c) noOfClosedWonOpps,
                         FISCAL_MONTH(Date__c) month, Opportunity_Split__r.Opportunity__r.AccountId accId,
                         Opportunity_Split__r.Opportunity__r.Agency__c agencyId, 
                         Opportunity_Split__r.Opportunity__r.StageName oppStageName, 
                         CALENDAR_YEAR(Date__c) year
                    FROM Split_Detail__c 
                GROUP BY FISCAL_MONTH(Date__c),Opportunity_Split__r.Opportunity__r.Agency__c,
                     Opportunity_Split__r.Opportunity__r.AccountId,
                      Opportunity_Split__r.Opportunity__r.StageName,
                     Date__c
                HAVING (Opportunity_Split__r.Opportunity__r.AccountId =: accountId OR Opportunity_Split__r.Opportunity__r.Agency__c =: accountId) AND 
                       Date__c = THIS_FISCAL_YEAR];
    }
    
    //Closed Revenue for Current Year By Month (Bar Chart) Closed won
    public List<OpportunityData> getOppDataForClosedWon() {
        
    
        List<OpportunityData> oppsData = new List<OpportunityData>();
        Integer saveIdx = 0;
        for (AggregateResult a : result)
        {
            Datetime d=Datetime.newInstance((Integer)a.get('year'),(Integer)a.get('month'), 1);
            if(a.get('oppStageName') == 'Closed Won') {
                OpportunityData opp ;
                if(oppsData.size() != 0){
                    if(oppsData.get(saveIdx-1).month == d.format('MMM')){
                        oppsData.get(saveIdx-1).countOpps = (Integer)a.get('noOfClosedWonOpps') + oppsData.get(saveIdx-1).countOpps;
                        oppsData.get(saveIdx-1).monthlyRev = (Double)a.get('monthlyRev') + oppsData.get(saveIdx-1).monthlyRev;
                    } else{
                        opp = new OpportunityData(d.format('MMM'),
                                                          (Integer)a.get('noOfClosedWonOpps') ,
                                                          (Double)a.get('monthlyRev'));
                        oppsData.add(opp);
                        saveIdx++;
                    }
                } else{
                    opp = new OpportunityData(d.format('MMM'),
                                                          (Integer)a.get('noOfClosedWonOpps') ,
                                                          (Double)a.get('monthlyRev'));
                    oppsData.add(opp);
                    saveIdx++;
                }
            }
        }
        return oppsData;
    }
    
    //Pipeline Revenue For Current Year By Month (Bar Chart) Not closed/won or closed/lost
    public List<OpportunityData> getOppDataForNotClosedWonLost() {
        
        List<OpportunityData> oppsData = new List<OpportunityData>();
        Integer saveIdx = 0;
        for (AggregateResult a : result)
        {
            Datetime d=Datetime.newInstance((Integer)a.get('year'),(Integer)a.get('month'), 1);
            if(a.get('oppStageName') != 'Closed Won' && a.get('oppStageName') != 'Closed Lost') {
                OpportunityData opp ;    
    
                if(oppsData.size() != 0){
                    if(oppsData.get(saveIdx-1).month == d.format('MMM')){
                        oppsData.get(saveIdx-1).countOpps = (Integer)a.get('noOfClosedWonOpps') + oppsData.get(saveIdx-1).countOpps;
                        oppsData.get(saveIdx-1).monthlyRev = (Double)a.get('monthlyRev') + oppsData.get(saveIdx-1).monthlyRev;
                    } else{
                        opp = new OpportunityData(d.format('MMM'),
                                                          (Integer)a.get('noOfClosedWonOpps') ,
                                                          (Double)a.get('monthlyRev'));
                        oppsData.add(opp);
                        saveIdx++;
                    }
                } else{
                    opp = new OpportunityData(d.format('MMM'),
                                                          (Integer)a.get('noOfClosedWonOpps') ,
                                                          (Double)a.get('monthlyRev'));
                    oppsData.add(opp);
                    saveIdx++;
                }
                
                
            }
        }
        return oppsData;
    }
    
    //Closed Revenue for Year by Budget Source (Pie Chart)
    public List<PieWedgeData> getPieDataGrpByBudgetSource() {
        List<PieWedgeData> data = new List<PieWedgeData>();
        List<AggregateResult> opps = [SELECT SUM(Amount__c) revenue, 
                                     Opportunity_Split__r.Opportunity__r.AccountId accId,
                                     Opportunity_Split__r.Opportunity__r.Agency__c agencyId, 
                                     Opportunity_Split__r.Opportunity__r.Budget_Source__c budgetSource
                                FROM Split_Detail__c 
                            GROUP BY Opportunity_Split__r.Opportunity__r.Budget_Source__c, Opportunity_Split__r.Opportunity__r.Agency__c,
                                 Opportunity_Split__r.Opportunity__r.AccountId,
                                 Opportunity_Split__r.Opportunity__r.StageName,
                                 Date__c
                            HAVING (Opportunity_Split__r.Opportunity__r.AccountId =: accountId OR Opportunity_Split__r.Opportunity__r.Agency__c =: accountId) AND
                                   Opportunity_Split__r.Opportunity__r.StageName = 'Closed Won' AND 
                                   Date__c = THIS_FISCAL_YEAR AND Opportunity_Split__r.Opportunity__r.Budget_Source__c != null];
        
        Map<String, Double> mapOfNameToRevenue = new Map<String, Double>();
        for(AggregateResult ar : opps){
            String strNameTemp = String.valueOf(ar.get('budgetSource'));
            if(mapOfNameToRevenue.containsKey(strNameTemp)) {
                mapOfNameToRevenue.put(strNameTemp, mapOfNameToRevenue.get(strNameTemp)+(Double)ar.get('revenue'));
            }else {
                mapOfNameToRevenue.put(strNameTemp, (Double)ar.get('revenue'));
            }
        }
        for(String str : mapOfNameToRevenue.keySet()){
            data.add(new PieWedgeData(str, mapOfNameToRevenue.get(str)));
        }
        return data;
    }
    
    //Closed Revenue For Year by Industry category (Pie Chart)
    public List<PieWedgeData> getPieDataGrpByIndustryCategory() {
        List<PieWedgeData> data = new List<PieWedgeData>();
        List<AggregateResult> opps = [SELECT SUM(Amount__c) revenue, COUNT_DISTINCT(Opportunity_Split__r.Opportunity__c) noOfClosedWonOpps,
                                     Opportunity_Split__r.Opportunity__r.AccountId accId,
                                     Opportunity_Split__r.Opportunity__r.Agency__c agencyId, 
                                     Opportunity_Split__r.Opportunity__r.Industry_Category__c industryCategory
                                FROM Split_Detail__c 
                            GROUP BY Opportunity_Split__r.Opportunity__r.Industry_Category__c, Opportunity_Split__r.Opportunity__r.Agency__c,
                                 Opportunity_Split__r.Opportunity__r.AccountId,
                                 Opportunity_Split__r.Opportunity__r.StageName,
                                 Date__c
                            HAVING (Opportunity_Split__r.Opportunity__r.AccountId =: accountId OR Opportunity_Split__r.Opportunity__r.Agency__c =: accountId) AND
                                   Opportunity_Split__r.Opportunity__r.StageName = 'Closed Won' AND 
                                   Date__c = THIS_FISCAL_YEAR AND Opportunity_Split__r.Opportunity__r.Industry_Category__c != null];
        
        Map<String, Double> mapOfNameToRevenue = new Map<String, Double>();
        for(AggregateResult ar : opps){
            String strNameTemp = String.valueOf(ar.get('industryCategory'));
            if(mapOfNameToRevenue.containsKey(strNameTemp)) {
                mapOfNameToRevenue.put(strNameTemp, mapOfNameToRevenue.get(strNameTemp)+(Double)ar.get('revenue'));
            }else {
                mapOfNameToRevenue.put(strNameTemp, (Double)ar.get('revenue'));
            }
        }
        for(String str : mapOfNameToRevenue.keySet()){
            data.add(new PieWedgeData(str, mapOfNameToRevenue.get(str)));
        }
        return data;
    }
    
    
    public class OpportunityData {
        public String month { get; set; }
        public Integer countOpps { get; set; }
        public Double monthlyRev { get; set; }

        public OpportunityData(String mon, Integer noOfOpps, Double rev)
        {
            month = mon;
            countOpps = noOfOpps;
            monthlyRev = rev;
        }
    }
    
    public class PieWedgeData {
        public String name { get; set; }
        public Double monthlyRev { get; set; }

        public PieWedgeData(String name, Double rev) {
            this.name = name;
            this.monthlyRev = rev;
        }
    }
    
    @isTest
    public static void ACC_ChartsTest() {
        Account testAcc = new Account(Name = 'Test Account');
        insert testAcc;
        
        ApexPages.StandardController sc = new ApexPages.StandardController(testAcc);
        ApexPages.currentPage().getParameters().put('id', testAcc.Id);

          
           // updated by VG 12/26/2012
          Contact con = UTIL_TestUtil.generateContact(testAcc.id);
    
        
        //Contact c = new Contact();// specify all the required fields
        con.firstname = 'MyFristName';
        con.lastname = 'MyLastName';
        con.Title = 'MyTitle';
        con.Email = 'myemail@mydomain.com';
        con.MailingCity = 'Fremont';
        con.MailingState = 'CA';
        con.MailingCountry = 'USA';
        con.MailingStreet = 'MyStreet';
       insert con;
          
        
        //For Closed Won Stages    
        Opportunity opp = new Opportunity(Name='Test',StageName='Closed Won',CloseDate=Date.today(), AccountId = testAcc.Id, Primary_Billing_Contact__c =con.Id, Bill_on_Broadcast_Calendar2__c = 'Yes',
                                          Industry_Category__c = 'Entertainment', Industry_Sub_Category__c='Entertainment - Gaming',Budget_Source__c = 'TV');
        insert opp;
        
        List<Opportunity_Split__c> lstSplit = 
            [Select Id from Opportunity_Split__c where Opportunity__c = :opp.Id];
        //System.assertNotEquals(0, lstSplit.size());
        opp = [Select OwnerId,X1st_Salesperson_Split__c from Opportunity where id = :opp.Id];
        
        Opportunity_Split__c split = new Opportunity_Split__c();
            split.Split__c = 50;
            split.Role_Type__C='Seller';
            split.Salesperson__c = opp.OwnerId;
            split.Opportunity__c = opp.Id;   
        
            insert split;
            Split_Detail__c splitDetail = new Split_Detail__c(Date__c=Date.today(), Opportunity_Split__c = split.Id);
            insert splitDetail;
            splitDetail = new Split_Detail__c(Date__c=Date.today(), Opportunity_Split__c = split.Id);
            insert splitDetail;
            
            
            
            test.startTest();
            ACC_Charts accChartsTest = new ACC_Charts(sc);
            accChartsTest.getOppDataForClosedWon();
            accChartsTest.getOppDataForNotClosedWonLost();
            accChartsTest.getPieDataGrpByBudgetSource();
            accChartsTest.getPieDataGrpByIndustryCategory();
            test.stopTest();
        
        
    }
    @isTest   
    public static void ACC_ChartsTest1()
    {
            Account testAcc = new Account(Name = 'Test Account');
            insert testAcc;
            
            ApexPages.StandardController sc = new ApexPages.StandardController(testAcc);
            ApexPages.currentPage().getParameters().put('id', testAcc.Id);
    
              
               // updated by VG 12/26/2012
              Contact con = UTIL_TestUtil.generateContact(testAcc.id);
        
            
            //Contact c = new Contact();// specify all the required fields
            con.firstname = 'MyFristName';
            con.lastname = 'MyLastName';
            con.Title = 'MyTitle';
            con.Email = 'myemail@mydomain.com';
            con.MailingCity = 'Fremont';
            con.MailingState = 'CA';
            con.MailingCountry = 'USA';
            con.MailingStreet = 'MyStreet';
           insert con;
        //For other stages
            Opportunity opp = new Opportunity(Name='Test',StageName='Pending Final Paperwork',CloseDate=Date.today(), AccountId = testAcc.Id, Primary_Billing_Contact__c =con.Id, Bill_on_Broadcast_Calendar2__c = 'Yes',
                                              Industry_Category__c = 'Entertainment', Industry_Sub_Category__c='Entertainment - Gaming',Budget_Source__c = 'TV');
            insert opp;
            
            list<Opportunity_Split__c> lstSplit1 = 
                [Select Id from Opportunity_Split__c where Opportunity__c = :opp.Id];
            //System.assertNotEquals(0, lstSplit1.size());
            
            opp = [Select OwnerId,X1st_Salesperson_Split__c from Opportunity where id = :opp.Id];
            
            Opportunity_Split__c split = new Opportunity_Split__c();
            split.Split__c = 50;
            split.Salesperson__c = opp.OwnerId;
            split.Opportunity__c = opp.Id;
            insert split;
            Split_Detail__c splitDetail = new Split_Detail__c(Date__c=Date.today(), Opportunity_Split__c = split.Id);
            insert splitDetail;
            Split_Detail__c splitDetail1 = new Split_Detail__c(Date__c=Date.today(), Opportunity_Split__c = split.Id);
            insert splitDetail1;
            
            test.startTest();
            ACC_Charts accChartsTest = new ACC_Charts(sc);
            accChartsTest.getOppDataForClosedWon();
            accChartsTest.getOppDataForNotClosedWonLost();
            accChartsTest.getPieDataGrpByBudgetSource();
            accChartsTest.getPieDataGrpByIndustryCategory();
            test.stopTest();
    
    }
    
}