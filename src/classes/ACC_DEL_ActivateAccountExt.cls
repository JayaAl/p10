/***************************************************
   Name: ACC_DEL_ActivateAccountExt
   Usage: This Class update the following fields on account to null
			Action
			Delete Requested By
			Delete Requested
			Merge
   Author – Clear Task
   Date – 2/4/2011   
   Revision History
******************************************************/
public class ACC_DEL_ActivateAccountExt {  
     /*Create an object of Account*/ 
     public Account acctObj{get; set;}
     /*flag to show on page*/
     public boolean msgFlag{get; set;}      
    /*
     *Constructor for getting Account record values
     */      
     public ACC_DEL_ActivateAccountExt(ApexPages.StandardController stdController){
         acctObj = (Account)stdController.getRecord();          
     }
     
    /*
     * doActivate method for update fields to null
     */
     public PageReference doActivate(){                  
         if(acctObj.id != null){            
             acctObj.Action__c = null;
             acctObj.Delete_Requested__c = null;
             acctObj.Delete_Requested_By__c = null;
             acctObj.Merge__c = null;
             acctObj.RecordTypeId = [Select Id from RecordType where Name = 'default' And SObjectType = 'Account'].Id;
             try{
                 update acctObj;
                 msgFlag = true;   
                 //now post to chatter
                 postToChatter(acctObj);             
             } catch(DMLException e){
                 msgFlag = false;    
             }                   
         }
         return null;                  
     }
     
     public void postToChatter(Account acctObj) {
		FeedPost fpost = new FeedPost();
		fpost.ParentId = acctObj.Id; //eg. Opportunity id, custom object id..
		fpost.Body = 'Account ' + acctObj.Name + ' has been activated.';
		insert fpost;
     }
     /**
     *testMethod for doActivate
     */
     static testMethod void testDoActivate(){
        /*create account record*/
        Account acctObj = new Account(
            Name = 'First Test Account',
            Type = 'Ad Agency'
        );
        Account acct = new Account(
            Name = 'First Test Account',
            Type = 'Ad Agency'
        );
        try{
            insert acctObj;
            insert acct;
        } catch(DMLException e){}  
        /*standard controller for account*/
       System.currentPagereference().getParameters().put('accountId',acctObj.Id);
       ApexPages.StandardController sc1 = new ApexPages.StandardController(acctObj);
       ACC_DEL_SendRequestController obj=new ACC_DEL_SendRequestController(sc1);
       obj.save();
       Account a = [select Action__c,Delete_Requested_By__c,Delete_Requested__c from Account where id = :acctObj.id];
       System.assertEquals('Delete', a.Action__c);
       System.assertEquals(UserInfo.getName(), a.Delete_Requested_By__c);
       //System.assertEquals(System.now(), a.Delete_Requested__c);
        ApexPages.StandardController sc = new ApexPages.StandardController(a);    
        /*created class object*/
        ACC_DEL_ActivateAccountExt accDelObj = new ACC_DEL_ActivateAccountExt(sc);       
        /*calling doActivate method*/ 
        accDelObj.doActivate();
       /*assert for messageFlag tobe true*/
       Account a1 = [select Action__c,Delete_Requested_By__c,Delete_Requested__c,Merge__c  from Account where id = :a.id];
        System.assertEquals(null, a1.Action__c);      
       System.assertEquals(null, a1.Delete_Requested_By__c);
       System.assertEquals(null, a1.Delete_Requested__c);
       System.assertEquals(null,a1.Merge__c);
    }
}