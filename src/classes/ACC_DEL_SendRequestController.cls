/***************************************************
   Name: ACC_DEL_SendRequestController
   Usage: This Class Changes Picklist value of field Action
   Author – Clear Task
   Date – 1/17/2011   
   Revision History
******************************************************/

public class ACC_DEL_SendRequestController{
   //static variable declaration
     private static final String ACCOUNTMERGE = 'Merge';    
     private static final String ACCOUNTDELETE = 'Delete';
     
   //variables used    
     public boolean showBlock{get; set;} 
     
   //return account    
     public Account record {get; set;}      
     
   //Constructor
     public ACC_DEL_SendRequestController(ApexPages.StandardController stdController){     
         record = (Account) stdController.getRecord();        
         showBlock = true;           
     }
             
   //This method changes the value of Action field
     public PageReference save(){       
         if(record.id != null){ 
             System.debug('Merge With: ' + record.Merge__c);     
             record.Action__c = (record.Merge__c != null) ? ACCOUNTMERGE : ACCOUNTDELETE;             

             try{
                update record;
                //insert chatter feed
                FeedPost fpost = new FeedPost();
                fpost.ParentId = record.id; //eg. Opportunity id, custom object id..
                fpost.Body = 'Account ' + record.Name + ' has been flagged for deletion.';
                insert fpost;
             } catch(Exception e){} 
                               
             showBlock = false;
         }
         return null;
     }
}