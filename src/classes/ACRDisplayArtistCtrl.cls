/**
* ─────────────────────────────────────────────────────────────────────────────────────────────────┐
* Music Makers Hirearchy Display.
* 
* This class is Controller for ACRDisplay vf page.
* Also refere ACRDisplayHelper.cls, ACRDisplayUtil.cls* 
* ──────────────────────────────────────────────────────────────────────────────────────────────────
* @author         Jaya Alaparthi
* @maintainedBy   Jaya Alaparthi
* @version        1.0
* @created        2017-01-13
* @modified       YYYY-MM-DD
* @systemLayer    Service
* @see            ????
* @see            ????
* ──────────────────────────────────────────────────────────────────────────────────────────────────
* @changes
* vX.X            Jaya Alaparthi
* YYYY-MM-DD      Explanation of the change.  Multiple lines can be used to explain the change, but
*                 each line should be indented till left aligned with the previous description text.
*
* vX.X            Jaya Alaparthi
* YYYY-MM-DD      Each change to this file should be documented by incrementing the version number,
*                 and adding a new entry to this @changes list. Note that there is a single blank
*                 line between each @changes entry.
* ─────────────────────────────────────────────────────────────────────────────────────────────────┘
*/
public with sharing class ACRDisplayArtistCtrl {
	
	public String accountId;
	//public void setAccountId(String acctId) {
	//        accountId = acctId;
	//}
	//public String getAccountId() {
	//        return accountId;
	//}
	public ACRDisplayWrapper acrWrapper{get;set;}

    public ACRDisplayArtistCtrl() {

    }
    //─────────────────────────────────────────────────────────────────────────┐
	// init: method invoked as display page action.
    //─────────────────────────────────────────────────────────────────────────┘
    public void init() {
    	
    	acrWrapper = new ACRDisplayWrapper();
    	accountId = ApexPages.currentPage().getParameters().get('id');
    	if(String.isBlank(accountId)) {

    		ApexPages.addmessage(new ApexPages.message(ApexPages.severity.ERROR,'Invalid account.'));
    	}
    	if(!String.isBlank(accountId)) {

    		Account accoutRetrived = new Account();
    		// validate Account Type
    		// Label/SubLabel/Artist
    		accoutRetrived = ACRDisplayUtil.getAccountDetails(accountId);
    		artistLoad(accoutRetrived);
    		//Management/Promotion/Venue/Agency
    	}
    }
    //─────────────────────────────────────────────────────────────────────────┐
	// init: method invoked as display page action.
    //─────────────────────────────────────────────────────────────────────────┘
//	public ACRDisplayArtistCtrl(ApexPages.StandardController ctrl) {

//	}

	//─────────────────────────────────────────────────────────────────────────┐
	// artistLoad: taking current account retrives data and populates wrapper.
    //─────────────────────────────────────────────────────────────────────────┘
    public void artistLoad(Account accoutRetrived) {

		acrWrapper = new ACRDisplayWrapper();
		ACRDisplayHelper acrRef = new ACRDisplayHelper();
		// pass parent accountId and then if parentaccountId is blank then
		// the current account it self is the top level parent account.
		try {
				acrWrapper = acrRef.displayArtistHirearchy(accountId,accoutRetrived);
				if(!String.isBlank(acrWrapper.errorMsg)) {
					ApexPages.addmessage(new ApexPages.message(ApexPages.severity.ERROR,acrWrapper.errorMsg));
				}
			} catch(Exception e) {
				ApexPages.addmessage(new ApexPages.message(ApexPages.severity.ERROR,'Record Provided is not a valid Account.'+e.getMessage()));
			}
    }
        
}