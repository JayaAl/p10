/*
* ─────────────────────────────────────────────────────────────────────────────────────────────────┐
* One or two sentence summary of this class.
*
* Additional information about this class should be added here, if available. Add a single line
* break between the summary and the additional info.  Use as many lines as necessary.
* ──────────────────────────────────────────────────────────────────────────────────────────────────
* @author         Jaya Alaparthi
* @maintainedBy   Jaya Alaparthi
* @version        1.0
* @created        2017-01-13
* @modified       YYYY-MM-DD
* @systemLayer    Service
* @see            ????
* @see            ????
* ──────────────────────────────────────────────────────────────────────────────────────────────────
* @changes
* vX.X            Jaya Alaparthi
* YYYY-MM-DD      Explanation of the change.  Multiple lines can be used to explain the change, but
*                 each line should be indented till left aligned with the previous description text.
*
* vX.X            Jaya Alaparthi
* YYYY-MM-DD      Each change to this file should be documented by incrementing the version number,
*                 and adding a new entry to this @changes list. Note that there is a single blank
*                 line between each @changes entry.
* ─────────────────────────────────────────────────────────────────────────────────────────────────┘
*/
public with sharing class ACRDisplayHelper {

  public ACRDisplayHelper() {}
  //─────────────────────────────────────────────────────────────────────────┐
  // init: method invoked as display page action.
  // @param accountId  current accountId
  // @return ACRDisplayWrapper
    //─────────────────────────────────────────────────────────────────────────┘
   public ACRDisplayWrapper displayArtistHirearchy(Id accountId,
                          Account accountWithParentDetails) {

    System.debug('accountId recived in helper:'+accountId);
    List<AccountContactRelation> acrList = new List<AccountContactRelation>();
    Map<Id,list<AccountContactRelation>> artistContactMap = 
                    new Map<Id,list<AccountContactRelation>>();
    ACRDisplayWrapper artistWrapper = new ACRDisplayWrapper();
    list<Account> artistSiblingAccounts = new list<Account>();
    Id artistParentAcctId;
    list<Account> subLevelAccounts = new list<Account>();
    list<Id> subLabelAcctIds = new list<Id>();
    Map<Id,set<Account>> subLabelArtistMap = new Map<Id,set<Account>>();
    String queryStr = ACRDisplayUtil.ACCT_TYPE_SubLabel;
    //─────────────────────────────────────────────────────────────────────┐
    // Step 1
    // get top level parentId
    // get map if sublabel to artist 
    //─────────────────────────────────────────────────────────────────────┘
    String accountType = accountWithParentDetails.Type;
    // get all the top level parent possible
    Id topLevelParentId = accountId;

    // check if the current account is artist and without 
    // parent then pass the current
    // account Id to retrive the acr list
    // dont call the getCurrentAccountSubLabel
    if(accountWithParentDetails.Type.equalsIgnoreCase(ACRDisplayUtil.ACCT_TYPE_ARTIST)
      && accountWithParentDetails.ParentId == null) {
      
      subLabelAcctIds.add(accountWithParentDetails.Id);
      queryStr = ACRDisplayUtil.ACCT_TYPE_ARTIST;

    } else {

      if(accountWithParentDetails.ParentId != null) {
        topLevelParentId = accountWithParentDetails.ParentId;
        queryStr = ACRDisplayUtil.ACCT_TYPE_SubLabel;
        if(accountWithParentDetails.Parent.ParentId != null){
          topLevelParentId = accountWithParentDetails.Parent.ParentId;
          artistParentAcctId = topLevelParentId;  
          
        }
      }

      // get all children for top level parent.
      subLevelAccounts = ACRDisplayUtil.getCurrentAccountSubLabel(topLevelParentId);
      artistWrapper.sublabelAccounts = subLevelAccounts;
      // get sub label accountIds
      // use this collection to query on ACR
      subLabelAcctIds = getSublabelAcctIds(subLevelAccounts);
    }
    
    acrList = ACRDisplayUtil.getCurrentAccountRelatedContacts(subLabelAcctIds,
            queryStr);
    if(!acrList.isEmpty()) {
      //ACRDisplayUtil.ACCT_TYPE_ARTIST);
      //─────────────────────────────────────────────────────────────────────┐
      // create sublevel to artist map 
      // <subLabel,Set<artistIds>>
      //─────────────────────────────────────────────────────────────────────┘
      subLabelArtistMap = subLabelArtistMapCreation(acrList);
      artistWrapper.perSubLabelArtistList = subLabelArtistMap;
      artistWrapper.sublabelSet = subLabelArtistMap.keySet();
      //─────────────────────────────────────────────────────────────────────┐
      //
      //─────────────────────────────────────────────────────────────────────┘
      
      //─────────────────────────────────────────────────────────────────────┐
      // map artist to contacts for all sibling accounts
      //─────────────────────────────────────────────────────────────────────┘
      artistContactMap = artistAcctContactsMapCreation(acrList);
      //─────────────────────────────────────────────────────────────────────┐
      // End of step 4
      // get sibling accounts related accountcontactrelation records
      //artistAcctContactsMapCreation(acrList);
      //─────────────────────────────────────────────────────────────────────┘
      artistWrapper.acctIdAcctContRelMap = artistContactMap;
      //─────────────────────────────────────────────────────────────────────┐
      // assign grand parent
      //─────────────────────────────────────────────────────────────────────┘
      if(acrList.size() > 0 && acrList[0].Account.ParentId != null
        && acrList[0].Account.Parent.ParentId != null) {

        artistWrapper.parentLabelAcct = 
                new Account(Id = acrList[0].Account.Parent.ParentId,
                      Name = acrList[0].Account.Parent.Parent.Name);
      } 
      //─────────────────────────────────────────────────────────────────────┐
      // get hirearchy for artist to its parent
      //─────────────────────────────────────────────────────────────────────┘
    } else {
      artistWrapper.errorMsg = 'There are no contacts for the account : '
                   +accountWithParentDetails.Name 
                   +' or for the other acounts in its hirearchy';

    }
    return artistWrapper;
  }
  //─────────────────────────────────────────────────────────────────────────┐
  // method to create mapping between artistAcctId to list of 
  // accountcontactRElation under them
  //
  // @param acrList : list of AccountContractRelation records.
  // @return Map<Id,list<AccountContactRelation>>
    //─────────────────────────────────────────────────────────────────────────┘
  public Map<Id,list<AccountContactRelation>> artistAcctContactsMapCreation(list<AccountContactRelation> acrList) {
    
    Map<Id,list<AccountContactRelation>> artistAcctIdContacts 
          = new Map<Id,list<AccountContactRelation>>();

    for(AccountContactRelation acr: acrList) {

      list<AccountContactRelation> acctContactsList = new list<AccountContactRelation>();
      if(artistAcctIdContacts.containsKey(acr.AccountId)) {

        acctContactsList = artistAcctIdContacts.get(acr.AccountId);
      }
      acctContactsList.add(acr);
      artistAcctIdContacts.put(acr.AccountId,acctContactsList);      
    }    
    return artistAcctIdContacts;
  }
  //─────────────────────────────────────────────────────────────────────────┐
  // method to create mapping between artistAcctId to list of 
  // accountcontactRElation under them
  //
  // @param acrList : list of AccountContractRelation records.
  // @return Map<Id,list<AccountContactRelation>>
    //─────────────────────────────────────────────────────────────────────────┘
  public Map<Id,set<Account>> subLabelArtistMapCreation(list<AccountContactRelation> acrList) {
    
    Map<Id,set<Account>> subLabelArtistMap
          = new Map<Id,set<Account>>();

    for(AccountContactRelation acr: acrList) {

      set<Account> artistSet = new set<Account>();
      Account artistAccount = new Account();
      if(acr.Account.ParentId != null) {

        artistAccount.Id = acr.AccountId;
        artistAccount.Name = acr.Account.Name;
        artistAccount.Type = acr.Account.Type;
        if(subLabelArtistMap.containsKey(acr.Account.ParentId)) {
          artistSet = subLabelArtistMap.get(acr.Account.ParentId);
        }
        artistSet.add(artistAccount);
        subLabelArtistMap.put(acr.Account.ParentId,artistSet);      
      }
    }  
    return subLabelArtistMap;
  }
  
  //─────────────────────────────────────────────────────────────────────────┐
  // getArtistSiblingAccts: method to create mapping between artistAcctId 
  // to list of accountcontactRElation under them
  //
  // @param parentLabelId parent accountId
  // @return list<Account>
    //─────────────────────────────────────────────────────────────────────────┘
  private list<Id> getSublabelAcctIds(list<Account> sublabelAccounts) {
    
    list<Id> sublabelAcctIds = new list<Id>();
    for(Account acct: sublabelAccounts) {
      sublabelAcctIds.add(acct.Id);
    }
    return sublabelAcctIds;
  }

  //─────────────────────────────────────────────────────────────────────────┐
  // 1,Get all the current account Id of related contacts [relatedList DetailPage]
  // 2,Get all the current account Id of related contacts [relatedList DetailPage]
  //   and accounts that manageged by those contacts.
  // 3,Get the account name of the current account Id
  // 4, Pass 1 & 2 as a parameter to the getcontManagedAcountList and get 
  // the account list for each contact if it's direct contact to the current account.
  // @return ACRDisplayWrapper
    //─────────────────────────────────────────────────────────────────────────┘
  public ACRDisplayWrapper displayManagementHierarchy(Id accountId){

      List<AccountContactRelation> accContList  = new List<AccountContactRelation>();
      List<AccountContactRelation> contManageAcountList = new List<AccountContactRelation>();
    ACRDisplayWrapper acrWrapper = new ACRDisplayWrapper();
        try{
          // 1
          accContList = ACRDisplayUtil.getAccountContacts(accountId,'AccountRelatedContacts');
          // 2
          contManageAcountList = ACRDisplayUtil.getAccountContacts(accountId,'ContactManagedAccounts');
          // 3
          Account acct = [Select Id , Name from Account where id =:accountId];
      if(accContList.size() > 0) {
          acrWrapper.managementAccountName = new Account(Id = accountId,
                            Name = acct.Name);
            acrWrapper.accContList = accContList; 
           /*for( AccountContactRelation acrChild:accContList) {

              System.debug('acrChild-->'+acrChild.Contact.Name);
          }*/
          } else {

            acrWrapper.errorMsg = 'There are no contacts for the account '+acrWrapper.managementAccountName;
             ApexPages.addmessage(new ApexPages.message(ApexPages.severity.INFO,'There are no contacts for the account -'+acct.Name));
          }
      if(contManageAcountList.size() > 0) {
        // 4
        acrWrapper.contManagedAcountList = getcontManagedAcountList(contManageAcountList,accContList); 
      }
      } catch(Exception ex) {
         ApexPages.addmessage(new ApexPages.message(ApexPages.severity.WARNING,ex.getMessage()));
      }
    
      return acrWrapper;
  }
    
   //─────────────────────────────────────────────────────────────────────────┐
  // // 1,Get all the current account Id of related contacts [relatedList DetailPage]
  // 2,Get all the current account Id of related contacts [relatedList DetailPage]
  // accContList - has only account contacts
  // contManageAcountList - has all the records
  // @return Map<Id,List<AccountContactRelation>> to Wrapper class
    //─────────────────────────────────────────────────────────────────────────┘ 
  private Map<Id,List<AccountContactRelation>> getcontManagedAcountList(List<AccountContactRelation> contManageAcountList,
                              List<AccountContactRelation> accContList) {

        Map<Id,List<AccountContactRelation>>  contManageAcctMap = new Map<Id,List<AccountContactRelation>> ();
        
        try{
          //1
          for( AccountContactRelation acrChild:accContList) {

            List<AccountContactRelation> acrList = new List<AccountContactRelation>();
            //2
            for( AccountContactRelation acrGrandChild:contManageAcountList){
             System.debug('acrChild.IsDirect'+acrChild.IsDirect);
              System.debug('acrChild.IsDirect'+acrChild.IsDirect);
              Boolean flagDuplicate = false;
              if(acrChild.ContactId == acrGrandChild.ContactId 
                      && acrChild.IsDirect == True 
                      && acrGrandChild.IsDirect != True) {
                if(contManageAcctMap.containsKey(acrChild.Id)) {

                  acrList = contManageAcctMap.get(acrChild.Id);
                  //check if duplicate exists 
                  flagDuplicate = ACRDisplayUtil.isDuplicateExists(acrList,acrChild);
                } 
                if(!flagDuplicate) {
                  acrList.add(acrGrandChild);  
              }
               contManageAcctMap.put(acrChild.id, acrList);
               System.debug('acrGrandChild-->'+contManageAcctMap.get(acrGrandChild.id));
                
              }
            }
          }  
        } catch(Exception ex){
         ApexPages.addmessage(new ApexPages.message(ApexPages.severity.WARNING,ex.getMessage()));
      }
   
      return contManageAcctMap;
    
  }
}