/*
* ─────────────────────────────────────────────────────────────────────────────────────────────────┐
* One or two sentence summary of this class.
*
* Additional information about this class should be added here, if available. Add a single line
* break between the summary and the additional info.  Use as many lines as necessary.
* ──────────────────────────────────────────────────────────────────────────────────────────────────
* @author         Jaya Alaparthi
* @maintainedBy   Jaya Alaparthi
* @version        1.0
* @created        2017-01-13
* @modified       YYYY-MM-DD
* @systemLayer    Utility
* @see            ????
* @see            ????
* ──────────────────────────────────────────────────────────────────────────────────────────────────
* @changes
* vX.X            Jaya Alaparthi
* YYYY-MM-DD      Explanation of the change.  Multiple lines can be used to explain the change, but
*                 each line should be indented till left aligned with the previous description text.
*
* vX.X            Jaya Alaparthi
* YYYY-MM-DD      Each change to this file should be documented by incrementing the version number,
*                 and adding a new entry to this @changes list. Note that there is a single blank
*                 line between each @changes entry.
* ─────────────────────────────────────────────────────────────────────────────────────────────────┘
*/
public with sharing class ACRDisplayUtil {
    
    public static List<String> ACCT_TYPE = new List<String> {'Label','Artist','Management Company',
                                            'Venue','Promotion Company',
                                            'Agency'};
    
    public static String ACCT_TYPE_ARTIST = 'Artist';
    public static String ACCT_TYPE_Label = 'Label';
    public static String ACCT_TYPE_SubLabel = 'Sub';
    public static String AACT_RECORD_TYPE_MMG   = 'Music Makers Group';
    public static String CONTACT_RECORD_TYPE_MMG    = 'Music Makers Group';
    
    public static Id getRecordTypeId(String obj, String recType) {
        
        Id mmgRecordId = Schema.getGlobalDescribe().get(obj).getDescribe()
                            .getRecordTypeInfosByName().get(recType)
                            .getRecordTypeId();
        return mmgRecordId;
    }
    //─────────────────────────────────────────────────────────────────────────┐
    // init: method invoked as display page action.
    // @param accountId  current accountId
    // @param queryStr   
    // @return List<AccountContactRelation>
    //─────────────────────────────────────────────────────────────────────────┘
     public static List<AccountContactRelation> getCurrentAccountRelatedContacts(list<Id> accountIds, 
                                                                                String queryStr) {

        List<AccountContactRelation> accountContactRelationList = new List<AccountContactRelation>();
        // retrive records based on the query
        // SELECT Id,Account.Name,Contact.Name,ContactId,Contact.Account.Name,
        //Account_Type__c,Contact_Role__c,IsActive FROM AccountContactRelation
        //WHERE AccountId = '0012200000FQOGQ' OR Contact.AccountId = '0012200000FQOGQ'
        
        String MMGAcctRTypeId = '';
        String MMGContactRTId = '';

        MMGAcctRTypeId = getRecordTypeId('Account',AACT_RECORD_TYPE_MMG);
        MMGContactRTId = getRecordTypeId('Contact',CONTACT_RECORD_TYPE_MMG);
        
        String query = 'SELECT Id,Account.Name,'
                            +' Contact.Name,ContactId,'
                            +' Contact.Account.Name,'
                            +' Account.Type,'
                            +' Contact.Role__c,'
                            +' Account.ParentId,'
                            +' Account.Parent.Name,'
                            +' Account.Parent.ParentId,'
                            +' Account.Parent.Parent.Name '
	                        +' FROM AccountContactRelation'
	                        +' WHERE Account.Type IN:ACCT_TYPE '
	                        +' AND Account.RecordTypeId =: MMGAcctRTypeId '
                            +' AND Contact.RecordTypeId =: MMGContactRTId ';
        if(queryStr.equalsIgnoreCase(ACCT_TYPE_ARTIST)) {

            query += ' AND AccountId IN: accountIds ';

        } else if(queryStr.equalsIgnoreCase(ACCT_TYPE_Label)) {

            query += ' AND Account.Parent.ParentId =: accountIds';

        } else if(queryStr.equalsIgnoreCase(ACCT_TYPE_SubLabel)) {

            query += ' AND Account.ParentId =: accountIds ';
        }
        query += ' Order by Account.Name, Contact.Name ASC ';
        System.debug('Query:'+ query);
        accountContactRelationList = Database.query(query);
        return accountContactRelationList;
    }
    //─────────────────────────────────────────────────────────────────────────┐
    // getCurrentAccountSiblings: retrive siblings of current account
    // @param accountId  current accountId
    // @return list<Account>
    //─────────────────────────────────────────────────────────────────────────┘
    public static list<Account> getCurrentAccountSubLabel(Id accountId) {

        list<Account> siblingAccounts = new list<Account>();
        Id MMGAcctRTypeId = getRecordTypeId('Account','Music Makers Group');
        siblingAccounts = [SELECT Id,Name ,Type
                            FROM Account
                            WHERE ParentId =: accountId
                            AND RecordTypeId =: MMGAcctRTypeId
                            ORDER BY Name ASC];
        return siblingAccounts;
    } 
    //─────────────────────────────────────────────────────────────────────────┐
    // getAccount: get accountType for the accountId
    // @param accountId  current accountId
    // @return Account parent account
    //─────────────────────────────────────────────────────────────────────────┘
    public static Account getAccountDetails(Id accountId) {

        Account account = new Account();
        Id MMGAcctRTypeId = getRecordTypeId('Account','Music Makers Group');
        // do accountId validation
        if(validateParam(accountId)) {
            account = [SELECT Id,Type,ParentId,
                        Parent.ParentId, Name 
                        FROM Account
                        WHERE Id =: accountId
                        AND Type != null
                        AND RecordTypeId =: MMGAcctRTypeId];    
            String acctType = account.Type;
            
            if(account.Type.equalsIgnoreCase(ACCT_TYPE_Label)
                && account.ParentId != null) {
                acctType = ACCT_TYPE_SubLabel;
                account.Type = acctType;
            }
        }
        return account;
    }
    //─────────────────────────────────────────────────────────────────────────┐
    // accountValidation: account validation
    // @param accountId  current accountId
    // @return Boolean 
    //─────────────────────────────────────────────────────────────────────────┘
    private static Boolean validateParam(String accountId) {

        String accountPrefix = Account.sobjecttype.getDescribe().getKeyPrefix();
        Boolean flag = false;
        
        if(!String.isBlank(accountId)) {
            flag = accountId.startsWith(accountPrefix);
        }
        return flag;
    }
    //─────────────────────────────────────────────────────────────────────────┐
    // @param accountId  
    // @return List<AccountContactRelation>
    //─────────────────────────────────────────────────────────────────────────┘
    public static List<AccountContactRelation> getAccountContacts(Id accountId, 
                                                                    String returnACRRecords) {

        List<AccountContactRelation> accountContactList = new List<AccountContactRelation>();

        String MMGAcctRTypeId = '';
        String MMGContactRTId = '';

        MMGAcctRTypeId = getRecordTypeId('Account',AACT_RECORD_TYPE_MMG);
        MMGContactRTId = getRecordTypeId('Contact',CONTACT_RECORD_TYPE_MMG);

        String query = 'SELECT Id,Account.Name,'
                      +' Contact.Name,'
                      +' ContactId,'
                      +' Account.Type,'
                      +' Contact.Account.Name,'
                      +' Contact.Role__c,'
                      +' IsDirect,'
                      +' Contact.AccountId,'
                      +' IsActive '
                      +' FROM AccountContactRelation '
                      +' WHERE Account.Type IN:ACCT_TYPE '
                      +' AND Account.RecordTypeId =: MMGAcctRTypeId ';
         // Get all the current(selected) account Id related contacts from AccountContactRelation object
        if(returnACRRecords == 'AccountRelatedContacts'){
                query += ' AND  AccountId =: accountId ';      
        } 
        // Get all the current(selected) account Id related contacts and accounts that manageged by those contacts. 
        // Fetech only direct contact related accounts
        // Direct Contact - Employee to the selected account
        // InDirect Contact - Contractor to the seleted account
        else if(returnACRRecords == 'ContactManagedAccounts'){
                 query += ' AND ( AccountId =: accountId OR Contact.AccountId =:accountId )'; 
        }  
          query += ' Order by Contact.Name , Account.Name ASC';
          accountContactList = Database.query(query);
        return accountContactList;

    }
    public static Boolean isDuplicateExists(List<AccountContactRelation> acrList, 
                                                AccountContactRelation acr) {
        Boolean duplicateFlag = false;
        for(AccountContactRelation acrIterate : acrList) {

            if(acrIterate.AccountId == acr.AccountId) {
                duplicateFlag = true;
                break;
            }
        }
        return duplicateFlag;
    }
}