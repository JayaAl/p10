/**
* ─────────────────────────────────────────────────────────────────────────────────────────────────┐
* One or two sentence summary of this class.
*
* Additional information about this class should be added here, if available. Add a single line
* break between the summary and the additional info.  Use as many lines as necessary.
* ──────────────────────────────────────────────────────────────────────────────────────────────────
* @author         Jaya Alaparthi
* @maintainedBy   Jaya Alaparthi
* @version        1.0
* @created        2017-01-17
* @modified       YYYY-MM-DD
* @systemLayer    Utility
* @see            ????
* @see            ????
* ──────────────────────────────────────────────────────────────────────────────────────────────────
* @changes
* vX.X            Jaya Alaparthi
* YYYY-MM-DD      Explanation of the change.  Multiple lines can be used to explain the change, but
*                 each line should be indented till left aligned with the previous description text.
*
* vX.X            Jaya Alaparthi
* YYYY-MM-DD      Each change to this file should be documented by incrementing the version number,
*                 and adding a new entry to this @changes list. Note that there is a single blank
*                 line between each @changes entry.
* ─────────────────────────────────────────────────────────────────────────────────────────────────┘
*/
public with sharing class ACRDisplayWrapper {
	

	public Map<Id,list<AccountContactRelation>> acctIdAcctContRelMap {get;set;}
	public List<Account> sublabelAccounts {get;set;}
	public Map<Id,set<Account>> perSubLabelArtistList{get;set;}
	public set<Id> sublabelSet{get;set;}
	public Account parentLabelAcct {get;set;}
	public String errorMsg {get;set;}
	
	//The variable used by account type - Management Company, Promotion Company
	// & Venue, Agency and Artist who doesn't have a parent
	public Account managementAccountName {get;set;}
	public List<AccountContactRelation> accContList{get;set;}
	public Map<Id,list<AccountContactRelation>> contManagedAcountList{get;set;}

	public ACRDisplayWrapper() {}

}