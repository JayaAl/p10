@isTest
public with sharing class ACRHierarchyTest {
    public ACRHierarchyTest() {
        
    }
    
    //insert Bulk accounts and contacts. This list used when user search accounts and contacts
    public static void insertAccountAndContacts(){
    // Hierarchy View Type 1 -  used by account type - Label, Artist
    //Hierarchy View Type 2 -  used by account type - Management Company, Promotion Company
	// & Venue, Agency and Artist who doesn't have a parent
    // insert Accounts/Contacts/AccountContactRelationship to View Type 1
        List<AccountContactRelation> acrList;
        Integer count = 201;
        String accountType = 'Artist';
        String contRole = 'Manager';
        List<Account> accList = new List<Account>();
        List<Contact> contList = new List<Contact>();
        // Get Account & Contact Record Type Id;
        Id acctRecdtypeId = getRecordTypeId('Account','Music Makers Group');
        Id contRecdtypeId = getRecordTypeId('contact','Music Makers Group');

        //LEVEL 1 : insert Grand Parent Account
        Account grantParentAccount = new Account(Name = 'Sony', 
                                                Type = 'Label', 
                                                RecordTypeId = acctRecdtypeId,
                                                c2g__CODAAccountTradingCurrency__c = 'USD');
        insert grantParentAccount;
        //LEVEL 2 :insert Parent Account
        Account parentAccount = new Account(Name = ' RCA', 
                                            Type = 'Label', 
                                            ParentId = grantParentAccount.id, 
                                            RecordTypeId = acctRecdtypeId,
                                            c2g__CODAAccountTradingCurrency__c = 'USD');
        insert parentAccount;
        //LEVEL 3: insert List of Accounts 
        for(Integer i=0; i<count;i++){
            Account account = new Account(Name = '  Alicia Keys'+i, 
                                        Type = accountType,
                                        ParentId = parentAccount.Id,
                                        RecordTypeId = acctRecdtypeId,
                                        c2g__CODAAccountTradingCurrency__c = 'USD');
            accList.add(account);
        }
        
        insert accList;

        //LEVEL 4 : insert direct contacts to the above 
        for(Account accId:accList){
            for(Integer i=0; i<2;i++){
                Contact cont = new Contact(LastName ='MMG Hierarchy Contact LN'+i, AccountId = accId.id, Role__c = contRole, RecordTypeId = contRecdtypeId);
                contList.add(cont);
            } 
        }
        
        //Test.startTest();
        insert contList;
        //Test.stopTest();

        //insert Company account
        //Puspose of this account - Add Indirect Contact to the above Artist Account & below Maverick Management Account for Testing
        Account companyAcct = new Account(Name = 'Company ', 
                                    Type = 'Artist',
                                    RecordTypeId = acctRecdtypeId);
        insert companyAcct;
        //insert Contact to the above Company Account
        Contact companyCont = new Contact(LastName ='Brenda Reynoso',
                                        AccountId = companyAcct.id,
                                        Role__c = contRole,
                                        RecordTypeId = contRecdtypeId);
        insert companyCont;
        // Add ACR to Artist Account
        acrList = new List<AccountContactRelation>();
        for(Account acct : accList){
            AccountContactRelation acr = new AccountContactRelation();
            acr.AccountId = acct.Id;
            acr.ContactId = companyCont.Id;
            acrList.add(acr);
            
        }
        insert acrList;
        // Hierarchy View Type 2 Test code starts here
        // insert Mavericl Management Account
        Account maverickAcct = new Account(Name = 'Maverick Management', Type = 'Management Company',  RecordTypeId = acctRecdtypeId);
        insert maverickAcct;
        //insert Direct Contact to the Maverick account
        Contact maverickCont = new Contact(LastName ='Guy Oseary',
                                            AccountId = maverickAcct.id,
                                            Role__c = 'Other', 
                                            RecordTypeId = contRecdtypeId);
        insert maverickCont;
        
        // Add ACR to Maverick Management Account
         AccountContactRelation maverickACR = new AccountContactRelation();
         maverickACR.AccountId = maverickAcct.Id;
         maverickACR.ContactId = companyCont.Id;
         insert maverickACR;
         // Get inserted Artist ACR Id List
        set<Id> acrIds = new set<Id>();
        for(AccountContactRelation acId:acrList){
            acrIds.add(acId.Id);
        }
        
        List<AccountContactRelation> acrList2 = [Select Id from AccountContactRelation where Id IN:acrIds];
        system.debug('acrList==>'+acrList.size());
       //Get inserted Account Id List
        set<Id> accIds = new set<Id>();
        for(Account acId:accList){
            accIds.add(acId.Id);
        }
        // Add Company Accout Id to the above account Id list
        accIds.add(maverickAcct.id);
         // verify the inserted accounts	
        List<Account> totalAcc = [Select Id, Type from Account where id IN: accIds];
        system.assertEquals(202, totalAcc.size());
        system.debug('totalAcc'+totalAcc.size());
        // Verify the inserted contacts	
        set<Id> contIds = new set<Id>();
        for(Contact conId:contList){
            contIds.add(conId.Id); 
        }

        List<Contact> totalCont = [Select Id from Contact where id IN: contIds];
        //Inerted 2 Accounts for each Account(see the above variable called Count for no of accounts)
        system.assertEquals(402, totalCont.size());
        //Get only Maverick Account
        List<Account> totalAcc2 = [Select Id,Type from Account where id =: maverickAcct.id];
        system.assertEquals(1, totalAcc2.size());
        system.debug('totalAcc2'+totalAcc2[0].Type);
        Test.startTest();
        acrManagementHierarchyViewTest(totalAcc);		
        Test.stopTest();
        
    }
     // Return Music Makers Group Account/Contact Record Type Id
     public static Id getRecordTypeId(String obj, String recType) {
        Id mmgRecordId = Schema.getGlobalDescribe().get(obj).getDescribe()
            .getRecordTypeInfosByName().get(recType)
            .getRecordTypeId();
        return mmgRecordId;
    }
   
    
    public static testMethod void excuteinsertMethod(){
        insertAccountAndContacts();
    }
   
    public static void acrManagementHierarchyViewTest(List<Account> totalAcc){
    	
        ACRManagementHierarchyCompCtlr managementHierarcy = new ACRManagementHierarchyCompCtlr();
        ACRDisplayArtistCtrl artistHierarchy = new ACRDisplayArtistCtrl();
       
        // Verify hierarchy view type 1 -  used by account type - Label, Artist
        ApexPages.currentPage().getParameters().put('id',totalAcc[0].id);
        artistHierarchy.init();
        Account accountName = artistHierarchy.acrWrapper.parentLabelAcct;
        // Hierarchy Level 1
        system.assertEquals('Sony', accountName.Name);
        List<Account> sublabelAccounts = artistHierarchy.acrWrapper.sublabelAccounts;
        // Hierarchy Level 2
        system.assertEquals('RCA', sublabelAccounts[0].Name);
        system.assertEquals('Label', sublabelAccounts[0].Type);
        Map<Id,list<AccountContactRelation>> acctIdAcctContRelMap = artistHierarchy.acrWrapper.acctIdAcctContRelMap;
         system.assertEquals(201, acctIdAcctContRelMap.size());
       // system.debug('accContactList SSS'+acctIdAcctContRelMap.size());
       // Hierarchy Level 3
        Map<Id,set<Account>> perSubLabelArtistList = artistHierarchy.acrWrapper.perSubLabelArtistList;
         system.assertEquals(1, perSubLabelArtistList.size());
        //system.debug('accContactList SSS'+perSubLabelArtistList.size());
        
        Id acctRecdtypeId = getRecordTypeId('Account','Music Makers Group');
        Account grantParentAccount = new Account(Name = 'Sony Test', Type = 'Label', RecordTypeId = acctRecdtypeId);
        insert grantParentAccount;
        ApexPages.currentPage().getParameters().put('id',grantParentAccount.id);
        artistHierarchy.init();
        //Hierarchy View Type 2 -  used by account type - Management Company, Promotion Company
	// & Venue, Agency and Artist who doesn't have a parent
        for(Account acct : totalAcc){
        	if(acct.Type == 'Management Company'){
        	 ApexPages.currentPage().getParameters().put('id',acct.id);
        	  managementHierarcy.init();
        	}
       
        }
         Account maverickAccountName = managementHierarcy.acrWrapper.managementAccountName; 
         system.assertEquals('Maverick Management',maverickAccountName.Name);
        List<AccountContactRelation> accContactList = managementHierarcy.acrWrapper.accContList;
        system.assertEquals(2,accContactList.size());
        ApexPages.currentPage().getParameters().put('id',Null);
        artistHierarchy.init();
        ApexPages.currentPage().getParameters().put('id','');
        managementHierarcy.init();
    }
}