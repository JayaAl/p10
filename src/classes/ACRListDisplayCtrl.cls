/**
* ─────────────────────────────────────────────────────────────────────────────────────────────────┐
* Controller for ACR list display componenets and also for ServiceRelatedContacts.page.
* this class also includes Lightning supported methods which are commented.
* ──────────────────────────────────────────────────────────────────────────────────────────────────
* @author         Jaya Alaparthi
* @maintainedBy   Jaya Alaparthi
* @version        1.0
* @created        2018-01-11
* @modified       
* @systemLayer    Controller
* @see            
* @see            ????
* ──────────────────────────────────────────────────────────────────────────────────────────────────
* @changes
* v1.1          Jaya Alaparthi
* YYYY-MM-DD    
* ──────────────────────────────────────────────────────────────────────────────────────────────────
* @changes
* v1.1          Jaya Alaparthi
* YYYY-MM-DD       
* ─────────────────────────────────────────────────────────────────────────────────────────────────┘
*/
public with sharing class ACRListDisplayCtrl {


    public list<AccountContactRelation> acrList {get;set;}
    public list<AccountContactRelation> subACRList {get;set;}
    public Boolean listMore {get;set;}

     public ACRListDisplayCtrl(ApexPages.StandardController stdController) {


        listmore = false;
        subACRList = new list<AccountContactRelation>();
        //Id currentRecId = (Case)stdController.getRecord().Id;

        listmore = ApexPages.currentPage().getparameters().get('l') == 'true' ? true : false;
        System.debug('listmore:'+listmore);
        Id caseId = ((Case)stdController.getRecord()).Id;
        System.debug('caseId:'+caseId);
        Case caseRec = getCaseDetails(caseId);
        acrList = getACRs(caseRec.AccountId);
        for(Integer i=0; i<1 && acrList.size() >= 1; i++) {
        	subACRList.add(acrList[i]);
        }
        
    }
    public void morefromList() {
    	listmore = true;
    }
    //───────────────────────────────────────────────────────────────────────────┐
    // param        type                    
    // searchKey    String passing in user keyed search name.
    // lookupObj    SObject that we have query on. 
    //              This is passed from lightning componenet
    // ──────────────────────────────────────────────────────────────────────────
    // Description: getCurrentUserId returns current userId                     
    //───────────────────────────────────────────────────────────────────────────┘
    /*@AuraEnabled
    public static List<AccountContactRelation> getACRList(Id caseId, Id caseAcctId) {

        Id caseRelatedAcctId;
        list<AccountContactRelation> acctContRelationList = new List<AccountContactRelation>();

        /*Case caseRec = getCaseDetails(caseId);
        if(caseRec.Id != null && caseRec.AccountId != null) {
            caseRelatedAcctId = caseRec.AccountId;
            acctContRelationList = getACRs(caseRelatedAcctId);
        }*/
        /*System.debug('caseId:'+caseId+' caseAcctId:'+caseAcctId);
        if(caseId != null && String.isBlank(caseAcctId)) {

            Case retrivedCase = getCaseDetails(caseId);
            caseAcctId = retrivedCase.AccountId;
        }
        acctContRelationList = getACRs(caseAcctId);
        System.debug('acctContRelationList:'+acctContRelationList);
        return acctContRelationList;
    }*/
    //───────────────────────────────────────────────────────────────────────────┐
    // param        type                    
    // caseId    	Case Id
    // ──────────────────────────────────────────────────────────────────────────
    // Description: getCaseDetails gets case realted details
    //───────────────────────────────────────────────────────────────────────────┘
    private static Case getCaseDetails(Id caseId) {

        System.debug('caseId:'+caseId);
        Case caseRec = new Case();
        caseRec = [SELECT Id, AccountId, ContactId 
        			FROM Case 
                    WHERE Id =: caseId];
        System.debug('caseRec:'+caseRec);
        return caseRec;
    }
    //───────────────────────────────────────────────────────────────────────────┐
    // param        type                    
    // acctId    	Account Id
    // ──────────────────────────────────────────────────────────────────────────
    // Description: getACRs retrives Account Contact Relations
    //───────────────────────────────────────────────────────────────────────────┘
    private static list<AccountContactRelation> getACRs(Id acctId) {

        list<AccountContactRelation> acctContRelationList = new List<AccountContactRelation>();
        
        System.debug('acctId:'+acctId);
        acctContRelationList = [SELECT Id,AccountId,ContactId,Contact.AccountId,
                                        Account.Name, Contact.Name,Contact.Email,
                                            Relationship_Type__c 
                                FROM AccountContactRelation
                                WHERE AccountId =: acctId];
        System.debug('acctContRelationList:'+acctContRelationList);
        return acctContRelationList;        
    }
}