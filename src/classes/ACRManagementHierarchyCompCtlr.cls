public with sharing class ACRManagementHierarchyCompCtlr {

	public String accountId 	{get;set;}
	public ACRDisplayWrapper acrWrapper    {get;set;} 

	public ACRManagementHierarchyCompCtlr() {}

    public void init() {
    	accountId = ApexPages.currentPage().getParameters().get('id');
    	managementHierarchyLoad();
    }
    
	public void managementHierarchyLoad(){
		acrWrapper = new ACRDisplayWrapper();
		ACRDisplayHelper acrHelper = new ACRDisplayHelper();
		try{
			if(accountId != Null){
			acrWrapper = acrHelper.displayManagementHierarchy(accountId);	
		}
	}catch(Exception ex){
		 ApexPages.addmessage(new ApexPages.message(ApexPages.severity.WARNING,ex.getMessage()));
		}	
	}
}