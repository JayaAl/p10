public with sharing class ACRManagementHierarchyHelper {

  public ACRManagementHierarchyHelper() {}

  Public ACRManagementHierarchyWrapper displayManagementHierarchy(Id accountId, String accountType){
    List<AccountContactRelation> accContList  = new List<AccountContactRelation>();
      List<AccountContactRelation> contManageAcountList = new List<AccountContactRelation>();
      ACRManagementHierarchyWrapper acrWrapper = new ACRManagementHierarchyWrapper();
      accContList = ACRManagementHierarchyUtil.getAccountContacts(accountId);
      contManageAcountList = ACRManagementHierarchyUtil.getContactManageUsersAccounts(accountId);
      if(accountId != Null){
        Account acct = [Select Id , Name from Account where id =:accountId  Limit 1];
        if(acct != Null){
          acrWrapper.parentSublabelAcct = new Account(Id = accountId,Name = acct.Name);
        }
      }
      if(accContList.size() > 0){

        acrWrapper.managementRelatedContacts = accContList; 
      }
      if(contManageAcountList.size() > 0){
        acrWrapper.managementGrantChildContacts = getChildList(contManageAcountList,accContList); 
      }
      return acrWrapper;
  }
    
 public Map<Id,List<AccountContactRelation>> getChildList(List<AccountContactRelation> contManageAcountList,List<AccountContactRelation> accContList){
        Map<Id,List<AccountContactRelation>>  contManageAcctMap = new Map<Id,List<AccountContactRelation>> ();
          for( AccountContactRelation acrChild:accContList){
            for( AccountContactRelation acrGrandChild:contManageAcountList){
              if(acrChild.ContactId == acrGrandChild.ContactId && acrChild.IsDirect == True){
                if(acrGrandChild.IsDirect != True){
                List<AccountContactRelation> acrList = new List<AccountContactRelation>();
               acrList.add(acrGrandChild);
               contManageAcctMap.put(acrGrandChild.id, acrList);
                }
              }
             
            }
          }
      return contManageAcctMap;
  }
     
}