public with sharing class ACRManagementHierarchyUtil {

public static List<String> ACCT_TYPE = new List<String> {'Label','Artist','Management_Company', 'Venue','Promotion_Company', 'Agency'};


public ACRManagementHierarchyUtil() {}

public static List<AccountContactRelation> getAccountContacts(Id accountId) 
{

List<AccountContactRelation> accountContactList = new List<AccountContactRelation>();
String query = 'SELECT Id,Account.Name,'
          +'Contact.Name,'
          +'ContactId,'
          +'Account.Type,'
          +'Contact.Account.Name,'
          +'Contact.Role__c,'
          +'IsDirect,'
          +'Contact.AccountId,'
          +'IsActive '
          +'FROM AccountContactRelation '
          +'WHERE Account.Type IN:ACCT_TYPE '
          + 'AND  AccountId =: accountId ';
accountContactList = Database.query(query);
return accountContactList;

}
public static list<AccountContactRelation> getContactManageUsersAccounts(Id accountId) {
  
List<AccountContactRelation> contactRelatedAccList = new List<AccountContactRelation>();
String query = 'SELECT Id,Account.Name,'
          +'Contact.Name,'
          +'ContactId,'
          +'Account.Type,'
          +'Contact.Account.Name,'
          +'Contact.Role__c,'
          +'IsDirect,'
          +'IsActive '
          +'FROM AccountContactRelation '
          +'WHERE Account.Type IN:ACCT_TYPE '
          +'AND ( AccountId =: accountId '
	        +'OR Contact.AccountId =:accountId )'; 
	 contactRelatedAccList = Database.query(query);
return contactRelatedAccList;

}
//public static Id getRecordTypeId(String obj, String recType) {
//    Id mmgRecordId = Schema.getGlobalDescribe().get(obj).getDescribe()
//              .getRecordTypeInfosByName().get(recType)
//              .getRecordTypeId();
//    return mmgRecordId;
//  }
//public static Account getAccountDetails(Id accountId) {

//    Account account = new Account();
//    Id MMGAcctRTypeId = getRecordTypeId('Account','MMG');
//    // do accountId validation
//    if(validateParam(accountId)) {
//      account = [SELECT Id,Type,ParentId,
//            Parent.ParentId, Name 
//            FROM Account
//            WHERE Id =: accountId
//            AND Type != null
//            AND RecordTypeId =: MMGAcctRTypeId];  
//      String acctType = account.Type;
      
      
//    }
//    return account;
//  }
//   private static Boolean validateParam(String accountId) {

//      String accountPrefix = Account.sobjecttype.getDescribe().getKeyPrefix();
//      Boolean flag = false;
      
//      if(!String.isBlank(accountId)) {
//        flag = accountId.startsWith(accountPrefix);
//      }
//      return flag;
    //}
}