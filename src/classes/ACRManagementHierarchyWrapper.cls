public with sharing class ACRManagementHierarchyWrapper {

	public Account parentSublabelAcct {get;set;}
	public Account managementAccountName {get;set;}
	public List<AccountContactRelation> accContList{get;set;}
	public Map<Id,list<AccountContactRelation>> contManageAcountList{get;set;}
	public ACRManagementHierarchyWrapper() {}
}