public with sharing class ADP_AccountChartsController{
     public String associatedAccountIds {get;set;}
     public ADP_AccountChartsController(ApexPages.StandardController stdController) {
         String accountStr = '';
         for(ADP_Associated_Accounts__c aaa: [Select Account__c from ADP_Associated_Accounts__c 
                                                where Account_Development_Plan__c=: Apexpages.currentpage().getparameters().get('id')]) {
             accountStr += aaa.Account__c + ',';
         }
         if(accountStr != '') {
             accountStr = accountStr.substring(0, accountStr.length()-1);
         }
         associatedAccountIds = accountStr;
         system.debug('associatedAccountIds******' + associatedAccountIds);
     }
    
}