/**
 * @name: ADP_AssociatedAccountController 
 * @desc: Used to inline add, edit, delete, delete all ADP Associated Accounts
 * @author: Lakshman(sfdcace@gmail.com)
 * @date: 24-8-2014
 */
public with sharing class ADP_AssociatedAccountController {

    //list of all account team members, existing and blank for the page
    public List<ADP_Associated_Accounts__c> aaaList { get; set; }
    public ADP_Associated_Accounts__c editAAA { get; set; }
    public boolean nowAdding { get; private set; }
    public Boolean saveDone {get; set;}
    public List<ADP_Associated_Accounts__c> fullAAAList { get {
        List<ADP_Associated_Accounts__c> fullAAAList = new List<ADP_Associated_Accounts__c>();
        fullAAAList.addAll(aaaList);
        if (nowAdding != null && nowAdding && editAAA != null) {
            fullAAAList.add(editAAA);
        }
        return fullAAAList;
    }}
    
    public boolean hasMessages { get {return ApexPages.hasMessages();} }
    
    //account id from the page
    public id accId {get; set
        {
            this.accId = value;
            aaaList = queryAAAList();
        } 
    }
    private String ownerId;
    private Account_Development_Plan__c accountInfo;
    public ADP_AssociatedAccountController(ApexPages.StandardController stdController) {
        //grab the account Id from the acc record we're on
        accId = stdController.getId();
        system.debug('******' + accId);
        accountInfo = [Select OwnerId, Owner.Email, Name, Id from Account_Development_Plan__c where Id = :accId];
        ownerId = accountInfo.OwnerId;
        nowAdding = false;
        saveDone = false;
    }

    public void addNewRow() {
        ADP_Associated_Accounts__c newRow = new ADP_Associated_Accounts__c(Account_Development_Plan__c = accId);
        editAAA = newRow;
        nowAdding = true;
    }

    //These methods are responsible for editing and deleting an EXISTING condition
    public String getParam(String name) {
        return ApexPages.currentPage().getParameters().get(name);  
    }

    public void delAAA() {
        String delid = getParam('delid');
        try {
            ADP_Associated_Accounts__c ocr = [SELECT Id /*Primary_Account__c*/ FROM ADP_Associated_Accounts__c WHERE ID=:delid];
            //if(!ocr.Primary_Account__c) {
                delete ocr;
                saveDone = true;
                aaaList = queryAAAList();
            //} else {
            //    ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.Fatal, 'You need to have atleast one Primary Account'));
            //}
            
        } catch (system.Dmlexception e) {
            ApexPages.addMessages(e);
        }
    }
    /*
    public void deleteAll() {
        try {
            ADP_Associated_Accounts__c[] ocr = [SELECT Id FROM ADP_Associated_Accounts__c WHERE Account_Development_Plan__c=:accId];
            delete ocr;
            aaaList = queryAAAList();
        } catch (system.Dmlexception e) {
            ApexPages.addMessages(e);
        }
    }
    */
        
    public void editOneAAA() {
        String editid = getParam('editid');
        editAAA  = [SELECT id, Account_Development_Plan__c, Account__c, /*Primary_Account__c,*/ Account__r.Name, Total_Employees__c, Industry__c, Type__c
              FROM ADP_Associated_Accounts__c where id = :editId]; 
    }

    
    public void cancelEdit() {
        editAAA = null;
        nowAdding = false;
    }
    
    public void saveEdit() { 
        try { 
            //Set<Id> existingPrimary = new Set<Id> ();
            // Refresh list of account team member
            List<ADP_Associated_Accounts__c> existingAAA = queryAAAList();
            List<ADP_Associated_Accounts__c> aaaToUpsert = new List<ADP_Associated_Accounts__c>();
            system.debug('******' + accId);
            /*if(existingAAA.size() != 0) {
                for (ADP_Associated_Accounts__c aaa : existingAAA) {
                    if( aaa.Primary_Account__c) {
                        existingPrimary.add(aaa.Id);
                    }
                }
            }
            if((editAAA.Id != null && existingPrimary.contains(editAAA.Id) && !editAAA.Primary_Account__c) || (existingPrimary.isEmpty() && !editAAA.Primary_Account__c)) {
                ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.Fatal, 'You need to have atleast one Primary Account'));
            }*/ 
            if(true) {
                //system.debug('Edit AAA '+ editAAA);
                //system.debug('AAA for my update ' + aaaToUpsert);
     
                aaaToUpsert.add(editAAA);
                upsert aaaToUpsert id; 
                AT_EmailUtil.sendBulkEmail();
                editAAA = null;
                nowAdding = false;
                saveDone = true;
                aaaList = queryAAAList();
                
            }
            
        } catch (system.Dmlexception e) {
            ApexPages.addMessages(e);
        }
        
    } 
    
    private List<ADP_Associated_Accounts__c> queryAAAList() {
        return [SELECT id, Account_Development_Plan__c, Account__c, /*Primary_Account__c,*/ Account__r.Name, Total_Employees__c, Industry__c, Type__c
              FROM ADP_Associated_Accounts__c WHERE
              Account_Development_Plan__c = :accId ORDER BY Account__r.Name];
    } 
}