/**
 * @name: ADP_PandoraSalesTeamController 
 * @desc: Used to inline add, edit, delete, delete all Pandora Sales Team
 * @author: Lakshman(sfdcace@gmail.com)
 * @date: 24-8-2014
 */
public with sharing class ADP_PandoraSalesTeamController {

    //list of all account team members, existing and blank for the page
    public List<Pandora_Sales_Team__c> pstList { get; set; }
    public Pandora_Sales_Team__c editPST { get; set; }
    public boolean nowAdding { get; private set; }
    
    public List<Pandora_Sales_Team__c> fullPSTList { get {
        List<Pandora_Sales_Team__c> fullPSTList = new List<Pandora_Sales_Team__c>();
        fullPSTList.addAll(pstList);
        if (nowAdding != null && nowAdding && editPST != null) {
            fullPSTList.add(editPST);
        }
        return fullPSTList;
    }}
    
    public boolean hasMessages { get {return ApexPages.hasMessages();} }
    
    //account id from the page
    public id accId {get; set
        {
            this.accId = value;
            pstList = queryPSTList();
        } 
    }
    private String ownerId;
    private Account_Development_Plan__c accountInfo;
    public ADP_PandoraSalesTeamController(ApexPages.StandardController stdController) {
        //grab the account Id from the acc record we're on
        accId = stdController.getId();
        accountInfo = [Select OwnerId, Owner.Email, Name, Id from Account_Development_Plan__c where Id = :accId];
        ownerId = accountInfo.OwnerId;
        nowAdding = false;
    }

    public void addNewRow() {
        Pandora_Sales_Team__c newRow = new Pandora_Sales_Team__c(Account_Development_Plan__c = accId, Team_Role__c=null);
        editPST = newRow;
        nowAdding = true;
    }

    //These methods are responsible for editing and deleting an EXISTING condition
    public String getParam(String name) {
        return ApexPages.currentPage().getParameters().get(name);  
    }

    public void delPST() {
        String delid = getParam('delid');
        try {
            Pandora_Sales_Team__c ocr = [SELECT Id FROM Pandora_Sales_Team__c WHERE ID=:delid];
            delete ocr;
            pstList = queryPSTList();
        } catch (system.Dmlexception e) {
            ApexPages.addMessages(e);
        }
    }
    
    public void deleteAll() {
        try {
            Pandora_Sales_Team__c[] ocr = [SELECT Id FROM Pandora_Sales_Team__c WHERE Account_Development_Plan__c=:accId];
            delete ocr;
            pstList = queryPSTList();
        } catch (system.Dmlexception e) {
            ApexPages.addMessages(e);
        }
    }
   
        
    public void editOnePST() {
        String editid = getParam('editid');
        editPST  = [SELECT id, Account_Development_Plan__c, Team_Role__c, Team_Member__c, Team_Member__r.Name, Team_Member__r.IsActive
              FROM Pandora_Sales_Team__c where id = :editId]; 
    }

    
    public void cancelEdit() {
        editPST = null;
        nowAdding = false;
    }
    
    public void saveEdit() { 
        try { 
            boolean primaryAssigned = false;
            //Set<String> teamMemberRolesSelected = new Set<String> ();
            Set<Id> usersSelected = new Set<Id> ();
            // Refresh list of account team member
            List<Pandora_Sales_Team__c> existingPST = queryPSTList();

            List<Pandora_Sales_Team__c> pstToUpsert = new List<Pandora_Sales_Team__c>();
            
            if(existingPST.size() != 0) {
                for (Pandora_Sales_Team__c pst : existingPST) {
                    if( pst.id != editPST.id) {
                        //teamMemberRolesSelected.add(pst.Team_Role__c);
                        usersSelected.add(pst.Team_Member__c);
                    }
                }
            }
            /*if(teamMemberRolesSelected.contains(editPST.Team_Role__c)) {
                ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.Fatal, 'You cannot have duplicate Team Member Role'));
            } else*/ if(usersSelected.contains(editPST.Team_Member__c)) {
                ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.Fatal, 'You cannot have duplicate Team Member'));
            } else {
                //system.debug('Edit PST '+ editPST);
                //system.debug('PST for my update ' + pstToUpsert);
     
                pstToUpsert.add(editPST);
                /*We dont need Email functionality
                if(accountInfo.OwnerId != editPST.Team_Member__c) {
                    User teamMember = [Select Email, Name from User where Id =: editPST.Team_Member__c];
                    String sfdcBaseURL = URL.getSalesforceBaseUrl().toExternalForm();
                    if(editPST.Id == null) {
                        AT_EmailUtil.to(new String[]{accountInfo.Owner.Email})
                                    .saveAsActivity(false)
                                    .senderDisplayName('Salesforce.com')
                                    .subject('Pandora Sales Team was updated' )
                                    .htmlBody('Pandora Sales Team was updated for: <a href="' + sfdcBaseURL +'/' + accountInfo.Id + '">' + accountInfo.Name + '</a> <br/>' +
                                              'Pandora Sales Team name-' + teamMember.Name + '<br/> Team Role: ' + editPST.Team_Role__c)
                                    .useSignature(false)
                                    .replyTo(teamMember.email)
                                    .stashForBulk();
    
                    } else {
                        AT_EmailUtil.to(new String[]{accountInfo.Owner.Email})
                                    .saveAsActivity(false)
                                    .senderDisplayName('Salesforce.com')
                                    .subject('New Pandora Sales Team was added' )
                                    .htmlBody('New Pandora Sales Team was added for: <a href="' + sfdcBaseURL +'/' + accountInfo.Id + '">' + accountInfo.Name + '</a> <br/>' +
                                              'Pandora Sales Team name-' + teamMember.Name + '<br/> Team Role: ' + editPST.Team_Role__c)
                                    .useSignature(false)
                                    .replyTo(teamMember.email)
                                    .stashForBulk();
                    }
                }
                */
                upsert pstToUpsert id; 
                //AT_EmailUtil.sendBulkEmail();
                editPST = null;
                nowAdding = false;
                
                pstList = queryPSTList();
                
            }
            
        } catch (system.Dmlexception e) {
            ApexPages.addMessages(e);
        }
        
        //return null;
    } 
    
    private List<Pandora_Sales_Team__c> queryPSTList() {
        return [SELECT id, Account_Development_Plan__c, Team_Role__c, Team_Member__c, Team_Member__r.Name, Team_Member__r.IsActive 
              FROM Pandora_Sales_Team__c WHERE
              Account_Development_Plan__c = :accId ORDER BY LastModifiedDate];
    } 
}