/*
 * @name: ADP_TestClass
 * @author: Lakshman(sfdcace@gmail.com)
 * @desc: Test class to cover all ADP related classes
 * @date: 11-9-2014
 */
@isTest
public class ADP_TestClass {
    static testMethod void testADPChartsController() {
        Account_Development_Plan__c testADP = new Account_Development_Plan__c(Name= 'Sample');
        insert testADP;
        Account testAccount = UTIL_TestUtil.createAccount();
        ADP_Associated_Accounts__c testAAA = new ADP_Associated_Accounts__c();
        testAAA.Account__c = testAccount.Id;
        testAAA.Account_Development_Plan__c = testADP.Id;
        insert testAAA;
        ADP_AccountChartsController ex = new ADP_AccountChartsController(new ApexPages.StandardController(testADP));
        
    }
    static testmethod void testADP_AssociatedAccountController(){
        User testUser = UTIL_TestUtil.createUser();
        Id userId = UserInfo.getUserId();
        system.runAs(testUser) {
            Account testAccount2 = UTIL_TestUtil.createAccount();
            Account_Development_Plan__c testADP = new Account_Development_Plan__c(Name= 'Sample');
            insert testADP;
            test.startTest();
            PageReference myVfPage = Page.ADP_AssociatedAccountPage;
            Test.setCurrentPage(myVfPage);
            ADP_AssociatedAccountController ex = new ADP_AssociatedAccountController(new ApexPages.StandardController(testADP));
            ex.addNewRow();
            ex.editAAA.Account__c = testAccount2.Id;
            ex.saveEdit();
            ApexPages.currentPage().getParameters().put('editid',ex.aaaList[0].Id); 
            ex.editOneAAA();
            ex.cancelEdit();
            ApexPages.currentPage().getParameters().put('delid',ex.aaaList[0].Id); 
            ex.delAAA();
            test.stopTest();
        }
    }
    
    static testMethod void testKDMI_CustomContactLookupController() {
        Account testAccount = UTIL_TestUtil.createAccount();
        Contact testContact = UTIL_TestUtil.createContact(testAccount.Id);
        Account_Development_Plan__c testADP = new Account_Development_Plan__c(Name= 'Sample');
        insert testADP;
        ADP_Associated_Accounts__c testAAA = new ADP_Associated_Accounts__c();
        testAAA.Account__c = testAccount.Id;
        testAAA.Account_Development_Plan__c = testADP.Id;
        insert testAAA;
        ApexPages.currentPage().getParameters().put('lksrch',testContact.Name);
        ApexPages.currentPage().getParameters().put('accDevPlanId',testADP.Id);
        KDMI_CustomContactLookupController testCntrl = new KDMI_CustomContactLookupController();
        testCntrl.getFormTag();
        testCntrl.getTextBox();
        
        
    }
    
    static testMethod void testADP_PandoraSalesTeamController() {
        Account testAccount2 = UTIL_TestUtil.createAccount();
        Account_Development_Plan__c testADP = new Account_Development_Plan__c(Name= 'Sample');
        insert testADP;
        test.startTest();
        PageReference myVfPage = Page.ADP_AssociatedAccountPage;
        Test.setCurrentPage(myVfPage);
        ADP_PandoraSalesTeamController ex = new ADP_PandoraSalesTeamController(new ApexPages.StandardController(testADP));
        ex.addNewRow();
        ex.saveEdit();
        ApexPages.currentPage().getParameters().put('editid',ex.pstList[0].Id); 
        ex.editOnePST();
        ex.cancelEdit();
        ApexPages.currentPage().getParameters().put('delid',ex.pstList[0].Id); 
        ex.delPST();
        test.stopTest();
    }
}