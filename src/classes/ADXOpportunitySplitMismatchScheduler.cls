public class ADXOpportunitySplitMismatchScheduler implements Schedulable {
    
    public void execute(SchedulableContext sc) {
        adxOpportunitySplitAmountMismatchBatch ops = new adxOpportunitySplitAmountMismatchBatch();
        Database.executeBatch(ops,125);
    }
}