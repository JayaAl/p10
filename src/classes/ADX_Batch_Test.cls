@isTest(SeeAllData=false)
public class ADX_Batch_Test{

    
    public static testMethod void adxOpportunitySplitAmountMismatchScheduler_Test(){
        User user1 = [SELECT ID from user where Profile.Name = 'System Administrator' and isActive=true limit 1];
        Account a = new Account(Name = 'Test Account', Type='Advertiser');
        insert a;
        
        Contact conObj = UTIL_TestUtil.generateContact(a.id);
        insert conObj;
        
        Schema.DescribeSObjectResult cfrSchema = Schema.SObjectType.Opportunity; 
        Map<String,Schema.RecordTypeInfo> AccountRecordTypeInfo = cfrSchema.getRecordTypeInfosByName(); 
        Id rtId = AccountRecordTypeInfo .get('Opportunity - Programmatic').getRecordTypeId();
        
        Opportunity o1 = new Opportunity(Name = 'Test Opportunity1', AccountId = a.Id, Primary_Billing_Contact__c =conObj.Id, StageName = 'Negotiation', CloseDate = System.Today()+10,Confirm_direct_relationship__c=true,
                                            Deal_Id__c = '1234',Industry_Sub_Category__c='Political Campaign Marketing',Type='Advertiser');
                                            
        o1.RecordTypeId = rtId ;
        insert o1;
        
        
        Opportunity_Split__c optSplit = new Opportunity_Split__c(Split__c = 100,
        Salesperson__c = o1.OwnerId,
        Opportunity__c = o1.Id          
        );
        insert optSplit;
         
        
        
        Product2 prod = new Product2(Name = 'Web',Auto_Schedule__c=true, CanUseRevenueSchedule=true,
            Family = 'Hardware');
        insert prod;
        
        Product2 prod1 = new Product2(Name = 'Mobile',Auto_Schedule__c=true,CanUseRevenueSchedule=true,
            Family = 'Software');
        insert prod1;
        
        // Get standard price book ID.
        // This is available irrespective of the state of SeeAllData.
        Id standard = Test.getStandardPricebookId();
     
        PricebookEntry standardPrice = new PricebookEntry(
            Pricebook2Id = standard, Product2Id = prod.Id,
            UnitPrice = 10000, IsActive = true);
        insert standardPrice;
        
        // Create a custom price book
        Pricebook2 customPB = new Pricebook2(Name='Custom Pricebook', isActive=true);
        insert customPB;
        
        // 2. Insert a price book entry with a custom price.
        PricebookEntry customPrice = new PricebookEntry(
            Pricebook2Id = standard, Product2Id = prod1.Id,
            UnitPrice = 12000, IsActive = true);
        insert customPrice;
        
        PriceBookEntry pbe = [select id from pricebookentry where name = 'Web' limit 1];
        system.debug(logginglevel.info, '===========================================================================entry is :' + pbe.id);
        OPT_PROD_STATIC_HELPER.runForProds = true;
        List<OpportunityLineItem> oplisToUpload = new List<OpportunityLineItem>();
        for (Integer i=1;i<3;i++)
        {
            String[] inputvalues = new String[]{};
            
            OpportunityLineItem opli = new OpportunityLineItem();
            opli.Team_Type__c = 'Performance'; 
            opli.OpportunityId = o1.Id;
            opli.ServiceDate = date.parse('8/16/2016'); 
            opli.End_Date__c = date.parse('8/23/2016');
            opli.Rate__c = 125;
            opli.Impression_s__c = 1000;
            opli.Clicks__c = 10000;
            opli.Performance_Type__c = 'Web Display - Top';
            opli.Cost_Type__c = 'CPA'; 
            opli.Age__c = '20';
            opli.Gender__c = 'M';
            opli.Zip__c = 94538; 
            opli.MSA__c =  '';           
            opli.DMA__c = ''; 
            opli.Add_l_Targets__c = 'test';
            opli.Frequency_Cap__c = 'test';
            opli.Conversions__c = 1000;
            opli.UnitPrice = 1000; 
            opli.Quantity = 1;
            opli.PricebookEntryId = pbe.id;
            opli.Duration__c = 1;
            
            oplisToUpload.add(opli); 
            
         }
         
         insert oplisToUpload;
         
         
         /*List<OpportunityLineItemSchedule> lstSchedules = new List<OpportunityLineItemSchedule>();
         OpportunityLineItemSchedule schedule1 = new OpportunityLineItemSchedule(OpportunityLineItemId =oplisToUpload[0].Id,Revenue= 1000,ScheduleDate = date.parse('7/23/2016'),Type='Revenue');
         lstSchedules.Add(schedule1);
         
         OpportunityLineItemSchedule schedule2 = new OpportunityLineItemSchedule(OpportunityLineItemId =oplisToUpload[1].Id,Revenue= 1000,ScheduleDate = date.parse('7/23/2016'),Type='Revenue');
         lstSchedules.Add(schedule2);
         */
         
         //system.debug('THIS IS IT' + schedulesEnabledForOrg());
         //insert lstSchedules; 
         
         List<Split_Detail__c> lstSplitDetails = new  List<Split_Detail__c>();
         lstSplitDetails = [select id from Split_Detail__c where Opportunity_Split__r.Opportunity__c =:o1.Id];
         
        if(lstSplitDetails.size()>0)
            delete lstSplitDetails;
        
         
      
          List<OpportunityLineItem> lstOppli = [SELECT Id, HasSchedule FROM OpportunityLineItem WHERE OpportunityId =: o1.Id ];
          system.debug(logginglevel.info, 'lstOppli?' + lstOppli);
          boolean hasSchedules = false;
          for(OpportunityLineItem ol:lstOppli)
          {
              system.debug(logginglevel.info, 'ol.HasSchedule?' + ol.HasSchedule);
              if(ol.HasSchedule){
                  hasSchedules = true;
                  break;    
              }
          }
          
          if(lstOppli.size()>0 && hasSchedules==true)
          {
           Test.startTest();
            ADXOpportunitySplitMismatchScheduler opsscheduler = new ADXOpportunitySplitMismatchScheduler();
        
            System.schedule('Test', '0 0 * ? * * *', opsscheduler);
            
           
            Test.stopTest();
          }
    }
    
     public static testMethod void adxOpportunitySplitAmountMismatchBatch_Test(){  //adxIntegrationBatchable_Test() {
        system.debug(logginglevel.info, 'INSIDE THIS');
        
        User user1 = [SELECT ID from user where Profile.Name = 'System Administrator' and isActive=true limit 1];
        Account a = new Account(Name = 'Test Account', Type='Advertiser');
        insert a;
        
        Contact conObj = UTIL_TestUtil.generateContact(a.id);
        insert conObj;
        
        Schema.DescribeSObjectResult cfrSchema = Schema.SObjectType.Opportunity; 
        Map<String,Schema.RecordTypeInfo> AccountRecordTypeInfo = cfrSchema.getRecordTypeInfosByName(); 
        Id rtId = AccountRecordTypeInfo .get('Opportunity - Programmatic').getRecordTypeId();
        
        Opportunity o1 = new Opportunity(Name = 'Test Opportunity1', AccountId = a.Id, Primary_Billing_Contact__c =conObj.Id, StageName = 'Negotiation', CloseDate = System.Today()+10,Confirm_direct_relationship__c=true,
                                            Deal_Id__c = '1234',Industry_Sub_Category__c='Political Campaign Marketing',Type='Advertiser');
                                            
        o1.RecordTypeId = rtId ;
        insert o1;
        
        
        Opportunity_Split__c optSplit = new Opportunity_Split__c(Split__c = 100,
        Salesperson__c = o1.OwnerId,
        Opportunity__c = o1.Id          
        );
        insert optSplit;
         
        
        
        Product2 prod = new Product2(Name = 'Web',Auto_Schedule__c=true, CanUseRevenueSchedule=true,
            Family = 'Hardware');
        insert prod;
        
        Product2 prod1 = new Product2(Name = 'Mobile',Auto_Schedule__c=true,CanUseRevenueSchedule=true,
            Family = 'Software');
        insert prod1;
        
        // Get standard price book ID.
        // This is available irrespective of the state of SeeAllData.
        Id standard = Test.getStandardPricebookId();
     
        PricebookEntry standardPrice = new PricebookEntry(
            Pricebook2Id = standard, Product2Id = prod.Id,
            UnitPrice = 10000, IsActive = true);
        insert standardPrice;
        
        // Create a custom price book
        Pricebook2 customPB = new Pricebook2(Name='Custom Pricebook', isActive=true);
        insert customPB;
        
        // 2. Insert a price book entry with a custom price.
        PricebookEntry customPrice = new PricebookEntry(
            Pricebook2Id = standard, Product2Id = prod1.Id,
            UnitPrice = 12000, IsActive = true);
        insert customPrice;
        
        PriceBookEntry pbe = [select id from pricebookentry where name = 'Web' limit 1];
        system.debug(logginglevel.info, '===========================================================================entry is :' + pbe.id);
        OPT_PROD_STATIC_HELPER.runForProds = true;
        List<OpportunityLineItem> oplisToUpload = new List<OpportunityLineItem>();
        for (Integer i=1;i<3;i++)
        {
            String[] inputvalues = new String[]{};
            
            OpportunityLineItem opli = new OpportunityLineItem();
            opli.Team_Type__c = 'Performance'; 
            opli.OpportunityId = o1.Id;
            opli.ServiceDate = date.parse('8/16/2016'); 
            opli.End_Date__c = date.parse('8/23/2016');
            opli.Rate__c = 125;
            opli.Impression_s__c = 1000;
            opli.Clicks__c = 10000;
            opli.Performance_Type__c = 'Web Display - Top';
            opli.Cost_Type__c = 'CPA'; 
            opli.Age__c = '20';
            opli.Gender__c = 'M';
            opli.Zip__c = 94538; 
            opli.MSA__c =  '';           
            opli.DMA__c = ''; 
            opli.Add_l_Targets__c = 'test';
            opli.Frequency_Cap__c = 'test';
            opli.Conversions__c = 1000;
            opli.UnitPrice = 1000; 
            opli.Quantity = 1;
            opli.PricebookEntryId = pbe.id;
            opli.Duration__c = 1;
            
            oplisToUpload.add(opli); 
            
         }
         
         insert oplisToUpload;
         
         List<Split_Detail__c> lstSplitDetails = new  List<Split_Detail__c>();
         lstSplitDetails = [select id from Split_Detail__c where Opportunity_Split__r.Opportunity__c =:o1.Id];
         
         if(lstSplitDetails.size()>0)
            delete lstSplitDetails;
        
         
      
          List<OpportunityLineItem> lstOppli = [SELECT Id, HasSchedule FROM OpportunityLineItem WHERE OpportunityId =: o1.Id ];
          system.debug(logginglevel.info, 'lstOppli?' + lstOppli);
          boolean hasSchedules = false;
          for(OpportunityLineItem ol:lstOppli)
          {
              system.debug(logginglevel.info, 'ol.HasSchedule?' + ol.HasSchedule);
              if(ol.HasSchedule){
                  hasSchedules = true;
                  break;    
              }
          }
          
          if(lstOppli.size()>0 && hasSchedules==true)
          {
              adxOpportunitySplitAmountMismatchBatch adxOpp = new adxOpportunitySplitAmountMismatchBatch();
              database.executebatch(adxOpp);
          }
            
        
    }
    
    
    public static testMethod void adxIntegrationBatchable_Test(){ 
        system.debug(logginglevel.info, 'INSIDE THIS');
        
        User user1 = [SELECT ID from user where Profile.Name = 'System Administrator' and isActive=true limit 1];
        Account a = new Account(Name = 'Test Account', Type='Advertiser');
        insert a;
        
        Contact conObj = UTIL_TestUtil.generateContact(a.id);
        insert conObj;
        set<Id> opptyId = new set<Id>();
        Schema.DescribeSObjectResult cfrSchema = Schema.SObjectType.Opportunity; 
        Map<String,Schema.RecordTypeInfo> AccountRecordTypeInfo = cfrSchema.getRecordTypeInfosByName(); 
        Id rtId = AccountRecordTypeInfo .get('Opportunity - Programmatic').getRecordTypeId();
        
        Opportunity o1 = new Opportunity(Name = 'Test Opportunity1', AccountId = a.Id, Primary_Billing_Contact__c =conObj.Id, StageName = 'Negotiation', CloseDate = System.Today()+10,Confirm_direct_relationship__c=true,
                                            Deal_Id__c = '1234',Industry_Sub_Category__c='Political Campaign Marketing',Type='Advertiser');
                                            
        o1.RecordTypeId = rtId ;
        insert o1;
        opptyId.Add(o1.Id);
        
        Opportunity_Split__c optSplit = new Opportunity_Split__c(Split__c = 100,
        Salesperson__c = o1.OwnerId,
        Opportunity__c = o1.Id          
        );
        insert optSplit;
         
        
        
        Product2 prod = new Product2(Name = 'Web',Auto_Schedule__c=true, CanUseRevenueSchedule=true,
            Family = 'Hardware');
        insert prod;
        
        Product2 prod1 = new Product2(Name = 'Mobile',Auto_Schedule__c=true,CanUseRevenueSchedule=true,
            Family = 'Software');
        insert prod1;
        
        // Get standard price book ID.
        // This is available irrespective of the state of SeeAllData.
        Id standard = Test.getStandardPricebookId();
     
        PricebookEntry standardPrice = new PricebookEntry(
            Pricebook2Id = standard, Product2Id = prod.Id,
            UnitPrice = 10000, IsActive = true);
        insert standardPrice;
        
        // Create a custom price book
        Pricebook2 customPB = new Pricebook2(Name='Custom Pricebook', isActive=true);
        insert customPB;
        
        // 2. Insert a price book entry with a custom price.
        PricebookEntry customPrice = new PricebookEntry(
            Pricebook2Id = standard, Product2Id = prod1.Id,
            UnitPrice = 12000, IsActive = true);
        insert customPrice;
        
        PriceBookEntry pbe = [select id from pricebookentry where name = 'Web' limit 1];
        system.debug(logginglevel.info, '===========================================================================entry is :' + pbe.id);
        OPT_PROD_STATIC_HELPER.runForProds = true;
        List<OpportunityLineItem> oplisToUpload = new List<OpportunityLineItem>();
        for (Integer i=1;i<3;i++)
        {
            String[] inputvalues = new String[]{};
            
            OpportunityLineItem opli = new OpportunityLineItem();
            opli.Team_Type__c = 'Performance'; 
            opli.OpportunityId = o1.Id;
            opli.ServiceDate = date.parse('8/16/2016'); 
            opli.End_Date__c = date.parse('8/23/2016');
            opli.Rate__c = 125;
            opli.Impression_s__c = 1000;
            opli.Clicks__c = 10000;
            opli.Performance_Type__c = 'Web Display - Top';
            opli.Cost_Type__c = 'CPA'; 
            opli.Age__c = '20';
            opli.Gender__c = 'M';
            opli.Zip__c = 94538; 
            opli.MSA__c =  '';           
            opli.DMA__c = ''; 
            opli.Add_l_Targets__c = 'test';
            opli.Frequency_Cap__c = 'test';
            opli.Conversions__c = 1000;
            opli.UnitPrice = 1000; 
            opli.Quantity = 1;
            opli.PricebookEntryId = pbe.id;
            opli.Duration__c = 1;
            
            oplisToUpload.add(opli); 
            
         }
         
         insert oplisToUpload;
         
         List<Split_Detail__c> lstSplitDetails = new  List<Split_Detail__c>();
         lstSplitDetails = [select id from Split_Detail__c where Opportunity_Split__r.Opportunity__c =:o1.Id];
         
        if(lstSplitDetails.size()>0)
            delete lstSplitDetails;
        
         
      
          List<OpportunityLineItem> lstOppli = [SELECT Id, HasSchedule FROM OpportunityLineItem WHERE OpportunityId =: o1.Id ];
          system.debug(logginglevel.info, 'lstOppli?' + lstOppli);
          boolean hasSchedules = false;
          for(OpportunityLineItem ol:lstOppli)
          {
              system.debug(logginglevel.info, 'ol.HasSchedule?' + ol.HasSchedule);
              if(ol.HasSchedule){
                  hasSchedules = true;
                  break;    
              }
          }
          
          if(lstOppli.size()>0 && hasSchedules==true)
          {
              adxIntegrationBatchable adxIB = new adxIntegrationBatchable(opptyId);
              database.executebatch(adxIB,125);
          }
            
        
    }
    
    
    public static testMethod void adxDeleteSplitDetailBatchable_Test(){  //adxIntegrationBatchable_Test() {
        system.debug(logginglevel.info, 'INSIDE THIS');
        
        User user1 = [SELECT ID from user where Profile.Name = 'System Administrator' and isActive=true limit 1];
        Account a = new Account(Name = 'Test Account', Type='Advertiser');
        insert a;
        
        Contact conObj = UTIL_TestUtil.generateContact(a.id);
        insert conObj;
        set<Id> opptyId = new set<Id>();
        Schema.DescribeSObjectResult cfrSchema = Schema.SObjectType.Opportunity; 
        Map<String,Schema.RecordTypeInfo> AccountRecordTypeInfo = cfrSchema.getRecordTypeInfosByName(); 
        Id rtId = AccountRecordTypeInfo .get('Opportunity - Programmatic').getRecordTypeId();
        
        Opportunity o1 = new Opportunity(Name = 'Test Opportunity1', AccountId = a.Id, Primary_Billing_Contact__c =conObj.Id, StageName = 'Negotiation', CloseDate = System.Today()+10,Confirm_direct_relationship__c=true,
                                            Deal_Id__c = '1234',Industry_Sub_Category__c='Political Campaign Marketing',Type='Advertiser');
                                            
        o1.RecordTypeId = rtId ;
        insert o1;
        opptyId.Add(o1.Id);
        
        Opportunity_Split__c optSplit = new Opportunity_Split__c(Split__c = 100,
        Salesperson__c = o1.OwnerId,
        Opportunity__c = o1.Id          
        );
        insert optSplit;
         
        
        
        Product2 prod = new Product2(Name = 'Web',Auto_Schedule__c=true, CanUseRevenueSchedule=true,
            Family = 'Hardware');
        insert prod;
        
        Product2 prod1 = new Product2(Name = 'Mobile',Auto_Schedule__c=true,CanUseRevenueSchedule=true,
            Family = 'Software');
        insert prod1;
        
        // Get standard price book ID.
        // This is available irrespective of the state of SeeAllData.
        Id standard = Test.getStandardPricebookId();
     
        PricebookEntry standardPrice = new PricebookEntry(
            Pricebook2Id = standard, Product2Id = prod.Id,
            UnitPrice = 10000, IsActive = true);
        insert standardPrice;
        
        // Create a custom price book
        Pricebook2 customPB = new Pricebook2(Name='Custom Pricebook', isActive=true);
        insert customPB;
        
        // 2. Insert a price book entry with a custom price.
        PricebookEntry customPrice = new PricebookEntry(
            Pricebook2Id = standard, Product2Id = prod1.Id,
            UnitPrice = 12000, IsActive = true);
        insert customPrice;
        
        PriceBookEntry pbe = [select id from pricebookentry where name = 'Web' limit 1];
        system.debug(logginglevel.info, '===========================================================================entry is :' + pbe.id);
        OPT_PROD_STATIC_HELPER.runForProds = true;
        List<OpportunityLineItem> oplisToUpload = new List<OpportunityLineItem>();
        for (Integer i=1;i<3;i++)
        {
            String[] inputvalues = new String[]{};
            
            OpportunityLineItem opli = new OpportunityLineItem();
            opli.Team_Type__c = 'Performance'; 
            opli.OpportunityId = o1.Id;
            opli.ServiceDate = date.parse('8/16/2016'); 
            opli.End_Date__c = date.parse('8/23/2016');
            opli.Rate__c = 125;
            opli.Impression_s__c = 1000;
            opli.Clicks__c = 10000;
            opli.Performance_Type__c = 'Web Display - Top';
            opli.Cost_Type__c = 'CPA'; 
            opli.Age__c = '20';
            opli.Gender__c = 'M';
            opli.Zip__c = 94538; 
            opli.MSA__c =  '';           
            opli.DMA__c = ''; 
            opli.Add_l_Targets__c = 'test';
            opli.Frequency_Cap__c = 'test';
            opli.Conversions__c = 1000;
            opli.UnitPrice = 1000; 
            opli.Quantity = 1;
            opli.PricebookEntryId = pbe.id;
            opli.Duration__c = 1;
            
            oplisToUpload.add(opli); 
            
         }
         
         insert oplisToUpload;
         
         
         /*List<OpportunityLineItemSchedule> lstSchedules = new List<OpportunityLineItemSchedule>();
         OpportunityLineItemSchedule schedule1 = new OpportunityLineItemSchedule(OpportunityLineItemId =oplisToUpload[0].Id,Revenue= 1000,ScheduleDate = date.parse('7/23/2016'),Type='Revenue');
         lstSchedules.Add(schedule1);
         
         OpportunityLineItemSchedule schedule2 = new OpportunityLineItemSchedule(OpportunityLineItemId =oplisToUpload[1].Id,Revenue= 1000,ScheduleDate = date.parse('7/23/2016'),Type='Revenue');
         lstSchedules.Add(schedule2);
         */
         
         //system.debug('THIS IS IT' + schedulesEnabledForOrg());
         //insert lstSchedules; 
         
         List<Split_Detail__c> lstSplitDetails = new  List<Split_Detail__c>();
         lstSplitDetails = [select id from Split_Detail__c where Opportunity_Split__r.Opportunity__c =:o1.Id];
         
        //if(lstSplitDetails.size()>0)
        //    delete lstSplitDetails;
        
         
      
          List<OpportunityLineItem> lstOppli = [SELECT Id, HasSchedule FROM OpportunityLineItem WHERE OpportunityId =: o1.Id ];
          system.debug(logginglevel.info, 'lstOppli?' + lstOppli);
          boolean hasSchedules = false;
          for(OpportunityLineItem ol:lstOppli)
          {
              system.debug(logginglevel.info, 'ol.HasSchedule?' + ol.HasSchedule);
              if(ol.HasSchedule){
                  hasSchedules = true;
                  break;    
              }
          }
          
          if(lstOppli.size()>0 && hasSchedules==true)
          {
              //adxIntegrationBatchable adxIB = new adxIntegrationBatchable(opptyId);
              //database.executebatch(adxIB,125);
              
              adxDeleteSplitDetailBatchable  adxDel = new adxDeleteSplitDetailBatchable(opptyId);
              database.executebatch(adxDel,125);
              
              //adxInsertSplitDetailsBatchable  adxIns = new adxInsertSplitDetailsBatchable();
              //database.executebatch(adxIns,125);
          }
            
        
    }
    
    public static testMethod void adxInsertSplit_BatchTest()
    {
    
        system.debug(logginglevel.info, 'INSIDE THIS');
        
        User user1 = [SELECT ID from user where Profile.Name = 'System Administrator' and isActive=true limit 1];
        Account a = new Account(Name = 'Test Account', Type='Advertiser');
        insert a;
        
        Contact conObj = UTIL_TestUtil.generateContact(a.id);
        insert conObj;
        set<Id> opptyId = new set<Id>();
        Schema.DescribeSObjectResult cfrSchema = Schema.SObjectType.Opportunity; 
        Map<String,Schema.RecordTypeInfo> AccountRecordTypeInfo = cfrSchema.getRecordTypeInfosByName(); 
        Id rtId = AccountRecordTypeInfo .get('Opportunity - Programmatic').getRecordTypeId();
        
        Opportunity o1 = new Opportunity(Name = 'Test Opportunity1', AccountId = a.Id, Primary_Billing_Contact__c =conObj.Id, StageName = 'Negotiation', CloseDate = System.Today()+10,Confirm_direct_relationship__c=true,
                                            Deal_Id__c = '1234',Industry_Sub_Category__c='Political Campaign Marketing',Type='Advertiser');
                                            
        o1.RecordTypeId = rtId ;
        insert o1;
        opptyId.Add(o1.Id);
        
        Opportunity_Split__c optSplit = new Opportunity_Split__c(Split__c = 100,
        Salesperson__c = o1.OwnerId,
        Opportunity__c = o1.Id          
        );
        insert optSplit;
         
        
        
        Product2 prod = new Product2(Name = 'Web',Auto_Schedule__c=true, CanUseRevenueSchedule=true,
            Family = 'Hardware');
        insert prod;
        
        Product2 prod1 = new Product2(Name = 'Mobile',Auto_Schedule__c=true,CanUseRevenueSchedule=true,
            Family = 'Software');
        insert prod1;
        
        // Get standard price book ID.
        // This is available irrespective of the state of SeeAllData.
        Id standard = Test.getStandardPricebookId();
     
        PricebookEntry standardPrice = new PricebookEntry(
            Pricebook2Id = standard, Product2Id = prod.Id,
            UnitPrice = 10000, IsActive = true);
        insert standardPrice;
        
        // Create a custom price book
        Pricebook2 customPB = new Pricebook2(Name='Custom Pricebook', isActive=true);
        insert customPB;
        
        // 2. Insert a price book entry with a custom price.
        PricebookEntry customPrice = new PricebookEntry(
            Pricebook2Id = standard, Product2Id = prod1.Id,
            UnitPrice = 12000, IsActive = true);
        insert customPrice;
        
        PriceBookEntry pbe = [select id from pricebookentry where name = 'Web' limit 1];
        system.debug(logginglevel.info, '===========================================================================entry is :' + pbe.id);
        OPT_PROD_STATIC_HELPER.runForProds = true;
        List<OpportunityLineItem> oplisToUpload = new List<OpportunityLineItem>();
        for (Integer i=1;i<3;i++)
        {
            String[] inputvalues = new String[]{};
            
            OpportunityLineItem opli = new OpportunityLineItem();
            opli.Team_Type__c = 'Performance'; 
            opli.OpportunityId = o1.Id;
            opli.ServiceDate = date.parse('8/16/2016'); 
            opli.End_Date__c = date.parse('8/23/2016');
            opli.Rate__c = 125;
            opli.Impression_s__c = 1000;
            opli.Clicks__c = 10000;
            opli.Performance_Type__c = 'Web Display - Top';
            opli.Cost_Type__c = 'CPA'; 
            opli.Age__c = '20';
            opli.Gender__c = 'M';
            opli.Zip__c = 94538; 
            opli.MSA__c =  '';           
            opli.DMA__c = ''; 
            opli.Add_l_Targets__c = 'test';
            opli.Frequency_Cap__c = 'test';
            opli.Conversions__c = 1000;
            opli.UnitPrice = 1000; 
            opli.Quantity = 1;
            opli.PricebookEntryId = pbe.id;
            opli.Duration__c = 1;
            
            oplisToUpload.add(opli); 
            
         }
         
         insert oplisToUpload;
         List<Split_Detail_Staging__c> lstSplitDetailsStaging = new  List<Split_Detail_Staging__c>();
         Split_Detail_Staging__c SD = new Split_Detail_Staging__c();
            SD.Opportunity_Split__c = optSplit.Id;
            SD.Date__c = system.Today();
            SD.Salesperson_User__c = user1.Id;
            SD.Amount__c = 10000;
            SD.Name                = '000001';
            SD.Product__c          = oplisToUpload[0].PricebookEntry.Product2Id;
            SD.Banner__c           = oplisToUpload[0].Banner__c;
            SD.Banner_Type__c      = oplisToUpload[0].Banner_Type__c;
            SD.Cost_Type__c        = oplisToUpload[0].Cost_Type__c;
            SD.Medium__c           = oplisToUpload[0].Medium__c;
            SD.Offering_Type__c    = oplisToUpload[0].Offering_Type__c;
            SD.Platform__c         = oplisToUpload[0].Platform__c;
            SD.Size__c             = oplisToUpload[0].Size__c;
            SD.Sub_Platform__c     = oplisToUpload[0].Sub_Platform__c;
            SD.Takeover__c         = oplisToUpload[0].Takeover__c;
            String fixedOffer = (oplisToUpload[0].Offering_Type__c==null||oplisToUpload[0].Offering_Type__c=='')?'[None]':oplisToUpload[0].Offering_Type__c;
            String fixedMedium = (oplisToUpload[0].Medium__c==null||oplisToUpload[0].Medium__c=='')?'[None]':oplisToUpload[0].Medium__c;
            
            SD.Offering_Type_Medium__c = fixedOffer+' / '+fixedMedium;
            lstSplitDetailsStaging.add(SD);   
            
            insert lstSplitDetailsStaging;
         
            adxInsertSplitDetailsBatchable  ins = new adxInsertSplitDetailsBatchable();
            database.executeBatch(ins);
    
    }

}