public with sharing class AD_OPS_AddTrafficProducts {

	/* Constants, Properties, & Variables */

	// Public Constants
	
	public static final Integer PRODUCT_NUMBER = 20;
	
	// Public Properties
	
	public Integer displayedProducts {
		get {
			Integer value = 1;
			for(MyProduct product : products) {
				if(product.recordTypeId != startOption) {
					value++;
				}
			}
			return value;
		}
	}
	
	public List<MyFieldSet> fieldSets { 
		get {
			return fieldListsByRtIdMap.values();
		}
	}
	
	public List<SelectOption> recordTypeOptions {
		get {
			return AD_OPS_AddTrafficProductsFieldLists.recordTypeOptions;	
		}
	}
	
	public String startOption {
		get {
			return AD_OPS_AddTrafficProductsFieldLists.START_OPTION;
		}
	}
	
	public List<String> allFields {
		get {
			if(allFields == null) {
				allFields = new List<String>();
				allFields.addAll(AD_OPS_AddTrafficProductsFieldLists.allFields);
			}
			return allFields;
		}
		private set;
	}
	
	// Public Variables
	
	public List <MyProduct> products { get; set; }
	public Integer removedProductNum { get; set; }
	
	// Private Properties
	
	private static Map<String, String> baseFieldErrorsMap {
		get {
			if(baseFieldErrorsMap == null) {
				baseFieldErrorsMap = new Map<String, String>();
				for(String field : AD_OPS_AddTrafficProductsFieldLists.allFields) {
					baseFieldErrorsMap.put(field, '');
				}
			}
			return baseFieldErrorsMap;
		}
		private set;
	}
	
	private Map<Id, MyFieldSet> fieldListsByRtIdMap {
		get {
			if(fieldListsByRtIdMap == null) {
				fieldListsByRtIdMap = AD_OPS_AddTrafficProductsFieldLists.fieldSetsByRtIdMap;
			}
			return fieldListsByRtIdMap; 
		}
		private set;
	}
	
	// Private Variables
	
	private ApexPages.StandardController controller;
	
	/* Constructor */
	
	public AD_OPS_AddTrafficProducts(ApexPages.StandardController controller) {
		this.controller = controller;
		generateProducts();		
	}
	
	/* Action Methods */
	
	public void removeProduct() {
		products.remove(removedProductNum);
		for(Integer i = 0; i < products.size(); i++) {
			products[i].num = i;
		}
	}
		
	public PageReference save() {
		
		resetFieldErrors();
		
		Boolean foundError = false;
		for(MyProduct product : products) {
			if(product.recordTypeId == null) {
				product.recordTypeId = startOption;
			} else if(product.recordTypeId != startOption){
				if(!isValid(product)) {
					foundError = true;
				}
			}
		}
		
		if(foundError) {
			ApexPages.addMessage(new ApexPages.Message(
				  ApexPages.Severity.ERROR
				, 'Required field(s) missing.  Please complete enter all marked fields.'
			));
		} else {
			List<Traffic_Product__c> newProducts = new List<Traffic_Product__c>();
			for(MyProduct product : products) {
				if(product.recordTypeId != startOption) {
					Traffic_Product__c newProduct = product.record;
					newProduct.recordTypeId = product.recordTypeId;
					newProducts.add(newProduct); 
				}
			}
			try {
				insert newProducts;
				return new ApexPages.StandardController((Case) controller.getRecord()).view();
			} catch(DMLException e) {
				ApexPages.addMessage(new ApexPages.Message(
					  ApexPages.Severity.ERROR
					, 'Unexpected error when saving traffic products.  Please contact your administrator.  Exception: ' + e
				));
			}
		}
		
		return null;
	}
	
	/* Support Methods */
	
	// clear error messages from prior submits
	private void resetFieldErrors() {
		for(MyProduct product : products) {
			product.fieldErrors = baseFieldErrorsMap.clone();
		}
	}
	
	// return true if product has all required field values entered
	private Boolean isValid(MyProduct product) {
		Boolean errorFound = false;
		MyFieldSet fieldSet = fieldListsByRtIdMap.get(product.recordTypeId);
		for(MyField field : fieldSet.fieldList) {
			String fieldName = field.name;
			if(field.required && product.record.get(fieldName) == null) {
				errorFound = true;
				product.fieldErrors.put(fieldName,'A value is required');		
			}
		}
		return !errorFound;
	}
	
	// generate the intial list of products
	private void generateProducts() {
		Case record = (Case) controller.getRecord();
		products = new List<MyProduct>();
		for(Integer i = 0; i < PRODUCT_NUMBER; i++) {
			products.add(new MyProduct(i, record));
		}
	}

	/* Inner Classes */
	
	public class MyField {
		public String label { get; private set; }
		public Boolean required { get; private set; }
		public String name { get; private set; }
		public String type { get; private set; }
		
		public MyField(String fieldAPIName, Boolean required) {
			this.required = required;
			name = fieldApiName;
			Schema.DescribeFieldResult fieldDescribe = UTIL_SchemaHelper.getFieldDescribe('Traffic_Product__c', fieldAPIName); 
			label = fieldDescribe.getLabel();
			type = fieldDescribe.getType().name();
		}
	}
	
	public class MyFieldSet {
		public List<MyField> fieldList { get; set; }
		public Id recordTypeId { get; set; }
		public String recordTypeLabel { get; set; }
		public String recordTypeName { get; set; }
		
		public MyFieldSet() {
			fieldList = new List<MyField>();
		}
	}
	
	public class MyProduct {
		public Traffic_Product__c record { get; set; }
		public Map<String, String> fieldErrors { get; set; }
		public String recordTypeId { get; set; }
		public Integer num { get; private set; }
		
		public MyProduct(Integer num, Case parent) {
			fieldErrors = baseFieldErrorsMap.clone();
			recordTypeId = AD_OPS_AddTrafficProductsFieldLists.START_OPTION;
			this.num = num;
			record = new Traffic_Product__c(
				  traffic_case__c = parent.id
				, product_due_date__c = parent.due_date__c
			);
		}
	}

}