/*
Developer: Ralph Callaway <ralph.w.callaway@gmail.com>
Description:
	Builds and stores the list of fields and whether they required for a particular traffic
	product record type.  Build list of select options for available record types for user.
	Merge these lists so they we don't show select options for record types that don't have
	a defined fields lists, or field lists that don't correspond to a defined record type.
Update:
	As of summer '12, field sets are available in apex, meaning the traffic products field
	sets no longer need to be stored as xml with an intermediary controller. As the existing
	versions works fine we've opted not to update to use fieldsets directly.
*/
public class AD_OPS_AddTrafficProductsFieldLists {

	/* Constants, Properties, and Variables */
	
	// Public Properties 
	
	public static List<SelectOption> recordTypeOptions {
		get {
			if(recordTypeOptions == null) {
				init();
			}
			return recordTypeOptions;
		}
		private set;
	}
	
	public static Map<Id, AD_OPS_AddTrafficProducts.MyFieldSet> fieldSetsByRtIdMap {
		get {
			if(fieldSetsByRtIdMap == null) {
				init();
			}
			return fieldSetsByRtIdMap;
		}
		private set;
	}
	
	public static Set<String> allFields {
		get {
			if(allFields == null) {
				init();
			}
			return allFields;
		}
		private set;
	}
	
	// Public Constants
	
	public static final String FIELD_SET_TAG = 'fieldset';
	public static final String FIELD_TAG = 'field';
	public static final String REQUIRED_ATTR = 'required';
	public static final String RT_DEV_NAME_ATTR = 'rtDevName';
	public static final String START_OPTION = 'Select Product Type';
	public static final String TRAFFIC_PRODUCT_OBJ_NAME = 'traffic_product__c';
	public static final String TRUE_VAL = 'true';
	
	// Private Variables
	
	private static Map<String, Id> recordTypeDevNameToIdMap = new Map<String, Id>();
	private static Map<Id, String> recordTypeIdToLabelMap = new Map<Id, String>();
	
	// Private Constants
	
	private static final String CONFIG_FILE_NAME = 'AD_OPS_TrafficProductFieldSets';
	
	/* Helper Functions */
	
	private static String getConfigXml() {
		return (testConfigXml == null)
			? [select body from StaticResource where name = :CONFIG_FILE_NAME][0].body.toString()
			: testConfigXml;
	}
	
	private static void init() {
		// map record type labels by their ids
		for(Schema.RecordTypeInfo recordType : UTIL_SchemaHelper.getRecordTypeInfos(TRAFFIC_PRODUCT_OBJ_NAME)) {
			// exclude record types that aren't available for running users
			// and the master record type
			if(recordType.isAvailable() && recordType.getName() != 'Master') {
				recordTypeIdToLabelMap.put(
					  recordType.getRecordTypeId()
					, recordType.getName()
				);
			}
		}
		
		// map dev names to their ids
		for(RecordType recordType : [
			select developerName 
			from RecordType
			where id in :recordTypeIdToLabelMap.keySet()
		]) {
			recordTypeDevNameToIdMap.put(recordType.developerName, recordType.id);
		}
		
		// load and parse field set config lists
		fieldSetsByRtIdMap = new Map<Id, AD_OPS_AddTrafficProducts.MyFieldSet>();
		XmlStreamReader reader = new XMLStreamReader(getConfigXml());
		while(reader.hasNext()) {
			if(reader.getEventType() == XmlTag.START_ELEMENT && reader.getLocalName() == FIELD_SET_TAG) {
				parseFieldSet(reader);
			}
			reader.next();
		}
		
		// build select options
		recordTypeOptions = new List<SelectOption>();
		recordTypeOptions.add(new SelectOption(START_OPTION, START_OPTION, true));
		Map<String, SelectOption> temp = new Map<String, SelectOption>();
		for(Id recordTypeId : fieldSetsByRtIdMap.keySet()) {
			String recordTypeLabel = recordTypeIdToLabelMap.get(recordTypeId);
			temp.put(recordTypeLabel, new SelectOption(recordTypeId, recordTypeLabel)); 
		}
		List<String> sortedLabels = new List<String>();
		sortedLabels.addAll(temp.keySet());
		sortedLabels.sort();
		for(String label : sortedLabels) {
			recordTypeOptions.add(temp.get(label));
		}
		
		// create consolidated list of fields
		allFields = new Set<String>();
		for(AD_OPS_AddTrafficProducts.MyFieldSet fieldSet : fieldSetsByRtIdMap.values()) {
			for(AD_OPS_AddTrafficProducts.MyField field : fieldSet.fieldList) {
				allFields.add(field.name);
			}
		}
	}
	
	private static void parseFieldSet(XmlStreamReader reader) {
		AD_OPS_AddTrafficProducts.MyFieldSet fieldSet = new AD_OPS_AddTrafficProducts.MyFieldSet();
		String rtDevName = reader.getAttributeValue(null, RT_DEV_NAME_ATTR);
		if(recordTypeDevNameToIdMap.containsKey(rtDevName)) {
			Id recordTypeId = recordTypeDevNameToIdMap.get(rtDevName);
			while(reader.hasNext()) {
				if(reader.getEventType() == XmlTag.START_ELEMENT && reader.getLocalName() == FIELD_TAG) {
					Boolean required = reader.getAttributeValue(null, REQUIRED_ATTR) == TRUE_VAL;
					while(reader.hasNext()) {
						if(reader.getEventType() == XmlTag.CHARACTERS) {
							String fieldApiName = reader.getText();
							if(UTIL_SchemaHelper.isValidField(TRAFFIC_PRODUCT_OBJ_NAME,fieldApiName)) { 
								fieldApiName = fieldApiName.toLowerCase();
								fieldSet.fieldList.add(new AD_OPS_AddTrafficProducts.MyField(
									  fieldApiName
									, required
								));
							} else {
								system.debug('==>Skipping field.  Invalid field api name: ' + fieldApiName);
							}
							break;
						}
						reader.next();
					}
				} else if(reader.getEventType() == XmlTag.END_ELEMENT && reader.getLocalName() == FIELD_SET_TAG) {
					fieldSet.recordTypeId = recordTypeId;
					fieldSet.recordTypeName = rtDevName;
					fieldSet.recordTypeLabel = recordTypeIdToLabelMap.get(recordTypeId);
					fieldSetsByRtIdMap.put(recordTypeId, fieldSet);
					break;
				}
				reader.next();
			}
		} else {
			system.debug('==>Skipping field set.  Invalid record type dev name: ' + rtDevName);
		}
	}
	
	/* Test Hooks */
	
	public static String testConfigXml {
		get;
		set {
			system.assert(Test.isRunningTest(), 'testConfigBody may only be set by test methods');
			testConfigXml = value;
		}
	}
}