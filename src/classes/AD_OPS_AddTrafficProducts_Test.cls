/*
Developer: Ralph Callaway <ralph@callawaycloudconsulting.com>
Description:
	Test methods for AD_OPS_AddTrafficProducts.cls and 
	AD_OPS_AddTrafficProductsFieldList.cls
*/
@isTest
private class AD_OPS_AddTrafficProducts_Test { 

	/* Constants, Properties, and Variables */
	
	// Constants
	private static final String FIELD_SET_TAG = AD_OPS_AddTrafficProductsFieldLists.FIELD_SET_TAG;
	private static final String FIELD_TAG = AD_OPS_AddTrafficProductsFieldLists.FIELD_TAG;
	private static final String REQUIRED_ATTR = AD_OPS_AddTrafficProductsFieldLists.REQUIRED_ATTR;
	private static final String RT_DEV_NAME_ATTR = AD_OPS_AddTrafficProductsFieldLists.RT_DEV_NAME_ATTR;
	private static final String TRAFFIC_PRODUCT_OBJ_NAME = AD_OPS_AddTrafficProductsFieldLists.TRAFFIC_PRODUCT_OBJ_NAME;
	private static final String TRUE_VAL = AD_OPS_AddTrafficProductsFieldLists.TRUE_VAL;
	
	// Variables 
	private static String fieldName1 = 'Path_to_Banner__c'; // NB: test depends on this being a text field
	private static String fieldName2 = 'Path_to_Tags__c'; // NB: test depends on this being a text field
	private static String invalidFieldName = UTIL_TestUtil.TEST_STRING;
	private static Id recordTypeId1;
	private static Id recordTypeId2;
	private static String recordTypeDevName1;
	private static String recordTypeDevName2;
	private static String invalidRecordTypeDevName = UTIL_TestUtil.TEST_STRING;
	
	
	
	/* Before Test Actions */
	
	// Create config xml for field sets, add one entry for invalid record type
	// , add another for record type 1 with field 1 as required, and finally a
	// a thir for record type 2, with field 2 as non-required and an invalid field
	static {
		// gather some valid record type ids
		Set<Id> recordTypeIds = new Set<Id>();
		for(Schema.RecordTypeInfo rtInfo : UTIL_SchemaHelper.getRecordTypeInfos(TRAFFIC_PRODUCT_OBJ_NAME)) {
			if(rtInfo.isAvailable()) {
				recordTypeIds.add(rtInfo.getRecordTypeId());
			}
			if(recordTypeIds.size() == 2) {
				break;
			}
		}
		for(RecordType recordType : [select developerName from RecordType where id in :recordTypeIds]) {
			if(recordTypeDevName1 == null) {
				recordTypeDevName1 = recordType.developerName;
				recordTypeId1 = recordType.id;
			} else {
				recordTypeDevName2 = recordType.developerName;
				recordTypeId2 = recordType.id;
				break;
			}
		}
		
		// create config xml
		AD_OPS_AddTrafficProductsFieldLists.testConfigXml = '<?xml version="1.0"?>'
			+ '<fieldsets>'
				// add entry for invalid record type
				+ '<' + FIELD_SET_TAG + ' ' + RT_DEV_NAME_ATTR + '="' + invalidRecordTypeDevname + '">'
				+ '</' + FIELD_SET_TAG + '>'
				// add entry for record type 1 and required field 1
				+ '<' + FIELD_SET_TAG + ' ' + RT_DEV_NAME_ATTR + '="' + recordTypeDevName1 + '">'
					+ '<' + FIELD_TAG + ' ' + REQUIRED_ATTR + '="' + TRUE_VAL + '">'
						+ fieldName1
					+ '</' + FIELD_TAG + '>'
				+ '</' + FIELD_SET_TAG + '>'
				// add entry for record type 2 and field 2 and invalid field, neither required
				+ '<' + FIELD_SET_TAG + ' ' + RT_DEV_NAME_ATTR + '="' + recordTypeDevName2 + '">'
					+ '<' + FIELD_TAG + '>'
						+ invalidFieldName
					+ '</' + FIELD_TAG + '>'
					+ '<' + FIELD_TAG + '>'
						+ fieldName2
					+ '</' + FIELD_TAG + '>'
				+ '</' + FIELD_SET_TAG + '>'
			+ '</fieldsets>';
	}

	/* Test Methods */
	
	@isTest
	private static void testTrafficProductsFieldList() {
		// load record type map
		Map<Id, AD_OPS_AddTrafficProducts.MyFieldSet> fieldSetsByRtIdMap = AD_OPS_AddTrafficProductsFieldLists.fieldSetsByRtIdMap;
		
		// check that we have only the valid record types
		system.assertEquals(2, fieldSetsByRtIdMap.size());
		system.assert(fieldSetsByRtIdMap.containsKey(recordTypeId1));
		system.assert(fieldSetsByRtIdMap.containsKey(recordTypeId2));
		
		// check field set 1 has the expected field and that it's required
		AD_OPS_AddTrafficProducts.MyFieldSet fieldSet1 = fieldSetsByRtIdMap.get(recordTypeId1);
		system.assertEquals(1, fieldSet1.fieldList.size());
		AD_OPS_AddTrafficProducts.MyField field1 = fieldSet1.fieldList[0];
		system.assertEquals(true, field1.required);
		system.assert(fieldName1.equalsIgnoreCase(field1.name));
		
		// check field set 1 has field 2 and not the invalid field, and that field 2 is not required
		AD_OPS_AddTrafficProducts.MyFieldSet fieldSet2 = fieldSetsByRtIdMap.get(recordTypeId2);
		system.assertEquals(1, fieldSet2.fieldList.size());
		AD_OPS_AddTrafficProducts.MyField field2 = fieldSet2.fieldList[0];
		system.assertEquals(false, field2.required);
		system.assert(fieldName2.equalsIgnoreCase(field2.name));
	}
	
	@isTest
	private static void testAddProducts() {
		// create test case
		Case testCase = UTIL_TestUtil.createCase();
		
		// load traffic products page
		ApexPages.StandardController controller = new ApexPages.StandardController(testCase);
		AD_OPS_AddTrafficProducts extension = new AD_OPS_AddTrafficProducts(controller);
		
		// test non-trivial property values and exercise the rest
		system.assertEquals(2, extension.fieldSets.size());
		system.assertEquals(3, extension.recordTypeOptions.size()); // add 1 for start option
		system.assertEquals(1, extension.displayedProducts);
		system.assertNotEquals(null, extension.startOption);
		system.assertEquals(2, extension.allFields.size());
		system.assertEquals(null, extension.removedProductNum);
		
		// fill out first product for record type 1, but don't populate required field 1
		AD_OPS_AddTrafficProducts.MyProduct product1 = extension.products[0];
		product1.recordTypeId = recordTypeId1;
		PageReference nextPage = extension.save();
		
		// validate we have some errors for the required field
		system.assertEquals(null, nextPage);		
		system.assert(ApexPages.hasMessages(ApexPages.Severity.ERROR));
		system.assert(product1.fieldErrors.containsKey(fieldName1.toLowerCase()));
		
		// fill out required field for product1, fill out product2 for record type 2 with 
		// no fields (none are required)
		system.assertEquals(2, extension.displayedProducts);
		product1.record.put(fieldName1, UTIL_TestUtil.TEST_STRING); 
		AD_OPS_AddTrafficProducts.MyProduct product2 = extension.products[1];
		product2.recordTypeId = recordTypeId2;
		nextPage = extension.save();
		
		// validate we have two traffic products
		Integer productCount = [select count() from Traffic_Product__c where traffic_case__c = :testCase.id];
		system.assertEquals(2, productCount);
	}
}