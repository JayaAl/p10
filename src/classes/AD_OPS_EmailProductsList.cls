/*
Developer: Ralph Callaway <ralph@callawaycloudconsulting.com>
Description:
	Dynamically queries the fields needed to list the traffic 
	products in new trafficking request email template via the 
	AD_OPS_EmailProductsList component
*/
public class AD_OPS_EmailProductsList {
	
	/* Constants, Properties, and Variables */
	
	// NB: We're forced to initialize in the getter method
	// as the constructor runs before the trafficCaseId has
	// been set via it's component attribute.
	public List<MyProduct> products { 
		get {
			if(products == null) {
				init();
			}
			return products;
		}
		private set;
	}

	public Id trafficCaseId { get; set; }
	
	/* Support Functions */

	private void init() {
		Set<String> allFields = new Set<String>();
		for(AD_OPS_AddTrafficProducts.MyFieldSet fieldSet : AD_OPS_AddTrafficProductsFieldLists.fieldSetsByRtIdMap.values()) {
			for(AD_OPS_AddTrafficProducts.MyField field : fieldSet.fieldList) {
				allFields.add(field.name.toLowerCase());
			}
		}
		
		// NB: fields name must be lower case as sets are case-sensitive, and won't recognize
		// if a particular field is already present in the set.
		allFields.add('name');
		allFields.add('recordtype.name');
		allFields.add('special_instructions_additional_info__c');
		
		String fieldSelect = '';
		for(String field : allFields) {
			fieldSelect += ', ' + field;
		}
		String query = 'select ' + fieldSelect.subString(2)
			+ ' from Traffic_Product__c'
			+ ' where traffic_case__c = :trafficCaseId';
		products = new List<MyProduct>();
		for(Traffic_Product__c product : (List<Traffic_Product__c>) Database.query(query)) {
			products.add(new MyProduct(product));
		}
	}
	
	/* Inner Classes */
	
	public class MyProduct {
		public Traffic_Product__c record { get; private set; }
		public List<AD_OPS_AddTrafficProducts.MyField> fieldList { get; private set; }
		
		public MyProduct(Traffic_Product__c record) {
			this.record = record;
			fieldList = AD_OPS_AddTrafficProductsFieldLists.fieldSetsByRtIdMap.get(record.recordTypeId).fieldList;
		} 
	}
	
	/* Test Methods */
	
	@isTest
	private static void testEmailProductsList() {
		// create test data
		Integer batchSize = 3;
		Case testCase = UTIL_TestUtil.createCase();
		List<Traffic_Product__c> products = new List<Traffic_Product__c>();
		for(Integer i = 0; i < batchSize; i++) {
			Traffic_Product__c product = UTIL_TestUtil.generateTrafficProduct();
			product.traffic_case__c = testCase.id;
			products.add(product);
		}
		insert products;
		
		// load component
		AD_OPS_EmailProductsList controller = new AD_OPS_EmailProductsList();
		controller.trafficCaseId = testCase.id;
		
		// validate products are loaded
		system.assertEquals(batchSize, controller.products.size());
	}

}