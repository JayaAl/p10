global without sharing class ATG_AgencyAccountSummaryController {
//Custom Exception:
  public class ATG_AgencyAccountSummaryControllerException extends Exception {}
  
  

  public List<SBQQ__Quote__c> quoteList{get;set;}
  public id accountId{get;set;}
  public id selectedAccountId{get;set;}
  public id quoteId{get;set;}
  public string loggedInUserEmail{get;set;}
  public String campaignIdtoDel {get;set;}
  public String accountType {get;set;}
  public String selectedAdvertiserIdVal {get;set;}
  public string sortParam{get;set;}
  public string sortType{get;set;}
  public set<id> advertiserId{get;set;}

  
  	//List to show the limited records on the page
    public List<SBQQ__Quote__c> quoteListToShow{get;set;} 
    //Navigation variables
    Integer counter = 0;//TO track the number of records parsed
    Integer limitSize = 5;//Number of records to be displayed
    Integer totalSize =0; //To Store the total number of records available


//Contructor:
  public ATG_AgencyAccountSummaryController(){


  	quoteListToShow = new List<SBQQ__Quote__c>();
    //this.accountId = ApexPages.currentPage().getParameters().get('accountId');
    if(accountId==null)
    {
        loggedInUserEmail = UserInfo.getUserEmail();
        Id loggedInUserId = UserInfo.getUserId();
        User userObj = [Select id,contactId from User where id =: loggedInUserId ];
        system.debug('Contact is ::: '+userObj.contactId);
        system.debug('loggedInUserEmail' + loggedInUserEmail); 
        for(contact c:[select account.type,accountId from contact where Id =:userObj.contactId limit 1]){
			accountType = c.account.type;
            accountId = c.accountId;
            /*if(c.account.type=='Ad Agency'){
            	accountId=null;
            }
            else {
            	accountId = c.accountId;//'0014000000cTAnN';//c.accountId;

            }*/
         }


            

    }
    system.debug('here it is' + accountId);
    advertiserId = new set<id>();
    for(AccountPartner ap:[select AccountFromId from AccountPartner where AccountToId=:accountId])
    {
    	advertiserId.add(ap.AccountFromId);
    }    
      system.debug('here it is' + advertiserId);
    //system.debug('accountId' + accountId);  
    //accountId='0014000001hwrPr';
    if(advertiserId.size()>0){
    	quoteList = [select id,ATG_Campaign_Status__c,ATG_Campaign_Name__c,SBQQ__StartDate__c,SBQQ__EndDate__c, SBQQ__ExpirationDate__c,ATG_BudgetTotal__c ,SBQQ__Account__r.Name from SBQQ__Quote__c where SBQQ__Account__c in: advertiserId];
    }
    
    totalSize = quoteList.size();
        
    //Intial adding of contacts to quoteListToShow
    //check the total records are more than limitSize and assign the records
    if((counter+limitSize) <= totalSize){
        for(Integer i=0;i<limitSize;i++){
            quoteListToShow.add(quoteList.get(i));
        }
    }else{
        for(Integer i=0;i<totalSize;i++){
            quoteListToShow.add(quoteList.get(i));
        }
    }
    
    
  }
  public PageReference sortCampaigns()
  {
  	system.debug('here it is' + selectedAdvertiserIdVal);
  	if(advertiserId.size()>0){
      quoteListToShow = new List<SBQQ__Quote__c>();
  		quoteList.sort();
	  	if(sortType=='asc')
	  	{
	  		sortType = 'desc';
	  	}
	  	else if(sortType=='desc')
	  	{
	  		sortType='asc';
	  	}
	  	else {
	  		sortType='asc';
	  	}
	  	string sortQuery = 'select id,ATG_Campaign_Status__c,ATG_Campaign_Name__c,SBQQ__StartDate__c,SBQQ__EndDate__c, SBQQ__ExpirationDate__c,ATG_BudgetTotal__c ,SBQQ__Account__r.Name from SBQQ__Quote__c where SBQQ__Account__c in: advertiserId' ;
	  	sortQuery += ' order by '+ sortParam + ' ' + sortType;
	  	system.debug('THIS IS sortQuery' + sortQuery);
	  	quoteList = database.query(sortQuery);
      totalSize = quoteList.size();
        
      //Intial adding of contacts to quoteListToShow
      //check the total records are more than limitSize and assign the records
      if((counter+limitSize) <= totalSize){
          for(Integer i=0;i<limitSize;i++){
              quoteListToShow.add(quoteList.get(i));
          }
      }else{
          for(Integer i=0;i<totalSize;i++){
              quoteListToShow.add(quoteList.get(i));
          }
      }
  	}
  	return null;

  }
  


/*public PageReference previous()
  {
  	system.debug('here it is' + selectedAdvertiserIdVal);
  	if(String.isNotBlank(selectedAdvertiserIdVal)){
  		selectedAccountId = selectedAdvertiserIdVal;
  		system.debug('here it is');
  		if(selectedAccountId!=null)
  			quoteList = [select id,ATG_Campaign_Status__c,ATG_Campaign_Name__c,SBQQ__StartDate__c,SBQQ__EndDate__c, SBQQ__ExpirationDate__c,ATG_BudgetTotal__c ,SBQQ__Account__r.Name from SBQQ__Quote__c where SBQQ__Account__c=:selectedAccountId];
    }

    return null;

  }


public PageReference next()
  {
  	system.debug('here it is' + selectedAdvertiserIdVal);
  	if(String.isNotBlank(selectedAdvertiserIdVal)){
  		selectedAccountId = selectedAdvertiserIdVal;
  		system.debug('here it is');
  		if(selectedAccountId!=null)
  			quoteList = [select id,ATG_Campaign_Status__c,ATG_Campaign_Name__c,SBQQ__StartDate__c,SBQQ__EndDate__c, SBQQ__ExpirationDate__c,ATG_BudgetTotal__c ,SBQQ__Account__r.Name from SBQQ__Quote__c where SBQQ__Account__c=:selectedAccountId];
    }

    return null;

  }*/


  //Navigation methods
    
    
	public void beginning(){
   
        quoteListToShow.clear();
        counter=0;
        if((counter + limitSize) <= totalSize){
       
            for(Integer i=0;i<limitSize;i++){
                quoteListToShow.add(quoteList.get(i));
            }   
           
        } else{
       
            for(Integer i=0;i<totalSize;i++){
                quoteListToShow.add(quoteList.get(i));
            }       
           
        }
       
    }
   
    public void next(){
   
        quoteListToShow.clear();
        counter=counter+limitSize;
       
        if((counter+limitSize) <= totalSize){
            for(Integer i=counter;i<(counter+limitSize);i++){
                quoteListToShow.add(quoteList.get(i));
            }
        } else{
            for(Integer i=counter;i<totalSize;i++){
                quoteListToShow.add(quoteList.get(i));
            }
        }
    }
   
    public void previous(){
   
        quoteListToShow.clear();

        counter=counter-limitSize;       
       
        for(Integer i=counter;i<(counter+limitSize); i++){
            quoteListToShow.add(quoteList.get(i));
        }
    }
 
    public void last (){
   
        quoteListToShow.clear();
       
        if(math.mod(totalSize , limitSize) == 0){
            counter = limitSize * ((totalSize/limitSize)-1);
        } else if (math.mod(totalSize , limitSize) != 0){
            counter = limitSize * ((totalSize/limitSize));
        }
       
        for(Integer i=counter-1;i<totalSize-1;i++){
                quoteListToShow.add(quoteList.get(i));
        }
       
    }
   
    public Boolean getDisableNext(){
   
        if((counter + limitSize) >= totalSize )
            return true ;
        else
            return false ;
    }
   
    public Boolean getDisablePrevious(){
   
        if(counter == 0)
            return true ;
        else
            return false ;
    } 








  public PageReference NewCampaign() {
    Pagereference pageRef;
    if(accountId != null){
      //accountId = '001n000000KTXYV';
    
     pageRef = new PageReference('/apex/ATG_NewQuote?accountId='+accountId);
    }
    else {
      pageRef = new PageReference('login');
    }

    return pageRef;
  }

   
    /*public  PageReference redirectToEditPageAction(){
    

        Pagereference pageRef = new PageReference('/apex/ATG_NewQuote?QID='+quoteId);
        return pageRef;
    }*/

  public Pagereference deleteCampaignAction(){
    try{
      system.debug('campaignIdtoDel ::: '+campaignIdtoDel);
      Id opptyId; 
      List<SBQQ__Quote__c> quoteToDel = [Select Id,SBQQ__Opportunity2__c from SBQQ__Quote__c where Id =: campaignIdtoDel];
      if(quoteToDel != null && !quoteToDel.isEmpty()){
          
          opptyId = quoteToDel[0].SBQQ__Opportunity2__c;
          system.debug('b4 delete opptyId :::'+opptyId);
          delete quoteToDel;
          system.debug('after delete opptyId :::'+opptyId);

          List<Opportunity> opptyLst = [Select id,StageName from Opportunity where id =: opptyId];
          if(opptyLst != null && !opptyLst.isEmpty()){
            system.debug('opptyLst :::'+opptyLst);  
            opptyLst[0].StageName = 'Closed Lost';
            opptyLst[0].Lost_Comments__c = 'Self service opportunity, Quote deleted , Thus closing Opportunity. ';
            update opptyLst;

          }
              
      }

    }catch(Exception dmle){
        system.debug('dmle.getMessage()' + dmle.getMessage() + '---' + dmle.getStackTraceString());
        ApexPages.addmessage(new ApexPages.message(ApexPages.severity.ERROR,dmle.getMessage()));
        return null;
    }  
       String url = '/apex/ATG_AccountSummary';
       if(accountId != null)
        url += '?accountId='+accountId;
        
       Pagereference pageRef = new PageReference(url);
       pageRef.setRedirect(true);
    return pageRef;   
  }
    
}