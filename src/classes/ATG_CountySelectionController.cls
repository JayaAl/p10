global with sharing class ATG_CountySelectionController {
//Custom Exception:
  public class ATG_CountySelectionControllerException extends Exception {}
  
  @TestVisible private List<ATG_Quote_Line_GeoData__c> geoLinesToWrite;
    public String productOptionValue { get; set; }  
    public date startDate{get;set;} 
    public date endDate{get;set;} 
    public decimal spend{get;set;} 
    private Id qliId;
    public String selectedCountylst {get;set;}
    public String treeContext {get;set;}
    Public void ProductOptionValue(){        
        system.debug('<<option value>>'+productOptionValue);        
    }

    public String genderOptionValue { get; set; }    
    Public void GenderrOptionValue(){        
        system.debug('<<option value>>'+genderOptionValue);        
    }
    public static Map<Id,ATG_State__c> statMap;
    public static List<ATG_County__c> countyLststatic;

    //public list<product2> productIdList{get;set;}
    public list<product2> productIdList {
    get {
        if(productIdList == null) {
            // Typically a getter shouldn't change the objects state.
            
                // Use this with caution!
                // You don't want to be doing the same SOQL query every time
                // something accesses this property.
                productIdList = [SELECT Id,name 
                                      FROM Product2 WHERE name='Audio' or name='Display' order by name limit 2];
            
                // Should the name be defaulted to something in the new instance?
                // At what point will the new Assessment be inserted?
            }
        return productIdList;

                   
    }
    set {
        productIdList = value;

        // Do you want to insert or update any assigned value to the database?
        // I wouldn't recommend this as good practice in a property. 
        // Better that the caller has already inserted it or it is inserted later.
        //upsert value;

        
    }
}





    public product2 getProductDetails()
    {
       return [select id,name from product2 limit 1];
    }

    @TestVisible private product2 memo_product;
    public product2 products {
    get {
      if(memo_product == null){
        memo_product = [SELECT Id, Name
        FROM product2
         LIMIT 1];
      }
      return memo_product;
    }
    private set;
    }



    String gender = null;

                
    public List<SelectOption> getItems() {
        List<SelectOption> options = new List<SelectOption>(); 
        options.add(new SelectOption('Male','Male')); 
        options.add(new SelectOption('Female','Female')); 
        return options; 
    }
                   
    public String getGender() {
        return gender;
    }
                    
    public void setGender(String gender) { this.gender = gender; }

    public product2 product{get;set;}


 

  String[] age = new String[]{};

  public List<SelectOption> getAges() {

        List<SelectOption> options = new List<SelectOption>();

        options.add(new SelectOption('13+','13+'));

        options.add(new SelectOption('13-17','13-17'));
        options.add(new SelectOption('18-24','18-24'));
        options.add(new SelectOption('25-54','25-54'));
        options.add(new SelectOption('55+','55+'));

 

        return options;

    }

 

    public String[] getAge() {

        return age;

    }

 

    public void setAge(String[] age) {

        this.age = age;

    }


  

  public SBQQ__QuoteLine__c quoteLine{get;set;}
  public id quoteLineId{get;set;}

  public Id quoteLineIdParam {get;set;}
  public boolean isEditMode {get;set;}

//Contructor:
  public ATG_CountySelectionController(){
    //this.stateContainerId    = ApexPages.currentPage().getParameters().get('stateContainerId');
    //this.quoteId     = ApexPages.currentPage().getParameters().get('quoteId');
    //this.quoteLineId = ApexPages.currentPage().getParameters().get('quoteLineId');
    isEditMode = false;
    if(ApexPages.currentPage().getParameters().containsKey('quoteLineId')){
        quoteLineIdParam = ApexPages.currentPage().getParameters().get('quoteLineId');
        isEditMode = true;
    }    
//    srRegionMap = new Map<Id, Super_Region__c>(srRegions);
//    regionMap = new Map<Id, Regions__c>(regions);
//    zoneMap = new Map<Id, ATG_County__c>>(zones);
    for(product2 pd:[select id,Name from product2 where name='Audio' or Name='Display' order by name])
        productIdList.add(pd);

    quoteLine = [select id, ATG_Gender__c,atg_age__C,ATG_Budget__c,SBQQ__StartDate__c,SBQQ__EndDate__c from SBQQ__QuoteLine__c limit 1];
    
    ProductOptionValue = string.valueof(productIdList[0].Id);
    GenderOptionValue = 'Male';





    //if(this.stateContainerId == null) {
      //throw new ATG_CountySelectionControllerException('Failed to find the StateContainerId in the url string.');
    //}

    /*if(this.quoteId == null) {
      throw new ATG_CountySelectionControllerException('Failed to find the QuoteId in the url string.');
    }

    if(this.quoteLineId == null) {
      throw new ATG_CountySelectionControllerException('Failed to find the QuoteLineId in the url string.');
    }*/
  }

//PageRef Methods:
  public PageReference cancel() {
    //return returnToParentObj();
    return null;
  }

  public PageReference save() {
    //List<String> selected = (List<String>) JSON.deserialize(this.selectedObjects, List<String>.class);
    //geoLinesToWrite = generateLineGeoDataFromSelection(selected);

    try {
         quoteLine=new SBQQ__QuoteLine__c();
      //if(geos.size() > 0) {
        //delete any existing geo records before inserting the new ones:
        //delete geos; 
      //}

      id quoteId = 'a4tn00000008qTk';

      system.debug('QUOTELINE ProductOptionValue' + ProductOptionValue);
      system.debug('QUOTELINE GenderOptionValue' + GenderOptionValue);
      system.debug('QUOTELINE age' + age);
      system.debug('QUOTELINE quoteLine.SBQQ__StartDate__c' + startDate);
      system.debug('QUOTELINE quoteLine.Spend' + endDate);
      system.debug('QUOTELINE quoteLine.SATG_Budget__c' + spend);
      
      if(string.isEmpty(ProductOptionValue)){
        ApexPages.addmessage(new ApexPages.message(ApexPages.severity.WARNING,'Please select a Product'));
        return null;
      }
      if(string.isEmpty(GenderOptionValue)){
        ApexPages.addmessage(new ApexPages.message(ApexPages.severity.WARNING,'Please select a Gender'));
        return null;
      }
      if(age.size()==0){
        ApexPages.addmessage(new ApexPages.message(ApexPages.severity.WARNING,'Please select Target Age'));
        return null;
      }
      if(spend==null || (!(spend>0))){
        ApexPages.addmessage(new ApexPages.message(ApexPages.severity.WARNING,'Please enter a valid Budget'));
        return null;
      }
      if(startDate==null){
        ApexPages.addmessage(new ApexPages.message(ApexPages.severity.WARNING,'Please select a Start Date'));
        return null;
      }
      if(endDate==null){
        ApexPages.addmessage(new ApexPages.message(ApexPages.severity.WARNING,'Please select a End Date'));
        return null;
      }
      if(!(system.Today().daysBetween(startDate) > 4))
      {
        ApexPages.addmessage(new ApexPages.message(ApexPages.severity.WARNING,'Start Date should be atleast 4 days ahead of Today'));
        return null;

      }

      if((startDate.daysBetween(endDate))<0)
      {
        ApexPages.addmessage(new ApexPages.message(ApexPages.severity.WARNING,'Please select a valid end date. End date should be greater than the start date'));
        return null;

      }

      quoteLine.SBQQ__Product__c = ProductOptionValue;
      quoteLine.ATG_Gender__c = GenderOptionValue;
      string ageSelected='';
      for(string ageSelection:age)
        ageSelected+=ageSelection+';';

      if(!string.isempty(ageSelected))
        quoteLine.atg_age__C = ageSelected.substringBeforeLast(';');
      

        

      
      system.debug('QUOTELINE quoteLine.quoteLine.atg_age__C' + quoteLine.atg_age__C);
      quoteLine.SBQQ__StartDate__c = startDate;
      quoteLine.SBQQ__EndDate__c = endDate;
      decimal d = Decimal.valueOf(string.valueof(spend));
      quoteLine.ATG_Budget__c = d;
      system.debug('QUOTELINE quoteLine.quoteLine.ATG_Budget__c ' + quoteLine.ATG_Budget__c );
      quoteLine.SBQQ__Quote__c = quoteId;

      insert quoteLine;
      qliId = quoteLine.Id;
      system.debug('Quote line id : '+qliId);
    } catch (exception dmle){
      system.debug(dmle.getMessage());
    }

    return returnToParentObj();
  }

  private id last(List<String> incoming){
    return incoming[incoming.size()-1];
  }

  private List<ATG_Quote_Line_GeoData__c> generateLineGeoDataFromSelection(List<String> selected) {
    List<ATG_Quote_Line_GeoData__c> gdata = new List<ATG_Quote_Line_GeoData__c>();
    Set<String> completeSR = new Set<String>();
    Set<String> completeR = new Set<String>();
    for (String id: selected) {
      String[] idParts = id.split('_');
      Id potentialStateContainerId = (id) idParts[0];
      integer partsInt = idParts.size();
      if (partsInt == 1) {
        gdata.add(new ATG_Quote_Line_GeoData__c(ATG_State_Container__c = last(idParts), ATG_Quote_Line__c=this.quoteLineId));
        break;
      } else if (partsInt == 2) {
        gdata.add(new ATG_Quote_Line_GeoData__c(ATG_State__c = last(idParts), ATG_Quote_Line__c=this.quoteLineId));
        completeSR.add(last(idParts));
      } else if (partsInt == 3) {
        completeR.add(last(idParts));
        if (!completeSR.contains(idParts[1])) {
          gdata.add(new ATG_Quote_Line_GeoData__c(ATG_County__c = last(idParts), ATG_Quote_Line__c=this.quoteLineId));
        }
      }/* else if (partsInt == 4) {
        if (!completeR.contains(idParts[2])) {
          gdata.add(new ATG_Quote_Line_GeoData__c(ATG_County__c> = last(idParts), Quote_Line__c=this.quoteLineId));
        }
      }*/
    }
    return gdata;
  }


public PageReference returnToParentObj()
{

    return null;
    
}
  @RemoteAction
     global static List<JSTreeWrapper> getStateCountyRelation(string isEditmodeStr, String quoteLineItemIdParam) {
         Set<Id> selectedCountySet = new Set<Id>();
         system.debug('getStateCountyRelation Remote Action :::'+isEditmodeStr+'QLI ::'+quoteLineItemIdParam);
         if(isEditmodeStr == 'true'){
           Map<Id,ATG_Quote_Line_GeoData__c> existing_QLI_Geo_Map = new Map<Id,ATG_Quote_Line_GeoData__c>([select id,name,ATG_County__c,ATG_State__c from ATG_Quote_Line_GeoData__c where ATG_Quote_Line__c =: quoteLineItemIdParam]);
           for(ATG_Quote_Line_GeoData__c geoVar : existing_QLI_Geo_Map.values())
              selectedCountySet.add(geoVar.ATG_County__c);
         }

         system.debug('selectedCountySet ::::'+selectedCountySet);

         statMap = new Map<Id,ATG_State__c>([Select id,name,ATG_State_Code__c from ATG_State__c]);
         countyLststatic = [Select id,name,ATG_State__c,ATG_County_Name__c from ATG_County__c where ATG_State__c IN : statMap.keySet()];
        
         Map<Id, List<JSTreeCountyWrapper>> stateCountyMap = new Map<Id, List<JSTreeCountyWrapper>>();
         for(ATG_County__c countyVar : countyLststatic){
            system.debug('condition :::'+(isEditmodeStr == 'true' && selectedCountySet.contains(countyVar.Id)));
            if(stateCountyMap.containsKey(countyVar.ATG_State__c)){
                stateCountyMap.get(countyVar.ATG_State__c).add(new JSTreeCountyWrapper(countyVar,(isEditmodeStr == 'true' && selectedCountySet.contains(countyVar.Id) )?true : false));
            }else{
                stateCountyMap.put(countyVar.ATG_State__c,new list<JSTreeCountyWrapper>{new JSTreeCountyWrapper(countyVar,(isEditmodeStr == 'true' && selectedCountySet.contains(countyVar.Id) )?true:false)});
            } 
         }
          
         List<JSTreeWrapper> wrapperLst = new List<JSTreeWrapper>(); 

         for(Id stateId : stateCountyMap.keySet()){
            JSTreeWrapper wrapper = new JSTreeWrapper();
            wrapper.stateVar = statMap.get(stateId);
            wrapper.countyLst = stateCountyMap.get(stateId);
            wrapperLst.add(wrapper);
         } 
         
         system.debug('wrapperLst ::::'+wrapperLst);
         String jsonStr = JSON.serialize(wrapperLst, true);
         system.debug('jsonStr ::: '+jsonStr);
         return wrapperLst;
     }
     
     @RemoteAction
     global static List<ATG_DMA__c> getDMARecords(){
        return [Select id,name from ATG_DMA__c ];
     } 

     global class JSTreeWrapper{
         global List<JSTreeCountyWrapper> countyLst;
         global ATG_State__c  stateVar ;           
     }

     global class JSTreeCountyWrapper{
        global ATG_County__c county;
        global boolean isSelected; 

        global JSTreeCountyWrapper(ATG_County__c countyVar, boolean isSelectedVar){
            county = countyVar;
            isSelected = isSelectedVar;
        }
     }
     public void insertQLILocationAction(){
        system.debug('selectedCountylst ::: '+selectedCountylst);
        List<String> namelst = selectedCountylst.split(',');
        if(isEditMode){
           if(quoteLineIdParam != null) 
              delete [select id,name,ATG_County__c,ATG_State__c from ATG_Quote_Line_GeoData__c where ATG_Quote_Line__c =: quoteLineIdParam];
        }
        List<ATG_Quote_Line_GeoData__c> QLIGeoLst = new List<ATG_Quote_Line_GeoData__c>();       
        if('geo'.equalsIgnoreCase(treeContext)){
          List<ATG_County__c> selCountyLst = [Select id,name,ATG_State__c from ATG_County__c where ATG_County_Name__c in: namelst];
          Map<Id,ATG_County__c> countyMap = new Map<Id,ATG_County__c>(selCountyLst);
          system.debug('qliId :::: '+qliId);
          for(ATG_County__c countyVar : selCountyLst){
            ATG_Quote_Line_GeoData__c QLIGeoVar = new ATG_Quote_Line_GeoData__c();
            QLIGeoVar.ATG_Quote_Line__c = qliId;
            QLIGeoVar.ATG_County__c = countyVar.Id;
            QLIGeoVar.ATG_State__c = countyVar.ATG_State__c;
            QLIGeoLst.add(QLIGeoVar);
          }
          

        }else if('dma'.equalsIgnoreCase(treeContext)){
          List<ATG_dma__c> selDMALst = [Select id,name from ATG_dma__c where name in: namelst];
          for(ATG_dma__c dmaVar : selDMALst){
            ATG_Quote_Line_GeoData__c QLIGeoVar = new ATG_Quote_Line_GeoData__c();
            QLIGeoVar.ATG_Quote_Line__c = qliId;
            QLIGeoVar.ATG_dma__c = dmaVar.Id;
            
            QLIGeoLst.add(QLIGeoVar); 
          }
        }
        
        system.debug('QLIGeoLst ::: '+QLIGeoLst);
        database.insert(QLIGeoLst,false);
     }   


 
}