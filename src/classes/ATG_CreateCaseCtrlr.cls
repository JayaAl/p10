public with sharing class ATG_CreateCaseCtrlr{

	public Case caseObj {get;set;}

	public ATG_CreateCaseCtrlr(){
		caseObj = new Case();
	}	

	public void createCase(){
		caseObj.RecordTypeid = '012n00000004jql';
		insert caseObj;	
	}
}