public with sharing class ATG_CreativeCtrlr{
	
	private Id quoteLineIdParam;
	public SBQQ__QuoteLine__c quoteLine{get;set;}
	public string quoteLineType {get;set;}
	public Id opptyId {get;set;}
	public string urlStr {get;set;}
	
	private Id quoteiD;
		

	public List<Attachment> selectedAttachmentLst{get{
			system.debug('Getter List');
			Id qlid = ApexPages.currentPage().getParameters().get('QLID');
			system.debug('Getter qlid'+qlid);
			return [Select Id,Name,Description from Attachment where parentId =: qlid];
		}set;}
	public ATG_CreativeCtrlr(){
		
		initialiseVairable();

	}

	private void initialiseVairable(){
		if(ApexPages.currentPage().getParameters().containsKey('QLID')){
	        quoteLineIdParam = ApexPages.currentPage().getParameters().get('QLID');
	        quoteiD = ApexPages.currentPage().getParameters().get('QID');
	        quoteLine = [select id,SBQQ__Quote__c,ATG_Gender__c,SBQQ__Quote__r.SBQQ__Opportunity2__c,SBQQ__Product__c,atg_age__C,ATG_Budget__c,SBQQ__StartDate__c,SBQQ__EndDate__c,SBQQ__Quote__r.ATG_Campaign_Status__c from SBQQ__QuoteLine__c where id =:quoteLineIdParam limit 1];
	        
	        if(quoteiD != null){
	        	quoteiD = quoteLine.SBQQ__Quote__c;				
			}

	        Map<Id,product2> productMap = new Map<Id,product2>([select id,Name from product2 where name='Audio' or Name='Display' order by name]);
	        //opptyId = quoteLine.SBQQ__Quote__r.SBQQ__Opportunity2__c;
	        opptyId = quoteLine.Id;
	        
	        quoteLineType = productMap.get(quoteLine.SBQQ__Product__c).Name;

	        selectedAttachmentLst = [Select Id,Name,description from Attachment where parentId =: opptyId];
	        system.debug('Getter selectedAttachmentLst'+selectedAttachmentLst);
		}
	} 


	

	

	public PageReference proceedToSummaryPage(){

		Note noteObj = new Note();
	    noteObj.body = urlStr;
	    noteObj.title = 'CLick Through URL  ';
	    noteObj.isPrivate = false;	    
	    noteObj.ParentId = quoteLineIdParam;

	    try{
		    if(String.isNotBlank(urlStr)){
		    	insert noteObj; 	    	
		    }		      
	    }catch(Exception ex){
	      ApexPages.addMessage(new ApexPages.message(ApexPages.severity.ERROR,'Error uploading Notes'));
	      return null;
	    }

		Pagereference pageRef = new PageReference('/apex/ATG_QuoteSummary?QID='+quoteiD+'&QLID='+quoteLineIdParam);
        return pageRef;
	}


	public PageReference ProceedToQLIPage(){
			
		Pagereference pageRef = new PageReference('/apex/ATG_QuoteLine?QID='+quoteiD);
        return pageRef;
	}

	public PageReference proceedToAccountSummaryPage(){

		Pagereference pageRef;
		if(quoteiD != null){
			SBQQ__Quote__c quoteObj = [Select id,SBQQ__Account__c from SBQQ__Quote__c where id =: quoteiD];
			pageRef = new PageReference('/apex/ATG_AccountSummary?accountid='+quoteObj.SBQQ__Account__c);
		}
		else if(quoteLineIdParam != null){
			SBQQ__QuoteLine__c quoteLine = [Select id,SBQQ__Quote__r.SBQQ__Account__c from SBQQ__QuoteLine__c where id = : quoteLineIdParam ];
			pageRef = new PageReference('/apex/ATG_AccountSummary?accountid='+quoteLine.SBQQ__Quote__r.SBQQ__Account__c);
		}

		return pageRef;
	}
}