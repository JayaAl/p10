global without sharing class ATG_NewQuoteController {
//Custom Exception:
  public class ATG_NewQuoteControllerException extends Exception {}
  
  @TestVisible private List<ATG_Quote_Line_GeoData__c> geoLinesToWrite;
    public String labelText { get; set; }  
    public date startDate{get;set;} 
    public date endDate{get;set;} 
    public Id quoteId;
    public  Id accountId {get;set;}
    public String accountName {get;set;}
    public String campaignName {get;set;}
    public string loggedInUserEmail{get;set;}
    

    public SBQQ__Quote__c quote{get;set;}
    public boolean isEditMode {get;set;}
    private Id QLID;
    public String accountType {get;set;}
    public String selectedAdvertiserIdVal {get;set;}

//Contructor:
  public ATG_NewQuoteController(){
    //this.stateContainerId    = ApexPages.currentPage().getParameters().get('accountId');
    //this.quoteId     = ApexPages.currentPage().getParameters().get('quoteId');
    //this.quoteLineId = ApexPages.currentPage().getParameters().get('quoteLineId');
    //if(ApexPages.currentPage().getParameters().containsKey('accountId'))

    /*if(accountId==null)
      this.accountId = '001n000000KTXYV';*/

    if(accountId==null && ApexPages.currentPage().getParameters().containsKey('accountId') ){
      if( ApexPages.currentPage().getParameters().get('accountId')!=null )
        this.accountId    = ApexPages.currentPage().getParameters().get('accountId');
    }

    //this.accountId = '001n000000KTXYV';
    if(accountId==null)
    {
        loggedInUserEmail = UserInfo.getUserEmail();
        system.debug('loggedInUserEmail' + loggedInUserEmail); 
        for(User userObj : [Select id,name,ContactId,Contact.AccountId from User where id =: Userinfo.getUserId()]){          
          accountId = userObj.Contact.AccountId;

          if(userObj.ContactId == null)
            this.accountId = '001n000000KTXYV';
        }

        
        /*for(contact c:[select accountId from contact where email=:loggedInUserEmail limit 1])
            accountId = c.accountId;*/

    } 
    
    
    labelText = 'Advertiser :';
    isEditMode = false;

    //this.accountId = '0014000001haPm7';
    //this.accountId = '001n000000IkGN2AAN';
    Account acctObj = [select name,Type from account where id=:accountId limit 1];
    accountName = acctObj.name;
    accountType = acctObj.Type;

    if(ApexPages.currentPage().getParameters().containsKey('QID')){
        quoteId = ApexPages.currentPage().getParameters().get('QID');
        isEditMode = true;
     
      quote = [select id,ATG_Campaign_Name__c,ATG_Campaign_Status__c,SBQQ__Opportunity2__c from SBQQ__Quote__c where id=:quoteId];
      campaignName = quote.ATG_Campaign_Name__c;

      // Abhishek

        List<SBQQ__QuoteLine__c> qlidLst = [Select Id from SBQQ__QuoteLine__c where SBQQ__Quote__c =: quote.id];

        if(qlidLst != null && !qlidLst.isEmpty()){
          QLID = qlidLst[0].id;
        }
    }



//    srRegionMap = new Map<Id, Super_Region__c>(srRegions);
//    regionMap = new Map<Id, Regions__c>(regions);
//    zoneMap = new Map<Id, ATG_County__c>>(zones);
    




    //if(this.stateContainerId == null) {
      //throw new ATG_NewQuoteControllerException('Failed to find the StateContainerId in the url string.');
    //}

    /*if(this.quoteId == null) {
      throw new ATG_NewQuoteControllerException('Failed to find the QuoteId in the url string.');
    }

    if(this.quoteLineId == null) {
      throw new ATG_NewQuoteControllerException('Failed to find the QuoteLineId in the url string.');
    }*/
  }

//PageRef Methods:
  public PageReference cancel() {
    //return returnToParentObj();
    
    
    
    PageReference accountSummaryPage = new PageReference('/apex/ATG_AccountSummary?accountId=' + accountId);
    accountSummaryPage.setRedirect(true);
    return accountSummaryPage;
    
  }

  public PageReference save() {
  
    system.debug('QUOTELINE campaignName' + campaignName);
    system.debug('QUOTELINE accountId' + accountId);
    system.debug('QUOTELINE string.isEmpty(campaignName)' + string.isEmpty(campaignName));
    
    if(string.isBlank(campaignName)){
        ApexPages.addmessage(new ApexPages.message(ApexPages.severity.WARNING,'Please enter a valid Campaign Name'));
        return null;
    }
    
    Date startDate =  Date.today(). addDays(4); //Give your date
    
    Date lastDate = startDate.addDays(date.daysInMonth(startDate.year() , startDate.month())  - 1);
    system.debug(startDate  + ' **** ' +lastDate );
      
      
      system.debug('QUOTELINE quoteLine.SBQQ__StartDate__c' + startDate);
      system.debug('QUOTELINE quoteLine.Spend' + endDate);
     SYSTEM.DEBUG(quoteId) ;
    try
    {

      if(quoteId==null) {
        quote = new SBQQ__Quote__c();
        SYSTEM.DEBUG('SYSTEM.DEBUG(quoteId) ;' + quoteId) ;

        
        quote.ATG_Campaign_Name__c = campaignName;
        if(accountType.contains('Agency')){
           
           if(String.isNotBlank(selectedAdvertiserIdVal)){
            quote.SBQQ__Account__c = selectedAdvertiserIdVal;
            quote.ATG_Agency__c = accountId;
           }else {              
              ApexPages.addmessage(new ApexPages.message(ApexPages.severity.WARNING,'Please select an appropriate advertiser.'));
              return null;
            } 

        }else{
            quote.SBQQ__Account__c = accountId;
        }
        
        quote.SBQQ__StartDate__c = startDate;
        quote.SBQQ__EndDate__c = lastDate;
        quote.SBQQ__ExpirationDate__c = lastDate;
        quote.OwnerID = userinfo.getUserId();
      }
      else{

        quote.ATG_Campaign_Name__c = campaignName;

        
     }
        
     
    
        upsert quote;

        //ATG_NewQuoteController.shareRecords(quote.Id,quote.SBQQ__Opportunity2__c);

        return pageRedirect(quote.Id);
        
    }
    catch (exception dmle){
      system.debug(dmle.getMessage());
    }

    return null;
      
    //List<String> selected = (List<String>) JSON.deserialize(this.selectedObjects, List<String>.class);
    //geoLinesToWrite = generateLineGeoDataFromSelection(selected);

    //try {
    //     quote=new SBQQ__Quote__c();
      //if(geos.size() > 0) {
        //delete any existing geo records before inserting the new ones:
        //delete geos; 
      //}

      //id quoteId = 'a4tn00000008qTk';

      /*system.debug('QUOTELINE ProductOptionValue' + ProductOptionValue);
      system.debug('QUOTELINE GenderOptionValue' + GenderOptionValue);
      system.debug('QUOTELINE age' + age);
      system.debug('QUOTELINE quoteLine.SBQQ__StartDate__c' + startDate);
      system.debug('QUOTELINE quoteLine.Spend' + endDate);
      system.debug('QUOTELINE quoteLine.SATG_Budget__c' + spend);
      
      if(string.isEmpty(ProductOptionValue)){
        ApexPages.addmessage(new ApexPages.message(ApexPages.severity.WARNING,'Please select a Product'));
        return null;
      }
      if(string.isEmpty(GenderOptionValue)){
        ApexPages.addmessage(new ApexPages.message(ApexPages.severity.WARNING,'Please select a Gender'));
        return null;
      }
      if(age.size()==0){
        ApexPages.addmessage(new ApexPages.message(ApexPages.severity.WARNING,'Please select Target Age'));
        return null;
      }
      if(spend==null || (!(spend>0))){
        ApexPages.addmessage(new ApexPages.message(ApexPages.severity.WARNING,'Please enter a valid Budget'));
        return null;
      }
      if(startDate==null){
        ApexPages.addmessage(new ApexPages.message(ApexPages.severity.WARNING,'Please select a Start Date'));
        return null;
      }
      if(endDate==null){
        ApexPages.addmessage(new ApexPages.message(ApexPages.severity.WARNING,'Please select a End Date'));
        return null;
      }
      if(!(system.Today().daysBetween(startDate) > 4))
      {
        ApexPages.addmessage(new ApexPages.message(ApexPages.severity.WARNING,'Start Date should be atleast 4 days ahead of Today'));
        return null;

      }

      if((startDate.daysBetween(endDate))<0)
      {
        ApexPages.addmessage(new ApexPages.message(ApexPages.severity.WARNING,'Please select a valid end date. End date should be greater than the start date'));
        return null;

      }

      quoteLine.SBQQ__Product__c = ProductOptionValue;
      quoteLine.ATG_Gender__c = GenderOptionValue;
      string ageSelected='';
      for(string ageSelection:age)
        ageSelected+=ageSelection+';';

      if(!string.isempty(ageSelected))
        quoteLine.atg_age__C = ageSelected.substringBeforeLast(';');
      

        

      
      system.debug('QUOTELINE quoteLine.quoteLine.atg_age__C' + quoteLine.atg_age__C);
      quoteLine.SBQQ__StartDate__c = startDate;
      quoteLine.SBQQ__EndDate__c = endDate;
      decimal d = Decimal.valueOf(string.valueof(spend));
      quoteLine.ATG_Budget__c = d;
      system.debug('QUOTELINE quoteLine.quoteLine.ATG_Budget__c ' + quoteLine.ATG_Budget__c );
      quoteLine.SBQQ__Quote__c = quoteId;

      insert quoteLine;
      qliId = quoteLine.Id;
      
    } catch (exception dmle){
      system.debug(dmle.getMessage());
    }*/

    
  }

  public id last(List<String> incoming){
    return incoming[incoming.size()-1];
  }

  


public PageReference pageRedirect(Id quoteId)
{
    String url = '/apex/ATG_QuoteLine?QID=' + quoteId;
    if(QLID != null)
      url += '&QLID='+QLID;
    PageReference newQuoteLinePage = new PageReference(url);
    
    newQuoteLinePage.setRedirect(true);
    return newQuoteLinePage;
    
}
   
@future(callout=true)
public static void shareRecords(Id quoteIdParam, Id opportunityIdParam){


    OpportunityShare share = new OpportunityShare();
    share.OpportunityId = opportunityIdParam;
    share.OpportunityAccessLevel = 'Edit';
    share.UserOrGroupId = '005n0000002krJR';

   // insert share;

    SBQQ__Quote__share Qshare = new SBQQ__Quote__share();
    Qshare.ParentId = quoteIdParam;
    Qshare.AccessLevel = 'Edit';
    Qshare.UserOrGroupId = '005n0000002krJR';

    insert Qshare;

}

 
}