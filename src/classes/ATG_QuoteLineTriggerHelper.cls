public without sharing class ATG_QuoteLineTriggerHelper 
{
    /**
     * Create opportunities from quotes
     * Opportunities are only updated if the quoteline is created
     * have a specified opportunity
     * @param quotes [description]
     */
    public static void handleAfterInsert(List<SBQQ__QuoteLine__c> quoteLines) {
        
        List<Opportunity> opportunities = new List<Opportunity>();
        set<id> opptyIds = new set<id>();
        system.debug('quoteLines' + quoteLines);
        set<id> quoteIds = new set<id>();
        Map<id,SBQQ__Quote__c> quoteObjectMap = new Map<id,SBQQ__Quote__c>();

        system.debug('quoteLines' + quoteLines);

        for(SBQQ__QuoteLine__c quoteLine : quoteLines){
            quoteIds.add(quoteLine.SBQQ__Quote__c);
        }
        system.debug('quoteIds' + quoteIds);
        for(SBQQ__Quote__c quote:[select id,SBQQ__Opportunity2__c,SBQQ__StartDate__c, SBQQ__EndDate__c,ATG_End_Date__c,ATG_Start_Date__c from SBQQ__Quote__c where id in: quoteIds])
        {   
            opptyIds.add(quote.SBQQ__Opportunity2__c);
            quoteObjectMap.put(quote.Id,quote);
        }
        system.debug('opportunities' + opptyIds);

        if(quoteObjectMap.size()>0)
            updateQuoteDates(quoteLines, quoteObjectMap);

        if(opptyIds.size()>0)
            ATS_Util.updateOppStage(opptyIds,'In Negotiation','Draft',50);

        // Update quotes to primary and add opportunity to quote
        /*for(Opportunity opp : opportunities) {
            opp.Probability =50;
        }
        upsert opportunities;
        */
        system.debug('quoteIds:::'+quoteIds);
       /*  // update opportunity lines 
        Map<Id,OpportunityLineItem> QLIToOLIMap = new Map<Id,OpportunityLineItem>();
        for(OpportunityLineItem  opptyLine : [Select id,name from OpportunityLineItem  where SBQQ__QuoteLine__c  IN: quoteIds ]){
            QLIToOLIMap.put(opptyLine.SBQQ__QuoteLine__c, opptyLine);
        }
        if(QLIToOLIMap.size()>0)
        updateOpptyLineItems(QLIToOLIMap,quoteLines);*/
    }

    /**
     * Create opportunities from quotes
     * Opportunities are only updated if the quoteline is created
     * have a specified opportunity
     * @param quotes [description]
     */
    public static void handleAfterUpdate(List<SBQQ__QuoteLine__c> quoteLines,map<id,SBQQ__QuoteLine__c> oldMap) {
        
        set<id> quoteIds = new set<id>();
        Map<id,SBQQ__Quote__c> quoteObjectMap = new Map<id,SBQQ__Quote__c>();

        for(SBQQ__QuoteLine__c quoteLine : quoteLines){
            if((oldMap.get(quoteLine.Id).SBQQ__StartDate__c<>quoteLine.SBQQ__StartDate__c)|| (oldMap.get(quoteLine.Id).SBQQ__EndDate__c<>quoteLine.SBQQ__EndDate__c))
                quoteIds.add(quoteLine.SBQQ__Quote__c);
        }
        
        for(SBQQ__Quote__c quote:[select id,SBQQ__Opportunity2__c,SBQQ__StartDate__c, SBQQ__EndDate__c,ATG_End_Date__c,ATG_Start_Date__c from SBQQ__Quote__c where id in: quoteIds])
        {   
            quoteObjectMap.put(quote.Id,quote);
        }
        
        if(quoteObjectMap.size()>0)
            updateQuoteDates(quoteLines, quoteObjectMap);

        // update opportunity lines 
      /*  Map<Id,OpportunityLineItem> QLIToOLIMap = new Map<Id,OpportunityLineItem>();
        for(OpportunityLineItem  opptyLine : [Select id,Product2.Name,name,ServiceDate,End_Date__c,SBQQ__QuoteLine__c from OpportunityLineItem  where SBQQ__QuoteLine__c  IN: quoteLines ]){
            QLIToOLIMap.put(opptyLine.SBQQ__QuoteLine__c, opptyLine);
        }
        system.debug('QLIToOLIMap ::: '+QLIToOLIMap);

        if(QLIToOLIMap.size()>0)
        updateOpptyLineItems(QLIToOLIMap,quoteLines);*/
    }

   /* public static void updateOpptyLineItems(Map<Id,OpportunityLineItem> QLIToOLIMap, List<SBQQ__QuoteLine__c> quoteLineLst ){
        system.debug('inside my method');
        List<OpportunityLineItem> oliToUpdateLst = new List<OpportunityLineItem>();

        for(SBQQ__QuoteLine__c qliVar : quoteLineLst){
            if(qliVar != null){

                OpportunityLineItem lineItemVar = QLIToOLIMap.get(qliVar.id);
                if(lineItemVar != null){
                lineItemVar.ServiceDate  = qliVar.SBQQ__StartDate__c;
                lineItemVar.End_Date__c  = qliVar.SBQQ__EndDate__c;
                lineItemVar.Offering_Type__c = 'Standard';
                lineItemVar.Platform__c = 'Everywhere';
                lineItemVar.Medium__c = lineItemVar.Product2.Name;
                lineItemVar.duration__c =  10;
                system.debug('lineItemVar :::'+lineItemVar);
                oliToUpdateLst.add(lineItemVar);    
                }
            }
            
        }

        system.debug('inside my method'+oliToUpdateLst);
       /* if(oliToUpdateLst.size() != 0)
        update oliToUpdateLst;

    }*/

    public static void updateQuoteDates(List<SBQQ__QuoteLine__c>quoteLines, Map<id,SBQQ__Quote__c> quoteObjectMap)
    {
        List<SBQQ__Quote__c> quotesToUpdate = new List<SBQQ__Quote__c>();
        date startDate = system.today();
        date endDate = system.today();
        SBQQ__Quote__c quote;
        if(quoteLines.size()>0)
            quote = quoteObjectMap.get(quoteLines[0].SBQQ__Quote__c);
        else {
            return;
        }
        List<Date> lstStartDate = new List<Date>();
        List<Date> lstEndDate = new List<Date>();
        List<SBQQ__QuoteLine__c> allQuoteLines = new List<SBQQ__QuoteLine__c>();
        allQuoteLines = [select id,SBQQ__StartDate__c,SBQQ__EndDate__c from SBQQ__QuoteLine__c where SBQQ__Quote__c =:quote.Id  AND ID NOT IN: quoteLines];
        system.debug('INSIDE UPDQUOTEDATES 1' + quoteLines);

        allQuoteLines.ADDALL(quoteLines);
        
        system.debug('INSIDE UPDQUOTEDATES 2' + allQuoteLines);
        system.debug('INSIDE UPDQUOTEDATES 3' + allQuoteLines.size());
        for(SBQQ__QuoteLine__c quoteLine : allQuoteLines){
            system.debug('INSIDE UPDQUOTEDATES 4'  + quoteLine);
            system.debug('INSIDE UPDQUOTEDATES 5' + quote.ATG_Start_Date__c);
            if(quote.ATG_Start_Date__c==null)
            {
                quote.SBQQ__StartDate__c = quoteLine.SBQQ__StartDate__c;
                quote.SBQQ__EndDate__c = quoteLine.SBQQ__EndDate__c;
                quotesToUpdate.add(quote);
            }
            else
            {
                lstStartDate.add(quoteLine.SBQQ__StartDate__c);
                lstEndDate.add(quoteLine.SBQQ__EndDate__c);


            }
            
        }
        system.debug('INSIDE UPDQUOTEDATES 6' + lstStartDate);
        system.debug('INSIDE UPDQUOTEDATES 7' + lstEndDate);

        if(lstStartDate.size()>0){
            lstStartDate.sort();
            lstEndDate.sort();
            system.debug('INSIDE UPDQUOTEDATES inside 8' + lstStartDate);
            system.debug('INSIDE UPDQUOTEDATES inside 9' + lstEndDate);
            quote.SBQQ__StartDate__c = lstStartDate[0];
            quote.SBQQ__EndDate__c = lstEndDate[lstEndDate.size()-1];

            quotesToUpdate.add(quote);

        }
        system.debug('INSIDE UPDQUOTEDATES inside 10' + quotesToUpdate.size());
        system.debug('INSIDE UPDQUOTEDATES inside 11' + quotesToUpdate);
        //if(quotesToUpdate !=null && quotesToUpdate.size()>1)
            
        
        /*for(SBQQ__QuoteLine__c quoteLine : quoteLines){

            SBQQ__Quote__c quote = quoteObjectMap.get(quoteLine.SBQQ__Quote__c);
            if(quote.ATG_Start_Date__c==null)
            {
                quote.SBQQ__StartDate__c = quoteLine.SBQQ__StartDate__c;
                quote.SBQQ__EndDate__c = quoteLine.SBQQ__EndDate__c;
                quotesToUpdate.add(quote);
            }
            else
            {
                if(quote.ATG_Start_Date__c>quoteLine.SBQQ__StartDate__c || quote.ATG_End_Date__c < quoteLine.SBQQ__EndDate__c){
                    if(quote.SBQQ__StartDate__c>quoteLine.SBQQ__StartDate__c)
                        quote.SBQQ__StartDate__c = quoteLine.SBQQ__StartDate__c;

                    if(quote.SBQQ__EndDate__c < quoteLine.SBQQ__EndDate__c)
                        quote.SBQQ__EndDate__c = quoteLine.SBQQ__EndDate__c;


                        quotesToUpdate.add(quote);

                }

            }
            
        }*/

        if(quotesToUpdate.size()>0)
            update quotesToUpdate;

    }


    

    
    /*public static void updateOppStages(set<id> opptyIds,string stageName,integer probability) {
        
        List<Opportunity> opportunities = new List<Opportunity>();
        system.debug('caled here');
        system.debug('opportunitiesid' + opptyIds);
        for(opportunity op : [select StageName,Probability from opportunity where id in:opptyIds]) {
            // If quote does not have an opportunity, create a new one
            if(op.Id <> null) {
                
                op.StageName = stageName;
                op.Probability = probability;
                
                opportunities.add(op);                
            }
        }
        system.debug('opportunities' + opportunities);
        update opportunities;
        // Update quotes to primary and add opportunity to quote
        
    } 
    */

}