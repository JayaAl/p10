global without sharing class ATG_QuoteSummaryController {
//Custom Exception:
  public class ATG_QuoteSummaryControllerException extends Exception {}
  
  

  public List<SBQQ__QuoteLine__c> quoteLines{get;set;}
  public SBQQ__Quote__c quote{get;set;}
  public id quoteLineId{get;set;}
  public id quoteId{get;set;}
  public id accountId{get;set;}
  public string creditStatus{get;set;}
  public decimal creditApprovedAmount{get;set;}

  public Id quoteLineIdParam {get;set;}
  public boolean isEditMode {get;set;}
  public id qliIdtoDel {get;set;}
  private boolean invokeRecursion;
//Contructor:
  public ATG_QuoteSummaryController(){
    this.quoteId = ApexPages.currentPage().getParameters().get('QID');
    if(quoteId==null)
        return;
    quoteLineId = ApexPages.currentPage().getParameters().get('QLID');  
    quote = [select id,SBQQ__Opportunity2__r.ATG_Campaign_Status__c,ATG_Campaign_Name__c,SBQQ__StartDate__c,SBQQ__EndDate__c,ATG_BudgetTotal__c, SBQQ__ExpirationDate__c ,SBQQ__Account__r.Id, SBQQ__Account__r.Name,ATG_Campaign_Status__c,SBQQ__Account__r.Credit_Status__c,SBQQ__Account__r.Credit_Approved_Amount__c from SBQQ__Quote__c where id=:quoteId];
    creditStatus = quote.SBQQ__Account__r.Credit_Status__c;
    creditApprovedAmount = (quote.SBQQ__Account__r.Credit_Approved_Amount__c != null ) ? quote.SBQQ__Account__r.Credit_Approved_Amount__c : 0;
    accountId = quote.SBQQ__Account__r.Id;
    invokeRecursion = true;
    quoteLines  = new List<SBQQ__QuoteLine__c>();
    quoteLines = [select id,name, SBQQ__Product__c, SBQQ__Product__r.Name,SBQQ__Quote__r.SBQQ__Account__c,SBQQ__Quote__r.ATG_Campaign_Name__c,SBQQ__Quote__r.ATG_Campaign_Status__c,SBQQ__Quote__r.SBQQ__StartDate__c,SBQQ__Quote__r.SBQQ__EndDate__c,SBQQ__Quote__r.SBQQ__Account__r.Name, SBQQ__ProductCode__c, SBQQ__StartDate__c, SBQQ__EndDate__c, ATG_Budget__c,SBQQ__ListPrice__c FROM SBQQ__QuoteLine__c where SBQQ__Quote__c=:quoteId  ORDER BY createddate ASC]; 
    system.debug('quoteLines ==> '+quoteLines);    
    
    //populateCurrentQuoteLines();
    for(SBQQ__QuoteLine__c quoteLine : quoteLines){
      system.debug('List Price : '+quoteLine.SBQQ__ListPrice__c);
    }
    
    
  }

  public void populateCurrentQuoteLines(){
    SBQQ__QuoteLine__c quoteLine;
    do{
      
      quoteLine = [select id,name, SBQQ__Product__c, SBQQ__Product__r.Name,SBQQ__Quote__r.SBQQ__Account__c,SBQQ__Quote__r.ATG_Campaign_Name__c,SBQQ__Quote__r.ATG_Campaign_Status__c,SBQQ__Quote__r.SBQQ__StartDate__c,SBQQ__Quote__r.SBQQ__EndDate__c,SBQQ__Quote__r.SBQQ__Account__r.Name, SBQQ__ProductCode__c, SBQQ__StartDate__c, SBQQ__EndDate__c, ATG_Budget__c,SBQQ__ListPrice__c FROM SBQQ__QuoteLine__c where SBQQ__Quote__c=:quoteId  AND Id =: quoteLineId ORDER BY createddate ASC]; 
      system.debug('^^^^^^^^^^^'+quoteLine.SBQQ__ListPrice__c);
      if(quoteLine != null && quoteLine.SBQQ__ListPrice__c != null){
          quoteLines.add(quoteLine);
      }

    }while(quoteLine.SBQQ__ListPrice__c == null);
  }

  public PageReference edit() {
    return null;
  }

   public PageReference redirectToCreativePageAction()
   {
        Pagereference pageRef = new PageReference('/apex/ATG_CreativePage?QID='+quoteiD+'&QLID='+quoteLineiD);
        return pageRef;

   }
    public  PageReference redirectToEditPageAction(){
    

        Pagereference pageRef = new PageReference('/apex/ATG_QuoteLine?QID='+quoteiD+'&QLID='+quoteLineiD);
        return pageRef;
    }

    public PageReference createNewQuoteLine() {
      Pagereference pageRef = new PageReference('/apex/ATG_QuoteLine?QID='+quoteiD);
    return pageRef;
  }
  public PageReference submitOrder() {
    
    /*opportunity op = [select id,ATG_Campaign_Status__c from opportunity where id=:quote.SBQQ__Opportunity2__r.Id];
    op.ATG_Campaign_Status__c = 'Submitted';
    update op;
    Pagereference pageRef = new PageReference('/apex/ATG_AccountSummary?accountId='+accountId);
    return pageRef;
    */
    system.debug('THIS IS THE MESSAGE');
    boolean isQuoteLineHasPastDate=false;
    for(SBQQ__QuoteLine__c quoteLine : quoteLines){
system.debug('THIS IS THE MESSAGE 0' + (quoteLine.SBQQ__StartDate__c));
system.debug('THIS IS THE MESSAGE 0' + (system.today().addDays(5)));
      system.debug('THIS IS THE MESSAGE 1' + (quoteLine.SBQQ__StartDate__c<system.today().addDays(4)));
      if(quoteLine.SBQQ__StartDate__c<system.today().addDays(5)){
        isQuoteLineHasPastDate = true;
        break;
      }

    }

    Pagereference pageRef;
    system.debug('THIS IS THE MESSAGE 2' +isQuoteLineHasPastDate);
    if(isQuoteLineHasPastDate){
        system.debug('THIS IS THE MESSAGE 3');
        ApexPages.Message myMsg = new ApexPages.Message(ApexPages.Severity.ERROR,'Quote Line(s) start date should be in the future');
        ApexPages.addMessage(myMsg);
    }    
    else{
      pageRef = new PageReference('/apex/ATG_TermsNCondn?accountId='+accountId+'&QID='+quoteiD+'&QLID='+quoteLineiD);
      
    }

    return pageRef;
  }

  public PageReference cancelOrder() {
      opportunity op = [select id,ATG_Campaign_Status__c from opportunity where id=:quote.SBQQ__Opportunity2__r.Id];
      op.ATG_Campaign_Status__c = 'Cancelled';
      update op;
      Pagereference pageRef = new PageReference('/apex/ATG_AccountSummary?accountId='+accountId);
      return pageRef;

      return null;
  }

  public PageReference editCampaignAction(){

      Pagereference pageRef = new PageReference('/apex/ATG_QuoteLine?QID='+quoteiD+'&QLID='+qliIdtoDel);
      return pageRef;

  }
  public Pagereference deleteCampaignAction(){
    try{
      //system.debug('campaignIdtoDel ::: '+campaignIdtoDel);
      List<SBQQ__QuoteLine__c> quoteToDel = [Select Id from SBQQ__QuoteLine__c where Id =: qliIdtoDel];
      if(quoteToDel != null && !quoteToDel.isEmpty())
      delete quoteToDel;

      
   
    }catch(Exception ex){

    }  
       String url = '/apex/ATG_QuoteSummary';
       if(quoteId != null)
        url += '?QID='+quoteId;
        
       Pagereference pageRef = new PageReference(url);
       pageRef.setRedirect(true);
    return pageRef;   
  }
 
}