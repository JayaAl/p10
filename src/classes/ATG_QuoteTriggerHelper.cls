/*
 * Quote Trigger Helper
 * ─────────────────────────────────────────────────────────────────────────────────────────────────
* @modifiedBy     Awanish Kumar <akumar3@pandora.com>
* @version        1.1
* @modified       2018-01-23
* @changes        Changes to Update Industry Category of Opportunity with Account Industry Category Field
* @Description    When new Opportunity is created, Industry Category of Opportunity is updated with Account
                  Industry Category Field.
                  
* ─────────────────────────────────────────────────────────────────────────────────────────────────┘
 */
public without sharing class ATG_QuoteTriggerHelper {
    
    
    public static void handleAfterInsert(List<SBQQ__Quote__c> quotes) {
        /*set<id> quoteIdSet = new set<Id>();
        for(SBQQ__Quote__c quote: quotes)
        {
            quoteIdSet.add(quote.id);

        }
system.debug('THIS IS THE quoteIdSet' + quoteIdSet);
        */
        createOppFromQuote(quotes); 
        //createOppFromQuote(quoteIdSet);
    }



    /*@future
    private static void createOppFromQuote(set<id> quoteIdSet) {
        system.debug('THIS IS THE inside quoteIdSet' + quoteIdSet);
        List<Opportunity> opportunities = new List<Opportunity>();
        List<SBQQ__Quote__c> quotes = new List<SBQQ__Quote__c>();
        List<SBQQ__Quote__c> quotesToUpdate = new List<SBQQ__Quote__c>();
        quotes = [select id,name,SBQQ__Opportunity2__c,ATG_Account_Naming_for_Display__c,ATG_Campaign_Name__c,
                    ATG_Name_of_Month__c,SBQQ__StartDate__c,SBQQ__Account__c
                    from SBQQ__Quote__c where id in:quoteIdSet];

        system.debug('THIS IS THE quotes' + quotes);

        Map<string,recordtype> opptyRecordTypes = new Map<string,recordtype>();
        for(recordtype rt: [SELECT Id, Name, DeveloperName FROM RecordType WHERE DeveloperName = 'Opportunity_Self_Service' or DeveloperName = 'Communities' ])
        {
            opptyRecordTypes.put(rt.DeveloperName,rt);
        }
        system.debug('THIS IS THE opptyRecordTypes' + opptyRecordTypes);
        try{
            //Query to locate the Self-Service Opportunity Record Type
            RecordType oppSelfServiceRT = opptyRecordTypes.get('Opportunity_Self_Service');//[SELECT Id, Name, DeveloperName FROM RecordType WHERE DeveloperName = '' LIMIT 1];
            system.debug('THIS IS THE oppSelfServiceRT' + oppSelfServiceRT);
            //Query to locate the Self-Service Opportunity Record Type
            RecordType quoteSelfServiceRT = opptyRecordTypes.get('Communities');//[SELECT Id, Name, DeveloperName FROM RecordType WHERE DeveloperName = 'Communities' LIMIT 1];
            system.debug('THIS IS THE quoteSelfServiceRT' + quoteSelfServiceRT);
            //Query to locate the Self-Service User
            User selfServiceUser = [SELECT Id, Username FROM User WHERE Username LIKE 'selfservice-ats@pandora.com.pandora10%' LIMIT 1];
            system.debug('THIS IS THE selfServiceUser' + selfServiceUser);
            //Query to locate the CPQ Pricebook
            Pricebook2 cpqPriceBook = [SELECT Id, Name FROM Pricebook2 WHERE Name = 'CPQ Price Book' LIMIT 1];
            system.debug('THIS IS THE cpqPriceBook' + cpqPriceBook);
            //Get the email of the current user
            //String currentUserEmail =  UserInfo.getUserEmail();
            
            //Query to find the contact associated with the current user
            //Contact currentUserContact = [SELECT Id, Email, FirstName, LastName, Account.Id FROM Contact WHERE  Email LIKE: currentUserEmail LIMIT 1];
            
            
            for(SBQQ__Quote__c quote : quotes) {
                // If quote does not have an opportunity, create a new one
                if(quote.SBQQ__Opportunity2__c == null) {
                    opportunities.add(new Opportunity(
                        Name =  'Self Service Opp - ' + quote.ATG_Account_Naming_for_Display__c + ' - ' + quote.ATG_Campaign_Name__c + ' - ' + quote.ATG_Name_of_Month__c + ' - ' + quote.SBQQ__StartDate__c.year(),
                        AccountId = quote.SBQQ__Account__c,
                        CloseDate = Date.today().addMonths(1),
                        Budget_Source__c = 'Digital / Other',
                        SBQQ__PrimaryQuote__c = quote.Id,
                        StageName = 'RFP Received/Sent Proposal',
                        ATG_Campaign_Status__c = 'Drafted',
                        SBQQ__QuotePricebookId__c = cpqPriceBook.Id,
                        RecordTypeId = oppSelfServiceRT.Id,
                        OwnerId = selfServiceUser.Id
                    ));
                }
            }
            insert opportunities;
            system.debug('THIS IS THE opportunities' + opportunities);
            // Update quotes to primary and add opportunity to quote
            for(Opportunity opp : opportunities) {
                quotesToUpdate.add(new SBQQ__Quote__c(
                    Id = opp.SBQQ__PrimaryQuote__c,
                    SBQQ__Primary__c = true,
                    SBQQ__Opportunity2__c = opp.Id,
                    SBQQ__Status__c = 'Draft',
                    SBQQ__PricebookId__c= cpqPriceBook.Id,
                    RecordTypeId = quoteSelfServiceRT.Id//,
                    //SBQQ__PrimaryContact__c = currentUserContact.Id//,
                    //SBQQ__Account = currentUserContact.Account.Id
                ));
            }

            system.debug('THIS IS THE quotesToUpdate' + quotesToUpdate);
            
            // udpate quotes if needed
            if(quotesToUpdate.size() > 0) {
                update quotesToUpdate;
            }
            system.debug('THIS IS THE quotesToUpdate' + quotesToUpdate);
        }
        catch(exception ex)
        {

            system.debug('THIS IS THE EXCEPTION' + ex.getMessage() + '---' + ex.getStackTraceString());
        }
    }

*/

    
    
    /**
     * Create opportunities from quotes
     * Opportunities are only created if the quote does not already
     * have a specified opportunity
     * @param quotes [description]
     */
    private static void createOppFromQuote(List<SBQQ__Quote__c> quotes) {
        
        List<Opportunity> opportunities = new List<Opportunity>();
        List<SBQQ__Quote__c> quotesToUpdate = new List<SBQQ__Quote__c>();
        
        //Query to locate the Self-Service Opportunity Record Type
        //RecordType oppSelfServiceRT = [SELECT Id, Name, DeveloperName FROM RecordType WHERE DeveloperName = 'Opportunity_Self_Service' LIMIT 1];
        
        //Query to locate the Self-Service Opportunity Record Type
        //RecordType quoteSelfServiceRT = [SELECT Id, Name, DeveloperName FROM RecordType WHERE DeveloperName = 'Communities' LIMIT 1];
        
        //Query to locate the Self-Service User
        //User selfServiceUser = [SELECT Id, Username FROM User WHERE Username LIKE 'selfservice-ats@pandora.com.pandora10%' LIMIT 1];
        
        //Query to locate the CPQ Pricebook
        //Pricebook2 cpqPriceBook = [SELECT Id, Name FROM Pricebook2 WHERE Name = 'CPQ Price Book' LIMIT 1];

        Map<string,recordtype> opptyRecordTypes = new Map<string,recordtype>();
        set<string> lstrecordTypeNames = new set<string>{'Opportunity_Self_Service','Communities'};
        

        for(recordtype rt: [SELECT Id, Name, DeveloperName FROM RecordType WHERE DeveloperName in:lstrecordTypeNames Limit 2])
        {
            opptyRecordTypes.put(rt.DeveloperName,rt);
        }
        system.debug('THIS IS THE opptyRecordTypes' + opptyRecordTypes);
        
            //Query to locate the Self-Service Opportunity Record Type
        RecordType oppSelfServiceRT = opptyRecordTypes.get('Opportunity_Self_Service');//[SELECT Id, Name, DeveloperName FROM RecordType WHERE DeveloperName = '' LIMIT 1];
        system.debug('THIS IS THE oppSelfServiceRT' + oppSelfServiceRT);
        //Query to locate the Self-Service Opportunity Record Type
        RecordType quoteSelfServiceRT = opptyRecordTypes.get('Communities');//[SELECT Id, Name, DeveloperName FROM RecordType WHERE DeveloperName = 'Communities' LIMIT 1];
        system.debug('THIS IS THE quoteSelfServiceRT' + quoteSelfServiceRT);
        

        //Query to locate the Self-Service User Commented by Sri
        //User selfServiceUser = [SELECT Id, Username FROM User WHERE Username LIKE 'selfservice-ats@pandora.com.pandora10%' LIMIT 1];
        //system.debug('THIS IS THE selfServiceUser' + selfServiceUser);
        

        //Query to locate the CPQ Pricebook
        Pricebook2 cpqPriceBook = [SELECT Id, Name FROM Pricebook2 WHERE Name = 'Standard Price Book' LIMIT 1];// Name = 'CPQ Price Book' LIMIT 1];
        system.debug('THIS IS THE cpqPriceBook' + cpqPriceBook);

        id contactId; 
        String secondaryUserEmail ;
        user u = [select id,contactId,Secondary_Email__c from user where id=:userinfo.getuserId()];
        if(u.ContactId<>NULL){
            contactId = u.contactId;
            secondaryUserEmail = u.Secondary_Email__c;
        }
        
        //Get the email of the current user
        //String currentUserEmail =  UserInfo.getUserEmail();
        
        //Query to find the contact associated with the current user
        //Contact currentUserContact = [SELECT Id, Email, FirstName, LastName, Account.Id FROM Contact WHERE  Email LIKE: currentUserEmail LIMIT 1];
        id accountId;
        set<id> accountIds = new set<id>();
        for(SBQQ__Quote__c quote : quotes) {
            accountIds.add(quote.SBQQ__Account__c);
        }

        Map<id,account> accountMap = new map<id,account>();

        for(account a:[select id,Type,Industry_Category__c from account where id in:accountIds])
        {
            accountMap.put(a.id,a);

        }
        
        Id quoteId ;
        
        //v1.1 Retreive all picklist fields for Opportunity
       // List<String> pickListValuesListForOpp= new List<String>();
        Map<String,String> pickListValuesListForOppMap = new Map<String,String>();
        Schema.DescribeFieldResult fieldResult = Opportunity.Industry_Category__c.getDescribe();
        List<Schema.PicklistEntry> ple = fieldResult.getPicklistValues();
        for( Schema.PicklistEntry pickListVal : ple){
           // pickListValuesListForOpp.add(pickListVal.getLabel());
            pickListValuesListForOppMap.put(pickListVal.getLabel(),pickListVal.getLabel());
        }
                    
        for(SBQQ__Quote__c quote : quotes) {
            accountId = quote.SBQQ__Account__c;
            boolean directRelationShip = false;


           // system.debug('system.debug(quote.SBQQ__Account__r.Type)' + quote.SBQQ__Account__c);
           // system.debug('system.debug(quote.SBQQ__Account__r.Type)' + accountMap.get(accountId).Type);
          //  system.debug('system.debug(quote.SBQQ__Account__r.Type)' + accountMap.get(accountId).Type);
            if(accountMap.containsKey(accountId) && accountMap.get(accountId).Type=='Advertiser')
                directRelationShip = true;
            else {
                directRelationShip = false;
            }
            
            quoteId = quote.id;
            
            // If quote does not have an opportunity, create a new one
            if(quote.SBQQ__Opportunity2__c == null) {
                
            //V1.1 Changes on 22-01-2018 - Update Industry Category of Opportunity
                
                Opportunity opp =  new Opportunity(
                    Name =  'Self Service Opp - ' + quote.ATG_Account_Naming_for_Display__c + ' - ' + quote.ATG_Campaign_Name__c + ' - ' + quote.ATG_Name_of_Month__c + ' - ' + quote.SBQQ__StartDate__c.year(),
                    AccountId = quote.SBQQ__Account__c,
                    CloseDate = Date.today(),
                    Radio_Type__c = 'Local',
                    Bill_on_Broadcast_Calendar2__c = 'No',
                    Budget_Source__c = 'Digital / Other',
                    SBQQ__PrimaryQuote__c = quote.Id,
                    StageName = 'RFP Received/Sent Proposal',
                    SBQQ__QuotePricebookId__c = cpqPriceBook.Id,
                    RecordTypeId = oppSelfServiceRT.Id,
                    OwnerId = UserInfo.getUserId(),//'005n0000002h8gP',
                    Confirm_direct_relationship__c = (quote.ATG_Agency__c != null ? false : true ),
                    Slingshot_Eligible__c = true,
                    Primary_Billing_Contact__c = contactId,
                    Primary_Contact__c = contactId,
                    Probability = 25,
                    ATG_Campaign_Status__c = 'Draft',
                    Agency__c = quote.ATG_Agency__c,
                    Alternate_Billing_Email_Address__c = secondaryUserEmail,
                    Opportunity_Type__c = 'New Business',
                    Campaign_Name__c = quote.ATG_Campaign_Name__c );
                
           
                //v1.1 Verifying wheather Opportunity Industry Category has the values present for corresponding Account Industry Category Fields .   
                if(accountMap.containsKey(quote.SBQQ__Account__c) )
                {
                        
                        Account currAccount = accountMap.get(quote.SBQQ__Account__c);
                     
                        system.debug('Map contains::'+pickListValuesListForOppMap.containsKey(currAccount.Industry_Category__c));
                        if(accountMap.get(quote.SBQQ__Account__c).Industry_Category__c != null && opp.Industry_Category__c == null && pickListValuesListForOppMap.containsKey(currAccount.Industry_Category__c)) {
                            opp.Industry_Category__c = currAccount.Industry_Category__c;
                            System.debug('Opportunity Industry'+opp.Industry_Category__c);
                        }
                    
                }
                
                 system.debug('opp' + opp);
                opportunities.add(opp);
            }


       }

        insert opportunities;
        
        system.debug('opportunity id inserted'+opportunities);
        /****************** Added to check : debug ::: Need to be removed ********************************/
        /*id userid = userinfo.getUserid();
        List<UserRecordAccess> urAccess = [SELECT RecordId, HasEditAccess FROM UserRecordAccess WHERE UserId =: userid  AND RecordId =: opportunities[0].Id];
        system.debug('urAccess  ::::==>'+urAccess);
        opportunities[0].Name = (opportunities[0].Name + 'ggggg');
        opportunities[0].SBQQ__PrimaryQuote__c = quoteId;
        update opportunities;*/
        system.debug('After update .... ');
       /* OpportunityShare share = new OpportunityShare();
        share.OpportunityAccessLevel = 'Edit';
        share.OpportunityId = opportunities[0].Id;
        share.UserOrGroupId = userinfo.getUserid();
        share.rowCause = 'Manual';
        insert share;*/
        /****************** Added to check : debug ::: Need to be removed ********************************/
        // Update quotes to primary and add opportunity to quote
        Set<Id> opptyIdSet = new Set<Id>();
        
        for(Opportunity opp : opportunities) {
            
            opptyIdSet.add(opp.Id);
            quotesToUpdate.add(new SBQQ__Quote__c(
                Id = opp.SBQQ__PrimaryQuote__c,
                SBQQ__Primary__c = true,
                SBQQ__Status__c = 'Draft',
                SBQQ__Opportunity2__c = opp.Id,
                SBQQ__PricebookId__c= cpqPriceBook.Id,
                RecordTypeId = quoteSelfServiceRT.Id//,
                //SBQQ__PrimaryContact__c = currentUserContact.Id//,
                //SBQQ__Account = currentUserContact.Account.Id
            ));
        }
        
     /*   // Create new sharing object for the custom object Job.
      OpportunityShare optShr  = new OpportunityShare();
   
      // Set the ID of record being shared.
      optShr.OpportunityId  = opportunities[0].Id;
        
      // Set the ID of user or group being granted access.
      optShr.UserOrGroupId = '005n0000002h8gP';
        
      // Set the access level.
      optShr.OpportunityAccessLevel = 'Edit';
        
      // Set rowCause to 'manual' for manual sharing.
      // This line can be omitted as 'manual' is the default value for sharing objects.
      optShr.RowCause = Schema.OpportunityShare.RowCause.Manual;
        
      // Insert the sharing record and capture the save result. 
      // The false parameter allows for partial processing if multiple records passed 
      // into the operation.
      //Database.SaveResult sr = Database.insert(optShr,false);
      insert optShr;
        */

        // udpate quotes if needed
        if(quotesToUpdate.size() > 0) {
            update quotesToUpdate;
        }

     
       
 
       //ATG_QuoteTriggerHelper.updateOpptyOwner(opptyIdSet);
    }
    
   // @future(callout=true)
   /* public static void updateOpptyOwner(Set<Id> opptyIdSet){
        system.debug('updateOpptyOwner future ==>'+opptyIdSet);
        List<Opportunity> opptyLstToUpdate = new List<Opportunity>();
        Id genericOwnerId = ATS_Util.getSelfServiceUserId();
        for(Opportunity oppty : [Select id,name,ownerId from Opportunity where id in: opptyIdSet]){
            
            oppty.OwnerId = genericOwnerId;
            opptyLstToUpdate.add(oppty);
            system.debug('oppty loop'+oppty);
        }
        
        system.debug('opptyLstToUpdate ==>'+opptyLstToUpdate);
        update opptyLstToUpdate;
    }*/

    /**
     * Create opportunities from quotes
     * Opportunities are only created if the quote does not already
     * have a specified opportunity
     * @param quotes [description]
     */
    private static void updateOppFromQuoteLineCreation(set<id> quoteLineId) {
        
        List<Opportunity> opportunities = new List<Opportunity>();
        
        set<id> oppotyIds = new set<Id>();
        for(SBQQ__QuoteLine__c quoteLine : [select SBQQ__Quote__r.SBQQ__Opportunity2__c,SBQQ__Quote__r.SBQQ__Opportunity2__r.StageName,SBQQ__Quote__r.SBQQ__Opportunity2__r.Probability from SBQQ__QuoteLine__c where id in:quoteLineId]) {
            // If quote does not have an opportunity, create a new one
            if(quoteLine.SBQQ__Quote__r.SBQQ__Opportunity2__c <> null) {

                oppotyIds.add(quoteLine.SBQQ__Quote__r.SBQQ__Opportunity2__c);                
            }
        }
        if(oppotyIds.size()>0)
            ATS_Util.updateOppStage(oppotyIds, 'In Negotiation', 'Draft', 50);
        

                /*opportunity op = quoteLine.SBQQ__Quote__r.SBQQ__Opportunity2__r;
                op.StageName = 'In Negotiation';
                op.Probability = 50;
                */
        //upsert opportunities;
        // Update quotes to primary and add opportunity to quote
        /*for(Opportunity opp : opportunities) {
            opp.Probability =50;
        }
        upsert opportunities;
        */
    }

}