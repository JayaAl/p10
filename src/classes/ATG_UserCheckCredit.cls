public without sharing class ATG_UserCheckCredit {
	

	
	@future
	public static void creditVerificationInitiation(id accountId)
	{

		try
		{
		    
			creditVerificationInitiation.applyForCreditInForseva(accountId,''); 
		
		}
		catch(exception ex)
		{
		    //Exception to be handled
		    system.debug(ex.getStackTraceString());
		}

	}
}