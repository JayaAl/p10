// Modified Version : 1.1
// Modified By : Jaya Alaparthi
// Modified Date: 1/31/2018
// Modified Purpose: Moving to Lightning Knowledge. Change the Object from ATS_Community__kav to Knowledge__kav with record type
public class ATS_FAQCtrlr {

    public Map<String,List<Knowledge__kav>> topicIdtoArticleLstMap {get;set;}
    public Set<String> topicNameSet {get;set;}
    public static String ARTICLE_RT = 'ATSCommunity';
    public static String OBJ_TYPE = 'Knowledge__kav';

    public ATS_FAQCtrlr(){      
        getArticles();
    }

    public void getArticles(){

        topicIdtoArticleLstMap = new Map<String,List<Knowledge__kav>>();
        
        // v1.1
        // retirive record types
        RecordType articleRT =  [SELECT Id 
                                FROM RecordType 
                                WHERE SobjectType =: OBJ_TYPE AND DeveloperName =:ARTICLE_RT];
        System.debug('articleRT:'+articleRT);
        // v1.1
        // fields not avilable ArticleType
        for(Knowledge__kav atsCmtyVar : [SELECT ArticleNumber,Body__c,Id,KnowledgeArticleId,SourceId,Summary,Title,UrlName,
                                            (SELECT Id,TopicId, Topic.Name from TopicAssignments) 
                                        FROM Knowledge__kav where PublishStatus = 'Online' 
                                        AND Language = 'en_US' 
                                        AND RecordTypeId =: articleRT.Id
                                        Order by Createddate asc ]){
            
            for(TopicAssignment topic: atsCmtyVar.TopicAssignments){
                if(topicIdtoArticleLstMap.containsKey(topic.Topic.Name))
                    (topicIdtoArticleLstMap.get(topic.Topic.Name)).add(atsCmtyVar);
                else
                    topicIdtoArticleLstMap.put(topic.Topic.Name,new List<Knowledge__kav>{atsCmtyVar});                    
            }           
        }   
        
        topicNameSet = new Set<String>();
        topicNameSet.add('Getting Started');
        topicNameSet.add('Advertising On Pandora');
        topicNameSet.add('Billing & Payment');
        
    }
}