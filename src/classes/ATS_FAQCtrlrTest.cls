@isTest(seeAllData=false)
private class ATS_FAQCtrlrTest {
    
    private static void setupData(){

        Knowledge__kav articleVar;
        List<Knowledge__kav> articleLst = new List<Knowledge__kav>();

        for(Integer ind = 0 ; ind < 2 ; ind ++){
            articleVar = new Knowledge__kav();
            articleVar.Body__c = 'Test Body'+ind;
            articleVar.title = 'test title'+ind;
            articleVar.urlname = 'test-title'+ind;
            articleLst.add(articleVar);
        }

        insert articleLst;

        Topic topicVar = new Topic();
        topicVar.Name = 'Getting Started';
        insert topicVar;

    }

    @isTest static void constructorTest() {
        
        setupData();
        ATS_FAQCtrlr ctrlr = new ATS_FAQCtrlr();

    }
    
}