/**
* ─────────────────────────────────────────────────────────────────────────────────────────────────┐
* @changes
* v1.1            Jaya Alaparthi
* 2017-10-19      
* @Description
*  Adding logic to retrive tokent fro Birst user
* ─────────────────────────────────────────────────────────────────────────────────────────────────┘
*/
public with sharing class ATS_HomeController {

    public List<SBQQ__Quote__c> quoteList{get;set;}
    public id accountId{get;set;}
    public id quoteId{get;set;}
    public string loggedInUserEmail{get;set;}
    public String campaignIdtoDel {get;set;}
    public String token{get;set;}
    public String hiddenFrame{get;set;}
    
    // v1.1
    //Commented after talking to Jaya
    public PageReference getBirstToken() {

        // get user Record
        token = 'in get token';
        Id userRecId = UserInfo.getUserId();
        
        Datetime current = System.now();
        Date currDate = current.date();
        Time currTime = current.time();
        Datetime localBirst = datetime.newInstance(currDate,currTime); 

        // send http request to birst to retrive token
        User userRec = new User(Id = userRecId);
        String birstToken = birstCallout();
        userRec.BirstToken__c = birstToken;
        userRec.LastBirstUpdated__c = localBirst;
        try{
            if(!String.isBlank(birstToken)) {
                update userRec;
                token = 'userRec is update for userId :'+userRec.Id;
            }
        } catch(Exception e) {token = e.getMessage();}
        return null;
    }
    public PageReference NewCampaign() {
        this.accountId = ApexPages.currentPage().getParameters().get('accountId');
        /*Pagereference pageRef;
        loggedInUserEmail = UserInfo.getUserEmail();
        if(loggedInUserEmail==null || loggedInUserEmail==''){
            pageRef = new PageReference('https://pandora10-pandoraforbrands.cs30.force.com/ads/s/login');
            return pageRef;
        }
        if(accountId==null)
        {
            
            Id loggedInUserId = UserInfo.getUserId();
            User userObj = [Select id,contactId from User where id =: loggedInUserId ];
            system.debug('Contact is ::: '+userObj.contactId);
            system.debug('loggedInUserEmail' + loggedInUserEmail); 
            for(contact c:[select accountId from contact where Id =:userObj.contactId limit 1])
                accountId = c.accountId;

            if(accountId == null)
                pageRef = new PageReference('https://pandora10-pandoraforbrands.cs30.force.com/ads/s/login');
            else {
                pageRef = new PageReference('/apex/ATG_NewQuote?accountId='+accountId);
            }
                //accountId = '001n000000KTXYV';

        } 
        return pageRef;  */

        return ATS_Util.isValidSession();
    }

    /*Commented after talking to Jaya*/
    private String birstCallout() {

        token = 'in callout';
        HttpRequest req = new HttpRequest();
        accountId = ApexPages.currentPage().getParameters().get('accountId');
        Account acct = new Account(Id = accountId);
        //String urlHost = new ApexPages.StandardController(acct).view().getHeaders().get('Host');
        String urlHost = URL.getSalesforceBaseUrl().toExternalForm();
        urlHost += '/ads/services/Soap/c/25.0/'+UserInfo.getOrganizationId();
        req.setEndpoint('https://login.bws.birst.com/TokenGenerator.aspx?');
        req.setMethod('POST');
        req.setHeader('Content-type', 'application/x-www-form-urlencoded');
        System.debug('urlHost:'+urlHost);
        String body = 'birst.username='+Label.CommunityBirstUser+
                       '&birst.ssopassword='+Label.CommunityBirstPWD+
                       '&birst.SpaceId='+Label.CommunityBirstSpaceId+
                       '&serverurl='+urlHost;
        
        req.setBody(body);
       
        // Create a new http object to send the request object
        // A response object is generated as a result of the request  
      
        Http http = new Http();
        HTTPResponse res = http.send(req);
        token = 'resq:'+req+'res'+res+'res.getBody():'+res.getBody();
        String respStr = res.getBody();
        System.debug(res.getBody());
        token = 'Before returning back:'+respStr;

        // rest callout for dashboard
        HttpRequest dashboardReq = new HttpRequest();
        Http dashHttp = new Http();
        //+Label.CommunityBirstDashboard+
        String resStr = 'https://login.bws.birst.com/SSO.aspx?birst.module=dashboard'+
                        '&birst.hideHeader=true'+
                    '&birst.embedded=true'+
                    '&birst.hideDashboardNavigation=true'+
                    '&birst.hideSubHeader=true'+
                    '&birst.viewMode=headerless'+
                    '&birst.hideDashboardPrompts=true'+
                    '&birst.dashboard='+Label.CommunityBirstDashboard+
                    '&birst.page='+Label.CommunityBirstPage+'&birst.SSOToken='+respStr+
                    '&birst.filters=Opportunity.Primary%20Billing%20Contact=003n000000NaPDh';
                    //'&serverurl='+urlHost;
        //dashboardReq.setEndpoint(resStr);
        dashboardReq.setMethod('GET');
        dashboardReq.setHeader('Content-type', 'application/x-www-form-urlencoded');
        dashboardReq.setBody(resStr);
        //HTTPResponse dashboardResp = http.send(dashboardReq);
        /*hiddenFrame = '<iframe scrolling="no" "width="0"  height="0px"'
                        +' id="ifId1" name="frame" style="display:none;opacity: 0.5;"'
                        +' src="' + resStr +'frameBorder="0"/>';*/
        hiddenFrame = resStr;

        return respStr;
    }
  
}