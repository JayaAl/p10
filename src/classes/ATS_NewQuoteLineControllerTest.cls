/*
* ─────────────────────────────────────────────────────────────────────────────────────────────────
* @modifiedBy     Awanish Kumar <akumar3@pandora.com>
* @version        1.1
* @modified       2018-01-31
* @changes        ESS-41985
* @Description    After creation of duplicate rule,few test classes were failing.
                  Eventually we realized the need to optimize utility test class.
                  Method to create record has been moved to Utility class and insert
                  the record to this class.
                  
* ─────────────────────────────────────────────────────────────────────────────────────────────────┘
*/
@isTest
private class ATS_NewQuoteLineControllerTest
{   

    enum PortalType { CSPLiteUser, PowerPartner, PowerCustomerSuccess, CustomerSuccess }
    public static User getPortalUser(PortalType portalType, User userWithRole, Boolean doInsert) {
    
        /* Make sure the running user has a role otherwise an exception 
           will be thrown. */
        if(userWithRole == null) {   
            
            if(UserInfo.getUserRoleId() == null) {

                UserRole r = new UserRole(name = 'TEST ROLE');
                Database.insert(r);
                
                userWithRole = new User(alias = 'hasrole', email='userwithrole@roletest1.com', userroleid = r.id,
                                    emailencodingkey='UTF-8', lastname='Testing', languagelocalekey='en_US', 
                                    localesidkey='en_US', profileid = '00en0000000M2Ai', 
                                    timezonesidkey='America/Los_Angeles', username='userwithrole@testorg.com');
            } else {
                userWithRole = new User(Id = UserInfo.getUserId(), UserRoleId = UserInfo.getUserRoleId());
            }
            
            
            System.assert(userWithRole.userRoleId != null, 
                          'This test requires the target org to have at least one UserRole created. Please create a user role in this organization and try again.');
        }

        Account a;
        Contact c;
        System.runAs(userWithRole) {

            // V1.1 Added parameter name as identifier to the class and create record with genearte method
            // Create Account
            a = UTIL_TestUtil.generateAccount('testAccount');
            //test.starttest();
            a.Name = a.Name+'Adv'; 
            a.Type = 'Advertiser';
            
            insert a;
            system.debug('THIS IS ACCOUNTID' + a.id);
            //Contact newContact = UTIL_TestUtil.newContact();
           // V1.1 Added parameter name as identifier to the class and create record with genearte method
            c = UTIL_TestUtil.generateContact('testContact',a.Id);
            insert c;
            //test.stoptest();
            system.debug('THIS IS contact' + c);


        }
        
        /* Get any profile for the given type.*/
        Profile p = [select id 
                      from profile 
                     where usertype = :portalType.name() and  name = 'Partner Community Login User'
                     limit 1];   
        
        String testemail = 'puser00021@amamama.com';
        User pu = new User(profileId = p.id, username = testemail, email = testemail, 
                           emailencodingkey = 'UTF-8', localesidkey = 'en_US', 
                           languagelocalekey = 'en_US', timezonesidkey = 'America/Los_Angeles', 
                           alias='cspu', lastname='lastname', contactId = c.id);
        
        if(doInsert) {
            Database.insert(pu);
        }
        
        system.debug('THIS IS USER' + pu);
        return pu;
    }

    @testSetup static void setupData(){

        User thisUser = [ select Id from User where Id = :UserInfo.getUserId()];
        User pu;
        Product2 prod;
        System.runAs (thisUser) {

            //test.startTest();
            // First, set up test price book entries.
            // Insert a test product.
            prod = new Product2(Name = 'Display', 
                Family = 'Hardware');
            insert prod;
            
            // Get standard price book ID.
            // This is available irrespective of the state of SeeAllData.
            Id pricebookId = Test.getStandardPricebookId();
            
            // 1. Insert a price book entry for the standard price book.
            // Standard price book entries require the standard price book ID we got earlier.
            PricebookEntry standardPrice = new PricebookEntry(
                Pricebook2Id = pricebookId, Product2Id = prod.Id,
                UnitPrice = 10000, IsActive = true);
            insert standardPrice;
            
            // Create a custom price book
            Pricebook2 customPB = new Pricebook2(Name='Standard Price Book', isActive=true);
            insert customPB;
            
            // 2. Insert a price book entry with a custom price.
            PricebookEntry customPrice = new PricebookEntry(
                Pricebook2Id = customPB.Id, Product2Id = prod.Id,
                UnitPrice = 12000, IsActive = true);
                insert customPrice;

            ATG_State__c state = new ATG_State__c(ATG_State_Code__c='CA');
                insert state;

            ATG_County__c county = new ATG_County__c(ATG_County_Name__c='Santa Clara',ATG_State__c = state.id);
                insert county;

            ATG_DMA__c dma = New ATG_DMA__c(Name = 'Fremont');
            insert dma;

            
            //Pricebook2 pb2 = new Pricebook2();
            //pb2 = [select Id, Name, IsActive from PriceBook2 where id=:pricebookId];
            //system.debug('THIS IS' + pb2);
            //Set<String> customerUserTypes = new Set<String> {'CSPLiteUser', 'PowerPartner', 'PowerCustomerSuccess',   'CustomerSuccess'};

            //Profile p = [select Id,name from Profile where UserType in: customerUserTypes  and name = 'Partner Community Login User' limit 1];

            
            //String useremail = 'puser00021@amamama.com';
            //pu = [select id,profileId,username,email,emailencodingkey,localesidkey,languagelocalekey,timezonesidkey,alias,lastname,contactId from user where email =: useremail];
            
            pu = getPortalUser(PortalType.PowerPartner, null, true);
            //test.stoptest();


        }
            

            
        system.debug('THIS IS USER' + pu);
        System.assert([select isPortalEnabled 
                         from user 
                        where id = :pu.id].isPortalEnabled,
                      'User was not flagged as portal enabled.');       
            
        System.RunAs(pu) {
            test.starttest();
            user uu = [select isPortalEnabled,contactid,AccountId,contact.accountId
                             from user 
                            where id = :UserInfo.getUserId()];
            

            system.debug('newUser.contact **** ' +uu.contactId );

            system.debug('newUser.account **** ' +uu.accountId );
            
            SBQQ__Quote__c quote = new SBQQ__Quote__c();
            quote.ATG_Campaign_Name__c = 'This is Test Campaign Setup';


            Date startDate =  Date.today(). addDays(4); //Give your date
    
            Date lastDate = startDate.addDays(date.daysInMonth(startDate.year() , startDate.month())  - 1);
            system.debug(startDate  + ' **** ' +lastDate );
            //system.debug('newUser.contact.accountId **** ' +c.accountId );
            
            quote.SBQQ__Account__c = uu.accountId;
            quote.SBQQ__StartDate__c = startDate;
            quote.SBQQ__EndDate__c = lastDate;
            quote.SBQQ__ExpirationDate__c = lastDate;
            quote.OwnerID = uu.Id; 

            //test.startTest();
            insert quote;

            SBQQ__QuoteLine__c quoteLine = new SBQQ__QuoteLine__c();
            quoteLine.ATG_Gender__c = 'Male';
            quoteLine.SBQQ__Product__c = prod.id;
            quoteLine.atg_age__C = '13-17';
            quoteLine.ATG_Budget__c = 2000;
            quoteLine.SBQQ__Quote__c = quote.Id;
            quoteLine.SBQQ__StartDate__c = startDate;
            quoteLine.SBQQ__EndDate__c = lastDate;
            quoteLine.ATG_DMA_County__c = 'dma';

            insert quoteLine;
            test.stopTest();
            system.assertNotEquals(quote.id,null);
            system.assertNotEquals(quoteLine.id,null);

        }
    }

        private static testMethod void ATS_CreateQuoteLineTest()
        {
            User thisUser = [ select Id from User where Id = :UserInfo.getUserId() ];
            string campaignName = 'This is Test Campaign Setup';
            SBQQ__Quote__c quote1 = [select id from SBQQ__Quote__c where ATG_Campaign_Name__c= :campaignName limit 1];
            product2 prod = [select id from product2 limit 1];

            User pu;
     
            System.runAs ( thisUser ) {
                String useremail = 'puser00021@amamama.com';
                pu = [select id,profileId,username,email,emailencodingkey,localesidkey,languagelocalekey,timezonesidkey,alias,lastname,contactId from user where email =: useremail];
                
            }
        
            system.debug('THIS IS USER' + pu);
            System.assert([select isPortalEnabled 
                             from user 
                            where id = :pu.id].isPortalEnabled,
                          'User was not flagged as portal enabled.');       
        
            System.RunAs(pu) {
                user uu = [select isPortalEnabled,contactid,AccountId,contact.accountId
                                 from user 
                                where id = :UserInfo.getUserId()];
                

                system.debug('newUser.contact **** ' +uu.contactId );

                system.debug('newUser.account **** ' +uu.accountId );
                
                System.assertNotEquals(quote1.Id,NULL);

                //system.debug('quote inserted' + ats.quote);
                        
                system.debug('I AM inside HERE');
                Test.setCurrentPageReference(new PageReference('Page.ATG_QuoteLine')); 
                System.currentPageReference().getParameters().put('QID', quote1.id);
                System.currentPageReference().getParameters().put('selectedValue', 'DMA');
                
                ATG_QuoteLineController quoteLineObj = new ATG_QuoteLineController();
                quoteLineObj.isEditMode = false;
                quoteLineObj.quoteLineError = false;
                quoteLineObj.ProductOptionValue = prod.id;
                list<string> ageList = new List<string>();
                ageList.Add('13-17');
                quoteLineObj.age = ageList;
                quoteLineObj.GenderOptionValue = 'Male';
                quoteLineObj.product = prod;
                quoteLineObj.spend = '10000';
                quoteLineobj.selectedMarketOption = 'DMA';
                test.starttest();
                quoteLineObj.save();
                test.stoptest();
                
                
                //System.assertNotEquals(ats.opptyIdParam,NULL);
            }
        }


        private static testMethod void ATS_NewQuoteLineControllerEditTest()
        {
                string campaignName = 'This is Test Campaign Setup';
                SBQQ__Quote__c quote = [select id from SBQQ__Quote__c where ATG_Campaign_Name__c= :campaignName limit 1];
                //SBQQ__QuoteLine__c quoteLine1 = [select id from SBQQ__QuoteLine__c where SBQQ__Quote__r.ATG_Campaign_Name__c= :campaignName limit 1];
                SBQQ__QuoteLine__c quoteLineEdit = [select id,ATG_Pandora_s_Audience__c,SBQQ__Quote__r.ATG_Campaign_Status__c, ATG_Gender__c,SBQQ__Product__c,atg_age__C,ATG_Budget__c,SBQQ__StartDate__c,SBQQ__EndDate__c,ATG_DMA_County__c,ATG_Audience_Rating__c from SBQQ__QuoteLine__c where SBQQ__Quote__r.ATG_Campaign_Name__c= :campaignName limit 1];
                        
                product2 prod = [select id from product2 limit 1];
                if(quote!=null){


                    test.starttest();
                    user pu = [select id,name from user where email=:'puser00021@amamama.com' limit 1];
                    system.runAs(pu){
                        Test.setCurrentPageReference(new PageReference('Page.ATG_QuoteLine')); 
                        System.currentPageReference().getParameters().put('QID', quote.Id);
                        System.currentPageReference().getParameters().put('QLID', quoteLineEdit.Id);
                        System.currentPageReference().getParameters().put('selectedValue', 'DMA');
                        ATG_QuoteLineController quoteLineEditObj = new ATG_QuoteLineController();
                        quoteLineEditObj.ProductOptionValue = prod.id;
                        list<string> ageListEdit = new List<string>();
                        ageListEdit.Add('13-17');
                        quoteLineEditObj.age = ageListEdit;
                        quoteLineEditObj.GenderOptionValue = 'Female';
                        quoteLineEditObj.product = prod;
                        quoteLineEditObj.spend = '10000';
                        quoteLineEditObj.selectedMarketOption = 'DMA';
                        quoteLineEditObj.save();
                        quoteLineEditObj.cancel();
                        quoteLineEditObj.calculateReachScore();
                        quoteLineEditObj.next();
                        

                     }
                     test.stoptest();


                     


                    
                }


        }



        private static testMethod void ATS_NewQuoteLineControllerEditDMATest()
        {
                string campaignName = 'This is Test Campaign Setup';
                SBQQ__Quote__c quote = [select id from SBQQ__Quote__c where ATG_Campaign_Name__c= :campaignName limit 1];
                SBQQ__QuoteLine__c quoteLineEdit = [select id,ATG_Pandora_s_Audience__c,SBQQ__Quote__r.ATG_Campaign_Status__c, ATG_Gender__c,SBQQ__Product__c,atg_age__C,ATG_Budget__c,SBQQ__StartDate__c,SBQQ__EndDate__c,ATG_DMA_County__c,ATG_Audience_Rating__c from SBQQ__QuoteLine__c where SBQQ__Quote__r.ATG_Campaign_Name__c= :campaignName limit 1];
                
                product2 prod = [select id from product2 limit 1];
                ATG_State__C state = [select id from ATG_State__c limit 1];
                ATG_DMA__C dma = [select id from ATG_DMA__C limit 1];
                if(quote!=null){


                    test.starttest();
                    user pu = [select id,name from user where email=:'puser00021@amamama.com' limit 1];
                    system.runAs(pu){
                        Test.setCurrentPageReference(new PageReference('Page.ATG_QuoteLine')); 
                        System.currentPageReference().getParameters().put('QID', quote.Id);
                        System.currentPageReference().getParameters().put('QLID', quoteLineEdit.Id);
                        System.currentPageReference().getParameters().put('selectedValue', 'DMA');
                        ATG_QuoteLineController quoteLineEditObj = new ATG_QuoteLineController();
                        quoteLineEditObj.ProductOptionValue = prod.id;
                        list<string> ageListEdit = new List<string>();
                        ageListEdit.Add('13-17');
                        quoteLineEditObj.age = ageListEdit;
                        quoteLineEditObj.GenderOptionValue = 'Male';
                        quoteLineEditObj.product = prod;
                        quoteLineEditObj.spend = '10000';
                        quoteLineEditObj.selectedMarketOption = 'DMA';
                        quoteLineEditObj.treeContext = 'dma';
                        quoteLineEditObj.selectedCountylst = 'Fremont,';
                        quoteLineEditObj.save();
                        ATG_Quote_Line_GeoData__c QLIGeoVar = new ATG_Quote_Line_GeoData__c();
                        QLIGeoVar.ATG_Quote_Line__c = quoteLineEdit.Id;
                        QLIGeoVar.ATG_dma__c = dma.Id;
                        insert QLIGeoVar;
                        quoteLineEditObj.getProductDetails();
                        quoteLineEditObj.getItems();
                        quoteLineEditObj.getAges();
                        quoteLineEditObj.selectedOption();
                        quoteLineEditObj.insertQLILocationAction();
                        //List<gaugeData> = quoteLineEditObj.data;
                        quoteLineEditObj.ProductOptionValue();
                        quoteLineEditObj.GenderrOptionValue();
                        quoteLineEditObj.setGender('Male');
                        string gender = quoteLineEditObj.getGender();
                        string[] age =new string[]{'13-17'};
                        

                        quoteLineEditObj.setAge(age);
                        age = quoteLineEditObj.getAge();
                        //quoteLineEditObj.gaugeData gd = new quoteLineEditObj.gaugeData('Score',4);
                        
                        product2 products = quoteLineEditObj.products;
                        ATG_QuoteLineController.getDMAValues('true',quoteLineEdit.Id);
                        ATG_QuoteLineController.getGeoCodeValues('true',quoteLineEdit.Id);
                        ATG_QuoteLineController.getCounties(state.id,'true',quoteLineEdit.Id);

                        

                     }
                     test.stoptest();

                    
                }


        }




        private static testMethod void ATS_NewQuoteLineControllerEditGEOTest()
        {
                string campaignName = 'This is Test Campaign Setup';
                SBQQ__Quote__c quote = [select id from SBQQ__Quote__c where ATG_Campaign_Name__c= :campaignName limit 1];
                SBQQ__QuoteLine__c quoteLineEdit = [select id,ATG_Pandora_s_Audience__c,SBQQ__Quote__r.ATG_Campaign_Status__c, ATG_Gender__c,SBQQ__Product__c,atg_age__C,ATG_Budget__c,SBQQ__StartDate__c,SBQQ__EndDate__c,ATG_DMA_County__c,ATG_Audience_Rating__c from SBQQ__QuoteLine__c where SBQQ__Quote__r.ATG_Campaign_Name__c= :campaignName limit 1];
                product2 prod = [select id from product2 limit 1];
                //ATG_State__C state = [select id from ATG_State__c limit 1];
                ATG_County__C county = [select id,ATG_State__c from ATG_County__C limit 1];
                if(quote!=null){


                    test.starttest();
                    user pu = [select id,name from user where email=:'puser00021@amamama.com' limit 1];
                    system.runAs(pu){
                        Test.setCurrentPageReference(new PageReference('Page.ATG_QuoteLine')); 
                        System.currentPageReference().getParameters().put('QID', quote.Id);
                        System.currentPageReference().getParameters().put('QLID', quoteLineEdit.Id);
                        System.currentPageReference().getParameters().put('selectedValue', 'GEO');
                        ATG_QuoteLineController quoteLineEditObj = new ATG_QuoteLineController();
                        quoteLineEditObj.ProductOptionValue = prod.id;
                        list<string> ageListEdit = new List<string>();
                        ageListEdit.Add('13-17');
                        quoteLineEditObj.age = ageListEdit;
                        quoteLineEditObj.GenderOptionValue = 'Male';
                        quoteLineEditObj.product = prod;
                        quoteLineEditObj.spend = '10000';
                        quoteLineEditObj.selectedMarketOption = 'GEO';
                        quoteLineEditObj.treeContext = 'geo';
                        quoteLineEditObj.selectedCountylst = 'Santa Clara,';
                        quoteLineEditObj.save();
                        ATG_Quote_Line_GeoData__c QLIGeoVar = new ATG_Quote_Line_GeoData__c();
                        QLIGeoVar.ATG_Quote_Line__c = quoteLineEdit.Id;
                        QLIGeoVar.ATG_County__c = county.Id;
                        QLIGeoVar.ATG_State__c = county.ATG_State__c;
                        insert QLIGeoVar;
                        quoteLineEditObj.insertQLILocationAction();
                        product2 products = quoteLineEditObj.products;
                        ATG_QuoteLineController.getDMAValues('true',quoteLineEdit.Id);
                        ATG_QuoteLineController.getGeoCodeValues('true',quoteLineEdit.Id);
                        ATG_QuoteLineController.getCounties(county.ATG_State__c,'true',quoteLineEdit.Id);

                        

                     }
                     test.stoptest();


                     


                    
                }


        }
    


}