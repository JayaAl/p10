public with sharing class ATS_OrderSummary {
    public id opptyIdParam{get; set;}
    public List<SBQQ__QuoteLine__c> quoteLines{get;set;}
    private SBQQ__Quote__c quote;
    public string contactName{get;set;}
    public string contactAddress{get;set;}
    public string contactPhone{get;set;}
    private string docURL;
    public string getDocURL()
    {
        List<Document> lstDocument = [Select Id,Name,LastModifiedById from Document where Name = 'ATS_P_LOGO_EmailTemplate' limit 1];
        string strOrgId = UserInfo.getOrganizationId();
        string orgInst = URL.getSalesforceBaseUrl().getHost();
        orgInst = orgInst.substring(0, orgInst.indexOf('.')) + '.content.force.com'; //orgInst = 'na14.content.force.com' in my org
        docURL = URL.getSalesforceBaseUrl().getProtocol() + '://c.' + orgInst + '/servlet/servlet.ImageServer?id=' + lstDocument[0].Id + '&oid=' + strOrgId;
        return docURL;
        
    }


    public SBQQ__Quote__c getQuote(){

        if(opptyIdParam==null)
            return null;

        quote = [select id,Name,SBQQ__Opportunity2__r.ATG_Campaign_Status__c,ATG_Campaign_Name__c,SBQQ__StartDate__c,SBQQ__EndDate__c,ATG_BudgetTotal__c, SBQQ__ExpirationDate__c ,SBQQ__Account__r.Id, SBQQ__Account__r.Name,ATG_Campaign_Status__c,SBQQ__Account__r.Credit_Status__c,SBQQ__Account__r.Credit_Approved_Amount__c from SBQQ__Quote__c where SBQQ__Opportunity2__c=:opptyIdParam];
        
        //system.debug('THIS SI THE OPPORTUNITY TO BE TESTED' + quote);

        creditStatus = quote.SBQQ__Account__r.Credit_Status__c;
        creditApprovedAmount = (quote.SBQQ__Account__r.Credit_Approved_Amount__c != null ) ? quote.SBQQ__Account__r.Credit_Approved_Amount__c : 0;
        accountId = quote.SBQQ__Account__r.Id;
        
        quoteLines  = new List<SBQQ__QuoteLine__c>();
        quoteLines = [select id,name, SBQQ__Product__c, SBQQ__Product__r.Name,SBQQ__Quote__r.SBQQ__Account__c,SBQQ__Quote__r.ATG_Campaign_Name__c,SBQQ__Quote__r.ATG_Campaign_Status__c,SBQQ__Quote__r.SBQQ__StartDate__c,SBQQ__Quote__r.SBQQ__EndDate__c,SBQQ__Quote__r.SBQQ__Account__r.Name, SBQQ__ProductCode__c, SBQQ__StartDate__c, SBQQ__EndDate__c, ATG_Budget__c,SBQQ__ListPrice__c FROM SBQQ__QuoteLine__c where SBQQ__Quote__c=:quote.Id ORDER BY createddate ASC]; 
        return quote;
    }
    
    
    public id accountId{get;set;}
    public string creditStatus{get;set;}
    public decimal creditApprovedAmount{get;set;}





    
    public ATS_OrderSummary() {

        system.debug('THIS SI THE OPPORTUNITY TO BE TESTED' + opptyIdParam);

        /*if(opptyIdParam==null)
            return;
        */ 
        //opptyIdParam = '006n0000006EsXr';   
        
        system.debug('THIS SI THE OPPORTUNITY TO BE TESTED' + quoteLines);
        
        
    }
}