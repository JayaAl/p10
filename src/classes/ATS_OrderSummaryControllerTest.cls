/*
* ─────────────────────────────────────────────────────────────────────────────────────────────────
* @modifiedBy     Awanish Kumar <akumar3@pandora.com>
* @version        1.1
* @modified       2018-01-31
* @changes        ESS-41985
* @Description    After creation of duplicate rule,few test classes were failing.
                  Eventually we realized the need to optimize utility test class.
                  Method to create record has been moved to Utility class and insert
                  the record to this class.
                  
* ─────────────────────────────────────────────────────────────────────────────────────────────────┘
*/
@isTest
private class ATS_OrderSummaryControllerTest
{
    enum PortalType { CSPLiteUser, PowerPartner, PowerCustomerSuccess, CustomerSuccess }
    public static User getPortalUser(PortalType portalType, User userWithRole, Boolean doInsert) {
    
        /* Make sure the running user has a role otherwise an exception 
           will be thrown. */
        if(userWithRole == null) {   
            
            if(UserInfo.getUserRoleId() == null) {

                UserRole r = new UserRole(name = 'TEST ROLE');
                Database.insert(r);
                
                userWithRole = new User(alias = 'hasrole', email='userwithrole@roletest1.com', userroleid = r.id,
                                    emailencodingkey='UTF-8', lastname='Testing', languagelocalekey='en_US', 
                                    localesidkey='en_US', profileid = '00en0000000M2Ai', 
                                    timezonesidkey='America/Los_Angeles', username='userwithrole@testorg.com');
            } else {
                userWithRole = new User(Id = UserInfo.getUserId(), UserRoleId = UserInfo.getUserRoleId());
            }
            
            
            System.assert(userWithRole.userRoleId != null, 
                          'This test requires the target org to have at least one UserRole created. Please create a user role in this organization and try again.');
        }

        Account a;
        Contact c;
        System.runAs(userWithRole) {

           // V1.1 Added parameter name as identifier to the class and create record with genearte method
            // Create Account
            a = UTIL_TestUtil.generateAccount('testAccount');
            a.Name = a.Name+'Adv'; 
            a.Type = 'Advertiser';
            
            insert a;
            system.debug('THIS IS ACCOUNTID' + a.id);
            //Contact newContact = UTIL_TestUtil.newContact();
           // V1.1 Added parameter name as identifier to the class and create record with genearte method
            c = UTIL_TestUtil.generateContact('testContact',a.Id);
            insert c;
            system.debug('THIS IS contact' + c);


        }
        
        /* Get any profile for the given type.*/
        Profile p = [select id 
                      from profile 
                     where usertype = :portalType.name() and  name = 'Partner Community Login User'
                     limit 1];   
        
        String testemail = 'puser000@amamama.com';
        User pu = new User(profileId = p.id, username = testemail, email = testemail, 
                           emailencodingkey = 'UTF-8', localesidkey = 'en_US', 
                           languagelocalekey = 'en_US', timezonesidkey = 'America/Los_Angeles', 
                           alias='cspu', lastname='lastname', contactId = c.id);
        
        if(doInsert) {
            Database.insert(pu);
        }
        
        system.debug('THIS IS USER' + pu);
        return pu;
    }


    private static void insertDocument(){

        Document document;

        document = new Document();
        document.Body = Blob.valueOf('Some Text');
        document.ContentType = 'application/pdf';
        document.DeveloperName = 'ATS_P_LOGO_EmailTemplate1';
        document.IsPublic = true;
        document.Name = 'ATS_P_LOGO_EmailTemplate';
        document.FolderId = UserInfo.getUserId();//[select id from folder limit 1].id;//where name = 'My Test Docs'].id;
        insert document;
    }
    private static testMethod void ATS_OrderSummary_getDocURLTest() {

        test.startTest();
            insertDocument();

        test.stoptest();
        
        ATS_OrderSummary ats = new ATS_OrderSummary();
        
        string docUrl = ats.getDocURL();
        System.assertNotEquals(docUrl,NULL);
    }

    private static testMethod void ATS_OrderSummary_getQuote()
    {
        User thisUser = [ select Id from User where Id = :UserInfo.getUserId() ];
        User pu;
 
        System.runAs ( thisUser ) {


        // First, set up test price book entries.
        // Insert a test product.
        Product2 prod = new Product2(Name = 'Laptop X200', 
            Family = 'Hardware');
        insert prod;
        
        // Get standard price book ID.
        // This is available irrespective of the state of SeeAllData.
        Id pricebookId = Test.getStandardPricebookId();
        
        // 1. Insert a price book entry for the standard price book.
        // Standard price book entries require the standard price book ID we got earlier.
        PricebookEntry standardPrice = new PricebookEntry(
            Pricebook2Id = pricebookId, Product2Id = prod.Id,
            UnitPrice = 10000, IsActive = true);
        insert standardPrice;
        
        // Create a custom price book
        Pricebook2 customPB = new Pricebook2(Name='Standard Price Book', isActive=true);
        insert customPB;
        
        // 2. Insert a price book entry with a custom price.
        PricebookEntry customPrice = new PricebookEntry(
            Pricebook2Id = customPB.Id, Product2Id = prod.Id,
            UnitPrice = 12000, IsActive = true);
        insert customPrice;

        
        //Pricebook2 pb2 = new Pricebook2();
        //pb2 = [select Id, Name, IsActive from PriceBook2 where id=:pricebookId];
        //system.debug('THIS IS' + pb2);
        //Set<String> customerUserTypes = new Set<String> {'CSPLiteUser', 'PowerPartner', 'PowerCustomerSuccess',   'CustomerSuccess'};

        //Profile p = [select Id,name from Profile where UserType in: customerUserTypes  and name = 'Partner Community Login User' limit 1];

        pu = getPortalUser(PortalType.PowerPartner, null, true);


        }
        

        
        system.debug('THIS IS USER' + pu);
        System.assert([select isPortalEnabled 
                         from user 
                        where id = :pu.id].isPortalEnabled,
                      'User was not flagged as portal enabled.');       
        
            System.RunAs(pu) {
                user uu = [select isPortalEnabled,contactid,AccountId,contact.accountId
                                 from user 
                                where id = :UserInfo.getUserId()];
                

                system.debug('newUser.contact **** ' +uu.contactId );

                system.debug('newUser.account **** ' +uu.accountId );
                SBQQ__Quote__c quote = new SBQQ__Quote__c();
                quote.ATG_Campaign_Name__c = 'This is Test Campaign';

                Date startDate =  Date.today(). addDays(4); //Give your date
        
                Date lastDate = startDate.addDays(date.daysInMonth(startDate.year() , startDate.month())  - 1);
                system.debug(startDate  + ' **** ' +lastDate );
                //system.debug('newUser.contact.accountId **** ' +c.accountId );
                
                quote.SBQQ__Account__c = uu.accountId;
                quote.SBQQ__StartDate__c = startDate;
                quote.SBQQ__EndDate__c = lastDate;
                quote.SBQQ__ExpirationDate__c = lastDate;
                quote.OwnerID = uu.Id; 

                //test.startTest();
                    insert quote;
                //test.stopTest();

                system.debug('QUOTE DATA' + quote);


                //System.assertNotEquals(ats.opptyIdParam,NULL);
           


                list<opportunity> opt = [SELECT ID From opportunity limit 1];

                system.debug('opt DATA' + opt);

                Test.setCurrentPageReference(new PageReference('Page.ATS_OrderSummary')); 
                System.currentPageReference().getParameters().put('opportunityId', opt[0].id);//quote.SBQQ__Opportunity2__c);
                system.debug('opt[0].id' + opt[0].id);
                ATS_OrderSummary ats = new ATS_OrderSummary();
                ats.opptyIdParam = opt[0].id;//quote.SBQQ__Opportunity2__c;
                ats.contactName='';
                ats.contactAddress ='';
                ats.contactPhone='';
                ats.accountId = null;
                ats.creditStatus='';
                ats.creditApprovedAmount=null;
                system.debug('QUOTE ats.opptyIdParam' + ats.opptyIdParam);
                ats.getQuote();

            }

        }

    


}