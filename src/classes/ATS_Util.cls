public without sharing class ATS_Util {
    public ATS_Util() {
        
    }
    //CODE CONDITION ADDED BELOW FOR AGENCY FLOW//
    private static contact getContact()
    {
        Id loggedInUserId = UserInfo.getUserId();
        contact c;
        User userObj = [Select id,contactId from User where id =: loggedInUserId limit 1];
        lIST<contact> lstContact = new List<contact>();
        lstContact = [select account.type,account.Credit_Status__c,account.Available_Credit__c from contact where Id =:userObj.contactId limit 1];
        if(lstContact.size()>0){
            c = lstContact[0];
        }

        return c;
    }
    public static string getAccountCreditStatus()
    {
        //Map<string,string> accountTypeandCreditStatus  = new Map<string,string>();
        string accountCreditStatus = '';
        contact c = getContact();
        //for(contact c:[select account.type,account.Credit_Status__c from contact where Id =:userObj.contactId limit 1]){
        if(c!=null){
            //accountTypeandCreditStatus.put(c.account.type,c.account.Credit_Status__c);
            //if(c.account.Available_Credit__c>0){
            if(c.account.Credit_Status__c=='Approved' && c.account.Available_Credit__c>0){
                accountCreditStatus  = c.account.Credit_Status__c;
            }
            else {
                accountCreditStatus = 'In Review';
            }
        }
        return accountCreditStatus;

    }

    //CODE CONDITION ADDED BELOW FOR AGENCY FLOW//
    public static string getAccountType()
    {
        //Map<string,string> accountTypeandCreditStatus  = new Map<string,string>();
        string accountType = '';
        contact c = getContact();
        if(c!=null){
            //accountTypeandCreditStatus.put(c.account.type,c.account.Credit_Status__c);
            accountType  = c.account.type;
        }
        return accountType;

    }
    
    public static void updateOpptyLineItems(List<OpportunityLineItem> oliLst){

        Set<Id> QLIIdSet = new Set<Id>();
        Set<Id> productIdSet = new Set<Id>();
        Map<Id,String> productNameMap = new Map<Id,String>();

        for(OpportunityLineItem lineItemVar : oliLst){
            QLIIdSet.add(lineItemVar.SBQQ__QuoteLine__c);
            productIdSet.add(lineItemVar.Product2Id);
        }

        Map<Id,SBQQ__QuoteLine__c> selectedQLIMap = new Map<Id,SBQQ__QuoteLine__c>([Select id,SBQQ__StartDate__c,SBQQ__EndDate__c,name from SBQQ__QuoteLine__c where id in: QLIIdSet ]);
        Map<Id,Product2> productMap = new Map<Id,Product2>([Select id,Name from Product2 where id in: productIdSet]);

        for(OpportunityLineItem lineItemVar : oliLst){
            
            SBQQ__QuoteLine__c qliItemVar = selectedQLIMap.get(lineItemVar.SBQQ__QuoteLine__c);
            if(qliItemVar != null){
                lineItemVar.ServiceDate  = qliItemVar.SBQQ__StartDate__c;
                lineItemVar.End_Date__c  = qliItemVar.SBQQ__EndDate__c;
                lineItemVar.Offering_Type__c = 'Standard';
                lineItemVar.Platform__c = 'Everywhere';
                lineItemVar.Medium__c = (productMap.get(lineItemVar.Product2Id)).Name;
                lineItemVar.duration__c =  (qliItemVar.SBQQ__StartDate__c).daysBetween(qliItemVar.SBQQ__EndDate__c);
                system.debug('lineItemVar :::'+lineItemVar);
                //.add(qliItemVar);    
            }       
        }

        system.debug('oliLst ==>'+oliLst);
    }

      @future(callout=true)
      public static void sendEmail(id quoteId,id opId)
      {
          SBQQ__Quote__c quote= [select id,Name,ATG_Campaign_Name__c,OwnerId from SBQQ__Quote__c where id=:quoteId];
         
          Messaging.EmailFileAttachment attach;
          /*if(quote != null){
           
            attach = new Messaging.EmailFileAttachment();
            PageReference ref = Page.GeoCodeInfoAttachment;       
            ref.getParameters().put('QID',quote.Id);        
            Blob b = ref.getContentAsPDF();       
            attach.setBody(b);       
            attach.setFileName('GeoInformation.pdf');       
            attach.setContentType('application/pdf');       

          }*/

          Messaging.SingleEmailMessage msg = new Messaging.SingleEmailMessage();
          List<EmailTemplate> emailtemplateLst = [Select id,name from EmailTemplate where DeveloperName = 'ATS_Order_Summary' ];
          List<UserRecordAccess> lstUserAccess = new List<UserRecordAccess>();
          boolean readAccessToOpportunity = true;
          lstUserAccess = [SELECT HasDeleteAccess,HasEditAccess,HasReadAccess,RecordId FROM UserRecordAccess WHERE RecordId =: opId AND UserId =: quote.OwnerId limit 1];
          
          for(UserRecordAccess ua:lstUserAccess)
          {
            readAccessToOpportunity = ua.HasReadAccess;

          }

          boolean accessProvided = false;
          if(readAccessToOpportunity==false)
            accessProvided = checkAndProvideReadAccess(opId,quote.OwnerId);

          if(accessProvided && emailtemplateLst != null && emailtemplateLst.size() > 0){
            msg.setTemplateId(emailtemplateLst[0].Id);       
            msg.setTargetObjectId(quote.OwnerId);
            msg.setWhatId(opId);
            msg.setSaveAsActivity(false);
           
            if(attach != null)
              msg.setFileAttachments(new Messaging.EmailFileAttachment[]{attach});
            
            Messaging.sendEmail(new Messaging.SingleEmailMessage[] { msg });
          }
      }



      public static boolean checkAndProvideReadAccess(Id recordId, Id userOrGroupId){
          // Create new sharing object for the custom object Job.
          OpportunityShare optShr  = new OpportunityShare();
       
          // Set the ID of record being shared.
          optShr.OpportunityId  = recordId;
            
          // Set the ID of user or group being granted access.
          optShr.UserOrGroupId = userOrGroupId;
            
          // Set the access level.
          optShr.OpportunityAccessLevel = 'Read';
            
          // Set rowCause to 'manual' for manual sharing.
          // This line can be omitted as 'manual' is the default value for sharing objects.
          optShr.RowCause = Schema.OpportunityShare.RowCause.Manual;
            
          // Insert the sharing record and capture the save result. 
          // The false parameter allows for partial processing if multiple records passed 
          // into the operation.
          Database.SaveResult sr = Database.insert(optShr,false);

          // Process the save results.
          if(sr.isSuccess()){
             // Indicates success
             return true;
          }
          else {
             // Get first save result error.
             Database.Error err = sr.getErrors()[0];
             
             // Check if the error is related to trival access level.
             // Access level must be more permissive than the object's default.
             // These sharing records are not required and thus an insert exception is acceptable. 
             if(err.getStatusCode() == StatusCode.FIELD_FILTER_VALIDATION_EXCEPTION  &&  
                      err.getMessage().contains('AccessLevel')){
                // Indicates success.
                return true;
             }
             else{
                // Indicates failure.
                return false;
             }
           }
       }

    @future(callout=true)
    public static void alignAccountsAndContacts(Id userId){

       User userLst = [Select id,name,CompanyName,state,city,PostalCode,country,Website__c,ContactId,AccountId from User where id =: userId limit 1];
       List<Account> acctLst = new List<Account>();
       Set<Id> contactIdSet = new Set<Id>();
       List<Contact> contactLstToUpdate = new List<Contact>();

       

       Id contactId = userLst.ContactId;      
       Id acctId;
       Map<Id,Contact> contactIdMap = new Map<Id,Contact>([Select id,AccountId,name from COntact where id =: contactId ]);
       User userObj = userLst;
       
            // check if user Account is deault Account
            if(userObj.AccountId == '001n000000MBsre'){                
                
                Account acctObj = new Account();
                acctObj.Name = userObj.CompanyName;
                acctObj.Type = 'Advertiser';
                acctObj.CurrencyIsoCode = 'USD'; 
                acctObj.Website = userObj.Website__c;
                acctObj.BillingState= userObj.state;
                acctObj.BillingCity = userObj.city; 
                acctObj.BillingCountry = userObj.country;             
                acctObj.BillingPostalCode = userObj.PostalCode;                
                acctObj.ownerid =  '005n0000002h8gPAAQ';
                List<Account> dulpicateAcctLst = [Select id,name from Account where IsPartner = true AND Name like : (acctObj.Name) AND Type =:acctObj.Type AND Website =: acctObj.Website AND BillingState =: acctObj.BillingState AND BillingCity =: acctObj.BillingCity AND BillingCountry =: acctObj.BillingCountry];
                if(dulpicateAcctLst != null && !dulpicateAcctLst.isEmpty()){
                    Account duplicateAcct = dulpicateAcctLst[0];
                    acctId = duplicateAcct.Id;                  

                }else{                    

                  Database.DMLOptions dml = new Database.DMLOptions();
                  dml.DuplicateRuleHeader.AllowSave = true; 
                  Database.SaveResult sr = Database.insert(acctObj, dml);

                  if (sr.isSuccess()) {
                      // Operation was successful, so get the ID of the record that was processed
                      System.debug('Successfully inserted account. Account ID: ' + sr.getId());
                      acctId = sr.getId();

                  }else{
                      String error = '';             
                      for(Database.Error err : sr.getErrors()) {
                          System.debug('The following error has occurred.');                    
                          System.debug(err.getStatusCode() + ': ' + err.getMessage());
                          error += (err.getStatusCode() + ': ' + err.getMessage());
                          System.debug('Account fields that affected this error: ' + err.getFields());
                      }

                  }                    
                }

                Account acct = [Select id,isPartner from Account where id =: acctId];
                if(!acct.isPartner){
                    acct.isPartner = true;
                    update acct;
                }

                Contact contactObj = contactIdMap.get(userObj.ContactId);
                contactObj.AccountId = acctId;
                system.debug(' contactObj.AccountId ==>'+contactObj.AccountId);
                contactObj.CanAllowPortalSelfReg = true;
                contactLstToUpdate.add(contactObj);
            }    


        if(contactLstToUpdate != null && !contactLstToUpdate.isEmpty())
            update contactLstToUpdate;

    }


    public static void updateOpportunity(Opportunity oppty){
        
        update oppty;
    }

    public static pagereference isValidSession()
    {
        Pagereference pageRef;
        string loggedInUserEmail;
        id accountId;
        loggedInUserEmail = UserInfo.getUserEmail();
        if(loggedInUserEmail==null || loggedInUserEmail==''){
            pageRef = new PageReference('https://pandora10-pandoraforbrands.cs30.force.com/ads/s/login');
            return pageRef;
        }
        if(accountId==null)
        {
            
            Id loggedInUserId = UserInfo.getUserId();
            User userObj = [Select id,contactId from User where id =: loggedInUserId ];
            system.debug('Contact is ::: '+userObj.contactId);
            system.debug('loggedInUserEmail' + loggedInUserEmail); 
            for(contact c:[select accountId from contact where Id =:userObj.contactId limit 1])
                accountId = c.accountId;

            if(accountId == null){
                pageRef = new PageReference('https://pandora10-pandoraforbrands.cs30.force.com/ads/s/login');
            }
            else {
                pageRef = new PageReference('/apex/ATG_NewQuote?accountId='+accountId);
            }
                //accountId = '001n000000KTXYV';

        } 
        return pageRef;   

    }
    
    public static void updateOppStage(set<id> opptyIds,string stageName,string campaignStatus,integer probability) {
        
        List<Opportunity> opportunities = new List<Opportunity>();
        system.debug('caled here');
        system.debug('opportunitiesid' + opptyIds);
        for(opportunity op : [select StageName,Probability from opportunity where id in:opptyIds]) {
            // If quote does not have an opportunity, create a new one
            if(op.Id <> null) {
                
                op.StageName = stageName;
                op.Probability = probability;
                //op.SBQQ__Status__c = campaignStatus;
                
                opportunities.add(op);                
            }
        }
        system.debug('opportunities' + opportunities);
        update opportunities;
        // Update quotes to primary and add opportunity to quote
        /*for(Opportunity opp : opportunities) {
            opp.Probability =50;
        }
        upsert opportunities;
        */
    } 
    
    public static void updateUserNameOnEmailChange(List<User> newUserLst, Map<Id,User> t_oldMap){

        Id partnerCommunityProfileId; 
        final String PARTNER_COMMTY_PROFILE_NAME = 'Partner Community Login User';
        
        try{
            
            List<Profile> profileLst = [Select id,name from Profile where name =: PARTNER_COMMTY_PROFILE_NAME LIMIT 1 ];
            if(profileLst != null && !profileLst.isEmpty())
                partnerCommunityProfileId = profileLst[0].Id;

            for(User newU : newUserLst){
                if(newU.ProfileId == partnerCommunityProfileId){
                    if(newU.Email != t_oldMap.get(newU.Id).Email){
                        newU.Username = newU.Email;
                    }
                }
            }

        }catch(Exception ex){

        }
    }

    public static void updateCommunityContacts(List<User> newUserLst, Map<Id,User> t_oldMap){
      
      Set<Id> contactIdSet = new Set<Id>();
      Set<id> accountIdSet = new Set<Id>();

      for(User newU : newUserLst){

        if(newU.contactId != null && t_oldMap != null){ // Community User
              if((newU.FirstName != t_oldMap.get(newU.Id).FirstName) || (newU.LastName != t_oldMap.get(newU.Id).LastName) || (newU.CompanyName != t_oldMap.get(newU.Id).CompanyName) 
                || (newU.Email != t_oldMap.get(newU.Id).Email) || (newU.Phone != t_oldMap.get(newU.Id).Phone) || (newU.Website__c != t_oldMap.get(newU.Id).Website__c) 
                || (newU.Street != t_oldMap.get(newU.Id).Street) || (newU.city != t_oldMap.get(newU.Id).city) || (newU.state != t_oldMap.get(newU.Id).state)
                || (newU.country != t_oldMap.get(newU.Id).country) || (newU.PostalCode != t_oldMap.get(newU.Id).PostalCode) ){

                
                  system.debug('inside if 2');              
                  contactIdSet.add(newU.contactId);  
                  accountIdSet.add(newU.AccountId);
              }
        }
  
      }
      
      
        system.debug('updateCommunityContacts  :::: '+contactIdSet);
        Map<Id,Contact> contactMap = new Map<id,Contact>([Select id,FirstName,LastName,EMail,Phone,ATG_Company_Name__c from Contact where id IN: contactIdSet ]); 
        Map<Id,Account> acctMap = new Map<id,Account>([Select id,Name,Phone,Website from Account where id IN: accountIdSet ]); 
        List<Contact> contactToUpdate = new List<Contact>();
        List<Account> acctsToUpdate = new List<Account>();

        for(User userObj : newUserLst){
          if(contactMap.containsKey(userObj.contactId)){
            Contact contactObj = contactMap.get(userObj.contactId);
            contactObj.FirstName = userObj.FirstName;
            contactObj.LastName = userObj.LastName;
            contactObj.ATG_Company_Name__c= userObj.CompanyName;
            contactObj.EMail = userObj.Email;
            contactObj.Phone = userObj.Phone;
            contactObj.MailingState= userObj.state;
              contactObj.MailingCity = userObj.city;
                contactObj.MailingCountry = userObj.country;
                contactObj.MailingStreet = userObj.street;
                contactObj.MailingPostalCode = userObj.PostalCode;
            contactToUpdate.add(contactObj);

            Account acctObj = acctMap.get(userObj.AccountId);
            if(!String.isBlank(userObj.CompanyName))
              acctObj.Name = userObj.CompanyName;
            acctObj.Phone = userObj.Phone;
            acctObj.Website = userObj.Website__c;
            acctObj.BillingState= userObj.state;
                acctObj.BillingCity = userObj.city; 
                acctObj.BillingCountry = userObj.country;
                acctObj.BillingStreet = userObj.street;
                acctObj.BillingPostalCode = userObj.PostalCode;
            
                acctsToUpdate.add(acctObj);

          }
        }
      system.debug('contactToUpdate ::: '+contactToUpdate);
      if(contactToUpdate.size() != 0)
        update contactToUpdate;
        //database.update(contactToUpdate,false);    

      if(acctsToUpdate.size() != 0)
        update acctsToUpdate;
        //database.update(acctsToUpdate,false);    
      
    }

    public static Id getSelfServiceUserId(){
        List<User> userlst = [Select id, name from user where name = 'Self Service' and email = 'salesforceadmin@pandora.com'];
        Id genericUserId = '00540000001alR5';

        if(userlst != null && !userlst.isEmpty()){
            return userlst[0].Id;
        }

        return genericUserId;

    }
}