/**
* ─────────────────────────────────────────────────────────────────────────────────────────────────┐
* One or two sentence summary of this class.
*
* Additional information about this class should be added here, if available. Add a single line
* break between the summary and the additional info.  Use as many lines as necessary.
* ──────────────────────────────────────────────────────────────────────────────────────────────────
* @author         Jaya Alaparthi	
* @modifiedBy     Jaya Alaparthi
* @maintainedBy   Jaya Alaparthi
* @version        1.0
* @created        2016-11-01
* ──────────────────────────────────────────────────────────────────────────────────────────────────
* @changes
* vX.X            Jaya Alaparthi
* YYYY-MM-DD      Explanation of the change.  Multiple lines can be used to explain the change, but
*                 each line should be indented till left aligned with the previous description text.
* ─────────────────────────────────────────────────────────────────────────────────────────────────┘
*/
public with sharing class AT_AccountForecastWrapper {
	
	public Account_Forecast__c accountForecast {get;set;}
	public Account_Forecast_Details__c acctForecastDetail {get;set;}
	
	public Integer index {get;set;}
	public String advertiserAgency {get;set;}
	public Boolean delFlag {get;set;}
	
	public AT_AccountForecastWrapper(Integer indexIterator) {
		
		accountForecast = new Account_Forecast__c();		
		acctForecastDetail = new Account_Forecast_Details__c();
		index = indexIterator;
	}
}