/**
* ─────────────────────────────────────────────────────────────────────────────────────────────────┐
* One or two sentence summary of this class.
*
* Additional information about this class should be added here, if available. Add a single line
* break between the summary and the additional info.  Use as many lines as necessary.
* ──────────────────────────────────────────────────────────────────────────────────────────────────
* @author         		Jaya Alaparthi
* @modifiedBy     	Jaya Alaparthi
* @maintainedBy   Jaya Alaparthi
* @version        1.0
* @created        YYYY-MM-DD
* @modified       YYYY-MM-DD
* @systemLayer    Invocation | Service | Utility | Selector | Domain | Test
* @see            ????
* @see            ????
* ──────────────────────────────────────────────────────────────────────────────────────────────────
* @changes
* vX.X            Jaya Alaparthi
* YYYY-MM-DD      Explanation of the change.  Multiple lines can be used to explain the change, but
*                 each line should be indented till left aligned with the previous description text.
*
* vX.X            Jaya Alaparthi
* YYYY-MM-DD      Each change to this file should be documented by incrementing the version number,
*                 and adding a new entry to this @changes list. Note that there is a single blank
*                 line between each @changes entry.
* ─────────────────────────────────────────────────────────────────────────────────────────────────┘
*/
public with sharing class AT_AccountFrecastCreateCtrl {
    
    public Integer forecastQuarter {get;set;}
    public static Integer NO_OF_REC = 2;
    public Integer indexIterator = 0;
    public list<AT_AccountForecastWrapper> acctForecastDetailList {get;set;}
    public Date currentDate;
    
    public AT_AccountFrecastCreateCtrl() {
    	
    	list<AccountForecastCurrentDate__c> customDate = new list<AccountForecastCurrentDate__c>();
    	acctForecastDetailList = new list<AT_AccountForecastWrapper>();
    	currentDate = Date.today();
    	
    	customDate = AccountForecastCurrentDate__c.getAll().values();
    	if(customDate[0].Use_Custom_Date__c) {
    		
    		currentDate = Date.valueOf(customDate[0].Name);
    	}
    	// forecastQuarter : always holds the quarter that we can forecast on
    	forecastQuarter = AT_AcctForecastCreateUtil.getForecastQuarter(currentDate);
    	for(Integer i=0; i<NO_OF_REC; i++) {
    		
    		acctForecastDetailList.add(new AT_AccountForecastWrapper(indexIterator++));    		
    	}
    }
    public void addRow() {
    	
    	System.debug('acctForecastDetailList:'+acctForecastDetailList+' \n indexIterator:'+indexIterator);
    	acctForecastDetailList.add(new AT_AccountForecastWrapper(indexIterator++));    	
    } 
    public void deleteRow() {
    	
    	String indexDel = System.currentPageReference().getParameters().get('delIndex');
    	
    	if(!String.isBlank(indexDel) && Integer.valueOf(indexDel) < acctForecastDetailList.size()) {
    		
    		acctForecastDetailList.remove(Integer.valueOf(indexDel));
    	} 
    }
    public void saveRecords() {
    	
    	map<String,Account_Forecast_Details__c> parentToDetailMap = new map<String,Account_Forecast_Details__c>();
    	set<Account_Forecast__c> parentAcctForecastSet = new set<Account_Forecast__c>();
    	list<Account_Forecast__c> parentAcctForecastList = new list<Account_Forecast__c>();
    	set<Id> createdForecastIdsSet = new set<Id>();
    	list<Account_Forecast_Details__c> acctDetailForecastList = new list<Account_Forecast_Details__c>();
    	
    	
    	
    	String employeeId = AT_AcctForecastCreateUtil.getCurrentEmployeeId();
    	
    	for(AT_AccountForecastWrapper wrapperREC : acctForecastDetailList) {
    		
    		wrapperREC.advertiserAgency = employeeId+'|'+wrapperRec.accountForecast.Advertiser__c+'|'+wrapperRec.accountForecast.Agency__c;
    		System.debug('wrapperREC:'+wrapperREC);
    		
    		// create map of key to account forecast detail records.
    		String detailRecKey = wrapperREC.advertiserAgency+'|'+currentDate;
    		parentToDetailMap.put(detailRecKey,wrapperREC.acctForecastDetail);
    		Account_Forecast__c accountForecast = new Account_Forecast__c();
    		accountForecast = wrapperREC.accountForecast;
    		accountForecast.Rep__c = System.UserInfo.getUserId();
    		accountForecast.External_ID__c = employeeId+'-'+wrapperRec.accountForecast.Advertiser__c+'-'+wrapperRec.accountForecast.Agency__c;
    		// prepare Account Forecast records
    		parentAcctForecastSet.add(accountForecast);
    	}
    	
    	// create Account Forecast records for dml operation
    	for(Account_Forecast__c af : parentAcctForecastSet) {
    		
    		if(af.Advertiser__c != null && af.Agency__c != null) {
    			
    			parentAcctForecastList.add(af);
    		}
    	}
    	
    	if(!parentAcctForecastList.isEmpty()) {
    		
    		System.debug('parentAcctForecastList: '+parentAcctForecastList);
    		Database.SaveResult [] acctForecastSaveResult = Database.insert(parentAcctForecastList);
    		// check result and record error
    		for(Integer i=0; i< acctForecastSaveResult.size(); i++) {
    			
    			if(acctForecastSaveResult[i].isSuccess()) {
    				
    				createdForecastIdsSet.add(acctForecastSaveResult[i].getId());
    			} else {
    					
    					//record error
    			}
    		}
    		System.debug('createdForecastIdsSet :'+createdForecastIdsSet);
    		System.debug('parentToDetailMap:'+parentToDetailMap);
    		
    		// retrive acctforecast recoded 
    		// iterate Account Forecast records and map the key to the account forecast rec ID
    		for(Account_Forecast__c acctCast: 
    								AT_AcctForecastCreateUtil.getAcctForecast(createdForecastIdsSet)) {
				
				String key = employeeId+'|'+acctCast.Advertiser__c+'|'+acctCast.Agency__c+'|'+currentDate;
				System.debug('key:'+key);				
				if(!parentToDetailMap.isEmpty() && parentToDetailMap.containsKey(key)) {
					
					Account_Forecast_Details__c acctForecastDetail = new Account_Forecast_Details__c();
					acctForecastDetail = parentToDetailMap.get(key);
					acctForecastDetail.Account_Forecast__c = acctCast.Id;
					//move to next year in case of last quarter
					if(AT_AcctForecastCreateUtil.Q4.contains(currentDate.month()) && forecastQuarter == 1) {
						
						Integer forecastDateYear = currentDate.addYears(1).year();
						acctForecastDetail.External_ID_Year__c = acctCast.External_ID__c+'-'+forecastDateYear;
					} else {
						
						acctForecastDetail.External_ID_Year__c = acctCast.External_ID__c+'-'+currentDate.year();
					}						
						acctDetailForecastList.add(acctForecastDetail);
					
				}
    		}
    		
    		if(!acctDetailForecastList.isEmpty()) {
    			System.debug('acctDetailForecastList: '+acctDetailForecastList);
    			Database.SaveResult [] detailInsert = Database.insert(acctDetailForecastList);
    			// check with the errors
    		}
    		
    	}// end of forecast is empty condation
    	
    }
}