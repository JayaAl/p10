/**
 * @name: AT_AccountTeamMemberController 
 * @desc: Used to inline add, edit, delete, delete all Account Team Member
 * @author: Lakshman(sfdcace@gmail.com)
 * @date: 27-7-2013
 */
public with sharing class AT_AccountTeamMemberController {

    //list of all account team members, existing and blank for the page
    public List<AccountTeamMember> atmList { get; set; }
    public AccountTeamMember editATM { get; set; }
    public boolean nowAdding { get; private set; }
    
    public List<AccountTeamMember> fullATMList { get {
        List<AccountTeamMember> fullATMList = new List<AccountTeamMember>();
        fullATMList.addAll(atmList);
        if (nowAdding != null && nowAdding && editATM != null) {
            fullATMList.add(editATM);
        }
        return fullATMList;
    }}
    
    public boolean hasMessages { get {return ApexPages.hasMessages();} }
    
    //account id from the page
    public id accId {get; set
        {
            this.accId = value;
            atmList = queryATMList();
        } 
    }
    private String ownerId;
    private Account accountInfo;
    public AT_AccountTeamMemberController(ApexPages.StandardController stdController) {
        //grab the account Id from the acc record we're on
        accId = stdController.getId();
        accountInfo = [Select OwnerId, Owner.Email, Name, Id from Account where Id = :accId];
        ownerId = accountInfo.OwnerId;
        nowAdding = false;
    }

    public void addNewRow() {
        AccountTeamMember newRow = new AccountTeamMember(accountid = accId, TeamMemberRole=null);
        editATM = newRow;
        nowAdding = true;
    }

    //These methods are responsible for editing and deleting an EXISTING condition
    public String getParam(String name) {
        return ApexPages.currentPage().getParameters().get(name);  
    }

    public void delATM() {
        String delid = getParam('delid');
        try {
            AccountTeamMember ocr = [SELECT Id FROM AccountTeamMember WHERE ID=:delid];
            delete ocr;
            atmList = queryATMList();
        } catch (system.Dmlexception e) {
            ApexPages.addMessages(e);
        }
    }
    
    public void deleteAll() {
        try {
            AccountTeamMember[] ocr = [SELECT Id FROM AccountTeamMember WHERE AccountId=:accId AND UserId != :ownerId];
            delete ocr;
            atmList = queryATMList();
        } catch (system.Dmlexception e) {
            ApexPages.addMessages(e);
        }
    }
   
        
    public void editOneATM() {
        String editid = getParam('editid');
        editATM  = [SELECT id, AccountId, TeamMemberRole, UserId, User.Name, User.IsActive
              FROM AccountTeamMember where id = :editId]; 
    }

    
    public void cancelEdit() {
        editATM = null;
        nowAdding = false;
    }
    
    public void saveEdit() { 
        try { 
            boolean primaryAssigned = false;
            //Set<String> teamMemberRolesSelected = new Set<String> ();
            Set<Id> usersSelected = new Set<Id> ();
            // Refresh list of account team member
            List<AccountTeamMember> existingATM = queryATMList();

            List<AccountTeamMember> atmToUpsert = new List<AccountTeamMember>();
            
            if(existingATM.size() != 0) {
                for (AccountTeamMember atm : existingATM) {
                    if( atm.id != editATM.id) {
                        //teamMemberRolesSelected.add(atm.TeamMemberRole);
                        usersSelected.add(atm.UserId);
                    }
                }
            }
            /*if(teamMemberRolesSelected.contains(editATM.TeamMemberRole)) {
                ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.Fatal, 'You cannot have duplicate Team Member Role'));
            } else*/ if(usersSelected.contains(editATM.UserId)) {
                ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.Fatal, 'You cannot have duplicate Team Member'));
            } else {
                //system.debug('Edit ATM '+ editATM);
                //system.debug('ATM for my update ' + atmToUpsert);
     
                atmToUpsert.add(editATM);
                
                if(accountInfo.OwnerId != editATM.UserId) {
                    User teamMember = [Select Email, Name from User where Id =: editATM.UserId];
                    String sfdcBaseURL = URL.getSalesforceBaseUrl().toExternalForm();
                    if(editATM.Id == null) {
                        AT_EmailUtil.to(new String[]{accountInfo.Owner.Email})
                                    .saveAsActivity(false)
                                    .senderDisplayName('Salesforce.com')
                                    .subject('Account Team Member was updated' )
                                    .htmlBody('Account Team Member was updated for: <a href="' + sfdcBaseURL +'/' + accountInfo.Id + '">' + accountInfo.Name + '</a> <br/>' +
                                              'Account Team Member name-' + teamMember.Name + '<br/> Team Role: ' + editATM.TeamMemberRole)
                                    .useSignature(false)
                                    .replyTo(teamMember.email)
                                    .stashForBulk();
    
                    } else {
                        AT_EmailUtil.to(new String[]{accountInfo.Owner.Email})
                                    .saveAsActivity(false)
                                    .senderDisplayName('Salesforce.com')
                                    .subject('New Account Team Member was added' )
                                    .htmlBody('New Account Team Member was added for: <a href="' + sfdcBaseURL +'/' + accountInfo.Id + '">' + accountInfo.Name + '</a> <br/>' +
                                              'Account Team Member name-' + teamMember.Name + '<br/> Team Role: ' + editATM.TeamMemberRole)
                                    .useSignature(false)
                                    .replyTo(teamMember.email)
                                    .stashForBulk();
                    }
                }
                upsert atmToUpsert id; 
                AT_EmailUtil.sendBulkEmail();
                editATM = null;
                nowAdding = false;
                
                atmList = queryATMList();
                
            }
            
        } catch (system.Dmlexception e) {
            ApexPages.addMessages(e);
        }
        
        //return null;
    } 
    
    private List<AccountTeamMember> queryATMList() {
        return [SELECT id, AccountId, TeamMemberRole, UserId, User.Name, User.IsActive 
              FROM AccountTeamMember WHERE
              accountid = :accId ORDER BY LastModifiedDate];
    } 
}