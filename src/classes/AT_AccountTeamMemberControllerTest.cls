/**
 * @name: AT_AccountTeamMemberControllerTest 
 * @desc: Test class for AT_AccountTeamMemberController
 * @author: Lakshman(sfdcace@gmail.com)
 * @date: 27-7-2013
 */
@isTest
private class AT_AccountTeamMemberControllerTest {
    static testmethod void unittest(){
        User testUser = UTIL_TestUtil.createUser();
        Id userId = UserInfo.getUserId();
        system.runAs(testUser) {
            Account testAccount2 = UTIL_TestUtil.createAccount();
            //AccountTeamMember[] newmembers = new AccountTeamMember[]{};
            //integer  i;
            AccountTeamMember atm = new AccountTeamMember();
            atm.AccountId = testAccount2.id;
            atm.userid = testUser.Id;
            atm.teammemberrole = 'Account Manager';
            insert atm;
            test.startTest();
            PageReference myVfPage = Page.AT_AccountTeamMemberPage;
            Test.setCurrentPage(myVfPage);
            AT_AccountTeamMemberController ex = new AT_AccountTeamMemberController(new ApexPages.StandardController(testAccount2));
            ex.addNewRow();
            ex.editATM.UserId = userId;
            ex.saveEdit();
            ApexPages.currentPage().getParameters().put('editid',ex.atmList[0].Id); 
            ex.editOneATM();
            ex.cancelEdit();
            ApexPages.currentPage().getParameters().put('delid',ex.atmList[0].Id); 
            ex.delATM();
            ex.deleteAll();
            test.stopTest();
        }
    }

}