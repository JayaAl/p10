/**
 * This class contains unit tests for validating the behavior of Apex classes
 * and triggers.
 *
 * Unit tests are class methods that verify whether a particular piece
 * of code is working properly. Unit test methods take no arguments,
 * commit no data to the database, and are flagged with the testMethod
 * keyword in the method definition.
 *
 * All test methods in an organization are executed whenever Apex code is deployed
 * to a production organization to confirm correctness, ensure code
 * coverage, and prevent regressions. All Apex classes are
 * required to have at least 75% code coverage in order to be deployed
 * to a production organization. In addition, all triggers must have some code coverage.
 * 
 * The @isTest class annotation indicates this class only contains test
 * methods. Classes defined with the @isTest annotation do not count against
 * the organization size limit for all Apex scripts.
 *
 * See the Apex Language Reference for more information about Testing and Code Coverage.
 */
@isTest
private class AT_AcctForecastCreateTest {

    @testsetup 
    static void testData() {
        
        // create custom constants
        // create Account Forecast
        // create Account Forecast Detail records
        // create Rep Quota records under current user
		Date dt = Date.today();
        Custom_Constants__c cc = new Custom_Constants__c();
        cc.Name='Account Forecast Date';
        cc.Value__c = dt.month()+'/'+dt.day()+'/'+dt.year();
        insert cc;
        Custom_Constants__c cc1 = new Custom_Constants__c();
        cc1.Name = 'Account Forecast Manager Cutoff';
        cc1.Value__c = String.valueOf(dt.day());
        insert cc1;
        Custom_Constants__c cc2 = new Custom_Constants__c();
        cc2.Name = 'Account Forecast Rep Cutoff';
        cc2.Value__c = String.valueOf(dt.day());
        insert cc2;
        
    }
    private static testMethod void useCase() {
        
        list<AT_AccountForecastWrapper> accountForecastIP = new list<AT_AccountForecastWrapper>();
        
       // User manager = UTIL_TestUtil.createUser();
        Account advertiserAccount = UTIL_TestUtil.generateAccount();
        advertiserAccount.Name = advertiserAccount.Name+'Adv'; 
        advertiserAccount.Type = 'Advertiser';
        insert advertiserAccount;
        
        Account agencyAccount = UTIL_TestUtil.generateAccount();
        agencyAccount.Name = agencyAccount.Name+'Agency';
        agencyAccount.Type = 'Ad Agency';
        insert agencyAccount;
        
        Account agencyAccount2 = new Account(Name = 'Agency2',Type = 'Ad Agency');
        insert agencyAccount2;
        
        
        
        Test.startTest();
        
        	Id currentAdminUser = UserInfo.getUserId();
        	User currentUser = [SELECT Id, EmployeeNumber FROM User WHERE Id=: currentAdminUser];
            Account_Forecast__c afM =new Account_Forecast__c();
            User manager = UTIL_TestUtil.createUser();
            Account testAccount = UTIL_TestUtil.createAccount();
            afM.Rep__c = currentAdminUser;
            afM.Manager__c = currentAdminUser;
            afM.Advertiser__c = testAccount.Id;
            afM.Account_Category__c = 'Target';
            afM.External_ID__c = '123456'+ testAccount.Id;
            insert afM;
            
        	Account_Forecast__c af = new Account_Forecast__c(Rep__c = UserInfo.getUserId(),
        												  Manager__c = UserInfo.getUserId(),
        												  Advertiser__c = advertiserAccount.Id,
        												  Agency__c = agencyAccount.Id,
        												  Account_Category__c = 'Target',
        												  External_ID__c = '753951Te'+'-'+advertiserAccount.Id+'-'+agencyAccount.Id);
        	Account_Forecast_Details__c afDetail = 
        						new Account_Forecast_Details__c(Rep_Forecast_Q1__c = 45,
        														Rep_Forecast_Q2__c = 44,
        														Rep_Forecast_Q3__c = 78,
        														Rep_Forecast_Q4__c = 12);
			AT_AccountForecastWrapper afw = new AT_AccountForecastWrapper(0);
			afw.accountForecast = af;
			afw.acctForecastDetail = afDetail;
			accountForecastIP.add(afw);
			
			Account_Forecast__c af1 = new Account_Forecast__c(Rep__c = UserInfo.getUserId(),
        												  Manager__c = UserInfo.getUserId(),
        												  Advertiser__c = advertiserAccount.Id,
        												  Agency__c = agencyAccount2.Id,
        												  Account_Category__c = 'Key',
        												  External_ID__c = '753951Te'+'-'+advertiserAccount.Id+'-'+agencyAccount2.Id);
            
            Account_Forecast_Details__c afDetail1 = 
        						new Account_Forecast_Details__c(Rep_Forecast_Q1__c = 45,
        														Rep_Forecast_Q2__c = 44,
        														Rep_Forecast_Q3__c = 78,
        														Rep_Forecast_Q4__c = 12);
        	
			
			
        	AT_SearchAccountTeamController ref = new AT_SearchAccountTeamController();
        	ref.accountName = 'repOak';
        	ref.runForecast();
			ref.popUpNewBusiness();
        	ref.deleteRow();
        	ref.acctForecastDetailList = accountForecastIP;
        	ref.addRow();
        	afw = new AT_AccountForecastWrapper(1);
			afw.accountForecast = af1;
			afw.acctForecastDetail = afDetail1;
			accountForecastIP.add(afw);
            System.debug('accountForecastIP:'+accountForecastIP);
        	ref.acctForecastDetailList = accountForecastIP;
        	ref.saveRecords();
        	Account_Forecast__c forecastVerify = new Account_Forecast__c();
            String af1ExternalId = currentUser.EmployeeNumber+'-'+advertiserAccount.Id+'-'+agencyAccount2.Id;
			forecastVerify = [SELECT Id,External_ID__c FROM Account_Forecast__c 
                                WHERE Account_Category__c = 'Key' limit 1];
			System.assert(forecastVerify.External_ID__c == af1ExternalId,'calculated:'+af1ExternalId+'retrived:'+forecastVerify.External_ID__c);
            //Error in setting business category to NB for new business records created in popup.
        	ref.afIdToDelete = af.Id;
        	ref.deleteNewBusiness();
        	
        	//verify delete
        	Account_Forecast__c deletedRecord = new Account_Forecast__c();
        	deletedRecord = [SELECT Id,IsDeleted FROM Account_Forecast__c WHERE Id =:af.Id ALL ROWS];
        	System.assert(deletedRecord.Id == af.Id,'Error in deleting new business records');
        	// to cover close popup
        	ref.popUpNewBusiness();
        	ref.closePopup();
            ref.popUpNewBusiness();
            ref.saveRecords();
        Test.stopTest();
    }
}