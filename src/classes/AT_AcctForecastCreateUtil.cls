/**
* ─────────────────────────────────────────────────────────────────────────────────────────────────┐
* This is Util for Account Fore case new business creation.
* Referenced in AT_SearchAccountTeamController.
* ──────────────────────────────────────────────────────────────────────────────────────────────────
* @author         Jaya Alaparthi
* @modifiedBy     Jaya Alaparthi
* @maintainedBy   Jaya Alaparthi
* @version        1.0
* @created        2016-11-01
* ─────────────────────────────────────────────────────────────────────────────────────────────────
* @modifiedBy     Jaya Alaparthi <JAlaparthi@pandora.com>
* @version        1.1
* @modified       2017-15-02
* @changes        Modifying logic to let NB be created with just Advertiser.
* ──────────────────────────────────────────────────────────────────────────────────────────────────
* @changes
* vX.X            Jaya Alaparthi
* YYYY-MM-DD      Explanation of the change.  Multiple lines can be used to explain the change, but
*                 each line should be indented till left aligned with the previous description text.
* ─────────────────────────────────────────────────────────────────────────────────────────────────┘
*/
public with sharing class AT_AcctForecastCreateUtil {
	
	//─────────────────────────────────────────────────────────────────────────┐
	//
	//─────────────────────────────────────────────────────────────────────────┘	
	
	public static set<Integer> Q1 = new set<Integer>{1,2,3};
	public static set<Integer> Q2 = new set<Integer>{4,5,6};
	public static set<Integer> Q3 = new set<Integer>{7,8,9};
	public static set<Integer> Q4 = new set<Integer>{10,11,12};
	public static set<String> ACCT_CATEGORY_REASON = new set<String>{'Move To Inside Sales', 'Move to Seller','Other','Account inactive','Moved Agencies'};
	public static String ACCT_CATEGORY = 'Not My Account';
    //─────────────────────────────────────────────────────────────────────────┐
	// retrives account forecast records for the given ids.
	//─────────────────────────────────────────────────────────────────────────┘
	public static list<Account_Forecast__c>  getAcctForecast(set<Id> acctForecastIds) {
		
		list<Account_Forecast__c> accountForecast = new list<Account_Forecast__c>();
		if(!acctForecastIds.isEmpty()) {
			
			// 1.2
			// this condation is been removed as thee is a possibablity 
			// of having AF record without Agency
			// AND Agency__c != null
			accountForecast =  [SELECT Id,Advertiser__c,Agency__c,External_ID__c  
                                        FROM Account_Forecast__c 
                                        WHERE Advertiser__c != null 
                                        AND ID IN: acctForecastIds];
        }
        return accountForecast;
	}
   
	//─────────────────────────────────────────────────────────────────────────┐
	// returns the current quarter
	//─────────────────────────────────────────────────────────────────────────┘
	public static Integer getForecastQuarter(Date currentDate) {
		
		Integer quarter = 0;
		Integer year = currentDate.year();
		Integer month = currentDate.month();
		Integer day = currentDate.day();
		
		
		if(month == 1 || month == 4 || month == 7 || month == 10) {
			
            quarter = Q1.contains(month) ? 1 : Q2.contains(month) ? 2 : Q3.contains(month) ? 3 : Q4.contains(month) ? 4: quarter;
			
		}
		return quarter;
	}
	//─────────────────────────────────────────────────────────────────────────┐
	// based on old account forecasts created manager userId is  returned
	//─────────────────────────────────────────────────────────────────────────┘
	public static String determineManager(Id userId) {
		
		String managerUserId = '';
		
		Account_Forecast__c prevAccountForecastManager = new Account_Forecast__c();
		try {
            //In_Active__c = true AND 
            // //: System.UserInfo.getUserId() 
			prevAccountForecastManager = [SELECT Id,Manager__c 
										FROM Account_Forecast__c 
										WHERE Rep__c =: userId 
										AND Manager__c != null
										Order By CreatedDate Limit 1];
			managerUserId = prevAccountForecastManager.Manager__c;
		} catch(Exception e){
			managerUserId = 'Error in Identifying your manager';
		}
		return managerUserId; 
	}
    //─────────────────────────────────────────────────────────────────────────┐
	// based on old account forecasts created manager userId is  returned
	//─────────────────────────────────────────────────────────────────────────┘
	public static String repIdRetrival(String repId) {
		
		String repEmpId = '';
		
		User userObj = [SELECT Id, EmployeeNumber 
								FROM User 
								WHERE EmployeeNumber != null
								AND Id =: repId];
		repEmpId = userObj != null ? userObj.EmployeeNumber : '';
		return repEmpId;
	}
	//─────────────────────────────────────────────────────────────────────────┐
	// retrive anaplan quota retrival
	//─────────────────────────────────────────────────────────────────────────┘
	public static Rep_Quota__c getAnnaplanQuota(String user, String currentYear) {
		
		Rep_Quota__c repQuotas = new Rep_Quota__c();
		System.debug('user:'+user+'currentYear:'+currentYear);
		if(user.startsWith('005')) {
            try{
                for(Rep_Quota__c rq : [SELECT Id,Q1_Quota__c,Q2_Quota__c,
                                              Q3_Quota__c,Q4_Quota__c,
                                       		  Quota_For_Year__c
                                        FROM Rep_Quota__c
                                        WHERE User__c =: user
                                       AND Quota_For_Year__c != null
                                       ]) {
                	System.debug('quota year :'+rq.Quota_For_Year__c 
                		+ 'currentYear:'+currentYear+
                		'boolean:'+(Integer.valueof(rq.Quota_For_Year__c) == Integer.valueOf(currentYear)));
                    if(Integer.valueof(rq.Quota_For_Year__c) == Integer.valueOf(currentYear)){
                        repQuotas = rq;
                    }
                } 
            } catch(Exception e) {}
		}
		System.debug('repQuotas:'+repQuotas);
		return repQuotas;
	}
    //─────────────────────────────────────────────────────────────────────────┐
	// validate to check if the advertiser and Agency are entered.
	// any further vaidations also should be added here.
	//─────────────────────────────────────────────────────────────────────────┘
    /*public static String forecastInputValidation(list<AT_AccountForecastWrapper> acctForecastDetailList) {
        
        String popupErrorMsg = '';
        for(AT_AccountForecastWrapper inputValidate : acctForecastDetailList) {
        	
            if(inputValidate.accountForecast.Advertiser__c == null) {
            	popupErrorMsg = 'Advertiser is a mandatory field';
        	}
        	if(inputValidate.accountForecast.Agency__c == null) {
            	popupErrorMsg = 'Agency is a mandatory field';
       		}
        }
        return popupErrorMsg;
    }*/
}