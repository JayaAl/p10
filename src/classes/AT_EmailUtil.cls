/**
 * @name: AT_EmailUtil
 * @desc: generic class Used to send emails from Apex Class/Trigger
 * @author: Lakshman(sfdcace@gmail.com)
 * @date: 21-7-2013
 */
public class AT_EmailUtil {
    /* ===================================
        Default settings for email content
    ======================================= */
    // defaults for email content  
    private String subject = '';
    private String htmlBody = ''; 
    private String plainTextBody = '';
    private Boolean useSignature = false;
    private Boolean SaveAsActivity = false;
    private List<Messaging.EmailFileAttachment> fileAttachments = null;
    // Default replyTo is current user's email address
    // using a static to save this result across multiple email constructions.
    public static User currentUser {
        get {
            if (currentUser == null)
                currentUser = [Select email from User where username = :UserInfo.getUserName() limit 1];
            return currentUser;
        } set;
    }
    private String replyTo = currentUser.email; //replyTo defaults to current user's email 
    private String senderDisplayName = UserInfo.getFirstName()+' '+UserInfo.getLastName();
    
    // Template options
    private Id templateId;
    private Id whatId;
    
    // defaults for recipient types
    private final Id targetObjectId; // Contact, Lead, or User.
    private final List<String> toAddresses; 
    
    // Used to temporarily hold the email during the build command
    private Messaging.SingleEmailMessage singleEmailMessage; 
    
    
        
    /* ===================================
        Chain Starters
    ======================================= */
    public static AT_EmailUtil to(List<String> addresses) {
        return new AT_EmailUtil(addresses);
    }
    
    public static AT_EmailUtil to(Id target) { 
        // warning: can't override this with a string handler, apex can't tell an Id from a String.
        return new AT_EmailUtil(target);
    }
    
    /* ===================================
        Attribute Setters
    ======================================= */
    
    /*
    .saveAsActivity(Boolean val)
    .senderDisplayName(String val)
    .subject(String val)
    .htmlBody(String val)
    .useSignature(Boolean bool)
    .replyTo(String val)
    .plainTextBody(String val)
    .fileAttachments(List<Messaging.Emailfileattachment> val)
    
    for use in template:
    .templateId(Id an_id)
    .whatId(Id an_id)
    */
    
    public AT_EmailUtil saveAsActivity(Boolean val) {
        saveAsActivity = val;
        return this;
    }
    
    public AT_EmailUtil senderDisplayName(String val) {
        senderDisplayName = val;
        return this;
    }
    
    public AT_EmailUtil subject(String val) {
        subject = val;
        return this;
    }
    
    public AT_EmailUtil htmlBody(String val) {
        htmlBody = val;
        return this;
    }
    
    public AT_EmailUtil templateId(Id an_id) {
        templateId = an_id;
        return this;
    }
    
    public AT_EmailUtil whatId (Id an_id) {
        whatId = an_id;
        return this;
    }
    
    public AT_EmailUtil useSignature(Boolean bool) {
        useSignature = bool;
        return this;
    }
    
    public AT_EmailUtil replyTo(String val) {
        replyTo = val;
        return this;
    }
    
    public AT_EmailUtil plainTextBody(String val) {
        plainTextBody = val;
        return this;
    }
    
    public AT_EmailUtil fileAttachments(List<Messaging.Emailfileattachment> val) {
        fileAttachments = val;
        return this;
    }
    
    /* ===================================
        Chain Terminators: call to send now or stash for bulk send.
    ======================================= */
    public void sendEmail() {
        // build and send email.
        build();
        last_sendEmail_result = Messaging.sendEmail(new Messaging.SingleEmailMessage[] { singleEmailMessage });
    } 
    public void stashForBulk() {
        //build and stash email.
        build();
        bulk_stash.add(singleEmailMessage);
    }
    
    /* ===================================
        Other Bulk Actions
    ======================================= */
    
    public static void sendBulkEmail() {
        // send emails in bulk_stash, empty it.
        last_sendEmail_result = Messaging.sendEmail(bulk_stash);
        bulk_stash.clear();
    }
    
    public static Boolean hasEmailsToSend() {
        return bulk_stash.size() != 0 ? true : false;       
    }
    
    // static method for holding email results, so I can test when triggers send emails
    public static Messaging.SendEmailResult[] last_sendEmail_result {get; private set;}
    
    

    /* ===================================
        Helpers & private constructors
    ======================================= */

    // private constructors, force you to use the static chain methods.
    private AT_EmailUtil(List<String> addresses) {
        this.toAddresses = addresses;
    }
    
    private AT_EmailUtil(Id target) {
        this.targetObjectId = target; 
    }
    
    // build method, constructs a single email message.
    // this method is private and is called from sendEmail() or stashForBulk()
    private AT_EmailUtil build() {
        singleEmailMessage = new Messaging.SingleEmailMessage();
        singleEmailMessage.setTargetObjectId(this.targetObjectId);
        singleEmailMessage.setWhatId(this.whatId);
        singleEmailMessage.setToAddresses(this.toAddresses);
        singleEmailMessage.setSenderDisplayName(this.senderDisplayName);
        singleEmailMessage.setUseSignature(this.useSignature);
        singleEmailMessage.setReplyTo(this.replyTo);
        singleEmailMessage.setFileAttachments(this.fileAttachments);
        singleEmailMessage.setSaveAsActivity(this.saveasactivity);
        // use template if one exists, else use html and plain text body
        if (this.templateId == null) {
            singleEmailMessage.setHtmlBody(this.htmlBody);
            singleEmailMessage.setPlainTextBody(this.plainTextBody);
            singleEmailMessage.setSubject(this.subject);
        } else {
            singleEmailMessage.setTemplateId(this.templateId);
        }
        return this;
    }
    
    private static Messaging.SingleEmailMessage[] bulk_stash {
        get {
            if (bulk_stash == null) 
                bulk_stash = new Messaging.SingleEmailMessage[]{};
            return bulk_stash;
        } private set;
    }
    
}