/**
 * @name: AT_EmailUtilTest
 * @desc: Test Method for AT_EmailUtil
 * @author: Lakshman(sfdcace@gmail.com)
 * @date: 21-7-2013
 */
@isTest
private class AT_EmailUtilTest {   
    private static testMethod void should_store_email_results() {
        // Given an email util that hasn't sent emails
        System.assertEquals(null, AT_EmailUtil.last_sendEmail_result);
        
        // When you send an email
        AT_EmailUtil.to(new String[]{'test@spam.com'}).sendEmail();
        
        // Then the email util should store the send result.
        System.assertNotEquals(null, AT_EmailUtil.last_sendEmail_result);
        System.assertEquals(1, AT_EmailUtil.last_sendEmail_result.size());
        for (Messaging.SendEmailResult ser : AT_EmailUtil.last_sendEmail_result)
            System.assertEquals(true, ser.isSuccess());
    }
    
    private static testMethod void should_work_with_templates() {
        // Given an email util that has not sent emails, and a template for emails
        System.assertEquals(null, AT_EmailUtil.last_sendEmail_result);
        
        Folder test_template_folder = 
            [Select Id from Folder Where Type = 'Email' And IsReadOnly = false Limit 1];
        
        EmailTemplate test_template = new EmailTemplate(
            Name = 'test email template', DeveloperName = 'test_template_uniqueasdfbahkls',
            TemplateType = 'text', isActive = true, Description = 'test template',
            Subject = 'test email', FolderId = test_template_folder.Id,
            Body = 'Hi {!Receiving_User.FirstName}, this is a test email to a user.'
        );
        insert test_template; 
        
        // When an email is constructed & sent to the current user with that template
        AT_EmailUtil.to(UserInfo.getUserId())
            .templateId(test_template.Id)
            .sendEmail();
        
        // Then it should be sent successfully
        System.assertEquals(1, AT_EmailUtil.last_sendEmail_result.size());
        for (Messaging.SendEmailResult ser : AT_EmailUtil.last_sendEmail_result)
            System.assertEquals(true, ser.isSuccess());
    }
    
    private static testMethod void should_bulk_stash() {
        // Given an AT_EmailUtil that doesn't have emails to send.
        System.assertEquals(false, AT_EmailUtil.hasEmailsToSend());
        
        // When you stash an email to send
        AT_EmailUtil.to(new String[]{'test@spam.com'})
            .stashForBulk();
            
        // Then AT_EmailUtil should have emails to send
        System.assertEquals(true, AT_EmailUtil.hasEmailsToSend());
    }
    
    private static testMethod void should_bulk_send() {
        // Given an Email util with  bulk email to send
        AT_EmailUtil.to(new String[]{'test1@spam.com'}).stashForBulk();
        AT_EmailUtil.to(new String[]{'test2@spam.com'}).stashForBulk();
            
        // When you send bulk
        AT_EmailUtil.sendBulkEmail();
        
        // Then two emails should be sent successfully
        system.assertEquals(2, AT_EmailUtil.last_sendEmail_result.size());
        for (Messaging.SendEmailResult ser : AT_EmailUtil.last_sendEmail_result)
            System.assertEquals(true, ser.isSuccess());
    }
    
    private static testMethod void should_gracefully_handle_empty_bulk_send() {
        // Given an AT_EmailUtil with no emails to send
        System.assertEquals(false, AT_EmailUtil.hasEmailsToSend());

        try {
            // When you try and do a bulk send
            AT_EmailUtil.sendBulkEmail();
        } catch (Exception e) {
            // then it shouldn't fail horribly.
            System.assert(false);
        }
    }
    
    private static testMethod void setters_should_not_throw_exceptions() {
        try {
            // When using all the setters
            AT_EmailUtil.to(new String[]{AT_EmailUtil.currentUser.email})
                .saveAsActivity(false)
                .senderDisplayName('test sender')
                .subject('test email')
                .htmlBody('this is html')
                .useSignature(false)
                .replyTo(AT_EmailUtil.currentUser.email)
                .plainTextBody('this is plaintext')
                .fileAttachments(null); 
        } catch (Exception e) {
            // Then it should not throw an exception.
            system.assert(false);
        }
    }
}