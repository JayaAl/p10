/**
 * @name: AT_GrantOwnershipOnApprovalTest 
 * @desc: Test Class for AT_GrantOwnershipOnApproval
 * @author: Lakshman(sfdcace@gmail.com)
 * @date: 6-8-2013
 */
@isTest
private class AT_GrantOwnershipOnApprovalTest {
    static testMethod void testAccountOwnerShip() {
        User testUser = UTIL_TestUtil.createUser();
        Account testAccount = UTIL_TestUtil.createAccount();
        Account_Ownership__c ao = new Account_Ownership__c();
        ao.Status__c = 'Pending';
        ao.Account_Owners_Manager__c = testUser.Id;
        ao.Requesters_Manager__c = UserInfo.getUserId();
        ao.Requested_By__c = testUser.Id;
        ao.Account__c = testAccount.Id;
        insert ao;
        ao.Status__c = 'Approved';
        update ao;
        String ownerId = [Select OwnerId from Account where Id =: testAccount.Id].OwnerId;
        system.assertEquals(ownerId, ao.Requested_By__c);

    }
}