/**
 * @name: AT_SearchAccountTeamControllerTest
 * @desc: Test class for AT_SearchAccountTeamController
 * Jaya : THIS Test class needs a clean up. Right now modified only soem that are failing
 *     under version v1.1
 */
@isTest
private class AT_SearchAccountTeamControllerTest {
    
    static testMethod void runAFBatchRemainder()
    {
        Custom_Constants__c cc = new Custom_Constants__c();
        cc.Name='Account Forecast Date';
        cc.Value__c = '04/20/2016';
        insert cc;
        Custom_Constants__c cc1 = new Custom_Constants__c();
        cc1.Name = 'Account Forecast Manager Cutoff';
        cc1.Value__c = '25';
        insert cc1;
        Custom_Constants__c cc2 = new Custom_Constants__c();
        cc2.Name = 'Account Forecast Rep Cutoff';
        cc2.Value__c = '15';
        insert cc2;
        
        Account_Forecast__c af =new Account_Forecast__c();
        User manager = UTIL_TestUtil.createUser();
        Account testAccount = UTIL_TestUtil.createAccount();
        af.Rep__c = UserInfo.getUserId();
        af.Manager__c = manager.Id;
        af.Advertiser__c = testAccount.Id;
        af.Account_Category__c = 'Target';
        af.External_ID__c = '123456'+ testAccount.Id;
        insert af;
        
        Account_Forecast_Details__c AFD=NEW Account_Forecast_Details__c();
        AFD.Account_Forecast__c = af.Id;
        AFD.Budget_Type__c = 'All Budget Sources';
        AFD.Q1_Target__c = 10000;
        AFD.Q2_Target__c = 10000;
        AFD.Q3_Target__c = 10000;
        AFD.Q4_Target__c = 10000;
        
        AFD.Rep_Forecast_Q1__c = 9900;
        AFD.Rep_Forecast_Q2__c = 9900;
        AFD.Rep_Forecast_Q3__c = 9900;
        AFD.Rep_Forecast_Q4__c = 9900;
        AFD.Year__c = 2016;
        insert AFD;
        
        
        
        AT_SearchAccountTeamController testController = new  AT_SearchAccountTeamController();
        
        testController.runForecast();
        testController.save();
        testController.submit();
        
        
        
        
        Test.startTest();

        BatchProcessAFAutoApproval bpAF = new BatchProcessAFAutoApproval();
        

        Database.executeBatch(bpAF);

        Test.stopTest();


        
    }
    
    static testMethod void runAFBatchAutoApprove()
    {
        Custom_Constants__c cc = new Custom_Constants__c();
        cc.Name='Account Forecast Date';
        cc.Value__c = '01/25/2016';
        insert cc;
        Custom_Constants__c cc1 = new Custom_Constants__c();
        cc1.Name = 'Account Forecast Manager Cutoff';
        cc1.Value__c = '25';
        insert cc1;
        Custom_Constants__c cc2 = new Custom_Constants__c();
        cc2.Name = 'Account Forecast Rep Cutoff';
        cc2.Value__c = '15';
        insert cc2;
        
        Account_Forecast__c af =new Account_Forecast__c();
        User manager = UTIL_TestUtil.createUser();
        Account testAccount = UTIL_TestUtil.createAccount();
        
        
        // create Opportunity
        Contact testContact = UTIL_TestUtil.generateContact(testAccount.Id);
        insert testContact;
        Opportunity testOpportunity = UTIL_TestUtil.generateOpportunity(testAccount.Id,testContact.Id);
        testOpportunity.StageName = 'Closed Won';
        testOpportunity.Name = 'Test Opportunity';
        testOpportunity.CloseDate = System.Today();
        testOpportunity.Confirm_direct_relationship__c=true;
        testOpportunity.CurrencyIsoCode = 'USD';
        testOpportunity.Industry_Sub_Category__c='Political Campaign Marketing';
        testOpportunity.Type='Advertiser';
        insert testOpportunity;
        Id currentUserId = System.UserInfo.getUserId();
        /*Opportunity testUpdate = [SELECT Id,  
                                  X1st_Salesperson_Split__c, 
                                  X2nd_Salesperson_Split__c ,
                                  X2nd_Salesperson__c, 
                                  X3rd_Salesperson_Split__c ,
                                  X3rd_Salesperson__c, 
                                  X4th_Salesperson_Split__c ,
                                  X4th_Salesperson__c 
                                  FROM Opportunity WHERE id =:testOpportunity.id];
    testUpdate.X1st_Salesperson_Split__c=50;
    testUpdate.X2nd_Salesperson__c=currentUserId;
    testUpdate.X2nd_Salesperson_Split__c=30;
    testUpdate.X3rd_Salesperson__c=currentUserId;
    testUpdate.X3rd_Salesperson_Split__c=20;
    testUpdate.X4th_Salesperson__c=currentUserId;
    testUpdate.X4th_Salesperson_Split__c=10;
    update testUpdate;*/
        // have to create Oppty split as it would be deleted
        Opportunity_Split__c oSplit = new Opportunity_Split__c();
        oSplit.Split__c = 100;
        oSplit.Salesperson__c = currentUserId;
        oSplit.Opportunity__c = testOpportunity.Id;
        oSplit.Opportunity_Owner__c = true;
        oSplit.CurrencyIsoCode = 'USD';
        insert oSplit;
    
        Split_Detail__c sd = new Split_Detail__c(Opportunity_Split__c = oSplit.Id,
                                                 Salesperson_User__c=currentUserId,
                                                Date__c = Date.valueOf('2016-01-15'));
        insert sd;
        af.Rep__c = UserInfo.getUserId();
        af.Manager__c = manager.Id;
        af.Advertiser__c = testAccount.Id;
        af.Account_Category__c = 'Target';
        af.External_ID__c = '123456'+ testAccount.Id;
        insert af;
        
        Account_Forecast_Details__c AFD=NEW Account_Forecast_Details__c();
        AFD.Account_Forecast__c = af.Id;
        AFD.Budget_Type__c = 'All Budget Sources';
        AFD.Q1_Target__c = 10000;
        AFD.Q2_Target__c = 10000;
        AFD.Q3_Target__c = 10000;
        AFD.Q4_Target__c = 10000;
        
        AFD.Rep_Forecast_Q1__c = 9900;
        AFD.Rep_Forecast_Q2__c = 9900;
        AFD.Rep_Forecast_Q3__c = 9900;
        AFD.Rep_Forecast_Q4__c = 9900;
        AFD.Year__c = 2016;
        insert AFD;
        
        
        
        AT_SearchAccountTeamController testController = new  AT_SearchAccountTeamController();
        
        testController.runForecast();
        testController.save();
        testController.submit();
        
        
        
        
        Test.startTest();

        BatchProcessAFAutoApproval bpAF = new BatchProcessAFAutoApproval();
        

        Database.executeBatch(bpAF);

        Test.stopTest();


        
    }
   
    
    static testMethod void runAFRepTest()
    { 
        Custom_Constants__c cc = new Custom_Constants__c();
        cc.Name='Account Forecast Date';
        cc.Value__c = '04/20/2016';
        insert cc;
        Custom_Constants__c cc1 = new Custom_Constants__c();
        cc1.Name = 'Account Forecast Manager Cutoff';
        cc1.Value__c = '25';
        insert cc1;
        Custom_Constants__c cc2 = new Custom_Constants__c();
        cc2.Name = 'Account Forecast Rep Cutoff';
        cc2.Value__c = '15';
        insert cc2;
        
        Account_Forecast__c af =new Account_Forecast__c();
        User manager = UTIL_TestUtil.createUser();
        Account testAccount = UTIL_TestUtil.createAccount();
        af.Rep__c = UserInfo.getUserId();
        af.Manager__c = manager.Id;
        af.Advertiser__c = testAccount.Id;
        af.Account_Category__c = 'Target';
        af.External_ID__c = '123456'+ testAccount.Id;
        insert af;
        
        Account_Forecast_Details__c AFD=NEW Account_Forecast_Details__c();
        AFD.Account_Forecast__c = af.Id;
        AFD.Budget_Type__c = 'All Budget Sources';
        AFD.Q1_Target__c = 10000;
        AFD.Q2_Target__c = 10000;
        AFD.Q3_Target__c = 10000;
        AFD.Q4_Target__c = 10000;
        
        AFD.Rep_Forecast_Q1__c = 9900;
        AFD.Rep_Forecast_Q2__c = 9900;
        AFD.Rep_Forecast_Q3__c = 9900;
        AFD.Rep_Forecast_Q4__c = 9900;
        AFD.Year__c = 2016;
        insert AFD;
        
        
        
        AT_SearchAccountTeamController testController = new  AT_SearchAccountTeamController();
        
        testController.runForecast();
        testController.save();
        testController.submit();
        testController.stdSetConForecast = NULL;
        testController.getAccountForecastRecords();
        testController.doNextAF();
        testController.doPreviousAF();
        testController.getHasPreviousAF();
        testController.getHasNextAF();
        testController.getPageNumberAF();
        testController.getTotalPagesAF();
        
        
        
    }
    
    static testMethod void runAFMgrTest() {
      
      // v1.1
      // adding a the custom creation  
        Custom_Constants__c cc = new Custom_Constants__c();
        cc.Name='Account Forecast Date';
        cc.Value__c = '07/20/2016';
        insert cc;
        Custom_Constants__c cc1 = new Custom_Constants__c();
        cc1.Name = 'Account Forecast Manager Cutoff';
        cc1.Value__c = '25';
        insert cc1;
        Custom_Constants__c cc2 = new Custom_Constants__c();
        cc2.Name = 'Account Forecast Rep Cutoff';
        cc2.Value__c = '15';
        insert cc2;
        
        Account_Forecast__c af =new Account_Forecast__c();
        User rep = UTIL_TestUtil.createUser();
        Account testAccount = UTIL_TestUtil.createAccount();
        af.Rep__c = rep.Id;
        af.Manager__c = UserInfo.getUserId();
        af.Advertiser__c = testAccount.Id;
        af.Account_Category__c = 'Target';
        af.External_ID__c = '123456'+ testAccount.Id;
        insert af;
        
        Account_Forecast_Details__c AFD=NEW Account_Forecast_Details__c();
        AFD.Account_Forecast__c = af.Id;
        AFD.Budget_Type__c = 'All Budget Sources';
        AFD.Q1_Target__c = 10000;
        AFD.Q2_Target__c = 10000;
        AFD.Q3_Target__c = 10000;
        AFD.Q4_Target__c = 10000;
        
        AFD.Rep_Forecast_Q1__c = 9900;
        AFD.Rep_Forecast_Q2__c = 9900;
        AFD.Rep_Forecast_Q3__c = 9900;
        AFD.Rep_Forecast_Q4__c = 9900;
        AFD.Year__c = 2016;
        insert AFD;
        
        
        
        AT_SearchAccountTeamController testController = new  AT_SearchAccountTeamController();
        testController.objAccount.Requested_By__c = rep.Id;
        testController.runForecast();
        testController.save();
        testController.submit();
        
        
    }
    
    
    static testMethod void runAFWithParamenters() { 
      // v1.1
      // adding a the custom creation  
        Custom_Constants__c cc = new Custom_Constants__c();
        cc.Name='Account Forecast Date';
        cc.Value__c = '10/20/2016';
        insert cc;
        Custom_Constants__c cc1 = new Custom_Constants__c();
        cc1.Name = 'Account Forecast Manager Cutoff';
        cc1.Value__c = '25';
        insert cc1;
        Custom_Constants__c cc2 = new Custom_Constants__c();
        cc2.Name = 'Account Forecast Rep Cutoff';
        cc2.Value__c = '15';
        insert cc2;
        
        Account_Forecast__c af =new Account_Forecast__c();
        User manager = UTIL_TestUtil.createUser();
        Account testAccount = UTIL_TestUtil.createAccount();
        testAccount.DMA_Name__c = 'DMA';
        testAccount.MSA__c = 'MSA';
        testAccount.Status__c = 'Active';
        testAccount.Data_com_Industry_Category__c = 'Education';
        //testAccount.Transfer_Ready__c = '';
        update testAccount;
        
        
        
        af.Rep__c = UserInfo.getUserId();
        af.Manager__c = manager.Id;
        af.Advertiser__c = testAccount.Id;
        af.Account_Category__c = 'Target';
        af.External_ID__c = '123456'+ testAccount.Id;
        insert af;
        
        Account_Forecast_Details__c AFD=NEW Account_Forecast_Details__c();
        AFD.Account_Forecast__c = af.Id;
        AFD.Budget_Type__c = 'All Budget Sources';
        AFD.Q1_Target__c = 10000;
        AFD.Q2_Target__c = 10000;
        AFD.Q3_Target__c = 10000;
        AFD.Q4_Target__c = 10000;
        
        AFD.Rep_Forecast_Q1__c = 9900;
        AFD.Rep_Forecast_Q2__c = 9900;
        AFD.Rep_Forecast_Q3__c = 9900;
        AFD.Rep_Forecast_Q4__c = 9900;
        AFD.Year__c = 2016;
        insert AFD;
        
        
        
        AT_SearchAccountTeamController testController = new  AT_SearchAccountTeamController();
        testController.accountTerritory = 'West';
        testController.accountDMA = 'DMA';
        testController.accountMSA = 'MSA';
        testController.accountStatus = 'Active';
        testController.accountIndustry = 'Education';
        testController.selected = true;
        
        //testController.objAccount.Account__c = testAccount.Id;
        testController.accountName = testAccount.Name;
        testController.runForecast();
        testController.fetchAllRecords();
        testController.save();
        
    }
    
    
    
    
    static testMethod void runPositiveTestCases1() {
        User testUser = UTIL_TestUtil.createUser();
        Account testAccount;
        Account testAccount2 = UTIL_TestUtil.generateAccount();
        testAccount2.Name = 'Testing Account Search123';
        
        // v1.1
        Custom_Constants__c cc = new Custom_Constants__c();
        cc.Name='Account Forecast Date';
        cc.Value__c = '04/20/2016';
        insert cc;
        Custom_Constants__c cc1 = new Custom_Constants__c();
        cc1.Name = 'Account Forecast Manager Cutoff';
        cc1.Value__c = '25';
        insert cc1;
        Custom_Constants__c cc2 = new Custom_Constants__c();
        cc2.Name = 'Account Forecast Rep Cutoff';
        cc2.Value__c = '15';
        insert cc2;
        
        insert testAccount2;
        system.runAs(testUser) {
          
          testAccount = UTIL_TestUtil.createAccount();
            AT_SearchAccountTeamController testController = new  AT_SearchAccountTeamController();
            testController.accountName = testAccount.Name;
            testController.search();
            testController.getStatusPicklistValues();
            testController.doNext();
            testController.doNextOwned();
            testController.doPrevious();
            testController.doPreviousOwned();
            testController.getHasPrevious();
            testController.getHasPreviousOwned();
            testController.getHasNext();
            testController.getHasNextOwned();
            testController.getPageNumber();
            testController.getPageNumberOwned();
            testController.getItems();
            testController.getTotalPages();
            testController.getTotalPagesOwned();
            testController.listATRowItemOwned[0].isSelected = true;
            testController.updateSelectedAccounts();
            testController.listATRowItem[0].isSelected = true;
            testController.addAsTeamMember();
            testController.listATRowItem[0].isSelected = true;
            testController.requestOwnership();
            testController.listATRowItem[0].isSelected = true;
            testController.updateSelectedAccountsSearched();
            testController.rmodeSearch = 'PDF';
            testController.renderPageSearch();
            testAccount2.DMA_Name__c = 'Testing DMA';
            testAccount2.MSA__c = 'Testing MSA';
            testAccount2.Status__c = 'Testing Status';    
            testController.search();
        }
    }
    
    static testMethod void runPositiveTestCases2() {
        User testUser = UTIL_TestUtil.createUser();
        Account testAccount;
        Account testAccount2 = UTIL_TestUtil.generateAccount();
        testAccount2.Name = 'Testing Account Search321';
        insert testAccount2;
        
        // v1.1
        Custom_Constants__c cc = new Custom_Constants__c();
        cc.Name='Account Forecast Date';
        cc.Value__c = '04/20/2016';
        insert cc;
        Custom_Constants__c cc1 = new Custom_Constants__c();
        cc1.Name = 'Account Forecast Manager Cutoff';
        cc1.Value__c = '25';
        insert cc1;
        Custom_Constants__c cc2 = new Custom_Constants__c();
        cc2.Name = 'Account Forecast Rep Cutoff';
        cc2.Value__c = '15';
        insert cc2;
        
        system.runAs(testUser) {
          
          
            testAccount = UTIL_TestUtil.createAccount();
            AT_SearchAccountTeamController testController = new  AT_SearchAccountTeamController();
            testController.accountName = testAccount2.Name;
            testController.search();
            testController.accountId = testAccount2.Id;
            testController.changeOwner();
            testController.accountId = testAccount2.Id;
            testController.changeStatus();
            testController.rmodeSearch = 'CSV';
            testController.renderPageSearch();
            testController.ShowTable();
            testController.sortTableAccounts();
            testController.sortTableAccountsOwned();
            testController.getIndustryCategory();
            testController.getSortDirection();
            testController.getSortDirectionOwned();
            testController.renderPage();
            testController.getTransferReady();
        }
    }
}