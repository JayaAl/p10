/**
* ─────────────────────────────────────────────────────────────────────────────────────────────────┐
* Helper Class to handler any DML or Helper methods 
* for AccountForecastTriggerHandler.
* ──────────────────────────────────────────────────────────────────────────────────────────────────
* @author         Jaya Alaparthi
* @maintainedBy   Jaya Alaparthi
* @version        1.0
* @created        2017-02-14
* @modified       YYYY-MM-DD
* @systemLayer    Service
* @see            ????
* @see            ????
* ──────────────────────────────────────────────────────────────────────────────────────────────────
* @changes
* vX.X            Jaya Alaparthi
* YYYY-MM-DD      Explanation of the change.  Multiple lines can be used to explain the change, but
*                 each line should be indented till left aligned with the previous description text.
*
* vX.X            Jaya Alaparthi
* YYYY-MM-DD      Each change to this file should be documented by incrementing the version number,
*                 and adding a new entry to this @changes list. Note that there is a single blank
*                 line between each @changes entry.
* ─────────────────────────────────────────────────────────────────────────────────────────────────┘
*/
public with sharing class AccountForecastHelper {
	
	/*
	* updateAccountClassification is for afterInsert Action On AccountForecast Trigger
	* ────────────────────────────────────────────────────────────────────────┐
	* @param acctIdsSet  Set of Advertiser and Agency account Ids
	* ────────────────────────────────────────────────────────────────────────┘
	*/
	public void updateAccountClassification(Set<Id> acctIdsSet) {
		
		List<Account> accountsRetrivedList = new List<Account>();
		List<Account> accountsToUpdateList = new List<Account>();
		Map<Id,String> acctErrorMap = new Map<Id,String>();

		if(!acctIdsSet.isEmpty()) {
			
			accountsRetrivedList = AccountForecastUtil.getAccounts(acctIdsSet);
			if(!accountsRetrivedList.isEmpty()) {

				for(Account acct: accountsRetrivedList) {

					accountsToUpdateList.add(new Account(Id = acct.Id,
											Classification__c = AccountForecastUtil.PROTFOLIO_ACCOUNT));
				}
				Database.SaveResult [] detailInsert = Database.update(accountsToUpdateList);
			}
		}
		//return acctErrorMap;
	}
}