/**
* ─────────────────────────────────────────────────────────────────────────────────────────────────┐
* All the Trigger action for Trigger on AccountForecast must be handled or added in this class.
* ──────────────────────────────────────────────────────────────────────────────────────────────────
* @author         Jaya Alaparthi
* @maintainedBy   Jaya Alaparthi
* @version        1.0
* @created        2017-02-14
* @modified       YYYY-MM-DD
* @systemLayer    Handler
* @see            ????
* @see            ????
* ──────────────────────────────────────────────────────────────────────────────────────────────────
* @changes
* vX.X            Jaya Alaparthi
* YYYY-MM-DD      Explanation of the change.  Multiple lines can be used to explain the change, but
*                 each line should be indented till left aligned with the previous description text.
*
* vX.X            Jaya Alaparthi
* YYYY-MM-DD      Each change to this file should be documented by incrementing the version number,
*                 and adding a new entry to this @changes list. Note that there is a single blank
*                 line between each @changes entry.
* ─────────────────────────────────────────────────────────────────────────────────────────────────┘
*/
public with sharing class AccountForecastTriggerHandler {
	/*
	* afterInsertAction is for afterInsert Action On AccountForecast Trigger
	* ────────────────────────────────────────────────────────────────────────┐
	* @param acctForecastList  List of Account_Forecast__c.
	* ─────────────────────────────────────────────────────────────────────────┘
	*/
	public void afterInsertAction(List<Account_Forecast__c> acctForecastList) {

		Set<Id> acctIdsSet = new Set<Id>();
		for(Account_Forecast__c acctForecast : acctForecastList) {

			if(acctForecast.Advertiser__c != null) {
				acctIdsSet.add(acctForecast.Advertiser__c);
				if(acctForecast.Agency__c != null) {
					acctIdsSet.add(acctForecast.Agency__c);
				}
			}
		}
		if(!acctIdsSet.IsEmpty()) {
			AccountForecastHelper ref =  new AccountForecastHelper();
			ref.updateAccountClassification(acctIdsSet);
		}
	}
}