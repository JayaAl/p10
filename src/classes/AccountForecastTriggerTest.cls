/**
* ─────────────────────────────────────────────────────────────────────────────────────────────────┐
* Test class for AccountForecastTrigger.trigger
* AccountForecastTriggerHandler.cls, AccountForecastTriggerHelper.cls,
* AccountForecastUtil.cls are covered in this test class.
* Using UTIL_TestUtil for account creation.
* ──────────────────────────────────────────────────────────────────────────────────────────────────
* @author         Jaya Alaparthi
* @maintainedBy   Jaya Alaparthi
* @version        1.0
* @created        2017-02-15
* @modified       YYYY-MM-DD
* @systemLayer    Service
* @see            ????
* @see            ????
* ──────────────────────────────────────────────────────────────────────────────────────────────────
* @changes
* vX.X            Jaya Alaparthi
* YYYY-MM-DD      Explanation of the change.  Multiple lines can be used to explain the change, but
*                 each line should be indented till left aligned with the previous description text.
*
* vX.X            Jaya Alaparthi
* YYYY-MM-DD      Each change to this file should be documented by incrementing the version number,
*                 and adding a new entry to this @changes list. Note that there is a single blank
*                 line between each @changes entry.
* ─────────────────────────────────────────────────────────────────────────────────────────────────┘
*/
@isTest
private class AccountForecastTriggerTest
{
	@testSetup
	static void createTestData()
	{
		List<Account> advAccountList = new List<Account>();
		List<Account> agencyAccountList = new List<Account>();

		// Create Account with type Advertiser and Agancy in bulk.
		for(Integer i = 0; i< 1000; i++) {

			Account accountADV  = UTIL_TestUtil.generateAccount();
			accountADV.Type = AccountForecastUtil.ADVERTISER_ACCOUNT;
			accountADV.name += 'Adv'+i;
			advAccountList.add(accountADV);
		}				
		for(Integer i = 0; i< 1000; i++) {

			Account accountAgency = UTIL_TestUtil.generateAccount();
			accountAgency.Type = AccountForecastUtil.AGENCY_ACCOUNT;
			accountAgency.name += 'Agency'+i;
			agencyAccountList.add(accountAgency);
		}
		insert advAccountList;
		insert agencyAccountList;		
	}
	public static testmethod void acctForecastWithAdvAndAgencyTest() {

		List<Account> advAccountList = new List<Account>();
		List<Account> agencyAccountList = new List<Account>();
		List<Account_Forecast__c> acctForecastList = 
							new List<Account_Forecast__c>();

		String query = 'SELECT Id FROM Account WHERE Type = :';
		String advertiser = AccountForecastUtil.ADVERTISER_ACCOUNT;
		String agency = AccountForecastUtil.AGENCY_ACCOUNT;
		String advQuery = query+'advertiser';
		String agencyQuery = query+'agency';
		
		for(Account advAccount : Database.query(advQuery)){
			advAccountList.add(advAccount);
		}
		for(Account agencyAccount : Database.query(agencyQuery)){
			agencyAccountList.add(agencyAccount);
		}
		// create Account Forecast records using the advertiser and agency
		// records created above
		for(Integer i=0; i<advAccountList.size(); i++) {
			
			Account_Forecast__c acctForecast = new Account_Forecast__c();
			acctForecast.External_ID__c = 'test'+i;
			acctForecast.Advertiser__c = advAccountList[i].Id;
			acctForecast.Agency__c = agencyAccountList[i].Id;
			acctForecastList.add(acctForecast);
		}
		Test.startTest();
		insert acctForecastList;
		Test.stopTest();
		String portfolio = AccountForecastUtil.PROTFOLIO_ACCOUNT;
		String acfCountQuery = 'SELECT Id'
						+' FROM Account_Forecast__c'
						+' WHERE Advertiser__c != null'
						+' AND Agency__c != null'
						+' AND Advertiser__r.Classification__c =:portfolio'
						+' AND Agency__r.Classification__c =: portfolio';
		//Integer accountClasficationCount = database.countQuery(acfCountQuery);
		list<Account_Forecast__c> accountList = database.query(acfCountQuery);
		System.assertEquals(accountList.size(),acctForecastList.size(),
					'Classification__c is not updated on all accountforecast records.');
	}
}