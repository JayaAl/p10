/**
* ─────────────────────────────────────────────────────────────────────────────────────────────────┐
* 
* ──────────────────────────────────────────────────────────────────────────────────────────────────
* @author         Jaya Alaparthi
* @maintainedBy   Jaya Alaparthi
* @version        1.0
* @created        2017-02-14
* @modified       YYYY-MM-DD
* @systemLayer    Service
* @see            ????
* @see            ????
* ──────────────────────────────────────────────────────────────────────────────────────────────────
* @changes
* vX.X            Jaya Alaparthi
* YYYY-MM-DD      Explanation of the change.  Multiple lines can be used to explain the change, but
*                 each line should be indented till left aligned with the previous description text.
*
* vX.X            Jaya Alaparthi
* YYYY-MM-DD      Each change to this file should be documented by incrementing the version number,
*                 and adding a new entry to this @changes list. Note that there is a single blank
*                 line between each @changes entry.
* ─────────────────────────────────────────────────────────────────────────────────────────────────┘
*/
public with sharing class AccountForecastUtil {
	
	public static String PROTFOLIO_ACCOUNT = 'Portfolio Account';
	public static String ADVERTISER_ACCOUNT = 'Advertiser';
	public static String AGENCY_ACCOUNT = 'Ad Agency';
	public static List<Account> getAccounts(Set<Id> searchIds) {

		List<Account> accountsList = new List<Account>();
		for(Account acct : [SELECT Id, Classification__c
							FROM Account
							WHERE Id IN: searchIds
							AND Classification__c !=: PROTFOLIO_ACCOUNT]) {
			accountsList.add(acct);
		}
		return accountsList;
	}
}