public class AccountHierarchyLtngCompCtrlr {
    @AuraEnabled
    public static ACRDisplayWrapper loadHeirarchy(Id accountId){
        //id accountId = '0014000001mLn53';
        //id accountId = '0014000001mLnCw';  
        List <Account> acctLst = [Select Id,Type,ParentId,RecordType.DeveloperName from Account where RecordType.DeveloperName = 'Music_Makers_Group' AND id =: accountId Limit 1];
        
        Account acctObj = (acctLst != null && acctLst.size () > 0) ? acctLst[0] : null;
        ACRDisplayWrapper wrapperVar;

        if(acctObj != null){
            if( ( 'Management Company'.equalsIgnoreCase(acctObj.Type) || 'Venue'.equalsIgnoreCase(acctObj.Type) || 'Agency'.equalsIgnoreCase(acctObj.Type) || 'Promotion Company'.equalsIgnoreCase(acctObj.Type) ) || ( 'Artist'.equalsIgnoreCase(acctObj.Type) && String.isBlank(acctObj.ParentId)) ){
                ACRDisplayHelper acrHelper = new ACRDisplayHelper();
                wrapperVar = acrHelper.displayManagementHierarchy(acctObj.Id);              
                wrapperVar.isArtistWrapper = true;
            }else if('Artist'.equalsIgnoreCase(acctObj.Type) || 'Label'.equalsIgnoreCase(acctObj.Type)  ){
                Account accoutRetrived = new Account();
                // validate Account Type
                // Label/SubLabel/Artist
                accoutRetrived = ACRDisplayUtil.getAccountDetails(acctObj.Id);
                wrapperVar = artistLoad(accoutRetrived,acctObj.Id);
                wrapperVar.isArtistWrapper = false;
            } 
        }
        
        return wrapperVar;
    }

     private static ACRDisplayWrapper artistLoad(Account accoutRetrived, Id accountId) {

        ACRDisplayWrapper acrWrapper = new ACRDisplayWrapper();
        ACRDisplayHelper acrRef = new ACRDisplayHelper();
        // pass parent accountId and then if parentaccountId is blank then
        // the current account it self is the top level parent account.
        try {           
                acrWrapper = acrRef.displayArtistHirearchy(accountId,accoutRetrived);
                return acrWrapper;

                if(!String.isBlank(acrWrapper.errorMsg)) {
                    ApexPages.addmessage(new ApexPages.message(ApexPages.severity.ERROR,acrWrapper.errorMsg));
                }
            } catch(Exception e) {
                ApexPages.addmessage(new ApexPages.message(ApexPages.severity.ERROR,'Record Provided is not a valid Account.'+e.getMessage()));
            }
        return acrWrapper;

    }
}