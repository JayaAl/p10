/**
* ─────────────────────────────────────────────────────────────────────────────────────────────────┐
* This class services lightning action buttons on Account.
* ──────────────────────────────────────────────────────────────────────────────────────────────────
* @author         Jaya Alaparthi
* @maintainedBy   Jaya Alaparthi
* @version        1.0
* @created        2017-02-06
* @modified       2017-04-12
* @systemLayer    Service
* @see            ????
* @see            ????
* ──────────────────────────────────────────────────────────────────────────────────────────────────
* @changes
* v1.1            Jaya Alaparthi
* 2017-04-12      Adding Credit Check callout for FLOW.
*                 
*
* vX.X            Jaya Alaparthi
* YYYY-MM-DD      Each change to this file should be documented by incrementing the version number,
*                 and adding a new entry to this @changes list. Note that there is a single blank
*                 line between each @changes entry.
* ─────────────────────────────────────────────────────────────────────────────────────────────────┘
*/
global with sharing class AccountLightningActionServices implements Process.Plugin{
	

	global Process.PluginResult invoke(Process.PluginRequest request) { 
      
      Map<String,Object> result = new Map<String,Object>();

      String accountId = (String) request.inputParameters.get('aid');
      String creditVerficationId = (String) request.inputParameters.get('cid');
      
      String creditCheckResponse = '';
      System.debug('B4 calling creditCheckCallout '+accountId+'cid '+creditVerficationId);
      creditCheckResponse = creditCheckCallout(accountId,creditVerficationId);
      result.put('resp', creditCheckResponse);

      return new Process.PluginResult(result);
   }

   global Process.PluginDescribeResult describe() { 

      Process.PluginDescribeResult result = new Process.PluginDescribeResult(); 
      result.Name = 'Account Credit Check';
      result.Tag = 'Credit Check';
      result.inputParameters = new 
         List<Process.PluginDescribeResult.InputParameter>{ 
            new Process.PluginDescribeResult.InputParameter('aid', 'Account Id',
            			Process.PluginDescribeResult.ParameterType.STRING, true),
            new Process.PluginDescribeResult.InputParameter('cid', 'Account creditVerfication Id',
            			Process.PluginDescribeResult.ParameterType.STRING,true)
        }; 
      result.outputParameters = new 
         List<Process.PluginDescribeResult.OutputParameter>{              
            new Process.PluginDescribeResult.OutputParameter('resp', 'Credit check response',
            			Process.PluginDescribeResult.ParameterType.STRING)
        }; 
      return result; 
   }
	//─────────────────────────────────────────────────────────────────────────┐
	// getPLIList: get the list of plis related to the current opptyId
	// @param opptyId  opportunity Id
	// @return list<OpportunityLineItem> 
    //─────────────────────────────────────────────────────────────────────────┘
	private String creditCheckCallout(String acctId,String cvId) {

		String calloutResp = '';
		calloutResp = creditVerificationInitiation.applyForCreditInForseva(acctId,cvId);
		return calloutResp;	
	}

}