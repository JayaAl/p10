/**
* ─────────────────────────────────────────────────────────────────────────────────────────────────┐
* This is a wrapper class for CC on Account
* ──────────────────────────────────────────────────────────────────────────────────────────────────
* @author         Jaya Alaparthi
* @maintainedBy   Jaya Alaparthi
* @version        1.0
* @created        2017-02-08
* @modified       
* @systemLayer    Service
* @see            ????
* @see            ????
* ──────────────────────────────────────────────────────────────────────────────────────────────────
* @changes
* 
* vX.X            Jaya Alaparthi
* YYYY-MM-DD      Each change to this file should be documented by incrementing the version number,
*                 and adding a new entry to this @changes list. Note that there is a single blank
*                 line between each @changes entry.
* ─────────────────────────────────────────────────────────────────────────────────────────────────┘
*/
public with sharing class AccountLightningWrapper {
	
	@AuraEnabled
	public String message {get;set;}
	
	@AuraEnabled
	public Boolean confirmFlag {get;set;}

	@AuraEnabled
	public Boolean messageFlag {get;set;}
	
	@AuraEnabled
	public String accountId {get;set;}
	
	@AuraEnabled
	public String ccVerificationId {get;set;}
	
	public AccountLightningWrapper() {		
	}
}