/**
* ─────────────────────────────────────────────────────────────────────────────────────────────────┐
* This class services lightning action buttons on Account.
* ──────────────────────────────────────────────────────────────────────────────────────────────────
* @author         Jaya Alaparthi
* @maintainedBy   Jaya Alaparthi
* @version        1.0
* @created        2017-02-06
* @modified       2017-04-12
* @systemLayer    Service
* @see            ????
* @see            ????
* ──────────────────────────────────────────────────────────────────────────────────────────────────
* @changes
* v1.1            Jaya Alaparthi
* 2017-04-12      Adding Credit Check callout for FLOW.
*                 
*
* vX.X            Jaya Alaparthi
* YYYY-MM-DD      Each change to this file should be documented by incrementing the version number,
*                 and adding a new entry to this @changes list. Note that there is a single blank
*                 line between each @changes entry.
* ─────────────────────────────────────────────────────────────────────────────────────────────────┘
*/
public with sharing class AccountRelatedligComponent {

	@AuraEnabled
	public AccountLightningWrapper acctWrapper;

	// public AccountRelatedligComponent() {}
	//─────────────────────────────────────────────────────────────────────────┐
	// getAccount: retrive 
	// @param opptyId  opportunity Id
	// @return list<OpportunityLineItem> 
    //─────────────────────────────────────────────────────────────────────────┘
	private static Account getAccount(Id acctId) {

		System.debug('getAccount:'+acctId);
		Account acct = new Account();
		acct = [SELECT Id, RecordTypeId, RecordType.Name,
					RecordType.DeveloperName,
					Credit_Check_Processed__c,
					Credit_Verification2__c,
					Credit_Verification2__r.Id,
					Name,BillingStreet,BillingCity,
					BillingState,BillingPostalCode,
					BillingCountry,D_U_N_S__c
				FROM Account
				WHERE Id =: acctId];
		System.debug('acct:'+acct);
		return acct;
	}
	//─────────────────────────────────────────────────────────────────────────┐
	// isValidAcct: Is Account valid for Credit check.
	//				This validation is only for Credit check only, It happens 
	//				In 3 check points / condations
	//				1> if there is no Oppty ask for confirmation to proceed to 
	//					credit check[CC / cc].
	//				2> if credit check is already let the user know.
	//				3> if there is an oppty and cc is not done b4, Proceed to CC.
	// @param acctId  account Id
	// @return AccountLightningWrapper 
    //─────────────────────────────────────────────────────────────────────────┘
	@AuraEnabled
	public static AccountLightningWrapper isValidAcct(Id acctId) {

		Account account = new Account();
		AccountLightningWrapper acctLightWrapper = new AccountLightningWrapper();
		
		System.debug('isValidAcct acct:'+acctId);
		
		Opportunity oppty = new Opportunity();
		System.debug('isValidAcct acct:'+acctId);

		if(acctId != null) {

			account = getAccount(acctId);
			System.debug('after account is retrived:'+account);
			acctLightWrapper.accountId = account.Id;
			try{
				oppty = [SELECT Id
						FROM Opportunity
						WHERE Agency__c =: acctId
						OR AccountId =: acctId Limit 1];
			} catch(Exception e) {

				acctLightWrapper.message = 'There are no Opportunities associated with this Account or Agency.'
										+' Please confirm if you still want to Check Credit.';
				System.debug('Error retriving Opportunity:'+e.getMessage());
			}
			// !account.Credit_Check_Processed__c
			// Insted of checkin g for formula use in code 
			if(oppty.Id == null 
				&& account.Credit_Verification2__c == null) {

				acctLightWrapper.confirmFlag = true;
				acctLightWrapper.messageFlag = false;
				acctLightWrapper.ccVerificationId = account.Credit_Verification2__c;
				acctLightWrapper.message = 'There are no Opportunities associated with this Account or Agency.'
										+' Please confirm if you still want to Check Credit.';

			} else if (account.Credit_Verification2__c != null) {

				acctLightWrapper.message = 'Credit Check has already been done for this Account, and cannot be repeated.';
				acctLightWrapper.messageFlag = true;
				acctLightWrapper.confirmFlag = false;
			} else if(oppty.Id != null 
						&& account.Credit_Verification2__c == null) {

				acctLightWrapper.confirmFlag = false;
				acctLightWrapper.messageFlag = false;
				acctLightWrapper.ccVerificationId = null;
			}
			// callout
			if(!acctLightWrapper.confirmFlag
				&& !acctLightWrapper.messageFlag) {

				String resp = creditCheckCallout(acctId,
									account.Credit_Verification2__c);
				
				acctLightWrapper.message = resp;						
				acctLightWrapper.messageFlag = true;
				acctLightWrapper.confirmFlag = false;
			}
		}
		return acctLightWrapper;
	}
	//─────────────────────────────────────────────────────────────────────────┐
	// creditCheck: creditCheck callout.
	// @param acctId  account Id
	// @param cvId  
	// @return list<OpportunityLineItem> 
    //─────────────────────────────────────────────────────────────────────────┘
	@AuraEnabled
	public static String creditCheck(String acctId,String cvId) {

		String calloutResp = '';
		calloutResp = creditCheckCallout(acctId,cvId);
		return calloutResp;
	}

	private static String creditCheckCallout(String acctId,String cvId) {

		String calloutResp = '';
		if(cvId == null || cvId == '') {
			cvId = '';
		}
		calloutResp = creditVerificationInitiation.applyForCreditInForseva(acctId,cvId);
		return calloutResp;	
	}
}