/**
* ─────────────────────────────────────────────────────────────────────────────────────────────────┐
* Account Related actions buttons test class.
* 
* Test class for:
* AccountRelatedligComponentTest.cls
* AccountLightningWrapper.cls
* ──────────────────────────────────────────────────────────────────────────────────────────────────
* @author         Jaya Alaparthi
* @maintainedBy   Jaya Alaparthi
* @version        1.0
* @created        2017-05-15
* @modified       YYYY-MM-DD
* @systemLayer    Test
* ──────────────────────────────────────────────────────────────────────────────────────────────────
* @changes
* vX.X            Jaya Alaparthi
* YYYY-MM-DD      Explanation of the change.  Multiple lines can be used to explain the change, but
*                 each line should be indented till left aligned with the previous description text.
* ─────────────────────────────────────────────────────────────────────────────────────────────────┘
*/
@isTest
private class AccountRelatedligComponentTest {

	@testSetUp static void testDataSetup() {

		// create Account
		Account account =  new Account();
		account = UTIL_TestUtil.generateAccount();
		account.Name = 'acctLightActSErvicesTEST';
		insert account;

		Id opptyRecordTypeId = Schema.SObjectType.Opportunity.getRecordTypeInfosByName().get('Opportunity - Inside/Local Full').getRecordTypeId();
        
		System.debug('opptyRecordTypeId:'+opptyRecordTypeId);
		Opportunity opportunity = new Opportunity();
		opportunity = UTIL_TestUtil.generateOpportunity(account.Id);
		opportunity.Name = 'AcctLighServicesTest';
		opportunity.CurrencyIsoCode = 'USD';
		opportunity.RecordTypeId = opptyRecordTypeId;
		opportunity.TTR_User_Data__c = 'Email Address';
		insert opportunity;
	}
	@isTest
    static void isValidAcctWithOpptyTest() {

    	Account acct = [SELECT Id FROM Account 
    					WHERE Name = 'acctLightActSErvicesTEST' Limit 1];
    	Test.startTest();
    	AccountLightningWrapper wrapperObj = AccountRelatedligComponent.isValidAcct(acct.Id);
    	System.assert(wrapperObj.message== 'Credit could not be established, the credit team will investigate.','Error:'+wrapperObj.message);
    	Test.stopTest();
    	// cover all the other cases for this oppty.
    }
    @isTest
    static void isValidAcctWithoutOpptyTest() {
		
        Account account =  new Account();
		account = UTIL_TestUtil.generateAccount();
		account.Name = 'acctLightActOnlyAcctTEST';
        account.Website = 'noOppty.com';
        account.BillingPostalCode = '94356';
		insert account;
    	Test.startTest();
    	AccountLightningWrapper wrapperObj = AccountRelatedligComponent.isValidAcct(account.Id);
    	System.assert(wrapperObj.message == 'There are no Opportunities associated with this Account or Agency.'
										+' Please confirm if you still want to Check Credit.',
                      'Error in response returned in case of No Opportunity.');
        AccountRelatedligComponent.creditCheck(account.Id,'');
    	Test.stopTest();
    	// cover all the other cases for this oppty.
    }
}