/***************************************************************************************************************
*  Name                 : AccountRevenueBatch
*  Author               : Sridharan Subramanian
*  Last Modified Date   : 01/17/2018
*  Description          : Used to Calculate Account based Revenue that has opportunities which are closed with in the Last 1 year
*                          
*                          
************************************************************************************************************************/
/************************************************************************************************************************
* Modification History
* Modified By   : Sridharan Subramanian
* Modified Date : 01/23/2018
* Description   : 
*************************************************************************************************************************/
global class AccountRevenueBatch implements Database.Batchable<sObject> {
	
	String query;
	
	
	global AccountRevenueBatch() {

		     //Query to get ACCOUNTS WHERE THE opportunity closed date is with in the last 1 yr
		     query = 'SELECT Id,Last_Close_Date_Std__c, Account_Total_Revenue__c';
		     query += ' FROM Account ';
		     query += ' WHERE  ';
		     query += ' Last_Close_Date_Std__c >= LAST_N_DAYS:376 ';
		     query += ' order by Last_Close_Date_Std__c asc';
	}
	
	global Database.QueryLocator start(Database.BatchableContext BC) {
		return Database.getQueryLocator(query);
	}

   	global void execute(Database.BatchableContext BC, List<sObject> scope) {
		
		Map<id,decimal> accountRevenueMap = new Map<id,decimal>();
		list<opportunity> lstOpportunitiesToUpdate = new List<opportunity>();
		Map<id,account> accountMap = new Map<Id,account>();
		List<account> accountToUpdate = new List<account>();
		for(account act:(list<account>)scope){
			accountMap.put(act.id,act);	
		}
		system.debug('accountRevenueMap' + accountMap);
		
		try{
			//Calculate Account Based revenue
			accountRevenueMap = AccountRevenueUtil.CalculateTotalRevenue(accountMap.keyset());
			system.debug('accountRevenueMap' + accountRevenueMap);
			//Based on the calculated revenue load the account Account_Total_Revenue__c field
			for(account act:(list<account>)scope){
				act.Account_Total_Revenue__c = 0;
				if(accountRevenueMap.get(act.Id)!=null)
					act.Account_Total_Revenue__c = (decimal)accountRevenueMap.get(act.Id);
				accountToUpdate.add(act);
			}
			system.debug('accountRevenueMap accountToUpdate' + accountToUpdate);
			//Update account with total revenue
			if(!accountToUpdate.isEmpty())
				update accountToUpdate;

		} Catch(Exception ex){
                    
                	Error_Log__c errorRec = AccountRevenueUtil.createError(ex.getMessage(),
                														BC.getJobId(),
                														'AccountRevenueBatch');
                	insert errorRec;
        }
	}
	
	global void finish(Database.BatchableContext BC) {
		
	}
	
}