/***************************************************************************************************************
*  Name                 : AccountRevenueBatchTest
*  Author               : Sridharan Subramanian
*  Created Date         : 01/25/2018
*  Description          : Test Class for AccountRevenueBatch batch class.
                          This test class covers the code coverage for AccountRevenueBatch
                          which Calculate Account based Revenue that has opportunities which
                          are closed with in the Last 1 year.
*                          
*                          
************************************************************************************************************************/
@isTest 
public class AccountRevenueBatchTest 
{
    static testMethod void accountRevenueCalculate() 
    {
        String TEST_STRING = 'aaaa';

        Account acc = new Account(Name = 'Test Account for Opp Pandora');
        insert acc;

        Account acc2 = new Account(
            name = TEST_STRING+'2'                      
        );

        Contact cont = new Contact(lastName = TEST_STRING, accountId = acc.id , MailingStreet = 'MyStreet1', MailingCountry = 'USA', MailingState = 'CA', MailingCity = 'Fremont', Email = 'myemail1@mydomain.com', Title = 'MyTitle1', firstname = 'MyFristName1');
        insert cont;

        // Insert custom setting Auto_Assign_Biller__c   
        insert UTIL_TestUtil.createAutoAssignCS(UserInfo.getUserId(),UserInfo.getUserName(),1);

        
        System.assert(acc.Id!= null);
        
        List<Opportunity> opps = new List<Opportunity>();
        
        Opportunity opp = new Opportunity(
                  accountId = acc.id
                , bill_on_broadcast_calendar2__c = TEST_STRING
                , closeDate = Date.Today()
                , confirm_direct_relationship__c = false
                , name = TEST_STRING+String.ValueOf('TEST OPPTY Revenue')
                , stageName = 'Closed Won'
                , Primary_Billing_Contact__c = cont.id  // This is to address the validation rule where Primary Billing Contact is now a required field for all opportunities 
                , Lead_Campaign_Manager__c = UserInfo.getUserId()
                , Agency__c = acc2.id 
                , probability = 100 
                , Sales_Planner_NEW__c = UserInfo.getUserId()
                , Primary_Contact__c = cont.id
            );
            opp.ContractStartDate__c = Date.today();
            opp.ContractEndDate__c = Date.today()+50;

        opps.add(opp);
        
        insert opps;

        Product2 prod = new Product2(Name = 'Pandora One - 6 Month', 
                                   Family = 'Hardware');
        insert prod;
            
        Id pricebookId = Test.getStandardPricebookId();

        PricebookEntry standardPrice = new PricebookEntry(
          Pricebook2Id = pricebookId, Product2Id = prod.Id,
          UnitPrice = 10000, IsActive = true, CurrencyIsoCode = 'USD');

        insert standardPrice;

        List<OpportunityLineItem> oliLst = new List<OpportunityLineItem>();
        OpportunityLineItem oli = new OpportunityLineItem(OpportunityId = opps[0].Id, PricebookEntryId = standardPrice.Id, Quantity = 1, 
                ServiceDate = System.Today(), End_Date__c = System.Today().addDays(100), UnitPrice = 100);
            oliLst.add(oli);
        
    
        
        OpportunityTriggerUtil.insertOLIRecords(oliLst);
        
        Test.startTest();

            AccountRevenueBatch obj = new AccountRevenueBatch();
            DataBase.executeBatch(obj); 
            
        Test.stopTest();
        list<Account> acctUpdated = new list<Account>();
        Opportunity currOppObj = [SELECT Id,StageName,Probability,CloseDate,accountId,account.Account_Total_Revenue__c 
                                    FROM Opportunity 
                                    WHERE Id =: opps[0].Id];
        System.assert(currOppObj.accountId != null);
        //acctUpdated = [select id, Account_Total_Revenue__c from Account where id=:opps.accountId limit 1];
        //system.assert(acctUpdated.size()>0);
        system.debug('currOppObj.Account_Total_Revenue__c' + currOppObj.account.Account_Total_Revenue__c);
        system.assert(currOppObj.account.Account_Total_Revenue__c>0);





    }


    static testMethod void  callErrorLogTest()
    {

        Error_Log__c ec = AccountRevenueUtil.createError('AccountRevenueTest','TestError','1');
        system.assert(ec!=null);

    }

    static testMethod void accountRevenueSchedulerTest() {

        String CRON_EXP = '0 0 0 15 3 ? 2022';
        Test.startTest();
            // Schedule the test job
            String jobId = System.schedule('TestAccountRevenueScheduler',CRON_EXP,new AccountRevenueScheduler());
        Test.stopTest();
        // Get the information from the CronTrigger API object
        CronTrigger ct = [SELECT Id,CronExpression,
                                TimesTriggered,NextFireTime 
                            FROM CronTrigger WHERE id = :jobId];
        // Verify the expressions are the same
        System.assertEquals(CRON_EXP,ct.CronExpression);
        // Verify the job has not run
        System.assertEquals(0, ct.TimesTriggered);
        // Verify the next time the job will run
        System.assertEquals('2022-03-15 00:00:00',String.valueOf(ct.NextFireTime));
    }
}