/***************************************************************************************************************
*  Name                 : AccountRevenueScheduler
*  Author               : Sridharan Subramanian
*  Last Modified Date   : 01/17/2018
*  Description          : Used to Calculate Account based Revenue that has opportunities which are closed with in the Last 1 year
*                          
*                          
************************************************************************************************************************/
/************************************************************************************************************************
* Modification History
* Modified By   : Sridharan Subramanian
* Modified Date : 01/23/2018
* Description   : 
*************************************************************************************************************************/
global class AccountRevenueScheduler implements Schedulable {
	global void execute(SchedulableContext sc) {
		
		 AccountRevenueBatch accountRevenueBatch = new AccountRevenueBatch(); 
		 
         database.executebatch(accountRevenueBatch,500);
    }
}