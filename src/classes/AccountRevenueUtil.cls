/***************************************************************************************************************
*  Name                 : AccountRevenueUtil
*  Author               : Sridharan Subramanian
*  Last Modified Date   : 01/23/2018
*  Description          : Used to handle all Account Revenue Calculation related events and Error Logging
*                          
*                          
************************************************************************************************************************/
/************************************************************************************************************************
* Modification History
* Modified By   : Sridharan Subramanian
* Modified Date : 01/23/2018
* Description   : 
*************************************************************************************************************************/

public with sharing class AccountRevenueUtil {
	
    /*───────────────────────────────────────────────────────────────────────────┐
    // param			type
    // errStr			String [error]
    // jobId			String [Batch Job Id]
    // srcClsStr		String [source class]
	// ──────────────────────────────────────────────────────────────────────────
	// Description: createError	:Record Errors
	//									
	//───────────────────────────────────────────────────────────────────────────┘*/
    public static Error_Log__c createError(String errStr,
    								String jobId,
    								String srcClsStr) {

    	Error_Log__c errorRec = new Error_Log__c(Class__c = srcClsStr,
    											Error__c = errStr,
    											Type__c = 'Error',
    											ID_List__c = jobId);
    	return errorRec;
    }

    /*───────────────────────────────────────────────────────────────────────────┐
    // param			type
    // accountMap		map<id,account>
	// ──────────────────────────────────────────────────────────────────────────
	// Description: CalculateTotalRevenue for the accounts which has the 
					opportunity closed won with in the last 1 yr								
	//───────────────────────────────────────────────────────────────────────────┘*/
    public static Map<id,decimal> CalculateTotalRevenue(set<id> accountIdSet)
    {
    	Map<id,decimal> accountRevenueMap = new Map<id,decimal>();
    	for(AggregateResult ar:[select accountId,sum(amount) from opportunity where accountId in:accountIdSet and stageName=:'Closed Won' and CloseDate>=LAST_N_DAYS:365 group by accountId])
		{
			accountRevenueMap.put((Id)ar.get('accountId'),(decimal)ar.get('expr0'));

		}
		return accountRevenueMap;
    }
}