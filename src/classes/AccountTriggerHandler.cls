public class AccountTriggerHandler {
    public Boolean isExecuting;
    public Integer size;
    public Set<Id> setOppIds;
    public AccountTriggerHandler(Boolean isExecuting, Integer size){
        this.isExecuting = isExecuting;
        this.size = size;       
    }
    
    /*public void onAfterUpdate(Account[] accounts, Map<Id,Account> oldAccountMap, Map<Id,Account> newAccountMap){
        if(!SABRIX_Constants.runSabrixBatchOnce){
            setOppIds = new Set<Id>();
            for(Opportunity objOpp : [Select Id, 
                                             RecordType.DeveloperName, 
                                             AccountId, 
                                             Account.BillingCity, 
                                             Account.BillingCountry, 
                                             Account.BillingPostalCode, 
                                             Account.BillingState, 
                                             Account.BillingStreet
                                        from Opportunity where AccountId In : newAccountMap.keySet() 
                                        AND  StageName != 'Closed Lost' 
                                        AND  StageName != 'Closed Won' 
                                        AND  RecordType.DeveloperName = 'Opportunity_Pandora_One_Subs']){
                if((objOpp.Account.BillingCity != null && objOpp.Account.BillingCity != oldAccountMap.get(objOpp.AccountId).BillingCity) ||
                   (objOpp.Account.BillingCountry != null && objOpp.Account.BillingCountry != oldAccountMap.get(objOpp.AccountId).BillingCountry) ||
                   (objOpp.Account.BillingPostalCode != null && objOpp.Account.BillingPostalCode != oldAccountMap.get(objOpp.AccountId).BillingPostalCode) ||
                   (objOpp.Account.BillingState != null && objOpp.Account.BillingState != oldAccountMap.get(objOpp.AccountId).BillingState) ||
                   (objOpp.Account.BillingStreet != null && objOpp.Account.BillingStreet != oldAccountMap.get(objOpp.AccountId).BillingStreet)
                   ){
                    setOppIds.add(objOpp.Id);
               } 
            }
            if(!setOppIds.isEmpty()){
                OPT_CalculateTaxFromSabrixAPI calculateTax = new OPT_CalculateTaxFromSabrixAPI();
                String strOppIds = '(';
                for(String s: setOppIds){
                    if(s != null)
                    strOppIds += '\'' + s + '\',';
                }
                strOppIds = strOppIds.substring(0,strOppIds.length()-1) + ')';  
                system.debug('*****strOppIds'+strOppIds);                            
                calculateTax.query = 'SELECT Id, Opportunity.Account.Name,' +
                                     'Opportunity.Account.BillingStreet,'+ 
                                     'Opportunity.Account.BillingCity,' +
                                     'Opportunity.Account.BillingState,' +
                                     'Opportunity.Account.BillingPostalCode,' +
                                     'Opportunity.FinancialForce_Invoice_Number__r.Name,'+ 
                                     'UnitPrice, Quantity, Sales_Tax__c' +
                                     ' FROM OpportunityLineItem ' + 
                                     ' WHERE OpportunityId In ' + strOppIds;
                calculateTax.email='sfdcace@gmail.com';
                SABRIX_Constants.runSabrixBatchOnce = true;
                ID batchprocessid = Database.executeBatch(calculateTax, 1);
                System.debug('Returned batch process ID: ' + batchProcessId);
            }
        }
    }*/
    
    public void updateOpportunitySplitOwner(Account[] accounts, Map<Id,Account> oldAccountMap, Map<Id,Account> newAccountMap) {
        Map<Id, Id> mapOfAccountIdToOwnerId = new Map<Id, Id>();//Map of Account Id to changed Owner Id
        Map<Id, Opportunity_Split__c> mapOfSalesPersonIdToOppSplit = new Map<Id, Opportunity_Split__c>(); //mapOfSalesPersonIdToOppSplit map to hold OwnerId to split updates
        Opportunity_Split__c[] opportunitySplitDeletes = new Opportunity_Split__c[0]; //Opportunity_Split__c sObject to hold SalesPerson deletes
        Opportunity_Split__c[] opportunitySplitUpdates = new Opportunity_Split__c[0]; //Opportunity_Split__c sObject to hold SalesPerson updates
        for(Account acc : accounts){
            if(UpdateOpportunitySplit.hasChanges('OwnerId',oldAccountMap.get(acc.Id),acc)){
                mapOfAccountIdToOwnerId.put(acc.Id, acc.OwnerId);
            }  
        }
        
        if(! mapOfAccountIdToOwnerId.isEmpty()) {
            for(Opportunity opp: [Select OwnerId, 
                                         AccountId, 
                                         Id, 
                                         (Select Salesperson__c, 
                                                 Id, 
                                                 Opportunity_Owner__c, 
                                                 Split__c 
                                            FROM Opportunity_Split__r 
                                            ORDER BY Opportunity_Owner__c desc) 
                                    from Opportunity 
                                    where AccountId=: mapOfAccountIdToOwnerId.keySet()]) {  
                String ownerId = mapOfAccountIdToOwnerId.get(opp.AccountId);//get the new OwnerId value for the account
                Decimal percent = 0;//Split percent initialized to 0
                for(Opportunity_Split__c split : opp.Opportunity_Split__r){
                    if(split.Opportunity_Owner__c){//check for the first record which would be Opportunity Owner
                        if(opp.OwnerId == ownerId && split.Salesperson__c != ownerId){
                            split.Salesperson__c = opp.OwnerId;
                            mapOfSalesPersonIdToOppSplit.put(split.Salesperson__c, split);//Put Salesperson to split in map
                            percent += split.Split__c;//Add the existing split percent
                            system.debug('**********percent1' + percent);
                        }
                    } else{
                        if(split.Salesperson__c == ownerId){//Check if changed ownerid already existed in multiple split scenario
                            if(mapOfSalesPersonIdToOppSplit.containsKey(split.Salesperson__c)) {
                                percent += split.Split__c;
                                system.debug('**********percent2' + percent);
                                mapOfSalesPersonIdToOppSplit.get(split.Salesperson__c).Split__c = percent;
                                system.debug('**********mapOfSalesPersonIdToOppSplit' + mapOfSalesPersonIdToOppSplit);
                                opportunitySplitDeletes.add(split); //add the Opportunity_Split__c to our List of deletes
                            }
                        }
                    }
                }
                if(! mapOfSalesPersonIdToOppSplit.isEmpty()) {
                    opportunitySplitUpdates.addAll(mapOfSalesPersonIdToOppSplit.values());
                    mapOfSalesPersonIdToOppSplit.clear();
                }
            }
            
            
            try {
                if(!opportunitySplitUpdates.isEmpty()) {
                    update opportunitySplitUpdates;
                }
                if(! opportunitySplitDeletes.isEmpty()) {
                    delete opportunitySplitDeletes;
                }
            }catch(Exception ex) {
                System.Debug('reassignSplitOwner failure: '+ex.getMessage()); //write error to the debug log
            }
                
        }
    }

    public void addAccountOwnerAsTeamMember(Account[] accounts, Map<Id,Account> oldAccountMap, Map<Id,Account> newAccountMap) {
        Map<Id, Id> mapOfAccountIdToOwnerId = new Map<Id, Id>();//Map of Account Id to changed Owner Id
        Map<Id, AccountTeamMember> mapOfOwnerIdToATM = new Map<Id, AccountTeamMember>(); //mapOfOwnerIdToATM map to hold OwnerId to ATN updates
        //AccountTeamMember[] atmDeletes = new AccountTeamMember[0]; //AccountTeamMember sObject to hold ATM deletes
        AccountTeamMember[] atmUpdates = new AccountTeamMember[0]; //AccountTeamMember sObject to hold ATM updates
        for(Account acc : accounts){
            if(Trigger.isUpdate) {
                if(UpdateOpportunitySplit.hasChanges('OwnerId',oldAccountMap.get(acc.Id),acc)){
                    mapOfAccountIdToOwnerId.put(acc.Id, acc.OwnerId);
                }
            } else if(Trigger.isInsert) {
                mapOfAccountIdToOwnerId.put(acc.Id, acc.OwnerId);
            }
            
        }
        
        if(! mapOfAccountIdToOwnerId.isEmpty()) {
            for(Account acc: [Select Id, (SELECT Id, TeamMemberRole, UserId 
                                          FROM AccountTeamMembers) FROM Account WHERE
                                          Id = :mapOfAccountIdToOwnerId.keySet()]) {
            
            String ownerId = mapOfAccountIdToOwnerId.get(acc.Id);//get the new OwnerId value for the account
            String oldOwnerId = '';
            if(Trigger.isUpdate && oldAccountMap.containsKey(acc.Id) && oldAccountMap.get(acc.Id).OwnerId != null) {
                oldOwnerId = oldAccountMap.get(acc.Id).OwnerId;//get the old OwnerId value for the account
            }
            if(!acc.AccountTeamMembers.isEmpty()) {
                Boolean hasTeamMember = false;
                for(AccountTeamMember atm: acc.AccountTeamMembers) {
                    if(atm.UserId == ownerId) {
                        atmUpdates.add(new AccountTeamMember(accountid = acc.Id, TeamMemberRole=Label.AT_OwnerTeamRole, UserId = ownerId));
                        //atmDeletes.add(atm);
                        hasTeamMember = true;
                    }
                    /*if(atm.UserId == oldOwnerId) {
                        atmDeletes.add(atm);
                    }*/
                }
                if(!hasTeamMember) {
                    atmUpdates.add(new AccountTeamMember(accountid = acc.Id, TeamMemberRole=Label.AT_OwnerTeamRole, UserId = ownerId));
                }
            } else {
                atmUpdates.add(new AccountTeamMember(accountid = acc.Id, TeamMemberRole=Label.AT_OwnerTeamRole, UserId = ownerId));
            }
        }                                          
        }
        try {
            //system.debug('***********atmDeletes' + atmDeletes);
            system.debug('***********atmUpdates' + atmUpdates);            
            /*if(! atmDeletes.isEmpty()) {
                delete atmDeletes;
            }*/
             if(!atmUpdates.isEmpty()) {
                insert atmUpdates;
            }
        }catch(Exception ex) {
            System.Debug('addAccountTeamMember failure: '+ex.getMessage()); //write error to the debug log
        }
        
    }
     
}