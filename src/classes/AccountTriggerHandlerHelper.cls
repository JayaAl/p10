/* ──────────────────────────────────────────────────────────────────────────────────────────────────
* @author         Abhishek Bidap
* @modifiedBy     Abhishek Bidap
* @maintainedBy   Abhishek Bidap
* @version        1.0
* @created        2018-01-25
* @modified       2018-01-25
* @systemLayer    service Helper
* ──────────────────────────────────────────────────────────────────────────────────────────────────
*/
public class AccountTriggerHandlerHelper
{	
	/*
	*@ DESCRIPTION : Populates Forseva Account fields based on Credit policy and collection policy
	*				 Populates ERP Collector Fields on Account based on config set in Custom setting : Auto Assign Collector
	*				 Serves for Existing Trigger : 	ForsevaAccountUpsertTrigger
	*												Account_OnCreate (Before Insert)
	*/
	public static void updateForsevaValues(List<Account> newAcctLst, Boolean isInsertContext)
	{
		try {
		

			Map<String, forseva1__CollectionsPolicy__c> collectionPolNameMap = new Map<String, forseva1__CollectionsPolicy__c>();
			Map<String, forseva1__CreditPolicy__c> creditPolNameMap = new Map<String, forseva1__CreditPolicy__c>();
			Set<id> acctOwnerIdSet = new Set<Id>();
			AccountTriggerUtil.getAutoAssignCollectorCS();

			// Get territory for all Account Owners
			for (Account acc : newAcctLst) {
	            acctOwnerIdSet.add(acc.OwnerId);
	        } 
	        AccountTriggerUtil.getUserTerritory(acctOwnerIdSet);
	        
	        // Query All Credit policies
	        for (forseva1__CreditPolicy__c crp : AccountTriggerUtil.queryCreditPolicy()) {
	            creditPolNameMap.put(crp.Name, crp);
	        }

	        // Query All Collection policies
			for(forseva1__CollectionsPolicy__c collectionPolicy : AccountTriggerUtil.queryCollectionPolicy()){
				collectionPolNameMap.put(collectionPolicy.Name,collectionPolicy);
			}

			for(Account acc : newAcctLst) {
				
				if(isInsertContext){ // Populate ERP Collector Fields on Account based on config set in Custom setting : Auto Assign Collector
					String userTeritory = ( AccountTriggerUtil.usermap.containsKey(acc.OwnerId) && ( AccountTriggerUtil.usermap.get(acc.OwnerId).Territory__c != null || AccountTriggerUtil.usermap.get(acc.OwnerId).Territory__c != '') ) ? (AccountTriggerUtil.usermap.get(acc.OwnerId)).Territory__c : '0' ;
		            system.debug('userTeritory ===>'+userTeritory);
		            // Remove space and , from the list
	                String acctCollectionPolicy = ((acc.Collections_Policy_Territory__c != null || acc.Collections_Policy_Territory__c != '') ? (acc.Collections_Policy_Territory__c) : '0'); 
	                if(acctCollectionPolicy != null){
	                    acctCollectionPolicy = acctCollectionPolicy.replace(',',' ');
	                    acctCollectionPolicy = acctCollectionPolicy.replaceAll( '\\s+', '');
	                }   
	                system.debug('acctCollectionPolicy ===>'+acctCollectionPolicy);
	                acc.ERP_Credit_Manager__c = (AccountTriggerUtil.autoAssignCollectorMap.get(userTeritory+acctCollectionPolicy) != null) ? (AccountTriggerUtil.autoAssignCollectorMap.get(userTeritory+acctCollectionPolicy)).Collector_Name__c : null;
				}

				// asspcoate Collection policy
				if(acc.Collections_Policy_Territory__c != null) 
	                acc.forseva1__CollectionsPolicy__c = (collectionPolNameMap.containsKey(acc.Collections_Policy_Territory__c)) ? collectionPolNameMap.get(acc.Collections_Policy_Territory__c).Id : null;
	           	
	           	if(!acc.Override_Credit_Policy_Auto_Assignment__c) {
	                if (acc.Type == AccountTriggerUtil.ACCOUNT_TYPE_ADVERTISER) {
	                    acc.forseva1__Dunning_Enabled__c = true;
	                    acc.forseva1__Credit_Policy__c = creditPolNameMap.containsKey('Agency') ? creditPolNameMap.get('Agency').Id : null;
	                }
	                else if (acc.Type == AccountTriggerUtil.ACCOUNT_TYPE_AD_AGENCY) {
	                    acc.forseva1__Credit_Policy__c = creditPolNameMap.containsKey('Agency') ? creditPolNameMap.get('Agency').Id : null;
	                }
	            }

	            if (acc.D_U_N_S__c != null) {
	                acc.forseva1__DUNS_Number__c = acc.D_U_N_S__c;
	            }

	            // Account_OnCreate : Before Insert
	            if (isInsertContext && ( (acc.Type == AccountTriggerUtil.ACCOUNT_TYPE_ADVERTISER) || (acc.Type == AccountTriggerUtil.ACCOUNT_TYPE_AD_AGENCY) ) ){
	            	 acc.c2g__CODAAccountsReceivableControl__c = Label.General_Ledger_Account;
	        	}

			}

		} catch(Exception e) {
		
            Logger.logMessages(new List<Error_Log__c> {new Error_Log__c(Class__c = 'AccountTriggerHandlerHelper',
                        Method__c = 'updateForsevaValues',
                        Error__c = e.getMessage()+e.getStackTraceString(),
                        Object__c = 'Account',
                        ID_List__c = null,
                        Running_user__c = UserInfo.getUserId(),
                        Type__c = 'Error'
                    )});
		}
	}

	/*
	*@ DESCRIPTION : Used for Account merge functionality , Creates map of Account id and old contact list, Once map is populate it is used in After Update scenario
	*
	*				 Serves for Trigger : 	Account_OnCreate (Before Delete)
	*/

	public static void generateAccountOldContMapForMerge(List<Account> oldAccountLst){

		Map<Id,List<Contact>> acctContactMap = new Map<Id,List<Contact>>(); // Map of Account Id=> Old Contact List (Before Merge)
		Boolean isMergeCondn = false;

		try {
		
			for(Account acctObj : oldAccountLst){
				if(Test.isRunningTest() || acctObj.MasterRecordId != null){
					isMergeCondn = true;
					break;
				}
			}

			if(isMergeCondn){
				
				for( Contact contactObj : AccountTriggerUtil.getMergedContacts(oldAccountLst)){
		            if(acctContactMap.containsKey(contactObj.AccountId)){
		                (acctContactMap.get(contactObj.AccountId)).add(contactObj);
		            }else{
		                acctContactMap.put(contactObj.AccountId, new List<Contact>{contactObj});
		            }
		        }

		        MergeAccountService.acctIdToContIdStaticMap = acctContactMap;
	    	}

    	} catch(Exception e) {
		
            Logger.logMessages(new List<Error_Log__c> {new Error_Log__c(Class__c = 'AccountTriggerHandlerHelper',
                        Method__c = 'generateAccountOldContMapForMerge',
                        Error__c = e.getMessage()+e.getStackTraceString(),
                        Object__c = 'Account',
                        ID_List__c = null,
                        Running_user__c = UserInfo.getUserId(),
                        Type__c = 'Error'
                    )});
		}
	}

	/*
	*@ DESCRIPTION : Used for Account merge functionality , Invokes Merge Account Service , passes the static Account map maintained
	*
	*				 Serves for Trigger : 	Account_OnCreate (After Update)
	*/
	public static void invokeMergeAccountService(){
		

		if(MergeAccountService.acctIdToContIdStaticMap != null){
            MergeAccountService mergeService = new MergeAccountService();
            mergeService.acctIdToContIdMap = MergeAccountService.acctIdToContIdStaticMap;
            System.enqueueJob(mergeService);
        }
	}

	/*
	*@ DESCRIPTION : Used for Setting DMA fields on Account on insert and update. It checks the data from DMA master data and if match is found sets values based on master
	*
	*				 Serves for Trigger : 		DMA_UpdateOrAddDMATrigger (Before Insert , Before Update)
	*/
	public static void addUpdateDMAonAccount(List<Account> newAcctLst, Map<Id,Account> oldAcctMap , Boolean isUpdate , Boolean isInsert){

		 //first check if the trigger is not fired from a Scheduler or custom code to perform this operation
    	if(ACC_Static_Helper.runDMATrigger) {
    
	        //Variables
	        set <string> zipCodes = new set<string>();
	        set <string> states= new set<string>();
	        set <string> cities= new set<string>();
	        set <string> recordTypeIds = new set<string>();
	        map<string, DMA__c> zipDMA = new map<String, DMA__c>();
	        map<string, DMA__c> cityStateDMA = new map<String, DMA__c>();
	        map<string, string> recordTypeMap = new map<string, string>();
	     
	        //zip, state, city, and recordType sets
	        for(Account a : newAcctLst){
	            if(a.BillingPostalCode != null && a.BillingPostalCode.length() >= 5)
	                zipCodes.add(a.BillingPostalCode.subString(0,5));
	            if(a.BillingState != null)
	                states.add(a.BillingState.toUpperCase());
	            if(a.BillingCity!=null)
	                cities.add(a.BillingCity.toUpperCase());
	            if(a.RecordTypeId !=null)           
	                recordTypeIds.add(a.RecordTypeId);
	        }
        
	        // v1.1
	        // The purpose of this query is to get the 
	        // recordtypId for default recordTypeId.
	        Id defaultRecordTypeId = Schema.SObjectType.Account.getRecordTypeInfosByName().get(AccountTriggerUtil.ACCOUNT_RT_DEFAULT).getRecordTypeId();
	        // old-- 
	        //get record type objects and make map
	        /*list <RecordType> recordTypes = [Select SobjectType, Name, IsActive, Id From RecordType where id in :recordTypeIds];
	        for (RecordType rt:recordTypes){
	            recordTypeMap.put(rt.Id, rt.Name);      
	        }*/
        
	        //get list of DMAS
	        list <DMA__c> dmaList =  AccountTriggerUtil.getDMAList(zipCodes, cities, states);
	        
	        //make DMA maps
	        for (DMA__c dma:dmaList){
	            zipDMA.put(dma.Zip_Code__c, dma);
	            cityStateDMA.put(dma.city_name__c+dma.state__c, dma);	            
	        }
        
	        //params needed in for loop
	        Boolean isChangeOfAddress;
	        DMA__c dmaMatch;
	        Account oldAccount;
	        String recordTypeName;
        
	        for(Account a: newAcctLst){
	            isChangeOfAddress = false;
	            
	            //check for update and if address changed
	            if(isUpdate){
	                oldAccount = oldAcctMap.get(a.ID);
	                if(a.BillingPostalCode !=null && a.BillingPostalCode.length() >= 5)
	                    isChangeOfAddress = a.BillingPostalCode != oldAccount.BillingPostalCode;
	                if(a.BillingPostalCode == null && a.BillingCity!=null && a.BillingState != null)
	                    isChangeOfAddress = !((a.BillingCity+a.BillingState).equalsIgnoreCase(oldAccount.BillingCity+oldAccount.BillingState));
	                //if((a.BillingPostalCode !=null ||(a.BillingCity!=null && a.BillingState != null)) && a.DMA_Code__c == null)
	                if(a.DMA_Name__c == AccountTriggerUtil.ACCOUNT_DMA_NAME_UPDATE)//this is for batch update
	                    isChangeOfAddress = true;
	            }
	            
	            dmaMatch = null;
	            // v1.1 
	            // old version--
	            // recordTypeName = recordTypeMap.get(a.RecordTypeId);
	            //update values if found in DMA maps
	            // if((Trigger.isInsert || isChangeOfAddress) && (a.RecordTypeId ==null ||(recordTypeName != null && recordTypeName.equalsIgnoreCase('default')))){
	            if((isInsert || isChangeOfAddress) 
	                && (a.RecordTypeId ==null 
	                    ||(defaultRecordTypeId == a.RecordTypeId))) {
	                // v1.1 
	                // (defaultRecordTypeId == a.RecordTypeId) added in version 1.1
	                a.DMA_Code__c = 0;
	                a.DMA_Name__c = AccountTriggerUtil.ACCOUNT_DMA_NAME_NONE;
	                if(a.BillingPostalCode != null && a.BillingPostalCode.length() >= 5){
	                    dmaMatch = zipDMA.get(a.BillingPostalCode.subString(0,5));
	                }
	                if(dmaMatch == null && a.BillingCity != null && a.BillingState != null){
	                    dmaMatch = cityStateDMA.get(a.BillingCity.toUpperCase()+a.BillingState.toUpperCase());
	                }
	                if(dmaMatch != null){
	                	a = AccountTriggerUtil.assignDMAVAlues(a,dmaMatch);	                    
	                } 
	                else{
	                    a = AccountTriggerUtil.assignDMAVAlues(a,null);	                   
	                }
	            }
	        } 

		}

	}

	
	/*
	*@ DESCRIPTION : Updates Opportunity Splits Data whenever Owner on Account is changed
	*
	*				 Serves for Trigger : 		
	*/
	public static void updateOpportunitySplitOwner(Account[] accounts, Map<Id,Account> oldAccountMap, Map<Id,Account> newAccountMap) {
    
        Map<Id, Id> mapOfAccountIdToOwnerId = new Map<Id, Id>();//Map of Account Id to changed Owner Id
        Map<Id, Opportunity_Split__c> mapOfSalesPersonIdToOppSplit = new Map<Id, Opportunity_Split__c>(); //mapOfSalesPersonIdToOppSplit map to hold OwnerId to split updates
        Opportunity_Split__c[] opportunitySplitDeletes = new Opportunity_Split__c[0]; //Opportunity_Split__c sObject to hold SalesPerson deletes
        Opportunity_Split__c[] opportunitySplitUpdates = new Opportunity_Split__c[0]; //Opportunity_Split__c sObject to hold SalesPerson updates
    
        for(Account acc : accounts){
            if(oldAccountMap != null && UpdateOpportunitySplit.hasChanges(AccountTriggerUtil.ACCOUNT_OWNERID_FIELD,oldAccountMap.get(acc.Id),acc)){   // update trigger             
                mapOfAccountIdToOwnerId.put(acc.Id, acc.OwnerId);
            } 
        }
        
        if(! mapOfAccountIdToOwnerId.isEmpty()) {

            for(Opportunity opp: AccountTriggerUtil.getOpptyAssociatedToAcct(mapOfAccountIdToOwnerId.keySet())) {  
                String ownerId = mapOfAccountIdToOwnerId.get(opp.AccountId);//get the new OwnerId value for the account
                Decimal percent = 0;//Split percent initialized to 0
                for(Opportunity_Split__c split : opp.Opportunity_Split__r){
                    if(split.Opportunity_Owner__c){//check for the first record which would be Opportunity Owner
                        if(opp.OwnerId == ownerId && split.Salesperson__c != ownerId){
                            split.Salesperson__c = opp.OwnerId;
                            mapOfSalesPersonIdToOppSplit.put(split.Salesperson__c, split);//Put Salesperson to split in map
                            percent += split.Split__c;//Add the existing split percent
                            system.debug('**********percent1' + percent);
                        }
                    } else{
                        if(split.Salesperson__c == ownerId){//Check if changed ownerid already existed in multiple split scenario
                            if(mapOfSalesPersonIdToOppSplit.containsKey(split.Salesperson__c)) {
                                percent += split.Split__c;
                                system.debug('**********percent2' + percent);
                                mapOfSalesPersonIdToOppSplit.get(split.Salesperson__c).Split__c = percent;
                                system.debug('**********mapOfSalesPersonIdToOppSplit' + mapOfSalesPersonIdToOppSplit);
                                opportunitySplitDeletes.add(split); //add the Opportunity_Split__c to our List of deletes
                            }
                        }
                    }
                }
                if(! mapOfSalesPersonIdToOppSplit.isEmpty()) {
                    opportunitySplitUpdates.addAll(mapOfSalesPersonIdToOppSplit.values());
                    mapOfSalesPersonIdToOppSplit.clear();
                }
            }
            
            
            try {
                if(!opportunitySplitUpdates.isEmpty()) {
                    update opportunitySplitUpdates;
                }
                if(! opportunitySplitDeletes.isEmpty()) {
                    delete opportunitySplitDeletes;
                }
            }catch(Exception e) {
                System.Debug('reassignSplitOwner failure: '+e.getMessage()); //write error to the debug log
                Logger.logMessages(new List<Error_Log__c> {new Error_Log__c(Class__c = 'AccountTriggerHandlerHelper',
                        Method__c = 'updateOpportunitySplitOwner',
                        Error__c = e.getMessage()+e.getStackTraceString(),
                        Object__c = 'Account',
                        ID_List__c = null,
                        Running_user__c = UserInfo.getUserId(),
                        Type__c = 'Error'
                    )});
            }
                
        }
    }

    /*
	*@ DESCRIPTION : Add Account Team Members once Account is created / Owner is changed.
	*
	*				 Serves for Trigger : 		
	*/

     public static void addAccountOwnerAsTeamMember(Account[] accounts, Map<Id,Account> oldAccountMap, Map<Id,Account> newAccountMap) {
        Map<Id, Id> mapOfAccountIdToOwnerId = new Map<Id, Id>();//Map of Account Id to changed Owner Id
        Map<Id, AccountTeamMember> mapOfOwnerIdToATM = new Map<Id, AccountTeamMember>(); //mapOfOwnerIdToATM map to hold OwnerId to ATN updates
        //AccountTeamMember[] atmDeletes = new AccountTeamMember[0]; //AccountTeamMember sObject to hold ATM deletes
        AccountTeamMember[] atmUpdates = new AccountTeamMember[0]; //AccountTeamMember sObject to hold ATM updates
        for(Account acc : accounts){
            if(Trigger.isUpdate) {
                if(UpdateOpportunitySplit.hasChanges(AccountTriggerUtil.ACCOUNT_OWNERID_FIELD,oldAccountMap.get(acc.Id),acc)){
                    mapOfAccountIdToOwnerId.put(acc.Id, acc.OwnerId);
                }
            } else if(Trigger.isInsert) {
                mapOfAccountIdToOwnerId.put(acc.Id, acc.OwnerId);
            }
            
        }
        
        if(! mapOfAccountIdToOwnerId.isEmpty()) {
            for(Account acc: AccountTriggerUtil.getAccountTeamMembers(mapOfAccountIdToOwnerId.keySet())) {
            
            String ownerId = mapOfAccountIdToOwnerId.get(acc.Id);//get the new OwnerId value for the account
            String oldOwnerId = '';
            if(Trigger.isUpdate && oldAccountMap.containsKey(acc.Id) && oldAccountMap.get(acc.Id).OwnerId != null) {
                oldOwnerId = oldAccountMap.get(acc.Id).OwnerId;//get the old OwnerId value for the account
            }
            if(!acc.AccountTeamMembers.isEmpty()) {
                Boolean hasTeamMember = false;
                for(AccountTeamMember atm: acc.AccountTeamMembers) {
                    if(atm.UserId == ownerId) {
                        atmUpdates.add(new AccountTeamMember(accountid = acc.Id, TeamMemberRole=Label.AT_OwnerTeamRole, UserId = ownerId));
                        //atmDeletes.add(atm);
                        hasTeamMember = true;
                    }
                    /*if(atm.UserId == oldOwnerId) {
                        atmDeletes.add(atm);
                    }*/
                }
                if(!hasTeamMember) {
                    atmUpdates.add(new AccountTeamMember(accountid = acc.Id, TeamMemberRole=Label.AT_OwnerTeamRole, UserId = ownerId));
                }
            } else {
                atmUpdates.add(new AccountTeamMember(accountid = acc.Id, TeamMemberRole=Label.AT_OwnerTeamRole, UserId = ownerId));
            }
        }                                          
        }
        try {
            //system.debug('***********atmDeletes' + atmDeletes);
            system.debug('***********atmUpdates' + atmUpdates);            
            /*if(! atmDeletes.isEmpty()) {
                delete atmDeletes;
            }*/
             if(!atmUpdates.isEmpty()) {
                insert atmUpdates;
            }
        }catch(Exception e) {
            System.Debug('addAccountTeamMember failure: '+e.getMessage()); //write error to the debug log
            Logger.logMessages(new List<Error_Log__c> {new Error_Log__c(Class__c = 'AccountTriggerHandlerHelper',
                        Method__c = 'addAccountOwnerAsTeamMember',
                        Error__c = e.getMessage()+e.getStackTraceString(),
                        Object__c = 'Account',
                        ID_List__c = null,
                        Running_user__c = UserInfo.getUserId(),
                        Type__c = 'Error'
                    )});
        }
        
    }

}