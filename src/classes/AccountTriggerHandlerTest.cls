@isTest
public class AccountTriggerHandlerTest {
    public static testMethod void testUpdateOpportunitySplitOwner() {
        //nsk: 10/23 Adding below logic to control trigger execution via custom label
        if(Label.Disable_Account_Triggers!='YES'){  
            OpportunitySplit__c split = new OpportunitySplit__c();
            split.Name = 'Split Setting';
            split.Create_Splits__c = true;
            insert split; 

            Account acc = UTIL_TestUtil.createAccount();
            Opportunity opp = UTIL_TestUtil.createOpportunity(acc.Id);
            User user;
            system.runAs(new User(Id = UserInfo.getUserId()) ) {
                user = UTIL_TestUtil.createUser();
            }   
            Test.startTest();
            opp.OwnerId = user.Id;
            update opp;
            List<Opportunity_Split__c> oSplitLst = [Select SalesPerson__c from Opportunity_Split__c  where Opportunity__c =: opp.Id];
            if(oSplitLst != null && !oSplitLst.isEmpty()){
                Opportunity_Split__c oSplit = oSplitLst[0];
                oSplit.SalesPerson__c = UserInfo.getUserId();
                update oSplit;
                acc.OwnerId = user.Id;
                update acc;
                oSplit = [Select SalesPerson__c from Opportunity_Split__c  where Opportunity__c =: opp.Id];
              //  system.assertEquals(acc.OwnerId, oSplit.SalesPerson__c);
            }
            Test.stopTest();
        }            
    }
}