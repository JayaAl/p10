/**
 * See https://github.com/financialforcedev/fflib-apex-common for more info
 *
 * Install library via
 *   https://githubsfdeploy.herokuapp.com/app/githubdeploy/financialforcedev/fflib-apex-common
 */

/**
 * Encapsulates all service layer logic for a given function or module in the application
 * 
 * For more guidelines and details see 
 *   https://developer.salesforce.com/page/Apex_Enterprise_Patterns_-_Service_Layer
 *
 **/
public class AccountTriggerUtil
{
	public static List<forseva1__CollectionsPolicy__c> collectionPolicyLst;
	public static Map<String,AutoAssignCollector__c> autoAssignCollectorMap;
	public static List<forseva1__CreditPolicy__c> creditPolicyLst;

	// COnstants
	public static final String ACCOUNT_TYPE_ADVERTISER = 'Advertiser';
	public static final String ACCOUNT_TYPE_AD_AGENCY  = 'Ad Agency';
	public static final String ACCOUNT_DMA_NAME_UPDATE  = 'update';
	public static final String ACCOUNT_DMA_NAME_NONE  = 'none';
	public static final String ACCOUNT_RT_DEFAULT  = 'default';
	public static final String ACCOUNT_OWNERID_FIELD  = 'OwnerId';
	
	public static Map<id,User> usermap ;

	public static List<forseva1__CollectionsPolicy__c> queryCollectionPolicy()
	{
		Integer limit_Int = Limits.getLimitQueryRows() - Limits.getQueryRows();
		if(collectionPolicyLst == null)
			collectionPolicyLst = [select Id, Name from forseva1__CollectionsPolicy__c Limit: limit_Int];

		return collectionPolicyLst;
	}

	public static List<forseva1__CreditPolicy__c> queryCreditPolicy()
	{
		Integer limit_Int = Limits.getLimitQueryRows() - Limits.getQueryRows();
		if(creditPolicyLst == null)
			creditPolicyLst = [select Id, Name from forseva1__CreditPolicy__c Limit: limit_Int];

		return creditPolicyLst;
	}

	public static Map<String,AutoAssignCollector__c> getAutoAssignCollectorCS(){
		
		if(autoAssignCollectorMap == null)
			autoAssignCollectorMap = AutoAssignCollector__c.getall(); 

		return autoAssignCollectorMap;
	}

	public static Map<Id,User> getUserTerritory(Set<Id> userIdSet){
		if(usermap == null)
			usermap = new Map<Id,User>([Select id,name,Territory__c from User where id in: userIdSet]);

		return usermap;
	}

	public static List<DMA__c> getDMAList(set <string> zipCodes, set <string> cities, set <string> states){

		return [Select Zip_Code__c, State__c, Name, DMA_Code__c, City_Name__c, Region__c, Territory__c, MSA__c, MSA_Threshold__c 
	                                 From DMA__c 
	                                 where Zip_Code__c in :zipCodes 
	                                 or (City_Name__c in :cities and State__c in :states)];
	}


	public static Account assignDMAVAlues(Account acct, DMA__c matchingDMA){

		acct.DMA_Code__c = (matchingDMA != null) ? matchingDMA.DMA_Code__c : null;
        acct.DMA_Name__c = (matchingDMA != null) ? matchingDMA.Name : null;
        acct.Region__c = (matchingDMA != null) ? matchingDMA.Region__c : null;
        acct.Territory__c = (matchingDMA != null) ? matchingDMA.Territory__c : null;
        acct.MSA__c = (matchingDMA != null) ? matchingDMA.MSA__c : null;   
        acct.MSA_Threshold__c = (matchingDMA != null) ? matchingDMA.MSA_Threshold__c : null;  

        return acct;
	}

	
	public static List<Opportunity> getOpptyAssociatedToAcct(Set<Id> acctIdSet){

		return [Select OwnerId, AccountId, Id, 
                     (Select Salesperson__c, 
                             Id, 
                             Opportunity_Owner__c, 
                             Split__c 
                        FROM Opportunity_Split__r 
                        ORDER BY Opportunity_Owner__c desc) 
		                from Opportunity 
		                where AccountId=: acctIdSet];
	}

	public static List<Account> getAccountTeamMembers(Set<id> acctIdSet){
		return [Select Id, (SELECT Id, TeamMemberRole, UserId 
                                          FROM AccountTeamMembers) FROM Account WHERE
                                          Id = :acctIdSet];
	}

	public static List<Contact> getMergedCOntacts(List<Account> acctLstChanged){
		return [SELECT Id, Name,AccountId,LastModifiedDate,Account.MasterRecordId FROM Contact where AccountId in: acctLstChanged AND Account.MasterRecordId != null ];
	}
}