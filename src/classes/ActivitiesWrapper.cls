/**
 * @name: ActivitiesWrapper 
 * @desc: Custom Sorter to sort by Last Modified Date
 * @author: Lakshman (sfdcace@gmail.com)
 * @date: 01-08-2014
 **/
global class ActivitiesWrapper implements Comparable{
    /*
    public DateTime lastModifiedDate {get;set;}
    public String whoId {get;set;}
    public String whatId {get;set;}
    public String whoName {get;set;}
    public String whatName {get;set;}
    public String assignedId {get;set;}
    public String assignedName {get;set;}
    public Boolean isTask {get;set;}
    public String subject {get;set;}
    public String id {get;set;}
    public Date ActivityDate{get;set;}
    */
    public ActivityHistory sObjA {get;set;}
    
    // Default sorting order --> Descending
    public static String SortDirection = 'Descending';
    
    // Default sorting field --> LastModifiedDate
    public static String SortField = 'LastModifiedDate';
    
    public ActivitiesWrapper(ActivityHistory sObj, String who_Name, String what_Name) {
        sObjA = sObj;
        /*
        lastModifiedDate = sObj.LastModifiedDate;
        whoId = sObj.whoId;
        whatId = sObj.whatId;
        whoId = sObj.whoId;
        whoName = sObj.who.name;
        whatName = sObj.what.name;
        assignedId = sObj.OwnerId;
        assignedName = sObj.Owner.Name;
        isTask = sObj.IsTask;
        subject = sObj.Subject;
        id = sObj.Id;
        ActivityDate = sObj.ActivityDate;
        */
    }
    global Integer compareTo(Object other) {
        // The return value of 0 indicates that both elements are equal.
        Integer returnValue = 0;
        ActivitiesWrapper aHW = (ActivitiesWrapper)other;
        if ( sObjA.LastModifiedDate < aHW.sObjA.LastModifiedDate) { 
            // Set return value to a positive value.
            return 1;
        } else if ( sObjA.LastModifiedDate > aHW.sObjA.LastModifiedDate) {
            // Set return value to a negative value. 
            return -1; 
        }      
        return returnValue;
    }
    
}