global with sharing class AdxImportSchduler  implements Schedulable,Database.AllowsCallouts  {
	
    global void execute(SchedulableContext sc) { 
        
        cleanupDealsLoadedYesterday();
        String EndPointURL=Pandora_AdExCallOutHandler.getEndPointURL_Preferred_PrivateAuctionDeals();
        /*if(test.isRunningTest())
        {
         
         return;
        }*/
        //string accessToken='ya29.CjGHA_kKaUGSOpNS1aujQo-dPxSSKf536dSDOugdLDaiI8ixKRJWvbvY5fRGIFEwyfZQ';
        Pandora_AdExImport padExp= new Pandora_AdExImport(null,EndPointURL,'Preferred deals');
        database.executeBatch(padExp, 2000);        
    }

    public void cleanupDealsLoadedYesterday()
    {
      //This method cleans up the deal loaded today. Incase if prior batch fails, the existing data is cleaned up before imorting the data again.
      date dt=Pandora_RevenueLoadBatchHelper.getDate();
      system.assert(dt!=null,'Date is required.');

      delete [select id from AdEx_Response_temp__c where date__c=:dt];
    } 

    // to be used with caution. delete the opportunity line items.
    /*public void rollbackrevenue()
    {

 
    }*/
	
	/*
	   Pandora_AdExImport: Import the deals for yesterday. To be scheduled to run everyday to load deals the day before.
       This schduler calls the batch Pandora_AdExImport with Preferred deals parameter which load preferred deals and private auctions
       The Pandora_AdExImport is self chained and loads opens auction and firstlook.


	*/
}