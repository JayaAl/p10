/**
* ─────────────────────────────────────────────────────────────────────────────────────────────────┐
* This class is  .
* ──────────────────────────────────────────────────────────────────────────────────────────────────
* @author         Jaya Alaparthi
* @maintainedBy   Jaya Alaparthi
* @version        1.0
* @created        2017-10-05
* @modified       
* @systemLayer    Util
* @see            ????
* @see            ????
* ──────────────────────────────────────────────────────────────────────────────────────────────────
* @changes
* v1.1            Jaya Alaparthi
* 2017-11-29      adding renewal rep as the sender for mail
* ──────────────────────────────────────────────────────────────────────────────────────────────────
* @changes
* v1.2            Jaya Alaparthi
* 2017-12-07	  using AutoEmailSubject from mapping purpose insted of Subject      
* ─────────────────────────────────────────────────────────────────────────────────────────────────┘
*/
global class AutoEmailSendBatch implements Database.Batchable<sObject> {
	
	String query;
	
	global AutoEmailSendBatch() {
		
		query = 'SELECT Id,WhatId,AutoEmailAccount__c,'
				+'EmailTo__c,Status,Subject,AutoEmailSubject__c,'
				+' ActivityDate,AutoEmailAccount__r.Id, '
				+' OwnerId,PrimaryContact__r.Account.DisableAutoEmailSendOut__c, '
				+' PrimaryContact__r.Id, AutoEmailFrom__c,SendersName__c '
				+' FROM Task '
				+' WHERE ActivityDate = today'
				+' AND AutoEmailFrom__c != null'
				+' AND 	PrimaryContact__r.Account.DisableAutoEmailSendOut__c = false';
	}
	
	global Database.QueryLocator start(Database.BatchableContext BC) {

		System.debug('query:'+query);
		return Database.getQueryLocator(query);
	}

   	global void execute(Database.BatchableContext BC, List<sObject> scope) {
		
		map<String,Task> finalEmailTasksMap = new map<String,Task>();
		list<Task> batchTaskList = new list<Task>();
		map<String,EmailTemplate> tskSubToEmailTemplateMap = new map<String,EmailTemplate>();
		list<Messaging.SingleEmailMessage> emailsList = new list<Messaging.SingleEmailMessage>();
		map<String,set<Id>> setOfWhatIdToAcctSubMap = new map<String,set<Id>>();

		batchTaskList = scope;
		System.debug('batchTaskList:'+batchTaskList);
		// retrive email template for every subject on Task. 
		// this map should contain 4 pairs one for each email template
		tskSubToEmailTemplateMap = AutoEmailSendUtil.getEmailTemplateMap();
		System.debug('tskSubToEmailTemplateMap:'+tskSubToEmailTemplateMap);
		
		//validates task for only one email in last one year for a given account
		// only one email for multiple opty under one account per criteria.
		finalEmailTasksMap = AutoEmailSendHelper.validateJobActivity(batchTaskList);
		System.debug('finalEmailTasksMap:'+finalEmailTasksMap);

		// map of AccountId|Subject to related Tasks
		setOfWhatIdToAcctSubMap = AutoEmailSendUtil.getWhatIdToAcctSubMap(batchTaskList);
		System.debug('setOfWhatIdToAcctSubMap:'+setOfWhatIdToAcctSubMap);

		// email build
		for(String key : finalEmailTasksMap.keySet()) {

			Messaging.SingleEmailMessage mail = new Messaging.SingleEmailMessage();
			EmailTemplate templateId = new EmailTemplate();

			Task taskRec = finalEmailTasksMap.get(key);
			

			//v1.1
			// set from as renewal rep which is in sync with AutoEmailFrom__c
			/*String [] toEmailAddressArray = new String []{};
			for(Integer i=0; i<emailAddressArr.size(); i++) {
				toEmailAddressArray.add(emailAddressArr[i]);
			}*/
			// v1.2
			if(taskRec.EmailTo__c != null 
					&& tskSubToEmailTemplateMap.containsKey(taskRec.AutoEmailSubject__c)) {

				String [] emailAddressArr = taskRec.EmailTo__c.split(';');

				templateId = tskSubToEmailTemplateMap.get(taskRec.AutoEmailSubject__c);
				mail.setTemplateId(templateId.Id);
				mail.saveAsActivity = false;
				mail.setTargetObjectId(taskRec.PrimaryContact__r.Id);
				mail.setWhatId(taskRec.WhatId);
				mail.setToAddresses(emailAddressArr);
				// v1.1
				//mail.setSenderDisplayName(toEmailAddressArray[0]);
				//String senderName = taskRec.AutoEmailFrom__c;	
				mail.setInReplyTo(taskRec.AutoEmailFrom__c);	
				mail.setSenderDisplayName(taskRec.SendersName__c);
				
				//mail.setSenderDisplayName('Auto Email Blabla');
				System.debug('mail:'+mail);
				emailsList.add(mail);
			}
		}
		if(emailsList.size() >0) {

			try {
				set<Id> targetIdsSuccessSet = new set<Id>();
				Set<Id> completedTasksSet = new Set<Id>();
                Set<Id> incompleteTasksSet = new Set<Id>();
                map<Id,String> targetIdToErrorMap = new map<Id,String>();
                map<Id,String> contactIdEmailErrorMap = new map<Id,String>();
                set<Task> afterEmailSentTaskSet = new set<Task>();

                System.debug('emailsList:'+emailsList);
                list<Messaging.SendEmailResult> emailSendResult = Messaging.sendEmail(emailsList);
                System.debug('emailSendResult:'+emailSendResult);

               // ApexPages.AddMessage(new ApexPages.Message(ApexPages.Severity.Confirm,'Sent!'));
                for(Messaging.SendEmailResult emailResult : emailSendResult) {
                	//setOfWhatIdToAcctSubMap
                	if(!emailResult.isSuccess()) {
                		// for every targetObjectId in the error create 
                		// a map with<targetObjectId,errorStr>
                		Messaging.SendEmailError [] errorArray = emailResult.getErrors(); 
						//errorArray[0].getMessage()
                		contactIdEmailErrorMap.put(errorArray[0].getTargetObjectId(),
                									errorArray[0].getMessage());
                	}
                }

                for(Task taskRec: batchTaskList) {
                	//v1.2
                	String key = taskRec.AutoEmailAccount__c
											+'|'+taskRec.AutoEmailSubject__c;
					Id currentTaskPCId = taskRec.PrimaryContact__c;
					String errorStr = '';

                	if(!setOfWhatIdToAcctSubMap.isEmpty()
                		&& setOfWhatIdToAcctSubMap.containsKey(key)) {

                		set<Id> opptyIdSet = setOfWhatIdToAcctSubMap.get(key);
                		if(!opptyIdSet.isEmpty() && !contactIdEmailErrorMap.isEmpty()
                			&& contactIdEmailErrorMap.containsKey(currentTaskPCId)) {

                			 errorStr = contactIdEmailErrorMap.get(currentTaskPCId);
                			 if(opptyIdSet.contains(taskRec.WhatId)) {
                			 	// updatet task with error msg
                			 	afterEmailSentTaskSet.add(new Task(Id = taskRec.Id,
                			 									AutoEmailResult__c = errorStr,
                			 									Status = 'Email Notification Error'));
                			 }
                		} else {

                			afterEmailSentTaskSet.add(new Task(Id = taskRec.Id,
                												Status = 'Email Notification Sent',
                			 									AutoEmailResult__c = 'Email Sent Successfull.'));
                		}
                		
                	}
                }
                if(!afterEmailSentTaskSet.isEmpty()) {

                	//AutoEmailSendUtil.AutoExecuted = false;
                	list<Task> updateTaskList = new list<Task>();
                	updateTaskList = new list<Task>(afterEmailSentTaskSet);
                	update updateTaskList;                    	
                }

                } Catch(Exception ex){
                    
                	Error_Log__c errorRec = AutoEmailSendUtil.createError(ex.getMessage(),
                														BC.getJobId(),
                														'AutoEmailSendBatch');
                	insert errorRec;
                }
		}
	}
	
	global void finish(Database.BatchableContext BC) {
		
	}
	
}