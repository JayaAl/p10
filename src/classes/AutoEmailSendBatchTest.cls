@isTest
private class AutoEmailSendBatchTest {
	
	@testSetup static void opportunityTestDataSetup() {
		// Implement test code
		list<Account> accountsList = new list<Account>();
		list<Contact> contactsList = new list<Contact>();


		Profile sysAdminProfile = [SELECT Id 
									FROM Profile 
									WHERE name = 'System Administrator' 
									LIMIT 1];
		/*User autoEmailTest = new User(lastName = 'batchAutoEmail',
					            userName = 'batchautoETest@email.com',
					            profileId = sysAdminProfile.Id,
					            alias = 'aEbT',
					            email = 'batchautoETest@email.com',
					            emailEncodingKey = 'ISO-8859-1',
					            languageLocaleKey = 'en_US',
					            localeSidKey = 'en_US',
					            timeZoneSidKey = 'America/Los_Angeles',
					            DisableAutoEmailSendOut__c = false);*/
        //System.runAs(autoEmailTest) {
			// create Account default
			for(Integer i=0; i<1; i++) {

				Account acct = new Account();
				acct.name = 'AutoEmailTestN'+i;
	            acct.Website = 'AutoEmailWebTest'+i+'.com';
	            acct.BillingStreet = 'AutoEmailWebTest'+i;
	            acct.BillingCity = 'AutoEmailWebTest'+i;
	            acct.BillingState = 'CA';
	            acct.BillingPostalCode = '45897643';
	            acct.c2g__CODAAccountTradingCurrency__c = 'USD';
	            acct.RenewalRep__c = UserInfo.getUserId();
				accountsList.add(acct);
			}
			insert accountsList;
			System.debug('accounts created in test data');
			// create Contact
			for(Integer i=0;i<1;i++) {

				Contact contact = new Contact();
				contact.lastName = 'autoLN'+i;
				contact.accountId = accountsList[i].Id;
				contact.MailingStreet = 'MyStreet1';
				contact.MailingCountry = 'USA';
				contact.MailingState = 'CA';
				contact.MailingCity = 'Fremont';
				contact.Email = 'autoEmail'+i+'@mydomain.com';
				contact.Title = 'autoEmail'+i;
				contact.firstname = 'autoEmailFN'+i;
				contact.Industry_Category__c = 'Ad Network';
				contact.GCLID__c = 'autoEmailTEst';
				contactsList.add(contact);
			}
			insert contactsList;
			System.debug('Contacts created in test data');
			
			// create opportunities
			list<Account> accountList = new list<Account>();
			list<Contact> contactList = new list<Contact>();
			list<Opportunity> opportunityList = new list<Opportunity>();
			list<Task> taskList = new list<Task>();

			accountList = [SELECT Id FROM Account];
			contactList = [SELECT Id,AccountId FROM Contact];
			Id opptyRecordTypeId = AutoEmailSendUtil.getRecordTypeId('Opportunity',
												AutoEmailSendUtil.INSIDE_SALES_RT);

			for(Integer i=0;i<1;i++) {

				Id accountId = contactList[i].AccountId;
				Id contactId = contactList[i].Id;

				Date currentDT = Date.today().addDays(-1);
				Date contractEndDate = currentDT.addDays(3);

				Opportunity oppty = new Opportunity(
										RecordTypeId = opptyRecordTypeId,
										accountId = accountId,
										Industry_Category__c = 'Ad Network',
										Budget_Source__c = 'TV',
										Radio_Type__c = 'Local',
										Initiative_Location__c = 'US',
							            closeDate = System.today(),
							            name = 'autoEmailOpptyTestName'+i,
							            stageName = 'Closed Won',
							            Primary_Billing_Contact__c = contactId,
							            Primary_Contact__c = contactId,
							            ContractStartDate__c = currentDT,
										ContractEndDate__c = contractEndDate,
										Preferred_Invoicing_Method__c = 'Email',
										Bill_on_Broadcast_Calendar2__c = 'No',
										Lead_Campaign_Manager__c = UserInfo.getUserId());
				opportunityList.add(oppty);
			}

			insert opportunityList;
			System.debug('Opportunities created in test data');
		//}
	}
	
	@isTest static void emailBatchTest() {
		
		Test.startTest();			
			AutoEmailSendBatch obj = new AutoEmailSendBatch();
			System.debug('before ExecuteBatch');
			DataBase.executeBatch(obj);
		Test.stopTest();
		String taskRecordTypeID = '';
		taskRecordTypeID = AutoEmailSendUtil.getRecordTypeId('Task',AutoEmailSendUtil.TASK_IS_RT);
		list<Task> taskList = [SELECT Id FROM Task WHERE RecordTypeId =: taskRecordTypeId];
		System.assertEquals(taskList.size(),2);

	}
	@isTest static void emailSchedulerTest() {

		String CRON_EXP = '0 0 0 15 3 ? 2022';
		Test.startTest();
			// Schedule the test job
			String jobId = System.schedule('TestAutoEmailScheduler',CRON_EXP,new AutoEmailSendScheduler());
		Test.stopTest();
		// Get the information from the CronTrigger API object
		CronTrigger ct = [SELECT Id,CronExpression,
								TimesTriggered,NextFireTime 
							FROM CronTrigger WHERE id = :jobId];
		// Verify the expressions are the same
		System.assertEquals(CRON_EXP,ct.CronExpression);
		// Verify the job has not run
		System.assertEquals(0, ct.TimesTriggered);
		// Verify the next time the job will run
		System.assertEquals('2022-03-15 00:00:00',String.valueOf(ct.NextFireTime));
	}
}