/**
* ─────────────────────────────────────────────────────────────────────────────────────────────────┐
* AutoEmailSendHelper: Helper class for Auto email send process.
* ──────────────────────────────────────────────────────────────────────────────────────────────────
* @author         Jaya Alaparthi
* @maintainedBy   Jaya Alaparthi
* @version        1.0
* @created        2017-10-05
* @modified       
* @systemLayer    Helper
* @see            ????
* @see            ????
* ──────────────────────────────────────────────────────────────────────────────────────────────────
* @changes
* v1.1          Jaya Alaparthi
* 2017-10-30    90% email should be sent 
* 				to owner, campaing owner and to primary contact.
* 				Campaign end email should not be sent.
* ──────────────────────────────────────────────────────────────────────────────────────────────────
* @changes
* v1.2            Jaya Alaparthi
* 2017-11-28      send email to renewal rep.
*				  Only Mid campaign and 90% of campaign should be sent.
* ──────────────────────────────────────────────────────────────────────────────────────────────────
* @changes
* v1.3            Jaya Alaparthi
* 2017-12-06      changing Task Subject as this needs to reflect understable subject for user.
* ─────────────────────────────────────────────────────────────────────────────────────────────────┘
*/
public with sharing class AutoEmailSendHelper {
	

	//───────────────────────────────────────────────────────────────────────────┐
	// param		type					descreption
	// newOppty		list of opportunities 	list if provided from trigger.new
	// oldOpptyMap	map of oppty 			map is provied from trigger.old
	// triggerFlag  boolean					helps to chek if this method is 
	//										invoked by a trigger
	// ──────────────────────────────────────────────────────────────────────────
	// Description: createEmailActivites	validates Opportunities, 
	//										Gathers existing Tasks, based on 
	//										retrived results either updates tasks 
	//										or creats tasks
	//			
	//───────────────────────────────────────────────────────────────────────────┘
	public static String createEmailActivites(list<Opportunity> newOppty,
									map<Id,Opportunity> oldOpptyMap,
									Boolean isInsertFlag) {

		list<Id> validOpptyIds = new list<Id>();
		list<Opportunity> validOpptyList = new list<Opportunity>();
		map<String,Task> formatedTaskMap = new map<String,Task>();
		list<Task> tasksBuilt = new list<Task>();
		String returnStr = '';

		
		System.debug('in createEmailActivites:'+newOppty);
		validOpptyIds = validateOpportunities(newOppty,oldOpptyMap,isInsertFlag);
		System.debug('in createEmailActivites validOpptyIds:'+validOpptyIds);
		// && AutoEmailSendUtil.AutoExecuted
		if(!validOpptyIds.isEmpty()) {

			String taskRecordTypeID = '';
			taskRecordTypeID = AutoEmailSendUtil.getRecordTypeId('Task',AutoEmailSendUtil.TASK_IS_RT);
			System.debug('in if statement');
			validOpptyList = AutoEmailSendUtil.getOpportunityDetails(validOpptyIds);
			System.debug('validOpptyList:'+validOpptyList);
			formatedTaskMap = validateExistingActivity(validOpptyList,validOpptyIds,taskRecordTypeID);
			tasksBuilt = generateTask(formatedTaskMap,validOpptyList,taskRecordTypeID);	
			System.debug('in createEmailActivites tasksBuilt:'+tasksBuilt);
			if(!tasksBuilt.isEmpty()) {

				try{
					//AutoEmailSendUtil.AutoExecuted = false;
					upsert tasksBuilt;
				} catch(Exception ex) {

					returnStr = ex.getMessage();
				} 
			}
		}
		return returnStr;
	}

	//───────────────────────────────────────────────────────────────────────────┐
	// param		type					descreption
	// newOppty		list of opportunities 	list if provided from trigger.new
	// oldOpptyMap	map of oppty 			map is provied from trigger.old
	// ──────────────────────────────────────────────────────────────────────────
	// Description: validateOpportunities validates opportunity checking for required 
	//				fields for creation of activity	
	//───────────────────────────────────────────────────────────────────────────┘	
	private static list<Id> validateOpportunities(list<Opportunity> newOppty,
									map<Id,Opportunity> oldOpptyMap,Boolean isInsertFlag) {

		list<Id> validatedOpptys = new list<Id>();

		Id opptyInsideSalesRT = AutoEmailSendUtil.getRecordTypeId('Opportunity',
														AutoEmailSendUtil.INSIDE_SALES_RT);
		for(Opportunity oppty: newOppty) {

				Opportunity oldOppty = new Opportunity();
				System.debug('recordType:'+oppty.RecordTypeId+'ISRT:'+opptyInsideSalesRT);

				if(oppty.RecordTypeId == opptyInsideSalesRT) {

					if(!isInsertFlag 
						&& !oldOpptyMap.isEmpty() 
						&& oldOpptyMap.containsKey(oppty.Id)) {

						oldOppty = oldOpptyMap.get(oppty.Id);
						if(oppty.StageName == 'Closed Won'
							&& oppty.ContractStartDate__c != null 
							&& oppty.Primary_Contact__c != null
							&& oppty.Lead_Campaign_Manager__c != null
							&& (oldOppty.ContractStartDate__c != oppty.ContractStartDate__c
								|| oldOppty.ContractEndDate__c != oppty.ContractEndDate__c
								|| oldOppty.Primary_Contact__c != oppty.Primary_Contact__c
								|| oldOppty.Lead_Campaign_Manager__c != oppty.Lead_Campaign_Manager__c
								|| oldOppty.StageName != oppty.StageName)) {
							
							validatedOpptys.add(oppty.Id);
						}
					} else if(isInsertFlag && oppty.ContractStartDate__c != null 
								&& oppty.ContractEndDate__c != null 
								&& oppty.Primary_Contact__c != null
								&& oppty.Lead_Campaign_Manager__c != null
								&& oppty.StageName == 'Closed Won') {

							validatedOpptys.add(oppty.Id);
					}
				}
			}
		return validatedOpptys;
	}

	//───────────────────────────────────────────────────────────────────────────┐
	// param		type					descreption
	// opptyIds		list of opptyIds 		list is validated
	// ──────────────────────────────────────────────────────────────────────────
	// Description: for every valid opportuity checks to see if there is any 
	//				already recorded activity. formats tasks as per the key used in map.
	//				for a give oppty| activity subject | Activity Date | email to__c
	//				 will be unique
	//───────────────────────────────────────────────────────────────────────────┘	
	private static map<String,Task> validateExistingActivity(list<Opportunity> opptyList,
																list<Id> opptyIds,
																Id taskRecordTypeID) {

		list<Task> existingTaskList = new list<Task>();
		map<String,Task> formatedTasks = new map<String,Task>();

		existingTaskList = AutoEmailSendUtil.getRecordedActivities(opptyIds,taskRecordTypeID);
		System.debug('existingTaskList:'+existingTaskList);
		formatedTasks = formatTasks(existingTaskList);
		system.debug('in validateExistingActivity formatedTasks:'+formatedTasks);
		return formatedTasks;
	}

	//───────────────────────────────────────────────────────────────────────────┐
	// param				type					
	// existingTaskList		list of tasks that are already existing
	// ──────────────────────────────────────────────────────────────────────────
	// Description: formats tasks as per the key used in map.
	//				for a give oppty| activity subject 
	//				 will be unique
	//───────────────────────────────────────────────────────────────────────────┘
	private static map<String,Task> formatTasks(list<Task> existingTaskList) {

		map<String,Task> formatedTasks = new map<String,Task>();

		for(Task tRec : existingTaskList) {

			String taskKey;
			//String taskFlag;
			//v1.3
			if(!String.isBlank(tRec.WhatId) 
				&& !String.isBlank(tRec.AutoEmailSubject__c)
				&& !String.isBlank(tRec.Subject) 
				&& tRec.ActivityDate != null) {

				/*taskKey = tRec.WhatId+'|'+tRec.Subject+'|'
							+tRec.ActivityDate+'|'+tRec.EmailTo__c;*/
				// v1.3
				//taskKey = tRec.WhatId+'|'+tRec.Subject;
				taskKey = tRec.WhatId+'|'+tRec.AutoEmailSubject__c;
				//taskFlag = tRec.Status;
				formatedTasks.put(taskKey,tRec);
			}			
		}
		return formatedTasks;
	}

	//───────────────────────────────────────────────────────────────────────────┐
	// param				type					
	// formatedTasksMap		map of existing tasks
	// opptyList			valid opportunity list
	// ──────────────────────────────────────────────────────────────────────────
	// Description: generates a map of tasks wit unique key from valida opportuity
	//				if the key is not found in formatedTasksMap
	//				 oppty| activity subject
	//				 will be unique
	//───────────────────────────────────────────────────────────────────────────┘
	private static list<Task> generateTask(map<String,Task> formatedTasksMap,
											list<Opportunity> opptyList, 
											Id taskRecordTypeID) {

		list<Task> tasksBuilt = new list<Task>();
		map<String,EmailTemplate> tskSubToEmailTemplateMap = new map<String,EmailTemplate>();
		//v1.3
		//get email template map for template subject in build task
		tskSubToEmailTemplateMap = AutoEmailSendUtil.getEmailTemplateMap();
		System.debug('opptyList:'+opptyList);
		for(Opportunity oppty: opptyList) {

			String campStartKey, campMiddleKey, camp90Key, campEndKey;
			Date contractMiddleDate;
			Date contract90Date;
			list<String> startMiddleEndKeys = new list<String>();
			list<String> nintyKey = new list<String>();
			String all3Emails = '';
			list<Task> tempTaskList = new list<Task>();
			Id primaryContactId;
			
			if(oppty.ContractStartDate__c != null && oppty.ContractEndDate__c != null) {

				Date startDate = oppty.ContractStartDate__c;
				Date endDate = oppty.ContractEndDate__c;
				Integer daysDiff = startDate.daysBetween(endDate);
				contractMiddleDate = startDate.addDays(Integer.valueOf(daysDiff/2));
				contract90Date = startDate.addDays(Integer.valueOf(daysDiff*0.9));
			}
			// camp start
			// v1.2 do not send email on campaign start.
			/*if(oppty.ContractStartDate__c != null) {

				campStartKey = oppty.id+'|'+AutoEmailSendUtil.START_CAMP
								+'|'+oppty.ContractStartDate__c;
				startMiddleEndKeys.add(campStartKey);
			}*/
			if(contractMiddleDate != null) {

				campMiddleKey = oppty.id+'|'+AutoEmailSendUtil.MID_CAMP
								+'|'+contractMiddleDate;
				startMiddleEndKeys.add(campMiddleKey);
			}
			if(contract90Date != null) {

				camp90Key = oppty.id+'|'+AutoEmailSendUtil.AT90_CAMP
								+'|'+contract90Date;
				// ownerClientServEmails 
				// v1.1
				//nintyKey.add(camp90Key);
				startMiddleEndKeys.add(camp90Key);
			}
			// v1.1
			/*if(oppty.ContractEndDate__c != null) {

				campEndKey = oppty.id+'|'+AutoEmailSendUtil.END_CAMP
								+'|'+oppty.ContractEndDate__c;
				startMiddleEndKeys.add(campEndKey);
			}*/

			// email list
			//v1.1
			//──────────────────────────────────────────────────────────────────────────
			/*if(!String.isBlank(oppty.Owner.Email)) {

				all3Emails = oppty.Owner.Email +';';
			}*/ 
			// end of v1.1
			//──────────────────────────────────────────────────────────────────────────
				
			if(oppty.Primary_Contact__c != null
				&& !String.isBlank(oppty.Primary_Contact__r.Email)) {

				all3Emails = oppty.Primary_Contact__r.Email +';';
				primaryContactId = oppty.Primary_Contact__c;
			}
			if(oppty.Lead_Campaign_Manager__c != null 
				&& !String.isBlank(oppty.Lead_Campaign_Manager__r.Email)) { 
				// have to add client services email
				all3Emails += oppty.Lead_Campaign_Manager__r.Email+';';
				//ownerClientServEmails += oppty.Lead_Campaign_Manager__r.Email+';';
			}
			if(oppty.Account.RenewalRep__c != null) {

				System.debug('adding renewal rep:'+oppty.Account.RenewalRep__c);
				all3Emails += oppty.Account.RenewalRep__r.Email+';';
			}
			// email s to Oppty Owner, Primary contact, Client Services
			// sent only for start, middle,end
			//,old3EmailStr
			tempTaskList = buildTask(all3Emails,startMiddleEndKeys,
								formatedTasksMap,oppty,taskRecordTypeID,
								tskSubToEmailTemplateMap);
			tasksBuilt.addAll(tempTaskList);
			System.debug('ALL 3 tasksBuilt:'+tasksBuilt);
			// email s to Oppty Owner, Client Services 
			// sent in case of 90% campaign
			//,oldOwnerClientServEmails

			//v1.1
			/*tempTaskList = buildTask(ownerClientServEmails,nintyKey,
									formatedTasksMap,oppty,taskRecordTypeID);

			System.debug('tempTaskList:'+tempTaskList);
			tasksBuilt.addAll(tempTaskList);
			System.debug('90 only tasksBuilt:'+tasksBuilt);*/
		} // end of for
		System.debug('tasksBuilt:'+tasksBuilt);
		return tasksBuilt;
	}
	//, String oldEmailStr
	//───────────────────────────────────────────────────────────────────────────┐
	// param				type & Description					
	// emailStr				String 	list if provided
	// keysList				list of string 			
	// formatedTasksMap 	map of string task		
	// oppty				Opportuity
	// taskRecordTypeID		record type Id
	// ──────────────────────────────────────────────────────────────────────────
	// Description: buildTask creates list of task each for wwelcome, 50 % and 90%	
	//───────────────────────────────────────────────────────────────────────────┘
	private static list<Task> buildTask(String emailStr, list<String> keysList,
									map<String,Task> formatedTasksMap,
									Opportunity oppty, Id taskRecordTypeID,
									map<String,EmailTemplate> tskSubToEmailTemplateMap) {

		list<Task> tasksBuilt = new list<Task>();

		if(!String.isBlank(emailStr)) {

			System.debug('formatedTasksMap:'+formatedTasksMap);
			System.debug('keysList:'+keysList+'\n emailStr:'+emailStr);

			// keysList : OppotyId|TaskSubject|activityDate
			for(String keyStr : keysList) {

				// get date from key str
				String currentDtStr = keyStr.substring(keyStr.lastIndexOf('|')+1,
														keyStr.length());
				System.debug('currentDtStr:'+currentDtStr);
				Date currentDate = Date.valueOf(currentDtStr);

				// get subject from keyStr
				String subStr = keyStr.substring(keyStr.indexOf('|')+1,
												keyStr.lastIndexOf('|'));
				System.debug('subStr:'+subStr);
				//v1.3
				// get email template subject.
				String emailTemplateSubject = tskSubToEmailTemplateMap.containsKey(subStr) 
												? tskSubToEmailTemplateMap.get(subStr).Subject
												:subStr;
				Task taskRec = new Task();
				
				String formatedKey = keyStr.substring(0,keyStr.lastIndexOf('|'));
				//oldKey += '|'+oldEmailStr;										
				System.debug('formatedKey:'+formatedKey);
				if(!formatedTasksMap.isEmpty() 
						&& !String.isBlank(formatedKey)
						&& formatedTasksMap.containsKey(formatedKey)) {

						Task tempTaskRec = formatedTasksMap.get(formatedKey);
						System.debug('Old record found:'+tempTaskRec);
						// modify the old task to new task
						
						//taskRec.EmailTo__c = oldEmailStr;
						//taskRec.PrimaryContactId__c = oppty.Primary_Contact__c;
						taskRec.RecordTypeId = taskRecordTypeID;
						taskRec.Id = tempTaskRec.Id;
						taskRec.EmailTo__c = emailStr;
						taskRec.PrimaryContact__c = oppty.Primary_Contact__c;
						taskRec.AutoEmailAccount__c = oppty.AccountId;
						taskRec.ActivityDate = currentDate;
						if(oppty.Account.RenewalRep__c != null) {
							taskRec.AutoEmailFrom__c = oppty.Account.RenewalRep__r.Email+';';
							taskRec.SendersName__c = oppty.Account.RenewalRep__r.Name;
						}
						tasksBuilt.add(taskRec);
					} else {

						System.debug('no Old record found:');
						taskRec = new Task(
			    				ActivityDate = currentDate,
			    				RecordTypeId = taskRecordTypeID,
			    				Subject=emailTemplateSubject,
			    				AutoEmailSubject__c = subStr,
			    				WhatId = oppty.Id,
			    				OwnerId = UserInfo.getUserId(),
			    				PrimaryContact__c = oppty.Primary_Contact__c,
			    				AutoEmailAccount__c = oppty.AccountId,
			    				Status = AutoEmailSendUtil.TASK_PENDING,
			    				EmailTo__c = emailStr);
						if(oppty.Account.RenewalRep__c != null) {
			    			taskRec.AutoEmailFrom__c = oppty.Account.RenewalRep__r.Email+';';
			    			taskRec.SendersName__c = oppty.Account.RenewalRep__r.Name;
						}
						tasksBuilt.add(taskRec);
					}
			}
		}
		System.debug('tasksBuilt:'+tasksBuilt);
		return tasksBuilt;
	}

	//Batch job
	// validate Activites from Job
	//───────────────────────────────────────────────────────────────────────────┐
	// param				type & Description					
	// taskList				list of tasks retrived in batch query
	// ──────────────────────────────────────────────────────────────────────────
	// Description: validateJobActivity validates list of task as per the welcome 
	//				email restriction that only one email in last one year	
	//───────────────────────────────────────────────────────────────────────────┘
    public static map<String,Task> validateJobActivity(list<Task> taskList) {

    	set<Id> primaryContactIds = new set<Id>();
    	list<Task> afterCampStartFilterList = new list<Task>();
    	map<String,Task> finalTaskFilterMap= new map<String,Task>();

    	// get primary contact Ids from each task
    	set<Id> accountIds = new set<Id>();
    	/* v1.1 
    	//──────────────────────────────────────────────────────────────────────────
    	for(Task taskRec : taskList) {

    		String [] emailList;
    		if(taskRec.Subject == AutoEmailSendUtil.START_CAMP && taskRec.EmailTo__c != null) {

    			primaryContactIds.add(taskRec.PrimaryContact__c);
    			accountIds.add(taskRec.AutoEmailAccount__c);
    		}
    	}
    	//──────────────────────────────────────────────────────────────────────────
    	// v1.1 end

    	//primaryContactIds = getPrimaryContact(taskList);
    	// validate as per DisableAutoEmailSendOut__c flag on Accout 

    	// validate task for only welcome email for a give account 
    	// and primary contact in the last one year.
    	/* v1.1 
    	//──────────────────────────────────────────────────────────────────────────
    	// As per the new change no longer the welcome email restriction applies. Taking it down
		// as the welcome email filteration is no longer in use 
		// pass al the tasks retrived to getTaskPerAccount and populate finalTaskFilterMap.

    	afterCampStartFilterList = AutoEmailSendUtil.campStartValidate(primaryContactIds,accountIds,taskList); 
    	system.debug('afterCampStartFilterList:'+afterCampStartFilterList);
    	finalTaskFilterMap = AutoEmailSendUtil.getTaskPerAccount(afterCampStartFilterList);*/
    	//──────────────────────────────────────────────────────────────────────────
    	// v1.1 end
    	finalTaskFilterMap = AutoEmailSendUtil.getTaskPerAccount(taskList);
    	//  build emails service 	
    	system.debug('finalTaskFilterMap:'+finalTaskFilterMap);
    	return finalTaskFilterMap;
    }
    // This method is called in Account Trigger on Renewal Rep  change 
    //───────────────────────────────────────────────────────────────────────────┐
    // param            type
    // accountsList		List<Accounts>
    // oldAccountsMap	Map<Id,Account>
    // ──────────────────────────────────────────────────────────────────────────
    // Description: updateISTaskEmails
    //              retrive existing task related to Account and append renewal 
    //				rep email to task. Retrive only tasks taht are in pending 
    //				status and IS recor type
    //───────────────────────────────────────────────────────────────────────────┘
    public static void updateISTaskEmails(list<Account> accountsList,
    									map<Id,Account> oldAccountsMap) {

    	//set<Account> accountsSet = new set<Account>();
    	set<Id> renewalRepIds = new set<Id>();
    	map<Id,Id> acctIdToRepMap = new map<Id,Id>();
    	map<Id,User> renewalRepIdToUser = new map<Id,User>();
    	list<Task> tasksRetrivedList = new list<Task>();
    	list<Task> taskUpdateList = new list<Task>();
    	set<Id> opptyIds = new set<Id>();
    	map<Id,Id> newUserToOldUserMap = new map<Id,Id>();
    	System.debug('in updateISTaskEmails:'+accountsList);
    	for(Account acct: accountsList) {

    		Account oldAccount = new Account();
    		if(oldAccountsMap.containsKey(acct.Id)) {

    			oldAccount = oldAccountsMap.get(acct.Id);	
    			if(acct.RenewalRep__c != null 
    				&& oldAccount.RenewalRep__c != acct.RenewalRep__c) {

    				//accountsSet.add(acct);
    				renewalRepIds.add(acct.RenewalRep__c);
    				if(oldAccount.RenewalRep__c!= null) {
    					renewalRepIds.add(oldAccount.RenewalRep__c);
    					newUserToOldUserMap.put(acct.RenewalRep__c,oldAccount.RenewalRep__c);
    				}
    				acctIdToRepMap.put(acct.Id,acct.RenewalRep__c);
    			}
    		}
    	}
    	if(!renewalRepIds.isEmpty()) {

    		// retrive only non closed oppty relaed to acounts
    		opptyIds = AutoEmailSendUtil.getClosedOpptyIds(acctIdToRepMap.keySet());

    		if(!opptyIds.isEmpty()) {
	    		
    		
	    		// retrive existing task related to this account
	    		tasksRetrivedList = AutoEmailSendUtil.getExistingTasksByAcctId(acctIdToRepMap.keySet(),
	    																	opptyIds);
	    		// append email to retrived tasks and update tasks
	    		if(!tasksRetrivedList.isEmpty()) {

	    			// retrive renewal rep email adress
		    		for(User renewalRep: [SELECT Id,Email,Name FROM User 
		    								WHERE Id IN: renewalRepIds
		    								AND Email != null]) {
		    			renewalRepIdToUser.put(renewalRep.Id,renewalRep);
		    		}
	    			
	    			for(Task taskRec : tasksRetrivedList) {

	    				if(!acctIdToRepMap.isEmpty()
	    					&& acctIdToRepMap.containsKey(taskRec.AutoEmailAccount__c)) {

	    					Id newRenewalRepId, oldRenewalRepId;
	    					User newRepUser = new User();
	    					User oldRepUser = new User();
	    					// get new and old renewal rep ids from newUserToOldUserMap
	    					newRenewalRepId = acctIdToRepMap.get(taskRec.AutoEmailAccount__c);
	    					System.debug('newRenewalRepId:'+newRenewalRepId);
	    					if(!newUserToOldUserMap.isEmpty() 
	    								&& newUserToOldUserMap.containsKey(newRenewalRepId)) {
	    						oldRenewalRepId = newUserToOldUserMap.get(newRenewalRepId);
	    						oldRepUser = renewalRepIdToUser.get(oldRenewalRepId);
	    					}
	    					// get old reps email and replace with new reps email
	    					if(!renewalRepIdToUser.isEmpty() 
	    						&& renewalRepIdToUser.containsKey(newRenewalRepId)) {

	    						newRepUser = renewalRepIdToUser.get(newRenewalRepId);
	    						System.debug('newRepUser.Email:'+newRepUser+'oldRepUser.Email:'+oldRepUser);
	    						if(oldRepUser.Id != null 
	    								&& !String.isBlank(oldRepUser.Email) 
	    								&& newRepUser.Id != null) {

	    							taskRec.EmailTo__c = taskRec.EmailTo__c.replace(oldRepUser.Email,
	    																			newRepUser.Email);
	    							taskRec.AutoEmailFrom__c = newRepUser.Email;
	    							taskRec.SendersName__c = newRepUser.Name;
	    						} else {
	    							taskRec.EmailTo__c += newRepUser.Email;
	    							taskRec.AutoEmailFrom__c = newRepUser.Email;
	    							taskRec.SendersName__c = newRepUser.Name;
	    						}
	    						System.debug('taskRec.EmailTo__c:'+taskRec.EmailTo__c);
	    						taskUpdateList.add(taskRec);
	    					}
	    				}
	    			} // end of for
	    			if(!taskUpdateList.isEmpty()) {

	    				update taskUpdateList;
	    			}
	    		}
	    	}// end of OpptyIds not empty If
    	}// end of renewal rep not empty If
    	
    }
}