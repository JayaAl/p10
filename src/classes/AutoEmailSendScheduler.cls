/**
* ─────────────────────────────────────────────────────────────────────────────────────────────────┐
* This class is  .
* ──────────────────────────────────────────────────────────────────────────────────────────────────
* @author         Jaya Alaparthi
* @maintainedBy   Jaya Alaparthi
* @version        1.0
* @created        2017-10-05
* @modified       
* @systemLayer    Util
* @see            ????
* @see            ????
* ──────────────────────────────────────────────────────────────────────────────────────────────────
* @changes
* v1.1            Jaya Alaparthi
* 2017-10-05      
* ──────────────────────────────────────────────────────────────────────────────────────────────────
* @changes
* v1.2            Jaya Alaparthi
* 2017-06-06      
* ─────────────────────────────────────────────────────────────────────────────────────────────────┘
*/
global class AutoEmailSendScheduler implements Schedulable {
	global void execute(SchedulableContext sc) {
		
		 AutoEmailSendBatch autoEmailBatch = new AutoEmailSendBatch(); 
		 System.debug('in auto email scheduler');
         database.executebatch(autoEmailBatch);
         System.debug('after batch call');
	}
}