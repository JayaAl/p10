@isTest
private class AutoEmailSendTest {
	
	@testSetup static void opportunityTestDataSetup() {
		// Implement test code
		list<Account> accountsList = new list<Account>();
		list<Contact> contactsList = new list<Contact>();
		// create Account default
		for(Integer i=0; i<100; i++) {

			Account acct = new Account();
			acct.name = 'AutoEmailTestN'+i;
            acct.Website = 'AutoEmailWebTest'+i+'.com';
            acct.BillingStreet = 'AutoEmailWebTest'+i;
            acct.BillingCity = 'AutoEmailWebTest'+i;
            acct.BillingState = 'CA';
            acct.BillingPostalCode = '45897643';
            acct.c2g__CODAAccountTradingCurrency__c = 'USD';
			accountsList.add(acct);
		}
		insert accountsList;
		// create Contact
		for(Integer i=0;i<100;i++) {

			Contact contact = new Contact();
			contact.lastName = 'autoLN'+i;
			contact.accountId = accountsList[i].Id;
			contact.MailingStreet = 'MyStreet1';
			contact.MailingCountry = 'USA';
			contact.MailingState = 'CA';
			contact.MailingCity = 'Fremont';
			contact.Email = 'autoEmail'+i+'@mydomain.com';
			contact.Title = 'autoEmail'+i;
			contact.firstname = 'autoEmailFN'+i;
			contact.Industry_Category__c = 'Ad Network';
			contact.GCLID__c = 'autoEmailTEst';
			contactsList.add(contact);
		}
		insert contactsList;
	}
	
	@isTest static void opportunityInsertTest() {
		
		// create opportunities
		list<Account> accountList = new list<Account>();
		list<Contact> contactList = new list<Contact>();
		list<Opportunity> opportunityList = new list<Opportunity>();
		list<Task> taskList = new list<Task>();

		accountList = [SELECT Id FROM Account];
		contactList = [SELECT Id,AccountId FROM Contact];
		Id opptyRecordTypeId = AutoEmailSendUtil.getRecordTypeId('Opportunity',
											AutoEmailSendUtil.INSIDE_SALES_RT);

		for(Integer i=0;i<10;i++) {

			Id accountId = contactList[i].AccountId;
			Id contactId = contactList[i].Id;

			Date currentDT = Date.today();
			Date contractEndDate = currentDT.addDays(5);

			Opportunity oppty = new Opportunity(
									RecordTypeId = opptyRecordTypeId,
									accountId = accountId,
									Industry_Category__c = 'Ad Network',
									Budget_Source__c = 'TV',
									Radio_Type__c = 'Local',
									Initiative_Location__c = 'US',
						            closeDate = System.today(),
						            name = 'autoEmailOpptyTestName'+i,
						            stageName = 'Closed Won',
						            Primary_Billing_Contact__c = contactId,
						            Primary_Contact__c = contactId,
						            ContractStartDate__c = currentDT,
									ContractEndDate__c = contractEndDate,
									Preferred_Invoicing_Method__c = 'Email',
									Bill_on_Broadcast_Calendar2__c = 'No',
									Lead_Campaign_Manager__c = UserInfo.getUserId());
			opportunityList.add(oppty);
		}
		Profile sysAdminProfile = [SELECT Id 
									FROM Profile 
									WHERE name = 'System Administrator' 
									LIMIT 1];
		User autoEmailTest = new User(lastName = 'lastNameAutoEmail',
					            userName = 'autoEmailTest@email.com',
					            profileId = sysAdminProfile.Id,
					            alias = 'aET',
					            email = 'autoEmailTest@email.com',
					            emailEncodingKey = 'ISO-8859-1',
					            languageLocaleKey = 'en_US',
					            localeSidKey = 'en_US',
					            timeZoneSidKey = 'America/Los_Angeles',
					            DisableAutoEmailSendOut__c = false);
        System.runAs(autoEmailTest) {
			Test.startTest();
				insert opportunityList;
			Test.stopTest();
			// validate task creation
			list<Id> opptyListIds = new list<Id>();
			map<Id,Integer> taskPerOpptyMap = new map<Id,Integer>();

			for(Opportunity oppty : opportunityList) {
				opptyListIds.add(oppty.Id);
			}
			for(Task taskObj : [SELECT Id,Subject,WhatId,
										AutoEmailSubject__c
								FROM Task 
								WHERE AutoEmailSubject__c LIKE 'Campaign%' 
								AND WhatId != null]) {
				Integer i=0;
				if(taskPerOpptyMap.containsKey(taskObj.WhatId)) {

					i = taskPerOpptyMap.get(taskObj.WhatId);
				}
				i += 1;
				taskPerOpptyMap.put(taskObj.WhatId,i);
			}
			for(Opportunity oppty: opportunityList) {

				if(taskPerOpptyMap.containsKey(oppty.Id)) {

					System.assertEquals(taskPerOpptyMap.get(oppty.Id),2);
				}
			}
		}
	}

	@isTest static void opportunityUpdateTest() {
		
		// create opportunities
		list<Account> accountList = new list<Account>();
		list<Contact> contactList = new list<Contact>();
		list<Opportunity> opportunityList = new list<Opportunity>();
		list<Task> taskList = new list<Task>();

		list<Id> opptyListIds = new list<Id>();
		map<Id,Integer> taskPerOpptyMap = new map<Id,Integer>();
		list<Opportunity> updateOpptyList = new list<Opportunity>();

		accountList = [SELECT Id FROM Account];
		contactList = [SELECT Id,AccountId FROM Contact];
		Id opptyRecordTypeId = AutoEmailSendUtil.getRecordTypeId('Opportunity',
											AutoEmailSendUtil.INSIDE_SALES_RT);

		Date currentDT = Date.today();
		Date contractEndDate = currentDT.addDays(5);
		Date contractNewStartDate = currentDT.addDays(1);
		Date contractNewEndDate = contractEndDate.addDays(5);

		Integer daysDiff = contractNewStartDate.daysBetween(contractNewEndDate);
		Date contractHalf = contractNewStartDate.addDays(Integer.valueOf(daysDiff/2));
		Date contract90 = contractNewStartDate.addDays(Integer.valueOf(daysDiff*0.9));

		Profile sysAdminProfile = [SELECT Id 
									FROM Profile 
									WHERE name = 'System Administrator' 
									LIMIT 1];
		User autoEmailTest = new User(lastName = 'lastNameAutoEmail',
					            userName = 'autoEmailTest@email.com',
					            profileId = sysAdminProfile.Id,
					            alias = 'aET',
					            email = 'autoEmailTest@email.com',
					            emailEncodingKey = 'ISO-8859-1',
					            languageLocaleKey = 'en_US',
					            localeSidKey = 'en_US',
					            timeZoneSidKey = 'America/Los_Angeles',
					            DisableAutoEmailSendOut__c = false
						      );
        System.runAs(autoEmailTest) {

			for(Integer i=0;i<10;i++) {

				Id accountId = contactList[i].AccountId;
				Id contactId = contactList[i].Id;

				Opportunity oppty = new Opportunity(
										RecordTypeId = opptyRecordTypeId,
										accountId = accountId,
										Industry_Category__c = 'Ad Network',
										Budget_Source__c = 'TV',
										Radio_Type__c = 'Local',
										Initiative_Location__c = 'US',
							            closeDate = System.today(),
							            name = 'autoEmailOpptyTestName'+i,
							            stageName = 'Closed Won',
							            Primary_Billing_Contact__c = contactId,
							            Primary_Contact__c = contactId,
							            ContractStartDate__c = currentDT,
										ContractEndDate__c = contractEndDate,
										Preferred_Invoicing_Method__c = 'Email',
										Bill_on_Broadcast_Calendar2__c = 'No',
										Lead_Campaign_Manager__c = UserInfo.getUserId());
				opportunityList.add(oppty);
			}
			insert opportunityList;

			Test.startTest();

				for(Opportunity oppty : opportunityList) {
				
					opptyListIds.add(oppty.Id);
				}
				for(Opportunity oppty: [SELECT Id,ContractStartDate__c,
												ContractEndDate__c
										FROM Opportunity
										WHERE Id IN: opptyListIds]) {
					oppty.ContractStartDate__c = contractNewStartDate;
					oppty.ContractEndDate__c = contractNewEndDate;
					updateOpptyList.add(oppty);
				}
				update updateOpptyList;
			Test.stopTest();
			// validate task update
			for(Task taskObj : [SELECT Id,Subject,WhatId,AutoEmailSubject__c,
									ActivityDate 
								FROM Task 
								WHERE AutoEmailSubject__c LIKE 'Campaign%' 
								AND WhatId != null]) {
				Integer i=0;
				if(taskPerOpptyMap.containsKey(taskObj.WhatId)) {

					i = taskPerOpptyMap.get(taskObj.WhatId);
				}
				i += 1;
				taskPerOpptyMap.put(taskObj.WhatId,i);
				
				if(taskObj.AutoEmailSubject__c == AutoEmailSendUtil.MID_CAMP) {
					System.assertEquals(taskObj.ActivityDate,contractHalf);
				}
				if(taskObj.AutoEmailSubject__c == AutoEmailSendUtil.AT90_CAMP) {
					System.assertEquals(taskObj.ActivityDate,contract90);
				}
				
			}
			for(Opportunity oppty: opportunityList) {

				if(taskPerOpptyMap.containsKey(oppty.Id)) {

					System.assertEquals(taskPerOpptyMap.get(oppty.Id),2);
				}
			}
		} // end of system run
	} // end of test method
	
	@isTest static void accountRenewalRepUpdateTest() {

		// create opportunities
		list<Account> accountList = new list<Account>();
		list<Contact> contactList = new list<Contact>();
		list<Opportunity> opportunityList = new list<Opportunity>();
		list<Task> taskList = new list<Task>();

		accountList = [SELECT Id,RenewalRep__c FROM Account];
		contactList = [SELECT Id,AccountId FROM Contact];
		Id opptyRecordTypeId = AutoEmailSendUtil.getRecordTypeId('Opportunity',
											AutoEmailSendUtil.INSIDE_SALES_RT);

		for(Integer i=0;i<10;i++) {

			Id accountId = contactList[i].AccountId;
			Id contactId = contactList[i].Id;

			Date currentDT = Date.today();
			Date contractEndDate = currentDT.addDays(5);

			Opportunity oppty = new Opportunity(
									RecordTypeId = opptyRecordTypeId,
									accountId = accountId,
									Industry_Category__c = 'Ad Network',
									Budget_Source__c = 'TV',
									Radio_Type__c = 'Local',
									Initiative_Location__c = 'US',
						            closeDate = System.today(),
						            name = 'autoEmailOpptyTestName'+i,
						            stageName = 'Closed Won',
						            Primary_Billing_Contact__c = contactId,
						            Primary_Contact__c = contactId,
						            ContractStartDate__c = currentDT,
									ContractEndDate__c = contractEndDate,
									Preferred_Invoicing_Method__c = 'Email',
									Bill_on_Broadcast_Calendar2__c = 'No',
									Lead_Campaign_Manager__c = UserInfo.getUserId());
			opportunityList.add(oppty);
		}
		Profile sysAdminProfile = [SELECT Id 
									FROM Profile 
									WHERE name = 'System Administrator' 
									LIMIT 1];
		User autoEmailTest = new User(lastName = 'lastNameAutoEmail',
					            userName = 'autoEmailTest@email.com',
					            profileId = sysAdminProfile.Id,
					            alias = 'aET',
					            email = 'autoEmailTest@email.com',
					            emailEncodingKey = 'ISO-8859-1',
					            languageLocaleKey = 'en_US',
					            localeSidKey = 'en_US',
					            timeZoneSidKey = 'America/Los_Angeles',
					            DisableAutoEmailSendOut__c = false);
		Id currentUserId = UserInfo.getUserId();
        System.runAs(autoEmailTest) {
			Test.startTest();
				insert opportunityList;
				// update accounts with renewal rep
				list<Account> updateAcctList = new list<Account>();
				for(Account acct: accountList) {

					acct.RenewalRep__c = currentUserId;
					updateAcctList.add(acct);
				}
				update updateAcctList;
			Test.stopTest();
			// validate task creation
			User currentUser = [SELECT Id,Email FROM User WHERE Id =: currentUserId];
			for(Task taskObj : [SELECT Id,Subject,WhatId,
										EmailTo__c,
										AutoEmailFrom__c,
										SendersName__c 
								FROM Task 
								WHERE AutoEmailSubject__c LIKE 'Campaign%' 
								AND WhatId != null]) {
				String emailsStr = taskObj.EmailTo__c;
				System.assertEquals(emailsStr.contains(currentUser.Email), true);
			}
			
		}
	}

	@isTest static void contactEmailUpdateTest() {

		// create opportunities
		list<Account> accountList = new list<Account>();
		list<Contact> contactList = new list<Contact>();
		list<Opportunity> opportunityList = new list<Opportunity>();
		list<Task> taskList = new list<Task>();

		accountList = [SELECT Id,RenewalRep__c FROM Account];
		contactList = [SELECT Id,AccountId,Email FROM Contact];
		Id opptyRecordTypeId = AutoEmailSendUtil.getRecordTypeId('Opportunity',
											AutoEmailSendUtil.INSIDE_SALES_RT);

		for(Integer i=0;i<10;i++) {

			Id accountId = contactList[i].AccountId;
			Id contactId = contactList[i].Id;

			Date currentDT = Date.today();
			Date contractEndDate = currentDT.addDays(5);

			Opportunity oppty = new Opportunity(
									RecordTypeId = opptyRecordTypeId,
									accountId = accountId,
									Industry_Category__c = 'Ad Network',
									Budget_Source__c = 'TV',
									Radio_Type__c = 'Local',
									Initiative_Location__c = 'US',
						            closeDate = System.today(),
						            name = 'autoEmailOpptyTestName'+i,
						            stageName = 'Closed Won',
						            Primary_Billing_Contact__c = contactId,
						            Primary_Contact__c = contactId,
						            ContractStartDate__c = currentDT,
									ContractEndDate__c = contractEndDate,
									Preferred_Invoicing_Method__c = 'Email',
									Bill_on_Broadcast_Calendar2__c = 'No',
									Lead_Campaign_Manager__c = UserInfo.getUserId());
			opportunityList.add(oppty);
		}
		Profile sysAdminProfile = [SELECT Id 
									FROM Profile 
									WHERE name = 'System Administrator' 
									LIMIT 1];
		User autoEmailTest = new User(lastName = 'lastNameAutoEmail',
					            userName = 'autoEmailTest@email.com',
					            profileId = sysAdminProfile.Id,
					            alias = 'aET',
					            email = 'autoEmailTest@email.com',
					            emailEncodingKey = 'ISO-8859-1',
					            languageLocaleKey = 'en_US',
					            localeSidKey = 'en_US',
					            timeZoneSidKey = 'America/Los_Angeles',
					            DisableAutoEmailSendOut__c = false);
		Id currentUserId = UserInfo.getUserId();
        System.runAs(autoEmailTest) {
			Test.startTest();
				insert opportunityList;
				list<Contact> updateContactList = new list<Contact>();
				// update contact Email
				for(Contact contactRec: contactList) {

					String orginalEmail = contactRec.Email;
					String updatedEmail = orginalEmail.replace('@mydomain.com','@updated.com');
					contactRec.Email = updatedEmail;
					updateContactList.add(contactRec);
				}
				update updateContactList;
			Test.stopTest();
			// validate task creation
			User currentUser = [SELECT Id,Email FROM User WHERE Id =: currentUserId];
			for(Task taskObj : [SELECT Id,EmailTo__c
								FROM Task 
								WHERE AutoEmailSubject__c LIKE 'Campaign%' 
								AND WhatId != null]) {
				String emailsStr = taskObj.EmailTo__c;
				System.assertEquals(emailsStr.contains('@updated.com'), true);
			}
			
		}
	}
}