/**
* ─────────────────────────────────────────────────────────────────────────────────────────────────┐
* This class is  .
* ──────────────────────────────────────────────────────────────────────────────────────────────────
* @author         Jaya Alaparthi
* @maintainedBy   Jaya Alaparthi
* @version        1.0
* @created        2017-10-05
* @modified       
* @systemLayer    Util
* @see            ????
* @see            ????
* ──────────────────────────────────────────────────────────────────────────────────────────────────
* @changes
* v1.1          Jaya Alaparthi
* 2017-10-30    90% email should be sent 
* 				to owner, campaing owner and to primary contact.
* 				Campaign end email should not be sent. 
* ──────────────────────────────────────────────────────────────────────────────────────────────────
* @changes
* v1.2            Jaya Alaparthi
* 2017-11-29      only mind campaign and 90% campaign emails should be sent.
* ──────────────────────────────────────────────────────────────────────────────────────────────────
* @changes
* v1.3            Jaya Alaparthi
* 2017-12-07      changing Task Subject as this needs to reflect understable subject for user.
* ─────────────────────────────────────────────────────────────────────────────────────────────────┘
*/
public with sharing class AutoEmailSendUtil {
	
	public static String TASK_PENDING = 'Email Notification Pending';
	public static String TASK_ERROR = 'Email Notification Error';
	public static String TASK_SENT = 'Email Notification Sent';

	public static String START_CAMP = 'CampaignStarted';
	public static String MID_CAMP = 'CampaignMiddle';
	public static String AT90_CAMP = 'Campaign90';
	public static String END_CAMP = 'CampaignEnd';



	public static String INSIDE_SALES_RT = 'Opportunity_Inside_Sales';
	public static String TASK_IS_RT = 'IS_automated_email';

	public static Boolean AutoExecuted = true;
	public static String INTERNAL_EMAIL = '@pandora.com';

    public static String CLOSED_WON = 'Closed Won';
	
	//───────────────────────────────────────────────────────────────────────────┐
	// param		type					
	// opptyIds		list of opportunity Ids
	// ──────────────────────────────────────────────────────────────────────────
	// Description: getOpportunityDetails	get all required fields from 
	//										Opportunity for provided OpptyIds
	//───────────────────────────────────────────────────────────────────────────┘
	public static list<Opportunity> getOpportunityDetails(list<Id> opptyIds) {

		list<Opportunity> opportunityList = new list<Opportunity>();
		Id opptyInsideSalesRT = AutoEmailSendUtil.getRecordTypeId('Opportunity',
														AutoEmailSendUtil.INSIDE_SALES_RT);
		if(!opptyIds.isEmpty()) {
			
			opportunityList = [SELECT Id,Owner.Email, AccountId,
									Primary_Contact__r.Email,
									Primary_Contact__c,
									ContractStartDate__c,
									ContractEndDate__c,
									Contract90Date__c,
									ContractMiddleDate__c,
									Lead_Campaign_Manager__r.Email,
									Primary_Contact__r.Account.DisableAutoEmailSendOut__c,
                                    Account.RenewalRep__r.Email,
                                    Account.RenewalRep__r.Name  
								FROM Opportunity
								WHERE Id IN: opptyIds
								AND RecordTypeId =: opptyInsideSalesRT
								AND Primary_Contact__r.Account.DisableAutoEmailSendOut__c = false
                                AND Account.RenewalRep__r.DisableAutoEmailSendOut__c = false];
		}
		return opportunityList;
	}
	//───────────────────────────────────────────────────────────────────────────┐
	// param				type					
	// opptyIds				list of opportunity Ids
	// taskRecordTypeID		task record type [IS_Autoemail record type Id]
	// ──────────────────────────────────────────────────────────────────────────
	// Description: getRecordedActivities	retrive al ready recorded Email 
	//										activity.
	//───────────────────────────────────────────────────────────────────────────┘
	public static list<Task> getRecordedActivities(list<Id> opptyIds, Id taskRecordTypeID) {

		list<Task> taskList = new list<Task>();

		taskList = [SELECT Id,Subject,ActivityDate,AutoEmailSubject__c,
							Status,WhatId,AutoEmailAccount__c,
							EmailTo__c, AutoEmailAccount__r.Id 
					FROM Task
					WHERE WhatId IN: opptyIds
					AND Status = 'Email Notification Pending'
					AND RecordTypeId =: taskRecordTypeID];
		return taskList;
	}
	//───────────────────────────────────────────────────────────────────────────┐
	// param				type					
	// obj					String
	// recType				record type developer name
	// ──────────────────────────────────────────────────────────────────────────
	// Description: getRecordTypeId	 retrive record type Id
	//───────────────────────────────────────────────────────────────────────────┘
	public static Id getRecordTypeId(String obj, String recType) {
        
        /*Id objRecordId = Schema.getGlobalDescribe().get(obj).getDescribe()
                            .getRecordTypeInfosByName().get(recType)
                            .getRecordTypeId();*/
        RecordType objRecordType = [SELECT DeveloperName,Id,
        									IsActive,SobjectType 
			        					FROM RecordType 
			        					WHERE SobjectType =: obj 
			        					AND IsActive = true 
			        					AND DeveloperName =: recType
			        					LIMIT 1];
		Id recordTypeId = objRecordType.Id;
        return objRecordType.Id;
    }
    /* v1.2
    //───────────────────────────────────────────────────────────────────────────┐
	// param				type					
	// taskList				list of tasks
	// ──────────────────────────────────────────────────────────────────────────
	// Description: getPrimaryContact	retrive primary contact Ids for given 
	//									task list
	//───────────────────────────────────────────────────────────────────────────┘
   	public static set<Id> getPrimaryContact(list<Task> taskList) {

    	set<Id> primaryContactIds = new set<Id>();
    	set<Id> accountIds = new set<Id>();

    	for(Task taskRec : taskList) {

    		String [] emailList;
    		if(taskRec.Subject == START_CAMP && taskRec.EmailTo__c != null) {

    			primaryContactIds.add(taskRec.PrimaryContact__c);
    		}
    	}
    	return primaryContactIds;
    }
    */
    /* v1.2
    // list of welcome task after excluding last one year criteria tasks
    //───────────────────────────────────────────────────────────────────────────┐
	// param						type					
	// primaryContactIds			list of tasks
	// accountIds					set of account Ids
	// taskList 					list of tasks
	// ──────────────────────────────────────────────────────────────────────────
	// Description: campStartValidate	checks if welcome email for the primary 
	//									contact is already sent in the last one year
	//									
	//───────────────────────────────────────────────────────────────────────────┘
    public static list<Task> campStartValidate(set<Id> primaryContactIds,
    											set<Id> accountIds,
    											list<Task> taskList) {

    	// get tasks from last one year with these contactIds
    	List<Task> oldTasksList = new List<Task>();
    	list<Task> campStartTaskList = new list<Task>();
    	map<String,String> campStartAlreadySent = new map<String,String>();

    	Date currentDT = Date.today();
    	Date aYearBackDt = Date.today().addYears(-1);
    	Integer lastYear = aYearBackDt.year();
    	Integer lastMonth = aYearBackDt.month();
    	Integer lastDay = aYearBackDt.day();    	

    	DateTime lastYearDT = Datetime.newInstance(lastYear,lastMonth,lastDay,00,0,0);
    	DateTime todayDt =Datetime.newInstance(currentDT.year(),
    								currentDT.month(),
    								currentDT.day(),00,0,0);

    	System.debug('in campStartValidate primaryContactIds:'+primaryContactIds
    					+'accountIds:'+accountIds
    					+'taskList:'+taskList);
    	oldTasksList = [SELECT Id,PrimaryContact__c,
    						AutoEmailAccount__r.Id,
    						ActivityDate,AutoEmailAccount__c 
    					FROM Task
	    				WHERE PrimaryContact__c IN: primaryContactIds
	    				AND AutoEmailAccount__c IN: accountIds
	    				AND  Subject =: START_CAMP
	    				AND DAY_ONLY(CreatedDate) > 2017-10-10
	    				AND Status =: TASK_SENT
	    				AND EmailTo__c != null];
    	
    	if(!oldTasksList.isEmpty()) {
    		for(Task taskRec: oldTasksList) {

    			if(taskRec.ActivityDate > lastYearDT 
    				&& taskRec.ActivityDate <= todayDt) {

    				String campStartKey = taskRec.PrimaryContact__c+'|'+taskRec.AutoEmailAccount__r.Id;
    				campStartAlreadySent.put(campStartKey,
    									taskRec.PrimaryContact__c);
    			}
    		}
    	} 

    	for(Task taskRec: taskList) {

    		String campStartKey;

    		System.debug('taskRec:'+taskRec);
    		if(!String.isBlank(taskRec.PrimaryContact__c) 
    			&& !String.isBlank(taskRec.AutoEmailAccount__c)) {
    		 
    		 campStartKey = taskRec.PrimaryContact__c+'|'+taskRec.AutoEmailAccount__r.Id;
    		}
    		if((!String.isBlank(taskRec.PrimaryContact__c)  && !String.isBlank(campStartKey)
    				&& !campStartAlreadySent.isEmpty() 
    				&& !campStartAlreadySent.containsKey(campStartKey))
    			||(!String.isBlank(taskRec.PrimaryContact__c) 
    				&& campStartAlreadySent.isEmpty())) {


    			// list the task for email send out
    			campStartTaskList.add(taskRec);
    		} 
    	}

    	return campStartTaskList;
    }
    */
    // delete Task from taskList fo those thaat are not counted 
    // filter out one email category per primary contact per account
    // the return value of this mapp should be give as input for batch for generating email tempates.
    //───────────────────────────────────────────────────────────────────────────┐
	// param						type					
	// campStartTaskList			list of tasks
	// ──────────────────────────────────────────────────────────────────────────
	// Description: getTaskPerAccount	arranges task in the form of a map with
	//									key[primary contact|accountId|Subject]
	//									
	//───────────────────────────────────────────────────────────────────────────┘
    public static map<String,Task> getTaskPerAccount(list<Task> campStartTaskList) {

    	map<String,Task> primContSubTaskMap = new map<String,Task>();

    	for(Task taskRec: campStartTaskList) {

    		String mapKey = taskRec.PrimaryContact__c+'|'
    						+taskRec.AutoEmailAccount__r.Id+'|'
    						+taskRec.Subject;
    		primContSubTaskMap.put(mapKey,taskRec);
    	}
    	return primContSubTaskMap;
    }
    //v1.3
    // map of AccountId|AutoEmailSubject__c to set of realted Tasks
    //───────────────────────────────────────────────────────────────────────────┐
	// param			type					
	// taskList			list of tasks
	// ──────────────────────────────────────────────────────────────────────────
	// Description: getWhatIdToAcctSubMap	arranges whatId in the form of a map with
	//									key[accountId|AutoEmailSubject__c]
	//									
	//───────────────────────────────────────────────────────────────────────────┘
    public static map<String,set<Id>> getWhatIdToAcctSubMap(list<Task> taskList) {

    	map<String,set<Id>> setOfWhatIdToAcctSubMap = new map<String,set<Id>>();

    	for(Task taskRec: taskList) {

            if(taskRec.AutoEmailAccount__c != null) {
        		String mapKey = taskRec.AutoEmailAccount__r.Id
        						+'|'+taskRec.AutoEmailSubject__c;    						
        		set<Id> whatIdsSet = new set<Id>();

        		if(taskRec.ActivityDate == Date.today()) {

    	    		if(setOfWhatIdToAcctSubMap.containsKey(mapKey)) {

    	    			whatIdsSet = setOfWhatIdToAcctSubMap.get(mapKey);
    	    		}
    	    		whatIdsSet.add(taskRec.WhatId);
    	    		setOfWhatIdToAcctSubMap.put(mapKey,whatIdsSet);
        		}
            }
    	}
    	return setOfWhatIdToAcctSubMap;
    }

    // method is used in Renewal Rep updated on account.REnewalRep__c field change.
    //───────────────────────────────────────────────────────────────────────────┐
    // param            type
    // acctIdsSet       Set<Id>
    // ──────────────────────────────────────────────────────────────────────────
    // Description: getExistingTasksByAcctId 
    //              retrive existing Tasks based on AccountId and update 
    //              REnewal Rep Email Id to Email To field on task
    //───────────────────────────────────────────────────────────────────────────┘
    public static list<Task> getExistingTasksByAcctId(set<Id> acctIdsSet,
                                                    set<Id> opptyIdsSet) {

        list<Task> existingTaskList = new list<Task>();

        Id taskRecordTypeID = getRecordTypeId('Task',AutoEmailSendUtil.TASK_IS_RT);
        if(!acctIdsSet.isEmpty()) {
        
            existingTaskList = [SELECT Id,Subject,ActivityDate,SendersName__c, 
                                    Status,WhatId,AutoEmailAccount__c,
                                    EmailTo__c, AutoEmailAccount__r.Id, 
                                    AutoEmailFrom__c,AutoEmailSubject__c  
                                FROM Task
                                WHERE AutoEmailAccount__c IN: acctIdsSet
                                AND Status = 'Email Notification Pending'
                                AND RecordTypeId =: taskRecordTypeID
                                AND WhatId IN: opptyIdsSet];
        }
        return existingTaskList;
    }
    // method is used in Renewal Rep updated on account.REnewalRep__c field change.
    //───────────────────────────────────────────────────────────────────────────┐
    // param            type
    // acctIdsSet       Set<Id>
    // ──────────────────────────────────────────────────────────────────────────
    // Description: getNonClosedOpptyIds 
    //              retrive Opportunties that are not closed won
    //───────────────────────────────────────────────────────────────────────────┘
    public static Set<Id> getClosedOpptyIds(set<Id> acctIdsSet) {

        set<Id> opptyIdsSet = new set<Id>();

        Id opptyInsideSalesRT = AutoEmailSendUtil.getRecordTypeId('Opportunity',
                                                        AutoEmailSendUtil.INSIDE_SALES_RT);
        if(!acctIdsSet.isEmpty()) {

            for(Opportunity oppty: [SELECT Id FROM Opportunity
                                    WHERE StageName =: CLOSED_WON
                                    AND AccountId IN: acctIdsSet
                                    AND RecordTypeId =: opptyInsideSalesRT]) {

                opptyIdsSet.add(oppty.Id);
            }
        }
        return opptyIdsSet;
    }
    // method to get map of task subject to email template Id
    //───────────────────────────────────────────────────────────────────────────┐
	// ──────────────────────────────────────────────────────────────────────────
	// Description: getEmailTemplateMap	:get email templates in the form of a map
	//									with template name as the key
	//									
	//───────────────────────────────────────────────────────────────────────────┘
    public static map<String,EmailTemplate> getEmailTemplateMap() {

    	list<String> Email_Template_List = new list<String>();
    	map<String,EmailTemplate> tskSubToEmailTemplateMap = new map<String,EmailTemplate>();
    	/*
    		('CampaignStarted',
			'CampaignMiddle',
			'Campaign90',
			'CampaignEnd')
    	*/
        // v1.2
    	/*Email_Template_List.add(START_CAMP);*/
    	Email_Template_List.add(MID_CAMP);
		Email_Template_List.add(AT90_CAMP);
		// v1.1
		//Email_Template_List.add(END_CAMP);

    	for(EmailTemplate emailTemp: [SELECT Body,DeveloperName,Id,
    										IsActive,Name,Subject 
    								FROM EmailTemplate 
    								WHERE Name IN : Email_Template_List
                                    AND TemplateType = 'visualforce' ]) {

    		tskSubToEmailTemplateMap.put(emailTemp.DeveloperName,emailTemp);
    	}
    	return tskSubToEmailTemplateMap;
    }
    // Record Errors
    // method to get map of task subject to email template Id
    //───────────────────────────────────────────────────────────────────────────┐
    // param			type
    // errStr			String [error]
    // jobId			String [Batch Job Id]
    // srcClsStr		String [source class]
	// ──────────────────────────────────────────────────────────────────────────
	// Description: getEmailTemplateMap	:get email templates in the form of a map
	//									with template name as the key
	//									
	//───────────────────────────────────────────────────────────────────────────┘
    public static Error_Log__c createError(String errStr,
    								String jobId,
    								String srcClsStr) {

    	Error_Log__c errorRec = new Error_Log__c(Class__c = srcClsStr,
    											Error__c = errStr,
    											Type__c = 'Error',
    											ID_List__c = jobId);
    	return errorRec;
    }
}