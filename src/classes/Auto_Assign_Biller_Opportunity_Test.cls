@isTest
public class Auto_Assign_Biller_Opportunity_Test {

    /* NON_THOROUGH TESTS */
    // Create Opportunity with Matching Lead_Campaign_Manager__c
    static testmethod void testInsertMatchingCM(){
		Auto_Assign_Biller_Opportunity_Test test = new Auto_Assign_Biller_Opportunity_Test();
        test.createTestUsers();
        test.setupAABRecords();
        Opportunity testOpp = test.testOpp(test.CM1.Id);
        Opportunity result = [SELECT Id, Billing_Coordinator__c, Lead_Campaign_Manager__c FROM Opportunity LIMIT 1 ];
        //system.assert(false,Auto_Assign_Biller__c.getAll() + ' <<>> ' + test.BC.Id + ' <<>> ' + result.Billing_Coordinator__c);
       // system.assertEquals(test.BC.Id, result.Billing_Coordinator__c);
    }
    
    // Create Opportunity with non-Matching Lead_Campaign_Manager__c
    static testmethod void testInsertNonMatchingCM(){
		Auto_Assign_Biller_Opportunity_Test test = new Auto_Assign_Biller_Opportunity_Test();
        test.createTestUsers();
        test.setupAABRecords();
        Opportunity testOpp = test.testOpp(test.CM3.Id);
        Opportunity result = [SELECT Id, Billing_Coordinator__c, Lead_Campaign_Manager__c FROM Opportunity  LIMIT 1 ];
        system.assertEquals(null, result.Billing_Coordinator__c);
    }
    // Create Opportunity without Lead_Campaign_Manager__c
    static testmethod void testInsertNoCM(){
		Auto_Assign_Biller_Opportunity_Test test = new Auto_Assign_Biller_Opportunity_Test();
        test.createTestUsers();
        test.setupAABRecords();
        Opportunity testOpp = test.testOpp(null);
        Opportunity result = [SELECT Id, Billing_Coordinator__c, Lead_Campaign_Manager__c FROM Opportunity  LIMIT 1 ];
        system.assertEquals(null, result.Billing_Coordinator__c);
    }
    
    // Create an Oppty where the batching BC is on vacation and has an alternate 
    static testmethod void testInsertAlternateCM(){
		Auto_Assign_Biller_Opportunity_Test test = new Auto_Assign_Biller_Opportunity_Test();
        test.createTestUsers();
        test.setupAABRecords();
        Opportunity testOpp = test.testOpp(test.CM2.Id);
        Opportunity result = [SELECT Id, Billing_Coordinator__c, Lead_Campaign_Manager__c FROM Opportunity LIMIT 1 ];
       // system.assertEquals(test.BCA.Id, result.Billing_Coordinator__c);
    }
    
/* UNTESTED: */
    
    /* Updates no longer part of this functionality
    // Update Opportunity to add Campaign_Manager__c previously missing
    static testmethod void testInsertAddCM(){
		Auto_Assign_Biller_Opportunity_Test test = new Auto_Assign_Biller_Opportunity_Test();
        test.createTestUsers();
        test.setupAABRecords();
        Opportunity testOpp = test.testOpp(null);
        Opportunity result = [SELECT Id, Billing_Coordinator__c, Campaign_Manager__c FROM Opportunity WHERE Id = :testOpp.Id ];
        system.assertEquals(null, result.Billing_Coordinator__c); // confirm that the BC is still missing
        result.Campaign_Manager__c = test.CM1.Id;
        update result;
        Opportunity result2 = [SELECT Id, Billing_Coordinator__c, Campaign_Manager__c FROM Opportunity WHERE Id = :testOpp.Id ];
        system.assertEquals(test.BC.Id, result2.Billing_Coordinator__c);
    }
    
    // Update Opportunity to remove Campaign_Manager__c
    static testmethod void testRemoveCM(){
		Auto_Assign_Biller_Opportunity_Test test = new Auto_Assign_Biller_Opportunity_Test();
        test.createTestUsers();
        test.setupAABRecords();
        Opportunity testOpp = test.testOpp(test.CM1.Id);
        Opportunity result = [SELECT Id, Billing_Coordinator__c, Campaign_Manager__c FROM Opportunity WHERE Id = :testOpp.Id ];
        system.assertEquals(test.BC.Id, result.Billing_Coordinator__c); // Confirm that the BC was assigned correctly
        result.Campaign_Manager__c = null;
        update result;
        Opportunity result2 = [SELECT Id, Billing_Coordinator__c, Campaign_Manager__c FROM Opportunity WHERE Id = :testOpp.Id ];
        system.assertEquals(test.BC.Id, result2.Billing_Coordinator__c); // Confirms that we do not remove the BC when the CM is removed
    }
    
    // Update Opportunity to switch Campaign_Manager__c from Matching to non-Matching
    static testmethod void testChangeToNonMatchingCM(){
		Auto_Assign_Biller_Opportunity_Test test = new Auto_Assign_Biller_Opportunity_Test();
        test.createTestUsers();
        test.setupAABRecords();
        Opportunity testOpp = test.testOpp(test.CM1.Id);
        Opportunity result = [SELECT Id, Billing_Coordinator__c, Campaign_Manager__c FROM Opportunity WHERE Id = :testOpp.Id ];
        system.assertEquals(test.BC.Id, result.Billing_Coordinator__c); // Confirm that the BC was assigned correctly
        result.Campaign_Manager__c = test.CM3.Id;
        update result;
        Opportunity result2 = [SELECT Id, Billing_Coordinator__c, Campaign_Manager__c FROM Opportunity WHERE Id = :testOpp.Id ];
        system.assertEquals(test.BC.Id, result2.Billing_Coordinator__c); // Confirms that we do not remove the BC when the CM changed top a non-matching CM
    }
    
	// Delete Opportunity with Campaign_Manager__c
    static testmethod void testDeleteOpp(){
		Auto_Assign_Biller_Opportunity_Test test = new Auto_Assign_Biller_Opportunity_Test();
        test.createTestUsers();
        test.setupAABRecords();
        Opportunity testOpp = test.testOpp(test.CM1.Id);
        Delete testOpp;
    }
    
	// un-delete Campaign_Manager__c with Campaign_Manager__c
    // Update Opportunity to switch Campaign_Manager__c from non-Matching to Matching
    // Update Opportunity to switch Campaign_Manager__c from Matching to Matching
    // Update Opportunity to switch Campaign_Manager__c from non-Matching to non-Matching
    */
    
/* UTILITY METHODS */
    
    // Create test users
    public User runningUser; 	// 	User to run the tests as
    public User CM1; 			// 	Campaign Manager1 assigned a biller
    public User CM2; 			// 	Campaign Manager2 assigned a biller W/ vacation
    public User CM3; 			// 	Campaign Manager3 no biller assigned
	public User BC; 			// 	Billing Coordinator
    public User BCA; 			// 	Billing Coordinator alternate
    public void createTestUsers(){
        runningUser = UTIL_TestUtil.generateUser();
        CM1 = UTIL_TestUtil.generateUser();
        CM2 = UTIL_TestUtil.generateUser();
        CM3 = UTIL_TestUtil.generateUser();
        BC = UTIL_TestUtil.generateUser();
        BCA = UTIL_TestUtil.generateUser();
        insert new List<User>{runningUser, CM1, CM2, CM3, BC, BCA};
    }
    
    // Create Auto_Assign_Biller__c records
    // 		Campaign Manager1 => Billing Coordinator => Billing Coordinator alternate

    public Auto_Assign_Biller__c aab1; // Biller
    public Auto_Assign_Biller__c aab2; // Biller w/ vacation
    public void setupAABRecords(){
        aab1 = new Auto_Assign_Biller__c(  // Biller
        	Name=CM1.Id,
            Campaign_Manager__c = CM1.Id,
            Billing_Coordinator__c = BC.Id,
            Billing_Coordinator_ID__c = BC.Id,
            Billing_Coordinator_on_Vacation__c = false,
            Temporary_Billing_Coordinator__c = '',
            Temporary_Billing_Coordinator_ID__c = ''
        );
        aab2 = new Auto_Assign_Biller__c( // Biller w/ vacation
        	Name=CM2.Id,
        	Campaign_Manager__c = CM2.Id,
            Billing_Coordinator__c = BC.Id,
            Billing_Coordinator_ID__c = BC.Id,
            Billing_Coordinator_on_Vacation__c = true,
            Temporary_Billing_Coordinator__c = BCA.Id,
            Temporary_Billing_Coordinator_ID__c = BCA.Id
        );
        insert new List<Auto_Assign_Biller__c>{aab1, aab2};
    }
    
    // 	Create Opportunity
    public Opportunity testOpp(Id cmId){
        // Create Account
        Account testAcct = UTIL_TestUtil.createAccount();
        // Create Opportunity
        Opportunity theOpp = UTIL_TestUtil.generateOpportunity(testAcct.Id);
        // add a Campaign_Manager__c
        theOpp.Lead_Campaign_Manager__c = cmId;
        // save and return the Oppty
        insert theOpp;
        return theOpp;
    }
}