@isTest(seeAllData=false)

private class AvisoOpportunitySplitTriggerTest {

	@testSetup static void setupDataForTest() {

		list<Opportunity_Split__c> opptySplitList = new list<Opportunity_Split__c>();
		
		// create Accout
		Account acc = new Account(name = 'AvisoSplitTest');

        insert acc;
        Contact cont = new Contact(lastName = 'AvisoTestContact',
        					 accountId = acc.id ,
        					  MailingStreet = 'MyStreet1',
        					  MailingCountry = 'USA',
        					  MailingState = 'CA',
        					  MailingCity = 'Fremont',
        					  Email = 'avisoOptySPlit@triggertest.com',
        					  Title = 'MyTitle1',
        					  firstname = 'MyFristName1');
        insert cont;

        // create Opportunity
		List<Opportunity> oppLst = new List<Opportunity>();
        Opportunity oppty;
        for(Integer i = 0; i < 5 ; i++) {

                oppty = new Opportunity(
		                accountId = acc.Id,
		                bill_on_broadcast_calendar2__c = 'testaviso',
		                closeDate = Date.Today(),
		                confirm_direct_relationship__c = false,
		                name = 'testaviso'+String.ValueOf(i),
		                stageName = 'Prospected',
		                Primary_Billing_Contact__c = cont.Id,
		                Lead_Campaign_Manager__c = UserInfo.getUserId(),
		                Agency__c = acc.id
		            );
            oppty.ContractStartDate__c = Date.today();
            oppty.ContractEndDate__c = Date.today()+50;

            oppLst.add(oppty);
        }
        insert oppLst;
        //Opportunity opty = [SELECT Id FROM Opportunity Limit 1];

        for(Opportunity opty : [SELECT Id FROM Opportunity]) {

        		opptySplitList.add(new 
        			Opportunity_Split__c(Opportunity__c = opty.Id,
										Role_Type__c = 'Seller',
										Split__c = 20));
        	
        }
		insert opptySplitList;

	}
	static testMethod void testAvisoSplitDetailUpdateTrigger (){
	
		list<Opportunity_Split__c> opptySplitList = new list<Opportunity_Split__c>();
		

		for(Opportunity_Split__c oppty : [SELECT Id,Role_Type__c
										FROM Opportunity_Split__c
										WHERE Role_Type__c ='Seller']) {
			oppty.Role_Type__c = 'Performance (Shadow)';
			oppty.Split__c = 0;
			opptySplitList.add(oppty);
		}
		
		Test.startTest();
			
			update opptySplitList;
			Gnana__AvisoCustomObject__c avisoTest = [SELECT Id,	Gnana__Action__c
													FROM Gnana__AvisoCustomObject__c
													WHERE Gnana__sObjectName__c = 'Opportunity_Split__c'
													limit 1];      
			System.debug('avisoTest:'+avisoTest);
			System.assertNotEquals(avisoTest,null);
			System.debug('avisoTest:'+avisoTest);
		Test.stopTest();
	}
	static testMethod void testAvisoSplitDetailDeleteTrigger (){
	
		list<Opportunity_Split__c> opptySplitList = new list<Opportunity_Split__c>();
		

		for(Opportunity_Split__c oppty : [SELECT Id,Role_Type__c
										FROM Opportunity_Split__c
										WHERE Role_Type__c ='Seller']) {
			oppty.Role_Type__c = 'Performance (Shadow)';
			oppty.Split__c = 0;
			opptySplitList.add(oppty);
		}
		
		Test.startTest();
			
			delete opptySplitList;
			Gnana__AvisoCustomObject__c avisoTest = [SELECT Id,	Gnana__Action__c
													FROM Gnana__AvisoCustomObject__c
													WHERE Gnana__sObjectName__c = 'Opportunity_Split__c'
													AND Gnana__Action__c = 'delete'
													limit 1];      
			System.debug('avisoTest:'+avisoTest);
			System.assertNotEquals(avisoTest,null);
			System.debug('avisoTest:'+avisoTest);

			undelete opptySplitList;
		Test.stopTest();
	}
}