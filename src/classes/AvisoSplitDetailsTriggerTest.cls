@isTest
private class AvisoSplitDetailsTriggerTest {
	
	@testSetup static void setupDataForTest() {

		list<Opportunity_Split__c> opptySplitList = new list<Opportunity_Split__c>();
		list<Split_Detail__c> splitDetailsList = new list<Split_Detail__c>();

		Id currentUserId = System.UserInfo.getUserId();
		// create Accout
		Account acc = new Account(name = 'AvisoSplitTest');

        insert acc;
        Contact cont = new Contact(lastName = 'AvisoTestContact',
        					 accountId = acc.id ,
        					  MailingStreet = 'MyStreet1',
        					  MailingCountry = 'USA',
        					  MailingState = 'CA',
        					  MailingCity = 'Fremont',
        					  Email = 'avisoOptySPlit@triggertest.com',
        					  Title = 'MyTitle1',
        					  firstname = 'MyFristName1');
        insert cont;

        // create Opportunity
		List<Opportunity> oppLst = new List<Opportunity>();
        Opportunity oppty;
        for(Integer i = 0; i < 5 ; i++) {

                oppty = new Opportunity(
		                accountId = acc.Id,
		                bill_on_broadcast_calendar2__c = 'testaviso',
		                closeDate = Date.Today(),
		                confirm_direct_relationship__c = false,
		                name = 'testaviso'+String.ValueOf(i),
		                stageName = 'Prospected',
		                Primary_Billing_Contact__c = cont.Id,
		                Lead_Campaign_Manager__c = UserInfo.getUserId(),
		                Agency__c = acc.id
		            );
            oppty.ContractStartDate__c = Date.today();
            oppty.ContractEndDate__c = Date.today()+50;

            oppLst.add(oppty);
        }
        insert oppLst;
        //Opportunity opty = [SELECT Id FROM Opportunity Limit 1];

        for(Integer i=0; i< 5; i++) {
        	for(Opportunity opty : [SELECT Id FROM Opportunity]) {

        		opptySplitList.add(new 
        			Opportunity_Split__c(Opportunity__c = opty.Id,
										Role_Type__c = 'Seller',
										Split__c = 20));
        	}
        }
		insert opptySplitList;

		Date tDay = Date.today();
		String todayStr = tDay.year()+'-'+tDay.month()+'-'+tDay.day();
		for(Opportunity_Split__c opptySplit : [SELECT Id FROM Opportunity_Split__c 
												WHERE Role_Type__c = 'Seller']) {

			 Split_Detail__c sd = new Split_Detail__c(Opportunity_Split__c = opptySplit.Id,
                                                 Salesperson_User__c=currentUserId,
                                                Date__c = Date.valueOf(todayStr));
        	splitDetailsList.add(sd);
		}
		insert splitDetailsList;
	}
	@isTest static void testUpdateAction() {
		
		list<Split_Detail__c> updateSplitDetailsList = new list<Split_Detail__c>();
		Date tDay = Date.today().addDays(5);
		String todayStr = tDay.year()+'-'+tDay.month()+'-'+tDay.day();

		for(Split_Detail__c sd : [SELECT Id,Date__c FROM Split_Detail__c]) {

			updateSplitDetailsList.add(new Split_Detail__c(Id=sd.Id,Date__c=Date.valueOf(todayStr)));
		}
		Test.startTest();
			update updateSplitDetailsList;
			Gnana__AvisoCustomObject__c updateAvisoTest = [SELECT Id,	Gnana__Action__c
													FROM Gnana__AvisoCustomObject__c
													WHERE Gnana__sObjectName__c = 'Split_Detail__c'
													AND Gnana__Action__c = 'update'
													limit 1];      
			System.debug('updateAvisoTest:'+updateAvisoTest);
			System.assertNotEquals(updateAvisoTest,null);
			System.debug('updateAvisoTest:'+updateAvisoTest);
		Test.stopTest();

	}
	@isTest static void testDeleteUndeleteAction() {

		list<Split_Detail__c> delSplitDetailsList = new list<Split_Detail__c>();

		delSplitDetailsList = [SELECT Id FROM Split_Detail__c];

		Test.startTest();
			
			delete delSplitDetailsList;
			Gnana__AvisoCustomObject__c delAvisoTest = [SELECT Id,	Gnana__Action__c
													FROM Gnana__AvisoCustomObject__c
													WHERE Gnana__sObjectName__c = 'Split_Detail__c'
													AND Gnana__Action__c = 'delete'
													limit 1];      
			System.debug('delAvisoTest:'+delAvisoTest);
			System.assertNotEquals(delAvisoTest,null);
			System.debug('delAvisoTest:'+delAvisoTest);

			undelete delSplitDetailsList;
			Gnana__AvisoCustomObject__c undelAvisoTest = [SELECT Id,	Gnana__Action__c
													FROM Gnana__AvisoCustomObject__c
													WHERE Gnana__sObjectName__c = 'Split_Detail__c'
													AND Gnana__Action__c = 'undelete'
													limit 1];    
			System.debug('undelAvisoTest:'+undelAvisoTest);
			System.assertNotEquals(undelAvisoTest,null);
			System.debug('undelAvisoTest:'+undelAvisoTest);

		Test.stopTest();
		
		
	}
	
	
	
}