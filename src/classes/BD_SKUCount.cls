public with sharing class BD_SKUCount {

    @future
    public static void calculateSKUCount(Set<Id> accIds){
        Set<Id> aggAccounts = new Set<Id>();
        System.debug(LoggingLevel.INFO, 'Accids for update = '+accIds);
        AggregateResult[] groupedResults = [select account__c, sum(Number_of_SKUs__c) from Project__c where account__c in :accIds GROUP BY Account__c];
        Map<Id,Account> accMap = new Map<Id, Account>([select id, Total_SKUs__c from Account where id in :accIds]);
        for(AggregateResult ar : groupedResults){
            System.debug(LoggingLevel.INFO,'AccountId = '+ ar.get('Account__c'));
            System.debug(LoggingLevel.INFO,'SKUs = '+ ar.get('expr0'));
            accMap.get((Id) ar.get('Account__c')).Total_SKUs__c = (Decimal) ar.get('expr0');
            aggAccounts.add((Id) ar.get('Account__c'));
        }
        //there will be some accounts for which there are no projects left. Need to update the total for those = 0 
        for(Account a : accMap.values()){
            if(!aggAccounts.contains(a.Id)){
                a.Total_SKUs__c = 0;
            }
        }
        try{
            update accMap.values();
        }catch(DMLException de){
            System.debug('Exception in Updating total SKUs : ' + de.getDMLMessage(0));
        }catch(Exception e){
            System.debug('Exception : ' + e.getMessage());
        }
        
    }
}