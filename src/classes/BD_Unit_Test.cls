@isTest
private class BD_Unit_Test {

    static testmethod void testSKUCountOnAccount(){
        List<Account> accList = new List<Account>();
        Account a1 = new Account(Name = 'Acc1', Type='Ad Agency');
        accList.add(a1);
        Account a2 = new Account(Name = 'Acc2', Type='Ad Agency');
        accList.add(a2);
        insert accList;
        Test.startTest();
        List<Project__c> projects = new List<Project__c>();
        Project__c p1 = new Project__c(Name='P1', Account__c = a1.Id, Number_Of_SKUs__c = 5);
        projects.add(p1);
        Project__c p2 = new Project__c(Name='P2', Account__c = a1.Id, Number_Of_SKUs__c = 5);
        projects.add(p2);
        Project__c p3 = new Project__c(Name='P3', Account__c = a2.Id, Number_Of_SKUs__c = 5);
        projects.add(p3);
        insert projects;
        Test.stopTest();
        Map<Id,Account> acctsMap = new Map<Id,Account>([select Id, Total_SKUs__c from Account where id in :accList]);
        System.assertEquals(10, acctsMap.get(a1.Id).Total_SKUs__c);
    }
    
    static testmethod void testUpdateSKUCountOnAccount(){
        List<Account> accList = new List<Account>();
        Account a1 = new Account(Name = 'Acc1', Type='Ad Agency');
        accList.add(a1);
        Account a2 = new Account(Name = 'Acc2', Type='Ad Agency');
        accList.add(a2);
        insert accList;
        Test.startTest();
        List<Project__c> projects = new List<Project__c>();
        Project__c p1 = new Project__c(Name='P1', Account__c = a1.Id, Number_Of_SKUs__c = 5);
        projects.add(p1);
        Project__c p2 = new Project__c(Name='P2', Account__c = a1.Id, Number_Of_SKUs__c = 5);
        projects.add(p2);
        Project__c p3 = new Project__c(Name='P3', Account__c = a2.Id, Number_Of_SKUs__c = 5);
        projects.add(p3);
        insert projects;
        p1.Number_of_SKUs__c = 10;
        update p1;
        Test.stopTest();
        Account acc1 = [select Id, Total_SKUs__c from Account where id = :a1.Id];
        System.assertEquals(15, acc1.Total_SKUs__c);
        
    }

    static testmethod void testUpdateAccountOnProject(){
        List<Account> accList = new List<Account>();
        Account a1 = new Account(Name = 'Acc1', Type='Ad Agency');
        accList.add(a1);
        Account a2 = new Account(Name = 'Acc2', Type='Ad Agency');
        accList.add(a2);
        insert accList;
        Test.startTest();
        List<Project__c> projects = new List<Project__c>();
        Project__c p1 = new Project__c(Name='P1', Account__c = a1.Id, Number_Of_SKUs__c = 5);
        projects.add(p1);
        Project__c p2 = new Project__c(Name='P2', Account__c = a1.Id, Number_Of_SKUs__c = 5);
        projects.add(p2);
        Project__c p3 = new Project__c(Name='P3', Account__c = a2.Id, Number_Of_SKUs__c = 5);
        projects.add(p3);
        insert projects;
        p1.Number_of_SKUs__c = 10;
        update p1;
        
        p1.Account__c = a2.Id;
        update p1;
        Test.stopTest();
        Map<Id,Account> acctsMap1 = new Map<Id,Account>([select Id, Total_SKUs__c from Account where id in :accList]);
        System.assertEquals(5, acctsMap1.get(a1.Id).Total_SKUs__c);
        System.assertEquals(15, acctsMap1.get(a2.Id).Total_SKUs__c);
        
    }
    
    static  testmethod void testDupeProject() {
        Account acc = new Account(Name = 'Acc1', Type='Ad Agency');
        insert acc;

        Project__c p1 = new Project__c(Name='P1', Account__c = acc.Id, Number_Of_SKUs__c = 5);
        insert p1;
        Project__c p2 = new Project__c(Name='P1', Account__c = acc.Id, Number_Of_SKUs__c = 5);

        try {
            insert p2;
            System.assertEquals('1', '2', 'Should not com here');
        } catch(DMLException e) {
            System.assertEquals('1', '1', 'Dupe project not allowed.');
            
        }
        
    }
    
    static  testmethod void testDupeBizDevAccount() {
        //nsk: 10/23 Adding below logic to control trigger execution via custom label
        if(Label.Disable_Account_Triggers!='YES'){        
            Map<String, Schema.RecordTypeInfo> accountRT = Schema.SObjectType.Account.getRecordTypeInfosByName();
            Schema.RecordTypeInfo bizDevRT = accountRT.get('Business Development');
            Id rtId = bizDevRT.getRecordTypeId();
            
            Account acc1 = new Account(Name = 'Acc1', Type='BizDev', RecordTypeId = rtId);
            insert acc1;
    
            Account acc2 = new Account(Name = 'Acc1', Type='BizDev', RecordTypeId = rtId);
    
            try {
                insert acc2;
                System.assertEquals('1', '2', 'Should not com3 here');
            } catch(DMLException e) {
                System.assertEquals('1', '1', 'Dupe Accounts not allowed.');
                
            }
        }            
    }

    static testmethod void testSetProject() {
        Account acc = new Account(Name = 'Acc1', Type='Ad Agency');
        insert acc;

        Project__c p1 = new Project__c(Name='P1', Account__c = acc.Id, Number_Of_SKUs__c = 5);
        insert p1;

        Agreement__c a1 = new Agreement__c(Account__c = acc.Id, Project__c = p1.Id);        
        Agreement__c a2 = new Agreement__c(Account__c = acc.Id);
        insert (new List<Agreement__c> {a1, a2});       
    }
}