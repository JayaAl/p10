/*
nsk: 11/14/14
Desc: This controller is used by the BadDebtsList VF page. The VF page is used to display the BadDebts records for a given account
*/
public class BadDebtsController {

    Id accId;
    public BadDebtsController (ApexPages.StandardController stdController) {
        accId = stdController.getId().substring(0,15);
    }
    
    
    public List<Bad_Debt_Refund__c> baddebtsList { get {
        system.debug(accId);
        String newID = ''+accId;
        newID = newID.substring(0,15); 
        List<Bad_Debt_Refund__c> baddebtsList = [select id, name, Advertiser__c, 
Invoice_Number_lu__r.c2g__Account__c, Status__c, RecordType.Name, Write_off_Amount__c, Bad_Debt_Amount_Invoice_Amount__c, LastModifiedDate from Bad_Debt_Refund__c where Hidden_Agency_Invoice_ID__c =: newId or Hidden_Advertiser_Id__c =: newId];
        return baddebtsList;
    }}

}