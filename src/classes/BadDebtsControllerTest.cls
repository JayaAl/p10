@isTest(seealldata=true)
public class BadDebtsControllerTest {
    static testMethod void testBadDebtsController() {
        
        
		//Setup        
        //FF_BLNG_TestUtil_InvoiceRollups util = new FF_BLNG_TestUtil_InvoiceRollups();
        //util.setup();
        
        system.runAs(new User(id = UserInfo.getUserId())) {
            Test.startTest();
            Account testCtrlAccount = UTIL_TestUtil.createAccount();
            //Create Account and Opportunity for Invoice
            //Account testAccount = FF_BLNG_TestUtil.createAccount();
            //Opportunity testOppty = FF_BLNG_TestUtil.createOpportunity(testAccount.id);
            //Create Invoice
           // c2g__codaInvoice__c invoice = util.createSalesInvoice(testAccount.id, testOppty.id);
            
            //Bad_Debt_Refund__c testBDR = new Bad_Debt_Refund__c(Client__c = testAccount.Id, Invoice_Number_lu__c = invoice.Id);
            //insert testBDR;
            PageReference pageRef = Page.BadDebtsList;
            pageRef.getParameters().put('id', String.valueOf(testCtrlAccount.Id));
            Test.setCurrentPage(pageRef);
            ApexPages.StandardController sc = new ApexPages.StandardController(testCtrlAccount);
            BadDebtsController testController = new BadDebtsController(sc);
            List<Bad_Debt_Refund__c> testListBDR = testController.baddebtsList;
            Test.stopTest();
        }
        
    }
}