/**
 * @name: BatchAccTeamdOwnerDupcleanup 
 * @desc: Batch class is Used to Cleanup the duplicate Ownerships on AccountTeams
 * @author: Priyanka Ghanta
 * @date: 07/28/2015
 */
global class BatchAccTeamdOwnerDupcleanup implements Database.Batchable<sObject>, Schedulable {
   
    global Map <String,String> oppLine_error = new Map <String,String>();
    global Database.QueryLocator start(Database.BatchableContext bc) {     
       return Database.getQueryLocator('SELECT Id,(SELECT Id, AccountId, TeamMemberRole FROM AccountTeamMembers WHERE TeamMemberRole = \'Account Owner\' order by createdDate DESC) FROM Account');      
    }
    global void execute(Database.BatchableContext bc, List<Account> scope){
        List<AccountTeamMember> lstAccTeamMember = new List<AccountTeamMember>();
        for(Account acc :scope) {
      
            for(Integer i=1;i<acc.AccountTeamMembers.size();i++){
                lstAccTeamMember.add(acc.AccountTeamMembers[i]);
            }
        }
        delete lstAccTeamMember;
    }
    global void execute(SchedulableContext SC) {
        BatchAccTeamdOwnerDupcleanup accTeamMem = new BatchAccTeamdOwnerDupcleanup();
        database.executeBatch(accTeamMem);
    }
    global void finish(Database.BatchableContext bc){
    
     /*AsyncApexJob aaj = [Select Id, Status, NumberOfErrors, JobItemsProcessed, MethodName, TotalJobItems, CreatedBy.Email
                                                                                 from AsyncApexJob where Id = :BC.getJobId()];     
        Messaging.SingleEmailMessage mail = new Messaging.SingleEmailMessage();
         String[] toAddresses = new String[] {aaj.CreatedBy.Email};
         mail.setToAddresses(toAddresses);
         mail.setSubject('Removing Duplicate Account Owners on AccountTeams is ' + aaj.Status);
         mail.setPlainTextBody('Errors: ' + oppLine_error);
         Messaging.sendEmail(new Messaging.SingleEmailMessage[] { mail }); */
    }
}