global class BatchCQForcastedRevenue implements Database.Batchable<sObject>,Database.Stateful,Schedulable  {
    
    Map<Id,Double> oppAmount = new Map<Id,Double>();
    
    /* Gather a list of IOs where the Opportunity has been updated within the last 7 days */
    global Database.QueryLocator start(Database.BatchableContext bc) {
        return Database.getQueryLocator([
            Select id,Opportunity__c,Status_2__c,CQ_Forecasted_Revenue__c 
            FROM IO_Detail__c 
            // WHERE Opportunity__r.LastModifiedDate = Last_N_Days:7
            WHERE (Status_2__c = 'Pending' OR Status_2__c = 'Rejected')
        ]);
    }
    
    global void execute(Database.BatchableContext bc, List<IO_Detail__c> scope) {
        
        Map<Id, Id> m_OpptyToIO = new Map<Id, Id>(); 						// Map of Opportunity Id to IO Id
        Map<Id, IO_Detail__c> m_IODetail = new Map<Id, IO_Detail__c>(); 	// Map of IO Id to the IO itself
        List<IO_Detail__c> l_IOsToUpdate = new List<IO_Detail__c>(); 		// List of IOs to be updated
        
        // Populate the Maps
        for(IO_Detail__c i:scope){
            m_OpptyToIO.put(i.Opportunity__c, i.Id);
            m_IODetail.put(i.Id, i);
        }
        
        // Get aggregate information from OpportunityLineItemSchedule
        AggregateResult[] groupedResults = [
            SELECT OpportunityLineItem.OpportunityId, Sum(Revenue) sumRev
            FROM OpportunityLineItemSchedule 
            WHERE scheduleDate = THIS_QUARTER 
            AND OpportunityLineItem.OpportunityId in :m_OpptyToIO.keySet()
            GROUP BY OpportunityLineItem.OpportunityId
        ];
        
        // Loop through AggregateResults, updating IOs as necessary, adding updated IOs to l_IOsToUpdate 
        for (AggregateResult ar : groupedResults)  {
            double sumRev = (double)ar.get('sumRev');
            Id oppId = (Id)ar.get('OpportunityId');
            Id ioId = m_OpptyToIO.get(oppId); // Get the IO for that Opportunity
            IO_Detail__c thisIO = m_IODetail.get(ioId); // Get the IO details from scope
            if(sumRev != thisIO.CQ_Forecasted_Revenue__c){
                l_IOsToUpdate.add(new IO_Detail__c(Id = thisIO.Id, CQ_Forecasted_Revenue__c = sumRev));
            }
            // remove processed IO from the collection, after all updates the remaining IOs will be zeroed out
            m_IODetail.remove(ioId); 
        }

        // Remaining IOs will be zeroed out
        for(Id remainingIO:m_IODetail.keySet()){
            l_IOsToUpdate.add(new IO_Detail__c(Id = remainingIO, CQ_Forecasted_Revenue__c = 0));
        }
        
        if(!l_IOsToUpdate.isEmpty()){
            // Wrap update in a try/catch to capture any errors...
            try{
                update l_IOsToUpdate;
            }
            catch(exception e ){
                logger.logMessage('BatchCQForcastedRevenue', 'execute', e.getMessage(), 'IO_Detail__c', '', '');
                System.debug('ERROR ======'+e.getMessage());
            }
        }
        
    }
    
    global void execute(SchedulableContext SC) {
        BatchCQForcastedRevenue cQFR = new BatchCQForcastedRevenue ();
        database.executeBatch(cQFR );
    }
    
    global void finish(Database.BatchableContext bc){}
}