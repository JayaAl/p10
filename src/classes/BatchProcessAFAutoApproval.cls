/**
* @name: BatchProcessAFAutoApproval 
 * @desc: Batch class is Used to process the Auto approval for rep submitted forecasts after the Manager cutoff date
* @author: Sridharan Subramanian
* @date: 03/16/2016
*/
global class BatchProcessAFAutoApproval implements Database.Batchable<sObject>,database.stateful,Schedulable {
    global String Query;
    global Map <String,String> AFD_error = new Map <String,String>();
    global boolean quarter1;
    global boolean quarter2;
    global boolean quarter3; 
    global boolean quarter4;
    global set<string> managerEmail;
    global string executionMode;
    global integer iMgrAFCutOff;
    global integer iMgrAFRemainder;
    global integer currentDay;
    global List<Account_Forecast_Details__c> forecastDetailsToUpdate;
   //global static final string OPEN_AUCTION = 'Open Auction';
    
    //This  list contains adx_log__c records which doesn`t have a matching dealid in opportunity
    global List<AdxLog__c> nodealid = new List<AdxLog__c>();
     
    // This query is on AdxLog__c where ADX records are stored and query to retrieve records where flag is false
    global BatchProcessAFAutoApproval() {   
        
        executionMode = '';
        Custom_Constants__c afMGRCutOff = Custom_Constants__c.getValues('Account Forecast Manager Cutoff');
        Custom_Constants__c afDate = Custom_Constants__c.getValues('Account Forecast Date');
        Custom_Constants__c afMGRRemainder = Custom_Constants__c.getValues('Forecast Remainder Cutoff');
        
         
        iMgrAFCutOff=0;
        iMgrAFRemainder=0;
        date currentDate;
        
        if(afDate!=null && (!string.isEmpty(afDate.Value__c)))
            currentDate = setStringToDateFormat(afDate.Value__c);
        else
            currentDate = system.Today();
        
        currentDay = currentDate.Day();
        integer currentMonth = currentDate.Month();
        integer currentYear = currentDate.Year();
        
        
        if(afMGRRemainder!=null && (!string.isEmpty(afMGRRemainder.Value__c)))
            iMgrAFRemainder = integer.valueOf(afMGRRemainder.Value__c);
        else
            iMgrAFRemainder = 20;
            
            
        if(afMGRCutOff!=null && (!string.isEmpty(afMGRCutOff.Value__c)))
            iMgrAFCutOff = integer.valueOf(afMGRCutOff.Value__c);
        else
            iMgrAFCutOff = 25;
        
        Query = getAFQuery(currentDay,currentMonth,currentYear);
        
        
         
        
    }
    
    /***************************************************************************************************************
        This Method used to convert the custom settings Account Forecast date to the DATE
    ****************************************************************************************************************/
    private Date setStringToDateFormat(String myDate) {
       String[] myDateOnly = myDate.split(' ');
       String[] strDate = myDateOnly[0].split('/');
       Integer myIntDate = integer.valueOf(strDate[1]);
       Integer myIntMonth = integer.valueOf(strDate[0]);
       Integer myIntYear = integer.valueOf(strDate[2]);
       Date d = Date.newInstance(myIntYear, myIntMonth, myIntDate);
       return d;
    }
    
    /***************************************************************************************************************
        This Method is used to generate the query for fetching the Account Forecast records that are Rep Submitted
    ****************************************************************************************************************/
    
    private string getAFQuery(integer currentDay,integer currentMonth,integer currentYear)
    {
        
        string status = 'Rep Submitted';
        string whereClause = '';
        string Query = '';
        
        quarter1 = false;
        quarter2 = false;
        quarter3 = false;
        quarter4 = false;
        if(currentMonth==1)
        {
            quarter1=true;
            Query +=',Q1_Status__c';
            
            whereClause += ' where Q1_Status__c= \'Rep Submitted\'';
        }
        else if(currentMonth==4)
        {
            quarter2=true;
            Query +=',Q2_Status__c';
            whereClause += ' where Q2_Status__c= \'Rep Submitted\'';
        }
        else if(currentMonth==7)
        {
            quarter3=true;
            Query +=',Q3_Status__c';
            whereClause += ' where Q3_Status__c= \'Rep Submitted\'';
        }
        else if(currentMonth==10)
        {
            quarter4=true;
            Query +=',Q4_Status__c';
            whereClause += ' where Q4_Status__c= \'Rep Submitted\'';
        }
        else
        {
            return '';
        }
        
        string soqlQuery = 'Select ID,Account_Forecast__r.Advertiser__r.Name,Account_Forecast__r.Advertiser__c,Account_Forecast__r.Manager__c,Approver_Name__c,Account_Forecast__r.Manager__r.Email,Account_Forecast__r.Manager__r.Name,Account_Forecast__r.Rep__r.Name,Account_Forecast__r.Rep__r.Email,Fiscal_Year__c  ';
        Query +=' from Account_Forecast_Details__c  ' ;
        
        Query += whereClause + ' and Fiscal_Year__c=\'' + currentYear +'\'';
        
        soqlQuery +=Query;
        return soqlQuery;
    }
   
    global Database.QueryLocator start(Database.BatchableContext bc) {
        
       system.debug(logginglevel.info,'THS IS MY WORK' + Query); 
       
       if(string.isEmpty(Query))
            Query = 'Select Account_Forecast__c from Account_Forecast_Details__c where id=null';
       
        return Database.getQueryLocator(Query) ;
       
       /*if(!string.isEmpty(query))
        {
            Map<id,Account_Forecast_Details__c> objAFD = database.query(query);
            
            customObject.Course_Version__c= 'Jim';
            update customObject;
        }*/
       
    }
    global void execute(SchedulableContext SC) {
        
        BatchProcessAFAutoApproval bpAFAutoApproval = NEW BatchProcessAFAutoApproval();
        database.executebatch(bpAFAutoApproval);
    }
    global void execute(Database.BatchableContext bc, List<sObject> scope) {
       
       
       
       executionMode = '';
       if(iMgrAFRemainder == currentDay)
       {
           executionMode = 'Remainder';
           SendRemainderEmail(scope,executionMode);
       }
       else if(iMgrAFCutOff == currentDay)
       {
           executionMode = 'Update';
           UpdateAFStatus(scope,executionMode);
       }
       
         
        
    }
    /***************************************************************************************************************
        This Method triggers emails on the following occassions
        1. Sends remainder email to the Manager to approve the rep submitted records
        
    ****************************************************************************************************************/
    private void SendRemainderEmail(List<sObject> scope,string executionMode)
    {
        
        try{
            if(scope.size()>0)
                sendEmail((List<Account_Forecast_Details__c>)scope,executionMode);
        }
        catch(exception e ){
            logger.logMessage('BatchProcessAFAutoApproval', 'execute', e.getMessage(), 'Account_Forecast__r', '', '');
            System.debug('ERROR ======'+e.getMessage());
        }
        
        
    }
    
    
    /*****************************************************************************************************************************
        This Method triggers Auto Approvals and sends emails on the following occassions
        1. On the manager cutoff day if there are any records in the Account Forecast object as rep submitted the auto approves it
        
        2. Sends email to the Manager and Rep that the records are auto approved
    ********************************************************************************************************************************/
    private void UpdateAFStatus(List<sObject> scope, string executionMode)
    {
        managerEmail = new set<string>();
        forecastDetailsToUpdate = new List<Account_Forecast_Details__c>();
       
        for(Account_Forecast_Details__c a : (List<Account_Forecast_Details__c>)scope){ 
           
           system.debug(logginglevel.info, 'THIS IS AFD ' + a);
           system.debug(logginglevel.info, 'THIS IS AFD manager  ' + a.Account_Forecast__r.Manager__c);
           system.debug(logginglevel.info, 'THIS IS AFD af' + a.Account_Forecast__c);
           system.debug(logginglevel.info, 'THIS IS AFD email' + a.Account_Forecast__r.Manager__r.Email);
           
           if(quarter1){
               a.Q1_Status__c = 'Auto Approved';
               
           }
           if(quarter2){
               a.Q2_Status__c = 'Auto Approved';
               
           }
           if(quarter3){
               a.Q3_Status__c = 'Auto Approved';
               
           }
           if(quarter4){
               a.Q4_Status__c = 'Auto Approved';
               
           }
           a.Approver_Name__c = a.Account_Forecast__r.Manager__c;
        
           managerEmail.add(a.Account_Forecast__r.Manager__r.Email);
           forecastDetailsToUpdate.add(a);
           
        }
        try{
            if(forecastDetailsToUpdate.size()>0)
                update forecastDetailsToUpdate; 
        }
        catch(exception e ){
            logger.logMessage('BatchProcessAFAutoApproval', 'execute', e.getMessage(), 'Account_Forecast__r', '', '');
            System.debug('ERROR ======'+e.getMessage());
        }
        
        system.debug(logginglevel.info, 'THIS IS AFD managerEmail list' + managerEmail);
        
        if(forecastDetailsToUpdate.size()>0)
            sendEmail(forecastDetailsToUpdate,executionMode);
    }
    
    
    
    global void finish(Database.BatchableContext bc) {
        
  
           // Get the ID of the AsyncApexJob representing this batch job
           // from Database.BatchableContext.
           // Query the AsyncApexJob object to retrieve the current job's information.
           AsyncApexJob a = [SELECT Id, Status, NumberOfErrors, JobItemsProcessed,
              TotalJobItems, CreatedBy.Email
              FROM AsyncApexJob WHERE Id =
              :BC.getJobId()];
           // Send an email to the Apex job's submitter notifying of job completion.
           Messaging.SingleEmailMessage mail = new Messaging.SingleEmailMessage();
           String[] toAddresses = new String[] {'ssubramanian@pandora.com'};
           mail.setToAddresses(toAddresses);
           mail.setSubject('Apex Sharing Recalculation ' + a.Status);
           mail.setPlainTextBody
           ('The batch Apex job processed ' + a.TotalJobItems +
           ' batches with '+ a.NumberOfErrors + ' failures.');
           Messaging.sendEmail(new Messaging.SingleEmailMessage[] { mail });

    } 
    /*************************************************************************************************************************
        This Method triggers emails on the following occassions
        1. Sends remainder email to the Manager as per remainder cutoff date and if there are records in rep submitted status
        2. Sends email to the Rep when the Manager approves the forecast
    **************************************************************************************************************************/
    private void SendEmail(List<Account_Forecast_Details__c> forecastDetailsToEmail, string executionMode)
    {
        try
        {
            
            Map<id,Account_Forecast__c> accountForecastMap = new Map<id,Account_Forecast__c>();
            List<Messaging.SingleEmailMessage> mails = new List<Messaging.SingleEmailMessage>();
            
            
            
            Map<id,Report> reportsMap = new Map<id,report>([select id,Name from Report where name='Account Forecast Details By Rep' or Name='Account Forecast Details By Account']);
            
            id reportId;
            for(report rpt: reportsMap.values())
            {
                if(rpt.Name == 'Account Forecast Details By Rep')
                    reportId = rpt.Id;
            }
            
            
            set<string> repEmail = new set<string>();
            for(Account_Forecast_Details__c af: forecastDetailsToEmail)
            {
                system.debug(logginglevel.info, 'THIS IS repEmail' + repEmail.size() + '----' + (!repEmail.contains(af.Account_Forecast__r.Rep__r.email)));
                    
                if(repEmail.size()==0 || (!repEmail.contains(af.Account_Forecast__r.Rep__r.email)))
                {
                    repEmail.add(af.Account_Forecast__r.Rep__r.email);
                    system.debug(logginglevel.info, 'THIS IS AFD accountForecastMap accountForecastMap' + af);
                    
                    
                    Messaging.SingleEmailMessage mail = new Messaging.SingleEmailMessage();
                    string messageBody = '';
                    string subject ='';
                    
                     if(executionMode=='Remainder')
                    {
                        subject = 'Pending Review - Account forecasts has been submitted by ' + af.Account_Forecast__r.Rep__r.Name  + ' as on ' + system.today().format();
                        
                        messageBody = '<html><body>Hi ' + af.Account_Forecast__r.Manager__r.Name + ', <br> <br> <p> Accounts forecasts for the current quarter submitted by ' + af.Account_Forecast__r.Rep__r.Name  + ' are pending review, please approve the forecasts by the 25th of the month. Post this date the forecasts will be auto-approved by the system and will no longer be editable. ';
                        messageBody += '<br> <br> Please <a href="'+ URL.getSalesforceBaseUrl().toExternalForm() +'/'+reportId + '?pv1='+ af.Account_Forecast__r.Rep__r.Name +'"> click here </a> to find the submitted forecast details as a report. To review and update the forecasts you can use the ‘Manage Accounts’ tab from your Salesforce UI. </p> <br><br>Thanks! <br>System administrator</body></html>';
                    
                    }
                    else
                    {
                        subject = 'Account forecasts has been Auto Approved for ' + af.Account_Forecast__r.Rep__r.Name  + ' as on ' + system.today().format();
                        
                        messageBody = '<html><body>Hi!, <br> <br> <p> Accounts forecasts for the current quarter have been Auto Approved for ' + af.Account_Forecast__r.Rep__r.Name  ;
                        messageBody += '<br> <br> Please <a href="'+ URL.getSalesforceBaseUrl().toExternalForm() +'/'+reportId + '?pv1='+ af.Account_Forecast__r.Rep__r.Name +'"> click here </a> to find the submitted forecast details as a report. To view the forecasts you can also use the ‘Manage Accounts’ tab from your Salesforce UI. </p> <br><br>Thanks! <br>System administrator</body></html>';
                    
                    } 
                    
                    mail.setSubject(subject);
                    
                    mail.setHtmlBody(messageBody);
                    
                    mail.setToAddresses(new String[] { af.Account_Forecast__r.Manager__r.Email,af.Account_Forecast__r.Rep__r.Email });
                    
                    mail.setTargetObjectId(af.Account_Forecast__r.Manager__r.Id); 
                    mail.setSaveAsActivity(false);
                    mail.setBccSender(false); 
                    mail.setUseSignature(false); 
                    mails.add(mail);
                }
            }
            
            
            //allmsg.add(mail);
            
            Integer emailbefore = Limits.getEmailInvocations();
            
            
            
            
            Messaging.SendEmailResult [] r = Messaging.sendEmail(mails); 
            
            
        }
        catch(exception e)
        {
            //throw new Exception();
            //ApexPages.addmessage(new Apexpages.Message(ApexPages.Severity.Error,ex.getMessage() + + '---' + ex.getStacktracestring()));
            //return null;
            logger.logMessage('BatchProcessAFAutoApproval', 'execute', e.getMessage(), 'Account_Forecast_Details__c', '', '');
            System.debug('ERROR ======'+e.getMessage());
        }
        
         
                
    }
    
    
}