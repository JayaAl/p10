/**
* @name: BatchProcessingDealsToOpp 
* @desc: Batch class is Used to process the deal data on AdxLog__c and add as lineitems on Opportunity
* @author: Priyanka Ghanta
* @date: 05/11/2015
*/
global class BatchProcessingDealsToOpp implements Database.Batchable<sObject>,database.stateful {
    global String Query;
    global Map <String,String> oppLine_error = new Map <String,String>();
    global static final string OPPORTUNITY_RECORDTYPE = 'Opportunity - Programmatic';
    //global static final string OPEN_AUCTION = 'Open Auction';
    
    //This  list contains adx_log__c records which doesn`t have a matching dealid in opportunity
    global List<AdxLog__c> nodealid = new List<AdxLog__c>();
    
    // This query is on AdxLog__c where ADX records are stored and query to retrieve records where flag is false
    global BatchProcessingDealsToOpp() {   
        Query = 'Select Start_Date__c, Ad_impressions__c, AdxTag__c, Line_Created__c, Deal_ID__c, End_Date__c, Estimated_Revenue__c,LastModifiedDate from AdxLog__c where LastModifiedDate = TODAY AND Line_Created__c = FALSE';
    }
    
    
    global Database.QueryLocator start(Database.BatchableContext bc) {
        
        return Database.getQueryLocator(Query) ;
        
    }
    
    global void execute(Database.BatchableContext bc, List<sObject> scope) {
        List<AdxLog__c> adxList = (List<AdxLog__c>)scope;
        //system.debug('...............ADx Listn :'+adxList);
        Set <String> dealIdSet = new Set <String> ();
        Map <String, Opportunity> oppMap = new Map <String, Opportunity> ();
        List<OpportunityLineItem> oppLineList = new List<OpportunityLineItem>();
        List <AdxLog__c> adxUpd = new List <AdxLog__c> ();
        Map<String,String> mapofProductNameandPricebookEntryId = new   Map<String,String>();  
        Map<String,ProductMapWrapper> mapofAdxTagandProductmapWrapper = new Map<String,ProductMapWrapper>();
        Map<String,BatchProcDeals__c> mapofAdxTagandSetting = new  Map<String,BatchProcDeals__c>();
        Set<String> setofUniqueDealIds = new Set<String>();
        
        
        
        Set<String> setofSfdcProducts = new Set<String>();
        //getting all custom setting mapping
        Map<String,BatchProcDeals__c> mapofSetting = BatchProcDeals__c.getAll();
        for(String key : mapofSetting.keyset()){
            BatchProcDeals__c mappingRecord =  mapofSetting.get(key);
            if(mappingRecord.SFDC_Products__c <> null){
                setofSfdcProducts.add(mappingRecord.SFDC_Products__c);
            }
            mapofAdxTagandSetting.put(mappingRecord.AdX_Tags__c,mappingRecord);
        }
        
        system.debug('**mapofAdxTagandSetting***'+mapofAdxTagandSetting);
        Map<String,List<AdxLog__c>> mapofdealidandlistofadxrecords = new  Map<String,List<AdxLog__c>>();
        
        for(AdxLog__c a : adxList){
            if(!mapofdealidandlistofadxrecords.containsKey(a.Deal_ID__c)){
                List<AdxLog__c> lisoflogs =new List<AdxLog__c>();
                lisoflogs.add(a);
                mapofdealidandlistofadxrecords.put(a.Deal_ID__c,lisoflogs);
            } else{
                List<AdxLog__c> lisoflogs =new List<AdxLog__c>();
                lisoflogs = mapofdealidandlistofadxrecords.get(a.Deal_ID__c);
                lisoflogs.add(a);
                mapofdealidandlistofadxrecords.put(a.Deal_ID__c,lisoflogs);
                
            }
            
            //dealIdSet.add(a.Deal_ID__c.trim());
        }  
        
        //If we have dealids  
        if(!mapofdealidandlistofadxrecords.keyset().isEmpty()){
            for (Opportunity opp: [SELECT Name, Id, Deal_Id__c 
                                   FROM Opportunity
                                   WHERE Recordtype.name =: BatchProcessingDealsToOpp.OPPORTUNITY_RECORDTYPE
                                   AND Deal_ID__c IN: mapofdealidandlistofadxrecords.keyset()]) { 
                                       
                                       oppMap.put(opp.Deal_Id__c.trim(), opp);
                                       
                                   } 
            
            System.debug('%%%%%%%%%%'+oppMap);
            System.debug('%%%%%setofSfdcProducts  %%%%%'+setofSfdcProducts);
            for(PricebookEntry priceBookEntry : [SELECT Id,Name 
                                                 FROM PricebookEntry
                                                 WHERE Name IN: setofSfdcProducts  and isActive= true AND CurrencyIsoCode='USD' AND Pricebook2.Name = 'Standard Price Book']){
                                                     mapofProductNameandPricebookEntryId.put(priceBookEntry.Name,priceBookEntry.id);
                                                     system.debug('***mapofProductNameandPricebookEntryId*'+mapofProductNameandPricebookEntryId);                
                                                 }
        }
        
        
        
        /*  Map<String,List<ProductMapWrapper>> mapofDealIdandListProductWrapper = new   Map<String,List<ProductMapWrapper>>();
Set<String> setofuniquetags ;

for(String dealId : mapofdealidandlistofadxrecords.keyset()){
List<ProductMapWrapper> productWrapperList = new List<ProductMapWrapper>();

} */
        
        // system.debug('**mapofPerormanceTypeandProductWrapper**'+mapofPerormanceTypeandProductWrapper);
        set<String> setofuniquetags = new set<String>();
        // Create a  product for every line item in ADXlog which is extracted from ADX server
        for(AdxLog__c adxLog : adxList){
            //line item/opp product instance for each deal
            OpportunityLineItem oppLine = new OpportunityLineItem();
            
            oppLine.servicedate = adxLog.Start_Date__c;
            oppLine.end_date__c = adxLog.End_Date__c;
            //oppLine.Impression_s__c= a.Ad_impressions__c;
            oppLine.Quantity= 1;
            oppLine.Duration__c = 1;
            oppLine.unitprice = adxLog.Estimated_Revenue__c; 
            System.debug('>>>> mapofAdxTagandSetting: '+mapofAdxTagandSetting);    
            System.debug('>>>> adxLog.Deal_id__c: '+adxLog.Deal_id__c);
            System.debug('>>>> adxLog.AdxTag__c: '+adxLog.AdxTag__c);                 
            //if -_Id is not open auction then a product is created under opportunity which matches with adx_deal_Id
            if(adxLog.Deal_id__c <> null && adxLog.Deal_id__c <> 'Open Auction' && adxLog.AdxTag__c <> null && mapofAdxTagandSetting.containsKey(adxLog.AdxTag__c)){
                
                // Get value template record from mapofAdxTagandSetting based on AdxTag__c
                BatchProcDeals__c bpd = mapofAdxTagandSetting.get(adxLog.AdxTag__c);
                
                if(oppMap.get(adxLog.Deal_ID__c) <> null){
                    oppLine.OpportunityId = oppMap.get(adxLog.Deal_ID__c).Id;
                    
                    // populate all values from the BatchProcDeals__c record
                    oppLine.performance_type__c = bpd.Performance_Type__c;
                    oppLine.Medium__c = bpd.Medium__c;
                    oppLine.Offering_Type__c = bpd.Offering_Type__c;
                    oppLine.Performance_Type__c = bpd.Performance_Type__c;
                    oppLine.Platform__c = bpd.Platform__c;
                    oppLine.Size__c = bpd.Size__c;
                    oppLine.Sub_Platform__c = bpd.Sub_Platform__c;
                    
                    //PRICEBOOK ENTRY
                    if(bpd.SFDC_Products__c<> null){
                        oppLine.PricebookEntryId = mapofProductNameandPricebookEntryId.get(bpd.SFDC_Products__c);
                    }
                    
                    oppLine.Impression_s__c = adxLog.Ad_impressions__c;
                    oppLine.unitprice = adxLog.Estimated_Revenue__c;
                    if(oppLine.unitprice !=null && oppLine.Impression_s__c !=null){    
                        oppLine.eCPM__c = (oppLine.unitprice/oppLine.Impression_s__c) * 1000;
                    }
                    //opup.HasSchedule=true;                        
                    
                    oppLineList.add(oppLine);
                    //setofuniquetags.add(adxLog.AdxTag__c);
                    //setofUniqueDealIds.add(adxLog.Deal_id__c.trim());
                    
                    adxLog.Line_Created__c = True;
                    adxUpd.add(adxLog);
                    //check through each setting record and compare
                } else if(oppMap.get(adxLog.Deal_ID__c) == null){
                    nodealid.add(adxLog);
                }
            } else {
                adxLog.Line_Created__c = False;
                nodealid.add(adxLog);               
            }
        }//End : for loop
        
        Map<String,List<OpportunityLineItem>> mapofoppIdandlistfopplines = new    Map<String,List<OpportunityLineItem>>(); 
        Map<String,OpportunityLineItem> mapofperformancetypeandlineitem = new Map<String,OpportunityLineItem>();
        
        //Processing rollup                  
        for(OpportunityLineItem oppline : oppLineList){
            if(!mapofoppIdandlistfopplines.containsKey(oppline.OpportunityId)){
                List<OpportunityLineItem> listoflineitems = new List<OpportunityLineItem>();
                listoflineitems.add(oppline);
                mapofoppIdandlistfopplines.put(oppline.OpportunityId,listoflineitems);
            }else{
                List<OpportunityLineItem> listoflineitems = new List<OpportunityLineItem>();
                listoflineitems = mapofoppIdandlistfopplines.get(oppline.OpportunityId);
                listoflineitems.add(oppline);
                mapofoppIdandlistfopplines.put(oppline.OpportunityId,listoflineitems);
            }
        }
        
        List<OpportunityLineItem> listoflineitemstobeinserted = new List<OpportunityLineItem>();
        Map<String, Map<String,OpportunityLineItem>> mapofoppidandperformancemap = new Map<String, Map<String,OpportunityLineItem>>();
        
        for(String oppid : mapofoppIdandlistfopplines.keyset()){
            mapofperformancetypeandlineitem = new Map<String,OpportunityLineItem>();
            //lineitems of each opp
            for(OpportunityLineItem lineItem : mapofoppIdandlistfopplines.get(oppid)) {
                
                if(lineItem.performance_type__c <> null && !mapofperformancetypeandlineitem.containsKey(lineItem.performance_type__c )){
                    mapofperformancetypeandlineitem.put(lineItem.performance_type__c ,lineItem);
                }else{
                    OpportunityLineItem oldItem = mapofperformancetypeandlineitem.get(lineItem.performance_type__c);
                    oldItem.Impression_s__c += lineItem.Impression_s__c;
                    oldItem.unitprice += lineItem.unitprice;
                    mapofperformancetypeandlineitem.put(lineItem.performance_type__c ,oldItem);
                }
            }
            listoflineitemstobeinserted.addall(mapofperformancetypeandlineitem.values());
        }
        
        system.debug('**oppLineList*'+listoflineitemstobeinserted);
        //If OpportunitylineItem is empty and if record fails we get a message for failure records.
        if(!listoflineitemstobeinserted.isEmpty()) {   
            OPT_PROD_STATIC_HELPER.runForProds = true;//Added by Lakshman on 06-11-2015 to initiate Schedules
            Database.SaveResult[] SaveResults = Database.insert (listoflineitemstobeinserted,false);
            for (Database.SaveResult sr : SaveResults) {
                if(!sr.isSuccess()) {
                    oppLine_error.put(sr.getId(),sr.getErrors()[0].getMessage());
                    System.debug(sr.getErrors()[0].getMessage());
                }
            }
        }
        //If AdxLog is empty and record fails we will get error message.
        if (!adxUpd.isEmpty()) {
            Database.SaveResult[] SaveResults = Database.update (adxUpd,false);
            for (Database.SaveResult sr : SaveResults) {
                if(!sr.isSuccess()) {
                    oppLine_error.put(sr.getId(),sr.getErrors()[0].getMessage());
                    System.debug(sr.getErrors()[0].getMessage());
                }
            }
        }
        //If there is no error on opportunity Line item send a mail to job scheduler
        if (oppLine_error !=null && !oppLine_error.isEmpty()) {
            AsyncApexJob aaj = [Select Id, Status, CreatedBy.Email from AsyncApexJob where Id = :BC.getJobId()];  
            Messaging.SingleEmailMessage mail = new Messaging.SingleEmailMessage();
            system.debug('==================================Mail sent to : ' + aaj.CreatedBy.Email); 
            String[] toAddresses = new String[] {aaj.CreatedBy.Email};
                mail.setToAddresses(toAddresses);
            mail.setSubject('ADX Processing FAILURES' + aaj.Status);
            mail.setPlainTextBody('Errors: ' + oppLine_error);
            Messaging.sendEmail(new Messaging.SingleEmailMessage[] { mail });
        }
        
        
    }
    // Send an email to the Apex job's submitter notifying of job completion. 
    global void finish(Database.BatchableContext bc) {
        String breakrec;
        //If there are ADX records without matching dealid in opportunities then a mail is sent to Job Scheduler
        if (!nodealid.isEmpty()) {
            System.debug('unmappred adx records========'+nodealid);
            AsyncApexJob aaj = [Select Id, Status, CreatedBy.Email from AsyncApexJob where Id = :BC.getJobId()];  
            Messaging.SingleEmailMessage mail = new Messaging.SingleEmailMessage();
            String[] toAddresses = new String[] {aaj.CreatedBy.Email};
                mail.setToAddresses(toAddresses);
            mail.setSubject('Unmapped ADX records');
            mail.setPlainTextBody('List of records : '+ nodealid);
            for(AdxLog__c a : nodealid){
                breakrec += '<tr><td>'+a.Deal_ID__c+'</td>'+'<td>'+a.Start_Date__c+'</td>'+'<td>'+a.End_Date__c+'</td>'+'<td>'+a.AdxTag__c+'</td>'+'<td>'+a.Ad_impressions__c+'</td>'+'<td>'+a.Estimated_Revenue__c+'</td>'+'</tr>';
            }
            System.Debug('..................................'+nodealid);
            breakrec = breakrec.replace('null' , '') ;
            String finalStr = '' ;
            finalStr = '<html><body><head><style>table, th, td {border: 1px solid black;border-collapse: collapse;}</style></head></body><table style="width:100%"><td> Deal Id </td> <td> Start Date </td><td> End Date </td> <td> AdxTag </td> <td> Ad Impressions </td> <td> Estimated Revenue </td> ' + breakrec +'</table>' ;
            mail.setHtmlBody(finalstr);
            Messaging.sendEmail(new Messaging.SingleEmailMessage[] { mail });
        }
        AsyncApexJob aaj = [Select Id, Status, NumberOfErrors, JobItemsProcessed, MethodName, TotalJobItems, CreatedBy.Email
                            from AsyncApexJob where Id = :BC.getJobId()];     
        Messaging.SingleEmailMessage mail = new Messaging.SingleEmailMessage();
        String[] toAddresses = new String[] {aaj.CreatedBy.Email};
        mail.setToAddresses(toAddresses);
        mail.setSubject('Processing Finished' + aaj.Status);
        mail.setPlainTextBody('Errors: ' + oppLine_error);
        // Messaging.sendEmail(new Messaging.SingleEmailMessage[] { mail }); 
    } 
    
    global class ProductMapWrapper{
        String sfdcProduct;
        Decimal totalImpression;
        Decimal totalRevenue;
        String parentoppId;
        String performaceType;
    }
}