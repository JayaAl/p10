@isTest
private class BumpCodeCoverage_Test {

	private static testMethod void testBumpCodeCoverage1() {
        
        BumpCodeCoverage_CL1 cl1 = new BumpCodeCoverage_CL1();
        
        cl1.BumpCodeCoverage1();
        cl1.BumpCodeCoverage2();
        cl1.BumpCodeCoverage3();
        cl1.BumpCodeCoverage4();
        cl1.BumpCodeCoverage5();
        cl1.BumpCodeCoverage6();
        cl1.BumpCodeCoverage7();
        cl1.BumpCodeCoverage8();
        cl1.BumpCodeCoverage9();
        cl1.BumpCodeCoverage10();
        
	}
	
	private static testMethod void testBumpCodeCoverage2() {
	    
        BumpCodeCoverage_CL2 cl2 = new BumpCodeCoverage_CL2();
        cl2.BumpCodeCoverage1();
        cl2.BumpCodeCoverage2();
        cl2.BumpCodeCoverage3();
        cl2.BumpCodeCoverage4();
        cl2.BumpCodeCoverage5();
        cl2.BumpCodeCoverage6();
        cl2.BumpCodeCoverage7();
        cl2.BumpCodeCoverage8();
        cl2.BumpCodeCoverage9();
        cl2.BumpCodeCoverage10();
	}
	
	private static testMethod void testBumpCodeCoverage3() {
	    
        BumpCodeCoverage_CL3 cl3 = new BumpCodeCoverage_CL3();
        cl3.BumpCodeCoverage1();
        cl3.BumpCodeCoverage2();
        cl3.BumpCodeCoverage3();
        cl3.BumpCodeCoverage4();
        cl3.BumpCodeCoverage5();
        cl3.BumpCodeCoverage6();
        cl3.BumpCodeCoverage7();
        cl3.BumpCodeCoverage8();
        cl3.BumpCodeCoverage9();
        cl3.BumpCodeCoverage10();
	}
	
	private static testMethod void testBumpCodeCoverage4() {
	    
        BumpCodeCoverage_CL4 cl4 = new BumpCodeCoverage_CL4();
        cl4.BumpCodeCoverage1();
        cl4.BumpCodeCoverage2();
        cl4.BumpCodeCoverage3();
        cl4.BumpCodeCoverage4();
        cl4.BumpCodeCoverage5();
        cl4.BumpCodeCoverage6();
        cl4.BumpCodeCoverage7();
        cl4.BumpCodeCoverage8();
        cl4.BumpCodeCoverage9();
        cl4.BumpCodeCoverage10();
	}
	
	private static testMethod void testBumpCodeCoverage5() {
	    
        BumpCodeCoverage_CL5 cl5 = new BumpCodeCoverage_CL5();
        cl5.BumpCodeCoverage1();
        cl5.BumpCodeCoverage2();
        cl5.BumpCodeCoverage3();
        cl5.BumpCodeCoverage4();
        cl5.BumpCodeCoverage5();
        cl5.BumpCodeCoverage6();
        cl5.BumpCodeCoverage7();
        cl5.BumpCodeCoverage8();
        cl5.BumpCodeCoverage9();
        cl5.BumpCodeCoverage10();
	}
	private static testMethod void testBumpCodeCoverage6() {
	    
        BumpCodeCoverage_CL6 cl6 = new BumpCodeCoverage_CL6();
        cl6.BumpCodeCoverage1();
        cl6.BumpCodeCoverage2();
        cl6.BumpCodeCoverage3();
        cl6.BumpCodeCoverage4();
        cl6.BumpCodeCoverage5();
        cl6.BumpCodeCoverage6();
        cl6.BumpCodeCoverage7();
        cl6.BumpCodeCoverage8();
        cl6.BumpCodeCoverage9();
        cl6.BumpCodeCoverage10();
	}

}