/***************************************************
   Name: CAMP_CreateAssetController
   Usage: This Class creates the Asset Records
   Author – Clear Task
   Date – 11/17/2011   
   Revision History
******************************************************/
public with sharing class CAMP_CreateAssetController {

    /* caseObj - for case Record*/
    public Case caseObj{get; set;}
    /* assetObj - for Asset__c Record*/
    public Asset__c assetObj{get; set;}
    /* for render Additional video section*/
    public Boolean flag{get; set;}
    /* for render Show fields when Legal Role section*/
    public Boolean roleFlag{get; set;}
    public Boolean legalFlag{get; set;}
    /* for render Show location as Link*/
    public Boolean locationflag{get; set;}
    /* Variable - Creative*/
    String CREATIVE = 'Creative';
    /* Variable - Video*/
    String VIDEO = 'Video';
    /* Variable - Mixtape*/
    String MIXTAPE = 'Mixtape';
    /* Variable - Audio*/
    String AUDIO = 'Audio';
    /* Variable - In Progress*/
    String IN_PROGRESS = 'In Progress';    
    private Integer index{ get; private set; } 
    /* List of ConditionRow class*/
    public List<ConditionRow> conditions { get; private set; } 
    /* nextId  variable for increment*/
    private Integer nextId = 0; 
    
    
       
    /* CAMP_CreateAssetController - Constructor for default values */
    public CAMP_CreateAssetController(ApexPages.StandardController stdController){
        assetObj = new  Asset__c(Case__c = stdController.getId());  
        locationflag = false; 
        caseObj = (Case)stdController.getRecord();  
        /*
        caseObj  = [Select c.Status, c.Sales_Planner__c, c.Video_Hub_URL__c, c.RecordType.Name, c.Record_Type__c, c.OwnerId, c.Opportunity__c, c.Opportunity_Owner__c, c.Launch_Date__c, c.Initial_Asset_Expiration_Date__c, c.Id, c.CaseNumber,
        c.Artist_s_Event__c,c.Crew_name_and_in_order__c, c.Filming_Locations__c, c.Third_Party_Material__c, c.Music_1st_source_description__c, c.Other_Info_Red_Flags__c, c.Video_Series_Title__c,c.Source_of_Video__c,c.Feature_Appearances_1st_in_order__c, c.Approver__c, c.Opportunity__r.Sales_Planner__c, c.Opportunity__r.ContractStartDate__c, c.Opportunity__r.AccountId, c.Opportunity__r.OwnerId,  c.AccountId From Case c where Id =:caseObj.id];  
        */
        conditions = new List<ConditionRow>();  
        
        User u = [Select UserRole.Name, UserRoleId, Profile.Name From User where id = :UserInfo.getUserId()]; 
        if(u.UserRole.Name != null && u.UserRole.Name.equals('Legal')){
            legalFlag = true;
        }else{
            legalFlag = false;
        }
        
        if(u.UserRole.Name != null 
                && (u.UserRole.Name.equals('Artist Partnership & Programming')
                    || u.UserRole.Name.equals('Client Services Central')
                    || u.UserRole.Name.equals('Client Services Analysts East')
                    || u.UserRole.Name.equals('Client Services Inside Sales')
                    || u.UserRole.Name.equals('Client Services West')
                    || u.Profile.Name.equals('Sales - Account Service Coordinator')
                    || u.Profile.Name.equals('Sales - Client Services User'))){
            roleFlag = true;
        }else{
            roleFlag = false;
        }            
    }
    
    /* onAddCondition method for adding new row on page */
    public PageReference onAddCondition() {
        index = conditions.size()+ 1;
        Asset__c   assetRecord = new Asset__c(
                Start_date__c = caseObj.Launch_date__c,
                End_date__c = caseObj.Initial_Asset_Expiration_Date__c,
                Asset_retired__c = 'No',
                Legally_Approved__c = 'No',
                Case__c = caseObj.Id
            );
        conditions.add(new ConditionRow(String.valueOf(index),assetRecord));
        return null;
    }
    
    /* onRemoveCondition method for remove  row from page */
    public PageReference onRemoveCondition() {
        flag = false;
        pollerEnabled = true;
        String selectedId = ApexPages.currentPage().getParameters().get('selectedId');
        System.debug('selectedId = ' + selectedId);
        if (selectedId != null) {
            Asset__c asstDelete;
            Integer selectedIndex = -1;
            for (Integer i=0;i<conditions.size();i++) {
                ConditionRow row = conditions.get(i);
                if (row.getId().equals(selectedId)) {
                    asstDelete = row.condition;
                    selectedIndex = i;
                    break;
                }
            }
            System.debug('selectedIndex = ' + selectedIndex);
            System.debug('conditions = ' + conditions.get(selectedIndex).condition);
            if (selectedIndex >= 0) conditions.remove(selectedIndex);
                
            for (ConditionRow row : conditions) {
                if(row.condition.Asset_Type__c != null){
                    flag = row.condition.Asset_Type__c.equals(VIDEO) || flag;
                }
                System.debug('row = ' + row.condition.Id);
            }
            
            try{
                System.debug('----asstDelete' + asstDelete);
                if(asstDelete != null && asstDelete.id != null){
                    delete asstDelete;
                }
            }catch(Exception e){
                System.debug('In Catch'+e.getMessage());
                
            }
        }
        ApexPages.Message success = new ApexPages.Message(ApexPages.SEVERITY.Confirm, 'Asset was deleted');
        ApexPages.addMessage(success);
        return null;
    }
    
    /* onRemoveCondition method for remove  row from page */
    public PageReference onCloneCondition() {
      try {
        flag = false;
        String selectedId = ApexPages.currentPage().getParameters().get('selectedId');
        if (selectedId != null) {
            index = conditions.size()+ 1;
            Asset__c assetRecord;
            
            for (Integer i=0;i<conditions.size();i++) {
                ConditionRow row = conditions.get(i);
                if (row.getId().equals(selectedId)) {
                    assetRecord = new Asset__c(
                        Start_date__c = row.condition.Start_date__c,
                        End_date__c = row.condition.End_date__c,
                        Asset_retired__c = 'No',
                        Legally_Approved__c = 'No',
                        Case__c = row.condition.Case__c,
                        Asset_Type__c = row.condition.Asset_Type__c,
                        Deliverable__c = row.condition.Deliverable__c,
                        Document_in_Place__c = row.condition.Document_in_Place__c,
                        Source_Type__c = row.condition.Source_Type__c,
                        Element_Descriptiion__c = row.condition.Element_Descriptiion__c,
                        Legal_Restrictions__c = null
                        
                    );
                    break;
                }
            }
            conditions.add(new ConditionRow(String.valueOf(index), assetRecord));
            
            for (ConditionRow row : conditions) {
                if(row.condition.Asset_Type__c != null){
                    flag = row.condition.Asset_Type__c.equals(VIDEO) || flag;
                }
            }
        }
      } catch(Exception e) {
          ApexPages.Message msg = new ApexPages.Message(ApexPages.Severity.INFO, e.getMessage());
          ApexPages.addMessage(msg);
      }
        return null;
    }
    
    /* onChangeType - Asset_Type__c = Video Show the section*/
    public void onChangeType() {
        flag = false;
        String selectedId = ApexPages.currentPage().getParameters().get('selectedId');
        if (selectedId != null) {
            for (Integer i = 0; i < conditions.size(); i++) {
                ConditionRow c = conditions.get(i);
                if (c.getId().equals(selectedId)) {
                    if(c.condition.Asset_Type__c != null){
                        if(c.condition.Asset_Type__c.equals(CREATIVE)){
                            c.condition.Element_Descriptiion__c = 'Artist Name, Images and Artwork';
                        }else if(c.condition.Asset_Type__c.equals(MIXTAPE)){
                            c.condition.Element_Descriptiion__c = 'Song List';  
                        }else if(c.condition.Asset_Type__c.equals(AUDIO)){
                            c.condition.Element_Descriptiion__c = 'Voice';
                        }else{
                            c.condition.Element_Descriptiion__c = 'Video';
                        }
                       /* c.condition.Element_Descriptiion__c = c.condition.Asset_Type__c;*/ 
                    }
                }
                if(c.condition.Asset_Type__c != null){
                    flag = c.condition.Asset_Type__c.equals(VIDEO) || flag;
                }
            }
            System.debug('---------conditions2' + conditions);
        } 
    }   
    
   public Boolean pollerEnabled {get; set;}
   public Pagereference resetPoller(){
        pollerEnabled = false;
        return null;
   }    
    
   /* save - upsert all Asset records*/   
   public PageReference save(){
        pollerEnabled = true;
        if(caseObj != null){
            Integer count =0;
            Map<String,List<Asset__c>> assetMap = new Map<String,List<Asset__c>>();
            List<Asset__c> assetList = new List<Asset__c>();
            List<Asset__c> assetArrangedlist = new List<Asset__c>();
            Set<Id> assIds = new  Set<Id>();
            //Boolean isRetired = false;
            for(ConditionRow c :conditions){ 
                //System.debug('------c===' + c);
                if (c.condition.Asset_type__c == null) continue;
                //c.condition.Case__c = caseObj.Id;
                c.condition.Asset_Location__c = assetObj.Asset_Location__c; 
                //System.debug('-----c' + c);
                assetList.add(c.condition); 
                //System.debug('assetList' + assetList);
                
                //isRetired = c.condition.Asset_Retired__c.equals('No') || isRetired;
                
            }
            
            /*if(isRetired && caseObj.Status != null && caseObj.Status.equals('Closed')){
                ApexPages.Message msg = new ApexPages.Message(ApexPages.Severity.ERROR, 'Validation: All assets must be retired before the case status can be closed.');
                ApexPages.addMessage(msg);
                return null;
            }*/
            
            try{
                
                if(assetList != null && assetList.size()>0){
                    upsert assetList;                     
               }
                
                if(flag != null && flag){
                    upsert caseObj;                    
                }
                
            }catch(Exception e){
                System.debug('In Catch'+e.getMessage());
                ApexPages.Message msg = new ApexPages.Message(ApexPages.Severity.INFO, e.getMessage());
                ApexPages.addMessage(msg);
                pollerEnabled = false;
                
                return null; 
            }
           
            assetArrangedlist  = arrangeAssetList();
            conditions = new List<ConditionRow>();  
            for(Asset__c  a: assetArrangedlist) {
                count++;
                System.debug('a'+a);
                a.Asset_Location__c = assetObj.Asset_Location__c; 
                conditions.add(new ConditionRow(String.valueOf(count), a));
                System.debug('conditions ='+conditions + 'Condition Size='+conditions.size());
            }
            System.debug('assetTemplist' + assetArrangedlist);
        }
        //PageReference pg = new PageReference('/'+caseObj.id);
        //pg.setRedirect(true);
        ApexPages.Message success = new ApexPages.Message(ApexPages.SEVERITY.Confirm, 'Assets saved');
        ApexPages.addMessage(success);
        return null; 
    }
    
    public List<Asset__c> arrangeAssetList(){
        List<Asset__c> assetTemplist = new List<Asset__c>();
        Map<String,List<Asset__c>> assetMap = new Map<String,List<Asset__c>>();
        List<Asset__c> tempList = new List<Asset__c>();
        List<Asset__c> assetRecordslist = [select id, name,Asset_Location__c,
                    Asset_Retired__c,Asset_Type__c,   Deliverable__c,Document_in_Place__c,
                    Element_Descriptiion__c, End_Date__c, Legally_Approved__c,
                    Legal_Restrictions__c,Source_Type__c, Start_Date__c, Case__c
                    from Asset__c where Case__c = :caseObj.Id  Order by Asset_Type__c];
        if(assetRecordslist != null && assetRecordslist.size()>0){
            System.debug('assetRecordslist' + assetRecordslist);
            for(Asset__c a :assetRecordslist){
                 if(assetMap != null && assetMap.containsKey(a.Asset_Type__c)){
                     tempList =  assetMap.get(a.Asset_Type__c); 
                 }else{
                     tempList = new List<Asset__c>();    
                 }if(a.Asset_Type__c != null){ 
                        tempList.add(a);
                        assetMap.put(a.Asset_Type__c,tempList); 
                 }    
            }
            if(assetMap != null && assetMap.containsKey(CREATIVE)){
                assetTemplist.addAll(assetMap.get(CREATIVE)); 
            }
            if(assetMap != null && assetMap.containsKey(MIXTAPE)){
                assetTemplist.addAll(assetMap.get(MIXTAPE)); 
            }
            if(assetMap != null && assetMap.containsKey(Audio)){
                assetTemplist.addAll(assetMap.get(Audio)); 
            }
            if(assetMap != null && assetMap.containsKey(VIDEO)){
                assetTemplist.addAll(assetMap.get(VIDEO)); 
            } 
        }   
        return assetTemplist;
    }
    /* onLoad - show all asset records*/
    public PageReference onLoad() {
        //Map<String,List<Asset__c>> assetMap = new Map<String,List<Asset__c>>();
        List<Asset__c> assetArrangedlist = new List<Asset__c>();
        assetArrangedlist = arrangeAssetList();
        flag = false;
        for(Asset__c  a: assetArrangedlist) {
            nextId++;
            System.debug('a'+a);
            assetObj.Asset_Location__c = a.Asset_Location__c;
            if(a.Asset_Location__c != null){
                locationflag = true;    
            }
            conditions.add(new ConditionRow(String.valueOf(nextId), a));
            System.debug('conditions ='+conditions + 'Condition Size='+conditions.size());
            // set the value to display the additional details
            flag = a.Asset_Type__c.equals(VIDEO) || flag;
        }
        if (conditions.size() == 0) {
            nextId++;
            Asset__c   assetRecord = new Asset__c(
                Start_date__c = caseObj.Launch_date__c,
                End_date__c = caseObj.Initial_Asset_Expiration_Date__c,
                Asset_retired__c = 'No',
                Legally_Approved__c = 'No',
                Case__c = caseObj.Id
            );
            conditions.add(new ConditionRow(String.valueOf(nextId), assetRecord));
        }
        return null;
    } 
    
    public class ConditionRow {
        private String id;
        private Asset__c condition;
        public ConditionRow(String id, Asset__c condition){
            this.id = id;
            this.condition = condition;
        }
        public String getId() {
            return id;
        }
        
        public Asset__c getCondition() {
            return condition;
        }
    }

}