@isTest
private class CAMP_TestCreateAssetController{
  static testMethod void CAMP_TestCreateAssetMethod(){
    
    Case caseObj = new Case(
     Agency__c = 'abc'
    );
    insert caseObj;
    
    Asset__c asset = new Asset__c(
        Asset_Type__c = 'Mixtape',
        Case__c = caseObj.Id
    );
    insert asset; 
 
    
    ApexPages.StandardController std = new ApexPages.StandardController(caseObj);
    CAMP_CreateAssetController objController = new CAMP_CreateAssetController(std);
    CAMP_CreateAssetController.ConditionRow  obj = new CAMP_CreateAssetController.ConditionRow ('1',asset);
    objController.conditions.add(obj); 
    System.currentPagereference().getParameters().put('selectedId', '1');

    objController.onAddCondition();
    objController.onChangeType();
    objController.save();
    objController.onRemoveCondition();
    objController.onLoad();
    objController.onCloneCondition();
    obj.getId(); 
    obj.getCondition(); 
  }
}