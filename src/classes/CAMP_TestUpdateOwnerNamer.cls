@isTest
private class CAMP_TestUpdateOwnerNamer{
  static testMethod void CAMP_TestUpdatebeforeTrigger(){
    String RT_CC = 'Campaign Clearance';
    String OBJ_CASE = 'Case';
    
         Account actObj = UTIL_TestUtil.generateAccount();
        insert actObj;
        
        Contact conObj = UTIL_TestUtil.generateContact(actObj.id);
        insert conObj;
      
   /** Opportunity oppObj = new Opportunity(
      Name = 'abc',
      StageName = 'Proposal/Pitch',
      CloseDate = system.today()
    );**/
      Opportunity oppObj = UTIL_TestUtil.generateOpportunity(actObj.id, conObj.id);
    insert oppObj;
    
    List<RecordType> recordID = [select id from RecordType where 
    name = :RT_CC and sObjecttype = :OBJ_CASE limit 1];
    
    Case caseObj = new Case(
      Opportunity__c = oppObj.id,
      RecordTypeId = recordId[0].id
    );
    insert caseObj;
  }
  
  static testMethod void CAMP_TestUpdatebeforeTrigger1(){
    String RT_CC = 'Campaign Clearance';
    String OBJ_CASE = 'Case';
    
         Account actObj = UTIL_TestUtil.generateAccount();
        insert actObj;
        
        Contact conObj = UTIL_TestUtil.generateContact(actObj.id);
        insert conObj;
      
    /**Opportunity oppObj = new Opportunity(
      Name = 'abc',
      StageName = 'Proposal/Pitch',
      CloseDate = system.today()
    );**/
      Opportunity oppObj = UTIL_TestUtil.generateOpportunity(actObj.id, conObj.id);
    insert oppObj;
    
    List<RecordType> recordID = [select id from RecordType where 
    name = :RT_CC and sObjecttype = :OBJ_CASE limit 1];
    
    Case caseObj = new Case(
      Opportunity__c = oppObj.id,
      RecordTypeId = recordId[0].id
    );
    insert caseObj;
    
    Profile p = [select id from Profile where name like 'Standard Platform%' limit 1];  
          User u = new User(LastName='Test User',alias = 'test',email='test@testorg.com',emailencodingkey='UTF-8',
                           languagelocalekey='en_US',localesidkey='en_US',timezonesidkey='America/Los_Angeles', 
                           username='testusercamp@testorg.com',profileId = p.Id);
          insert u;
          oppObj.Ownerid = u.id;
          update oppObj;
  }
    
    //updated by VG 12/20/2012
    static testMethod void CAMP_TestUpdatebeforeTrigger2(){
    String RT_IO = 'IO Approval Details';
    String OBJ_CASE = 'Case';
    
        Account actObj = UTIL_TestUtil.generateAccount();
        insert actObj;
        
        Contact conObj = UTIL_TestUtil.generateContact(actObj.id);
        insert conObj;
        
         Opportunity oppObj = UTIL_TestUtil.generateOpportunity(actObj.id, conObj.id);
         insert oppObj;
    
    List<RecordType> recordID = [select id from RecordType where 
    name = :RT_IO and sObjecttype = :OBJ_CASE limit 1];
    
    Case caseObj = new Case(
      Opportunity__c = oppObj.id,
      RecordTypeId = recordId[0].id,
       Billing_Type__c = 'Advertiser',
       Status = 'Closed',
        Payment_approval__c = 'Final IO Approved'
        
    );
    insert caseObj;

        test.starttest();
        Contact conObj2 = UTIL_TestUtil.generateContact(actObj.id);
        insert conObj2;
        
          oppObj.Primary_Billing_Contact__c = conObj2.id;
          update oppObj;
        test.stoptest();
  }
    
    
    
    
}