public class CAMP_Util{

    public static final String RT_CC = 'Campaign_Clearance';
    public static final String RT_IO = 'Legal_IO_Approval_Request'; //updated by VG 12/4/2012 - Contact and Order Properties Phase 2 
    
    public static Boolean hasChanges(String field, SObject oldRecord, SObject newRecord) {
       if (oldRecord == null) {
           return true;
       }
       System.debug('oldRecord.get(field)'+oldRecord.get(field));
        System.debug('newRecord.get(field)'+newRecord.get(field));
       return (oldRecord.get(field) != newRecord.get(field));
    }
    
          
}