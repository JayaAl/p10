/****************************************************************
* Name: CERT_TOOL_EmpVerificationController
* Usage: Page controller for the CERT_TOOL_EmployeeVerificationSent_v2 page.
* Author: Casey Grooms
* Date: 2014-05-19
* Description: Includes updated functionality for querying and presenting Certification questions
****************************************************************/
public without sharing Class CERT_TOOL_EmpVerificationController{
    
    public boolean showBlock{get; set;}                             /* show show in read only mode*/
    public boolean errorFlag{get; set;}                             /* flag for error message*/
    public String msgInfo {get; set;}                               /* Variable for showing message*/
    public String strException{get; set;}                           /* variable for storing exception value*/ 
    public Cert_Tool_Employee__c record {get; set;}                           /* variable for Employee object */
    public Cert_Tool_Employee_Certification__c empCertification{get; set;}    /* variable for Employee Certification object */
    public List <Cert_Tool_Employee_Certification__c> employee {get;set;}     /* variable for Employee object */ 
    public String response = null;                                  /* variable for checkbox value */   
    public boolean flag{get;set;}                                   /* flag for error message */
    public boolean lockFlag{get;set;}                               /* flag for lock record  message */
    public boolean resetFlag{get;set;}                              /* flag for reset token  message */
    public Cert_Tool_Employee_Certification__c employeeCertification ;        /* variable for employee Certification object */
    public String  email = ApexPages.currentPage().getParameters().get('email'); /* email from selecting employee record */
    public String  token = ApexPages.currentPage().getParameters().get('token'); /* email from selecting employee token */ 
    public String empId = ApexPages.currentPage().getParameters().get('id'); /* email from selecting employee Id*/
    public String cert = ApexPages.currentPage().getParameters().get('cert'); /* email from selecting Certifications Id*/
    public String docURL {get;set;}
    public String previewURL {get;set;}
    public boolean accept;          		                        /* variable for rendering a section */
    public boolean boolAccept{get;set;}								/* var to store the "I Accept" checkbox value from submission page */
    public boolean flagResponse{get;set;}
    public  String document{get;set;}                               /* variable for storing Content path */
    public boolean isExceptionEntered{get;set;}                     /*variable to check if exception is entered when response is I have exceptions*/
    static final String I_AGREE = 'I Agree';
    static final String I_DO_NOT_AGREE = 'I do not agree';
    static final String SUBMITTED = 'Submitted';
    static final String WORKING = 'Working';
    //static final String TEMPLATE_NAME = 'Send Response to Employees no Exceptions';
    //static final String TEMPLATE_NAME_EXCEPTIONS = 'Send Response to Employees with Exceptions';
    static final String TEMPLATE_NAME_RESET = 'Security question reset email';
    
    public String getResponse() {
        return response;
    }   
    
    public void setResponse(String response) { 
        this.response = response; 
    } 
    public Blob attach{get;set;}
    
    public static Boolean getIE9(){
        Boolean IE9 = false;
        String useragent = ApexPages.currentPage().getHeaders().get('User-Agent');
        if(useragent.contains('MSIE 9.0'))IE9 = true;
        return IE9;
    }
    
    public List<Cert_Tool_Question__c> theQuestions{get;set;}
    /*Constructor for setting default values */
    public CERT_TOOL_EmpVerificationController(ApexPages.StandardController stdController){    
        
        //IE9 Patch
        
        record = (Cert_Tool_Employee__c) stdController.getRecord();
        System.debug('email'+email);
        System.debug('token '+token);
        resetFlag = false;
        flag = false;    
        isExceptionEntered= true;   
        employee = [Select 
                    e.Token__c,e.Status__c, e.Due_Date__c, e.Id, e.Employee__r.Employee_ID__c, 
                    e.Exception__c,  e.Employee__r.Answer__c,e.Certification__r.Published_Document_URL__c,e.Certification__r.Preview_Document_URL__c, 
                    e.Employee__r.Question__c, e.Employee__r.Email__c,e.Response__c,
                    e.Employee__r.Department__c,  e.Employee__c, e.Certification__c, 
                    e.Certification__r.Due_Date__c , e.Accept__c, e.Employee__r.Name,e.Email__c
                    From Cert_Tool_Employee_Certification__c e 
                    where e.Token__c = :token and e.Employee__r.Email__c = : email
                   ];
        
        if(employee.isEmpty()){
            System.debug(LoggingLevel.INFO, 'Employee not found');
            lockFlag  = true;
            msgInfo = System.Label.CERT_TOOL_authorizedmessage; 
        }
        if(!employee.isEmpty()){
            boolAccept = employee[0].Accept__c;
            Id certificationId = employee[0].Certification__c;
            String strDepartment =  employee[0].Employee__r.Department__c;           
            List<ContentVersion > contentList =[select Id , PathOnClient , VersionData , Certification__c ,Department__c from ContentVersion  where Certification__c = :certificationId and Department__c = :strDepartment];
            this.docURL = employee[0].Certification__r.Published_Document_URL__c;
            if( employee[0].Certification__r.Preview_Document_URL__c != '' ){
                this.previewURL = employee[0].Certification__r.Preview_Document_URL__c;
            } else {
                this.previewURL = this.docURL;
            }
            if(!contentList.isEmpty()){
                document =  contentList[0].PathOnClient;
                Blob attach = contentList[0].VersionData; 
                System.debug('document '+document);             
            }
            
            theQuestions = [Select 
                            q.Name, q.Certification__c, q.Default_Answer__c, q.Picklist_Options__c, 
                            q.Question_Text__c, q.Question_Type__c, q.Sorting_Order__c 
                            From Cert_Tool_Question__c q 
                            Where q.Certification__c = :certificationId
                            Order By q.Sorting_Order__c, q.Name
                           ];
            errorFlag = false;
            lockFlag = false;
            if(employee[0].Employee__r.Question__c == null){
                showBlock = true;  
                response = I_AGREE ;
            }else{
                record.Question__c = employee[0].Employee__r.Question__c;       
                showBlock = false;
            }
            if(employee[0].Status__c != null && employee[0].Status__c.equals('Submitted')){
                datetime t = System.now();            
                date d = Date.newInstance(t.year(),t.month(),t.day());       
                System.debug('Today Date'+d);
                System.debug('Today Date'+t);
                System.debug('Due Date'+employee[0].Due_Date__c);
                if(employee[0].Due_Date__c < d){
                    System.debug('In If');
                    lockFlag  = true;
                    msgInfo = System.Label.CERT_TOOL_recordLock; 
                }
            }else{
                lockFlag = false;
            }
            if(employee[0].Accept__c){
                flag = false;
                response = I_AGREE ;
                strException = '';
                System.debug('In If'+response);
            }else{
                if(employee[0].Exception__c == null){
                    flag = false;
                    response = I_AGREE ;
                    System.debug('In else If'+response); 
                }else{
                    flag = true;
                    response = I_DO_NOT_AGREE ;
                    strException = employee[0].Exception__c ;
                    System.debug('In else'+response);
                }
            }
        }
        
        /*if(strException.equals('')){
System.debug('In strException is null' + strException);
errorFlag = true;
msgInfo = 'Please enter exceptions in the text box';

}  */
        
    }
    
    
    /* save method for checking question & answer*/
    public PageReference save(){
        if(!employee.isEmpty()){
            if(employee[0].Employee__r.Answer__c == null){
                employee[0].Employee__r.Answer__c = record.Answer__c;
                employee[0].Employee__r.Question__c = record.Question__c;
                update employee[0].Employee__r;
                showBlock = true; 
                return page.CERT_TOOL_EmployeeVerificationSent;    
            }else{
                if(record.Answer__c == employee[0].Employee__r.Answer__c){
                    showBlock = false; 
                    errorFlag = false;
                    return page.CERT_TOOL_EmployeeVerificationSent;   
                }else{
                    errorFlag = true;
                    msgInfo = System.Label.CERT_TOOL_ErrorMessage; 
                    return null;
                }
            }
        }
        return null;
    }
    
    
    /*getResponseList method for List of checkbox */     
    public List<SelectOption> getResponseList() {      
        List<SelectOption> options = new List<SelectOption>();
        Schema.DescribeFieldResult responseValues = Cert_Tool_Employee_Certification__c.Response__c.getDescribe(); 
        List<Schema.PicklistEntry> Ple = responseValues.getPicklistValues();
        for(Schema.PicklistEntry p : ple){
            options.add(new SelectOption(p.getValue(),p.getlabel()));
        }
        return options;
    }
    
    /*showTextFields method for rendering the section*/
    public void showTextFields(){
        if(response != null){
            if(response.equals(I_DO_NOT_AGREE)){
                flag = true;
            }else{
                strException = '';
                flag = false;
            }
        }
    }
    
    public boolean lockBtn{
        get{
            if(lockBtn==FALSE){lockBtn=!boolAccept;}
            return lockBtn;} 
        set;}
    public void unlockSubmit(){
        lockBtn = false;
    }
    
    /*saveAndSubmit  method update record to locked*/
    public  void saveAndSubmit(){   
        System.debug('strException = ' + strException);
        System.debug('>>>>>>>>>Start of save and submit employee: ' + employee);
        String TEMPLATE_NAME = 'Send Response to Employees [Agree]';
        String content = '';
        String subject = '';
        if(!employee.isEmpty()){
            String problem;
            if(strException!= null ){
                System.debug('In strException != null');
                
                if(!boolAccept){
                // if(response.equals(I_DO_NOT_AGREE)){
                    accept = false;
                    System.debug('strException'+strException);
                    TEMPLATE_NAME = 'Send Response to Employees [Do not agree]';
                }else{
                    accept = true;
                    System.debug('strException not'+strException);
                    strException = '';
                    problem = '';
                }
                System.debug('In If'+accept);
                problem = strException;
                System.debug('In If'+strException);
            }else{
                System.debug('In strException = null');
                if(boolAccept){
                //if(response.equals(I_AGREE)){
                    accept = true;
                }else{
                    accept = false;
                }
                problem = null;
            }
            
            
            System.debug('In If'+employeeCertification);
            System.debug('employeeCertification: ' + employeeCertification);
            try{
                employee[0].Status__c = SUBMITTED;
                employee[0].Exception__c = problem ;              
                employee[0].Response__c = boolAccept ? 'I Accept' : '' ;               
                // employee[0].Response__c = response;
                employee[0].Accept__c = accept;
                
                CERT_TOOL_Util.upsertEmployee(employee[0]);
                
                System.debug('>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>employee: ' + employee[0]);              

                //update employee;            
                errorFlag = true;
                msgInfo = System.Label.CERT_TOOL_Success_Message; 
                
            }catch (DmlException e){
                System.debug('Error when updating employee certification: ' + e.getMessage());
            }
            System.debug('In Mail'+token + 'Email'+email);
            System.debug('token '+employee[0].Employee__r.Name);
            
            if(accept){
                content = employee[0].Employee__r.Name +  ', <br /><br /> Your Code certification has been successfully submitted. You have replied "I agree".  <br /><br />Thank you, <br />The Pandora Legal Team ';
                subject = 'Your Code certification has been successfully submitted';
            }
            else{
                content = employee[0].Employee__r.Name +  ', <br /><br /> Your Code certification has been successfully submitted. You have replied "I do not agree".  ' + 
                    '<br /><br />Thank you, <br />The Pandora Legal Team ';
                subject = 'Your Code certification has been successfully submitted';
            }
            content = content.replaceAll('\r\n', '<br/>');
            // content = content.replaceAll('\n', 'WORLD');
            
            CERT_TOOL_Util.sendResponseEmail(employee[0] ,subject,content);
            resetFlag = true;
            msgInfo = System.Label.CERT_TOOL_Success_Message;          
            
        }
    }
    
    /*dosave method for updating values*/
    public  PageReference saveForLater(){
        strException = ''; // there is no exception to save on this form
        System.debug('saveForLater(): strException = ' + strException); 
        if(!employee.isEmpty()){
            String problem;
            if(strException!= null ){
                if(!boolAccept){
                // if(response==null || response.equals(I_DO_NOT_AGREE)){
                    accept = false;
                }else{
                    accept = true;
                    strException = '';
                    problem = '';
                }
                System.debug('In If'+accept);
                problem = strException;
                System.debug('In If'+strException);
            }else{
                if(boolAccept){
                // if(response.equals(I_AGREE)){
                    accept = true;
                }else{
                    accept = false;
                }
                problem = null;
            }
            System.debug('In If '+accept);
            employeeCertification = new Cert_Tool_Employee_Certification__c(
                Id = employee[0].Id ,
                Employee__c = employee[0].Employee__c,  
                Certification__c = employee[0].Certification__c,
                Exception__c = problem ,
                Accept__c = accept,
                Response__c = boolAccept ? 'I Accept' : '' ,
                Status__c = WORKING 
            );
            try{
                employee[0].Status__c = WORKING;
                employee[0].Exception__c = problem;
                employee[0].Response__c = boolAccept ? 'I Accept' : '' ;
                CERT_TOOL_Util.upsertEmployee(employee[0]);
                System.debug('employee = ' + employee);
                System.debug('employeeCertification = ' + employeeCertification);
                //upsert employeeCertification;
                CERT_TOOL_Util.upsertCertification(employeeCertification);
                errorFlag = true;
                msgInfo = System.Label.CERT_TOOL_Save_for_later_Message; 
                return null; 
            }catch (DmlException e){
                System.debug(e.getMessage());
            }
        }
        
        return null;
    }
    
    /* resetToken method for reset to token and send email*/
    public void resetToken(){
        String strTokenNumber = '';
        strTokenNumber = CERT_TOOL_RandomStringUtils.randomGUID();
        Cert_Tool_Employee__c employeeUpdate = new Cert_Tool_Employee__c(
            Id = employee[0].Employee__c,
            Answer__c = null,
            Question__c = null
        );
        
        Cert_Tool_Employee_Certification__c employeeCertificationforReset = new Cert_Tool_Employee_Certification__c(
            Id = employee[0].Id ,
            Token__c = strTokenNumber
        );
        try{
            if(employeeUpdate!= null){
                update employeeUpdate;
            } 
            System.debug('employeeCertificationforReset  '+employeeCertificationforReset);
            if(employeeCertificationforReset  != null)
                update employeeCertificationforReset;
            resetFlag = true;
            msgInfo = System.Label.CERT_TOOL_Reset_Token;  
        }catch (DmlException e){
            System.debug(e.getMessage());
        } 
        Cert_Tool_Employee_Certification__c employeeUpdateToken = [Select e.Token__c,e.Status__c, e.Id, e.Employee__r.Employee_ID__c, e.Exception__c,  e.Employee__r.Answer__c, 
                                                                   e.Employee__r.Question__c, e.Employee__r.Email__c, e.Employee__c, e.Certification__c, e.Accept__c, e.Employee__r.Name 
                                                                   From Cert_Tool_Employee_Certification__c e 
                                                                   where e.Token__c = :strTokenNumber and e.Employee__r.Email__c = : email]; 
        String subject = 'Your security question and corresponding answer used to access your quarterly certification have been reset';
        String content = employeeUpdateToken.Employee__r.Name + ', <br /><br /> Your security question and corresponding answer ' + 
            'used to access your quarterly certification have been reset. ' + 
            'Please click the link below to continue your login.<br/><br />' + 
            '<a href=" ' + Label.CERT_TOOL_SFDC_Domain + '/CERT_TOOL_EmployeeVerification?email=' + employeeUpdateToken.Employee__r.Email__c + Label.CERT_TOOL_Ampersand + 'token=' + strTokenNumber + '">Quarterly Certification</a> <br /><br />' + 
            'Thank you,<br />The Pandora Legal Team';
        
        CERT_TOOL_Util.sendResponseEmail(employeeUpdateToken, subject, content);
        
        /*Messaging.SingleEmailMessage mail = new Messaging.SingleEmailMessage(); 
EmailTemplate template = [select id, body, ownerid ,subject from EmailTemplate where Name = :TEMPLATE_NAME_RESET];
String[] toAddresses = new String[] {employeeUpdateToken.Employee__r.Email__c};
mail.setToAddresses(toAddresses);
//mail.setWhatId(employee[0].Id);
mail.setTemplateId(template.id);
//mail.setTargetObjectId(template.ownerid); 
mail.setTargetObjectId(userinfo.getuserId());       
mail.setWhatId(employeeUpdateToken.Id);  
mail.setSaveAsActivity(false); 
try{
Messaging.SendEmailResult [] r =Messaging.sendEmail(new Messaging.SingleEmailMessage [] {mail}); 
resetFlag = true;
//msgInfo = System.Label.Reset_Token; 
}catch(Exception e){ } */
    }
    
    /*method for docancel*/
    public  PageReference docancel(){
        Id str = employee[0].Id;
        System.debug('record'+str);
        PageReference pr = new PageReference('/'+str);
        pr.setRedirect(true);
        return pr;
    }
    
    public PageReference onLoad() {
        String browserType = Apexpages.currentPage().getHeaders().get('USER-AGENT'); 
        if(browserType != null && browserType.contains('MSIE')) {
            System.debug('IE9 being used');
            //return Page.IE9_Generic_Error;
        }
        return null;  
    }
    
    public String browser {get; set;}
    
    public String getCurrentPageURL() {
        return System.currentPageReference().getURL();
    }
}