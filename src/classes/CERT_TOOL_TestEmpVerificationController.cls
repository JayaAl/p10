@isTest
private class CERT_TOOL_TestEmpVerificationController{
   static testmethod void testUnitCase(){
       CERT_TOOL_Employee__c emp = new CERT_TOOL_Employee__c(
           Name = 'test', 
           Email__c = 'testing@yahoo.com',
           Employee_ID__c = '7y17',
           Record_lock__c = true,
           Answer__c = 'test',
           Question__c = 'On what street did you grow up?'
           
       );
       insert emp;
       datetime t = System.now();            
       date d = Date.newInstance(t.year(),t.month(),t.day());    
       CERT_TOOL_Certification__c cerfication = new CERT_TOOL_Certification__c(
           Name = 'document',
           Due_Date__c = d
       );
       insert cerfication; 
       CERT_TOOL_Employee_Certification__c empCerfication = new CERT_TOOL_Employee_Certification__c(
           
           Certification__c = cerfication.Id, 
           Employee__c = emp.Id,
           Token__c = '777',
           Status__c = 'Submitted'
       );
       insert empCerfication ;
       System.currentPagereference().getParameters().put('email',emp.Email__c );
       System.currentPagereference().getParameters().put('token',empCerfication.Token__c);
       ApexPages.StandardController sc = new ApexPages.StandardController(emp);
       CERT_TOOL_EmpVerificationController obj = new CERT_TOOL_EmpVerificationController(sc);
           obj.record.Answer__c = 'test';
           obj.save();
           String str = 'I Agree';
           obj.getResponse();
           obj.setResponse(str);
           obj.getResponseList();
           obj.response = 'I do not agree';
           obj.showTextFields();
           //obj.strException1 = 'test';
           //obj.strException = 'test1';
           obj.saveForLater();
           obj.saveAndSubmit();
           obj.saveForLater();
           obj.resetToken();
       CERT_TOOL_Employee__c objemp = new CERT_TOOL_Employee__c(
           Name = 'test1', 
           Email__c = 't@yahoo.com',
           Employee_ID__c = '7zxxy17',
           Record_lock__c = false
       );
       insert objemp ;    
       System.currentPagereference().getParameters().put('email',objemp.Email__c );
       ApexPages.StandardController scr = new ApexPages.StandardController(objemp);
       CERT_TOOL_EmpVerificationController objtest = new CERT_TOOL_EmpVerificationController(scr);
           //objtest.employee[0].Employee__r.Answer__c = '';
           objtest.record.Answer__c = 'test';
           obj.save();
           obj.getResponse();
           //String str = 'I Agree';
           obj.setResponse(str);
           obj.getResponseList();
           obj.response = 'I agree';
           obj.showTextFields();
           obj.saveForLater();
           obj.response = 'I agree';
           obj.saveAndSubmit();
           obj.docancel();
           obj.resetToken();
           obj.getResponseList();
           PageReference pObj1 = obj.save();
       System.assertEquals( new PageReference('/apex/cert_tool_employeeverificationsent').getURL(), pObj1.getURL());
       
   }
}