@istest
private class CERT_TOOL_TestResendEmailController {
  private static testmethod void resendEmail(){
    
    CERT_TOOL_Certification__c objCertification = new CERT_TOOL_Certification__c(
        Name = 'test'
    );
    insert objCertification ;
    
    CERT_TOOL_Employee__c  objEmployee = new CERT_TOOL_Employee__c(
        Name = 'test'
    );
    insert objEmployee ;
    
    CERT_TOOL_Employee_Certification__c  objEmp = new CERT_TOOL_Employee_Certification__c (
        Employee__c = objEmployee.id ,
        Certification__c = objCertification.id,
        Token__c = '1234'
    );
    insert objEmp;
       
    ApexPages.StandardController controller = new ApexPages.StandardController(objEmp);
       
    CERT_TOOL_ResendEmailController objResendMail = new CERT_TOOL_ResendEmailController(controller );   
    objResendMail.sendEmail();
    
    objEmp.Token__c = null;
    update objEmp;
    objResendMail.sendEmail();
  }

}