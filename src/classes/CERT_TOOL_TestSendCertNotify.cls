@isTest
private class CERT_TOOL_TestSendCertNotify{

    static testMethod void testEmpCertitificationNotification() {
        //Create test employee
        CERT_TOOL_Employee__c testEmp = new CERT_TOOL_Employee__c(Name='Test Employee',Email__c='test@emp.com');
        insert testEmp; 
        
        //Create test certification
        CERT_TOOL_Certification__c testCert =
                 new CERT_TOOL_Certification__c(Name='Test Cert',Quarter__c='Q1',Fiscal_Year__c='FY2011',Response_type__c='Primary',Published_Document_URL__c='https://null--Pandora10.cs12.my.salesforce.com/sfc/p/V00000000Qey.rtmGzXb.6Sn0zbGnNIK094nOV4=', Due_Date__c=Datetime.parse('10/31/2011 5:34 PM'));
        insert testCert;
                 
        //Assign test certification to test employee
        Cert_Tool_Employee_Certification__c ec1 = new Cert_Tool_Employee_Certification__c (Employee__c = testEmp.Id,Certification__c=testCert.Id);
        insert ec1;
        
        //Create reminder for test certification for test employee
        Cert_Tool_Employee_Reminder__c ecr1 = 
            new Cert_Tool_Employee_Reminder__c (Date_Time__c=DateTime.parse('10/24/2011 5:34 PM'),Email_Template__c='Employee Certification - one day before due date',Employee_Certification__c=ec1.Id);
        insert ecr1;
        
        
        //Update emplyee certification status
        ec1 = [Select Id, Status__c from CERT_TOOL_Employee_Certification__c where Id = :ec1.Id];
        ec1.Status__c = 'Open';
        update ec1;
         
         
        //Update emplyee certification reminder to send notification
        ecr1 = [Select Id, Send_Notifications__c from Cert_Tool_Employee_Reminder__c where id=:ecr1.Id];
        ecr1.Send_Notifications__c = true;
        update ecr1;           
    }
}