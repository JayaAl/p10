@isTest
private class CFSTriggerTest {
	
	static testMethod void testAccountTrigger() {
        //uncomment these lines below to test creating an Account and triggering the CFS Account Trigger
        /*
        //populate any additional Account required fields in this constructor call:
        Account account = new Account(Name= 'CFSTriggerTest Account');
        insert account;
        System.assert(account.Id != null);
        */
	}

    static testMethod void testCaseTrigger() {
        //uncomment these lines below to test creating a Case and triggering the CFS Case Trigger
        /*
        //populate any Case required fields in this constructor call:
        Case testCase = new Case();
        insert testCase;
        System.assert(testCase.Id != null);
        */
    }

    static testMethod void testContactTrigger() {
        //uncomment these lines below to test creating a Contact and triggering the CFS Contact Trigger
        /*
        //populate any additional Contact required fields in this constructor call:
        Contact contact = new Contact(LastName= 'CFSTriggerTestContact');
        insert contact;
        System.assert(contact.Id != null);
        */
    }
	
    static testMethod void testLeadTrigger() {
        //uncomment these lines below to test creating a Lead and triggering the CFS Lead Trigger
        /*
        //populate any additional Lead required fields in this constructor call:
        Lead lead = new Lead(LastName= 'CFSTriggerTestLead', Company= 'CFSTriggerTest Company');
        insert lead;
        System.assert(lead.Id != null);
        */
    }

    static testMethod void testOpportunityTrigger() {
        //uncomment these lines below to test creating an Opportunity and triggering the CFS Opportunity Trigger
        /*
        //populate any addtional Opportunity required fields in this constructor call:
        Opportunity opportunity = new Opportunity(Name= 'CFSTriggerTest Opportunity', StageName = 'test', CloseDate = Date.today()); 
        insert opportunity;
        System.assert(opportunity.Id != null);
        */
    }
}