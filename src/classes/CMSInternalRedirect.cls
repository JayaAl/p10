/*
Developer: Ralph Callaway <ralph.w.callaway@gmail.com>
Description:
	Enable CMS Force pages to be viewed internally.  Replaces the function
	of the URL Rewriter used by CMSForce in Sites.  Takes a friendly_url param
	that is used to look up the target page.

	Instead of links like this:
		- http://pandora.force.com/thewall/cms/local
		
	We now use links like these
		- http://c.na2.visual.force.com/apex/CMSInternalRedirect?friendly_url=local
*/
public without sharing class CMSInternalRedirect {
	
	private static final String CMSFORCE_VISUALFORCE_PAGE = '/apex/page?pageid=';
	private static final String FRIENDLY_URL_PARAM = 'friendly_url';
	
	public PageReference redirect() {
		
		// find page based on friendly url and redirect if found
		String friendlyUrl = ApexPages.currentPage().getParameters().get(FRIENDLY_URL_PARAM);
		List<Page__c> pages = [select id from Page__c where friendlyUrl__c = :friendlyUrl limit 1];
		
		if(!pages.isEmpty()) {
			return new PageReference(CMSFORCE_VISUALFORCE_PAGE + pages[0].id);	
		} else {
			ApexPages.addMessage(new ApexPages.Message(
				  ApexPages.Severity.ERROR
				, 'Page not found. [' + friendlyUrl + ']'
			));
			return null;
		}
	}
	
	// Test Methods //
	
	@isTest
	private static void testValidFriendlyUrlRedirect() {
		// create test data
		String friendlyUrl = 'asdf' + System.now().getTime();
		CMSFolder__c testFolder = new CMSFolder__c(
			  type__c = 'Page'
			, name = 'asdf'
			, site_id__c = 'asdf'
		);
		insert testFolder;
		Page__c testPage = new Page__c(
			  name = friendlyUrl
			, friendlyUrl__c = friendlyUrl
			, folder__c = testFolder.id
		);
		insert testPage;
		
		// load controller
		Test.startTest();
		ApexPages.currentPage().getParameters().put(FRIENDLY_URL_PARAM, friendlyUrl);
		CMSInternalRedirect controller = new CMSInternalRedirect();
		PageReference nextPage = controller.redirect();
		Test.stopTest();
		
		// validate redirect
		system.assertNotEquals(null, nextPage);
		system.assertEquals(CMSFORCE_VISUALFORCE_PAGE + testPage.id, nextPage.getUrl());
	}
	
	@isTest
	private static void testInvalidFriendlyUrlRedirect() {
		// don't create any test pages
		String friendlyUrl = 'asdf' + System.now().getTime();
		
		// load controller
		Test.startTest();
		ApexPages.currentPage().getParameters().put(FRIENDLY_URL_PARAM, friendlyUrl);
		CMSInternalRedirect controller = new CMSInternalRedirect();
		PageReference nextPage = controller.redirect();
		Test.stopTest();
		
		// validate no redirect and error message
		system.assertEquals(null, nextPage);
		system.assert(ApexPages.hasMessages(ApexPages.Severity.ERROR));
	}
}