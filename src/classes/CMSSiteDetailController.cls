public with sharing class CMSSiteDetailController {

	public String homepreview {get;set;}
	public String instance {get;set;}
	public Site s {get;set;}
	
	public CMSSiteDetailController() {
		try {
			String siteId = System.currentPageReference().getParameters().get('id').substring(0,15);
			
			// Update for Internal CMS
			// - query home page directly instead of redirecting to sites base url
			Page__c home = [select Id from Page__c where Home_Page__c = true and Site_Id__c = :siteId limit 1];
			homePreview = '/apex/page?pageid=' + home.id;
		}
		catch(Exception ex) {
			Apexpages.addMessage(new ApexPages.Message(ApexPages.Severity.INFO, 'No CMSForce home page found for this site'));
		}
	}
	
	
	/** TESTS **/
	private static testMethod void testSiteWithHomepage() {
		String siteId = '0123456789abcde';
		CMSFolder__c testFolder = new CMSFolder__c(
			  type__c = 'Page'
			, name = 'asdf'
			, site_id__c = siteId
		);
		insert testFolder;
		Page__c testPage = new Page__c(
			  name = 'asdf'
			, folder__c = testFolder.id
			, site_id__c = siteId
			, home_page__c = true
		);
		insert testPage;
		ApexPages.currentPage().getParameters().put('id', siteId);
		CMSSiteDetailController controller = new CMSSiteDetailController();
		system.assertEquals('/apex/page?pageid=' + testPage.id, controller.homePreview);
	}
	
	private static testMethod void testSiteWithoutHomepage() {
		String siteId = '0123456789abcde';
		ApexPages.currentPage().getParameters().put('id', siteId);
		CMSSiteDetailController controller = new CMSSiteDetailController();
		system.assert(ApexPages.hasMessages(ApexPages.Severity.INFO));
	}

}