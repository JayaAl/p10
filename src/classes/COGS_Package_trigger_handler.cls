public class COGS_Package_trigger_handler {
// Class and helper methods to perform COGS Package updates

    // Flag that ensures that we don't recursively call these functions.
    // Checked by the individual Triggers before passing data to the below methods.
    public Boolean isRunning = false; 
    
    private Map<Id, Opportunity_COGS_Junction__c> mapIdJunction;
    
    
/* COGS_2_0_Package__c beforeInsert TRIGGER on COGS_2_0_Package__c creation that spools off the required processes */
    public void COGSPackageBeforeInsert(List<COGS_2_0_Package__c> listNew) {
        // Finds the parent COGS_Header_Id
        Set<Id> SetHeaderIds = populateHeaderIdSet(listNew);
        
        // Queries the Opportunity_COGS_Junction__c records associated to the Header
        Map<Id, Opportunity_COGS_Junction__c> mapIdJunction = populateJunctionMap(SetHeaderIds);
        
        // Loop through the COGS_2_0_Package__c records and populate the fields if data was found.
        // List<COGS_2_0_Package__c> listToUpdate = listPackagesToUpdate(listNew, mapIdJunction);
        List<COGS_2_0_Package__c> listToUpdate = listsObjectToUpdate(listNew, mapIdJunction, 'Opportunity_COGS_Junction__c', 'COGS_2_0_Package__c', 'COGS__c');
        
        // Since this is strictly a Before Insert trigger there is no need to perform DML
        // update listToUpdate;
    }
    
/* Opportunity_COGS_Junction__c beforeUpdate: Evaluate if the Opportunity_Sales_Planner__c, Opportunity_Seller__c fields changed, 
   if so then create a Map to feed COGSJunctionBeforeInsert */    
    public void COGSJunctionBeforeUpdate(Map<Id, Opportunity_COGS_Junction__c> newMap, Map<Id, Opportunity_COGS_Junction__c> oldMap) {
        /*
        Map<Id, Opportunity_COGS_Junction__c> theMap = new Map<Id, Opportunity_COGS_Junction__c>();
        for(Id i:newMap.keySet()){
            Opportunity_COGS_Junction__c newJ = newMap.get(i);
            Opportunity_COGS_Junction__c oldJ = oldMap.get(i);
            if(newJ.Opportunity_Seller__c != oldJ.Opportunity_Seller__c 
                || newJ.Opportunity_Sales_Planner__c != oldJ.Opportunity_Sales_Planner__c
                || newJ.Opportunity_Sales_Dev__c != oldJ.Opportunity_Sales_Dev__c 
                || newJ.Opportunity_Probability__c != oldJ.Opportunity_Probability__c 
                || newJ.Opportunity_Owner__c != oldJ.Opportunity_Owner__c 
                || newJ.Opportunity_Probability__c != oldJ.Opportunity_Probability__c 
            ){
                theMap.put(i,newJ);
            }
        }
        if(!theMap.isEmpty()){
            COGSJunctionBeforeInsert(theMap);
        }
		*/
        // Just do the updates anyway, dont bother checking for updates.
        COGSJunctionBeforeInsert(newMap);
    }
    
/* Opportunity_COGS_Junction__c beforeInsert TRIGGER on Opportunity_COGS_Junction__c creation and edit */    
    public void COGSJunctionBeforeInsert(Map<Id, Opportunity_COGS_Junction__c> newMap) {
        // Finds the parent COGS_Header_Id
        Set<Id> SetHeaderIds = populateHeaderIdSet(newMap.values());
        
        // Gather the parent Opportunity information into a Map<COGS_Header_Id , Opportunity_Info>
        // We are intentionally populating this Map via a new query in order to assure that we only have one Opportunity_COGS_Junction__c record even on mass updates
        Map<Id, Opportunity_COGS_Junction__c> mapIdJunction = populateJunctionMap(SetHeaderIds);
        
        // Create an instance of each Header that we need, no need to perform a query since we need no other fields
        List<COGS_2_0__c> listHeadersToUpdate = new List<COGS_2_0__c>();
        for(Opportunity_COGS_Junction__c j:newMap.values()){
            COGS_2_0__c c = new COGS_2_0__c(Id=j.Cogs__c);
            listHeadersToUpdate.add(c);
        }
        // Loop through the COGS_2_0_Package__c records and populate the fields if data was found.
        // listPackages = listPackagesToUpdate(listPackages, mapIdJunction);
		listHeadersToUpdate = listsObjectToUpdate(listHeadersToUpdate, mapIdJunction, 'Opportunity_COGS_Junction__c', 'COGS_2_0__c', 'Id');
        
        // Find all COGS_2_0_Package__c records associated to any COGS_Header_Id
        // Since we are populating values into the record using the MapCOGSDataFields Map we dont need to include many fields in this query
        // This one does need to be a query since we are including the COGS__c lookup as the matching value
        List<COGS_2_0_Package__c> listPackages = [
            SELECT Id, COGS__c
            FROM COGS_2_0_Package__c 
            WHERE COGS__c IN : SetHeaderIds
        ];
        
        // Loop through the COGS_2_0_Package__c records and populate the fields if data was found.
        // listPackages = listPackagesToUpdate(listPackages, mapIdJunction);
		listPackages = listsObjectToUpdate(listPackages, mapIdJunction, 'Opportunity_COGS_Junction__c', 'COGS_2_0_Package__c', 'COGS__c');
        
        // Update the located COGS_2_0_Package__c records with the appropriate Opportunity_Info
        try{
            if(!listPackages.isEmpty()){
                update listPackages;
            }
            if(!listHeadersToUpdate.isEmpty()){
                update listHeadersToUpdate;
            }
        } catch (Exception e) { // Write error log
            logger.logMessage('COGS_Package_trigger_handler', 'COGSJunctionBeforeInsert', e.getMessage(), 'Opportunity_COGS_Junction__c', '', '');
        }
        
    }
    
/*method to loop through a list of given COGS Packages and gather the COGS header IDs */
    public Set<Id> populateHeaderIdSet(List<COGS_2_0_Package__c> listPackages){
        Set<Id> SetIds = new Set<Id>();
        if(!listPackages.isEmpty()){
            for( COGS_2_0_Package__c p : listPackages ){
                SetIds.add(p.COGS__c);
            }
        }
        return SetIds;
    }
    // Overloaded method to allow for feeding in a list of Opportunity_COGS_Junction__c records instead
    public Set<Id> populateHeaderIdSet(List<Opportunity_COGS_Junction__c> listJunctions){ 
        Set<Id> SetIds = new Set<Id>();
        if(!listJunctions.isEmpty()){
            for( Opportunity_COGS_Junction__c p : listJunctions ){
                SetIds.add(p.COGS__c);
            }
        }
        return SetIds;
    }
    
/*method to query COGS_Data_Transfer Custom Settings and populate a MAP<Source Object, Map<Target Object Map<String, String>>> */
    public Map<String, Map<String, Map<String, String>>> MapCOGSDataFields{
        get{
            if(MapCOGSDataFields == NULL || MapCOGSDataFields.isEmpty()){
                // Create empty Map framework
                Map<String, Map<String, Map<String, String>>> theMap = new Map<String, Map<String, Map<String, String>>>(); 
                // get all Custom Settings & Loop Through
                for(COGS_Data_Transfer__c s:COGS_Data_Transfer__c.getall().values()){
                    // If the Map does not contain source object create empty placeholder
                    if(!theMap.containsKey(s.Source_Object__c)){
                        theMap.put(s.Source_Object__c, new Map<String, Map<String, String>>());
                    }
                    // if the Map does not contain target object create empty placeholder
                    if(!theMap.get(s.Source_Object__c).containsKey(s.Target_Object__c)){
                        theMap.get(s.Source_Object__c).put(s.Target_Object__c, new Map<String, String>());
                    }
                    // Finally, just shove the two fields into the Map, overwriting anything else that may be there.
                    theMap.get(s.Source_Object__c).get(s.Target_Object__c).put(s.Source_Field__c,s.Target_Field__c);
                }
                MapCOGSDataFields = theMap;
            }
            return MapCOGSDataFields;
        }
        set;
    }    
    
/*method to query Junction records associated to specific COGS records */
    public Map<Id, Opportunity_COGS_Junction__c> populateJunctionMap(Set<Id> SetCOGSIds){
        mapIdJunction = new Map<Id, Opportunity_COGS_Junction__c>();
        // Queries the Opportunity_COGS_Junction__c records associated to the Header, sorted lastModDate descending
        for( Opportunity_COGS_Junction__c j : [
            SELECT Id, Name, COGS__c, Opportunity_Account__c, Opportunity_Upfront__c, Opportunity_Seller__c, 
            Opportunity_Sales_Planner__c, Opportunity_Owner__c, Opportunity_Sales_Dev__c, Opportunity_Probability__c
            FROM Opportunity_COGS_Junction__c 
            WHERE COGS__c IN :SetCOGSIds 
            ORDER BY LastModifiedDate ASC
        ] ){
            // Feed results into a Map<COGS_Header_Id , Opportunity_Info>
            mapIdJunction.put(j.COGS__c, j);
        }
        return mapIdJunction;
    }
    
/* method to loop through all given Packages and update them with information from related Junctions */
    public List<COGS_2_0_Package__c> listPackagesToUpdate(List<COGS_2_0_Package__c> listPackages, Map<Id, Opportunity_COGS_Junction__c> mapIdJunction){
        List<COGS_2_0_Package__c> newList = new List<COGS_2_0_Package__c>();
        // if mapIdJunction has no contents there is no need to perform any updates
        // OR if MapCOGSDataFields does not contain Opportunity_COGS_Junction__c > COGS_2_0_Package__c fields there are no updates
        if(
            mapIdJunction == NULL || mapIdJunction.isEmpty()
            || MapCOGSDataFields == NULL || MapCOGSDataFields.isEmpty()
            || !MapCOGSDataFields.containsKey('Opportunity_COGS_Junction__c') 
            || MapCOGSDataFields.get('Opportunity_COGS_Junction__c') == NULL || MapCOGSDataFields.get('Opportunity_COGS_Junction__c').isEmpty()
            || !MapCOGSDataFields.get('Opportunity_COGS_Junction__c').containsKey('COGS_2_0_Package__c')
          ){
            return newList; // just return the empty list
        }
        // Loop through the COGS_2_0_Package__c records and populate the fields if data was found.
        try{
            for( COGS_2_0_Package__c p : listPackages ){
                // Create a sObject, when doing so with the ID we ceate a new instance of the object which we can populate later 
                // only those fields which are populaed via the below code will be updated 
                COGS_2_0_Package__c newP = new COGS_2_0_Package__c(Id=p.Id); 
                if( mapIdJunction.containsKey(p.COGS__c) ){
                    Opportunity_COGS_Junction__c j = mapIdJunction.get(p.COGS__c);
                    // for each 
                    for(String sourceField:MapCOGSDataFields.get('Opportunity_COGS_Junction__c').get('COGS_2_0_Package__c').keySet()){
                        String targetfield = MapCOGSDataFields.get('Opportunity_COGS_Junction__c').get('COGS_2_0_Package__c').get(sourceField);
                        newP.put(targetfield,j.get(sourceField));
                    }
                    newList.add(newP); // write each of the new fields to the sObject
                }
            }
        } catch (Exception e) { // Write error log
            logger.logMessage('COGS_Package_trigger_handler', 'listPackagesToUpdate', e.getMessage(), 'COGS_2_0_Package__c', '', '');
        }
        // After processing all return the updated list
        return newList;
    }
    
/* method to loop through all given sObjects and update them with information from related source records identified by Key ID 
	listTargetRecords : Unfiltered List of records to be updated
	mapIdSource : Map of Id to match against > record to source values from
	sourceOBJ : API name of the object type the source fields are from
    targetOBJ : API name of the object type the target fields are from
    matchingField : API name of the target field which is matched against the mapIdSource key
*/
    public List<sObject> listsObjectToUpdate(List<sObject> listTargetRecords, Map<Id, sObject> mapIdSource, String sourceOBJ, String targetOBJ, String matchingField){
        List<sObject> newList = new List<sObject>();
        // if mapIdSource has no contents there is no need to perform any updates
        // OR if MapCOGSDataFields does not contain Opportunity_COGS_Junction__c > COGS_2_0__c fields there are no updates
        if(
            mapIdJunction == NULL || mapIdJunction.isEmpty()
            || MapCOGSDataFields == NULL || MapCOGSDataFields.isEmpty()
            || !MapCOGSDataFields.containsKey(sourceOBJ) 
            || MapCOGSDataFields.get(sourceOBJ) == NULL || MapCOGSDataFields.get(sourceOBJ).isEmpty()
            || !MapCOGSDataFields.get(sourceOBJ).containsKey(targetOBJ)
          ){
            return newList; // just return the empty list
        }
        // Loop through the target records and populate the fields if data was found.
        try{
            for( sObject so : listTargetRecords ){
                // Create a sObject, when doing so with the ID we ceate a new instance of the object which we can populate later 
                sObject sObj = Schema.getGlobalDescribe().get(targetOBJ).newSObject() ;
                sObj.Id=so.Id;
                Id match = (Id) so.get(matchingField); // Casting as an id otherwise APEX does not know what type of data is being returned
                if( mapIdJunction.containsKey(match) ){
                    sObject sourceRecord = mapIdJunction.get(match);
                    // for each 
                    for(String sourceField:MapCOGSDataFields.get(sourceOBJ).get(targetOBJ).keySet()){
                        String targetfield = MapCOGSDataFields.get(sourceOBJ).get(targetOBJ).get(sourceField);
                        sObj.put(targetfield,sourceRecord.get(sourceField));
                    }
                    newList.add(sObj); // write each of the new fields to the sObject
                }
            }
        } catch (Exception e) { // Write error log
            logger.logMessage('COGS_Package_trigger_handler', 'listsObjectToUpdate', e.getMessage(), targetOBJ, '', '');
        }
        // After processing all return the updated list
        return newList;
    }
    
/* Method to update related records once a COGS Line Item is marked as "Sold". 
   The decision was made to simply force updates to all related records regardless of whether they need to be updated or not. 
   This was done to limit the amount of logic needed in this trigger.
*/
    public void updateSoldCOGS(List<COGS_2_0_Line_Item__c> listLI){
        // Set of Headers associated to the updated Line Items. Used to find all Line Items associated to the same Packages/Headers.
        Set<Id> setPackageIds = new Set<Id>();
        Set<Id> setHeaderIds = new Set<Id>();
        Set<Id> setUpdatedHeaderIds = new Set<Id>();
                
        //transient Map used to determine which Packages are associated to each Header
		Map<Id, Set<Id>> mapHeaderToPackages = new Map<Id, Set<Id>>();
        
        // Maps to contain the eventual Sold boolean values to be assigned to the packages and headers
        Map<Id, Boolean> mapPackageSold = new Map<Id, Boolean>();
        // Map<Id, Boolean> mapHeaderSold = new Map<Id, Boolean>();
        
        // Three collections that we will eventually be performing updates on
        List<COGS_2_0_Package__c> listUpdatedPackages = new List<COGS_2_0_Package__c>();
        List<COGS_2_0__c> listUpdatedHeaders = new List<COGS_2_0__c>();
        List<Opportunity_COGS_Junction__c> listUpdatedJunctions = new List<Opportunity_COGS_Junction__c>();
        
/* Logic begins: */
        
        // Loop through the updated records, get all Package and Header IDs
        for(COGS_2_0_Line_Item__c l:listLI){
            setPackageIds.add(l.COGS_Package__c);
            setHeaderIds.add(l.Header_Id__c);
            setUpdatedHeaderIds.add(l.Header_Id__c);
        }
        system.debug('setPackageIds ==> '+setPackageIds);
        system.debug('setHeaderIds ==> '+setHeaderIds);
        
        // Query all Line Items associated to the indicated setPackageIds return isSold, setPackageIds, package Sold value, header Sold value, and setHeaderIds
        for(COGS_2_0_Line_Item__c lI: [
            SELECT Id, Sold__c, COGS_Package__c, COGS_Package__r.COGS__c 
            FROM COGS_2_0_Line_Item__c 
            WHERE COGS_Package__r.COGS__c IN :setHeaderIds
        ]){
            system.debug('Line Item ==> '+li);
            // For each record the Package 'Sold' Boolean, should be the result of (currentLine.Sold && prior located value)
            if(!mapPackageSold.containsKey(lI.COGS_Package__c)){ // If we don't contain the key just insert the current Sold boolean
                mapPackageSold.put(lI.COGS_Package__c, lI.Sold__c);
            } else { // if the key is found update the current value
                mapPackageSold.put(lI.COGS_Package__c, mapPackageSold.get(lI.COGS_Package__c) && lI.Sold__c);
            }
            // also create a Map that contains the Header ID and the IDs of all packages associated to it.
            if(!mapHeaderToPackages.containsKey(lI.COGS_Package__r.COGS__c)){
                mapHeaderToPackages.put(lI.COGS_Package__r.COGS__c, new Set<Id>());
            }
            mapHeaderToPackages.get(lI.COGS_Package__r.COGS__c).add(lI.COGS_Package__c);
        }
        
        // For each Package and Header found above instance a copy with the approproate "Sold" boolean
        for(Id i:mapPackageSold.keyset()){ // Packages
            listUpdatedPackages.add(new COGS_2_0_Package__c(Id = i, Sold__c = mapPackageSold.get(i)));
            setPackageIds.remove(i); // Remove the package from the initially found set
            system.debug('package ==> '+i+'' +mapPackageSold.get(i));
        }
        for(Id pId:setPackageIds){ // if after processing all remaining Lines there were Packages with no remaining Lines mark them as unsold
            listUpdatedPackages.add(new COGS_2_0_Package__c(Id = pId, Sold__c = false));
        }
        
        // Headers, mark as Sold if Any related Packages are Sold
        for(Id headerId:mapHeaderToPackages.keyset()){ 
            boolean isSold = false;
            for(Id packageId:mapHeaderToPackages.get(headerId)){
                isSold = isSold || mapPackageSold.get(packageId);
                system.debug('mapPackageSold.get(packageId) ==> '+mapPackageSold.get(packageId));
            }
            listUpdatedHeaders.add(new COGS_2_0__c(Id = headerId, Package_Sold__c = isSold));
            setHeaderIds.remove(headerId); // Remove the header from the initially found set
            system.debug('header ==> '+headerId+'' +isSold);
        }
        for(Id hId:setHeaderIds){ // if after processing all remaining Lines there were Packages with no remaining Lines mark them as unsold
            listUpdatedHeaders.add(new COGS_2_0__c(Id = hId, Package_Sold__c = false));
        }
        
        
        // Update Packages and Headers 
        try{
            if(!listUpdatedPackages.isEmpty()){
                update listUpdatedPackages;
            }
            if(!listUpdatedHeaders.isEmpty()){
                update listUpdatedHeaders;
            }
        } catch (Exception e) { // Write error log
            logger.logMessage('COGS_Package_trigger_handler', 'updateSoldCOGS', e.getMessage(), 'COGS_2_0_Line_Item__c', '', '');
        }
        
        // COGS Junction: Update Records, stick them in a collection for now : listUpdatedJunctions
        
        for(Opportunity_COGS_Junction__c j:[
            SELECT Id, Package_Sold__c, COGS__c, COGS__r.Package_Sold__c
            FROM Opportunity_COGS_Junction__c 
            WHERE COGS__c IN :setUpdatedHeaderIds
        ]){
            j.Package_Sold__c = j.COGS__r.Package_Sold__c;
            listUpdatedJunctions.add(j);
        }
		// Update Junctions
        try{
            if(!listUpdatedJunctions.isEmpty()){
                update listUpdatedJunctions;
            }
        } catch (Exception e) { // Write error log
            logger.logMessage('COGS_Package_trigger_handler', 'updateSoldCOGS', e.getMessage(), 'COGS_2_0_Line_Item__c', '', '');
        }
    }
    
    /* On deletion of junctions remove the Sold and Related COGS flags from the related Opportunities */
    public void COGSJunctionAfterDelete(Map<Id, Opportunity_COGS_Junction__c> oldMap){
        // Function to remove Sold and count flags from the Opportunity when the Junction is deleted.
        // Loop through all the junctions, gather the Opportunity IDs
        // Add prototype Opportunities to a Set, overwriting the COGS_Related__c and COGS_Sold__c values to FALSE
        Set<Opportunity> setOpptyToupdate = new Set<Opportunity>();
        for(Opportunity_COGS_Junction__c j: oldMap.values()){
            setOpptyToupdate.add(new Opportunity(Id=j.Opportunity__c,COGS_Related__c=false, COGS_Sold__c=false ));
        }
        try{
            if(!setOpptyToupdate.isEmpty()){
                update new List<Opportunity>(setOpptyToupdate);
            }
        } catch (Exception e) { // Write error log
            logger.logMessage('COGS_Package_trigger_handler', 'COGSJunctionAfterDelete', e.getMessage(), 'Opportunity_COGS_Junction__c', '', '');
        }
    }
    
    public void COGSHeaderAfterUpdate(Map<Id, COGS_2_0__c> newMap, Map<Id, COGS_2_0__c> oldMap) {
        
        // if the Header is changed to Canceled then add the Header Id to a Set
        Set<Id> setCanceledHeaders = new Set<Id>();
        for(Id i:newMap.keySet()){
            COGS_2_0__c o = oldMap.get(i);
            COGS_2_0__c n = newMap.get(i);
            if(n.All_Cogs_Canceled__c && (n.All_Cogs_Canceled__c != o.All_Cogs_Canceled__c)){
                setCanceledHeaders.add(i);
            }
        }
        // Find all Sold__c == TRUE Line Items associated to those Headers that were canceled and set Sold__c to FALSE
        List<COGS_2_0_Line_Item__c> listUpdatedLines = new List<COGS_2_0_Line_Item__c>();
        for(COGS_2_0_Line_Item__c lI: [
            SELECT Id, Sold__c 
            FROM COGS_2_0_Line_Item__c 
            WHERE COGS_Package__r.COGS__c IN :setCanceledHeaders
            AND Sold__c = true
        ]){
            lI.Sold__c = false;
            listUpdatedLines.add(li);
        }
		
        // Update Junctions
        try{
            if(!listUpdatedLines.isEmpty()){
                update listUpdatedLines;
            }
        } catch (Exception e) { // Write error log
            logger.logMessage('COGS_Package_trigger_handler', 'COGSHeaderAfterUpdate', e.getMessage(), 'COGS_2_0_Line_Item__c', '', '');
        }
        
        // Packages, Headers, and Junctions should recalculate attomatically due to recursive Trigger calculation.
    }
}