@isTest
private class COGS_TestOpportunityOwner{
 static testMethod void COGS_TestOpportunityOwnerMethod(){
 
    String RT_CAC = 'COGS Approval Create';
    String OBJ_CASE = 'Case';
    
    List<RecordType> recordID = [select id from RecordType where 
    name = :RT_CAC and sObjecttype = :OBJ_CASE limit 1];
    
    Opportunity oppObj = new Opportunity(
      Target__c = 'abc',
      Name = 'def',
      StageName = 'hello',
      CloseDate = system.today()
    );
    insert oppObj;
    
    Case caseObj = new Case(
      Opportunity__c = oppObj.id,
      RecordTypeId = recordId[0].id
    );
    insert caseObj;
  }
}