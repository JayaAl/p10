@isTest
private class COGS_TestUpdateOpportunityOwner{
 static testMethod void COGS_TestUpdateOpportunityOwnerMethod(){
 
    String RT_CAC = 'COGS Approval Details';
    String OBJ_CASE = 'Case';
    
    List<RecordType> recordID = [select id from RecordType where 
    name = :RT_CAC and sObjecttype = :OBJ_CASE limit 1];
    
    Opportunity oppObj = new Opportunity(
      Target__c = 'abc',
      Name = 'def',
      StageName = 'hello',
      CloseDate = system.today()
    );
    insert oppObj;
    system.debug('oppObj'+ oppObj);
    
    Case caseObj = new Case(
      Opportunity__c = oppObj.id,
      RecordTypeId = recordId[0].id
    );
    insert caseObj;
    system.debug('caseObj'+ caseObj);   
    
    /*Profile p = [select id from Profile where name like 'System Administrator%' limit 1];
    User u = new User(LastName='Test User',alias = 'test',email='test@testorg.com',emailencodingkey='UTF-8',
                           languagelocalekey='en_US',localesidkey='en_US',timezonesidkey='America/Los_Angeles', 
                           username='testuser@testorg.com',profileId = p.Id);                         
    insert u;*/
    User u = [Select Id from User where IsActive = true and Profile.Name = 'System Administrator' limit 1];
    system.debug('User'+u);
    oppObj.OwnerId = u.Id;
    update oppObj;
    system.debug('oppObj'+oppObj);
 }
}