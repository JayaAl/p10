public class COGS_Util{

    public static final String RT_CAC = 'COGS_Approval_Details';
    
    public static Boolean hasChanges(String field, SObject oldRecord, SObject newRecord) {
       if (oldRecord == null) {
           return true;
       }
       System.debug('oldRecord.get(field)'+oldRecord.get(field));
        System.debug('newRecord.get(field)'+newRecord.get(field));
       return (oldRecord.get(field) != newRecord.get(field));
    }
    
          
}