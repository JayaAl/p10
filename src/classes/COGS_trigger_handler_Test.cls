@isTest
public class COGS_trigger_handler_Test {

/* functions to create test records */
    // Create COGS Header
    public COGS_2_0__c createHeader(){
        COGS_2_0__c c = generateHeader();
        insert c;
        return c;
    }
    public COGS_2_0__c generateHeader(){
        COGS_2_0__c c = new COGS_2_0__c();
        return c;
    }
        
    // Create one COGS Package
    public COGS_2_0_Package__c createPackage(COGS_2_0__c h){
        COGS_2_0_Package__c c = generatePackage(h);
        insert c;
        return c;
    }
    public COGS_2_0_Package__c generatePackage(COGS_2_0__c h){
        COGS_2_0_Package__c c = new COGS_2_0_Package__c(
        	COGS__c = h.Id
        );
        return c;
    }
        
    // Create one COGS Line Item
    public COGS_2_0_Line_Item__c createLine(COGS_2_0_Package__c p){
        COGS_2_0_Line_Item__c c = generateLine(p);
        insert c;
        return c;
    }
    public COGS_2_0_Line_Item__c generateLine(COGS_2_0_Package__c p){
        COGS_2_0_Line_Item__c c = new COGS_2_0_Line_Item__c(
        	COGS_Package__c = p.Id,
            Cost__c = 0
        );
        return c;
    }
    
    // Create one Opportunity COGS junction
	public Opportunity_COGS_Junction__c createJunction(Opportunity o, COGS_2_0__c h){
        Opportunity_COGS_Junction__c c = generateJunction(o,h);
        insert c;
        return c;
    }
    public Opportunity_COGS_Junction__c generateJunction(Opportunity o, COGS_2_0__c h){
        Opportunity_COGS_Junction__c c = new Opportunity_COGS_Junction__c(
        	Opportunity__c = o.Id, 
            COGS__c = h.Id
        );
        return c;
    }
    

/* test methods */
    private static testmethod void testCreateCOGS(){
        Account a = UTIL_TestUtil.createAccount();
        Opportunity o = UTIL_TestUtil.createOpportunity(a.Id);
        
        COGS_trigger_handler_Test test = new COGS_trigger_handler_Test();
        COGS_2_0__c h = test.createHeader();
        COGS_2_0_Package__c p = test.createPackage(h);
        COGS_2_0_Line_Item__c l = test.createLine(p);
        Opportunity_COGS_Junction__c j = test.createJunction(o, h);
    }
    
}