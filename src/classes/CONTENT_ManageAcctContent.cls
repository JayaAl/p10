public with sharing class CONTENT_ManageAcctContent extends CONTENT_ManageContent {

    
    public CONTENT_ManageAcctContent(ApexPages.StandardController controller){
        super(controller);                          
    }

    
    public override void initializeWorkspaceList(){  
        Map<String,Workspace_Account__c> workspaceIdMap = Workspace_Account__c.getAll();      
        
        List<SelectOption> withoutRT = new List<SelectOption>();
        List<SelectOption> byRT = new List<SelectOption>();
        Boolean RTFound = false;
        
        //List<ContentWorkspace> workspaceList = new List<ContentWorkspace>([Select Id,Name from ContentWorkspace]);
        for (String workspaceId : workspaceIdMap.keySet()){
            Workspace_Account__c ws = workspaceIdMap.get(workspaceId);
            SelectOption thisSO = new SelectOption(workspaceIdMap.get(workspaceId).Workspace_ID__c,workspaceIdMap.get(workspaceId).Name);
            if(ws.ShowForAll__c){
            	withoutRT.add(thisSO);
                byRT.add(thisSO);
            } else if(ws.RecordTypeNames__c == '' || ws.RecordTypeNames__c == null ){
            	withoutRT.add(thisSO);
            } else if (ws.RecordTypeNames__c.Contains(String.valueOf( obj.get('RecordTypeId')))){
                byRT.add(thisSO);
                RTFound = true;
            }
        } 
        
        if(RTFound){
            this.workspaceOptions = byRT;
        } else {
            this.workspaceOptions = withoutRT;
        }
        
    }

    // populate field map
    public override Map<String,String> getFieldMapForFolder(){
        Map<String,String> fieldMap = new Map<String,String>();
        AccountFolderContent__c folderFieldMap = AccountFolderContent__c.getValues(this.selectedFolder);
        if(folderFieldMap.Field_Map1__c != null){
            List<String> splitList = folderFieldMap.Field_Map1__c.split(',');
            for(String split : splitList){
                fieldMap.put(split.split('=')[0],split.split('=')[1]);
            }
        }
        if(folderFieldMap.Field_Map2__c != null){
            List<String>  splitList = folderFieldMap.Field_Map2__c.split(',');
            for(String split : splitList){
                fieldMap.put(split.split('=')[0],split.split('=')[1]);
            }
        }
        return fieldMap;
    }
    
    // This method is for uploading the Content and refreshing the content list
    public override void upload(){
        if(this.selectedWorkspaceID == null || this.selectedFolder == null || this.file == null){
            ApexPages.Message myMsg = new ApexPages.Message(ApexPages.Severity.ERROR, 'Please fill in all fields');
            ApexPages.addMessage(myMsg);            
            return;
        }
       super.upload();    
    }   
    
    public override String getLookupFieldName(){
        return 'Account__c';
    }

}