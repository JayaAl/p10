/*
Developer: Ralph Callaway <ralph@callawaycloudconsulting.com>
Description:
	Test methods for CONTENT_ManageCaseContent.cls
Dependencies:
	Due to lack of support for creating content workspaces in test methods, this test
	uses the 'Campaign Documentation' workspace in testing.  Any workspace that a
	system administrator can publish to will work. See setupContent() to change the
	workspace being used if needed.
*/
@isTest 
private class CONTENT_ManageCaseContent_Test {

	/* Test Variables */
	
	static ContentVersion insertionOrder;
	static ContentVersion mediaPlan;
	static ContentVersion noFolderContent;
	static Case testCase;
	
	/* Before Test */

	// Creates an opportunity and generates a related case.  Content can be 
	// generated if needed by calling setupContent()
	static {
		// create test case and link to an opportunity
		Account testAccount = UTIL_TestUtil.createAccount();
		Opportunity testOpportunity = UTIL_TestUtil.createOpportunity(testAccount.id);
		testCase = UTIL_TestUtil.generateCase();
		testCase.opportunity__c = testOpportunity.id;
	}
	
	/* Test Methods */
	
	@isTest
	private static void testDigitalIOCasesOnlyShowInsertionOrders() {
		// Insert all content and case with io approval record type
		testCase.recordTypeId = UTIL_CaseRecordTypeHelper.getRecordTypeId(CONTENT_ManageCaseContent.IO_APPROVAL);
		insert testCase;
		setupContent();
		
		// load extension
		Test.startTest();
		CONTENT_ManageCaseContent extension = loadExtension();
		Test.stopTest();
		
		// check that only the insertion order was loaded
		Map<Id, ContentVersion> contentMap = new Map<Id, ContentVersion>(extension.contentList);
		system.assert(contentMap.containsKey(insertionOrder.id), contentMap);
		system.assert(!contentMap.containsKey(mediaPlan.id), contentMap);
		system.assert(!contentMap.containsKey(noFolderContent.id), contentMap); 
	}
	
	@isTest
	private static void testBroadcastIOCasesOnlyShowInsertionOrders() {
		// Insert all content and case with io approval record type
		testCase.recordTypeId = UTIL_CaseRecordTypeHelper.getRecordTypeId(CONTENT_ManageCaseContent.BROADCAST_IO);
		insert testCase;
		setupContent();
		
		// load extension
		Test.startTest();
		CONTENT_ManageCaseContent extension = loadExtension();
		Test.stopTest();
		
		// check that only the insertion order was loaded
		Map<Id, ContentVersion> contentMap = new Map<Id, ContentVersion>(extension.contentList);
		system.assert(contentMap.containsKey(insertionOrder.id), contentMap);
		system.assert(!contentMap.containsKey(mediaPlan.id), contentMap);
		system.assert(!contentMap.containsKey(noFolderContent.id), contentMap); 
	}
	
	@isTest
	private static void testYieldApprovalCaseOnlyShowMediaPlans() {
		// insert all content and case with yield approval record type
		testCase.recordTypeId = UTIL_CaseRecordTypeHelper.getRecordTypeId(CONTENT_ManageCaseContent.YIELD_APPROVAL);
		insert testCase;
		setupContent();
		
		// load extension 
		Test.startTest();
		CONTENT_ManageCaseContent extension = loadExtension();
		Test.stopTest();
		
		// check that only media plan was loaded
		Map<Id, ContentVersion> contentMap = new Map<Id, ContentVersion>(extension.contentList);
		system.assert(contentMap.containsKey(mediaPlan.id), contentMap);
		system.assert(!contentMap.containsKey(insertionOrder.id), contentMap);
		system.assert(!contentMap.containsKey(noFolderContent.id), contentMap);
	}
	
	@isTest
	private static void testNonFolderMappedRecordTypeCasesDisplayAllContent() {
		// Create case with a non folder mapped record type and insert all content
		for(Schema.RecordTypeInfo rtInfo : UTIL_SchemaHelper.getRecordTypeInfos('Case')) {
			Id recordTypeId = rtInfo.getRecordTypeId();
			if(rtInfo.isAvailable()) {
				String developerName = UTIL_CaseRecordTypeHelper.getRecordTypeDevName(recordTypeId);
				if(!CONTENT_ManageCaseContent.FOLDER_MAP.containsKey(developerName)) {
					testCase.recordTypeId = recordTypeId;
					break;
				}
			}
		}
		insert testCase;
		setupContent();
		
		// bail if we didn't find a record type (meaning all available record types are matched)
		if(testCase.recordTypeId != null) {
			
			// load extension
			Test.startTest();
			CONTENT_ManageCaseContent extension = loadExtension();
			Test.stopTest();
			
			// check that all content loaded
			Map<Id, ContentVersion> contentMap = new Map<Id, ContentVersion>(extension.contentList);
			system.assert(contentMap.containsKey(noFolderContent.id), contentMap);
			system.assert(contentMap.containsKey(mediaPlan.id), contentMap);
			system.assert(contentMap.containsKey(insertionOrder.id), contentMap);
		}
	}
	
	@isTest
	private static void testWarningMessageDisplayedForBroadcastCaseIfNoInsertionOrder() {
		// insert case with broadcast io record type
		testCase.recordTypeId = UTIL_CaseRecordTypeHelper.getRecordTypeId(CONTENT_ManageCaseContent.BROADCAST_IO);
		insert testCase;
		
		// load extension
		Test.startTest();
		CONTENT_ManageCaseContent extesion = loadExtension();
		Test.stopTest();
		
		// validate we have a warning message
		system.assert(ApexPages.hasMessages(ApexPages.Severity.WARNING), ApexPages.getMessages());
	}
	
	@isTest
	private static void testWarningMessageDisplayedForIOApprovalIfNoInsertionOrder() {
		// insert case with io approval record type
		testCase.recordTypeId = UTIL_CaseRecordTypeHelper.getRecordTypeId(CONTENT_ManageCaseContent.IO_APPROVAL);
		insert testCase;
		
		// load extension
		Test.startTest();
		CONTENT_ManageCaseContent extension = loadExtension();
		Test.stopTest();
		
		// validate we have a warning message
		system.assert(ApexPages.hasMessages(ApexPages.Severity.WARNING), ApexPages.getMessages());
	}
	
	@isTest
	private static void testNoWarningMessageForNonIOApprovalRecordTypesIfNoInsertionOrder() {
		// insert case with yield approval record type (any record type other than an 
		// io approval record type) will work
		testCase.recordTypeId = UTIL_CaseRecordTypeHelper.getRecordTypeId(CONTENT_ManageCaseContent.YIELD_APPROVAL);
		insert testCase;
		
		// load extension
		Test.startTest();
		CONTENT_ManageCaseContent extension = loadExtension();
		Test.stopTest();
		
		// validate no warning message
		system.assert(!ApexPages.hasMessages(ApexPages.Severity.WARNING), ApexPages.getMessages());
	}
	
	/* Test Helpers */
	
	private static CONTENT_ManageCaseContent loadExtension() {
		testCase = [select opportunity__c, recordType.developerName from Case where id = :testCase.id];
		ApexPages.StandardController controller = new ApexPages.StandardController(testCase);
		return new CONTENT_ManageCaseContent(controller);
	}
	
	// Create content related to opportunity.  Note that content needs to be inserted and 
	// associated with a workspace before we can set the any custom fields.  Content
	// folder is set to correspond with folders used in controller.
	private static void setupContent() {
		ContentWorkspace workspace = [select DefaultRecordTypeId from ContentWorkspace where name = 'Campaign Documentation'];
		insertionOrder = UTIL_TestUtil.generateContentVersion();
		insertionOrder.recordTypeId = workspace.defaultRecordTypeId;
		mediaPlan = insertionOrder.clone();
		noFolderContent = insertionOrder.clone();
		List<ContentVersion> allContent = new ContentVersion[] { insertionOrder, mediaPlan, noFolderContent };
		insert allContent;
		allContent = [select contentDocumentId from ContentVersion where id in :allContent];
		List<ContentWorkspaceDoc> workspaceDocs = new List<ContentWorkspaceDoc>();
		for(ContentVersion content : allContent) {
			content.opportunity__c = testCase.opportunity__c;
			workspaceDocs.add(new ContentWorkSpaceDoc(
				  contentWorkspaceId = workspace.id
				, contentDocumentId = content.contentDocumentId
			));
			if(content.id == insertionOrder.id) {
				content.folder__c = CONTENT_ManageCaseContent.INSERTION_ORDERS;
			} else if(content.id == mediaPlan.id) {
				content.folder__c = CONTENT_ManageCaseContent.MEDIA_PLANS;
			}
		}
		insert workspaceDocs;
		update allContent;
	}
}