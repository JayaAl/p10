public abstract with sharing class CONTENT_ManageContent {
    public sObject obj {get;set;}
    public List<ContentVersion> existingContent {public get;private set;}
    public String filename {get;set;}
    public List<SelectOption> workspaceOptions {public get; public set;}
    public ID selectedWorkspaceID {get;set;}    
    public Transient Blob file {get;set;}
    public List<SelectOption> folderOptions {public get; private set;}
    public String selectedFolder {get;set;}
    public String searchFolder {get;set;}   
    public List<SelectOption> searchFolderOptions {public get; private set;}
   	private RecordType recordType;
    
    public CONTENT_ManageContent(ApexPages.StandardController controller){
        this.obj = controller.getRecord();
  		String recordTypeName = Manage_Content_Record_Type_Name__c.getValues('RecordTypeName').Record_Type_Name__c;
    	this.recordType = [Select Id From RecordType  where SobjectType = 'ContentVersion' and Name = :recordTypeName.trim()];        
        initializeWorkspaceList();      
        initializeFolderOptions();
        initializeSearchFolderOptions();
        setExistingContent();                            
    }
    public abstract void initializeWorkspaceList();
    
    public void initializeFolderOptions(){
        this.folderOptions = new List<SelectOption>();
        List<Schema.Picklistentry> picklistValues =  Schema.sObjectType.ContentVersion.fields.Folder__c.getPicklistValues();
        for(Schema.Picklistentry entry : picklistValues){
            if(!entry.isActive()) {
                continue;
            }
            this.folderOptions.add(new SelectOption(entry.getValue(),entry.getLabel()));
        }
    }
       
    public void initializeSearchFolderOptions(){
        this.searchFolderOptions = new List<SelectOption>(this.folderOptions);
        this.searchFolderOptions.add(new SelectOption('All','All'));
        this.searchFolder = 'All';
    }
    
    public abstract String getLookupFieldName();
    
    public void setExistingContent(){
        String queryString 
            = 'Select ContentDocumentId,PathOnClient,Title,Folder__c,ContentModifiedDate, CreatedDate from ContentVersion where '+getLookupFieldName()+' = \''+this.obj.get('Id')+'\' and IsLatest = true';//Modified by Lakshman to get Content CreatedDate 
        if(!'All'.equals(this.searchFolder)){  
            queryString = queryString + ' and Folder__c = \''+ this.searchFolder + '\'';
        }
        
        this.existingContent = new List<ContentVersion>();
        for(ContentVersion content: Database.query(queryString)){
            this.existingContent.add(content);
        } 
    }     
    
    public ContentVersion createContentVersion(){
        ContentVersion contentVersion = new ContentVersion();
        contentVersion.VersionData = file;
        //String escapedFileName = escapeFileName(this.fileName);      
        contentVersion.PathOnClient = this.fileName;
        contentVersion.Title = this.fileName;
        contentVersion.RecordTypeId = this.recordType.Id;
        return contentVersion;      
    }   
    
    private String escapeFileName(String fileNameLocal){
    	String escapedFileName;
    	if(fileNameLocal == null) {
    		return null;
    	}
    	if(''.equals(fileNameLocal.trim())){
    		return '';
    	}
    	List<String> fileNameParts = fileNameLocal.split('\\x2E',-1);
    	for(Integer i=0; i < (fileNameParts.size() - 1);i++){
    		if(i==0){
    			escapedFileName = fileNameParts.get(i);
    		}else{
    			escapedFileName = escapedFileName + '_' + fileNameParts.get(i);
    		}
    		System.debug('bhak'+escapedFileName);
    	}
    	return escapedFileName;
    }
    
    public ContentWorkspaceDoc createContentWorkspaceDoc(ContentVersion contentVersion){
        contentVersion = [Select ContentDocumentId from ContentVersion where Id = :contentVersion.Id];
        ContentWorkspaceDoc wd = new ContentWorkspaceDoc();
        wd.ContentWorkspaceId = this.selectedWorkspaceID; 
        wd.ContentDocumentId = contentVersion.ContentDocumentId; 
        return wd;      
    }
    
    public abstract Map<String,String> getFieldMapForFolder();
    
    // Initializes the Content Version based on Field Map.
    public void initializeContentVersionFields(ContentVersion contentVersion, Map<String,String> fieldMap){
        String fieldSet = '';
        for(String key : fieldMap.keySet()){
            fieldSet = fieldSet + key + ',';
        } 
        this.obj = Database.query('Select '+fieldSet.substring(0,fieldSet.length()-1)+' from '+obj.getsObjectType()+' where Id = \''+this.obj.get('Id') + '\'');
        for(String key : fieldMap.keySet()){
            contentVersion.put(fieldMap.get(key),(String)this.obj.get(key));
        } 
    }

    // This method is for uploading the Content and refreshing the content list
    public virtual void upload(){
        
        Map<String,String> fieldMap;
        try{
            fieldMap = getFieldMapForFolder();
        }catch(Exception e){
            ApexPages.Message myMsg = new ApexPages.Message(ApexPages.Severity.ERROR, 'Error Parsing Field Mapping Custom Setting');
            ApexPages.addMessage(myMsg);
            System.debug(e);
            return;         
        }
        
        ContentVersion contentVersion;
        try{
            contentVersion = createContentVersion();
            insert contentVersion;
            
            ContentWorkspaceDoc workspaceDoc = createContentWorkspaceDoc(contentVersion);
            insert workspaceDoc;
        }catch(Exception e){
            ApexPages.Message myMsg = new ApexPages.Message(ApexPages.Severity.ERROR, e.getMessage());
            ApexPages.addMessage(myMsg);
            System.debug(e);            
        }
        

        
        initializeContentVersionFields(contentVersion, fieldMap);  
        contentVersion.Folder__c = this.selectedFolder;
        update contentVersion;  
        setExistingContent();
    }     
}