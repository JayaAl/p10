/**
* This class is responsible to display the content related to IO.
* @Author: Tom Marshall (Pandora).
* Cloned from CONTENT_ManageCaseContent
*/
public with sharing class CONTENT_ManageIOContent {

    public CONTENT_ManageIOContent(IO_Detail controller) {

    }


  public List<ContentVersion> contentList { get; private set; }
  
  // Content Folder Name Constants
  public static final String INSERTION_ORDERS = 'Insertion Orders';
  public static final String MEDIA_PLANS = 'Media Plans';
  
  // Record Type Dev Name Constants
  public static final String BROADCAST_IO = 'Broadcast_IO';
  public static final String IO_APPROVAL = 'Legal_IO_Approval_Request';
  public static final String YIELD_APPROVAL = 'Yield_Approval_Request';

/*  
  public static final Map<String, String> FOLDER_MAP = new Map<String, String> {
      BROADCAST_IO => INSERTION_ORDERS
    , IO_APPROVAL => INSERTION_ORDERS
    , YIELD_APPROVAL => MEDIA_PLANS
  };
*/
  
  public CONTENT_ManageIOContent(ApexPages.StandardController controller){  
    IO_Detail__c record = (IO_Detail__c) controller.getRecord();
    
    // String folder = FOLDER_MAP.get(record.recordType.developerName);
    String folder = INSERTION_ORDERS;
    
    /* if(folder != null) { */
      contentList = [
        select
            contentDocumentId
          , title
          , folder__c
          , contentModifiedDate
        from ContentVersion
        where opportunity__c = :record.opportunity__c
        and isLatest = true
        and folder__c = :folder
      ];
    /*} else {
      contentList = [
        select
            contentDocumentId
          , title
          , folder__c
          , contentModifiedDate
        from ContentVersion
        where opportunity__c = :record.opportunity__c
        and isLatest = true
      ];  
    }*/
    
    // display a warning message for io approval cases if there is no related content
    /* if(
      record.recordType.developerName == IO_APPROVAL
     || record.recordType.developerName == BROADCAST_IO
    ) { */
      if(contentList.isEmpty()) {
        ApexPages.addMessage(new ApexPages.Message(
            ApexPages.Severity.WARNING
          , 'No insertion order associated to this IO'
        ));
      }
    /* } */
  }
}