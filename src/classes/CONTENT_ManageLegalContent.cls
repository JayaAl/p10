public with sharing class CONTENT_ManageLegalContent {

/*
* This class is responsible to manage the content related to Legal_Contract_Database__c.
* @Author: Bharath Kumar Gadiyaram
* @Modified by: Shapoor Hashemi 3/9/11
*/
    
    public Legal_Contract_Database__c lcdb {get;set;}
    public List<ContentVersion> existingContent {public get;private set;}
    public String filename {get;set;}
    public List<SelectOption> workspaceOptions {public get; private set;}
    public ID recordTypeID {get; set;}
    public ID selectedWorkspaceID {get;set;}    
    public Transient Blob file {get;set;}
    public List<SelectOption> folderOptions {public get; private set;}
    public String selectedFolder {get;set;}
    public String searchFolder {get;set;}   
    public List<SelectOption> searchFolderOptions {public get; private set;}
   	
   	public String searchWorkspace {get; set;}
    public List<SelectOption> searchWorkspaceOptions {public get; private set;}
    
    public CONTENT_ManageLegalContent()
    {
        
    }        
    
    public CONTENT_ManageLegalContent(ApexPages.StandardController controller){
        this.lcdb = (Legal_Contract_Database__c) controller.getRecord();
        initializeWorkspaceList(); 
        initializeSearchFolderOptions();
        initializeSearchWorkspaceOptions();
        setExistingContent();                          
    }
    
    public void initializeSearchFolderOptions(){
        this.searchFolderOptions = new List<SelectOption>(this.folderOptions);
        this.searchFolderOptions.add(new SelectOption('All','All'));
        this.searchFolder = 'All';
    }
    
    public void initializeWorkspaceList(){
        Map<String,CustomSetting_LCDB_Workspace__c> workspaceIdMap = CustomSetting_LCDB_Workspace__c.getAll();      
        if (workspaceIdMap == null || workspaceIdMap.isEmpty()) {
        	ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, 'Please set up the Workspaces.'));
        	return; 
        }      
        this.workspaceOptions = new List<SelectOption>();
        for (String workspaceId : workspaceIdMap.keySet()){
            this.workspaceOptions.add(new SelectOption(workspaceIdMap.get(workspaceId).Workspace_ID__c,workspaceIdMap.get(workspaceId).Name));
            
        }
    }
       
    public void initializeSearchWorkspaceOptions(){
        this.searchWorkspaceOptions = new List<SelectOption>();
        this.searchWorkspaceOptions.add(new SelectOption('All','All Workspaces'));
        this.searchWorkspaceOptions.addAll(this.workspaceOptions);
        this.searchWorkspace = 'All';
    }
    
    public void setExistingContent(){
        String queryString 
            = 'Select ContentDocumentId,PathOnClient,Title,ContentModifiedDate from ContentVersion where Pandora_Legal_Contract_Database__c = \''+this.lcdb.Id+'\' and IsLatest = true';

        if(!'All'.equals(this.searchWorkspace)){  
            queryString = queryString + ' and ContentDocumentId In (Select c.ContentDocumentId From ContentWorkspaceDoc c where ContentWorkspaceId = \'' + this.searchWorkspace + '\')';
        }
        
        queryString += ' Order By ContentModifiedDate DESC';

        this.existingContent = new List<ContentVersion>();
        for(ContentVersion content: Database.query(queryString)){
            this.existingContent.add(content);
        } 
    }   


    // This method is for uploading the Content and refreshing the content list
    public void upload(){
        if(this.selectedWorkspaceID == null || this.file == null){
            ApexPages.Message myMsg = new ApexPages.Message(ApexPages.Severity.ERROR, 'Please fill in all fields');
            ApexPages.addMessage(myMsg);            
            return;
        }      
        try{            
            ContentVersion contentVersion = new ContentVersion();
            contentVersion.VersionData = file;      
            contentVersion.PathOnClient = this.filename;

            List<RecordType> rt = [select id from RecordType where name='Legal' And SobjectType = 'ContentVersion' limit 1];
            contentVersion.RecordTypeId = rt[0].Id;
            insert contentVersion;
            
            contentVersion = [Select ContentDocumentId from ContentVersion where Id = :contentVersion.Id];
            ContentWorkspaceDoc wd = new ContentWorkspaceDoc();
            wd.ContentWorkspaceId = this.selectedWorkspaceID; 
            wd.ContentDocumentId = contentVersion.ContentDocumentId;    
            insert wd;
            
            
            contentVersion.Pandora_Legal_Contract_Database__c = this.lcdb.Id;
            Map<String,String> fieldMap;

            update contentVersion;      
            
            setExistingContent();
        }catch(Exception e){
            ApexPages.Message myMsg = new ApexPages.Message(ApexPages.Severity.ERROR, e.getMessage());
            ApexPages.addMessage(myMsg);
            System.debug(e);
        }   
    }   
}