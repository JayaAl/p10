public with sharing class CONTENT_ManageOpportunityContent extends CONTENT_ManageContent {

    
    public CONTENT_ManageOpportunityContent(ApexPages.StandardController controller){
        super(controller);                          
    }

    
    public override void initializeWorkspaceList(){  
        Map<String,Workspaces_Opportunity__c> workspaceIdMap = Workspaces_Opportunity__c.getAll();      
        this.workspaceOptions = new List<SelectOption>();
        //List<ContentWorkspace> workspaceList = new List<ContentWorkspace>([Select Id,Name from ContentWorkspace]);
        for (String workspaceId : workspaceIdMap.keySet()){
            this.workspaceOptions.add(new SelectOption(workspaceIdMap.get(workspaceId).Workspace_ID__c,workspaceIdMap.get(workspaceId).Name));
        }
    }

    // populate field map
    public override Map<String,String> getFieldMapForFolder(){
        Map<String,String> fieldMap = new Map<String,String>();
        FolderFieldMap__c folderFieldMap = FolderFieldMap__c.getValues(this.selectedFolder);
        if(folderFieldMap.Field_Map1__c != null){
            List<String> splitList = folderFieldMap.Field_Map1__c.split(',');
            for(String split : splitList){
                fieldMap.put(split.split('=')[0],split.split('=')[1]);
            }
        }
        if(folderFieldMap.Field_Map2__c != null){
            List<String>  splitList = folderFieldMap.Field_Map2__c.split(',');
            for(String split : splitList){
                fieldMap.put(split.split('=')[0],split.split('=')[1]);
            }
        }
        return fieldMap;
    }
    
    // This method is for uploading the Content and refreshing the content list
    public override void upload(){
        if(this.selectedWorkspaceID == null || this.selectedFolder == null || this.file == null){
            ApexPages.Message myMsg = new ApexPages.Message(ApexPages.Severity.ERROR, 'Please fill in all fields');
            ApexPages.addMessage(myMsg);            
            return;
        }
       super.upload();    
    }   
    
    public override String getLookupFieldName(){
        return 'Opportunity__c';
    }

}