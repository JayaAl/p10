@isTest
private class CONTENT_TestClass {
    
    public static testMethod void testManageOppContent(){       
        Opportunity opp = createOpportunity();
        insert opp;
        
        ContentVersion contentVersion = createContentVersion();
        insert contentVersion;

        ContentWorkspace workspace = [Select Id from ContentWorkspace where Name = 'Campaign Documentation'];
        
        /*Map<String,Workspaces_Opportunity__c> workspaceIdMap = Workspaces_Opportunity__c.getAll();
        for (String workspaceId : workspaceIdMap.keySet()){
            selectedWorkspaceID = workspaceIdMap.get(workspaceId).Workspace_ID__c;
            System.debug('bhak'+workspaceIdMap.get(workspaceId));
            break;
        }*/
        
        String folderName = getSelectedFolderName();
        FolderFieldMap__c folderContent = FolderFieldMap__c.getValues(folderName);
        if(folderContent == null){
            folderContent = new FolderFieldMap__c();
            folderContent.Name = folderName;
            folderContent.Field_Map1__c = 'Name=Opportunity_Name__c,Id=Opportunity__c';
            folderContent.Field_Map1__c = 'Name=Opportunity_Name__c,Id=Opportunity__c';
            insert folderContent;  
        }      
        
        contentVersion = [Select ContentDocumentId from ContentVersion where Id = :contentVersion.Id];
        ContentWorkspaceDoc wd = createWorkspaceDoc(workspace.Id,contentVersion);
        insert wd;
        
        contentVersion = [Select Id from contentVersion where Id = :contentVersion.Id];
        contentVersion.Opportunity__c = opp.Id;
        update contentVersion;
        
        
        Content_ManageOpportunityContent oppContent = new Content_ManageOpportunityContent((new ApexPages.StandardController(opp)));
        oppContent.selectedWorkspaceID = workspace.Id;
        oppContent.selectedFolder = folderContent.Name;
        oppContent.file = Blob.valueOf('testString');
        oppContent.fileName = 'test.txt';
        oppContent.upload();
    }
    
    public static testMethod void testManageAccountContent(){
        Account acct = new Account();
        acct.Name = 'testbharathkumar';
        insert acct;
        acct = [select Id, RecordTypeId from Account where Id =:acct.Id Limit 1];

        ContentVersion contentVersion = createContentVersion();
        insert contentVersion;

        /*Id selectedWorkspaceID;
        Map<String,Workspace_Account__c> workspaceIdMap = Workspace_Account__c.getAll();
        for (String workspaceId : workspaceIdMap.keySet()){
            selectedWorkspaceID = workspaceIdMap.get(workspaceId).Workspace_ID__c;
            break;
        }*/
        
        ContentWorkspace workspace = [Select Name,Id from ContentWorkspace where Name = 'Campaign Documentation'];
        
        contentVersion = [Select ContentDocumentId from ContentVersion where Id = :contentVersion.Id];
        ContentWorkspaceDoc wd = createWorkspaceDoc(workspace.Id,contentVersion);
        insert wd;
        String folderName = getSelectedFolderName();
        AccountFolderContent__c folderContent = AccountFolderContent__c.getValues(folderName);
        if(folderContent == null){        
            folderContent = new AccountFolderContent__c();
            folderContent.Name = getSelectedFolderName();
            folderContent.Field_Map1__c = 'id=Account__c';
            folderContent.Field_Map1__c = 'id=Account__c';
            insert folderContent;
        }
        contentVersion = [Select Id from contentVersion where Id = :contentVersion.Id];
        contentVersion.Account__c = acct.Id;
        update contentVersion; 
                
        test.startTest();
        Content_ManageAcctContent acctContent = new Content_ManageAcctContent(new ApexPages.StandardController(acct));
        
        acctContent.selectedWorkspaceID = workspace.Id;
        acctContent.selectedFolder = folderContent.Name;
        acctContent.file = Blob.valueOf('testString');
        acctContent.fileName = 'test.txt';
        acctContent.upload();    
        test.stopTest();
 
    }
    
    public static Opportunity createOpportunity(){
        Opportunity opp = new Opportunity();
        opp.Name = 'test';
        opp.StageName = 'Closed';
        opp.CloseDate = Date.today();
        return opp; 
    }
    
    public static String getSelectedFolderName(){
        List<Schema.Picklistentry> picklistValues =  Schema.sObjectType.ContentVersion.fields.Folder__c.getPicklistValues();
        for(Schema.Picklistentry entry : picklistValues){
            return entry.getValue();
        }
        return '';  
    }
    
    public static ContentVersion createContentVersion(){
        ContentVersion contentVersion = new ContentVersion();
        contentVersion.VersionData = Blob.valueOf('testString');      
        contentVersion.PathOnClient = 'test.txt';
        String recordTypeName = Manage_Content_Record_Type_Name__c.getValues('RecordTypeName').Record_Type_Name__c;
        RecordType recordType = [Select Id From RecordType  where SobjectType = 'ContentVersion' and Name = :recordTypeName.trim()];        
        contentVersion.RecordTypeId = recordType.Id;
        return contentVersion;  
    }
    
    public static ContentWorkspaceDoc createWorkspaceDoc(Id selectedWorkspaceID,ContentVersion contentVersion){
        ContentWorkspaceDoc wd = new ContentWorkspaceDoc();
        wd.ContentWorkspaceId = selectedWorkspaceID;
        wd.ContentDocumentId = contentVersion.ContentDocumentId; 
        return wd;  
    }
}