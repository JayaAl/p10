/**
 * This class contains unit tests for validating the behavior of Apex classes
 * and triggers.
 *
 * Unit tests are class methods that verify whether a particular piece
 * of code is working properly. Unit test methods take no arguments,
 * commit no data to the database, and are flagged with the testMethod
 * keyword in the method definition.
 *
 * All test methods in an organization are executed whenever Apex code is deployed
 * to a production organization to confirm correctness, ensure code
 * coverage, and prevent regressions. All Apex classes are
 * required to have at least 75% code coverage in order to be deployed
 * to a production organization. In addition, all triggers must have some code coverage.
 * 
 * The @isTest class annotation indicates this class only contains test
 * methods. Classes defined with the @isTest annotation do not count against
 * the organization size limit for all Apex scripts.
 *
 * See the Apex Language Reference for more information about Testing and Code Coverage.
 */
@isTest
private class CONTENT_Test_ManageLegalContent {

    static testMethod void testEmptyMethod() {
        // TO DO: implement unit test
        CONTENT_ManageLegalContent mlc = new CONTENT_ManageLegalContent();
        mlc = null;
    }
    static testMethod void testUpload() {
        CONTENT_ManageLegalContent mlc = new CONTENT_ManageLegalContent();
        Legal_Contract_Database__c lcdTest = createLCD();
        insert lcdTest;
        ContentVersion contentVersion = createContentVersion();
        User objUser =[Select Id from User where Profile.Name ='System Administrator' AND IsActive=true limit 1];
        //system.runAs(objUser) {
            test.starttest();
            insert contentVersion;
            test.stopTest();
       // }
        Id selectedWorkspaceID;
        Map<String,CustomSetting_LCDB_Workspace__c> workspaceIdMap = CustomSetting_LCDB_Workspace__c.getAll();
        for (String workspaceId : workspaceIdMap.keySet()){
            selectedWorkspaceID = workspaceIdMap.get(workspaceId).Workspace_ID__c;
            break;
        }
        
        contentVersion = [Select ContentDocumentId from ContentVersion where Id = :contentVersion.Id];     
        //ContentWorkspace cw = [Select id from contentWorkspace where name like '%legal%' limit 1];
        //selectedWorkspaceID = cw.id;
        //ContentWorkspaceDoc wd = createWorkspaceDoc(selectedWorkspaceID,contentVersion);
        ContentWorkspaceDoc wd = createWorkspaceDoc('05840000000CdSF',contentVersion);
        insert wd;
        
        contentVersion = [Select Id from contentVersion where Id = :contentVersion.Id];
        contentVersion.Pandora_Legal_Contract_Database__c = lcdTest.Id;
        update contentVersion;
        system.debug('lcdtest: ' + lcdTest);
        Content_ManageLegalContent lcdContent = new Content_ManageLegalContent((new ApexPages.StandardController(lcdTest)));
        lcdContent.selectedWorkspaceID = selectedWorkspaceID;
        
        lcdContent.file = Blob.valueOf('testString');
        lcdContent.fileName = 'test.txt';
        lcdContent.upload();
        
    }    
      public static Legal_Contract_Database__c createLCD(){
        Legal_Contract_Database__c lcd = new Legal_Contract_Database__c();
        lcd.Name = 'test';
        return lcd;  
      }
          
      public static ContentVersion createContentVersion(){
        ContentVersion contentVersion = new ContentVersion();
            contentVersion.VersionData = Blob.valueOf('testString');      
            contentVersion.PathOnClient = 'test.txt';
           // contentVersion.RecordTypeId = '00h40000001PGS2AAO';
            //List<RecordType> rt = [select id from RecordType where name='Legal' limit 1];
            //contentVersion.RecordTypeId = rt[0].Id;
            contentVersion.RecordTypeId ='0124000000013gbAAA';

            return contentVersion;  
      }
      
      public static ContentWorkspaceDoc createWorkspaceDoc(Id selectedWorkspaceID,ContentVersion contentVersion){
            ContentWorkspaceDoc wd = new ContentWorkspaceDoc();
             wd.ContentWorkspaceId = selectedWorkspaceID;
            wd.ContentDocumentId = contentVersion.ContentDocumentId; 
            return wd;  
      }    
}