/**
* ─────────────────────────────────────────────────────────────────────────────────────────────────┐
* This class is controller for CommunityBirst vf page
* ──────────────────────────────────────────────────────────────────────────────────────────────────
* @author         Jaya Alaparthi
* @maintainedBy   Jaya Alaparthi
* @version        1.0
* @created        2017-08-29
* @modified       
* @systemLayer    Service
* @see            ????
* @see            ????
* ──────────────────────────────────────────────────────────────────────────────────────────────────
* @changes
* v1.1            Jaya Alaparthi
* 2017-05-25      
* ──────────────────────────────────────────────────────────────────────────────────────────────────
* @changes
* v1.2            Jaya Alaparthi
* 2017-06-06      
* ─────────────────────────────────────────────────────────────────────────────────────────────────┘
*/
public with sharing class CPQCommunityBirst {

	public String userId;
	public String communityUserAcctId;

	public CPQCommunityBirst() {

		communityUserAcctId = getCommunityUserAccountId(UserInfo.getUserId());
		if(communityUserAcctId.startsWith('Error')) {
			// apex page message
		} else {
			// callout to Birst for token
		}		
	}	

	private String getCommunityUserAccountId(String userId) {

		String communityUserAcctId;

		try {
			
			String birstToknResp = getBirstToken();
			/*String currentUserContactId = [SELECT ContactId 
								FROM User 
								WHERE Id=: userId].ContactId;
			Contact userContact = [SELECT AccountId
								FROM Contact
								WHERE Id =: currentUserContactId];
			communityUserAcctId = userContact.AccountId;*/
			
		} catch(Exception e) {

			communityUserAcctId = 'Error in user Account retrival';
		}
		return communityUserAcctId;
	}
	private String getBirstToken() {

		Http http = new HTTP();
		HttpRequest req = new HttpRequest();
		Map<String,String> bisrtSettings = new Map<String,String>();

		bisrtSettings = getBirstSettings();

		String baseUrl = bisrtSettings.get('TokenEndPoint');
		String returnserveURL = ApexPages.currentPage().getHeaders().get('Host');
		baseUrl += '&birst.ssopassword='+bisrtSettings.get('ssopassword')
				+'&birst.SpaceId='+bisrtSettings.get('SpaceId')
				+'&serverurl='+returnserveURL;
		req.setEndpoint(baseUrl);
		req.setMethod('POST');
    	req.setHeader('Content-type','application/x-www-form-urlencoded');
    	req.setHeader('Content-Length','200');
    	System.debug('req:'+req);
		HttpResponse resp = http.send(req);
		System.debug('resp:'+resp);
		return resp.getBody();
	}

	private Map<String,String> getBirstSettings() {

		Map<String,String> bisrtSettings = new Map<String,String>();
		for(CPQCommunitBirst__c str: CPQCommunitBirst__c.getAll().Values()) {

			bisrtSettings.put(str.Name,str.Value__c);
		}
		return bisrtSettings;
	}
}