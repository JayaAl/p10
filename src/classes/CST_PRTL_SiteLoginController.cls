global with sharing class CST_PRTL_SiteLoginController {
/**
 * An apex page controller that exposes the site login functionality
 */

    global String username {get; set;}
    global String password {get; set;}

    global PageReference login() {
  String orgId = UserInfo.getOrganizationId();
  List<CST_PRTL_Info__c> pIdList = CST_PRTL_Info__c.getall().values();
  String pId = pIdList[0].Portal_ID__c;
  System.debug(pId);
  //String sfdcHostURL = URL.getSalesforceBaseUrl().toExternalForm();
 // System.debug('sfdcHostURL:'+sfdcHostURL);
  //String startUrl = sfdcHostURL+'/secur/login_portal.jsp';
  String startUrl = 'https://na87.salesforce.com/secur/login_portal.jsp';
  //startUrl += '?orgId=00D300000001WOu&portalId=060400000004hLj';
  startUrl += '?orgId='+ orgId+'&portalId='+ pId;
  
  startUrl += '&startUrl='; 
  startUrl += '&loginUrl=';
  startUrl += '&useSecure=true';
  startUrl += '&un=' + username;
  startUrl += '&pw='+ password;

  PageReference portalPage  = new PageReference(startUrl);
  portalPage.setRedirect(true);
  PageReference p = Site.login(username, password, startUrl);
  if (p == null) return Site.login(username, password, null);
  else return portalPage;
    }
    
    global CST_PRTL_SiteLoginController () {}
    
  
}