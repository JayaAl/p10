/*
@(#)Last changed:   $Date: 07/30/2015 $By: Lakshman Ace
@(#)Purpose:        Call ERP webservice to invoke async batch on oracle
@(#)Author:         Lakshman Ace (sfdcace@gmail.com)
@(#)Project:        ERP
*/
global class CUST_CallERPWS {
    webservice static String invokeERPBatch(String accountId) { 
         HttpRequest request = new HttpRequest();
        request.SetMethod('GET');
        system.debug('************'+accountId);
        String domain = (!isSandboxEnv()) ? System.Label.ESB_Base_URL : System.Label.ESB_STAGE_URL;
        String endPointSTr = domain + '/route/erpaccounts/account/run?accountId=' + accountId;
        request.SetEndPoint(endPointSTr);
       
        request.setTimeout(20000);
        system.debug(request);
        HttpResponse httpResponse = new Http().Send(request);
        String returnMessage;
        if(httpResponse.getStatus().toLowerCase() == 'ok') {
            returnMessage = 'Customer Sync to Oracle Process submitted successfully';
        } else {
            returnMessage = httpResponse.getStatus();
        }
        return returnMessage;
    }
    
    private static boolean isSandboxEnv(){        
         Organization currentOrg = [Select IsSandbox from Organization limit 1];
         return currentOrg.IsSandbox ;
    }
}