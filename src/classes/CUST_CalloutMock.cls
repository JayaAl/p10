@isTest
global class CUST_CalloutMock implements HttpCalloutMock{
  global HttpResponse respond(HTTPRequest req){
    HttpResponse res = new HttpResponse();
    res.setStatus('OK');
    res.setStatusCode(200);
    res.setBody('TESTING ERP SFDC DUMMY RESPONSE BODY');
    return res;
  }
}