/*
@(#)Last changed:   $Date: 07/30/2015 $By: Lakshman Ace
@(#)Purpose:        List view of invoices on customer portal to facilitate Payments
@(#)Author:         Lakshman Ace (sfdcace@gmail.com)
@(#)Project:     ERP
*/
global class  CUST_PortalInvoiceListController {
    global CUST_PortalUtils FLS{get{return new CUST_PortalUtils();}set;}
    // List view
    public List<PortalInvoiceView> invoiceListView {get; set;}
    public Id accountFilterId {get; set;}
    global String accountFilterName {get; set;}
    global String selected_account_id {get;set;}    
    global Boolean no_records_available {get;set;}
    
    // The list of accounts to filter 
    private List<Account> accounts_related = new List<Account>();
    public Boolean hasPaymentAccess {get;set;}
    global Boolean multiple_accounts {get;set;}
    
    public String defaultAmount {get;set;}
    
    // Set controller (handle more records)
    public ApexPages.StandardSetController setController {get;set;} 
    private String soql {get;set;}
    private String base_soql {get;set;}
    public String invIds{get;set;}
     
    // Pagination Methods for the set controller
    public Integer PAGE_SIZE = 25;
    
    global void GoPrevious(){ setController.previous(); }
    global void GoNext(){ setController.next(); }
    global void GoLast(){ setController.last(); }
    global  void GoFirst(){ setController.first(); }
    global Boolean getRenderPrevious(){return setController.getHasPrevious();}
    global Boolean getRenderNext(){return setController.getHasNext();}
    global Integer getRecordSize(){return setController.getResultSize();}
    global Integer getPageNumber(){return setController.getPageNumber();}
    global boolean has_pages {get;set;}
    
    // Static vars, needed inside of the invoice inner class
    public static String current_filter_static {get;set;}
    public static String current_page_static {get;set;}
    public static String current_sort_order_static {get;set;}
    public static String current_sort_field_static {get;set;}
    public String paymentPageURL {get;set;}
    
    private List<Id> filter_notAllowedRecords;
    private String filter_hiddenUntilNumber;
    private Date filter_hiddenUntilDate;
    private String filter_hiddenByNamePattern;
    
    public String contact_id {get;set;} 

    public Map<String, String> mapOfInvoiceNameToStatus {get;set;}
    public String keyString {get;set;}

    global CUST_PortalUtils filters{get;set;}
    private String filters_extraString_for_soql = '';
    private String filters_saveSortOrder = '';
    private Boolean filters_setOneTime_sort = false;
    global void processFilters(){
        this.filters.receiveFilters();
        this.filters_extraString_for_soql = this.buildFiltersStringForSoql();
        this.filters_setOneTime_sort = true;
        this.filterInvoices();
    }
    private String buildFiltersStringForSoql(){
        String s = ' ';
        String name = '';
        Integer saveNumberStart = -1;
        Date saveDateStart = null;
        defaultAmount = '0.00';
    //Filters by Number
        if(this.filters.filterByNumber_start != null && this.filters.filterByNumber_start != ''){
            try{
                this.filters.filterByNumber_start = this.filters.filterByNumber_start.toLowerCase();
                //this.filters.filterByNumber_start = this.filters.filterByNumber_start.replaceAll('sin', '');
                Integer n = Integer.valueOf(this.filters.filterByNumber_start);
                if(n > 999999){
                    n = 999999;
                }else if(n < 0){
                    n = 0;
                }
                saveNumberStart = n;
                this.filters.filterByNumber_start = n + '';
                name = this.filters.filterByNumber_start;
                Integer length = name.length();
                for(Integer i = 0; i <5 - length; i++){
                    name = '0' + name;
                }
                //name =  name;
                this.filters.filterByNumber_start = name;
                s += 'AND Name >= \'' + name + '\' ';
            }catch(Exception e){this.filters.filterByNumber_start = '';}
        }
        if(this.filters.filterByNumber_end != null && this.filters.filterByNumber_end != ''){
            try{
                this.filters.filterByNumber_end = this.filters.filterByNumber_end.toLowerCase();
                //this.filters.filterByNumber_end = this.filters.filterByNumber_end.replaceAll('sin', '');
                Integer n = Integer.valueOf(this.filters.filterByNumber_end);
                if(saveNumberStart > -1 && n < saveNumberStart){
                    n = saveNumberStart;
                }
                if(n > 999999){
                    n = 999999;
                }else if(n < 0){
                    n = 0;
                }
                this.filters.filterByNumber_end = n + '';
                name = this.filters.filterByNumber_end;
                Integer length = name.length();
                for(Integer i = 0; i <5 - length; i++){
                    name = '0' + name;
                }
                //name = 'SIN' + name;
                this.filters.filterByNumber_end = name;
                s += 'AND Name <= \'' + name + '\' ';
            }catch(Exception e){this.filters.filterByNumber_end = '';}
        }
        
        //Filters by Date
        if(this.filters.filterByDate_start != null && this.filters.filterByDate_start != ''){
            try{
                String[] split1 = this.filters.filterByDate_start.split('/');
                if(split1.size() == 3){
                    Date dt;
                    String dateFormat =CUST_PortalUtils.getCurrentDateFormat(); 
                    if(dateFormat.equals('mm/dd/yyyy')){
                        dt = Date.newInstance(Integer.valueOf(split1[2]),Integer.valueOf(split1[0]),Integer.valueOf(split1[1]));
                    }else{
                        dt = Date.newInstance(Integer.valueOf(split1[2]),Integer.valueOf(split1[1]),Integer.valueOf(split1[0]));
                    }
                    saveDateStart = dt;
                    if(dateFormat.equals('mm/dd/yyyy')){
                        this.filters.filterByDate_start = CUST_PortalUtils.getFormattedDateString(dt, 'MM/dd/yyyy');
                    }else{
                        this.filters.filterByDate_start = CUST_PortalUtils.getFormattedDateString(dt, 'dd/MM/yyyy');
                    }
                    s += 'AND Invoice_Date__c >= ' + String.valueOf(dt) + ' ';
                }
            }catch(Exception e){this.filters.filterByDate_start = '';}
        }
        if(this.filters.filterByDate_end != null && this.filters.filterByDate_end != ''){
            try{
                String[] split1 = this.filters.filterByDate_end.split('/');
                if(split1.size() == 3){
                    Date dt;
                    String dateFormat =CUST_PortalUtils.getCurrentDateFormat();                  
                    if(dateFormat.equals('mm/dd/yyyy')){
                        dt = Date.newInstance(Integer.valueOf(split1[2]),Integer.valueOf(split1[0]),Integer.valueOf(split1[1]));
                    }else{
                        dt = Date.newInstance(Integer.valueOf(split1[2]),Integer.valueOf(split1[1]),Integer.valueOf(split1[0]));
                    }
                    if(saveDateStart != null && dt < saveDateStart){
                        dt = saveDateStart;
                    }
                    if(dateFormat.equals('mm/dd/yyyy')){
                        this.filters.filterByDate_end = CUST_PortalUtils.getFormattedDateString(dt, 'MM/dd/yyyy');
                    }else{
                        this.filters.filterByDate_end = CUST_PortalUtils.getFormattedDateString(dt, 'dd/MM/yyyy');
                    }
                    s += 'AND Invoice_Date__c <= ' + String.valueOf(dt) + ' ';
                }
            }catch(Exception e){this.filters.filterByDate_end = '';}
        }
        //Filters by column:
        if(this.filters.selectionInList != null && !this.filters.selectionInList.equals('all')){
            String st = this.filters.selectionInList;
            if(st.equals('paid')){
                s += 'AND Portal_Payment_Status__c = \'' + 'paid' + '\' ';
            }else if(st.equals('unpaid')){
                s += 'AND Portal_Payment_Status__c = \'' + 'unpaid' + '\' ';
            }else if(st.equals('partpaid')){
                s += 'AND Portal_Payment_Status__c = \'' + 'part paid' + '\' ';
            }else if(st.equals('processing')){
                s += 'AND Portal_Payment_Status__c = \'' + 'Processing' + '\' ';
            }
        }
        
        if(s.length() > 1){
            s = ' ' + s.trim();
        }else{
            s = '';
        }

        return s;
    }
    
    global Integer getTotalPages(){
        double div = (setController.getResultSize())/double.valueOf(String.valueOf(PAGE_SIZE));
        if (div > div.intValue()) {
          div = div + 1 ;          
        }           
        return div.intValue();
    }
    
    // Constructor
    global CUST_PortalInvoiceListController()
    {

        no_records_available = false;
        Id currentUserId = UserInfo.getUserId();
        User u = [Select ContactId, Contact.Name, Contact.Id, AccountId  from User where Id =: currentUserId Limit 1];
        if(u.AccountId != null) {
            this.hasPaymentAccess = CUST_PortalUtils.hasPaymentAccess(u.AccountId);
        } else {
            this.hasPaymentAccess = false;
        }
        contact_id = u.contact.id;
        mapOfInvoiceNameToStatus = new Map<String, String>();
        sortedByUrl = false;
        
        
        paymentPageURL = '';
        
        List<Id> accounts_ids = new List<Id>();
        
        List<Contact> contsList = [SELECT c.AccountId FROM Contact c WHERE c.id = :contact_id];
        if(contsList.size() > 0){
            accounts_ids.add(contsList[0].AccountId);
        } 

        // Set On Page document filters:
        this.filters = new CUST_PortalUtils();
        this.filters.filterByList = CUST_PortalUtils.getFiltersList1('CustomerPortalInvoiceList'); // ESS-37237 Added the Parameter to identify context for adding default picklist value for Customer Portal
        this.filters.selectionInList = (System.Label.Customer_Portal_Default_invoice_Filter != '' || System.Label.Customer_Portal_Default_invoice_Filter != null) ? System.Label.Customer_Portal_Default_invoice_Filter : 'unpaid';
        // Load Extra Filters:
        
        this.filters_extraString_for_soql = this.buildFiltersStringForSoql();
        // ------------------------------
        
        accounts_related = [Select a.id, a.Name From Account a where a.id IN :accounts_ids];
       
        // construct invoice list view
        if(accounts_related.size() > 0){
            selected_account_id = 'all';
            //selected_account_id = accounts_related[0].Id;
        } 
        
        multiple_accounts = false;
        
        if(accounts_related.size() > 1){
            multiple_accounts = true;
        }
        
                
        String extra_filters = this.filters_extraString_for_soql;       
        // Base SOQL sentence
        if(selected_account_id == 'all'){
            soql = 'select ERP_Invoice_Id__c, CurrencyIsoCode, ERP_Org_Id__c, Name, CC_Authorized__c, Company__c, Company__r.Name, Advertiser__c, Advertiser__r.Name, Due_Date__c, Invoice_Date__c, Invoice_Total__c, Invoice_Status__c, Outstanding_Value__c, Payment_Status__c, Portal_Payment_Status__c, (Select Id From Attachments Order By CreatedDate desc Limit 1) from ERP_Invoice__c where  Company__c in :accounts_related ' + extra_filters + ' '; // Added field Portal_Payment_Status__c 
        } else {
            soql = 'select ERP_Invoice_Id__c,Portal_Payment_Status__c, CurrencyIsoCode, ERP_Org_Id__c, Name, CC_Authorized__c, Company__c, Company__r.Name, Advertiser__c, Advertiser__r.Name, Due_Date__c, Invoice_Date__c, Invoice_Total__c, Invoice_Status__c, Outstanding_Value__c, (Select Id From Attachments Order By CreatedDate desc Limit 1) from ERP_Invoice__c where  Company__c =:selected_account_id ' + extra_filters + ' '; // added field Portal_Payment_Status__c 
        }   
        
        base_soql = soql;
                    
        
        ApexPages.currentPage().getParameters().put('sort_field', 'PaymentsStatus');
        ApexPages.currentPage().getParameters().put('sort_order', 'desc');
        if(ApexPages.currentPage().getParameters().get('sort_field') != null){
            String sort_field = ApexPages.currentPage().getParameters().get('sort_field');
            if(sort_field == 'Accounts'){ sortAccounts(); }
            if(sort_field == 'InvoiceNumbers'){ sortInvoiceNumbers(); }
            if(sort_field == 'InvoiceDates'){ sortInvoiceDates(); }
            if(sort_field == 'InvoiceDueDates'){ sortInvoiceDueDates(); }
            if(sort_field == 'InvoiceCurrencies'){ sortInvoiceCurrencies(); }
            if(sort_field == 'InvoiceTotals'){ sortInvoiceTotals(); }
            if(sort_field == 'PaymentsStatus'){ sortPaymentsStatus(); }
            if(sort_field == 'OutstandingAmounts'){ sortOutstandingAmounts(); }
            if(sort_field == 'Advertisers'){ sortAdvertisers(); }
        } else {
            queryInvoices();
        }
        
        if(ApexPages.currentPage().getParameters().get('p') != null){
            setController.setpageNumber(integer.valueOf(ApexPages.currentPage().getParameters().get('p')));
        }
    }
    
    // Get the list of accounts for the current contact
    global List<SelectOption> getActiveContactAccounts() {
        List<SelectOption> options = new List<SelectOption>();

        options.add(new SelectOption('all','All'));
                
        for(Account a:accounts_related){
            options.add(new SelectOption(a.Id,a.Name));
        }
        
        return options;
    }
    
    /*
    * Filter Invoices.
    *
    */
    global void filterInvoices(){

        String extra_filters = this.filters_extraString_for_soql;               
        // Base SOQL sentence
        if(selected_account_id == 'all'){
            base_soql = 'select ERP_Invoice_Id__c, CurrencyIsoCode, ERP_Org_Id__c, Name, CC_Authorized__c, Company__c, Company__r.Name, Advertiser__c, Advertiser__r.Name, Due_Date__c, Invoice_Date__c, Invoice_Total__c, Invoice_Status__c, Outstanding_Value__c, Payment_Status__c, Portal_Payment_Status__c, (Select Id From Attachments Order By CreatedDate desc Limit 1) from ERP_Invoice__c where Company__c in :accounts_related ' + extra_filters + ' ';
        } else {
            base_soql = 'select ERP_Invoice_Id__c, CurrencyIsoCode,Portal_Payment_Status__c, ERP_Org_Id__c, Name, CC_Authorized__c, Company__c, Company__r.Name, Advertiser__c, Advertiser__r.Name, Due_Date__c, Invoice_Date__c, Invoice_Total__c, Invoice_Status__c, Outstanding_Value__c, (Select Id From Attachments Order By CreatedDate desc Limit 1) from ERP_Invoice__c where Company__c =:selected_account_id ' + extra_filters + ' ';
        }  

        if(this.filters_setOneTime_sort == true){
            this.filters_setOneTime_sort = false;
            base_soql += this.filters_saveSortOrder;
        }
        
        soql = base_soql;
        
        queryInvoices();
    }
    
    /*
    * Do a query to the list of invoices.
    */
    public void queryInvoices(){
        system.debug('*****soql' + soql);
        this.setController = new ApexPages.StandardSetController(Database.getQueryLocator(soql));
        
        this.setController.setPageSize(PAGE_SIZE);
        
        if(getTotalPages() > 1){
            has_pages = true;
        } else {
            has_pages = false;
        }
        
        if(this.setController.getResultSize() > 0){
            no_records_available = false;
        } else {
            no_records_available = true;
        }
        
    }
    
    // Sorting
    global String previousSortField {get;set;}  
    global String sortOrder {get;set;}
    global Boolean sortedByUrl {get;set;}

    private Boolean setReOrder = false;
        
    public void doSort(String soql_query, String sortField){
        
        // The first time we sort, if not set previousSortField and the order comes through parameters, avoid the sorting change.
        if(!sortedByUrl && ApexPages.currentPage().getParameters().get('sort_order') != null){
            sortOrder = ApexPages.currentPage().getParameters().get('sort_order');
            previousSortField = sortField;
            sortedByUrl = true;
        } else {
            sortOrder = 'asc';
            
            if(setReOrder == true){
                previousSortField = '-';
                setReOrder = false;
            }
            
            if(previousSortField == sortField){
                sortOrder = 'desc';
                setReOrder = true;
            }else{
                previousSortField = sortField;
            }
        }
                        
        soql = soql_query+sortOrder;
        
        this.filters_saveSortOrder += sortOrder;
        
        queryInvoices();
    }
    
    global void sortAccounts() {
        this.removeCurentSortOrder();
        String soql_query = base_soql+'order by Company__r.Name ';
        this.filters_saveSortOrder = 'order by Company__r.Name ';
        doSort(soql_query,'Accounts');
    }
    
    global void sortInvoiceNumbers() {
        this.removeCurentSortOrder();
        String soql_query = base_soql+'order by Name ';
        this.filters_saveSortOrder = 'order by Name ';
        doSort(soql_query,'InvoiceNumbers');
    }
    
    global void sortInvoiceDates() {
        this.removeCurentSortOrder();
        String soql_query = base_soql+'order by Invoice_Date__c ';
        this.filters_saveSortOrder = 'order by Invoice_Date__c ';
        doSort(soql_query,'InvoiceDates');
    }
    
    global void sortInvoiceDueDates() {
        this.removeCurentSortOrder();
        String soql_query = base_soql+'order by Due_Date__c ';
        this.filters_saveSortOrder = 'order by Due_Date__c ';
        doSort(soql_query,'InvoiceDueDates');
    }
    
    global void sortInvoiceCurrencies() {
        this.removeCurentSortOrder();
        String soql_query = base_soql + 'order by CurrencyIsoCode ';
        this.filters_saveSortOrder = 'order by CurrencyIsoCode ';
        doSort(soql_query,'InvoiceCurrencies');
    }
    
    global void sortInvoiceTotals() {
        this.removeCurentSortOrder();
        String soql_query = base_soql+'order by Invoice_Total__c ';
        this.filters_saveSortOrder = 'order by Invoice_Total__c ';
        doSort(soql_query,'InvoiceTotals');
    }
    
    global void sortPaymentsStatus() {
        this.removeCurentSortOrder();
        String soql_query = base_soql+'order by Portal_Payment_Status__c ';
        this.filters_saveSortOrder = 'order by Portal_Payment_Status__c ';
        doSort(soql_query,'PaymentsStatus');
    }
    
    global void sortOutstandingAmounts() {
        this.removeCurentSortOrder();
        String soql_query = base_soql+'order by Outstanding_Value__c ';
        this.filters_saveSortOrder = 'order by Outstanding_Value__c ';
        doSort(soql_query,'OutstandingAmounts');
    }
    
    global void sortAdvertisers() {
        this.removeCurentSortOrder();
        String soql_query = base_soql+'order by Advertiser__r.Name ';
        this.filters_saveSortOrder = 'order by Advertiser__r.Name ';
        doSort(soql_query,'Advertisers');
    }
        
    
    private void removeCurentSortOrder(){
        if(base_soql != null && !base_soql.equals('')){
            String aux = base_soql;
            Integer pos = aux.IndexOf('order by');
            if(pos > -1){
                base_soql = base_soql.substring(0,pos);
            }
        }
    }
    
    // Getter for the table 
    global List<PortalInvoiceView> getInvoiceList(){
        
        invoiceListView = new List<PortalInvoiceView>();
        
        current_page_static = String.valueOf(setController.getPageNumber());
        current_filter_static = selected_account_id;
        
        // Set statics for link creation.
        current_sort_order_static = sortOrder;
        current_sort_field_static = previousSortField; 
        
        Set<String> invNumberSet = new Set<String> ();
        keyString = '';
        if(! mapOfInvoiceNameToStatus.isEmpty() ){
            mapOfInvoiceNameToStatus.clear();
        }
        
        for(ERP_Invoice__c invoice : getQuery())
        {
            invoiceListView.add(new PortalInvoiceView(invoice, this));
            
            Object paymentStatus = invoice.get('Portal_Payment_Status__c');
            if(paymentStatus != null){
                String invoicePaymentStatus = (String)paymentStatus;
                if(invoicePaymentStatus == 'Unpaid') {
                    invNumberSet.add(invoice.Name);
                }
            }
        }
        
        if(! invNumberSet.isEmpty() ) {
        
            Id loggedInUser = UserInfo.getUserId();
            for(sObject rec : Database.query('select Id, ERP_Invoice__r.Name from Receipt_Junction__c where ERP_Invoice__r.Name =: invNumberSet AND Receipt__r.IsSuccess__c = true AND Receipt__r.Customer_Portal_User__c = :loggedInUser AND (ERP_Invoice__r.Outstanding_Value__c = 0 OR ERP_Invoice__r.Outstanding_Value__c = null )AND Receipt__r.Type__c = \'ERP Invoice\'')){
                SObject invoice_obj = rec.getSObject('ERP_Invoice__r');
                if(invoice_obj != null) {
                Object invoice_name = invoice_obj.get('Name');
                    if(invoice_name != null) {
                        keyString += (String)invoice_name;
                        mapOfInvoiceNameToStatus.put((String)invoice_name, 'Processing');
                    }
                }
            }
        }
        return invoiceListView;
    } 
    
    
    public List<ERP_Invoice__c> getQuery(){
        
        
        return (List<ERP_Invoice__c>) this.setController.getRecords();
        
    }
    
    // viewstate
    global class PortalInvoiceView
    {
        public ERP_Invoice__c invoice {get;set;}
        
        public String invoiceName {get;set;}
        public Date invoiceDate {get;set;}
        public Double invoiceTotal {get;set;}
        public String invoiceAccount {get;set;}
        public Date invoiceDueDate {get;set;}
        public String invoiceCurrency {get;set;}
        public String invoicePaymentStatus {get;set;}
        public String invoiceOutstandingAmount {get;set;}
        public String attachemtId {get;set;}
        
        public Decimal invoiceOutstandingAmount2 {get;set;}
        
        public String strInvoiceOutstandingAmount2 {get;set;}//For avoiding String Type Exception
        
        public String advertiserName {get;set;}
        public CUST_PortalInvoiceListController parentController;
        
                
        global PortalInvoiceView(ERP_Invoice__c myInvoice, CUST_PortalInvoiceListController parent)
        {
            this.invoice = myInvoice;
            invoiceName = invoice.Name;
            invoiceDate = invoice.Invoice_Date__c;
            invoiceTotal = invoice.Invoice_Total__c;
            invoiceAccount = String.valueOf(invoice.Company__r.Name);
            invoiceDueDate = invoice.Due_Date__c;
            for(Attachment att: invoice.Attachments) {
                attachemtId = '/servlet/servlet.FileDownload?file=' + att.Id;
            }
            if(attachemtId == null || attachemtId == '') {
              attachemtId = '/apex/CUST_ViewPDF?invoiceNum=' + invoice.Name + '&orgId=' + invoice.ERP_Org_Id__c + '&type=INV';
            }
            
            
            invoiceCurrency = String.valueOf(invoice.CurrencyIsoCode);
            Object paymentStatus = invoice.get('Portal_Payment_Status__c');
            if(paymentStatus != null){
                invoicePaymentStatus = (String)paymentStatus;
            }
            
            
            invoiceOutstandingAmount = String.valueOf(invoice.Outstanding_Value__c);
            SObject advertiser = invoice.getSObject('Advertiser__r');
            if(advertiser != null)
            {
                advertiserName = (String)advertiser.get('Name');
            }
            try{
                this.invoiceOutstandingAmount2 = Math.abs(invoice.Outstanding_Value__c);
                strInvoiceOutstandingAmount2 = string.valueOf(invoiceOutstandingAmount);
            }catch(Exception e){
                this.invoiceOutstandingAmount2 = 0;
                strInvoiceOutstandingAmount2 = string.valueOf(invoiceOutstandingAmount);
            }
            
            parentController = parent;
        }       

    }
    
    /*Method used to pass selected invoices to payment page*/
    public PageReference processSelected() {
        system.debug( '********invIds' + invIds);
        String baseURL = paymentPageURL + '/apex/SI_UI_CUST_PORTAL_PaymentPage?selectedInvoices=';
        baseURL += EncodingUtil.base64Encode(Blob.valueOf(invIds));
        system.debug('baseURL----------' + baseURL);
        PageReference pg = new PageReference (baseURL) ;
        return pg;
    
    }
    

}