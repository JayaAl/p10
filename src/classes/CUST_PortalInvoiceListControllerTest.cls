@isTest 
public class CUST_PortalInvoiceListControllerTest {
	enum PortalType { CSPLiteUser, PowerPartner, PowerCustomerSuccess, CustomerSuccess }
    
    static testMethod void unitTestInvoiceList() {
        //Generate test data
		User pu = getPortalUser(PortalType.PowerCustomerSuccess, null, true);
        System.assert([select isPortalEnabled
                         from user
                        where id = :pu.id].isPortalEnabled,
                      'User was not flagged as portal enabled.');      
        System.RunAs(pu) {
            System.assert([select isPortalEnabled
                             from user
                            where id = :UserInfo.getUserId()].isPortalEnabled,
                          'User wasnt portal enabled within the runas block. ');
    
        
		
            CUST_PortalInvoiceListController pilc = new CUST_PortalInvoiceListController();
            pilc.getActiveContactAccounts();
            pilc.filterInvoices();
            pilc.queryInvoices();
            pilc.sortAccounts();
            pilc.sortInvoiceNumbers();
            pilc.sortInvoiceDates(); 
            pilc.sortInvoiceDueDates();
            pilc.sortInvoiceCurrencies();
            pilc.sortInvoiceTotals();
            pilc.sortPaymentsStatus();
            pilc.sortOutstandingAmounts();
            pilc.getInvoiceList();
            
            CUST_PortalInvoiceListController.PortalInvoiceView piv = new CUST_PortalInvoiceListController.PortalInvoiceView(new ERP_Invoice__c(),pilc);
            piv.invoiceName = 'aa';
            piv.invoiceDate = System.today();
            piv.invoiceTotal = Double.valueOf('10.0');
            piv.invoiceAccount = 'aa';
            piv.invoiceDueDate = System.today();
            piv.invoiceCurrency = 'aa';
            piv.invoicePaymentStatus = 'aa';
            piv.invoiceOutstandingAmount = 'aa';
            
            ApexPages.currentPage().getParameters().put('sort_field','Accounts');
            ApexPages.currentPage().getParameters().put('sort_order','asc');
            pilc = new CUST_PortalInvoiceListController();
            ApexPages.currentPage().getParameters().put('sort_field','InvoiceNumbers');
            ApexPages.currentPage().getParameters().put('sort_order','asc');
            pilc = new CUST_PortalInvoiceListController();
            ApexPages.currentPage().getParameters().put('sort_field','InvoiceDates');
            ApexPages.currentPage().getParameters().put('sort_order','asc');
            pilc = new CUST_PortalInvoiceListController();
            ApexPages.currentPage().getParameters().put('sort_field','InvoiceDueDates');
            ApexPages.currentPage().getParameters().put('sort_order','asc');
            pilc = new CUST_PortalInvoiceListController();
            ApexPages.currentPage().getParameters().put('sort_field','InvoiceCurrencies');
            ApexPages.currentPage().getParameters().put('sort_order','asc');
            pilc = new CUST_PortalInvoiceListController();
            ApexPages.currentPage().getParameters().put('sort_field','InvoiceTotals');
            ApexPages.currentPage().getParameters().put('sort_order','asc');
            pilc = new CUST_PortalInvoiceListController();
            ApexPages.currentPage().getParameters().put('sort_field','PaymentsStatus');
            ApexPages.currentPage().getParameters().put('sort_order','asc');
            pilc = new CUST_PortalInvoiceListController();
            ApexPages.currentPage().getParameters().put('sort_field','OutstandingAmounts');
            ApexPages.currentPage().getParameters().put('sort_order','asc');
            pilc = new CUST_PortalInvoiceListController();
    
            
            pilc.GoPrevious();
            pilc.GoNext();
            pilc.GoLast();
            pilc.GoFirst();
            pilc.getRenderPrevious();
            pilc.getRenderNext();
            pilc.getRecordSize();
            pilc.getPageNumber();
            
            pilc.filters.selectionInList = 'unpaid';
            pilc.filters.filterByNumber_start = '15';
            pilc.filters.filterByNumber_end = '120';
            pilc.filters.filterByDate_start = '01/01/2010'; 
            pilc.filters.filterByDate_end = '01/01/2011'; 
            pilc.processFilters();
            
    	}
    }
    
    
    public static User getPortalUser(PortalType portalType, User userWithRole, Boolean doInsert) {

        /* Make sure the running user has a role otherwise an exception
           will be thrown. */
        if(userWithRole == null) {  
            if(UserInfo.getUserRoleId() == null) {
                UserRole r = new UserRole(name = 'TEST ROLE ERP');
                Database.insert(r);
                userWithRole = new User(alias = 'hasrole', email='userwithrole@roletest1.com', userroleid = r.id,
                                    emailencodingkey='UTF-8', lastname='Testing', languagelocalekey='en_US', 
                                    localesidkey='en_US', profileid = UserInfo.getProfileId(), 
                                    timezonesidkey='America/Los_Angeles', username='userwithrole@testorg.com');
            } else {
                userWithRole = new User(Id = UserInfo.getUserId(), UserRoleId = UserInfo.getUserRoleId());
            }
            System.assert(userWithRole.userRoleId != null,
                          'This test requires the target org to have at least one UserRole created. Please create a user role in this organization and try again.');
        }
        Account a;
        Contact c;

        System.runAs(userWithRole) {
            a = UTIL_TestUtil.generateAccount();
            a.Credit_Card_Payments_Accepted__c = true;
            insert a;
            
            c = UTIL_TestUtil.createContact(a.Id);
            
            ERP_Invoice__c i = new ERP_Invoice__c(Company__c = a.Id, Advertiser__c = a.Id, Invoice_Date__c = Date.today(), Name = '12345');
            insert i;
            
        }
        /* Get any profile for the given type.*/
        Profile p = [select id
                      from profile
                     where usertype = :portalType.name()
                     limit 1];  
        String testemail = 'puser000@amamama.com';
        User pu = new User(profileId = p.id, username = testemail, email = testemail,
                           emailencodingkey = 'UTF-8', localesidkey = 'en_US',
                           languagelocalekey = 'en_US', timezonesidkey = 'America/Los_Angeles',
                           alias='cspu', lastname='lastname', contactId = c.id);
        if(doInsert) {
            Database.insert(pu);
        }
        return pu;
    }
    
}