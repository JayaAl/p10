public virtual with sharing class CUST_PortalUtils {
  
  
  
  public static Boolean hasPaymentAccess(String accountId) {
    sObject sobj = Database.query('select Credit_Card_Payments_Accepted__c from Account where Id =: accountId');
    Object hasAcc = sobj.get('Credit_Card_Payments_Accepted__c');
        if(hasAcc != null){
            return (Boolean)hasAcc;
    }
    return false;
  }
  
  public static String getFormattedDateString(Date d, String format)
  {
    DateTime dt = DateTime.newInstance(d.year(), d.month(), d.day());
    return dt.format(format);
  }
  
  public static String getCurrentDateFormat(){

    String ret = '';
    
    try{
      
      Integer day = 24;
      Integer month = 5;
      Integer year = 2011;
      
      Date d = Date.newInstance(2011,5,24);
      
      String[] split1 = d.format().split('/');
      
      if(split1.size() > 2){
        for(Integer i = 0; i < 3; i++){
          if(Integer.valueOf(split1[i]) == year){
            ret += 'yyyy';
          }else if(Integer.valueOf(split1[i]) == month){
            ret += 'mm';
          }else if(Integer.valueOf(split1[i]) == day){
            ret += 'dd';
          }
          if(i < 2){
            ret += '/';
          }    
        }
      }else{
        ret = 'dd/mm/yyyy';
      }

    }catch(Exception e){
      ret = 'dd/mm/yyyy';
    }
    
    return ret;
  }
  
  // On Page document filters:
  public String selectionInList{get;set;}
  public List<SelectOption> filterByList{get;set;}
  public String filterByNumber_start{get;set;}
  public String filterByNumber_end{get;set;}
  public String filterByDate_start{get;set;}
  public String filterByDate_end{get;set;}
  public String filterClearAll{get;set;}
  
  // Modified for ESS-37237 : For Customer Portal Invoce List: the default Invoice filter should be unpaid 
  // Getting the default value from the label and diplaying it accordingly
  public static List<SelectOption> getFiltersList1(String context){
    List<SelectOption> options = new List<SelectOption>();
    Map<String,String> pickListValMap = new Map<String,String>{'all'=>'All','unpaid'=>'Unpaid',
                                                                'paid'=>'Paid','partpaid'=>'Part Paid',
                                                                'processing'=>'Processing'};
    
    if('CustomerPortalInvoiceList'.equalsIgnoreCase(context)){                                                                 
      
      if(System.Label.Customer_Portal_Default_invoice_Filter != '' || System.Label.Customer_Portal_Default_invoice_Filter != null){
        String defaultFilterStr = System.Label.Customer_Portal_Default_invoice_Filter;

        for(String strVar : pickListValMap.keySet()){
          if((strVar).equalsIgnoreCase(defaultFilterStr))
            options.add(new SelectOption(strVar,pickListValMap.get(strVar)));          
        }

        for(String strVar : pickListValMap.keySet()){
          if(!(strVar).equalsIgnoreCase(defaultFilterStr))
            options.add(new SelectOption(strVar,pickListValMap.get(strVar)));          
        }
      }
    
    }else{
        for(String strVar : pickListValMap.keySet()){         
            options.add(new SelectOption(strVar,pickListValMap.get(strVar)));
        }      
    }
    
    return options;   
  }  
  
  public static List<SelectOption> getFiltersList2(){
    List<SelectOption> options = new List<SelectOption>();
    options.add(new SelectOption('all','All'));
    options.add(new SelectOption('payment','Payment'));
    options.add(new SelectOption('refund','Refund'));
    return options;   
  }
  public static List<SelectOption> getFiltersList3(){
    List<SelectOption> options = new List<SelectOption>();
    options.add(new SelectOption('all','All'));
    options.add(new SelectOption('invoice','Sales Invoice'));
    options.add(new SelectOption('prepayorder','Prepay Order'));
    return options;   
  }    
  
  public String receiveFilters(){
    String debug =  '';
    if(this.filterClearAll == null){
      this.filterClearAll = '';
    }
    if(this.filterClearAll.equals('1')){
      this.selectionInList = 'all';
      this.filterByNumber_start = '';
      this.filterByNumber_end = '';
      this.filterByDate_start = '';
      this.filterByDate_end = '';
    }
    this.filterClearAll = '';
    return debug;
  }
  
  
   // QUERY FOR CURRENT USER PERMISSION FOR FIELD LEVEL SECURITY
  public boolean invoiceFLS_Account_isAccessible{get{return Schema.sObjectType.ERP_Invoice__c.fields.Company__c.isAccessible();}}
  public boolean invoiceFLS_InvoiceNumber_isAccessible{get{return Schema.sObjectType.ERP_Invoice__c.fields.Name.isAccessible();}}
  public boolean invoiceFLS_InvoiceDate_isAccessible{get{return Schema.sObjectType.ERP_Invoice__c.fields.Invoice_Date__c.isAccessible();}}
  public boolean invoiceFLS_DueDate_isAccessible{get{return Schema.sObjectType.ERP_Invoice__c.fields.Due_Date__c.isAccessible();}}
  
  
}