/*
@(#)Last changed:   $Date: 07/30/2015 $By: Lakshman Ace
@(#)Purpose:        Call ERP webservice to view PDF
@(#)Author:         Lakshman Ace (sfdcace@gmail.com)
@(#)Project: 		ERP
*/
public class CUST_ViewPDFController {
	public String getData() {
    	HttpRequest request = new HttpRequest();
        request.SetMethod('GET');
        request.SetEndPoint(System.Label.ESB_PDF_Base_URL + '/route/portal/invoice-pdf?synch=true&transactionNumber=' +
                            ApexPages.currentPage().getParameters().get('invoiceNum') +
                            '&type=' + ApexPages.currentPage().getParameters().get('type') +'&businessUnit=' + 
                            ApexPages.currentPage().getParameters().get('orgId'));
        request.setTimeout(20000);
        HttpResponse httpResponse = new Http().Send(request);
        system.debug('Response1::' + httpResponse.getBody() ); 
        system.debug('Response2::' + httpResponse.getStatus() );
        Blob body = httpResponse.GetBodyAsBlob();
        system.debug('****' + body);
        
        return EncodingUtil.Base64Encode(body);
      	
    
  	}
}