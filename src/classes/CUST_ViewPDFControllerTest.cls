@isTest(seealldata = false)
public class CUST_ViewPDFControllerTest {
	static testMethod void unitTestViewPDFControllerTest() {
        ERP_Invoice__c testEI = new ERP_Invoice__c (Name = '1234535');
        insert testEI;
        PortalERPViewController testVC = new PortalERPViewController(new ApexPages.StandardController(testEI));
        ApexPages.currentPage().getParameters().put('id',testEI.Id);
        testVC.redirectToViewPDF();
        
        ERP_Credit_Note__c testCN = new ERP_Credit_Note__c (Name = '9898989');
        insert testCN;
        PortalERPViewController testVC2 = new PortalERPViewController(new ApexPages.StandardController(testCN));
        ApexPages.currentPage().getParameters().put('id',testCN.Id);
        testVC2.redirectToViewPDF();
        
        Test.startTest();
        Test.setMock(HttpCalloutMock.class, new CUST_CalloutMock());
        CUST_ViewPDFController testIns = new CUST_ViewPDFController();
        ApexPages.currentPage().getParameters().put('invoiceNum','12345');
        ApexPages.currentPage().getParameters().put('type','INV');
        ApexPages.currentPage().getParameters().put('orgId','pandora test');
        testIns.getData();
        Test.stopTest();
    }
}