/**
 * This class contains unit tests for validating the behavior of Apex classes
 * and triggers.
 *
 * Unit tests are class methods that verify whether a particular piece
 * of code is working properly. Unit test methods take no arguments,
 * commit no data to the database, and are flagged with the testMethod
 * keyword in the method definition.
 *
 * All test methods in an organization are executed whenever Apex code is deployed
 * to a production organization to confirm correctness, ensure code
 * coverage, and prevent regressions. All Apex classes are
 * required to have at least 75% code coverage in order to be deployed
 * to a production organization. In addition, all triggers must have some code coverage.
 * 
 * The @isTest class annotation indicates this class only contains test
 * methods. Classes defined with the @isTest annotation do not count against
 * the organization size limit for all Apex scripts.
 *
 * See the Apex Language Reference for more information about Testing and Code Coverage.
 */
@isTest
private class CV_Test_ApplyForCreditService_Mod {
    public static testMethod void nonExistentAccount2Test() {    
         comDnbiEaiServiceCreditapplication.isTest=true;   
         //O1RestHelper.isApexTest=true;
        Account testA = new Account();
        testA.name = 'Kermoony Consulting';
        testA.BillingCity = '277 Scenic Avenue';
        testA.BillingCountry = 'United States';
        testA.BillingPostalCode = '94611';
        testA.BillingState = 'CA';
        insert testA;
        String acctId = String.valueOf(testA.Id);
        Account testC = [Select id, credit_verification2__c, BillingCity, BillingCountry, BillingStreet, BillingPostalCode, BillingState from Account where name='Kermoony Consulting' limit 1];

        credit_verification__c testCV = new credit_verification__c();
        testCV.Approved_Amount_text__c = '0';
        testCV.Advertiser__c = testC.Id;
        testCV.Pandora_Application_Score__c = '1';
        insert testCV;
        String cvId = String.valueOf(testCV.Id);        
        
        comDnbiEaiServiceCreditapplication.CreditApplicationHttpPort stub = new comDnbiEaiServiceCreditapplication.CreditApplicationHttpPort();
        creditappMessageEaiDnbiCom.ApplyForCreditEnhancedResult test1= new creditappMessageEaiDnbiCom.ApplyForCreditEnhancedResult();
        businessbureauDtoEaiDnbiCom.BureauCompanyDTO test2 = new businessbureauDtoEaiDnbiCom.BureauCompanyDTO(); 
        businessbureauDtoEaiDnbiCom.ArrayOfBureauCompanyDTO test3 = new businessbureauDtoEaiDnbiCom.ArrayOfBureauCompanyDTO();
        comDnbiEaiServiceCreditapplication.CreditApplicationHttpPort test4 = new comDnbiEaiServiceCreditapplication.CreditApplicationHttpPort();
        comDnbiEaiServiceCreditapplication.getApplication_element test5 = new comDnbiEaiServiceCreditapplication.getApplication_element();
        comDnbiEaiServiceCreditapplication.getApplicationResponse_element test6 = new comDnbiEaiServiceCreditapplication.getApplicationResponse_element();
        comDnbiEaiServiceCreditapplication.getApplicationList_element test7 = new comDnbiEaiServiceCreditapplication.getApplicationList_element();
        comDnbiEaiServiceCreditapplication.ArrayOfString test8 = new comDnbiEaiServiceCreditapplication.ArrayOfString();
        comDnbiEaiServiceCreditapplication.performECFAction_element test9 = new comDnbiEaiServiceCreditapplication.performECFAction_element();
        comDnbiEaiServiceCreditapplication.updateCreditApplicationResponse_element test10 = new comDnbiEaiServiceCreditapplication.updateCreditApplicationResponse_element();
        comDnbiEaiServiceCreditapplication.matchCompany_element test11 = new comDnbiEaiServiceCreditapplication.matchCompany_element();
        comDnbiEaiServiceCreditapplication.matchCompanyResponse_element test12 = new comDnbiEaiServiceCreditapplication.matchCompanyResponse_element();
        comDnbiEaiServiceCreditapplication.getApplicationListResponse_element test13 = new comDnbiEaiServiceCreditapplication.getApplicationListResponse_element();
        comDnbiEaiServiceCreditapplication.updateCreditApplication_element test14 = new comDnbiEaiServiceCreditapplication.updateCreditApplication_element();
        comDnbiEaiServiceCreditapplication.applyForCredit_element test15 = new comDnbiEaiServiceCreditapplication.applyForCredit_element();
        comDnbiEaiServiceCreditapplication.performECFActionResponse_element test16 = new comDnbiEaiServiceCreditapplication.performECFActionResponse_element();
        comDnbiEaiServiceCreditapplication.applyForCreditResponse_element test17 = new comDnbiEaiServiceCreditapplication.applyForCreditResponse_element();
        comDnbiEaiServiceCreditapplication.applyForCreditEnhanced_element test18 = new comDnbiEaiServiceCreditapplication.applyForCreditEnhanced_element();
        comDnbiEaiServiceCreditapplication.applyForCreditEnhancedResponse_element test19 = new comDnbiEaiServiceCreditapplication.applyForCreditEnhancedResponse_element();
        
        dtoEaiDnbiCom.BureauIdDTO t1 = new dtoEaiDnbiCom.BureauIdDTO();
        dtoEaiDnbiCom.OutcomeDTO t2 = new dtoEaiDnbiCom.OutcomeDTO();
        dtoEaiDnbiCom.ArrayOfAutomatedDecisionReasonDTO t3 = new dtoEaiDnbiCom.ArrayOfAutomatedDecisionReasonDTO();
        dtoEaiDnbiCom.ArrayOfBureauIdDTO t4 = new dtoEaiDnbiCom.ArrayOfBureauIdDTO();
        dtoEaiDnbiCom.AuthenticationDTO_element t5 = new dtoEaiDnbiCom.AuthenticationDTO_element();
        dtoEaiDnbiCom.FieldDTO t6 = new dtoEaiDnbiCom.FieldDTO();
        dtoEaiDnbiCom.CompanySearchResultDTO t7 = new dtoEaiDnbiCom.CompanySearchResultDTO();
        dtoEaiDnbiCom.ScoreDTO t8 = new dtoEaiDnbiCom.ScoreDTO();
        dtoEaiDnbiCom.BusinessEntityDTO t9 = new dtoEaiDnbiCom.BusinessEntityDTO();
        dtoEaiDnbiCom.ArrayOfFieldErrorDTO t10 = new dtoEaiDnbiCom.ArrayOfFieldErrorDTO();
        dtoEaiDnbiCom.CreditTermsDTO t11 = new dtoEaiDnbiCom.CreditTermsDTO();
        dtoEaiDnbiCom.MatchCompanyRequestDTO t12 = new dtoEaiDnbiCom.MatchCompanyRequestDTO();
        dtoEaiDnbiCom.AddressDTO t13 = new dtoEaiDnbiCom.AddressDTO();
        dtoEaiDnbiCom.AutomatedDecisionReasonDTO t14 = new dtoEaiDnbiCom.AutomatedDecisionReasonDTO();
        dtoEaiDnbiCom.ArrayOfScoreDTO t15 = new dtoEaiDnbiCom.ArrayOfScoreDTO();
        dtoEaiDnbiCom.FieldErrorDTO t16 = new dtoEaiDnbiCom.FieldErrorDTO();
        creditappMessageEaiDnbiCom.ApplyForCreditEnhancedRequest ta1 = new creditappMessageEaiDnbiCom.ApplyForCreditEnhancedRequest();
        creditappMessageEaiDnbiCom.ApplyForCreditEnhancedResult ta2 = new creditappMessageEaiDnbiCom.ApplyForCreditEnhancedResult();
        creditappMessageEaiDnbiCom.ApplyForCreditResult ta3 = new creditappMessageEaiDnbiCom.ApplyForCreditResult();
        creditappMessageEaiDnbiCom.UpdateCreditApplicationResult ta4 = new creditappMessageEaiDnbiCom.UpdateCreditApplicationResult();
        creditappDtoEaiDnbiCom.ArrayOfCreditApplicationDTO tb1 = new creditappDtoEaiDnbiCom.ArrayOfCreditApplicationDTO();
        creditappDtoEaiDnbiCom.BureauErrorDTO tb2 = new creditappDtoEaiDnbiCom.BureauErrorDTO();
        creditappDtoEaiDnbiCom.CompanyInfoDTO tb3 = new creditappDtoEaiDnbiCom.CompanyInfoDTO();
        creditappDtoEaiDnbiCom.CorporateLinkageDTO tb4 = new creditappDtoEaiDnbiCom.CorporateLinkageDTO();
        creditappDtoEaiDnbiCom.CreditApplicationDTO tb5 = new creditappDtoEaiDnbiCom.CreditApplicationDTO();
        creditappDtoEaiDnbiCom.CreditApplicationECFDTO tb6 = new creditappDtoEaiDnbiCom.CreditApplicationECFDTO();
        creditappDtoEaiDnbiCom.GetCreditApplicationsResultDTO tb7 = new creditappDtoEaiDnbiCom.GetCreditApplicationsResultDTO();
        creditappDtoEaiDnbiCom.GetListOfApplicationsRequestDTO tb8 = new creditappDtoEaiDnbiCom.GetListOfApplicationsRequestDTO();
        creditappDtoEaiDnbiCom.UpdateCreditApplicationRequestDTO tb9 = new creditappDtoEaiDnbiCom.UpdateCreditApplicationRequestDTO();
        creditappDtoEaiDnbiCom.PerformECFActionRequestDTO tb10 = new creditappDtoEaiDnbiCom.PerformECFActionRequestDTO();
         creditappMessageEaiDnbiCom.ApplyForCreditEnhancedResult  ta88 = new creditappMessageEaiDnbiCom.ApplyForCreditEnhancedResult();
        
        test4.matchCompany(t12);
        test4.getApplication('foo');
        test4.getApplicationList(tb8);
        test4.updateCreditApplication(tb9);
        
        


        dtoEaiDnbiCom.MatchCompanyRequestDTO dto = creditVerificationInitiation.buildWebServiceRequest(stub, acctId);
        dtoEaiDnbiCom.CompanySearchResultDTO resp = new dtoEaiDnbiCom.CompanySearchResultDTO();
        resp.listOfSimilarsFound = false;
        businessbureauDtoEaiDnbiCom.ArrayOfBureauCompanyDTO compList = resp.bureauCompanyList;
        //businessbureauDtoEaiDnbiCom.BureauCompanyDTO[] compArray = compList.BureauCompanyDTO;

        /*compArray[0].bureauIdentifierNumber = '014068298';
        compArray[0].businessName = 'Kermoony Consulting';
        addArray[0].city = 'Piedmont';
        addArray[0].state = 'CA';
        addArray[0].country = 'United States';
        addArray[0].zipCode = '94611';
*/
        String result = creditVerificationInitiation.handleMatchCompanyWebServiceResponse(stub, resp, acctId);
        System.assertEquals(false, resp.listOfSimilarsFound);
        System.assertEquals('0', testCV.Approved_Amount_text__c);
    }
    static testMethod void testBuildCreditApplicationRequest()
    
    {
         comDnbiEaiServiceCreditapplication.isTest=true;
          //O1RestHelper.isApexTest=true;
        Account testA = new Account();
        testA.name = 'Kermoony Consulting';
        testA.BillingCity = '277 Scenic Avenue';
        testA.BillingCountry = 'United States';
        testA.BillingPostalCode = '94611';
        testA.BillingState = 'CA';
        insert testA;
        String acctId = String.valueOf(testA.Id);
        Account testC = [Select id, credit_verification2__c, BillingCity, BillingCountry, BillingStreet, BillingPostalCode, BillingState from Account where name='Kermoony Consulting' limit 1];

        credit_verification__c testCV = new credit_verification__c();
        testCV.Approved_Amount_text__c = '0';
        testCV.Advertiser__c = testC.Id;
        testCV.Pandora_Application_Score__c = '1';
        insert testCV;
        String cvId = String.valueOf(testCV.Id);        
        creditVerificationInitiation.buildCreditApplicationRequest(testA, '014068298', 123);
        creditappMessageEaiDnbiCom.ApplyForCreditResult responseCA = new creditappMessageEaiDnbiCom.ApplyForCreditResult();
        creditappMessageEaiDnbiCom.ApplyForCreditEnhancedResult test1= new creditappMessageEaiDnbiCom.ApplyForCreditEnhancedResult();
        businessbureauDtoEaiDnbiCom.BureauCompanyDTO test2 = new businessbureauDtoEaiDnbiCom.BureauCompanyDTO(); 
        businessbureauDtoEaiDnbiCom.ArrayOfBureauCompanyDTO test3 = new businessbureauDtoEaiDnbiCom.ArrayOfBureauCompanyDTO();
        comDnbiEaiServiceCreditapplication.CreditApplicationHttpPort test4 = new comDnbiEaiServiceCreditapplication.CreditApplicationHttpPort();
        comDnbiEaiServiceCreditapplication.getApplication_element test5 = new comDnbiEaiServiceCreditapplication.getApplication_element();
        comDnbiEaiServiceCreditapplication.getApplicationResponse_element test6 = new comDnbiEaiServiceCreditapplication.getApplicationResponse_element();
        comDnbiEaiServiceCreditapplication.getApplicationList_element test7 = new comDnbiEaiServiceCreditapplication.getApplicationList_element();
        comDnbiEaiServiceCreditapplication.ArrayOfString test8 = new comDnbiEaiServiceCreditapplication.ArrayOfString();
        comDnbiEaiServiceCreditapplication.performECFAction_element test9 = new comDnbiEaiServiceCreditapplication.performECFAction_element();
        comDnbiEaiServiceCreditapplication.updateCreditApplicationResponse_element test10 = new comDnbiEaiServiceCreditapplication.updateCreditApplicationResponse_element();
        comDnbiEaiServiceCreditapplication.matchCompany_element test11 = new comDnbiEaiServiceCreditapplication.matchCompany_element();
        comDnbiEaiServiceCreditapplication.matchCompanyResponse_element test12 = new comDnbiEaiServiceCreditapplication.matchCompanyResponse_element();
        comDnbiEaiServiceCreditapplication.getApplicationListResponse_element test13 = new comDnbiEaiServiceCreditapplication.getApplicationListResponse_element();
        comDnbiEaiServiceCreditapplication.updateCreditApplication_element test14 = new comDnbiEaiServiceCreditapplication.updateCreditApplication_element();
        comDnbiEaiServiceCreditapplication.applyForCredit_element test15 = new comDnbiEaiServiceCreditapplication.applyForCredit_element();
        comDnbiEaiServiceCreditapplication.performECFActionResponse_element test16 = new comDnbiEaiServiceCreditapplication.performECFActionResponse_element();
        comDnbiEaiServiceCreditapplication.applyForCreditResponse_element test17 = new comDnbiEaiServiceCreditapplication.applyForCreditResponse_element();
        comDnbiEaiServiceCreditapplication.applyForCreditEnhanced_element test18 = new comDnbiEaiServiceCreditapplication.applyForCreditEnhanced_element();
         
        
        //creditappDtoEaiDnbiCom.CreditApplicationECFDTO crdAppECF = responseCA.applicationECF;
        //dtoEaiDnbiCom.ArrayOfScoreDTO  score =  crdAppECF.scoreList;
        //dtoEaiDnbiCom.ScoreDTO[] scorearr = score.ScoreDTO;
        //scorearr[0].scoreName = 'Pandora Application Scorecard';
        //scorearr[0].value = 7.5;
        
        creditVerificationInitiation.handleCreditApplicationWebServiceResponse(responseCA, testCV, testA);
    }
    static testMethod void testBadStubReference() {
         comDnbiEaiServiceCreditapplication.isTest=true;
          //O1RestHelper.isApexTest=true;
        Account testA = new Account();
        testA.name = 'Kermoony Consulting';
        testA.BillingCity = '277 Scenic Avenue';
        testA.BillingCountry = 'United States';
        testA.BillingPostalCode = '94611';
        testA.BillingState = 'CA';
        insert testA;
        String acctId = String.valueOf(testA.Id);
        Account testC = [Select id, credit_verification2__c, BillingCity, BillingCountry, BillingStreet, BillingPostalCode, BillingState from Account where name='Kermoony Consulting' limit 1];

        credit_verification__c testCV = new credit_verification__c();
        testCV.Advertiser__c = testC.Id;
        testCV.Approved_Amount_text__c = '0';
        testCV.Pandora_Application_Score__c = '1';
        insert testCV;
        String cvId = String.valueOf(testCV.Id);        
        String results = creditVerificationInitiation.ApplyForCredit(acctId, cvId);
        comDnbiEaiServiceCreditapplication.CreditApplicationHttpPort stub = new comDnbiEaiServiceCreditapplication.CreditApplicationHttpPort();
        dtoEaiDnbiCom.MatchCompanyRequestDTO dto = creditVerificationInitiation.buildWebServiceRequest(stub, acctId);
        dtoEaiDnbiCom.CompanySearchResultDTO resp = new dtoEaiDnbiCom.CompanySearchResultDTO();
        resp.listOfSimilarsFound = false;
        String result = creditVerificationInitiation.handleMatchCompanyWebServiceResponse(stub, resp, acctId);
        System.assertEquals(false, resp.listOfSimilarsFound);
        System.assertEquals('0', testCV.Approved_Amount_text__c);   
    }
    public static testMethod void emptyBuildWebServiceRequest() { 
        // the buildWebServiceReqeust will have a bad Acct ID value.    
        comDnbiEaiServiceCreditapplication.isTest=true;
         //O1RestHelper.isApexTest=true;
        Account testA = new Account();
        testA.name = 'Kermoony Consulting';
        testA.BillingCity = '277 Scenic Avenue';
        testA.BillingCountry = 'United States';
        testA.BillingPostalCode = '94611';
        testA.BillingState = 'CA';
        insert testA;
        //String acctId = '001error';
        String acctId = String.valueOf(testA.Id);
        Account testC = [Select id, Name, credit_verification2__c, BillingCity, BillingCountry, BillingStreet, BillingPostalCode, BillingState from Account where name='Kermoony Consulting' limit 1];

        credit_verification__c testCV = new credit_verification__c();
        testCV.Approved_Amount_text__c = '0';
        testCV.Advertiser__c = testC.Id;
        testCV.Pandora_Application_Score__c = '1';
        insert testCV;
        String cvId = String.valueOf(testCV.Id);        
                
        comDnbiEaiServiceCreditapplication.CreditApplicationHttpPort stub = new comDnbiEaiServiceCreditapplication.CreditApplicationHttpPort();
        dtoEaiDnbiCom.MatchCompanyRequestDTO dto = creditVerificationInitiation.buildWebServiceRequest(stub, acctId);

        //TO BUILD THE REQUEST 
            dtoEaiDnbiCom.BusinessEntityDTO bdto = new dtoEaiDnbiCom.BusinessEntityDTO();
            dtoEaiDnbiCom.AddressDTO adto = new dtoEaiDnbiCom.AddressDTO();

            adto.city = testC.BillingCity;
            adto.state = testC.BillingState;
            adto.street = testC.BillingStreet;
            adto.zipCode = testC.BillingPostalCode;
            adto.country = testC.BillingCountry;

            bdto.businessName = testC.Name;

            bdto.address =  adto;
        
            dto.bureauName = 'dnb';
            dto.businessInformation = bdto;

            dto.listOfSimilars = False;
        //TO BUILD THE RESPONSE
        try
        {
        dtoEaiDnbiCom.CompanySearchResultDTO resp;
        businessbureauDtoEaiDnbiCom.ArrayOfBureauCompanyDTO compList;
        businessbureauDtoEaiDnbiCom.BureauCompanyDTO[] compArray = compList.BureauCompanyDTO;
        if ( compArray.size() != 0 )
        {
            compArray[0].matchScore = 8.9;
            if ( compArray[0].matchScore > 6 ){
                compArray[0].bureauIdentifierNumber = '1234';
            }
        }
        resp.bureauCompanyList = complist;
    



        String result = creditVerificationInitiation.handleMatchCompanyWebServiceResponse(stub, resp, acctId);
        System.assertEquals(null, resp); 
        }
        catch(Exception e)
         {
         }
//      System.assertEquals('0', testCV.Approved_Amount_text__c);
    }
}