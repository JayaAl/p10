global class CalculateAccountRevenueBatch implements database.batchable<Sobject>, database.stateful {
	public CalculateAccountRevenueBatch() {
		
	}

	public list<opportunity> lstOpportunitiesToUpdate = new List<opportunity>();
	public Map<id,decimal> accountRevenueMap = new Map<id,decimal>();
    class CalculateAccountRevenueBatchException extends Exception {}
   
    global Database.QueryLocator start(database.BatchableContext bc) {
		date yearOldDate = system.today()-365;
		//Leads_Settings__c leadProcessStartFromDate = Leads_Settings__c.getValues('Lead Processed Until');
		//datetime leadProcessedUntil;// = leadProcessStartFromDate.Lead_Process_Start_Date__c;
		//String name = 'SRITODAY';
		//String val='\'%' + String.escapeSingleQuotes(name) + '%\'';
		//List<sobject> lineItems = new List<sObject>();
		string query = 'SELECT Id,Last_Close_Date_Std__c, Account_Total_Revenue__c';
		     query += ' FROM Account ';
		     query += ' WHERE  ';
		     query += ' Last_Close_Date_Std__c >= LAST_N_DAYS:365 ';
		     query += ' order by Last_Close_Date_Std__c asc';
		     
		system.debug('This si query' + query);

		return database.getQueryLocator(query);

    }
   
    global void execute(database.BatchableContext bc,List<Sobject> scope) {
		system.debug('This is the scope size' + scope.size());
		lstOpportunitiesToUpdate = new List<opportunity>();
		Map<id,account> accountMap = new Map<Id,account>();
		List<account> accountToUpdate = new List<account>();
		for(account act:(list<account>)scope){
			accountMap.put(act.id,act);	

		}
		system.debug('accountRevenueMap' + accountMap);
		//List<AggregateResult> lstAR = 
		for(AggregateResult ar:[select accountId,sum(amount) from opportunity where accountId in:accountMap.keyset() and stageName=:'Closed Won' and CloseDate>=LAST_N_DAYS:365 group by accountId])
		{
			accountRevenueMap.put((Id)ar.get('accountId'),(decimal)ar.get('expr0'));

		}
		system.debug('accountRevenueMap' + accountRevenueMap);
		for(account act:(list<account>)scope){
			act.Account_Total_Revenue__c = 0;
			if(accountRevenueMap.get(act.Id)!=null)
				act.Account_Total_Revenue__c = (decimal)accountRevenueMap.get(act.Id);
			accountToUpdate.add(act);
		}
		system.debug('accountRevenueMap accountToUpdate' + accountToUpdate);
		
		update accountToUpdate;
      	
    }

    global void finish(database.BatchableContext bc) {
      
      
        
    }
}