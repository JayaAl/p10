/**
* ─────────────────────────────────────────────────────────────────────────────────────────────────┐
* CaseExpirationBatch : schedules CaseExpirationBatch.
* ──────────────────────────────────────────────────────────────────────────────────────────────────
* @author         Abhishek Bidap
* @maintainedBy   Abhishek Bidap
* @version        1.0
* @created        2017-06-15
* @modified       
* @systemLayer    Service
* @see            ????
* @see            ????
* ──────────────────────────────────────────────────────────────────────────────────────────────────
* @changes
* vX.X            Abhishek
* Y  CaseExpiration batch checks the lastmodifeddate of cases and sends email and closes cases based upon the requirment
* ─────────────────────────────────────────────────────────────────────────────────────────────────┘
*/

global class CaseExpirationBatch implements Database.Batchable<sObject> {
    
    String query;
    String close_cut_off = System.Label.Case_expiration_close_cut_off;
    String close_cut_off_plus_one = String.ValueOf((Integer.ValueOf(System.Label.Case_expiration_close_cut_off) + 1));
    String notification_cut_off = System.Label.Case_expiration_notific_cut_off;
    List<Emailtemplate> emailTemplatelst ;

    global CaseExpirationBatch() {      
        query = 'Select id,CreatedById,CaseNumber,OwnerId,LastModifiedDate from Case where LastModifiedDate = LAST_N_DAYS:'+close_cut_off_plus_one+' '+getRecordTypeFilter()+' AND status != \'Closed\' Order By LastModifiedDate DESC';  
    }
    
    global Database.QueryLocator start(Database.BatchableContext BC) {
        return Database.getQueryLocator(query);
    }

    global void execute(Database.BatchableContext BC, List<sObject> scope) {
        system.debug('scope ==> '+scope);
        List<Case> caseToCloseLst = new List<Case>();
        List<Messaging.SingleEmailMessage> mails = new List<Messaging.SingleEmailMessage>();    
        String senderDispName = 'Salesforce Admin';
        for(Case caseObj : (List<Case>) scope){
            system.debug(caseObj.LastModifiedDate);
            system.debug(Date.ValueOf(caseObj.LastModifiedDate).daysBetween(Date.Today()));
            DateTime modifiedDate = (!Test.isRunningTest() ? caseObj.LastModifiedDate : caseObj.LastModifiedDate + Integer.ValueOf(close_cut_off) );
            
            if(Date.ValueOf(modifieddate).daysBetween(Date.Today()) == (Integer.ValueOf(close_cut_off))){ // close cut off
            
                system.debug('in update if');
                caseObj.status = 'Closed';
                caseObj.Closed_Reason__c = 'Segment not needed';
                caseToCloseLst.add(caseObj);            
            }

            modifiedDate = (!Test.isRunningTest() ? caseObj.LastModifiedDate : caseObj.LastModifiedDate + Integer.ValueOf(notification_cut_off) );
            if(Date.ValueOf(modifiedDate).daysBetween(Date.Today()) == (Integer.ValueOf(notification_cut_off)) ){ // notification_cut_off
                            
                Id targetObjId = caseObj.OwnerId != null ? caseObj.OwnerId : caseObj.CreatedById;                
                String HTMLBodyStr = system.label.Case_Expiration_Body_Str.replace('{0}',caseObj.Id);
                String subjsectStr = 'Case - '+caseObj.CaseNumber+' Expiring in seven days.';
                Messaging.SingleEmailMessage singleEmail = EmailUtil.generateEmail(senderDispName,HTMLBodyStr,subjsectStr,new List<String>{'abidap@pandora.com'},targetObjId);  
                mails.add(singleEmail);
                system.debug('Inside mail if');
            }
        }

        try{

            if(!caseToCloseLst.isEmpty())
            update caseToCloseLst;

            system.debug('mails :-->'+mails);
             Messaging.reserveSingleEmailCapacity(2);
            if(!mails.isEmpty()  && !Test.isRunningTest())
                EmailUtil.sendEmail(mails); 
            
        }catch(Exception ex){
            system.debug(ex.getMessage());
            String HTMLBodyStr = 'There is following exception : '+ex.getMessage()+'<br/>'+ex.getStackTraceString();
            String subjsectStr = 'Exception in execution.';
            Messaging.SingleEmailMessage singleEmail = EmailUtil.generateEmail(senderDispName,HTMLBodyStr,subjsectStr,new List<String>{'abidap@pandora.com'},null);                  
            EmailUtil.sendEmail(new List<Messaging.SingleEmailMessage>{singleEmail});   
        }
        
    }
    
   
    
    private string getRecordTypeFilter(){
    
         String filterStr = ' AND RecordType.DeveloperName IN (';   
         for(String strVar : System.Label.Case_Rec_Type_Filter.Split(',')){            
            if(!String.isBlank(strVar)){
                
                filterStr += ('\'' + strVar + '\'' );
                filterStr += ',';             
            }
        }
        
        filterStr = filterStr.subString(0,filterStr.length()-1);
        filterStr += ')';
        
        return filterStr;
    } 
    global void finish(Database.BatchableContext BC) {
        
    }   
}