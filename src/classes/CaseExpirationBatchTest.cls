@istest
public class CaseExpirationBatchTest{

    public static void setupdata(){
        
        Case caseObj = Util_testUtil.generateCase();
        caseObj.RecordTypeid = [Select id from RecordType where DeveloperName = 'Internal_Testing_Case' Limit 1].id;
        caseObj.status = 'Open';
        
        insert caseObj;
    }
    
    public static testmethod void testdata(){
        setupdata();
        Test.starttest();
            database.executeBatch(new CaseExpirationBatch());
        Test.stopTest();
    }
    
    public static testmethod void testScheduler(){
         setupdata();
         Test.starttest();
         CaseExpirationBatchScheduler sh1 = new CaseExpirationBatchScheduler();      
         String sch = '0  00 1 3 * ?';
       system.schedule('Test', sch, sh1);
       test.stopTest();
    }
}