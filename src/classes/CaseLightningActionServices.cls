public with sharing class CaseLightningActionServices {
	
	
	public CaseLightningActionServices(ApexPages.StandardController ctrl) {

		Case caseRec = (Case)ctrl.getRecord();
	}
	@AuraEnabled
	public static List<String> getCaseStatusOptions() {

    	List<String> caseStatusList = new List<String>();
		List<Schema.Picklistentry> picklistValues =  Schema.sObjectType.Case.fields.Status.getPicklistValues();

		 for(Schema.Picklistentry entry : picklistValues) {

		 	caseStatusList.add(entry.getValue());
		 }
		 return caseStatusList;
    }
    @AuraEnabled
	public static List<String> getCaseReasonOptions() {

    	List<String> caseReasonList = new List<String>();
		List<Schema.Picklistentry> picklistValues =  Schema.sObjectType.Case.fields.Reason.getPicklistValues();

		 for(Schema.Picklistentry entry : picklistValues) {

		 	caseReasonList.add(entry.getValue());
		 }
		 return caseReasonList;
    }
    @AuraEnabled
    public static Case getCase(Id caseId) {

    	Case caseObj =  new Case();
    	try {
    		caseObj = [SELECT Id, Status, Reason, Reopen__c
    					FROM Case
    					WHERE Id =: caseId];
    	} catch(Exception e) {}
    	return caseObj;
    }
    @AuraEnabled
    public static String saveCase(Case caseObj) {

    	String dmlResponse = null;
    	try{

    		upsert caseObj;
    		dmlResponse = 'Case Closed.';
    	} catch(Exception e) {

    		dmlResponse = 'Error While Closing Case: '+e.getMessage();
    	}
    	return dmlResponse;
    }
}