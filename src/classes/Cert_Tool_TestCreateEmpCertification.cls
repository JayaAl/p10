@isTest
private class Cert_Tool_TestCreateEmpCertification{
   static testmethod void testUnitCase(){
       List<Cert_Tool_Employee__c> employee = new List<Cert_Tool_Employee__c>();
       Cert_Tool_Employee__c emp = new Cert_Tool_Employee__c(
           Name = 'test', 
           Email__c = 'testing@yahoo.com',
           Employee_ID__c = '7y17',
           Record_lock__c = false,
           Answer__c = 'test',
           Question__c = 'On what street did you grow up?'
           
       );
       //insert emp;
       employee.add(emp);
       
       Cert_Tool_Employee__c empRecord = new Cert_Tool_Employee__c(
           Name = 'test1', 
           Email__c = 'test@yahoo.com',
           Employee_ID__c = '7y17rr',
           Record_lock__c = false,
           Answer__c = 'test',
           Question__c = 'On what street did you grow up?'
           
       );
       //insert empRecord;
       employee.add(empRecord );
       insert employee;
       System.debug('In List'+employee); 
       Set<String> employeeName = new Set<String>();
       for(Cert_Tool_Employee__c e :employee){
           employeeName.add(e.Name);     
       }
       System.debug('In Set'+employeeName); 
       Cert_Tool_Certification__c cerfication = new Cert_Tool_Certification__c(
           Name = 'document'
       );
       insert cerfication; 
       
       ApexPages.StandardSetController controller = new ApexPages.StandardSetController(employee);
       controller.setSelected(employee);
       Cert_Tool_CreateEmpCertification objCreateEmployee  = new Cert_Tool_CreateEmpCertification(controller);
       objCreateEmployee.ec.Certification__c = cerfication.Id;
       objCreateEmployee.doSave();
       objCreateEmployee.onLoad();
       objCreateEmployee.confirmForReminder();
       objCreateEmployee.cancleToReminder();
       objCreateEmployee.onAddCondition();
       objCreateEmployee.onRemoveCondition();
       objCreateEmployee.saveReminder();
       objCreateEmployee.getEmailTemplate();
      Cert_Tool_CreateEmpCertification.ConditionRow co = new Cert_Tool_CreateEmpCertification.ConditionRow();
      co.getId();
      co.getCondition();
   }
}