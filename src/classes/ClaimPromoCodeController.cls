public with sharing class ClaimPromoCodeController {
    
    public Integer codesAssignedInt {get;set;}
    public Integer maxCodeAssignmentLimit {get;set;}
    public Integer requestedCodeInt {get;set;}

    public ClaimPromoCodeController() {
        
        
        codesAssignedInt = [Select count() from Employee_Promo_code__c where User_Assigned__c =: Userinfo.getUserId()];
        maxCodeAssignmentLimit = 5;
        getItems();
    }

    public List<SelectOption> getItems() {
        List<SelectOption> options = new List<SelectOption>();
        codesAssignedInt = [Select count() from Employee_Promo_code__c where User_Assigned__c =: Userinfo.getUserId()];
        Integer codesToBeAssigned = maxCodeAssignmentLimit - codesAssignedInt;
        for(Integer cnt = 1;cnt <= codesToBeAssigned ; cnt++)
            options.add(new SelectOption(String.valueOf(cnt),String.valueOf(cnt)));
        
        return options;
     }


    public PageReference allotPromoCodeAction(){
        Integer codesToBeAssigned = maxCodeAssignmentLimit - codesAssignedInt;  
        try{

            if(requestedCodeInt == 0){
                ApexPages.addMessage(new ApexPages.message(ApexPages.severity.ERROR,'This format is not accepted.'));
                return null;
            }
            if(requestedCodeInt > codesToBeAssigned ){
                     ApexPages.addMessage(new ApexPages.message(ApexPages.severity.ERROR,'This format is not accepted or you have already reached your code limit'));
                return null;
            }
            List<Employee_Promo_code__c> empCodeLst = [Select id,name,User_Assigned__c,Promo_Code__c,Redeem_URL__c from Employee_Promo_code__c where User_Assigned__c = null limit: requestedCodeInt for update];
            List<Messaging.SingleEmailMessage> mails = new List<Messaging.SingleEmailMessage>();

            for(Employee_Promo_code__c promoCode : empCodeLst){         
                promoCode.User_Assigned__c = UserInfo.getUserId();
            }

            update empCodeLst;

            Id targetObjId = UserInfo.getUserId();                
            String HTMLBodyStr = createEmailHTMLBody(empCodeLst);
            String senderDispName = 'Salesforce Admin';
            String subjsectStr = 'Your 90-day Premium free trial codes for friends & family!';
            Messaging.SingleEmailMessage singleEmail = EmailUtil.generateEmail(senderDispName,HTMLBodyStr,subjsectStr,null,targetObjId);  
            mails.add(singleEmail);

        

            if(!mails.isEmpty()  && !Test.isRunningTest())
                EmailUtil.sendEmail(mails);

             ApexPages.addMessage(new ApexPages.message(ApexPages.severity.INFO,'Congratulations, your Premium code email is on its way!'));    
        
        }catch(Exception ex){
             ApexPages.addMessage(new ApexPages.message(ApexPages.severity.ERROR,'Error processing the request, pls comtact system Admin'+ex.getMessage()));        
        }   

         return null;

    }

    private static string createEmailHTMLBody(List<Employee_Promo_code__c> empCodeLst){

        String htmlBody = '';

        htmlBody += 'Hello '+Userinfo.getName();
        Integer ind = 1;

        htmlBody += '<br/>';
        htmlBody += Label.Promo_Code_Email_Part1;
        htmlBody += '<br/>';

        for(Employee_Promo_code__c promoCodeVar : empCodeLst){          
            htmlBody += ('Code'+(ind++)+' : '+promoCodeVar.Redeem_URL__c);
            htmlBody += '<br/>';
        }
        
        htmlBody += '<br/>';
        
        htmlBody += Label.Promo_Code_Email_Part2;
        htmlBody += Label.Promo_Code_Email_Part3;

        return htmlBody;
    }
}