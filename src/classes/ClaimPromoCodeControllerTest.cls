@isTest(seeAllData=false)
public class ClaimPromoCodeControllerTest{

	public static void setupData(){
		List<Employee_Promo_code__c> empCodeLst = new List<Employee_Promo_code__c>();

		for(Integer ind = 0 ;ind < 50 ; ind++){

			Employee_Promo_code__c code = new Employee_Promo_code__c();
			code.User_Assigned__c = null;
			code.Promo_Code__c = 'dfdfgfg4'+ind;
			code.Redeem_URL__c = 'dfdfgfg4'+ind;
			empCodeLst.add(code);

		}
		
		insert empCodeLst;

	}

	public static testMethod void ClaimPromoCodeCtrlr(){

		ClaimPromoCodeController ctrlr = new ClaimPromoCodeController();
		ctrlr.requestedCodeInt = 3;
		ctrlr.allotPromoCodeAction();

		ctrlr.requestedCodeInt = 7;
		ctrlr.allotPromoCodeAction();

		ctrlr.requestedCodeInt = 0;
		ctrlr.allotPromoCodeAction();
	}


}