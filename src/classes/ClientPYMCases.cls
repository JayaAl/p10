public class ClientPYMCases {
    public List<Case> lstCases {get;set;}
    public Case cas;
    public ClientPYMCases(ApexPages.StandardController controller) {
       if (!Test.isRunningTest()) {
       controller.AddFields(new List<String>{'AccountId'});
       }
       this.cas = (Case)controller.getRecord();
       lstCases   = [Select id, CaseNumber, CreatedDate, Priority, Status, AccountId,Assigned_Reviewer__c, Ad_solutions_Approval__c from Case
                     where RecordType.name = 'Yield Approval Request' AND AccountId=:cas.AccountId ORDER BY CreatedDate DESC Limit 5 ];
            
        }
    }