public class CloneWithProductsExt {
    public List<Selectoption> listAllProd{get;set;}
    public List<PricebookEntry> listPE;
    public String selProduct{get;set;}
    public List<OpportunityLineItem> objOLI{get;set;}
    public String focusedLi{get;set;}
    public Integer oliIdx{get;set;}
    public List<OLIWrapper> listOLIWrapper{get;set;}
    public Boolean showSaveAllButton{get;set;}
    public Integer countAdded{get;set;}
    public Opportunity objOpp{get;set;}
    public String errorMessage{get;set;}
    public String pageId{get;set;}
    public String pageURL {set;}
    public Boolean reloadNeeded {get; set;}
    public Boolean disableButton {get;set;}
    Datetime currentDate = system.now();// Start date
    // public String PriceBookIdForPBE;
    
    public String getPageURL(){
        String newPageUrl = '/'+ApexPages.currentPage().getParameters().get('id')+'#'+pageId; 
        return newPageUrl;
    }
    
    // generic Constructor
    public CloneWithProductsExt(ApexPages.StandardController stdCont){
        init();             
    }
    
    private Opportunity oppById(String oppId){
        Opportunity theOpp = [
            SELECT CloseDate, CreatedDate, RecordType.Name, Pricebook2Id, Amount, CurrencyIsoCode, 
            AccountId, Account.Name, Account.BillingStreet, Account.BillingState, Account.BillingPostalCode,
            Account.BillingCountry, Account.BillingCity, FinancialForce_Invoice_Number__r.Name,
            RecordType.DeveloperName
            FROM Opportunity 
            WHERE Id =:oppId
        ];
        return theOpp;
    }
    
    private List<OpportunityLineItem> oliByOppId(String oppId){
        List<OpportunityLineItem> theList = [
            SELECT Date_Adjustment_Made__c, Audio_Everywhere_Split_Product__c, CreatedDate, HasSchedule, Duration__c,
            Discount__c, eCPM__c, End_Date__c, Impression_s__c, Performance_Type__c, PricebookEntry.Name,
            PricebookEntryId, ServiceDate, Quantity, CurrencyIsoCode, Total_Amount__c, UnitPrice,
            Banner__c, Banner_Type__c, Cost_Type__c, Medium__c, Offering_Type__c, Platform__c, Size__c, 
            Sub_Platform__c, Takeover__c
            FROM OpportunityLineItem o 
            WHERE OpportunityId =: oppId
            ORDER BY CreatedDate ASC
        ];
        return theList;
    }
    
    //This is init method called to reset the view
    public void init(){
        objOLI = new List<OpportunityLineItem>();
        listOLIWrapper = new List<OLIWrapper>();
        oliIdx = 0;
        reloadNeeded = false;
        disableButton = false;
        showSaveAllButton = false;
        countAdded = 0;
        pageId = [
            Select Id 
            FROM ApexPage 
            WHERE Name = 'OPT_PROD_UI_OpportunityLineItems'
        ].Id;
        pageId = pageId.substring(0,15);
        
        
        objOpp = oppById(System.currentPageReference().getParameters().get('id'));
        objOLI = oliByOppId(System.currentPageReference().getParameters().get('parentOppId'));
        
        for (OpportunityLineItem thisOLI : objOLI){
            OLIWrapper wrapper = new OLIWrapper(oliIdx++);
            wrapper.isNew = true;
            wrapper.item = thisOLI.clone(false, true, false, false);
            // clone(preserveId = false, isDeepClone = true, preserveReadonlyTimestamps = false, preserveAutonumber = false)
            wrapper.item.OpportunityId = System.currentPageReference().getParameters().get('id');
            wrapper.item.Product_ID1__c = null;
            // the following are nulled out on clone per ESS-26904
            wrapper.item.Platform__c = null;
            wrapper.item.Sub_Platform__c = null;
            wrapper.item.Banner__c = null;
            wrapper.item.Banner_Type__c = null;
            wrapper.item.Size__c = null;
            wrapper.item.Cost_Type__c = null;
            wrapper.item.Guaranteed__c = false; // Needs to be FALSE, checkboxes cannot be NULL
            listOLIWrapper.add(wrapper);
        }
        getListProd();
    }
    
    /* Helper to determine whether the Oppty should use updated Taxonomy layouts */
    public Set<String> setTaxonomyRTs{
        get{
            if(setTaxonomyRTs == null||setTaxonomyRTs.isEmpty()){
                setTaxonomyRTs = new Set<String>();
                for(TaxonomyRecordTypes__c t:[Select Name from TaxonomyRecordTypes__c]){
                    setTaxonomyRTs.add(t.Name);
                }
            }
            return setTaxonomyRTs;
        }
        set;
    }
    public Boolean isTaxonomyRT{
        get{
            return setTaxonomyRTs.contains(objOpp.RecordType.DeveloperName);
        }
        set;
    }
    
    //Called to get list of all products
    public void getListProd(){
        listAllProd = new List<Selectoption>(); 
        listPE = new List<PricebookEntry>();
        // Changed the if condition to DeveloperName condition from Name = 'Opportunity - Pandora One Subs'
        if(objOpp.RecordType.DeveloperName == 'Opportunity_Pandora_One_Subs'){
            listPE = [
                SELECT p.Id, p.Name, UnitPrice 
                FROM PricebookEntry p 
                WHERE IsActive=true
                AND Pricebook2.Name = 'Standard Price Book' 
                AND Name LIKE 'Pandora One %'
                AND CurrencyIsoCode =: objOpp.CurrencyIsoCode 
                ORDER BY Name DESC
            ];//Code modified by Lakshman on 23-04-2014 to fix multiple products issue for Pandora One Subs opps          
        } else {
            listPE = [
                SELECT p.Id, p.Name, UnitPrice 
                FROM PricebookEntry p 
                WHERE IsActive=true 
                AND Product2.Auto_Schedule__c = true 
                AND Pricebook2.Name = 'Standard Price Book'
                AND ( NOT Name LIKE 'Pandora One %' )
                AND CurrencyIsoCode =: objOpp.CurrencyIsoCode
                ORDER BY Name
            ];
        }
        for(PricebookEntry objPE: listPE){
            listAllProd.add(new SelectOption(objPE.Id,objPE.Name));
        }
    }
    
    //this method will deleted existing OLI
    public void deleteLineItem(){
        for(Integer i = 0; i < listOLIWrapper.size(); i++){
            if(listOLIWrapper.size() == 1){
                ApexPages.addMessage(new ApexPages.Message(
                    ApexPages.Severity.Warning
                    , 'Must have at least 1 product added'
                ));
                break;
            }
            if (listOLIWrapper[i].oliId == System.currentPageReference().getParameters().get('selectedID')){ 
                focusedLi = '';
                listOLIWrapper.remove(i);
                reloadNeeded = false;
                break;
            }
        }
    }    
    
    public void cancelAndContinue(){
        reloadNeeded = true;
    }
    
    // Validate OLI according to rules most recently updated in ESS-11221
    public boolean validateOLI(OpportunityLineItem theOLI){
        boolean hasErrors = false;
        
        if( theOLI.ServiceDate < objOpp.CloseDate && datetime.now() > objOpp.CreatedDate && String.valueOf(objOpp.RecordTypeId).substring(0,15) != Label.OpportunityRT_Performance){
            theOLI.ServiceDate.addError('Start Date must be after the Close Date'+ ' - OppId: ' + objOpp.id);
            hasErrors = true;
        }
        
        system.debug(logginglevel.info, 'HERE IT IS' + currentDate.Date().daysBetween(theOLI.ServiceDate) + '-----' + theOLI.ServiceDate);
        if(currentDate.Date().daysBetween(theOLI.ServiceDate) > 3650){
            theOLI.ServiceDate.addError('Start date cannot be over 10 years into the future');
            hasErrors = true;
        }
        if(currentDate.Date().daysBetween(theOLI.End_Date__c) > 3650){
            theOLI.End_Date__c.addError('End date cannot be over 10 years into the future');
            hasErrors = true;
        }
        if(theOLI.End_Date__c < theOLI.ServiceDate ){
            theOLI.End_Date__c.addError('End must be after start');
            hasErrors = true;
        }
        if(objOpp.RecordType.Name == 'Opportunity - Performance' && (theOLI.Impression_s__c == null || (theOLI.Impression_s__c == 0 && theOLI.Performance_Type__c != 'Monthly Adjustment'))){
            theOLI.Impression_s__c.addError('Please enter correct Impression'+ ' - OppId: ' + objOpp.id);
            hasErrors = true;
        }
        if(objOpp.RecordType.Name == 'Opportunity - Performance' && theOLI.Performance_Type__c == 'Monthly Adjustment' && theOLI.Date_Adjustment_Made__c == null){
            theOLI.Date_Adjustment_Made__c.addError('Adjustment Date cannot be blank if Monthly Adjustment is selected'+ ' - OppId: ' + objOpp.id);
            hasErrors = true;
        }
        if(objOpp.RecordType.Name == 'Opportunity - Performance' && theOLI.Performance_Type__c != 'Monthly Adjustment' && theOLI.Date_Adjustment_Made__c != null){
            theOLI.Date_Adjustment_Made__c.addError('Adjustment Date can only be added if Monthly Adjustment is selected'+ ' - OppId: ' + objOpp.id);
            hasErrors = true;
        }
        return hasErrors;
    }
    
    //called to save all the OLI
    public void saveAllLineItem(){
        try{
            List<OpportunityLineItem> massOLI = new List<OpportunityLineItem>();
            Boolean hasErrors = false;
            for(Integer i = 0; i < listOLIWrapper.size(); i++){
                if(listOLIWrapper[i].isNew == true){
                    hasErrors = hasErrors || validateOLI(listOLIWrapper[i].item);
                    if(hasErrors){
                        continue;
                    }
                    listOLIWrapper[i].item.Duration__c = listOLIWrapper[i].item.ServiceDate.daysBetween(listOLIWrapper[i].item.End_Date__c)+1;
                    
                     // Changed the if condition to DeveloperName condition from Name = 'Opportunity - Pandora One Subs'        
                    if(objOpp.RecordType.DeveloperName == 'Opportunity_Pandora_One_Subs') {
                        OPT_PROD_STATIC_HELPER.runForProds = false;
                        if(listOLIWrapper[i].item.Discount__c <> NULL)
                            listOLIWrapper[i].item.UnitPrice = listOLIWrapper[i].item.UnitPrice - (listOLIWrapper[i].item.UnitPrice * listOLIWrapper[i].item.Discount__c.divide(100,2));
                    } else {
                        if(objOpp.RecordType.Name == 'Opportunity - Performance'){
                            if(listOLIWrapper[i].item.Performance_Type__c != 'Monthly Adjustment')
                                listOLIWrapper[i].item.eCPM__c = (listOLIWrapper[i].item.UnitPrice/listOLIWrapper[i].item.Impression_s__c)*1000;
                            else if(listOLIWrapper[i].item.Impression_s__c != 0)
                                listOLIWrapper[i].item.eCPM__c = (listOLIWrapper[i].item.UnitPrice/listOLIWrapper[i].item.Impression_s__c)*1000;
                            else
                                listOLIWrapper[i].item.eCPM__c = 0;
                        }
                        OPT_PROD_STATIC_HELPER.runForProds = true;
                    }
                    massOLI.add(listOLIWrapper[i].item);
                    listOLIWrapper[i].isNew = true;
                }
            }
            if(hasErrors){
                return;
            }
            if(massOLI.size() > 0 && massOLI.size() == listOLIWrapper.size()) {
                insert massOLI;
                system.debug('@@@@       '+massOLI);
                focusedLi = '';
                reloadNeeded = true;
            }
        } catch(Exception ex) {
            if(ex.getMessage().contains('Owner role for Opportunity must match Agency')){
                errorMessage = 'Owner role for Opportunity must match Agency or Advertiser before you may continue to save!';
            }
            system.debug('Exception Caught@@@'+ex.getMessage());
        }    
    }
    
    //Everything is here in the wrapper
    public class OLIWrapper{
        public OpportunityLineItem item { get; set;}
        public String oliId{get;set;}
        public Boolean isNew{get;set;} 
        // public String priceBookId{get;set;}   
        public OLIWrapper(Integer oliId){
            item = new OpportunityLineItem();
            this.oliId = ''+ oliId;
            isNew = true;
        }
    }
    
}