@isTest
public class CloneWithProductsExtTest {
    private static Opportunity      opp;
    private static boolean        siteInit    = false;
    
    private static Product2 testprod1;
    private static Product2 testprod2;
    private static Pricebook2 testpb;
    private static PricebookEntry testpbe1;
    private static PricebookEntry testpbe2;
    private static Account acct1;
    private static Opportunity Oppty1;
    
/*    
Methods (10)
public	void cancelAndContinue()
public	ANY clone()
public	void deleteLineItem()
public	void getListProd()
public	String getPageURL()
public	void init()
private	List oliByOppId(String oppId)
private	Opportunity oppById(String oppId)
public	void saveAllLineItem()
private	Boolean validateOLI(OpportunityLineItem theOLI)
*/
    
/*
Properties
public	Integer countAdded
private	Datetime currentDate
public	Boolean disableButton
public	String errorMessage
public	String focusedLi
public	Boolean isTaxonomyRT
public	List listAllProd
public	List listOLIWrapper
public	List listPE
public	List objOLI
public	Opportunity objOpp
public	Integer oliIdx
public	String pageId
public	String pageURL
public	Boolean reloadNeeded
public	String selProduct
public	Set setTaxonomyRTs
public	Boolean showSaveAllButton
*/
    
    
    /* Data preparation and initialization at the bottom of the class*/
    
    static testMethod void myUnitTest(){
        DataPrep();
        System.test.starttest();
            Test.setCurrentPage(Page.OPT_PROD_UI_OpportunityLineItems);
            System.currentPageReference().getParameters().put('id',Oppty1.Id);
            ApexPages.StandardController testController = new ApexPages.Standardcontroller(Oppty1);
            CloneWithProductsExt ext = new CloneWithProductsExt(testController);
            ext.selProduct ='testProd';
            ext.errorMessage='errorMess';
            ext.pageURL ='test';
            ext.getPageURL();
            ext.deleteLineItem();
            ext.saveAllLineItem();
            ext.cancelAndContinue();
        System.test.stoptest();    
    }
      
    static testMethod void testPandoraOneSubs(){
        DataPrep();
        Oppty1.RecordTypeId = UTIL_TestUtil.findOpptyRTByName(System.Label.Oppty_Gift_Code_record_type_label);
        update Oppty1;
        System.test.starttest();
            Test.setCurrentPage(Page.OPT_PROD_UI_OpportunityLineItems);
            System.currentPageReference().getParameters().put('id',Oppty1.Id);
            ApexPages.StandardController testController = new ApexPages.Standardcontroller(Oppty1);
            CloneWithProductsExt ext = new CloneWithProductsExt(testController);
        System.test.stoptest();    
    }
    
    static testMethod void testdeleteLineItemError(){
        DataPrep();
        System.test.starttest();
            Test.setCurrentPage(Page.OPT_PROD_UI_OpportunityLineItems);
            System.currentPageReference().getParameters().put('id',Oppty1.Id);
            ApexPages.StandardController testController = new ApexPages.Standardcontroller(Oppty1);
            CloneWithProductsExt ext = new CloneWithProductsExt(testController);
        
        	// Clear the listOLIWrapper collection so that deleteLinetItem() throws an error
        	ext.listOLIWrapper = new List<CloneWithProductsExt.OLIWrapper>();
        	CloneWithProductsExt.OLIWrapper wrapper = new CloneWithProductsExt.OLIWrapper(1);
            ext.listOLIWrapper.add(wrapper);	
        	ext.deleteLineItem();
        	
        	system.assert(apexpages.hasMessages(ApexPages.Severity.Warning),'Expected Warning Severity: "Warning" not received!');
            List<Apexpages.Message> msgs = ApexPages.getMessages();
            boolean b = false;
            for(Apexpages.Message msg:msgs){
                if (msg.getDetail().contains('Must have at least 1 product added')) b = true;
            }
            system.assert(b,'Expected Warning Message: "Must have at least 1 product added" not received!');
        System.test.stoptest(); 
    }
    
    static testMethod void testvalidateOLI(){
        DataPrep();
        // test Oppty and OLI values to be changed to test each validation
        Oppty1.CloseDate = Date.today();
        Oppty1.RecordTypeId = UTIL_TestUtil.findOpptyRTByName('Opportunity - Performance');
        update Oppty1;
        
        OpportunityLineItem testOLI = new OpportunityLineItem(
        	OpportunityId = Oppty1.Id
        );
        
        System.test.starttest();
            Test.setCurrentPage(Page.OPT_PROD_UI_OpportunityLineItems);
            System.currentPageReference().getParameters().put('id',Oppty1.Id);
            ApexPages.StandardController testController = new ApexPages.Standardcontroller(Oppty1);
            CloneWithProductsExt ext = new CloneWithProductsExt(testController);
        	
        
        /*
        validateOLI(OpportunityLineItem theOLI)

        'Start Date must be after the Close Date'+ ' - OppId: ' + objOpp.id
        'Start date cannot be over 10 years into the future'
        'End date cannot be over 10 years into the future'
        'End must be after start'
        'Please enter correct Impression'+ ' - OppId: ' + objOpp.id
        'Adjustment Date cannot be blank if Monthly Adjustment is selected'+ ' - OppId: ' + objOpp.id
        'Adjustment Date can only be added if Monthly Adjustment is selected'+ ' - OppId: ' + objOpp.id
        */
		System.test.stoptest();        
    }
    
        
    static testmethod void test_OtherTests(){
        DataPrep();
        
        Test.setCurrentPage(Page.OPT_PROD_UI_OpportunityLineItems);
        Test.setCurrentPage(Page.OPT_PROD_UI_OpportunityLineItems);
        System.currentPageReference().getParameters().put('id',Oppty1.Id);
        System.currentPageReference().getParameters().put('parentOppId',Oppty1.id) ;
        
        test.startTest();
            ApexPages.StandardController testController = new ApexPages.Standardcontroller(Oppty1);
            CloneWithProductsExt ext = new CloneWithProductsExt(testController);
            ext.selProduct ='testProd';
            ext.errorMessage='errorMess';
            ext.pageURL ='test';
            //ext.focusedLi ='test';
            ext.getPageURL();
            ext.saveAllLineItem();
            List<CloneWithProductsExt.OLIWrapper> lstOwrap = new List<CloneWithProductsExt.OLIWrapper>();
            CloneWithProductsExt.OLIWrapper owrap = new CloneWithProductsExt.OLIWrapper(2);
            owrap.isNew = true;
            //owrap.selected= true;
            owrap.item.Quantity =1;
            owrap.item.UnitPrice=1;
            lstOwrap.add(owrap);
            System.currentPageReference().getParameters().put('selectedID','2');
            
            ext.deleteLineItem();
            ext.saveAllLineItem();
            
            System.currentPageReference().getParameters().put('id',Oppty1.Id);
            ext.deleteLineItem();
            ApexPages.StandardController testController1 = new ApexPages.Standardcontroller(Oppty1);
            CloneWithProductsExt ext1 = new CloneWithProductsExt(testController1);
            set<string> recordTypes = new set<string>();
            recordTypes = ext1.setTaxonomyRTs;
            
            boolean isTaxonomyRT = ext1.isTaxonomyRT;
            ext1.selProduct ='testProd';
            ext1.errorMessage='errorMess';
            ext1.pageURL ='test';
            //ext.focusedLi ='test';
            ext1.getPageURL();
            ext1.saveAllLineItem();
        test.stopTest();
    }
    
    
    static testMethod void ValidateErrors(){
        DataPrep();
        System.test.starttest();
            Test.setCurrentPage(Page.OPT_PROD_UI_OpportunityLineItems);
            
            List<Apexpages.Message> pageMessages = ApexPages.getMessages();
            
            System.currentPageReference().getParameters().put('id',Oppty1.Id);
            ApexPages.StandardController testController = new ApexPages.Standardcontroller(Oppty1);
            CloneWithProductsExt ext = new CloneWithProductsExt(testController);
            ext.selProduct ='testProd';
            ext.errorMessage='errorMess';
            ext.pageURL ='test';
            ext.getPageURL();
            ext.deleteLineItem();
            ext.saveAllLineItem();
            //ext.cancelAndContinue();
        System.test.stoptest();   
            
    }
    
    /* Data preparation for all tests */
    static void DataPrep(){
        // Create Products 
        testprod1 = UTIL_TestUtil.generateProduct();
        testprod2 = UTIL_TestUtil.generateProduct();
        insert new Product2[]{testprod1,testprod2};
            
        // Get Pricebook 
        testpb = [select id from Pricebook2 where isStandard=true];
        
        // Add to pricebook 
        testpbe1 = UTIL_TestUtil.generatePricebookEntry(testprod1.Id, testpb.Id);
            //testpbe1.UseStandardPrice = false;
            testpbe1.UnitPrice = 250;
        testpbe2 = UTIL_TestUtil.generatePricebookEntry(testprod2.Id, testpb.Id);
            //testpbe2.UseStandardPrice = false;
            testpbe2.UnitPrice = 250;
        insert new PricebookEntry[]{testpbe1,testpbe2};
        
        acct1 = UTIL_TestUtil.createAccount(); 
        
        Oppty1 = UTIL_TestUtil.generateOpportunity(acct1.Id);
            Oppty1.RecordTypeId = UTIL_TestUtil.findOpptyRTByName('Opportunity - Performance');
            Oppty1.Industry_Sub_Category__c='Political Campaign Marketing';
            Oppty1.Type='Advertiser';
        insert Oppty1;
        
        List<OpportunityLineItem> bulkoli = new List<OpportunityLineItem>();
        for(integer bi=0; bi<6; bi++) {
            bulkoli.add(
                new OpportunityLineItem(
                    Quantity = 1,
                    Duration__c=12, 
                    UnitPrice = 1, 
                    PriceBookEntryId = testpbe2.id,
                    Impression_s__c=10, 
                    OpportunityId = Oppty1.id, 
                    End_Date__c=Date.today().addDays(11),
                    ServiceDate = Date.today().addDays(11)
                )
            );
        } 
        insert bulkoli;
    }
}