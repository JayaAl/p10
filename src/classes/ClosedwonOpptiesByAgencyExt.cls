public class ClosedwonOpptiesByAgencyExt {
    public List<Opportunity> lstOpp {get;set;}   
    public Case cas;  
   
    public ClosedwonOpptiesByAgencyExt(ApexPages.StandardController controller) {
        if(!Test.isRunningTest()){
        controller.AddFields(new List<String>{'AccountId'});
        }
        this.cas = (Case)controller.getRecord();
        if(ApexPages.currentPage().getParameters().get('pg')!='1'){
            lstOpp = [SELECT Id, Name,View_in_System__c,StageName,Amount,CloseDate,owner.Name,AccountId FROM Opportunity where StageName = 'Closed Won' AND AccountId=:cas.AccountId AND Account.Type = 'Ad Agency' limit 5];            
        }else {
            lstOpp = [SELECT Id, Name,View_in_System__c,StageName,Amount,CloseDate,owner.Name,AccountId FROM Opportunity where StageName = 'Closed Won' AND AccountId=:cas.AccountId AND Account.Type = 'Ad Agency'];            
        }
           }

}