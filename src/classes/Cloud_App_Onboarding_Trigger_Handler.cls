public class Cloud_App_Onboarding_Trigger_Handler {
    
    private Cloud_Application_Onboarding_Util util = new Cloud_Application_Onboarding_Util();
    public Cloud_App_Onboarding_Trigger_Handler() {
        
    }

/* Delete ERP_Role_Assignment_Group__c records when checkbox marked */
    public void delete_ERP_Role_Assignment_Group(List<ERP_Role_Assignment_Group__c> theList){
        try{
            List<ERP_Role_Assignment_Group__c> listToDelete = new List<ERP_Role_Assignment_Group__c>();
            for(ERP_Role_Assignment_Group__c ag:theList){
                if(ag.toDelete__c){
                    // Cannot perform DML on trigger.new, creating instances of new ERP_Role_Assignment_Group__c records to delete
                    listToDelete.add(new ERP_Role_Assignment_Group__c(Id=ag.Id)); 
                }
            }
            if(!listToDelete.isEmpty()){
                delete listToDelete;
            }
        } catch (Exception e){
            logger.logMessage(
                'Cloud_App_Onboarding_Trigger_Handler', 
                'delete_ERP_Role_Assignment_Group', 
                e.getMessage(),
                'ERP_Role_Assignment_Group__c', 
                null, 
                null
            );
            return;
        }
    }
    
/* Import Roles and Modules */
    public void updateRoleAndModuleStaging(List<ERP_Role_and_Module_Staging__c> newStaging){
        // system.assert(false,'----> Begin');
        Set<ERP_Role_and_Module_Staging__c> toProcess = new Set<ERP_Role_and_Module_Staging__c>();
        for(ERP_Role_and_Module_Staging__c e:newStaging){
            // system.assert(false,'----> Pre-Staging');
            // If Processing_Complete__c then go to next record
            if(e.Processing_Complete__c){
                continue;
            }
            toProcess.add(e); // Add to collection to be processed later
        }

    // Create modules as needed
        // Gather Modules by Name
        mapNameToModule = new Map<String, ERP_Module__c>();
        setModuleIds = new Set<Id>();
        for(ERP_Module__c m:util.listERPModules()){
            // system.assert(false,'----> Match Module');
            mapNameToModule.put(m.Name, m);
            setModuleIds.add(m.Id);
        }

        Set<ERP_Module__c> setModules = new Set<ERP_Module__c>();
        for(ERP_Role_and_Module_Staging__c stage:toProcess){
            // system.assert(false,'----> process Module');
            // for each record inserted check to see if there is a Module by the same name
            ERP_Module__c thisMod = new ERP_Module__c( // create a placeholder Module
                Name = stage.APPLICATION__c,
                Business_Owner__c = thisERPUserId
            );
            if(mapNameToModule.containsKey(stage.APPLICATION__c)){ // overwrite it with an existing record if one exists
                thisMod = mapNameToModule.get(stage.APPLICATION__c);
            }
            setModules.add(thisMod);
        }
        if(!setModules.isEmpty()){
            // system.assert(false,'----> Upsert Module');
            upsert new List<ERP_Module__c>(setModules);
            for(ERP_Module__c m:util.listERPModules()){ // repopulate this Map to capture any new Modules
                mapNameToModule.put(m.Name, m);
                setModuleIds.add(m.Id);
            }
        }
        
    // Create Roles as needed
        // Gather Roles by GUID
        mapGUIDToERPRole = new Map<String, ERP_Role__c>();
        for(ERP_Role__c r: util.listERPRoles()){
            // system.assert(false,'----> Get existing Role');
            mapGUIDToERPRole.put(r.Name.Trim(),r);
        }
        Set<ERP_Role__c> setRoles = new Set<ERP_Role__c>();
        for(ERP_Role_and_Module_Staging__c stage:toProcess){
            // system.assert(false,'----> Process Role');
            String GUID = stage.ROLE_GUID__c.Trim();
            Id moduleId = mapNameToModule.get(stage.APPLICATION__c).Id;
            ERP_Role__c theRole = new ERP_Role__c(Name = GUID); // Create a placeholder Role
            if(mapGUIDToERPRole.containsKey(GUID)){
                theRole = mapGUIDToERPRole.get(GUID);
                // system.assert(false,'matching Role: '+theRole);
            }
            // system.assert(false,'--> ' + GUID + ' || ' + mapGUIDToERPRole);
            // overwrite all other values with those found within the import file
            theRole.ROLE_NAME__c = stage.ROLE_NAME__c;
            theRole.ROLE_ID__c = stage.ROLE_ID__c;
            // Name = GUID;
            theRole.ABSTRACT_ROLE__c = stage.ABSTRACT_ROLE__c;
            theRole.JOB_ROLE__c = stage.JOB_ROLE__c;
            theRole.DATA_ROLE__c = stage.DATA_ROLE__c;
            theRole.ACTIVE_FLAG__c = stage.ACTIVE_FLAG__c;
            theRole.ROLE_COMMON_NAME__c = stage.ROLE_COMMON_NAME__c;
            theRole.DESCRIPTION__c = stage.DESCRIPTION__c;
            theRole.ROLE_DISTINGUISHED_NAME__c = stage.ROLE_DISTINGUISHED_NAME__c;
            theRole.ERP_Module__c = moduleId;

            // finally update the collections
            setRoles.add(theRole);
            stage.Processing_Complete__c = TRUE;
        }
        if(!setRoles.isEmpty()){
            // system.assert(false,'----> Upsert Role');
            upsert new List<ERP_Role__c>(setRoles);
        }

        // system.assert(false,'----> END');
    }
    
/* Populate Role Assignment Group with related Role names based on a Set of Group IDs */
    public void populateGroupRoles(Set<Id> groupIds){
        // Collect all Assignments by Assignment Group
        Map<Id, List<ERP_Role_Assignment__c>> mapGroupToAssignments = new Map<Id, List<ERP_Role_Assignment__c>>();
        
        // Gather all Role Assignments for the Assignment Group
        for(ERP_Role_Assignment__c a:[
            SELECT Id,ERP_Role_ROLE_NAME__c, ERP_Role_Assignment_Group__c, 
                ERP_Role__r.ERP_Module__r.Business_Owner__r.User__c, ERP_User__r.Manager__c
    		FROM ERP_Role_Assignment__c
            WHERE ERP_Role_Assignment_Group__c IN:groupIds
            ORDER BY ERP_Role_Assignment_Group__c, ERP_Role_ROLE_NAME__c
        ]){
            if(!mapGroupToAssignments.containsKey(a.ERP_Role_Assignment_Group__c)){
                mapGroupToAssignments.put(a.ERP_Role_Assignment_Group__c, new List<ERP_Role_Assignment__c>());
            }
            mapGroupToAssignments.get(a.ERP_Role_Assignment_Group__c).add(a);
        }

        Map<Id, String> mapIdToRoleList = new Map<Id, String>();
        Map<Id, Id> mapIdToBusinessOwner = new Map<Id, Id>();
        Map<Id, Id> mapIdToManager = new Map<Id, Id>();
        for(Id i:mapGroupToAssignments.keySet()){
            for(ERP_Role_Assignment__c a:mapGroupToAssignments.get(i)){
                if(!mapIdToRoleList.containsKey(i)){
                    mapIdToRoleList.put(i,a.ERP_Role_ROLE_NAME__c);// 1st Entry
                } else {
                    mapIdToRoleList.put(i,mapIdToRoleList.get(i)+'\n' + a.ERP_Role_ROLE_NAME__c);// Nth entry
                }
                
                if(!mapIdToBusinessOwner.containsKey(i)){
                    mapIdToBusinessOwner.put(i, a.ERP_Role__r.ERP_Module__r.Business_Owner__r.User__c);
                }
                if(!mapIdToManager.containsKey(i)){
                    mapIdToManager.put(i, a.ERP_User__r.Manager__c);
                }
            }
        }
        // Update each Assignment Group's Related_Roles__c
        List<ERP_Role_Assignment_Group__c> theGroups = new List<ERP_Role_Assignment_Group__c>();
        for(Id i:mapIdToRoleList.keySet()){
            ERP_Role_Assignment_Group__c g = new ERP_Role_Assignment_Group__c(
            	Id = i,
                Related_Roles__c = mapIdToRoleList.get(i)
            );
            
            if(mapIdToBusinessOwner.containsKey(i)){
                g.Assigned_Approver__c = mapIdToBusinessOwner.get(i);
            }
            if(mapIdToManager.containsKey(i)){
                g.Approver_Manager__c = mapIdToManager.get(i);
            }
            
            theGroups.add(g);    
        }
        
        if(!theGroups.isEmpty()){
            update theGroups;
        }
    }

/* Import User Role Assignment */
    public void updateRoleStaging(List<ERP_Role_Assignment_Staging__c> newStaging){
        Set<ERP_Role_Assignment_Staging__c> toProcess = new Set<ERP_Role_Assignment_Staging__c>();
        setEmailAddress = new Set<String>();
        setGUIDs = new Set<String>();
        
        for(ERP_Role_Assignment_Staging__c e:newStaging){
            // If Processing_Complete__c then add to collection to be deleted, go to next record
            if(e.Processing_Complete__c){
                continue;
            }

            // Otherwise validate against existing records, creating as necessary
            String theEmail = e.EMAIL_ADDRESS__c.toLowerCase();
            setEmailAddress.add(theEmail); // gather user email address
            String theGUID = e.ROLE_GUID__c;
            setGUIDs.add(theGUID); // gather Role GUID
            toProcess.add(e);

        }

    // Get corresponding User and Role records for all users
        // Gather users by Email address
        mapEmailToERPUser = new Map<String, ERP_User__c>();
        setUserIds = new Set<Id>();
        for(ERP_User__c u: util.listERPUserByEmail(setEmailAddress)){
            mapEmailToERPUser.put(u.User_Email_Address__c.toLowerCase(),u);
            setUserIds.add(u.Id);
        }
        
        // Gather Roles by GUID
        mapGUIDToERPRole = new Map<String, ERP_Role__c>();
        setRoleIds = new Set<Id>();
        for(ERP_Role__c r: util.listERPRolesByGUID(setGUIDs)){
            mapGUIDToERPRole.put(r.Name,r);
            setRoleIds.add(r.Id);
        }

    // Gather all current assignments for indicated Users
        mapAssignmentGroups = new Map<Id, ERP_Role_Assignment_Group__c>();
        Map<Id, ERP_Role_Assignment_Group__c> newRAG = new Map<Id, ERP_Role_Assignment_Group__c>(); // collected Assignment Groups already imported by Oracle
        for(ERP_Role_Assignment_Group__c g:util.listAssignmentGroupsByUser(setUserIds)){
            mapAssignmentGroups.put(g.Id, g);
            if(g.Imported_from_Oracle__c){
                newRAG.put(g.Assigned_User__c,g);
            }
        }

        mapRoleAssignments = new Map<Id, Map<Id, ERP_Role_Assignment__c>>();
        // List<ERP_Role_Assignment__c> listAssignmentsById(List<Id> listIds)
        for(ERP_Role_Assignment__c a:util.listAssignmentsById(setUserIds)){
            if(!mapRoleAssignments.ContainsKey(a.ERP_User__c)){
                mapRoleAssignments.put(a.ERP_User__c, new Map<Id, ERP_Role_Assignment__c>());
            }
            mapRoleAssignments.get(a.ERP_User__c).put(a.ERP_Role__c, a);
        }
        
        mapRoleReviews = new Map<Id, Map<Id, ERP_Role_Review__c>>();
        for(ERP_Role_Review__c r:util.listRoleReviewsForUserAndPeriod(setUserIds, util.currentReviewPeriod)){
            if(!mapRoleReviews.ContainsKey(r.ERP_Role_Assignment__r.ERP_User__c)){
                mapRoleReviews.put(r.ERP_Role_Assignment__r.ERP_User__c, new Map<Id, ERP_Role_Review__c>());
            }
            mapRoleReviews.get(r.ERP_Role_Assignment__r.ERP_User__c).put(r.ERP_Role_Assignment__r.ERP_Role__c, r);
        }
        
        Set<ERP_Role_Assignment_Group__c> setRAG = new Set<ERP_Role_Assignment_Group__c>();
        Set<ERP_Role_Assignment__c> setRA =  new Set<ERP_Role_Assignment__c>();
        Set<ERP_Role_Review__c> setRR = new Set<ERP_Role_Review__c>();
        Set<ERP_Role_Assignment_Staging__c> setStage = new Set<ERP_Role_Assignment_Staging__c>();
        DateTime now = Datetime.now();
    // for each record remaining in newStaging
        for(ERP_Role_Assignment_Staging__c stage: toProcess){
            
            ERP_Role_Assignment_Group__c theRAG = new ERP_Role_Assignment_Group__c();
            ERP_Role_Assignment__c theRA =  new ERP_Role_Assignment__c();
            ERP_Role_Review__c theRR = new ERP_Role_Review__c();
            
            Id userId = null;
            if(!mapEmailToERPUser.containsKey(stage.EMAIL_ADDRESS__c.toLowerCase())){
                stage.Processing_Complete__c = false;
                stage.Errors__c = stage.Errors__c + 'EMAIL_ADDRESS__c: '+ stage.EMAIL_ADDRESS__c.toLowerCase() +' not found | ';
                setStage.add(stage);
                continue;
            } else {
                userId = mapEmailToERPUser.get(stage.EMAIL_ADDRESS__c.toLowerCase()).Id;
            }

            Id roleId = null;
            if(!mapGUIDToERPRole.containsKey(stage.ROLE_GUID__c)){
                stage.Processing_Complete__c = false;
                stage.Errors__c = stage.Errors__c + 'ROLE_GUID__c: '+ stage.ROLE_GUID__c +' not found | ';
                setStage.add(stage);
                continue;
            } else {
                roleId = mapGUIDToERPRole.get(stage.ROLE_GUID__c).Id;
            }

            // find if there is a Role_Assignment__c
            if(mapRoleAssignments.containsKey(userId) && mapRoleAssignments.get(userId).containsKey(roleId)){
                // If the record is found, return the record
                theRA = mapRoleAssignments.get(userId).get(roleId);
                theRAG = mapAssignmentGroups.get(theRA.ERP_Role_Assignment_Group__c);
            } else {
                // If no record is found, create one, also create a ERP_Role_Assignment_Group__c
                if(newRAG.containsKey(userId)){
                    theRAG = newRAG.get(userId);
                } else {
                    theRAG = new ERP_Role_Assignment_Group__c(
                        Approval_Status__c = 'Imported', 
                        Imported_from_Oracle__c = true, 
                        Assigned_User__c = userId, 
                        Requestor__c = thisERPUserId // Always associate these requests to the running user
                    );
                }
                newRAG.put(userId,theRag); // Add the Assignment Group to a Map<UserId,Assignment Group> for future reference
                theRA = new ERP_Role_Assignment__c(
                    ERP_User__c = userId, 
                    ERP_Role__c = roleId,
                    ERP_Role_Assignment_Group__c = theRAG.Id // setting placeholder which will need to be populated after inserting the new Group records
                );
            }
            setRAG.add(theRAG);
            setRA.add(theRA);

            // find if there is a Role_Review__c 
            if(mapRoleReviews.containsKey(userId) && mapRoleReviews.get(userId).containsKey(roleId)){
                // If the record is found:
                    theRR = mapRoleReviews.get(userId).get(roleId);
                    theRR.Latest_Timestamp__c = now; // Update the ERP_Role_Review__c.Latest_Timestamp__c value
            } else {
                // If no record is found:
                    // Create as needed
                theRR = new ERP_Role_Review__c(
                    ERP_Role_Assignment__c = theRA.Id, // populated after the Role_Assignment__c records are upserted
                    ERP_Role__c = roleId,
                    ERP_User__c = userId,
                    Latest_Timestamp__c = now,
                    Review_Period__c = util.currentReviewPeriod
                );
            }
            setRR.add(theRR);

            stage.Processing_Complete__c = true; // update ERP_Role_Assignment_Staging__c.Processing_Complete__c
            setStage.add(stage); // Add to collection of ERP_Role_Assignment_Staging__c records to update
        }

    /* DML */
        // Upsert Role_Assignment_Group__c
        if(!setRAG.isEmpty() && (setRAG.size()>0)){
            List<ERP_Role_Assignment_Group__c> theList = new List<ERP_Role_Assignment_Group__c>(setRAG);
            upsert theList;
            for(ERP_Role_Assignment_Group__c g: theList){
                newRAG.put(g.Assigned_User__c,g);
            }
        }


        
        // Upsert Role_Assignment__c
        if(!setRA.isEmpty()){
            for(ERP_Role_Assignment__c a:setRA){
                if(a.ERP_Role_Assignment_Group__c==null){
                    a.ERP_Role_Assignment_Group__c = newRAG.get(a.ERP_User__c).Id;
                }
            }
            upsert new List<ERP_Role_Assignment__c>(setRA);
            populateAssignment(); // repopulate mapRoleAssignments to be used for 
        }

        // Upsert Role_Review__c
        if(!setRR.isEmpty()){
            for(ERP_Role_Review__c r:setRR){
                if(r.ERP_Role_Assignment__c==NULL){
                    r.ERP_Role_Assignment__c = mapRoleAssignments.get(r.ERP_User__c).get(r.ERP_Role__c).Id;
                }
            }
            upsert new List<ERP_Role_Review__c>(setRR);
        }

    }

    private Id thisERPUserId{ // Always returns the ERP_User__c record that corresponds to the current user
        get{ 
            if(thisERPUserId==NULL){
                thisERPUserId = util.erpUserByEmail(UserInfo.getUserEmail()).Id;
            }
        return thisERPUserId;
        }
        set;
    }

    private Set<ERP_Role_Assignment_Staging__c> toDelete;
    private Set<String> setEmailAddress;
    private Set<String> setGUIDs;
    private Map<Id, Map<Id, ERP_Role_Review__c>> mapRoleReviews; // Map of <ERP_User__c.Id, Map<ERP_Role__c.Id, ERP_Role_Review__c>>
    private Map<Id, ERP_Role_Assignment_Group__c> mapAssignmentGroups;
    private Map<Id, Map<Id, ERP_Role_Assignment__c>> mapRoleAssignments;
    private Set<Id> setUserIds;
    private Set<Id> setRoleIds;
    private Map<String, ERP_User__c> mapEmailToERPUser;
    private Map<String, ERP_Role__c> mapGUIDToERPRole;

    private Map<String, ERP_Module__c> mapNameToModule;
    private Set<Id> setModuleIds;
    private Set<String> setModules;

    private void populateAssignment(){
        mapRoleAssignments = new Map<Id, Map<Id, ERP_Role_Assignment__c>>();
        // List<ERP_Role_Assignment__c> listAssignmentsById(List<Id> listIds)
        for(ERP_Role_Assignment__c a:util.listAssignmentsById(setUserIds)){
            if(!mapRoleAssignments.ContainsKey(a.ERP_User__c)){
                mapRoleAssignments.put(a.ERP_User__c, new Map<Id, ERP_Role_Assignment__c>());
            }
            mapRoleAssignments.get(a.ERP_User__c).put(a.ERP_Role__c, a);
        }
    }

}