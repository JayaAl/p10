public without sharing class Cloud_Application_OnboardingController {

/* Constructor */
    public Cloud_Application_OnboardingController(){

        try{
            initializeVars();
        } catch (Exception e){
            util.addError(e);
            util.hasErrors = true;
        }
    }
    
/* Method to initialize all vars for page functionality */
    private void initializeVars(){
        try{
            // Populate initial values for any working vars on the page
            util.hasErrors = false;
            showModules = false;
            showPreRequisites = false;
            selectedModule = '';
            selectedRole = new List<Id>();
            newAG = new ERP_Role_Assignment_Group__c(
                Requestor__c = thisERPUserId, // default both requestor and assignee to the current user
                Assigned_User__c = thisERPUserId,
                Approval_Status__c = 'Draft'
            );

            // clear any collection and or fields that were populated
            preRequisites = new Map<Id,String>();
            roleOptions = new List<SelectOption>();
            sessionAssignmentGroupIds = new Set<Id>();
        } catch (Exception e){
            util.addError(e);
            util.hasErrors = true;
        }
    }

/* Step 1: Select Oracle users */
    public void selectUser(){
        try{
            // if newAG.Assigned_User__c is empty return an error
            if(newAG.Assigned_User__c == NULL){
                util.addError(ApexPages.Severity.FATAL,'Invalid Assigned User selected, please Select a valid user to continue.');
                return;
            }
            // Get any assigned Roles, and their Approval Status
            populateMapUserRoles(newAG.Assigned_User__c);
            gatherModules();
            gatherRoles();
            showModules = true;
            showPreRequisites = false;
        } catch (Exception e){
            system.debug('eee:'+e.getMessage()+'Line No'+e.getLineNumber());
            util.addError(e);
            util.hasErrors = true;
        }
        
    }
    public void backToStep1(){
        try{
            initializeVars();
        } catch (Exception e){
            util.addError(e);  util.hasErrors = true;
        }
    }

/* Step 1.5: Create new User */
// Functionality not requered for release 1

/* Step 2: Select Roles for assignment */
    public void selectRoles(){
        try{
            populatePreRequisites(selectedRole);
            showPreRequisites = true;
        } catch (Exception e){
            util.addError(e); util.hasErrors = true;
        }
    }
    public void backToStep2(){
        try{
            showPreRequisites = false;
        } catch (Exception e){
            util.addError(e);  util.hasErrors = true;
        }
    }

/* Step 3: Confirm Pre-Requisites */
    public void saveAndAddModule(){
        try{
            upsertRoleAndModule(false);
            if(util.hasErrors){
                return; // If errors were encountered then end the process, do not reset any vars.
            }
            // repopulate the vars necessary for stage2
            selectUser();
            util.addError(ApexPages.Severity.CONFIRM, 'Progress has been saved, continue selecting Roles for user.');
        } catch (Exception e){
            util.addError(e);
            util.hasErrors = true;
        }
    }

    public void saveRoleRequests(){
        try{
            upsertRoleAndModule(true);
            if(util.hasErrors){
                return; // If errors were encountered then end the process, do not reset any vars.
            }
            util.addError(ApexPages.Severity.CONFIRM, 'Roles submitted for approval.');
            
            // clear all vars and go back to stage1
            initializeVars();
        } catch (Exception e){
            util.addError(e);
            util.hasErrors = true;
        }
    }

    public Set<Id> sessionAssignmentGroupIds{get;set;}
    public void upsertRoleAndModule(boolean complete){
        // get any open Role Requests for the same Requestor and Assignee

        try{ // Create a new ERP_Role_Assignment_Group__c with requested details
            Id assignedUserId = newAG.Assigned_User__c; // Gather the ID of the Assigned User from the UI
            Id requestorUserId = newAG.Requestor__c; // Gather the ID of the Requestor from the UI
            
            // Clean up the list of Roles to be added, removing duplicates as needed.
            populateMapUserRoles(assignedUserId); // populate the mapUserRoles with all currently assigned Roles for the user [mapUserRoles = Map<ERP_Role__c Id, ERP_Role_Assignment__c>]
            Set<Id> toAssign = new Set<Id>(); // Set to contain the unique Role Ids to assign between selected and pre-req Roles
            toAssign.addAll(selectedRole); // add all roles selected in the UI
            toAssign.addAll(preRequisites.KeySet()); // add all pre-requesites found
            for(Id s:toAssign){ // Remove any Roles that have already been assigned to the user
                if(mapUserRoles.containsKey(s)){
                    toAssign.remove(s);
                }
            }

            // determine which ERP_Role_Assignment_Group__c records need to be created
            Map<Id, Id> currentAssignments = mapOpenAssignments; // Create a Map<ERP_Module__c, ERP_Role_Assignment_Group__c> for existing records
            Map<Id, Id> currentModuleMap = mapRoleToModule; // Gather a Map<ERP_Role__c.Id, ERP_Module__c.Id> for all current Roles
            List<ERP_Role_Assignment_Group__c> listNewAG = new List<ERP_Role_Assignment_Group__c>();
            // for each requested Role determine if an existing Map entry exists
                // If so add to it
                // If not then create a the new ERP_Role_Assignment_Group__c records needed.
            for(Id s:toAssign){
                Id moduleId = mapRoleToModule.get(s); // If not found we want to throw an error here, so do not wrap in a containsKey test
                if(!currentAssignments.containsKey(moduleId)){ // If we do not have a current ERP_Role_Assignment_Group__c for this ERP_Module__c
                    ERP_Role_Assignment_Group__c newAG = new ERP_Role_Assignment_Group__c(
                        ERP_Module__c = moduleId,
                        Approval_Status__c = 'Draft',
                        Requestor__c = requestorUserId,
                        Assigned_User__c = assignedUserId,
                        Submit_for_Approval__c = false // to be updated after all new Roles have been created
                    );

                    listNewAG.add(newAG);
                    currentAssignments.put(moduleId, newAG.Id); // put in map so we dont create duplicate on next Role. newAG.Id will be empty until after we insert the new records and refresh the collection.
                }
            }
            if(!listNewAG.isEmpty()){
                insert listNewAG;
            }
            currentAssignments = mapOpenAssignments; // repopulate the Map<ERP_Module__c, ERP_Role_Assignment_Group__c>
            
            // insert any new ERP_Role_Assignment__c records
            List<ERP_Role_Assignment__c> theList = new List<ERP_Role_Assignment__c>();
            for(Id s:toAssign){ // Loop through each selected Role, if the Role is not yet currently assigned create a new ERP_Role_Assignment__c record
                if(!mapUserRoles.containsKey(s)){
                    Id moduleId = mapRoleToModule.get(s); // If not found we want to throw an error here, so do not wrap in a containsKey test
                    Id assnGroup = currentAssignments.get(moduleId);
                    theList.add(
                        new ERP_Role_Assignment__c(
                            ERP_User__c = assignedUserId,
                            ERP_Role__c = s,
                            ERP_Role_Assignment_Group__c = assnGroup
                        )
                    );
                    sessionAssignmentGroupIds.add(assnGroup); // add all Assignment Group IDs to a set so that we can update them later if necessary
                }
            }
            if(!theList.isEmpty()){
                insert theList; // insert the new ERP_Role_Assignment__c records
            }
            // finally update any ERP_Role_Assignment_Group__c records to submit for approval if needed.
            if(complete){ // interestingly this will only submit the current AG, not everything from this session, we need to instead do that
                Set<ERP_Role_Assignment_Group__c> setNewAG = new Set<ERP_Role_Assignment_Group__c>();
                for(Id agId:sessionAssignmentGroupIds){
                    ERP_Role_Assignment_Group__c ag = new ERP_Role_Assignment_Group__c(
                        Id = agId,
                        Submit_for_Approval__c = complete
                    );
                    setNewAG.add(ag);
                }
                update new List<ERP_Role_Assignment_Group__c>(setNewAG);
            }
        } catch (Exception e){
            util.addError(e);
            util.hasErrors = true;
            return;
        }
    }

/* Variables */
    private Id thisERPUserId{ // Always returns the ERP_User__c record that corresponds to the current user
        get{ if(thisERPUserId==NULL){
                thisERPUserId = util.erpUserByEmail(UserInfo.getUserEmail()).Id;
            }
            return thisERPUserId;
        } set;
    }

    public ERP_User__c requestorUser{ // Always returns the ERP_User__c corresponding to the selected Requestor
        get{ 
            return util.erpUserById(newAG.Requestor__c);
        } set;
    }

    public ERP_User__c assigneeUser{ // Always returns the ERP_User__c corresponding to the selected Assignee
        get{ 
            return util.erpUserById(newAG.Assigned_User__c);
        } set;
    }

    public Map<Id, Id> mapOpenAssignments{
        get{ 
            Map<Id, Id> newMap = new Map<Id, Id>();
            for(ERP_Role_Assignment_Group__c ag:[
                SELECT Id, ERP_Module__c
                FROM ERP_Role_Assignment_Group__c 
                WHERE Requestor__c = :requestorUser.Id
                AND Assigned_User__c = :assigneeUser.Id
                AND Approval_Status__c = 'Draft'
                ORDER BY CreatedDate
            ]){
                newMap.put(ag.ERP_Module__c, ag.Id);
            }
            return newMap;
        }set;
    }

    public Map<Id, Id> mapRoleToModule{
        get{
            Map<Id, Id> newMap = new Map<Id, Id>();
            for(ERP_Role__c r:util.listERPRoles()){
                newMap.put(r.Id, r.ERP_Module__c);
            }
            return newMap;
        }set;
    }

    public Cloud_Application_Onboarding_Util util = new Cloud_Application_Onboarding_Util();
    public Map<Id,ERP_Role__c> mapIdRole{get;set;} // Map of all ERP_Role__c records
    public Map<Id,ERP_Module__c> mapIdModule{get;set;} // Map of all ERP_Module__c records
    public ERP_Role_Assignment_Group__c newAG{get;set;} // ERP_Role_Assignment_Group__c record to be upserted during final steps
    public boolean showModules{get;set;} // Flag for conditionally displaying form elements
    public Boolean showPreRequisites{get;set;} // Flag for conditionally displaying form elements
    public Map<Id, ERP_Role_Assignment__c> mapUserRoles{get;set;} // Map showing all Roles currently assigned to the user
    public String selectedModule{get;set;} // Module Id selected in the user form, returned from the page as a String
    public List<Id> selectedRole{get;set;} // Role Ids selected in the user form, returned from the page as a List<Id>
    public List<SelectOption> moduleOptions{get;set;} // List of all 'Active' Modules
    public List<SelectOption> roleOptions{get;set;} // List of all Roles associated to the selectedModule, disabled where an Approved or Draft assignment exists for the user
    public Map<Id,String> preRequisites{get;set;} // Map used to display all pre-requesite Roles for the Roles in selectedRole
    public Boolean hasPreRequisites{ get { return preRequisites.isEmpty()==FALSE; } } // Flag used to determine if there are any pre-requesites for display

    public pageReference roleRequestReport{
        get{
            roleRequestReport = util.reportURLByDevName('ERP_Role_Requests_For_User');
            String to15chars = String.valueof(requestorUser.Id).Left(15);
            roleRequestReport.getParameters().put('pv0', to15chars);
            return roleRequestReport;
        }set;
    }

    public pageReference userRoleReport{
        get{
            userRoleReport = util.reportURLByDevName('ERP_Role_Assignments_for_User');
            String to15chars = String.valueof(assigneeUser.Id).Left(15);
            userRoleReport.getParameters().put('pv0', to15chars);
            return userRoleReport;
        }set;
    }

/* Helper and functional methods */
    // get all Modules
    public void gatherModules(){
        try{
            mapIdModule = new Map<Id,ERP_Module__c>();
            moduleOptions = new List<SelectOption>();
            moduleOptions.add(new SelectOption('','-- Select Module --'));
            for (ERP_Module__c m:util.cloudListERPModules()){
                moduleOptions.add(new SelectOption(m.Id,m.Name));
                mapIdModule.put(m.Id, m);
            }
        } catch (Exception e){
            util.addError(e);
            util.hasErrors = true;
            return;
        }
    }

    // populate both mapIdRole and roleOptions
    public void gatherRoles(){
        try{
            mapIdRole = new Map<Id,ERP_Role__c>();
            roleOptions = new List<SelectOption>();
            roleOptions.add(new SelectOption('','-- Select Role(s) --', true));
            for (ERP_Role__c r:util.cloudListERPRoles()){
                mapIdRole.put(r.Id, r); // All Roles pushed into the mapIdRole collection

                if( selectedModule != '' && r.ERP_Module__c == (Id)selectedModule && r.isactive__c == TRUE){ // get only those Roles related to the selected Module for the roleOptions SelectOptions list
                    String theId = r.Id;
                    String theName = r.ROLE_NAME__c == null ? r.Name : r.ROLE_NAME__c;

                    if(mapUserRoles.containsKey(theId)){
                        roleOptions.add(new SelectOption(theId, '(already assigned) ' + theName, true));
                    } else {
                        roleOptions.add(new SelectOption(theId, theName, false));
                    }
                }
            } // end for loop
        } catch (Exception e){
            util.addError(e);
            util.hasErrors = true;
            return;
        }
    }

    // Gathers currently assigned or pending Roles for the selected User
    public void populateMapUserRoles(Id assignedUserId){
        try{
            mapUserRoles = new Map<Id, ERP_Role_Assignment__c>();
            for(ERP_Role_Assignment__c ra: util.listAssignmentsById(assignedUserId)){
                mapUserRoles.put(ra.ERP_Role__c,ra);
            }
        } catch (Exception e){
            util.addError(e);
            util.hasErrors = true;
            return;
        }
    }

    // get Pre-Requisites associated to the selected Roles
    public void populatePreRequisites(List<Id> listRoleIds){
        try{
            preRequisites = new Map<Id,String>();
            for (ERP_Role_Pre_Requisites__c r: util.listPreReqById(listRoleIds)){
                String roleName = r.Parent_Role__r.ROLE_NAME__c == null ? r.Parent_Role__r.Name : r.Parent_Role__r.ROLE_NAME__c;
                String reqId  = r.Required_Role__c;
                if(preRequisites.containsKey(reqId)){
                    preRequisites.put(reqId, preRequisites.get(reqId) + ', ' + roleName); // Nth entry in the list
                } else {
                    preRequisites.put(reqId, roleName); // First requirement entry
                }
            }
        } catch (Exception e){
            util.addError(e);
            util.hasErrors = true;
            return;
        }
    }
}