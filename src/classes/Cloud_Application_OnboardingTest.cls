@isTest
public class Cloud_Application_OnboardingTest {
	public Cloud_Application_OnboardingTest(){}

/* utility to load testing data */
	private void loadTestData(){
		Test.loadData(User.sObjectType,'test_User');
		Test.loadData(ERP_User__c.sObjectType,'test_ERPUser');
		Test.loadData(ERP_Module__c.sObjectType, 'test_ERPModule');
		Test.loadData(ERP_Role__c.sObjectType, 'test_ERPRole');
		User testUser = [Select Id from User where username = 'tuser_01@testing.com'];
		System.runAs(testUser){
			Test.loadData(ERP_Role_Assignment_Staging__c.sObjectType, 'test_ERPRoleAssignmentStaging');
		}
	}


/* methods to test various Triggers */
	static testMethod void test_Role_Module_StagingInsert(){ // test insertion of ERP_Role_and_Module_Staging__c records
		Cloud_Application_OnboardingTest cloud = new Cloud_Application_OnboardingTest();
		cloud.loadTestData();
		Test.startTest();
		Test.loadData(ERP_Role_and_Module_Staging__c.sObjectType,'test_ModuleStaging1');
		Test.stopTest();
		
		Integer roleVerify = [Select Count() from ERP_Role__c]; // query ERP_Role__c records, 
		system.assertEquals(80,roleVerify); // count should = the number of staging records inserted
		
		Integer moduleVerify = [Select Count() from ERP_Module__c]; // query ERP_Module__c records, 
		system.assertEquals(30,moduleVerify); // ensure that there were the correct number
	}

	static testMethod void test_Role_Module_StagingUpsert(){ // test subsequent upsert of additional ERP_Role_and_Module_Staging__c records.
		// initial insert
		Cloud_Application_OnboardingTest cloud = new Cloud_Application_OnboardingTest();
		cloud.loadTestData();
		Test.loadData(ERP_Role_and_Module_Staging__c.sObjectType,'test_ModuleStaging1');

		// insert test records, includes updating records as well as creating new ones
		Test.startTest();
		Test.loadData(ERP_Role_and_Module_Staging__c.sObjectType,'test_ModuleStaging2');
		Test.stopTest();

		Integer roleVerify = [Select Count() from ERP_Role__c]; // query ERP_Role__c records, 
		system.assertEquals(95,roleVerify); // count should = the number of staging records inserted
		
		Integer moduleVerify = [Select Count() from ERP_Module__c]; // query ERP_Module__c records, 
		system.assertEquals(35,moduleVerify); // ensure that there were the correct number
	}

	static testmethod void test_ERPRoleAssignmentStaging(){
		// prepare test data, Users, ERP_User__c, ERP_Module__c, and ERP_Role__c
		Cloud_Application_OnboardingTest con = new Cloud_Application_OnboardingTest();
		con.loadTestData();

		// insert Role Assignment Staging records
		User testUser = [Select Id from User where username = 'tuser_01@testing.com'];
		Test.startTest();
		System.runAs(testUser){
			Test.loadData(ERP_Role_Assignment_Staging__c.sObjectType, 'test_ERPRoleAssignmentStaging');
		}
		Test.stopTest();

		// Validate that Role Assignments were created
		Integer groupVerify = [Select Count() from ERP_Role_Assignment_Group__c]; // ERP Role Assignment Groups
		system.assertEquals(4,groupVerify); // count should = the number of staging records inserted

		Integer assignmentVerify = [Select Count() from ERP_Role_Assignment__c]; // ERP Role Assignment
		system.assertEquals(40,assignmentVerify); // count should = the number of staging records inserted

		Integer reviewVerify = [Select Count() from ERP_Role_Review__c]; // ERP Role Review
		system.assertEquals(40,assignmentVerify); // count should = the number of staging records inserted

	}

	static testmethod void test_ERPRoleAssignmentStagingUpsert(){
		// prepare test data, Users, ERP_User__c, ERP_Module__c, and ERP_Role__c
		Cloud_Application_OnboardingTest con = new Cloud_Application_OnboardingTest();
		con.loadTestData();
		
		User testUser = [Select Id from User where username = 'tuser_01@testing.com'];
		System.runAs(testUser){
			Test.startTest();
			// now insert the update records
			Test.loadData(ERP_Role_Assignment_Staging__c.sObjectType, 'test_RoleAssignmentStaging2');
			Test.stopTest();
		}
		
		// Validate that Role Assignments were created
		Integer groupVerify = [Select Count() from ERP_Role_Assignment_Group__c]; // ERP Role Assignment Groups
		system.assertEquals(4,groupVerify); // count should = the number of staging records inserted

		Integer assignmentVerify = [Select Count() from ERP_Role_Assignment__c]; // ERP Role Assignment
		system.assertEquals(60,assignmentVerify); // count should = the number of staging records inserted

		Integer reviewVerify = [Select Count() from ERP_Role_Review__c]; // ERP Role Review
		system.assertEquals(60,assignmentVerify); // count should = the number of staging records inserted

	}

	static testmethod void test_ERPRoleAssignmentStagingDelete(){
		// prepare test data, Users, ERP_User__c, ERP_Module__c, and ERP_Role__c
		Cloud_Application_OnboardingTest con = new Cloud_Application_OnboardingTest();
		con.loadTestData();
		User testUser = [Select Id from User where username = 'tuser_01@testing.com'];
		System.runAs(testUser){
			// Confirm that we have the expected 4 records prior to marking for deletion
			List<ERP_Role_Assignment_Group__c> groupVerify = [Select Id, toDelete__c from ERP_Role_Assignment_Group__c]; // ERP Role Assignment Groups
			system.assertEquals(4,groupVerify.size()); // count should = the number of staging records inserted

			Test.startTest();
			for(ERP_Role_Assignment_Group__c ag:groupVerify){
				ag.toDelete__c = true;
			}
			update groupVerify;
			Test.stopTest();
		}
		List<ERP_Role_Assignment_Group__c> groupVerify = [Select Id, toDelete__c from ERP_Role_Assignment_Group__c]; // ERP Role Assignment Groups
		system.assertEquals(0,groupVerify.size()); // count should = the number of staging records inserted
	}

/* Compilation of the methods and vars that need to be tested per each class */

	static testMethod void testCloud_Application_OnboardingPage(){
		// prepare test data, Users, ERP_User__c, ERP_Module__c, and ERP_Role__c
		Cloud_Application_OnboardingTest testCon = new Cloud_Application_OnboardingTest();
		testCon.loadTestData();
		User testUser = [Select Id from User where username = 'tuser_03@testing.com'];
		Test.startTest();
		System.runAs(testUser){
			PageReference pageRef = Page.Cloud_Application_Onboarding;
	        Test.setCurrentPage(pageRef);
	        Cloud_Application_OnboardingController pageCon = new Cloud_Application_OnboardingController();

	        pageCon.newAG.Assigned_User__c = null;

	        // confirm vars set on load
	        testCon.confirmOnboardingControllerDefault(pageCon);

	        // select related to  and  assigned user
	        ERP_User__c testUser1 = [Select Id from ERP_User__c where User_Email_Address__c = 'tuser_01@testing.com'];
			ERP_User__c testUser2 = [Select Id from ERP_User__c where User_Email_Address__c = 'tuser_02@testing.com'];
	        // select related to  and  assigned user
	        pageCon.newAG.Requestor__c = testUser1.Id;
	        pageCon.newAG.Assigned_User__c = testUser2.Id;
	        pageCon.selectUser();
	        system.assertEquals(true,pageCon.showModules);
            system.assertEquals(false,pageCon.showPreRequisites);

            system.assertNotEquals(null,pageCon.roleRequestReport);
	        system.assertNotEquals(null,pageCon.userRoleReport);

            // populateMapUserRoles(newAG.Assigned_User__c);
            system.assertNotEquals(null,pageCon.mapUserRoles);

            // gatherModules();
            system.assertNotEquals(null,pageCon.moduleOptions);
            system.assertNotEquals(null,pageCon.mapIdModule);

            // gatherRoles();
            system.assertNotEquals(null,pageCon.mapIdRole);
            system.assertNotEquals(null,pageCon.roleOptions);

            // click backToStep1()
	        pageCon.backToStep1();
	        testCon.confirmOnboardingControllerDefault(pageCon);

	        // repopulate selected users
	        pageCon.newAG.Requestor__c = testUser1.Id;
	        pageCon.newAG.Assigned_User__c = testUser2.Id;
	        pageCon.selectUser();

	    	// click selectRoles()
	    	pageCon = testCon.test_selectRoles(pageCon);
	    	system.assertEquals(true,pageCon.showPreRequisites);

	        // click backToStep2()
	        pageCon.backToStep2();
	        system.assertEquals(false,pageCon.showPreRequisites);

	        // click selectRoles() again
	    	pageCon = testCon.test_selectRoles(pageCon);

	    	pageCon.gatherRoles();
	    	pageCon.saveAndAddModule();

	    	// lastly everything should be set back to default
	    	// testCon.confirmOnboardingControllerDefault(pageCon);

        }
        Test.stopTest();
	}

	static testMethod void testCloud_Application_OnboardingPageSave(){
		// prepare test data, Users, ERP_User__c, ERP_Module__c, and ERP_Role__c
		Cloud_Application_OnboardingTest testCon = new Cloud_Application_OnboardingTest();
		testCon.loadTestData();
		User testUser = [Select Id from User where username = 'tuser_03@testing.com'];
		Test.startTest();
		System.runAs(testUser){
			PageReference pageRef = Page.Cloud_Application_Onboarding;
	        Test.setCurrentPage(pageRef);
	        Cloud_Application_OnboardingController pageCon = new Cloud_Application_OnboardingController();
	        // select related to  and  assigned user
	        ERP_User__c testUser1 = [Select Id from ERP_User__c where User_Email_Address__c = 'tuser_01@testing.com'];
			ERP_User__c testUser2 = [Select Id from ERP_User__c where User_Email_Address__c = 'tuser_02@testing.com'];
	        // select related to  and  assigned user
	        pageCon.newAG.Requestor__c = testUser1.Id;
	        pageCon.newAG.Assigned_User__c = testUser2.Id;
	        pageCon.selectUser();
	    	pageCon = testCon.test_selectRoles(pageCon);
	    	pageCon.saveRoleRequests();
        }
        Test.stopTest();
	}

	private Cloud_Application_OnboardingController test_selectUser(Cloud_Application_OnboardingController pageCon){
		User testUser1 = [Select Id from User where username = 'tuser_01@testing.com'];
		User testUser2 = [Select Id from User where username = 'tuser_02@testing.com'];
        // select related to  and  assigned user
        pageCon.newAG.Requestor__c = testUser1.Id;
        pageCon.newAG.Assigned_User__c = testUser2.Id;
        pageCon.selectUser();
		return pageCon;
	}
	private Cloud_Application_OnboardingController test_selectRoles(Cloud_Application_OnboardingController pageCon){
		for(ERP_Role__c r:[Select Id from ERP_Role__c order by Name desc limit 10 ]){
			pageCon.selectedRole.add(r.Id);
		}

		for(ERP_Module__c m:[Select Id from ERP_Module__c limit 1]){
			pageCon.selectedModule = (String) m.Id;
		}

        pageCon.selectRoles();
		return pageCon;
	}

	private void confirmOnboardingControllerDefault(Cloud_Application_OnboardingController con){
			// Populate initial values for any working vars on the page
            system.assertEquals(false,con.showModules);
            system.assertEquals(false,con.showPreRequisites);
            system.assertEquals('',con.selectedModule);
            system.assertEquals(new List<Id>(),con.selectedRole);
            /*
            system.assertEquals(con.newAG = new ERP_Role_Assignment_Group__c(
                Requestor__c = thisERPUserId, // default both requestor and assignee to the current user
                Assigned_User__c = thisERPUserId,
                Approval_Status__c = 'Draft'
            );
			*/
            // clear any collection and or fields that were populated
            system.assertEquals(new Map<Id,String>(),con.preRequisites);
            system.assertEquals(new List<SelectOption>(),con.roleOptions);
            system.assertEquals(new Set<Id>(),con.sessionAssignmentGroupIds);
	}
/*
Cloud_Application_OnboardingController
	Properties
		Access	Name
		public	ERP_User__c	assigneeUser
		public	Boolean hasPreRequisites
		public	Map mapIdModule
		public	Map mapIdRole
		public	Map mapOpenAssignments
		public	Map mapRoleToModule
		public	Map mapUserRoles
		public	List moduleOptions
		public	ERP_Role_Assignment_Group__c	newAG
		public	Map preRequisites
		public	ERP_User__c	requestorUser
		public	List roleOptions
		public	System.PageReference roleRequestReport
		public	String selectedModule
		public	List selectedRole
		public	Set sessionAssignmentGroupIds
		public	Boolean showModules
		public	Boolean showPreRequisites
		public	System.PageReference userRoleReport

	Constructors
		Access	Signature
		public	Cloud_Application_OnboardingController()

	Methods
		Access	Signature
		public	void backToStep1()
		public	void backToStep2()
		public	ANY clone()
		public	void gatherModules()
		public	void gatherRoles()
		public	void populateMapUserRoles(Id assignedUserId)
		public	void populatePreRequisites(List listRoleIds)
		public	void saveAndAddModule()
		public	void saveRoleRequests()
		public	void selectRoles()
		public	void selectUser()
		public	void upsertRoleAndModule(Boolean complete)
*/

	static testMethod void testCloud_Application_Onboarding_ManagementPage(){
		// prepare test data, Users, ERP_User__c, ERP_Module__c, and ERP_Role__c
		Cloud_Application_OnboardingTest testCon = new Cloud_Application_OnboardingTest();
		testCon.loadTestData();
		User testUser = [Select Id from User where username = 'tuser_01@testing.com'];
		System.runAs(testUser){
			PageReference pageRef = Page.Cloud_Application_Onboarding_Management;
	        Test.setCurrentPage(pageRef);
	        Cloud_Application_Onboarding_MgtCon pageCon = new Cloud_Application_Onboarding_MgtCon();

	        // ensure that no functions return any errors
			PageReference prMod = pageCon.urlModuleReport;
			system.assertNotEquals(null,prMod);
			PageReference prRole = pageCon.urlRoleReport;
			system.assertNotEquals(null,prRole);

		}
	}

/*
public class Cloud_Application_Onboarding_Util

	Properties
		Access	Name
		public	String currentReviewPeriod
		public	Boolean hasErrors

	Constructors
		Access	Signature
		public	Cloud_Application_Onboarding_Util()

	Methods
		Access	Signature
		public	ERP_User__c	ERPUserByEmail(String theEmail)
		public	ERP_User__c	ERPUserById(Id theId)
		public	void addError(Exception e)
		public	void addError(ApexPages.Severity sev, String message)
		public	ANY clone()
		public	List listAllAssignmentGroups()
		public	List listAllAssignments()
		public	List listAssignmentGroupsByUser(Set userIds)
		public	List listAssignmentsById(Id theId)
		public	List listAssignmentsById(List listIds)
		public	List listAssignmentsById(Set theIds)
		public	List listERPModules()
		public	List listERPRolesByGUID(Set GUIDs)
		public	List listERPRolesByGUID(String GUID)
		public	List listERPRoles()
		public	List listERPUserByEmail(Set theEmails)
		public	List listERPUserById(Set theIds)
		public	List listERPUsers()
		public	List listPreReqById(Id theId)
		public	List listPreReqById(List listIds)
		public	List listPreReqById(Set setIds)
		public	List listRoleReviewsForPeriod(String period)
		public	List listRoleReviewsForUserAndPeriod(Set userIds, String period)
		public	List listRolesAvailable()
		public	List listUsersRoleColumns()
		public	List periodOptions()
		public	System.PageReference reportURLByDevName(String devName)
*/





}