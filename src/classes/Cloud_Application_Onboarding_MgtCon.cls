public with sharing class Cloud_Application_Onboarding_MgtCon {
    
    private Cloud_Application_Onboarding_Util util = new Cloud_Application_Onboarding_Util();
    
/* Constructor */
    public Cloud_Application_Onboarding_MgtCon() {
        try{
            initializeVars();
        } catch (Exception e){
            util.addError(e);
            util.haserrors = true;
        }
    }

/* Primary methods for page functionality*/
    private void initializeVars(){ // Method to initialize all vars for page functionality
        util = new Cloud_Application_Onboarding_Util();
    }
    
    public pageReference urlRoleReport{ // URL for All_ERP_Roles report
        get{
            if(urlRoleReport==null){
                urlRoleReport = util.reportURLByDevName('All_ERP_Roles');
            }
            return urlRoleReport;
        }
        set;
    }
    
    public pageReference urlModuleReport{ // URL for All_ERP_Modules report
        get{
            if(urlModuleReport==null){
                urlModuleReport = util.reportURLByDevName('All_ERP_Modules');
            }
            return urlModuleReport;
        }
        set;
    }
}