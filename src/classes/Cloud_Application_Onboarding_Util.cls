public with sharing class Cloud_Application_Onboarding_Util {
    public Cloud_Application_Onboarding_Util() {
        
    }
    
    // get ERP_User__c by Id
/*
    public List<ERP_User__c> listERPUserById(Set<Id> theIds){
        List<ERP_User__c> theList = new List<ERP_User__c>();
        for(ERP_User__c u:[
            SELECT Id, Name, Contact__c, User__c, User_Email_Address__c 
            FROM ERP_User__c
            WHERE Id IN :theIds
        ]){
            theList.add(u);
        }
        return theList;
    }
*/
    public ERP_User__c ERPUserById(Id theId){
        ERP_User__c theUser = new ERP_User__c();
        for(ERP_User__c u:[
            SELECT Id, Name, Contact__c, User__c, User_Email_Address__c 
            FROM ERP_User__c
            WHERE Id = :theId
            LIMIT 1
        ]){
            theUser = u;
        }
        return theUser;
    }
    // get ERP_User__c by email address
    public List<ERP_User__c> listERPUserByEmail(Set<String> theEmails){
        List<ERP_User__c> theList = new List<ERP_User__c>();
        for(ERP_User__c u:[
            SELECT Id, Name, Contact__c, User__c, User_Email_Address__c 
            FROM ERP_User__c
            WHERE User_Email_Address__c IN :theEmails
        ]){
            theList.add(u);
        }
        return theList;
    }
    public ERP_User__c ERPUserByEmail(String theEmail){
        ERP_User__c theUser = new ERP_User__c();
        for(ERP_User__c u:[
            SELECT Id 
            FROM ERP_User__c 
            Where User_Email_Address__c = :UserInfo.getUserEmail() 
            LIMIT 1
        ]){
            theUser = u;
        }
        return theUser;
    }
    /* Methods to add errors to the page */
    public boolean hasErrors{get;set;}
    public void addError(ApexPages.Severity sev, String message){
        ApexPages.addMessage(
            new ApexPages.Message(sev, message)
        );
    }
    public void addError(Exception e){
        ApexPages.addMessages(e);
        ApexPages.addMessage(
            new ApexPages.Message( ApexPages.Severity.FATAL, e.getMessage() )
        );
        ApexPages.addMessage(
            new ApexPages.Message( ApexPages.Severity.FATAL, e.getStackTraceString() )
        );
        hasErrors=true;
    }
    // Return a List of pre-requesite Roles for the supplied ERP_Role__c Ids, overloaded to accept either a single Id or a List<Id>
/*
    public List<ERP_Role_Pre_Requisites__c> listPreReqById(Id theId){
        return listPreReqById(new List<Id>{theId});
    }
    public List<ERP_Role_Pre_Requisites__c> listPreReqById(Set<Id> setIds){
        List<Id> theList = new List<Id>();
        theList.addAll(setIds);
        return listPreReqById(theList);
    }
*/
    public List<ERP_Role_Pre_Requisites__c> listPreReqById(List<Id> listIds){
        List<ERP_Role_Pre_Requisites__c> thelist = new List<ERP_Role_Pre_Requisites__c>();
        for (ERP_Role_Pre_Requisites__c r:[
            SELECT Id, Parent_Role__r.ROLE_NAME__c, Required_Role__r.ROLE_NAME__c, Parent_Role__r.Name, Required_Role__r.Name
            FROM ERP_Role_Pre_Requisites__c
            WHERE Parent_Role__c IN :listIds AND Parent_Role__r.isActive__c =: True
        ] ){
            thelist.add(r);
        }
        return thelist;
    }
    // Return a List of current ERP_Role_Assignment__c records for the supplies ERP_User__c Ids, overloaded to accept either a single Id or a List<Id>
    public List<ERP_Role_Assignment__c> listAssignmentsById(Id theId){
        return listAssignmentsById(new List<Id>{theId});
    }
    public List<ERP_Role_Assignment__c> listAssignmentsById(Set<Id> theIds){
        List<Id> theList = new List<Id>();
        theList.addAll(theIds);
        return listAssignmentsById(theList);
    }
    public List<ERP_Role_Assignment__c> listAssignmentsById(List<Id> listIds){
        List<ERP_Role_Assignment__c> thelist = new List<ERP_Role_Assignment__c>();
        for (ERP_Role_Assignment__c ra:[
            SELECT Id, ERP_Role_Assignment_Group__c, ERP_Role_Assignment_Group__r.Approval_Status__c, ERP_Role__c, ERP_User__c
            FROM ERP_Role_Assignment__c
            WHERE ERP_User__c IN :listIds
            AND ERP_Role_Assignment_Group__r.Approval_Status__c != 'Rejected' AND ERP_Role__r.isActive__c =: True
        ] ){
            thelist.add(ra);
        }
        return thelist;
    }
/*
    public List<ERP_Role_Assignment__c> listAllAssignments(){
        List<ERP_Role_Assignment__c> thelist = new List<ERP_Role_Assignment__c>();
        for (ERP_Role_Assignment__c ra:[
            SELECT Id, ERP_Role_Assignment_Group__c, ERP_Role_Assignment_Group__r.Approval_Status__c, ERP_Role__c, ERP_User__c
            FROM ERP_Role_Assignment__c
            WHERE ERP_Role_Assignment_Group__r.Approval_Status__c != 'Rejected'
        ] ){
            thelist.add(ra);
        }
        return thelist;
    }
*/
    public List<ERP_Role_Assignment_Group__c> listAssignmentGroupsByUser(Set<Id> userIds){
        List<ERP_Role_Assignment_Group__c> thelist = new List<ERP_Role_Assignment_Group__c>();
        for (ERP_Role_Assignment_Group__c ag:[
            SELECT Id, Name, Approval_Status__c, Imported_from_Oracle__c, Assigned_User__c, Requestor__c
            FROM ERP_Role_Assignment_Group__c
            WHERE Approval_Status__c != 'Rejected'
            AND Assigned_User__c IN:userIds
        ] ){
            thelist.add(ag);
        }
        return thelist;
    }
/*
    public List<ERP_Role_Assignment_Group__c> listAllAssignmentGroups(){
        List<ERP_Role_Assignment_Group__c> thelist = new List<ERP_Role_Assignment_Group__c>();
        for (ERP_Role_Assignment_Group__c ag:[
            SELECT Id, Name, Approval_Status__c, Imported_from_Oracle__c, Assigned_User__c, Requestor__c
            FROM ERP_Role_Assignment_Group__c
            WHERE Approval_Status__c != 'Rejected'
        ] ){
            thelist.add(ag);
        }
        return thelist;
    }
*/
    public pageReference reportURLByDevName(String devName){
        pageReference pr = new pageReference('/');
        for(Report r:[
            SELECT Id 
            FROM Report 
            WHERE DeveloperName = :devName
        ]){
            pr = new ApexPages.StandardController(r).view();
        }
        return pr;
    }
    

    /* Module and Role Management utilities */
/*
    public List<String> listRolesAvailable(){
        List<String> theList = new list<String>{
            'ROLE_ID','BUSINESS_GROUP_ID','CREATED_BY','CREATION_DATE','LAST_UPDATED_BY',               // Elements 00-04
                'ROLE_GUID','LAST_UPDATE_DATE','ABSTRACT_ROLE','JOB_ROLE','DATA_ROLE',                  // Elements 05-09
                'ACTIVE_FLAG','ROLE_COMMON_NAME','ROLE_NAME','DESCRIPTION','ROLE_DISTINGUISHED_NAME',   // Elements 10-14
                'APPLICATION'                                                                           // Elements 15
                };
                    // TODO:
                    // Convert the above to query a Custom Setting instead, also creating a Map of source > destination fields
                    return theList;
    }
    public List<String> listUsersRoleColumns(){
        List<String> theList = new list<String>{
            'USERNAME','ROLE_NAME','ROLE_COMMON_NAME','ACTIVE_FLAG','FULL_NAME',                        // Elements 00-04
                'EMAIL_ADDRESS','SUPERVISOR','LAST_UPDATE_DATE','LAST_UPDATED_BY','CREATION_DATE',      // Elements 05-09
                'CREATED_BY','USER_ID','ROLE_ID','ROLE_GUID','PERSON_ID',                               // Elements 10-14
                'SUPERVISOR_ID','ACCESS_OTHERS_LEVEL_CODE'                                              // Elements 15-16
                };
                    // TODO:
                    // Convert the above to query a Custom Setting instead, also creating a Map of source > destination fields
                    return theList;
    }
*/    
    public List<ERP_Module__c> listERPModules(){
        List<ERP_Module__c> theList = new List<ERP_Module__c>();
        for(ERP_Module__c m:[
            SELECT Id, OwnerId,isActive__c, IsDeleted, Name, CurrencyIsoCode, Business_Owner_Email__c, Business_Owner__c,
            Business_Owner__r.Name
            FROM ERP_Module__c 
            ORDER BY Name
        ]){
            theList.add(m);
        }
        return theList;
    }
     public List<ERP_Module__c> cloudListERPModules(){
        List<ERP_Module__c> theList = new List<ERP_Module__c>();
        for(ERP_Module__c m:[
            SELECT Id, OwnerId,isActive__c, IsDeleted, Name, CurrencyIsoCode, Business_Owner_Email__c, Business_Owner__c,
            Business_Owner__r.Name
            FROM ERP_Module__c where isActive__c =: True
            ORDER BY Name
        ]){
            theList.add(m);
        }
        return theList;
    }

/*
    public List<ERP_Role__c> listERPRolesByGUID(String GUID){
        Set<String> GUIDs = new Set<String>();
        GUIDs.add(GUID);
        return listERPRolesByGUID(GUIDs);
    }
*/
    public List<ERP_Role__c> listERPRolesByGUID(Set<String> GUIDs){
        List<ERP_Role__c> theList = new List<ERP_Role__c>();
        for(ERP_Role__c r:[
            SELECT Id, OwnerId, Name, ROLE_ID__c, ROLE_NAME__c, ABSTRACT_ROLE__c, JOB_ROLE__c, DATA_ROLE__c, isActive__c, 
            ACTIVE_FLAG__c, ROLE_COMMON_NAME__c, DESCRIPTION__c, ROLE_DISTINGUISHED_NAME__c, ERP_Module__c, ERP_Module__r.Name
            FROM ERP_Role__c
            WHERE Name In:GUIDs
            ORDER BY ROLE_NAME__c
        ]){
            theList.add(r);
        }
        return theList;
    }
    public List<ERP_Role__c> listERPRoles(){
        List<ERP_Role__c> theList = new List<ERP_Role__c>();
        for(ERP_Role__c r:[
            SELECT Id, OwnerId, Name, ROLE_ID__c, ROLE_NAME__c, ABSTRACT_ROLE__c, JOB_ROLE__c, DATA_ROLE__c, isActive__c, 
            ACTIVE_FLAG__c, ROLE_COMMON_NAME__c, DESCRIPTION__c, ROLE_DISTINGUISHED_NAME__c, ERP_Module__c, ERP_Module__r.Name
            FROM ERP_Role__c 
            ORDER BY ROLE_NAME__c
        ]){
            theList.add(r);
        }
        return theList;
    }
    public List<ERP_Role__c> cloudListERPRoles(){
        List<ERP_Role__c> theList = new List<ERP_Role__c>();
        for(ERP_Role__c r:[
            SELECT Id, OwnerId, Name, ROLE_ID__c, ROLE_NAME__c, ABSTRACT_ROLE__c, JOB_ROLE__c, DATA_ROLE__c, isActive__c, 
            ACTIVE_FLAG__c, ROLE_COMMON_NAME__c, DESCRIPTION__c, ROLE_DISTINGUISHED_NAME__c, ERP_Module__c, ERP_Module__r.Name
            FROM ERP_Role__c where isActive__c =: True 
            ORDER BY ROLE_NAME__c
        ]){
            theList.add(r);

        }
        return theList;
    }

/*
    public List<ERP_User__c> listERPUsers(){
        List<ERP_User__c> theList = new List<ERP_User__c>();
        for(ERP_User__c u:[
            SELECT Id, OwnerId, Name, User__c, Contact__c, User_Email_Address__c 
            FROM ERP_User__c
            ORDER BY Name
        ]){
            theList.add(u);
        }
        return theList;
    }
    public List<ERP_Role_Review__c> listRoleReviewsForPeriod(String period){
        List<ERP_Role_Review__c> theList = new List<ERP_Role_Review__c>();
        for(ERP_Role_Review__c r:[
            SELECT Id, ERP_Role_Assignment__c, Review_Period__c, Role_Approved__c, Latest_Timestamp__c,
            ERP_Role_Assignment__r.ERP_Role__c, ERP_Role_Assignment__r.ERP_User__c
            FROM ERP_Role_Review__c
            WHERE Review_Period__c = :period
        ]){
            theList.add(r);
        }
        return theList;
    }
*/
    public List<ERP_Role_Review__c> listRoleReviewsForUserAndPeriod(Set<Id> userIds, String period){
        List<ERP_Role_Review__c> theList = new List<ERP_Role_Review__c>();
        for(ERP_Role_Review__c r:[
            SELECT Id, ERP_Role_Assignment__c, Review_Period__c, Role_Approved__c, Latest_Timestamp__c,
            ERP_Role_Assignment__r.ERP_Role__c, ERP_Role_Assignment__r.ERP_User__c
            FROM ERP_Role_Review__c
            WHERE Review_Period__c = :period
            AND ERP_Role_Assignment__r.ERP_User__c IN:userIds
        ]){
            theList.add(r);
        }
        return theList;
    }
    
    public String currentReviewPeriod{
        get{
            if(currentReviewPeriod == NULL || currentReviewPeriod ==''){
                currentReviewPeriod = '';
                Date today = Date.Today();
                for(Period p:[
                    SELECT Id, StartDate, EndDate, Number
                    FROM Period 
                    WHERE type = 'quarter'
                    AND StartDate <= TODAY
                    AND EndDate >= TODAY
                    LIMIT 1
                ]){
                    currentReviewPeriod = 'Q'+p.Number+'-'+p.EndDate.Year();
                }
            }
            return currentReviewPeriod;
        }
        set;
    }
/*
    public List<SelectOption> periodOptions(){
        List<SelectOption> theList = new List<SelectOption>();
        Date today = Date.Today();
        theList.add(new SelectOption('','-- Select Period --', true));
        for(Period p:[
            SELECT Id, StartDate, EndDate, Number
            FROM Period 
            WHERE type = 'quarter'
            AND StartDate > LAST_N_QUARTERS:1
            AND EndDate <= NEXT_N_QUARTERS:4
            ORDER BY StartDate
        ]){
            String s = 'Q'+p.Number+'-'+p.EndDate.Year();
            theList.add(new SelectOption(s,s));
        }
        return theList;
    }
*/
}