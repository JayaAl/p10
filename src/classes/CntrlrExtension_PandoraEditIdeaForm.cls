/***************************************************
   Name: CntrlrExtension_PandoraEditIdeaForm 

   Usage: This class displays an Idea on PandoraEditIdeaForm VF page.

   Author – Stratitude, Inc.

   Date – 07/08/2012

  Modified By - Charudatta Mandhare
  
******************************************************/

public with sharing class CntrlrExtension_PandoraEditIdeaForm {
      
      Public Idea i {get; set;}
      public String s = 'Software Issues';
      public List<selectOption> CategoryValues{get; set;}
      
      public List<String>IdeaCategories {get; set;}
    public List<Idea>IdeasAllList{get;set;}
   public List<Idea>IdeasCategory1;
   public List<Idea>IdeasCategory2;
   public List<Idea>IdeasCategory3;
   public List<Idea>IdeasCategory4;
   public List<Idea>IdeasCategory5;
    public CntrlrExtension_PandoraEditIdeaForm(ApexPages.IdeaStandardController controller) {
    
    this.i = [SELECT Id, Title, Body, VoteTotal, CreatedById, createdby.communityNickname,Categories, (Select Id, IsDeleted, ParentId, Type, CreatedDate, CreatedById, SystemModstamp From Votes) FROM Idea 
                   WHERE Id = :ApexPages.currentPage().getParameters().get('id')];
     IdeasAllList =[SELECT Id, Title, Body, VoteTotal, CreatedById, createdby.communityNickname,categories, NumComments from Idea Order By VoteTotal Desc Limit 1000];
     List<selectOption> options = new List<selectOption>(); //new list for holding all of the picklist options
     IdeaCategories= new List<String>();
     
      Schema.DescribeFieldResult F = Idea.Categories.getDescribe();
      List<Schema.PicklistEntry> pick_list_values = F.getPicklistValues();
      for (Schema.PicklistEntry a : pick_list_values) { //for all values in the picklist list
         this.IdeaCategories.add(a.getValue());
         options.add(new selectOption(a.getLabel(), a.getValue())); //add the value and label to our final list
      }
      //return options; //return the List
    this.CategoryValues=options;
    }
    
    
    public pageReference Save(){
    
    
        i.title= this.i.title;
        i.body=this.i.Body;
        //i.CommunityId='09a50000000LLmW';
        i.categories=this.i.categories;
        try{
        database.update (i);
        }
        
        catch (Exception e){
        
        System.debug('Exception'+e);
        
        }
         
        PageReference secondPage = new pagereference('/apex/PandoraViewIdea');
            secondPage .setRedirect(true);
            secondPage .getParameters().put('id', i.id); 
            
            return secondPage ;
    }
    
    public pageReference Cancel(){
    
     PageReference secondPage = new pagereference('/apex/PandoraViewIdea');
            secondPage .setRedirect(true);
            secondPage .getParameters().put('id', i.id); 
            
            return secondPage ;
    
    
    return null;
    }
    public String getString() {
        return s;
    }
                        
    public void setString(String s) {
        this.s = s;
    }
 public Idea[] getIdeasCategory1(){
    // Idea [] TotalIdeas= ideaSetController.getIdeaList();
    IdeasCategory1=new List<idea> ();
     for (Idea i: IdeasAlllist){
   System.debug('i.categories'+i.categories+'this.IdeaCategories[0]' +this.IdeaCategories[0]);
      if(i.categories==this.IdeaCategories[0]){
          IdeasCategory1.add(i);
     }
   }
          //IdeasCategory1.sort();
   return IdeasCategory1;
  }
   public Idea[] getIdeasCategory2(){
     //Idea [] TotalIdeas= ideaSetController.getIdeaList();
    IdeasCategory2=new List<idea> ();
     for (Idea i: IdeasAllList){
   System.debug('i.categories'+i.categories+'this.IdeaCategories[0]' +this.IdeaCategories[1]);
      if(i.categories==this.IdeaCategories[1]){
          IdeasCategory2.add(i);
     }
   }
   //IdeasCategory2.sort();
   return IdeasCategory2;
  }
   public Idea[] getIdeasCategory3(){
     //Idea [] TotalIdeas= ideaSetController.getIdeaList();
    IdeasCategory3=new List<idea> ();
     for (Idea i: IdeasAllList){
   System.debug('i.categories'+i.categories+'this.IdeaCategories[0]' +this.IdeaCategories[2]);
      if(i.categories==this.IdeaCategories[2]){
          IdeasCategory3.add(i);
     }
   }
    //IdeasCategory3.sort();
   return IdeasCategory3;
  }
   public Idea[] getIdeasCategory4(){
    // Idea [] TotalIdeas= ideaSetController.getIdeaList();
    IdeasCategory4=new List<idea> ();
     for (Idea i: IdeasAllList){
   System.debug('i.categories'+i.categories+'this.IdeaCategories[0]' +this.IdeaCategories[3]);
      if(i.categories==this.IdeaCategories[3]){
         IdeasCategory4.add(i);
     }
   }
    //IdeasCategory4.sort();
   return IdeasCategory4;
  }
   public Idea[] getIdeasCategory5(){
     //Idea [] TotalIdeas= ideaSetController.getIdeaList();
     Idea [] TotalIdeas= ideasAlllist;
    IdeasCategory5=new List<idea> ();
     for (Idea i: TotalIdeas){
    System.debug('i.categories'+i.categories+'this.IdeaCategories[0]' +this.IdeaCategories[3]);
      if(i.categories==this.IdeaCategories[4]){
          IdeasCategory5.add(i);
     }
   }
// IdeasCategory4.sort();
   return IdeasCategory5;
  }
}