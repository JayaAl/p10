/***************************************************
   Name: CntrlrExtension_PandoraIdeaLandingPage

   Usage: This class displays an Idea on PandoraIdeaLandingPage VF page.

   Author – Stratitude, Inc.

   Date – 07/08/2012

  Modified By - Charudatta Mandhare
  
******************************************************/



public with sharing class CntrlrExtension_PandoraIdeaLandingPage {

    public string SearchIdeaText {get; set;}
    public string SelectedCategory {get; set;}
    public List<selectOption> CategoryValues;
    public boolean rendersearchedIdeas{get; set;}
    public boolean renderAllIdeas{get; set;}
    public List<Idea> IdeaList{get;set;}
    public List<Idea>IdeasAllList{get;set;}
    public List<Idea>IdeasVotingList{get;set;}
    public List<String>IdeaCategories {get; set;}
    public List<Idea>IdeasCategory1;
    public List<Idea>IdeasCategory2;
    public List<Idea>IdeasCategory3;
    public List<Idea>IdeasCategory4;
    public List<Idea>IdeasCategory5;
    public string ideaVoted{get; set;}
    public string upDownFlag{get; set;}
    public List<Idea> i {get;set;}

    //Instantiate the StandardSetController
    public ApexPages.StandardSetController con{get; set;}
    public final ApexPages.IdeaStandardSetController ideaSetController {get; set;}
   
   public CntrlrExtension_PandoraIdeaLandingPage(ApexPages.IdeaStandardSetController controller) {
        this.SearchIdeaText='Search Questions';
        Apexpages.currentPage().getHeaders().put('X-UA-Compatible', 'IE=8');
        ideaSetController = (ApexPages.IdeaStandardSetController)controller;
        this.renderAllIdeas=True;
        ideasAlllist =[SELECT Id, Title, Body, VoteTotal, CreatedDate, CreatedById, createdby.communityNickname,categories, NumComments from Idea Where CommunityID = '09a4000000000jZ' and   CreatedDate > 2012-01-07T01:02:03Z Order by VoteTotal Desc limit 1000];
        con = new ApexPages.StandardSetController(Database.getQueryLocator([select Id, title, body, CreatedById, CreatedBy.CommunityNickName, CreatedDate, VoteTotal, NumComments, categories from Idea Where CommunityID = '09a4000000000jZ' and  CreatedDate > 2012-01-07T01:02:03Z Order by VoteTotal Desc limit 1000]));
        con.setPageSize(3); 
        

        IdeaCategories= new List<String>();
        Schema.DescribeFieldResult F = Idea.Categories.getDescribe();
        List<Schema.PicklistEntry> pick_list_values = F.getPicklistValues();
        for (Schema.PicklistEntry a : pick_list_values) { //for all values in the picklist list
             this.IdeaCategories.add(a.getValue());
        }
    }
    
    
    public class ideasWrapper {
    public Idea idea{get; set;} //Idea object
    public Boolean isVoted {get; set;} //Vote flag variable 
    //Wrapper Class
    public ideasWrapper(Idea a, Boolean isVoted) {
            this.idea = a; //assign idea
            this.isVoted= isVoted;
        }
    }
    //List of Wrapper Class
    public List<ideasWrapper> vote_list {get; set;} 

    
    
    
    
     public List<IdeasWrapper> getIdeas() {
     
         List<Idea> IdeasAll = (List<Idea>)con.getRecords(); 
         vote_list = new List<ideasWrapper>();
         for(Idea i1 : IdeasAll ){
              String ideaId = i1.Id;
              Idea i2 =[SELECT Id, Title, Body, VoteTotal, CreatedDate, CreatedById, createdby.communityNickname,categories, NumComments from Idea Where CommunityID = '09a4000000000jZ' and CreatedDate > 2012-01-07T01:02:03Z and  Id = :ideaId Order by VoteTotal Desc limit 1000];
              Idea ideasVoting = [SELECT Id, Title, Body, VoteTotal,CreatedDate, CreatedById, createdby.communityNickname, categories,NumComments , (Select Id, IsDeleted, ParentId, Type, CreatedDate, CreatedById, CreatedBy.CommunityNickName, SystemModstamp From Votes Where  CreatedDate > 2012-01-07T01:02:03Z) FROM Idea Where Id=:ideaId and  CreatedDate > 2012-01-07T01:02:03Z Order by VoteTotal Desc limit 1000 ];   
              Boolean VotedIdeas = False;
              List<Vote> Votes = ideasVoting.Votes;
              for(Vote v : Votes){
                  if(v.CreatedById==UserInfo.getUserId()){  
                      VotedIdeas = True;
                  }
              } 
              vote_list.add( new ideasWrapper(i2,VotedIdeas));
         } 
         return (List<IdeasWrapper>)vote_list;  
    }
    

  
   public pagereference searchIdeas(){
  
      this.renderAllIdeas=False;
      this.rendersearchedIdeas=True;
      String srch = +SearchIdeaText+'*';
      List<List<Idea>> IdeaLists=[FIND :srch IN All FIELDS RETURNING Idea(id,title, body, createdDate,createdById, createdBy.CommunityNickName, NumComments, VoteTotal, categories Where CreatedDate > 2012-01-07T01:02:03Z Order By VoteTotal Desc)];
      this.con=new ApexPages.StandardSetController(+IdeaLists[0]);
      this.con.setPagesize(3);
       return null;
   }
   
   
    // indicates whether there are more records after the current page set.
    public Boolean hasNext {
        get {
            return con.getHasNext();
        }
        set;
    }
 
    // indicates whether there are more records before the current page set.
    public Boolean hasPrevious {
        get {
            return con.getHasPrevious();
        }
        set;
    }
 
    // returns the page number of the current page set
    public Integer pageNumber {
        get {
            return con.getPageNumber();
        }
        set;
    }
 
    // returns the first page of records
    public void first() {
       con.first();
    }
 
    // returns the last page of records
    public void last() {
        con.last();
    }
 
    // returns the previous page of records
    public void previous() {
        con.previous();
    }
 
    // returns the next page of records
    public void next() {
        con.next();
    }
 
    // returns the PageReference of the original page, if known, or the home page.
    public void cancel() {
       con.cancel();
    }
    
     public List<selectOption> getCategoryValues() {
      List<selectOption> options = new List<selectOption>(); //new list for holding all of the picklist options
      IdeaCategories = new List<String>();
      Schema.DescribeFieldResult F = Idea.Categories.getDescribe();
      List<Schema.PicklistEntry> pick_list_values = F.getPicklistValues();
      for (Schema.PicklistEntry a : pick_list_values) { //for all values in the picklist list          
           options.add(new selectOption(a.getLabel(), a.getValue())); //add the value and label     to our final list
           this.IdeaCategories.add(a.getValue());  
      }
      return options; //return the List
   }
  
   public pagereference getCategorizedIdeas(){
      if(this.SelectedCategory!='All'){
         con  = new ApexPages.StandardSetController(Database.getQueryLocator([select Id, title, body, CreatedById, CreatedBy.CommunityNickName, CreatedDate, VoteTotal, NumComments, categories from Idea where categories=:this.SelectedCategory And CreatedDate > 2012-01-07T01:02:03Z Order by VoteTotal Desc  limit 1000]));
         con.setPageSize(3); 
     } 
    if(this.SelectedCategory=='All')
     {
         this.con=new ApexPages.StandardSetController(+IdeasAllList);
         this.con.setPagesize(3);
     } 
     return null;
 
 }
 public Idea[] getIdeasCategory1(){
    Idea [] TotalIdeas= ideasAlllist;
    IdeasCategory1=new List<idea> ();
    for (Idea i: TotalIdeas){
          if(i.categories==this.IdeaCategories[0]){
              IdeasCategory1.add(i);
          }
    }
    return IdeasCategory1;
  }
  
  public Idea[] getIdeasCategory2(){
    Idea [] TotalIdeas= ideasAlllist;
    IdeasCategory2=new List<idea> ();
        for (Idea i: TotalIdeas){
           System.debug('i.categories'+i.categories+'this.IdeaCategories[0]' +this.IdeaCategories[1]);
           if(i.categories==this.IdeaCategories[1]){
              IdeasCategory2.add(i);
           }
        }
    return IdeasCategory2;
  }
   public Idea[] getIdeasCategory3(){
     Idea [] TotalIdeas= ideasAlllist;
     IdeasCategory3=new List<idea> ();
     for (Idea i: TotalIdeas){
       System.debug('i.categories'+i.categories+'this.IdeaCategories[0]' +this.IdeaCategories[2]);
       if(i.categories==this.IdeaCategories[2]){
          IdeasCategory3.add(i);
      }
     }
    return IdeasCategory3;
  }
   public Idea[] getIdeasCategory4(){
      Idea [] TotalIdeas= ideasAlllist;
      IdeasCategory4=new List<idea> ();
      for (Idea i: TotalIdeas){
        System.debug('i.categories'+i.categories+'this.IdeaCategories[0]' +this.IdeaCategories[3]);
        if(i.categories==this.IdeaCategories[3]){
          IdeasCategory4.add(i);
         }
       }
   return IdeasCategory4;
  }
  
  public Idea[] getIdeasCategory5(){
    Idea [] TotalIdeas= ideasAlllist;
    IdeasCategory5=new List<idea> ();
    for (Idea i: TotalIdeas){
        System.debug('i.categories'+i.categories+'this.IdeaCategories[0]' +this.IdeaCategories[3]);
          if(i.categories==this.IdeaCategories[4]){
              IdeasCategory5.add(i);
     }
    }
   return IdeasCategory5;
  }
  
   
      public pagereference voteUpDownFunctionForLandingPage(){
        Vote vote = new vote();
        vote.ParentId=ideaVoted;
        vote.type=upDownFlag;
        try{
            insert(vote);
            getIdeas();         
        }catch (Exception e){
             ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.Error, 'Already voted'));
             return null; 
        }
        return null; 
     } 
    
      public pagereference voteUpDownFunctionForSearch(){
        Vote vote = new vote();
        vote.ParentId=ideaVoted;
        vote.type=upDownFlag;
        try{
           insert(vote);
           getIdeas();
        }catch (Exception e){
             ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.Error, 'Already voted'));
             return null; 
        }
        //searchIdeas();
        return null; 
    }
          
}