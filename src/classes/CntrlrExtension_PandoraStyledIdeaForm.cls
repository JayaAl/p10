/***************************************************
   Name:  CntrlrExtension_PandoraStyledIdeaForm

   Usage: This class displays an Idea on PandoraStyledIdeaForm VF page.

   Author – Stratitude, Inc.

   Date – 07/08/2012

  Modified By - Charudatta Mandhare
  
******************************************************/


public with sharing class CntrlrExtension_PandoraStyledIdeaForm {
      
      Public Idea i {get; set;}
      public String s = '';
     public List<selectOption> CategoryValues;
       public List<String>IdeaCategories {get; set;}
       public string id {get; set;}
       
       //public final ApexPages.IdeaStandardSetController ideaSetController {get; set;}
          private final ApexPages.StandardController ideaController;
        /* public CntrlrExtension_PandoraStyledIdeaForm(ApexPages.IdeaStandardSetController controller) {
            
           ideaSetController = (ApexPages.IdeaStandardSetController)controller;
         }*/
     public List<Idea>IdeasAllList{get;set;}     
       public List<Idea>IdeasCategory1;
       public List<Idea>IdeasCategory2;
       public List<Idea>IdeasCategory3;
       public List<Idea>IdeasCategory4;
       public List<Idea>IdeasCategory5;    
    public CntrlrExtension_PandoraStyledIdeaForm(ApexPages.StandardController controller) {
   // ideaSetController = (ApexPages.IdeaStandardSetController)controller;
    Apexpages.currentPage().getHeaders().put('X-UA-Compatible', 'IE=9');
     this.i = (Idea)controller.getRecord();
     ideasAlllist =[SELECT Id, Title, Body, VoteTotal, CreatedById, createdby.communityNickname,categories, NumComments from Idea Where CreatedDate > 2012-01-07T01:02:03Z Order by VoteTotal Desc limit 1000];
     List<selectOption> options = new List<selectOption>(); //new list for holding all of the picklist options
     IdeaCategories= new List<String>();
     Schema.DescribeFieldResult F = Idea.Categories.getDescribe();
      List<Schema.PicklistEntry> pick_list_values = F.getPicklistValues();
      for (Schema.PicklistEntry a : pick_list_values) { //for all values in the picklist list
         this.IdeaCategories.add(a.getValue());
         options.add(new selectOption(a.getLabel(), a.getValue())); //add the value and label to our final list
      }
      //return options; //return the List
    this.CategoryValues=options;
    }
     public List<selectOption> getCategoryValues() {
          List<selectOption> options = new List<selectOption>(); //new list for holding all of the picklist options
          IdeaCategories = new List<String>();
          Schema.DescribeFieldResult F = Idea.Categories.getDescribe();
          List<Schema.PicklistEntry> pick_list_values = F.getPicklistValues();
          for (Schema.PicklistEntry a : pick_list_values) { //for all values in the picklist list
              
             options.add(new selectOption(a.getLabel(), a.getValue())); //add the value and label to our final list
           this.IdeaCategories.add(a.getValue());
          }
          return options; //return the List
   }
    public pageReference Save(){
    idea CurrentIdea = new Idea();
    
    CurrentIdea.title= this.i.title;
    CurrentIdea.body=this.i.Body;
   // CurrentIdea.CommunityId='09a4000000000jZ';
     CurrentIdea.CommunityId=[Select c.Id From Community c where c.Name='Internal Ideas'][0].id;
    
    CurrentIdea.categories= this.i.categories;
    try{
    database.insert (CurrentIdea);
    }
    
    catch (Exception e){
    
    System.debug('Exception'+e);
    
    }
     
   //String fullFileURL = URL.getSalesforceBaseUrl().toExternalForm()+'/apex/PandoraStyledIdeaForm/apex/PandoraViewIdea?id='+CurrentIdea.id;
   
   // PageReference secondPage = new pagereference(fullFileURL);
    
    
   //PageReference secondPage = new pagereference('/apex/PandoraStyledIdeaForm/apex/PandoraViewIdea');
    this.id = CurrentIdea.id;
   // PageReference secondPage = new pagereference('http://c.cs7.visual.force.com/apex/PandoraViewIdea?id=087M00000008P9TIAU');
   PageReference secondPage = Page.PandoraViewIdea;
    
      secondPage.setRedirect(true);
       secondPage.getParameters().put('id', CurrentIdea.id); 
        
        return secondPage ;
    }
    
    
    public String getString() {
        return s;
    }
                        
    public void setString(String s) {
        this.s = s;
    }
    
    public Idea[] getIdeasCategory1(){
     Idea [] TotalIdeas= ideasAlllist ;
    IdeasCategory1=new List<idea> ();
     for (Idea i: TotalIdeas){
   System.debug('i.categories'+i.categories+'this.IdeaCategories[0]' +this.IdeaCategories[0]);
      if(i.categories==this.IdeaCategories[0]){
          IdeasCategory1.add(i);
     }
   }
      // IdeasCategory1.sort();
   return IdeasCategory1;
  }
  
  public Idea[] getIdeasCategory2(){
     Idea [] TotalIdeas= ideasAlllist ;
    IdeasCategory2=new List<idea> ();
     for (Idea i: TotalIdeas){
   System.debug('i.categories'+i.categories+'this.IdeaCategories[0]' +this.IdeaCategories[1]);
      if(i.categories==this.IdeaCategories[1]){
          IdeasCategory2.add(i);
     }
   }
          // IdeasCategory2.sort();
   return IdeasCategory2;
  }
   public Idea[] getIdeasCategory3(){
     Idea [] TotalIdeas= ideasAlllist;
    IdeasCategory3=new List<idea> ();
     for (Idea i: TotalIdeas){
   System.debug('i.categories'+i.categories+'this.IdeaCategories[0]' +this.IdeaCategories[2]);
      if(i.categories==this.IdeaCategories[2]){
          IdeasCategory3.add(i);
     }
   }
       //IdeasCategory3.sort();
   return IdeasCategory3;
  }
   public Idea[] getIdeasCategory4(){
     Idea [] TotalIdeas= ideasAlllist ;
    IdeasCategory4=new List<idea> ();
     for (Idea i: TotalIdeas){
   System.debug('i.categories'+i.categories+'this.IdeaCategories[0]' +this.IdeaCategories[3]);
      if(i.categories==this.IdeaCategories[3]){
          IdeasCategory4.add(i);
     }
   }
     //  IdeasCategory4.sort();
   return IdeasCategory4;
  }
 public Idea[] getIdeasCategory5(){
     //Idea [] TotalIdeas= ideaSetController.getIdeaList();
     Idea [] TotalIdeas= ideasAlllist;
    IdeasCategory5=new List<idea> ();
     for (Idea i: TotalIdeas){
    System.debug('i.categories'+i.categories+'this.IdeaCategories[0]' +this.IdeaCategories[3]);
      if(i.categories==this.IdeaCategories[4]){
          IdeasCategory5.add(i);
     }
   }
// IdeasCategory4.sort();
   return IdeasCategory5;
  }
}