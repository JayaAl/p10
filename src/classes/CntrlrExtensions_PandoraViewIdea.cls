/***************************************************
   Name: CntrlrExtensions_PandoraViewIdea 

   Usage: This class displays an Idea on PandoraViewIdea VF page.

   Author – Stratitude, Inc.

   Date – 07/08/2012

  Modified By - Charudatta Mandhare
  
******************************************************/


public with sharing class CntrlrExtensions_PandoraViewIdea {

   public Idea i {get;set;}
   public IdeaComment[] Commments {get; set;}
   
   
   public String Comment{get; set;}
   public boolean VotedIdeas;
   public string CommentSize {get; set;}
   public boolean renderVoteUp{get;set;}
   public boolean renderVoteDown{get;set;}
  public boolean renderButton;
  
  public List<String>IdeaCategories {get; set;}
    public List<Idea>IdeasAllList{get;set;}
   public List<Idea>IdeasCategory1;
   public List<Idea>IdeasCategory2;
   public List<Idea>IdeasCategory3;
   public List<Idea>IdeasCategory4;
  public List<Idea>IdeasCategory5;
  
   public Boolean getrenderButton(){
   
   if(i.CreatedByid==Userinfo.getUserId()){
        renderButton= True;
   }else renderButton= False;
   return renderButton;
   
   }
   
  private final ApexPages.IdeaStandardController ideaController;
  //public final ApexPages.IdeaStandardSetController ideaSetController {get; set;}
  
    public CntrlrExtensions_PandoraViewIdea(ApexPages.IdeaStandardController controller) {
    
    Apexpages.currentPage().getHeaders().put('X-UA-Compatible', 'IE=8');
     ideaController= (ApexPages.IdeaStandardController)controller;
        i = [SELECT Id, Title, Body, VoteTotal, CreatedById, createdby.communityNickname, categories, (Select Id, IsDeleted, ParentId, Type, CreatedDate, CreatedById, CreatedBy.CommunityNickName, SystemModstamp From Votes) FROM Idea 
                   WHERE Id = :ApexPages.currentPage().getParameters().get('id')];
       ideasAlllist =[SELECT Id, Title, Body, VoteTotal, CreatedById, createdby.communityNickname,categories, NumComments from Idea Where CreatedDate > 2012-01-07T01:02:03Z Order By VoteTotal DESC limit 1000];
        VotedIdeas = False;           
        
      
      IdeaCategories= new List<String>();
      Schema.DescribeFieldResult F = Idea.Categories.getDescribe();
      List<Schema.PicklistEntry> pick_list_values = F.getPicklistValues();
      for (Schema.PicklistEntry a : pick_list_values) { //for all values in the picklist list
         this.IdeaCategories.add(a.getValue());
        
      }
                         
    }
    
    public boolean getVotedIdeas(){
    VotedIdeas = False;
    List<Vote> Votes = i.Votes;
    for(Vote v : Votes){
      if(v.CreatedById==UserInfo.getUserId()){
      
      VotedIdeas = True;
      
      
      }
    
    
    }
    
    return VotedIdeas;
    }
    public pagereference voteup(){
    
    Vote vote = new vote();
    vote.ParentId=i.id;
    vote.type='up';
    //vote.CreatedById= UserInfo.getUserId();
    try{
    insert(vote);
    }catch (Exception e){
    
    
     ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.Error, 'Already voted'));
       return null; 
    }
    
    
    this.i= [SELECT Id, Title, Body, VoteTotal, CreatedById, (Select Id, IsDeleted, ParentId, Type, CreatedDate, CreatedById, SystemModstamp From Votes) FROM Idea WHERE Id = :ApexPages.currentPage().getParameters().get('id')];
    
    return null; 
    
    }

   
   
    public pagereference votedown(){
    
    Vote vote = new vote();
    vote.ParentId=i.id;
    vote.type='down';
    //vote.CreatedById= UserInfo.getUserId();
    try{
    insert(vote);
   }catch (Exception e){
    
    
    ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.Error, 'Already voted'));
      return null; 
   }
    
    
    this.i= [SELECT Id, Title, Body, VoteTotal, CreatedById, (Select Id, IsDeleted, ParentId, Type, CreatedDate, CreatedById, SystemModstamp From Votes) FROM Idea WHERE Id = :ApexPages.currentPage().getParameters().get('id')];
    
    return null; 
    
    }
    
    
    
    public List<IdeaComment> getComments() {
        IdeaComment[] comments = ideaController.getCommentList();
        // modify comments here 
     this.commentsize=string.ValueOf(comments.size());
        return comments;
    }
  
   
    public PageReference AddComment(){
    
    IdeaComment ideaComment = new IdeaComment();
   // ideaComment.createdby.Id = 
    ideaComment.commentBody= this.comment;
    ideaComment.IdeaId = i.id;
    this.comment='';
   try {
    insert(ideaComment );
    }
    catch (DmlException e){
    
    //ApexPages.Message myMsg = new ApexPages.Message(ApexPages.Severity.Error,e.getmessage());
    ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.Error, 'Comment cannot be blank'));
   return null;
    }
    
     PageReference secondPage = new pagereference('/apex/PandoraViewIdea');
        
        secondPage .getParameters().put('id', i.id); 
        secondPage .setRedirect(true);
     return secondPage ; 
      //return null;
    
    
    
    }
   public pagereference deleteIdea(){
   
  database.delete(i);
  
   PageReference IdeaLandingPage = new pagereference('/apex/PandoraIdeaLandingPage');
        
        IdeaLandingPage .getParameters().put('id', i.id); 
        IdeaLandingPage .setRedirect(true);
     return IdeaLandingPage; 
   
   }
public pagereference EditIdea(){
    PageReference IdeaEditPage = new pagereference('/apex/pandoraIdeaEditPage');
        
        IdeaEditPage.getParameters().put('id',i.id); 
        IdeaEditPage.setRedirect(true);
     return IdeaEditPage ; 
 
   
   }
   public Idea[] getIdeasCategory1(){
    // Idea [] TotalIdeas= ideaSetController.getIdeaList();
    IdeasCategory1=new List<idea> ();
     for (Idea i: IdeasAlllist){
   System.debug('i.categories'+i.categories+'this.IdeaCategories[0]' +this.IdeaCategories[0]);
      if(i.categories==this.IdeaCategories[0]){
          IdeasCategory1.add(i);
     }
   }
   // IdeasCategory1.sort();
   return IdeasCategory1;
  }
   public Idea[] getIdeasCategory2(){
     //Idea [] TotalIdeas= ideaSetController.getIdeaList();
    IdeasCategory2=new List<idea> ();
     for (Idea i: IdeasAllList){
   System.debug('i.categories'+i.categories+'this.IdeaCategories[0]' +this.IdeaCategories[1]);
      if(i.categories==this.IdeaCategories[1]){
          IdeasCategory2.add(i);
     }
   }
 //IdeasCategory2.sort();
   return IdeasCategory2;
  }
   public Idea[] getIdeasCategory3(){
     //Idea [] TotalIdeas= ideaSetController.getIdeaList();
    IdeasCategory3=new List<idea> ();
     for (Idea i: IdeasAllList){
   System.debug('i.categories'+i.categories+'this.IdeaCategories[0]' +this.IdeaCategories[2]);
      if(i.categories==this.IdeaCategories[2]){
          IdeasCategory3.add(i);
     }
   }
    //IdeasCategory3.sort();
   return IdeasCategory3;
  }
   public Idea[] getIdeasCategory4(){
    // Idea [] TotalIdeas= ideaSetController.getIdeaList();
    IdeasCategory4=new List<idea> ();
     for (Idea i: IdeasAllList){
   System.debug('i.categories'+i.categories+'this.IdeaCategories[0]' +this.IdeaCategories[3]);
      if(i.categories==this.IdeaCategories[3]){
          IdeasCategory4.add(i);
     }
   }
    //IdeasCategory4.sort();
   return IdeasCategory4;
  }
   public Idea[] getIdeasCategory5(){
     //Idea [] TotalIdeas= ideaSetController.getIdeaList();
     Idea [] TotalIdeas= ideasAlllist;
    IdeasCategory5=new List<idea> ();
     for (Idea i: TotalIdeas){
    System.debug('i.categories'+i.categories+'this.IdeaCategories[0]' +this.IdeaCategories[3]);
      if(i.categories==this.IdeaCategories[4]){
          IdeasCategory5.add(i);
     }
   }
// IdeasCategory4.sort();
   return IdeasCategory5;
  }
}