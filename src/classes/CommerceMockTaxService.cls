public class CommerceMockTaxService implements CommerceTaxService{
	public CommerceMockTaxService()
	{	
		this.init();
		
	}
	
	public CommerceTaxResponse getSalesTaxAmount(Integer amount, CommerceAddress address, boolean isAudit, String invNumber)
	{
		Decimal tax = amount * .08;
		tax.setScale(2);
		CommerceTaxResponse resp = new CommerceTaxResponse(tax, true);
		return resp;
	}
	
	public void init()
	{
		
	}

}