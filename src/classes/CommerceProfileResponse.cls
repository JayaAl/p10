public abstract with sharing class CommerceProfileResponse {
	protected String errorMessage;
	protected boolean succesful;
	protected String extVaultId;
	protected CreditCardDetails creditCardDetails;
	
	protected CommerceProfileResponse(String errorMessage, boolean succesful)
	{
		this.errorMessage = errorMessage;
		this.succesful = succesful;
	}
	
	protected CommerceProfileResponse(boolean succesful)
	{
		this.succesful = succesful;
	}
	
	protected CommerceProfileResponse(CreditCardDetails creditCardDetails, boolean succesful)
	{
		this.creditCardDetails = creditCardDetails;
		this.succesful = succesful;
	}
	
	public String getErrorMessage()
	{
		return this.errorMessage;
	}
	
	public boolean isSuccesful()
	{
		return this.succesful;
	}
	
	public String getExtVaultId()
	{
		return this.extVaultId;
	}
	
	public CreditCardDetails getCreditCardDetails()
	{
		return this.creditCardDetails;
	}
	
	public void setIsSuccesful(boolean isSuccesful)
	{
		this.succesful = isSuccesful;
	}
	
	public void setCreditCardDetails(CreditCardDetails creditCardDetails)
	{
		this.creditCardDetails = creditCardDetails;
	}
	
	public void setExtVaultId(String extVaultId)
	{
		this.extVaultId = extVaultId;
	}
	
	public void setErrorMessage(String errorMessage)
	{
		this.errorMessage = errorMessage;
	}
}