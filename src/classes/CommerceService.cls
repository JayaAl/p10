/**
example of using the commerce service
amount is an integer value dollars and cents so $10.34 is sent as 1034  $10.00 is sent as 1000

CreditCardDetails det = new CreditCardDetails('Dan', 08, 2014, '94517', 'Ca', '123');
det.setCreditCardNumber('4552534341130849');
CommerceService gate = CommerceServiceFactory.getCommerceService();
CommerceProfileResponse resp = gate.createCustomerProfile(det);
System.debug(resp.getExtVaultId());˜
gate.makeSettlement(det, '10000', 'HHKHJKHHHDK123', resp.getExtVaultId());
**/

public interface CommerceService {
	/**
	 * Charges a credit card with the amount specified
	 * @param details details of the credit card
	 * @param amountInCents amount, in pennies, to be charged
	 *   request sent to the commerce system.
	 * @param invoiceNumber - an optionally provided invoice number
	 * @param customerReferenceNumber id of credit card to be charged
	 * @return
	 */
	CommerceTransactionResponse makeSettlement(CreditCardDetails details, String amountInCents, String invoiceNumber, String customerReferenceNumber);

	/**
     * Create an external entry of the credit card data within a third party vault of the payment processor.
     * @param creditCardNumber     
     * @param details
     * @return the external vault id of the card(customerReferenceNumber)
     */
    CommerceProfileResponse createCustomerProfile(CreditCardDetails details);

    /**
     * Update an external credit card profile with new credit card details for the given credit card
     * @param details
     * @return the external vault id of the card
     */
    CommerceProfileResponse updateCustomerProfile(String customerReferenceNumber, CreditCardDetails details);
    
    CommerceProfileResponse fetchCustomerProfile(String customerReferenceNumber);

}