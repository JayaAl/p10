public with sharing class CommerceServiceFactory {

	/**
	 * CommerceService instance singleton.
	 */
	private static CommerceService service = null;
	
	private static CommerceTaxService taxService = null;

	/**
	 * Private constructor.
	 */
	private CommerceServiceFactory() {
	}

	/**
	 * Factory method for retrieving a handle to the singleton CommerceService
	 * instance.
	 */
	public static CommerceService getCommerceService(){

		if (service == null) {
			if (Test.isRunningTest())
			{
				System.debug('return mock gateway');
				service = new PaymentechMockGateway();
			}
			else
			{	
				System.debug('return actual gateway for environment');	
				//the PaymentechNetGatewayConfig handles the credentials and 
				//endpoint for the test and production environments	
				service = new PaymentechNetGateway();
			}
		} 

		if (service == null) {
			throw new CommerceException('Failed to instantiate commerce service singleton!');
		}

		return service;
	}
	
	public static CommerceTaxService getCommerceTaxService()
	{
		if( taxService == null)
		{
			if(Test.isRunningTest())
			{
				System.debug('return mock tax service');
				taxService = new CommerceMockTaxService();
			}else
			{
				System.debug('Mock tax service for now');
				//the SabrixTaxServiceConfig handles the credentials and
				//endpoint for the test and production environments
				taxService = new SabrixTaxService();
			}
		}
		if(taxService == null)
		{
			throw new CommerceException('Failed to instantiate tax service');
		}
		
		return taxService;
	}

	/**
	 * Shuts down the single RadioService instance currently associated with this
	 * factory, and releases resources associated with it.
	 */
	public static void shutdown(){
		if (service == null) {
			throw new CommerceException(
				'Cannot shutdown because no singleton is currently instantiated!');
		}

		service = null;
	}

}