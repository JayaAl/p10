public class CommerceTaxResponse {
	protected String errorMessage;
	protected boolean succesful;
	protected Decimal tax;
	
	public CommerceTaxResponse(String errorMessage, boolean succesful)
	{
		this.errorMessage = errorMessage;
		this.succesful = succesful;
	}
	
	public CommerceTaxResponse(boolean succesful)
	{
		this.succesful = succesful;
	}
	
	public CommerceTaxResponse(Decimal tax, boolean succesful)
	{
		this.tax = tax;
		this.succesful = succesful;
	}
	
	public String getErrorMessage()
	{
		return this.errorMessage;
	}
	
	public boolean isSuccesful()
	{
		return this.succesful;
	}
	
	public Decimal getTax()
	{
		return this.tax;
	}
	
	public void setIsSuccesful(boolean isSuccesful)
	{
		this.succesful = isSuccesful;
	}
	
	public void setTax(Decimal tax)
	{
		this.tax = tax;
	}
	
	public void setErrorMessage(String errorMessage)
	{
		this.errorMessage = errorMessage;
	}
}