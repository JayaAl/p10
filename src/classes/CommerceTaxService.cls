/*
example of using the commerce tax service
amount is an integer value dollars and cents so $10.34 is sent as 1034  $10.00 is sent as 1000

CommerceAddress add = new CommerceAddress('Daniel', '17345 Bishopsgate Dr', '', 'Pflugerville', 'Tx', '78660');
CommerceTaxService serv = CommerceServiceFactory.getCommerceTaxService();
CommerceTaxResponse resp = serv.getSalesTaxAmount(10000, add, false, '1000');

*/
public interface CommerceTaxService {

	CommerceTaxResponse getSalesTaxAmount(Integer amount, CommerceAddress address, boolean isAudit, String invNumber);
	
}