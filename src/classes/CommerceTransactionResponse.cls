public abstract with sharing class CommerceTransactionResponse {

	protected final String transactionTime;
	protected final String errorMessage;
	protected final String orderNumber;
	protected final String approvalCode;
	protected final String transactionTimestamp;

	protected final boolean approved;
	protected final String approvedText;
	protected final String addressVerification;
	protected final boolean abnormalError;
    protected String txRefNumber;

    protected CommerceTransactionResponse(boolean approved, String approvedText,
		String transactionTime, String errorMessage, String orderNumber,
		String approvalCode, String transactionTimestamp,
		String addressVerification, boolean abnormalError) {
		this.approved = approved;
		this.approvedText = approvedText;
		this.transactionTime = transactionTime;
		this.errorMessage = errorMessage;
		this.orderNumber = orderNumber;
		this.approvalCode = approvalCode;
		this.transactionTimestamp = transactionTimestamp;
		this.addressVerification = addressVerification;
		this.abnormalError = abnormalError;
	}

    protected CommerceTransactionResponse(boolean approved, String approvedText,
		String transactionTime, String errorMessage, String orderNumber,
		String approvalCode, String transactionTimestamp,
		String addressVerification, String txRefNumber, boolean abnormalError) {
		this.approved = approved;
		this.approvedText = approvedText;
		this.transactionTime = transactionTime;
		this.errorMessage = errorMessage;
		this.orderNumber = orderNumber;
		this.approvalCode = approvalCode;
		this.transactionTimestamp = transactionTimestamp;
		this.addressVerification = addressVerification;
        this.txRefNumber = txRefNumber;
		this.abnormalError = abnormalError;
	}
	
	protected CommerceTransactionResponse(boolean approved, String errorMessage)
	{
		this.approved = approved;
		this.errorMessage = errorMessage;
	}

	public boolean getApproved() {
		return approved;
	}

	public abstract String getErrorMessage();

	public String getOrderNumber() {
		return orderNumber;
	}

	public String getTransactionTime() {
		return transactionTime;
	}

	public String getApprovalCode() {
		return approvalCode;
	}

	public String getTransactionTimestamp() {
		return transactionTimestamp;
	}

	public String getApprovedText() {
		return approvedText;
	}

	public String getAddressVerification() {
		return addressVerification;
	}

	public boolean isAbnormalError() {
		return abnormalError;
	}

    public abstract String getTxRefNumber();
}