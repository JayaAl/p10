public without sharing class CommunitiesTandCCompCtrlr {
  
  public string quoteIdStr {get;set;}
  public string tandCSelection {get;set;}
  public id accountId{get;set;}
  public boolean isSuccess {get;set;}
  public SBQQ__Quote__c quote{get;set;}
  public id quoteId{get;set;} 
  public boolean isCreditApproved {get;set;}

  public CommunitiesTandCCompCtrlr() {
    isSuccess= false;
    isCreditApproved = false;
    this.quoteId = ApexPages.currentPage().getParameters().get('QID');
    this.accountId = ApexPages.currentPage().getParameters().get('accountId');
      if(quoteId==null)
          return;
          
      quote = [select id,Name,OwnerId,SBQQ__Opportunity2__r.ATG_Campaign_Status__c,ATG_Campaign_Name__c,SBQQ__StartDate__c,SBQQ__EndDate__c,ATG_BudgetTotal__c, SBQQ__ExpirationDate__c ,SBQQ__Account__r.Id, SBQQ__Account__r.Name,ATG_Campaign_Status__c,SBQQ__Account__r.Credit_Status__c,SBQQ__Account__r.Credit_Approved_Amount__c from SBQQ__Quote__c where id=:quoteId];
        
  }
  public void acceptAction(){
    system.debug('accept action');
      setOpportunityStatus();
      isSuccess = true;    
    system.debug('accept action ends ');
  }

  public Pagereference redirectToPage(){

    Pagereference pageRef ;
        
    
      isSuccess = false;
      pageRef = new PageReference('/apex/ATG_QuoteSummary?QID='+quoteId);
    

    return pageRef;
  }

  public Pagereference setOpportunityStatus()
  {
    string campaignStatus='';

    opportunity op = [select id,ContractStartDate__c,ContractEndDate__c,ATG_Campaign_Status__c,StageName,probability from opportunity where id=:quote.SBQQ__Opportunity2__r.Id];
    //CODE COMMENTED BELOW FOR AGENCY FLOW//
    //if(quote.SBQQ__Account__r.Credit_Status__c=='Approved'){
    //CODE CONDITION ADDED BELOW FOR AGENCY FLOW//
    //Map<string,string> accountTypeandCreditStatus  = new Map<string,string>();
    //accountTypeandCreditStatus = ATS_Util.getAccountCreditStatus();
      if(ATS_Util.getAccountCreditStatus() =='Approved'){
            campaignStatus = 'Approved';
            isCreditApproved = true;
        }else {
            campaignStatus = 'In Review';
        }
    if(isCreditApproved){

	    op.StageName = 'Closed Won';
	    op.probability = 100;
	    op.ContractStartDate__c = quote.SBQQ__StartDate__c;
	    op.ContractEndDate__c = quote.SBQQ__EndDate__c;
	    op.ATG_Campaign_Status__c = campaignStatus;
      op.OwnerId = ATS_Util.getSelfServiceUserId();
	    system.debug('opporunity :::-->'+op);	    
	    ATS_Util.updateOpportunity(op); 
     
      ATS_Util.sendEmail(quote.Id,op.Id);
	   

     
	 }else{

      op.ContractStartDate__c = quote.SBQQ__StartDate__c;
      op.ContractEndDate__c = quote.SBQQ__EndDate__c;
      op.ATG_Campaign_Status__c = campaignStatus;
      system.debug('opporunity :::-->'+op);     
      ATS_Util.updateOpportunity(op); 

   }     
        //ATS_Util.updateOppStage(new Set<id>{op.Id}, 'Closed Won', campaignStatus, 100);
    return null;
  }
  
 
   

  public PageReference GotoAccountSummary(){
    Pagereference pageRef = new PageReference('/apex/ATG_AccountSummary?accountId='+accountId+'&QID='+quoteId);
    return pageRef;
  }
}