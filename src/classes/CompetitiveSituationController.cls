public class CompetitiveSituationController {

    //list of all account team members, existing and blank for the page
    public List<AccountTeamMember> atmList { get; set; }

    public List<Competitive_Situation__c> csList { get; set; }


    public Competitive_Situation__c editCS { get; set; }
    public boolean nowAdding { get; private set; }
    
    public List<Competitive_Situation__c> fullCSList { get {
        List<Competitive_Situation__c> fullCSList = new List<Competitive_Situation__c>();
        fullCSList.addAll(csList);
        if (nowAdding != null && nowAdding && editCS != null) {
            fullCSList.add(editCS);
        }
        return fullCSList;
    }}
    
    public boolean hasMessages { get {return ApexPages.hasMessages();} }
    
    //account DP id from the page
    public id accDPId {get; set
        {
            this.accDPId = value;
            csList = queryCSList();
        } 
    }
    
    private String ownerId;
    private Account_Development_Plan__c accountDPInfo;
    
    public CompetitiveSituationController(ApexPages.StandardController controller) {
        //grab the account Id from the acc record we're on
        accDPId = System.currentPageReference().getParameters().get('id');
        accountDPInfo = [Select OwnerId, /*Account__c,*/ Owner.Email, Name, Id from Account_Development_Plan__c where Id = :accDPId];
        ownerId = accountDPInfo.OwnerId;
        //No longer needed due to the change in data model
        //atmList = queryATMList();
        nowAdding = false;
    }

    public void addNewRow() {
        Competitive_Situation__c newRow = new Competitive_Situation__c(Account_Development_Plan__c = accDPId);
        editCS = newRow;
        nowAdding = true;
    }

    //These methods are responsible for editing and deleting an EXISTING condition
    public String getParam(String name) {
        return ApexPages.currentPage().getParameters().get(name);  
    }

    public void delCS() {
        String delid = getParam('delid');
        try {
            Competitive_Situation__c[] compSit = [SELECT Id FROM Competitive_Situation__c WHERE Id=:delid ];
            delete compSit;
            csList = queryCSList();
        } catch (system.Dmlexception e) {
            ApexPages.addMessages(e);
        }
    }
    
    public void deleteAll() {
        try {
            Competitive_Situation__c[] compSit = [SELECT Id FROM Competitive_Situation__c WHERE Account_Development_Plan__c=:accDPId];
            delete compSit;
            csList = queryCSList();
        } catch (system.Dmlexception e) {
            ApexPages.addMessages(e);
        }
    }
   
        
    public void editOneCS() {
        String editid = getParam('editid');
        editCS  = [SELECT id, Account_Development_Plan__c, Question_1__c, Question_2__c, Question_3__c, Question_4__c, Question_5__c, Question_6__c
        FROM Competitive_Situation__c where id = :editId]; 
    }

    
    public void cancelEdit() {
        editCS = null;
        nowAdding = false;
    }
    
    public void saveEdit() { 
        try { 
            boolean primaryAssigned = false;
            Set<Id> recordsSelected = new Set<Id>();

            // Refresh list of account team member
            List<Competitive_Situation__c> existingCS = queryCSList();

            if(existingCS.size() != 0) {
                for (Competitive_Situation__c cs : existingCS) {
                    if( cs.id != editCS.id) {
                        recordsSelected.add(cs.Id);
                    }
                }
            }

            List<Competitive_Situation__c> csToUpsert = new List<Competitive_Situation__c>();
            csToUpsert.add(editCS);

            upsert csToUpsert id; 
            editCS = null;
            nowAdding = false;
            
            csList = queryCSList();

            
        } catch (system.Dmlexception e) {
            ApexPages.addMessages(e);
        }
        
        //return null;
    } 
    
    private List<Competitive_Situation__c> queryCSList() {
        return [SELECT id, Account_Development_Plan__c, Question_1__c, Question_2__c, Question_3__c, Question_4__c, Question_5__c, Question_6__c
              FROM Competitive_Situation__c WHERE
              Account_Development_Plan__c = :accDPId ORDER BY LastModifiedDate];
    } 

/*
    public List<AccountTeamMember> queryATMList(){
        return [Select Id, AccountAccessLevel,TeamMemberRole, UserId from AccountTeamMember where AccountId = :accountDPInfo.Account__c];
    }
*/

}