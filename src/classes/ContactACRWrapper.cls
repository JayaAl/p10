public class ContactACRWrapper{
    @AuraEnabled
    public Id contactId {get;set;}
    
    @AuraEnabled    
    public List<AccountContactRelation> acrLst {get;set;}
}