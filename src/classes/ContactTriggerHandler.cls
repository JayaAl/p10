/**
* @name: OpportunityTriggerHandler
* @desc: Used to handle all trigger operation for Opportunity object
*        Contains method specifc to each context used in the trigger   
* ──────────────────────────────────────────────────────────────────────────────────────────────────
* @changes
* v1.1            Jaya Alaparthi
* 2017-10-05      
* ──────────────────────────────────────────────────────────────────────────────────────────────────
*    
* ─────────────────────────────────────────────────────────────────────────────────────────────────┘
*/
public class ContactTriggerHandler {
    /**
     *   beforeInsertAction
     */
    public void onBeforeInsert(List<Contact>contactsList) {
          
    }

    /*
     *   afterInsertAction
     */
    public void onAfterInsert(List<Contact> contactsList,
                                map<Id,Contact> newContactMap) {
          
    }

    /**
     *   beforeUpdateAction
     */
    public void onBeforeUpdate(list<Contact> contacsList,
                                map<Id,Contact> newContactMap,
                                map<Id,Contact> oldContactMap) {
          
    }

    /*
     *   afterUpdateAction
     */
    public void onAfterUpdate(list<Contact> contacsList,
                                map<Id,Contact> newContactMap,
                                map<Id,Contact> oldContactMap) {
        
        // task email update
        ISTaskEmail.updateTaskEmail(newContactMap,oldContactMap);
    }
}