global class Contact_Rectify_Lead_Account_Source_BTCH implements Database.Batchable<sObject>, Database.Stateful{
	
    global Map<Id, String> errorMap {get; set;}
    global Map<Id, SObject> IdToSObjectMap {get; set;}
    global Contact_Rectify_Lead_Account_Source_BTCH() {
        errorMap = new Map<Id, String>();
        IdToSObjectMap = new Map<Id, SObject>();
    }
    global Database.QueryLocator start(Database.BatchableContext BC){
        return Database.getQueryLocator('Select AccountSource, Id, Name from Account');
    }
    
    global void execute(Database.BatchableContext BC, List<sObject> scope){
    
    	List<Account> accountsToUpdate = new List<Account>();
        for(sobject s : scope){
        	Account objAccount = (Account)s;
            String accountSource = '';
            for(Contact c: [Select Hidden_Lead_Source__c from Contact where AccountId =: objAccount.Id Order By CreatedDate desc, LastModifiedDate desc]) { //to avoid invalid query locatory exception we will query the contacts here in execute method
                if(!String.isBlank(c.Hidden_Lead_Source__c) ) {
                    accountSource = c.Hidden_Lead_Source__c;
                    break;
                }
            }
            if(accountSource != '') {
                objAccount.AccountSource = accountSource;
                accountsToUpdate.add(objAccount);
                
            }
        }
        system.debug('*****accountsToUpdate' + accountsToUpdate);
        if(! accountsToUpdate.isEmpty()) {
            //we dont want to run DMA_UpdateOrAddDMATrigger so lets set the helper flag to false
            ACC_Static_Helper.runDMATrigger = false;
            List<Database.SaveResult> dsrs = Database.update( accountsToUpdate, false );
            Integer index = 0;
            for(Database.SaveResult dsr : dsrs){
                if(!dsr.isSuccess()){
                    String errMsg = dsr.getErrors()[0].getMessage();
                    errorMap.put(accountsToUpdate[index].Id, errMsg);
                    IdToSObjectMap.put(accountsToUpdate[index].Id, accountsToUpdate[index]);
                }
                index++;
            }
        }
    }
    
    global void finish(Database.BatchableContext BC){
        //Send an email to the User after your batch completes 
       if(!errorMap.isEmpty()){
            AsyncApexJob a = [SELECT id, ApexClassId,
                       JobItemsProcessed, TotalJobItems,
                       NumberOfErrors, CreatedBy.Email
                       FROM AsyncApexJob
                       WHERE id = :BC.getJobId()];
            String body = 'Your batch job '
             + 'Contact_Rectify_Lead_Account_Source_BTCH '
             + 'has finished. \n' 
             + 'There were '
             + errorMap.size()
             + ' errors. Please find the error list attached to the Case.';
 
            // Creating the CSV file
            String finalstr = 'Id, Name, Error \n';
            String subject = 'Account - Contact_Rectify_Lead_Account_Source_BTCH Error List';
            String attName = 'Account Errors.csv';
            for(Id id  : errorMap.keySet()){
                string err = errorMap.get(id);
                Account acct = (Account) IdToSObjectMap.get(id);
                string recordString = '"'+id+'","'+acct.Name+'","'+err+'"\n';
                finalstr = finalstr +recordString;
            } 
 
            // Define the email
            Messaging.SingleEmailMessage email = new Messaging.SingleEmailMessage(); 
 
            // Create the email attachment    
            Messaging.EmailFileAttachment efa = new Messaging.EmailFileAttachment();
            efa.setFileName(attName);
            efa.setBody(Blob.valueOf(finalstr));
 
            // Sets the paramaters of the email
            email.setSubject( subject );
            email.setToAddresses( new String[] {a.CreatedBy.Email} );
            email.setPlainTextBody( body );
            email.setFileAttachments(new Messaging.EmailFileAttachment[] {efa});
 
            // Sends the email
            Messaging.SendEmailResult [] r = 
                Messaging.sendEmail(new Messaging.SingleEmailMessage[] {email});   
            }
    }
}