@isTest(seealldata=false)
public class Contact_Rectify_Lead_Account_Source_TST {
    static testMethod void myTestBTCH() {
        Account testAccount = UTIL_TestUtil.generateAccount();
        testAccount.Name = 'Opp 6m Rollup Test';
        testAccount.Type = 'Advertiser';
        insert testAccount;
        
        //Insert Contact
        Contact testCon = UTIL_TestUtil.createContact(testAccount.Id);
        testCon.Hidden_Lead_Source__c = 'Test';
        update testCon;
        Test.startTest();
        	Contact_Rectify_Lead_Account_Source_BTCH btch = new Contact_Rectify_Lead_Account_Source_BTCH();
        	btch.errorMap.put(testAccount.Id, 'errMsg test');
        	btch.IdToSObjectMap.put(testAccount.Id, testAccount);
			Database.executeBatch(btch,1);
        Test.stopTest();
    }
}