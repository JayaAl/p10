public class Content_AgreementTriggerHandler {

	public Map<String, String> mapCurrencyTextTiISO{get;set;}

	public void recalculateRateCardData(List<Content_Agreement__c> theList){
		// 1st gather Rate card Ids to be queried
		Set<Id> setRateCardIds = gatherRateCardIDs(theList);

		// 2nd, create a Map of Rate Card Ids to the Rate Cards
		Map<Id, Rate_Card__c> mapIdtoRC = gatherMapIdtoRC(setRateCardIds);
		
		// 3rd, loop back through the Content_Agreement__c records to populate the values appropriately
		theList = updateCAFromRateCardMap(theList, mapIdtoRC);
	}
    
    public Set<Id> gatherRateCardIDs(List<Content_Agreement__c> theList){
        Set<Id> setRateCardIds = new Set<Id>();
		for(Content_Agreement__c ca:theList){
			setRateCardIds.add(ca.Rate_Card__c);
		}
        return setRateCardIds;
    }

    public Map<Id, Rate_Card__c> gatherMapIdtoRC(set<Id> setIds){
        Map<Id, Rate_Card__c> mapIdtoRC = new Map<Id, Rate_Card__c>();
		for(Rate_Card__c r:[
			SELECT Id, Name,
				AUD_Rates_Currency__c, NZD_Rates_Currency__c, US_Rates_Currency__c, 
				Compensable_Play_Seconds_AUS__c, Compensable_Play_Seconds_NZD__c, Compensable_Play_Seconds_US__c,
				Deduction_3PP_Bundle_AUS__c, Deduction_3PP_Bundle_NZD__c, Deduction_3PP_Bundle_US__c, 
				Deduction_App_Store_AUS__c, Deduction_App_Store_NZD__c, Deduction_App_Store_US__c, 
				Deduction_Carrier_Billing_AUS__c, Deduction_Carrier_Billing_NZD__c, Deduction_Carrier_Billing_US__c, 
				Deduction_Other_AUS__c, Deduction_Other_NZD__c, Deduction_Other_US__c, 
				Deduction_Sales_Cost_AUS__c, Deduction_Sales_Cost_NZD__c, Deduction_Sales_Cost_US__c, 
				Tier_1_AUS__c, Tier_1_NZD__c, Tier_1_US__c, 
				Tier_1_PPR_AUS__c, Tier_1_PPR_NZD__c, Tier_1_PPR_US__c, 
				Tier_1_PPR_Interactive_AUS__c, Tier_1_PPR_Interactive_NZD__c, Tier_1_PPR_Interactive_US__c, 
				Tier_2_AUS__c, Tier_2_NZD__c, Tier_2_US__c, 
				Tier_2_PSM_AUS__c, Tier_2_PSM_NZD__c, Tier_2_PSM_US__c, 
				Tier_2_Rev_Share_AUS__c, Tier_2_Rev_Share_NZD__c, Tier_2_Rev_Share_US__c, 
				Tier_3_AUS__c, Tier_3_NZD__c, Tier_3_US__c, 
				Tier_3_PSM_AUS__c, Tier_3_PSM_NZD__c, Tier_3_PSM_US__c, 
				Tier_3_Rev_Share_AUS__c, Tier_3_Rev_Share_NZD__c, Tier_3_Rev_Share_US__c
			FROM  Rate_Card__c
			WHERE Id in : setIds
		]){
			mapIdtoRC.put(r.Id,r);
		}
        return mapIdtoRC;
    }

    public List<Content_Agreement__c> updateCAFromRateCardMap(List<Content_Agreement__c> theList, Map<Id, Rate_Card__c> mapIdtoRC){
    	for(Content_Agreement__c ca:theList){
			if(ca.Rate_Card__c!=NULL && mapIdtoRC.containsKey(ca.Rate_Card__c)){
				Rate_Card__c rc = mapIdtoRC.get(ca.Rate_Card__c);
				ca.Calculate_Rate_Card__c = false;
				if(ca.Rate_Card_territory__c=='Australia'){
					ca.Compensable_Play_Seconds__c = rc.Compensable_Play_Seconds_AUS__c;
					ca.Deduction_3PP_Bundle__c = rc.Deduction_3PP_Bundle_AUS__c;
					ca.Deduction_App_Store__c = rc.Deduction_App_Store_AUS__c; 
					ca.Deduction_Carrier_Billing__c = rc.Deduction_Carrier_Billing_AUS__c;
					ca.Deduction_Other__c = rc.Deduction_Other_AUS__c;
					ca.Deduction_Sales_Cost__c = rc.Deduction_Sales_Cost_AUS__c;
					ca.Tier_1__c = rc.Tier_1_AUS__c;
					ca.Tier_1_PPR__c = rc.Tier_1_PPR_AUS__c;
					ca.Tier_1_PPR_Interactive__c = rc.Tier_1_PPR_Interactive_AUS__c;
					ca.Tier_2__c = rc.Tier_2_AUS__c;
					ca.Tier_2_PSM__c = rc.Tier_2_PSM_AUS__c;
					ca.Tier_2_Rev_Share__c = rc.Tier_2_Rev_Share_AUS__c;
					ca.Tier_3__c = rc.Tier_3_AUS__c;
					ca.Tier_3_PSM__c = rc.Tier_3_PSM_AUS__c;
					ca.Tier_3_Rev_Share__c = rc.Tier_3_Rev_Share_AUS__c;
				} else if(ca.Rate_Card_territory__c=='New Zealand'){
					ca.Compensable_Play_Seconds__c = rc.Compensable_Play_Seconds_NZD__c;
					ca.Deduction_3PP_Bundle__c = rc.Deduction_3PP_Bundle_NZD__c;
					ca.Deduction_App_Store__c = rc.Deduction_App_Store_NZD__c; 
					ca.Deduction_Carrier_Billing__c = rc.Deduction_Carrier_Billing_NZD__c;
					ca.Deduction_Other__c = rc.Deduction_Other_NZD__c;
					ca.Deduction_Sales_Cost__c = rc.Deduction_Sales_Cost_NZD__c;
					ca.Tier_1__c = rc.Tier_1_NZD__c;
					ca.Tier_1_PPR__c = rc.Tier_1_PPR_NZD__c;
					ca.Tier_1_PPR_Interactive__c = rc.Tier_1_PPR_Interactive_NZD__c;
					ca.Tier_2__c = rc.Tier_2_NZD__c;
					ca.Tier_2_PSM__c = rc.Tier_2_PSM_NZD__c;
					ca.Tier_2_Rev_Share__c = rc.Tier_2_Rev_Share_NZD__c;
					ca.Tier_3__c = rc.Tier_3_NZD__c;
					ca.Tier_3_PSM__c = rc.Tier_3_PSM_NZD__c;
					ca.Tier_3_Rev_Share__c = rc.Tier_3_Rev_Share_NZD__c;
				} else if(ca.Rate_Card_territory__c=='United States of America'){
					ca.Compensable_Play_Seconds__c = rc.Compensable_Play_Seconds_US__c;
					ca.Deduction_3PP_Bundle__c = rc.Deduction_3PP_Bundle_US__c;
					ca.Deduction_App_Store__c = rc.Deduction_App_Store_US__c; 
					ca.Deduction_Carrier_Billing__c = rc.Deduction_Carrier_Billing_US__c;
					ca.Deduction_Other__c = rc.Deduction_Other_US__c;
					ca.Deduction_Sales_Cost__c = rc.Deduction_Sales_Cost_US__c;
					ca.Tier_1__c = rc.Tier_1_US__c;
					ca.Tier_1_PPR__c = rc.Tier_1_PPR_US__c;
					ca.Tier_1_PPR_Interactive__c = rc.Tier_1_PPR_Interactive_US__c;
					ca.Tier_2__c = rc.Tier_2_US__c;
					ca.Tier_2_PSM__c = rc.Tier_2_PSM_US__c;
					ca.Tier_2_Rev_Share__c = rc.Tier_2_Rev_Share_US__c;
					ca.Tier_3__c = rc.Tier_3_US__c;
					ca.Tier_3_PSM__c = rc.Tier_3_PSM_US__c;
					ca.Tier_3_Rev_Share__c = rc.Tier_3_Rev_Share_US__c;
				}
			}
		}
		return theList;
    }
}