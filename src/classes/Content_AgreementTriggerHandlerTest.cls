@isTest
private class Content_AgreementTriggerHandlerTest {
	
    
    static testMethod void testInsertCA(){
		Content_Agreement__c ca = createContentAgreement();
	}
    
	/*
	static testMethod void testCurrencyMap(){
		Content_AgreementTriggerHandler testCon = new Content_AgreementTriggerHandler();
		System.assert(!testCon.mapCurrencyTextTiISO.isEmpty(),'mapCurrencyTextTiISO Empty');
		System.assert(testCon.mapCurrencyTextTiISO.containsKey('USD'), 'mapCurrencyTextTiISO does not contain Key "USD"');
	}
	*/
    
    static testMethod void testUpdateCA(){
		Rate_Card__c rc = createRateCard();
        Content_Agreement__c ca = createContentAgreement();
        ca.Rate_Card__c = rc.Id;
        update ca;
        
	}
    
    static testMethod void testInsertCAWIthRC(){
		Rate_Card__c rc = createRateCard();
        Content_Agreement__c ca = generateContentAgreement();
        ca.Rate_Card__c = rc.Id;
        insert ca;
	}

    static testMethod void testUpdateCACurrency(){
		Rate_Card__c rc = createRateCard();
        Content_Agreement__c ca = generateContentAgreement();
        ca.Rate_Card__c = rc.Id;
        insert ca;
        
        ca.CurrencyIsoCode = 'NZD';
        update ca;
            
        ca.CurrencyIsoCode = 'AUD';
        update ca;
        
        ca.CurrencyIsoCode = 'HKD';
        update ca;
	}

    public static Content_Agreement__c createContentAgreement(){
        Content_Agreement__c ca = generateContentAgreement();
        insert ca;
        return ca;
    }
    public static Content_Agreement__c generateContentAgreement(){
		return new Content_Agreement__c(
            Compensable_Play_Seconds__c	= '30',
            CurrencyIsoCode = 'USD',
            Deduction_3PP_Bundle__c = Math.round( Math.random() * 10 ) / 100,
            Deduction_App_Store__c = UTIL_TestUtil.generateRandomString(255),
            Deduction_Carrier_Billing__c = Math.round( Math.random() * 10 ) / 100,
            Deduction_Other__c = UTIL_TestUtil.generateRandomString(255),
            Deduction_Sales_Cost__c = Math.round( Math.random() * 10 ) / 100,
            Rate_Card__c = null,
            Tier_1__c = UTIL_TestUtil.generateRandomString(255),
            Tier_1_PPR__c = Math.round( Math.random() * 10000 ) / 1000,
            Tier_1_PPR_Interactive__c = Math.round( Math.random() * 10000 ) / 1000,
            Tier_2__c = UTIL_TestUtil.generateRandomString(255),
            Tier_2_PSM__c = Math.round( Math.random() * 10000 ) / 1000,
            Tier_2_Rev_Share__c = Math.round( Math.random() * 10 ) / 100,
            Tier_3__c = UTIL_TestUtil.generateRandomString(255),
            Tier_3_PSM__c = Math.round( Math.random() * 10000 ) / 1000,
            Tier_3_Rev_Share__c = Math.round( Math.random() * 10 ) / 100
        );
	} 
    
    public static Rate_Card__c createRateCard(){
        Rate_Card__c rc = generateRateCard();
        insert rc;
        return rc;
    }
    public static Rate_Card__c generateRateCard(){
		return new Rate_Card__c(
            AUD_Rates_Currency__c = 'Australian Dollar',
            NZD_Rates_Currency__c = 'New Zealand Dollar', 
            US_Rates_Currency__c = 'U.S. Dollar', 
            Compensable_Play_Seconds_AUS__c = '15', 
            Compensable_Play_Seconds_NZD__c = '25', 
            Compensable_Play_Seconds_US__c = '35',
            Deduction_3PP_Bundle_AUS__c = Math.round( Math.random() * 10 ) / 100,
            Deduction_3PP_Bundle_NZD__c = Math.round( Math.random() * 10 ) / 100,
            Deduction_3PP_Bundle_US__c = Math.round( Math.random() * 10 ) / 100,
            Deduction_App_Store_AUS__c = UTIL_TestUtil.generateRandomString(255),
            Deduction_App_Store_NZD__c = UTIL_TestUtil.generateRandomString(255),
            Deduction_App_Store_US__c = UTIL_TestUtil.generateRandomString(255),
            Deduction_Carrier_Billing_AUS__c = Math.round( Math.random() * 10 ) / 100,
            Deduction_Carrier_Billing_NZD__c = Math.round( Math.random() * 10 ) / 100,
            Deduction_Carrier_Billing_US__c = Math.round( Math.random() * 10 ) / 100,
            Deduction_Other_AUS__c = UTIL_TestUtil.generateRandomString(255),
            Deduction_Other_NZD__c = UTIL_TestUtil.generateRandomString(255),
            Deduction_Other_US__c = UTIL_TestUtil.generateRandomString(255),
            Deduction_Sales_Cost_AUS__c = Math.round( Math.random() * 10000 ) / 1000,
            Deduction_Sales_Cost_NZD__c = Math.round( Math.random() * 10000 ) / 1000,
            Deduction_Sales_Cost_US__c = Math.round( Math.random() * 10000 ) / 1000,
            Tier_1_AUS__c = UTIL_TestUtil.generateRandomString(255),
            Tier_1_NZD__c = UTIL_TestUtil.generateRandomString(255),
            Tier_1_US__c = UTIL_TestUtil.generateRandomString(255),
            Tier_1_PPR_AUS__c = Math.round( Math.random() * 10000 ) / 1000,
            Tier_1_PPR_NZD__c = Math.round( Math.random() * 10000 ) / 1000,
            Tier_1_PPR_US__c = Math.round( Math.random() * 10000 ) / 1000,
            Tier_1_PPR_Interactive_AUS__c = Math.round( Math.random() * 10000 ) / 1000,
            Tier_1_PPR_Interactive_NZD__c = Math.round( Math.random() * 10000 ) / 1000,
            Tier_1_PPR_Interactive_US__c = Math.round( Math.random() * 10000 ) / 1000,
            Tier_2_AUS__c = UTIL_TestUtil.generateRandomString(255),
            Tier_2_NZD__c = UTIL_TestUtil.generateRandomString(255),
            Tier_2_US__c = UTIL_TestUtil.generateRandomString(255),
            Tier_2_PSM_AUS__c = Math.round( Math.random() * 10000 ) / 1000,
            Tier_2_PSM_NZD__c = Math.round( Math.random() * 10000 ) / 1000,
            Tier_2_PSM_US__c = Math.round( Math.random() * 10000 ) / 1000,
            Tier_2_Rev_Share_AUS__c = Math.round( Math.random() * 10000 ) / 1000,
            Tier_2_Rev_Share_NZD__c = Math.round( Math.random() * 10000 ) / 1000,
            Tier_2_Rev_Share_US__c = Math.round( Math.random() * 10000 ) / 1000,
            Tier_3_AUS__c = UTIL_TestUtil.generateRandomString(255),
            Tier_3_NZD__c = UTIL_TestUtil.generateRandomString(255),
            Tier_3_US__c = UTIL_TestUtil.generateRandomString(255),
            Tier_3_PSM_AUS__c = Math.round( Math.random() * 10000 ) / 1000,
            Tier_3_PSM_NZD__c = Math.round( Math.random() * 10000 ) / 1000,
            Tier_3_PSM_US__c = Math.round( Math.random() * 10000 ) / 1000,
            Tier_3_Rev_Share_AUS__c = Math.round( Math.random() * 10000 ) / 1000,
            Tier_3_Rev_Share_NZD__c = Math.round( Math.random() * 10000 ) / 1000,
            Tier_3_Rev_Share_US__c = Math.round( Math.random() * 10000 ) / 1000
        );
    }
}