public with sharing class Content_ManageCOGSContent extends CONTENT_ManageContent {
    
    public Content_ManageCOGSContent(ApexPages.StandardController controller){
        super(controller);                          
    }
    
    public override void initializeWorkspaceList(){  
        Map<String,Workspace_COGS__c> workspaceIdMap = Workspace_COGS__c.getAll();      
        this.workspaceOptions = new List<SelectOption>();
        //List<ContentWorkspace> workspaceList = new List<ContentWorkspace>([Select Id,Name from ContentWorkspace]);
        for (String workspaceId : workspaceIdMap.keySet()){
            this.workspaceOptions.add(new SelectOption(workspaceIdMap.get(workspaceId).Workspace_ID__c,workspaceIdMap.get(workspaceId).Name));
        }
    }

    // populate field map
    public override Map<String,String> getFieldMapForFolder(){
        /* COGS has no direct relationship to Opportunities, as a result all normally used Opportunity references will fail */
        Map<String,String> fieldMap = new Map<String,String>();
        /*
        FolderFieldMap__c folderFieldMap = FolderFieldMap__c.getValues(this.selectedFolder);
        if(folderFieldMap.Field_Map1__c != null){
            List<String> splitList = folderFieldMap.Field_Map1__c.split(',');
            for(String split : splitList){
                fieldMap.put(split.split('=')[0],split.split('=')[1]);
            }
        }
        if(folderFieldMap.Field_Map2__c != null){
            List<String>  splitList = folderFieldMap.Field_Map2__c.split(',');
            for(String split : splitList){
                fieldMap.put(split.split('=')[0],split.split('=')[1]);
            }
        }
        */
        fieldMap.put('Id','COGS__c');
        return fieldMap;
    }
    
    // This method is for uploading the Content and refreshing the content list
    public override void upload(){
        if(this.selectedWorkspaceID == null || this.selectedFolder == null || this.file == null){
            ApexPages.Message myMsg = new ApexPages.Message(ApexPages.Severity.ERROR, 'Please fill in all fields');
            ApexPages.addMessage(myMsg);            
            return;
        }
       super.upload();    
    }   
    
    public override String getLookupFieldName(){
        return 'COGS__c';
    }

}