public with sharing class CreateBulkGiftCodeOpptyCtrlr{
    
    // TODO : Use Transient variables
    
    public Opportunity opptyObj {get;set;}    
    public OpportunityLineItem oliToInsert {get;set;}
    public List<Selectoption> listAllProd{get;set;}
    
    // used to store selected values 
    public transient String accountNameVar {get;set;}    
    public transient String contactIdVar {get;set;}    
    public transient String accountIdVar {get;set;}

    // used for Promo code address
    public transient String billingStreetStr {get;set;}
    public transient String billingStateStr {get;set;}
    public transient String billingCityStr {get;set;}
    public transient String billingPostalCodeStr {get;set;}
    public transient String billingCountryStr {get;set;}
    
    
    public Map<Id,String> productIdToNameMap {get;set;}

    private Map<Id,Decimal> productPriceMap;
    public Boolean isGiftCodeType{get;set;}
    public Integer counterVariable {get;set;}   
    public List<OLIWrapper> wrapperLst {get;set;}
    public Integer recordToRemove {get;set;}

    public CreateBulkGiftCodeOpptyCtrlr(){
        // intialise variables
        initialisevariables();
        // get the list of products        
        getListProd();
    }
    public void RemoveOLIAction(){
        List<OLIWrapper> wrapperIter = new List<OLIWrapper>();
        for(OLIWrapper wrapperVar : wrapperLst){

            if(wrapperVar.seqNo != recordToRemove){
                wrapperIter.add(wrapperVar);
            }
        }

        wrapperLst = wrapperIter;
    }
    
    private void initialisevariables(){
        opptyObj = new Opportunity(); // Oppty Object to insert
        opptyObj.RecordTypeId = Schema.SObjectType.Opportunity.getRecordTypeInfosByName().get(System.Label.Default_Record_Type_for_Promo_Code).getRecordTypeId();
        oliToInsert = new OpportunityLineItem(); // Oppty Line item used for binding to input data         
        isGiftCodeType = false;
        counterVariable = 0;
        wrapperLst = new List<OLIWrapper>();
        wrapperLst.add(new OLIWrapper(counterVariable,oliToInsert));        
    }
    
    /*
    *@Author : AB
    *@description : Used to insert Oppty and related line items 
    */
    public PageReference createOpptyAction(){
       try{ 
           
           if(accountIdVar == null || accountIdVar == '' ){
               ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.WARNING,'Please Select Account.'));       
               return null;        
           }
           
           /*if(contactIdVar == null || contactIdVar == '' ){
               ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.WARNING,'Please Select Contact.'));               
               return null;               
           }*/
           
           if(wrapperLst.isEmpty() || wrapperLst.size()<=1){
              ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.WARNING,'No Product Added, Please add Products before submitting.'));               
               return null;               
           }

           // insert Oppty 
           opptyObj.Name = getOpptyName(); 
           opptyObj.AccountId = accountIdVar;
          // opptyObj.Primary_Contact__c = contactIdVar;
           opptyObj.StageName = System.Label.Default_Stage_for_Promo_Codes; // CS
           opptyObj.CloseDate = Date.Today();           
         //  opptyObj.Billing_Address__c = billingStreetStr +','+ billingStateStr +','+ billingCountryStr +','+ billingCityStr +','+ billingPostalCodeStr; 
           
           Savepoint sp = Database.setSavepoint();

           insert opptyObj; 

           // create Line items 
           oliToInsert = new OpportunityLineItem();
           oliToInsert.OpportunityId = opptyObj.id;       
           insertOLIRecords(opptyObj.id,sp);
           invokeApprovalProcess(opptyObj.id); 
           return redirectToDetailPage(opptyObj.id);       

       }catch(Exception ex){
         if(ex.getMessage().contains('FIELD_CUSTOM_VALIDATION_EXCEPTION')){
             String errorMsg = ex.getMessage().substring(ex.getMessage().indexOf('FIELD_CUSTOM_VALIDATION_EXCEPTION'),ex.getMessage().indexOf(': ['));  
             ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.WARNING,errorMsg));             
         }else{
                    ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR,ex.getMessage()+ex.getSTackTraceString()));                        
         }

       }

       return null;
    }
    
    @testVisible
    private PageReference redirectToDetailPage(Id opptyId){
        
        PageReference pg = new PageReference('/'+opptyId);
        pg.setRedirect(true);        
        return pg;
    }
    
    /*@Author : AB
    *@Description : Private method to get OPpty name Auto - AccountName + Promo Code Type + uniqueId
    */
    private String getOpptyName(){

        String opptyName = '';
        // Account name
        opptyName += (!String.isBlank(accountNameVar)) ? accountNameVar : ' '; 
        // Seperater
        opptyName += '-';
        // Promo Code type
        opptyName += (!String.isBlank(opptyObj.Promo_Code_type__c)) ? opptyObj.Promo_Code_type__c : ' ';
        // Seperater
        opptyName += '-';
        //Count
        opptyName += (!String.isBlank(accountIdVar)) ? String.valueOf([Select Count() from Opportunity where AccountId =: accountIdVar] + 1) : ' ';

        system.debug(opptyName);
        
        return opptyName;

    }

    private static String getFilterConditionForProducts(){
        Integer index = 0;
        String filterCondnStr = '';

        for(String strVar : System.Label.Gift_Promo_Code_Filter.Split(',')){            
            if(!String.isBlank(strVar)){
                filterCondnStr +=  (index ++ != 0) ? ' OR' : '';
                strVar ='\'' + strVar + '\'';
                filterCondnStr += (' Name Like '+strVar);            
            }
        }

        return filterCondnStr;
    }

    /*
    *@Author : AB
    * @Description : Used to get the ist of Products from Pricebook
    */
    public void getListProd(){
     
        listAllProd = new List<Selectoption>();        
        productIdToNameMap = new Map<Id,String>();
        productPriceMap = new Map<Id,Decimal>();

        String SOQLStr = 'SELECT p.Id, p.Name, UnitPrice FROM PricebookEntry p WHERE IsActive=true AND ';        
        SOQLStr += (!String.isBlank(CreateBulkGiftCodeOpptyCtrlr.getFilterConditionForProducts()))?'('+CreateBulkGiftCodeOpptyCtrlr.getFilterConditionForProducts()+')':'';
        SOQLStr += 'AND Pricebook2.Name = '+'\''+System.Label.Default_Price_Book_for_Promo_Codes+'\'';
        SOQLStr += 'AND CurrencyIsoCode = '+'\''+System.Label.Default_Currency_for_Promo_Codes+'\'';
        SOQLStr += 'ORDER BY Name desc';
        
        for(PricebookEntry objPE: (List<PricebookEntry>) Database.query(SOQLStr)){
            productPriceMap.put(objPE.Id,objPE.UnitPrice);
            listAllProd.add(new SelectOption(objPE.Id,objPE.Name));
            productIdToNameMap.put(objPE.Id,objPE.Name);
        }
    }
    
    public PageReference AddOLIAction(){
        if(oliToInsert.Quantity <= 0){
               ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.WARNING,'Quantity should be greater than 0.'));       
               return null;        
        }
        getListProd();
        counterVariable++;
        system.debug(counterVariable);
        oliToInsert.UnitPrice = productPriceMap.get(oliToInsert.PricebookEntryId);
        
        oliToInsert = new OpportunityLineItem(); 
        wrapperLst.add(new OLIWrapper(counterVariable,oliToInsert));
        return null;
    }
    
    public void insertOLIRecords(Id OpptyId,Savepoint sp){
        List<OpportunityLineItem> oliLstToInsert = new List<OpportunityLineItem>();
        for(OLIWrapper lineItemVar : wrapperLst){
            if(lineItemVar.oliVar.Quantity != 0 ){
                lineItemVar.oliVar.OpportunityId = OpptyId;
                oliLstToInsert.add(lineItemVar.oliVar);    
            }            
        }

        try{
            if(!oliLstToInsert.isEmpty())
                insert oliLstToInsert;
        }catch(DMLException dmlExc){
            Database.rollback(sp);
            throw dmlExc;
        }        
    }
    
    public void checkOptionalFieldsAction(){        
        isGiftCodeType = (opptyObj.Promo_Code_type__c == 'Gift') ? true : false;
    }
    
    @RemoteAction
    public static List<SelectListWrapper> getListProdAction(String promoCodeType){
        
        system.debug('getListProdAction ::: '+promoCodeType);
        List<SelectListWrapper> listAllProd = new List<SelectListWrapper>();                

        String SOQLStr = 'SELECT p.Id, p.Name, UnitPrice FROM PricebookEntry p WHERE IsActive=true AND ';        
        SOQLStr += (!String.isBlank(getFilterConditionForProducts()))?'('+getFilterConditionForProducts()+')':'';
        SOQLStr += 'AND Pricebook2.Name = '+'\''+System.Label.Default_Price_Book_for_Promo_Codes+'\'';
        SOQLStr += 'AND CurrencyIsoCode = '+'\''+System.Label.Default_Currency_for_Promo_Codes+'\'';
        SOQLStr += 'ORDER BY Name ASC';
        
        String promoCodeFilter = 'Plus Gift';

        if(promoCodeType.equalsIgnoreCase('Comp')){
          promoCodeFilter = 'Plus Comp';
        }else if(promoCodeType.equalsIgnoreCase('Trial')){
          promoCodeFilter = 'Trial';
        }


        for(PricebookEntry objPE: (List<PricebookEntry>) Database.query(SOQLStr)){
            if(objPE.Name.containsIgnoreCase(promoCodeFilter))
                listAllProd.add(new SelectListWrapper(objPE.Name,objPE.Id));            
        }

        return listAllProd;
    }

    
    public void invokeApprovalProcess(Id opptyId){

       // try{

            // Create an approval request for the account
            Approval.ProcessSubmitRequest req1 = new Approval.ProcessSubmitRequest();
            req1.setComments('Promo Code request for Approval.');
            req1.setObjectId(opptyId);

            // Submit on behalf of a specific submitter
            req1.setSubmitterId(Userinfo.getUserId()); 

            // Submit the record to specific process
            req1.setProcessDefinitionNameOrId('Promo_Code4');

            // Submit the approval request for the account
            Approval.ProcessResult result = Approval.process(req1);

       /* }catch(Exception ex){
            system.debug(ex.getMessage()+ex.getStacktraceString());
        }*/
        
    }

    public class OLIWrapper{
        public Integer seqNo {get;set;}
        public OpportunityLineItem oliVar {get;set;}

        public OLIWrapper(Integer seqNoInt, OpportunityLineItem oliVarParam){
            seqNo = seqNoInt;
            oliVar = oliVarParam;
            oliVar.Quantity = 0;
            oliVar.ServiceDate = Date.Today();
            oliVar.End_Date__c = Date.Today();
        }

    }

    public class SelectListWrapper{
        public String SelectListLabel;
        public String SelectListValue;

        public SelectListWrapper(String labelStr,String valueStr){
            SelectListLabel = labelStr;
            SelectListValue = valueStr;
        }
    }
}