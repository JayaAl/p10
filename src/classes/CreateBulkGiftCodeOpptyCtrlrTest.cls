@isTest(seeAlldata = false)
public class CreateBulkGiftCodeOpptyCtrlrTest{
    public static Account testAccount;
    public static Contact testContact;

    public static void setUpdata(){

      testAccount = UTIL_TestUtil.createAccount();
      testContact = UTIL_TestUtil.createContact(testAccount.id);

      Product2 prod = new Product2(Name = 'Gift One - 6 Month', 
                                   Family = 'Hardware');
      insert prod;

      prod = new Product2(Name = 'Comp One - 6 Month', 
                                   Family = 'Hardware');
      insert prod;

      prod = new Product2(Name = 'Trial One - 6 Month', 
                                   Family = 'Hardware');
      insert prod;
            
      Id pricebookId = Test.getStandardPricebookId();
      
      PricebookEntry standardPrice = new PricebookEntry(
          Pricebook2Id = pricebookId, Product2Id = prod.Id,
          UnitPrice = 10000, IsActive = true, CurrencyIsoCode = System.Label.Default_Currency_for_Promo_Codes);

      insert standardPrice;

    }

    public static testMethod void testConstructor(){
        
        Test.starttest();
        CreateBulkGiftCodeOpptyCtrlr ctrlrObj = new CreateBulkGiftCodeOpptyCtrlr();
        Test.stopTest();
    
    }

    public static testMethod void testcreateOpptyActionNegative(){
        setUpdata();
        Test.startTest();
        CreateBulkGiftCodeOpptyCtrlr ctrlrObj = new CreateBulkGiftCodeOpptyCtrlr();
        ctrlrObj.accountIdVar = null;
        ctrlrObj.contactIdVar = testContact.Id;
        try{
          ctrlrObj.createOpptyAction();  
        }catch(Exception ex){
          system.assert(ex.getMessage() != 'Please Select Account.');
        }        
        
        ctrlrObj = new CreateBulkGiftCodeOpptyCtrlr();
        ctrlrObj.accountIdVar = testAccount.Id;
        ctrlrObj.contactIdVar = null;
        try{
          ctrlrObj.createOpptyAction();  
        }catch(Exception ex){
          system.assert(ex.getMessage() != 'Please Select Contact.');
        }        

        ctrlrObj = new CreateBulkGiftCodeOpptyCtrlr();
        ctrlrObj.accountIdVar = testAccount.Id;
        ctrlrObj.contactIdVar = testContact.Id;
        ctrlrObj.opptyObj.Discount_Amount__c = 10;                
        try{
          ctrlrObj.createOpptyAction();  
        }catch(Exception ex){
          system.assert(ex.getMessage() != 'Deposit Reason should be specified.');
        }        

        Test.stopTest();
    
    }
    public static testMethod void testgetListProdAction(){
      setUpdata();
      Test.startTest();
        CreateBulkGiftCodeOpptyCtrlr.getListProdAction('Gift');
        CreateBulkGiftCodeOpptyCtrlr.getListProdAction('Comp');
        CreateBulkGiftCodeOpptyCtrlr.getListProdAction('Trial');
      Test.stopTest();
    }

    public static testMethod void testcreateOpptyAction(){
        setUpdata();
        
          /*List<OLIWrapper> wrapperLst = new List<OLIWrapper>();
          OLIWrapper wrapperVar = new OLIWrapper();
          wrapperVar.seqNo = 1;
          wrapperVar.oliVar = */
          CreateBulkGiftCodeOpptyCtrlr ctrlrObj = new CreateBulkGiftCodeOpptyCtrlr();
          ctrlrObj.accountIdVar = testAccount.Id;
          ctrlrObj.contactIdVar = testContact.Id;
          ctrlrObj.oliToInsert.Quantity = 5;        
          ctrlrObj.AddOLIAction();
          try{
              Test.startTest();
                ctrlrObj.createOpptyAction();
              Test.stopTest(); 
          }catch(Exception ex){

          }
          ctrlrObj.redirectToDetailPage(testAccount.Id);
          ctrlrObj.checkOptionalFieldsAction();
          ctrlrObj.RemoveOLIAction();
          CreateBulkGiftCodeOpptyCtrlr.SelectListWrapper wrapper = new CreateBulkGiftCodeOpptyCtrlr.SelectListWrapper('','');
         //system.assert([Select count() from Opportunity] != 1);
        
           
    }

    public static testMethod void testcreateOpptyAction2(){
        setUpdata();
        Test.startTest();
          /*List<OLIWrapper> wrapperLst = new List<OLIWrapper>();
          OLIWrapper wrapperVar = new OLIWrapper();
          wrapperVar.seqNo = 1;
          wrapperVar.oliVar = */

          try{
            CreateBulkGiftCodeOpptyCtrlr ctrlrObj = new CreateBulkGiftCodeOpptyCtrlr();
            ctrlrObj.accountIdVar = testAccount.Id;
            ctrlrObj.contactIdVar = testContact.Id;
            
            ctrlrObj.AddOLIAction();
            ctrlrObj.createOpptyAction();
            ctrlrObj.redirectToDetailPage(ctrlrObj.opptyObj.Id);
            ctrlrObj.checkOptionalFieldsAction();
            ctrlrObj.invokeApprovalProcess(ctrlrObj.opptyObj.Id);
            ctrlrObj.RemoveOLIAction();
            CreateBulkGiftCodeOpptyCtrlr.SelectListWrapper wrapper = new CreateBulkGiftCodeOpptyCtrlr.SelectListWrapper('','');
         //system.assert([Select count() from Opportunity] != 1);
          }catch(Exception ex){
            
          }
        Test.stopTest();    
    }

     static testMethod void testOpportunityListController(){
        
            
       try{
            
            
            ID ProfileID = [ Select id from Profile where name = 'FinancialForce custom portal profile'].id;
            Account A1 = new Account(Name = 'Test Account');
            insert A1;
            
            
            List<Contact> ContactList = new List<Contact>();
            Contact C1 = new Contact( AccountID = A1.id, FirstName = 'Test', LastName = 'User', 
            email = 'test-user@fakeemail.com' );
            insert C1;
            Test.startTest(); 
            sObject opp;
          
              opp = Schema.getGlobalDescribe().get('Opportunity').newSObject() ;  
              opp.put('name' , 'Test Opp') ;  
              opp.put('Primary_Billing_Contact__c' , C1.Id) ;  
              opp.put('AccountId' , A1.Id) ;  
              opp.put('Probability' , 100) ;
              opp.put('StageName' , 'Closed Won') ;    
              opp.put('CloseDate' , system.today()) ;                          
              insert opp;
           
            sObject sObj = Schema.getGlobalDescribe().get('IO_Detail__c').newSObject() ;  
            sObj.put('Opportunity__c' , opp.Id) ;  
            sObj.put('PortalPaymentStatus__c' , 'Paid') ;
            sObj.put('Payment_Terms_Preferred__c' , 'Pre-Pay') ;
            
            insert sObj ;
            
            Test.stopTest();
            
            User u1 = new User( email='test-user@fakeemail.com', contactid = c1.id, profileid = profileid, 
                    UserName='test-user@fakeemail.com', alias='tuser1', CommunityNickName='tuser1', 
            TimeZoneSidKey='America/New_York', LocaleSidKey='en_US', EmailEncodingKey='ISO-8859-1', 
            LanguageLocaleKey='en_US', FirstName = 'Test', LastName = 'User' );
            system.runAs(new User(Id = UserInfo.getUserId())) {
                insert u1;
            }
            system.runAs(u1) {
                SI_OpportunityListController olc = new SI_OpportunityListController();
                olc.queryOpportunities();
                olc.sortOpportunites();
                olc.sortContractStartDate();
                olc.sortContractEndDate(); 
                olc.sortTotalIOAmount();
                olc.sortOriginalPaymentTerm();
                olc.getOpportunityList();
                
               /* OpportunityListController.OpportunityView ov = new OpportunityListController.OpportunityView(new SObject(),olc);
                ov.opportunityName = 'aa';
                ov.contractEndDate = System.today();
                ov.paymentAmount = string.valueOf('10.0');
                ov.opportunityAmount = Double.valueOf('10.0');
                ov.caseNumber = 'aa';
                ov.contractStartDate = System.today();
                ov.opportunityStatus = 'aa';
                */
                ApexPages.currentPage().getParameters().put('sort_field','Opportunities');
                ApexPages.currentPage().getParameters().put('sort_order','asc');
                 olc = new SI_OpportunityListController();
                ApexPages.currentPage().getParameters().put('sort_field','ContractStartDate');
                ApexPages.currentPage().getParameters().put('sort_order','asc');
                 olc = new SI_OpportunityListController();
                ApexPages.currentPage().getParameters().put('sort_field','ContractEndDate');
                ApexPages.currentPage().getParameters().put('sort_order','asc');
                 olc = new SI_OpportunityListController();
                ApexPages.currentPage().getParameters().put('sort_field','TotalIOAmount');
                ApexPages.currentPage().getParameters().put('sort_order','asc');
                 olc = new SI_OpportunityListController();
                ApexPages.currentPage().getParameters().put('sort_field','OriginalPaymentTerm');
                ApexPages.currentPage().getParameters().put('sort_order','asc');
                 olc = new SI_OpportunityListController();
               
                olc.GoPrevious();
                olc.GoNext();
                olc.GoLast();
                olc.GoFirst();
                olc.getRenderPrevious();
                olc.getRenderNext();
                olc.getRecordSize();
                olc.getPageNumber();
                
                
            
            
                //System.assert(ov != null);
            }
        } catch(Exception e ){
            //This is intended
        }
        
    }
}