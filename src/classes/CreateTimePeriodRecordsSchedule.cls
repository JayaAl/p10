/*
	Start : Forecast Planning Tool - Revisited
	Created Date : 18-03-2015
	Author : Lakshman(sfdcace@gmail.com)
	Purpose : This process will execute once every week and create 1 week ahead time period
	from existing max timeperiod, In this way we always have 3 years ahead time periods available in system (considering that we have existing 3 years ahead time period)
	with respect to current date, keeping this schedule job frequency high with 1 week at a time.
	
*/

global class CreateTimePeriodRecordsSchedule implements Schedulable {
	/*
	execute
	@param - SchedulableContext(ctx)
	return 
	*/
	global void execute(SchedulableContext ctx){
		//To call method runTheProcess 
		runTheProcess();
	}
	/*
		execute
		@param - runTheProcess
		return 
	*/
	global void runTheProcess(){
		/* Declaration of primitive data types */
		List<Time_Periods__c> allTimePeriods = new List<Time_Periods__c>();
		Time_Periods__c thisTimePeriod;
		Time_Periods__c MaxTimePeriod;
		//Default values
		MaxTimePeriod=[select id, Sunday_Date__c from Time_Periods__c order by Sunday_Date__c desc
                       limit 1];
		
		Date firstDayOfWeekCurrentMonth3YearsAfter;
		
		if(MaxTimePeriod!=null && MaxTimePeriod.Sunday_Date__c!=null){
			thisTimePeriod = new Time_Periods__c();
			firstDayOfWeekCurrentMonth3YearsAfter=MaxTimePeriod.Sunday_Date__c.addDays(7);
		}
        
        thisTimePeriod.Friday_Date__c = firstDayOfWeekCurrentMonth3YearsAfter.addDays(5);
        thisTimePeriod.Monday_Date__c = firstDayOfWeekCurrentMonth3YearsAfter.addDays(1);
        thisTimePeriod.Saturday_Date__c = firstDayOfWeekCurrentMonth3YearsAfter.addDays(6);
        thisTimePeriod.Sunday_Date__c = firstDayOfWeekCurrentMonth3YearsAfter;
        thisTimePeriod.Thursday_Date__c = firstDayOfWeekCurrentMonth3YearsAfter.addDays(4);
        thisTimePeriod.Tuesday_Date__c = firstDayOfWeekCurrentMonth3YearsAfter.addDays(2);
        thisTimePeriod.Wednesday_Date__c = firstDayOfWeekCurrentMonth3YearsAfter.addDays(3);
        thisTimePeriod.Name = thisTimePeriod.Wednesday_Date__c.format() + ' through ' + thisTimePeriod.Wednesday_Date__c.addDays(7).format();
        Datetime dt = datetime.newInstance(thisTimePeriod.Wednesday_Date__c.year(), thisTimePeriod.Wednesday_Date__c.month(),thisTimePeriod.Wednesday_Date__c.day());
        thisTimePeriod.Wednesday_Upsert_Key__c = dt.format('mmddyyyy');
        system.debug('thisTimePeriod: ---->>>>' + thisTimePeriod);
		
		if(thisTimePeriod!=null){
			insert thisTimePeriod;
		}
	}
}