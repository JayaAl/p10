@isTest(seealldata=true)
public class CreateTimePeriodRecordsScheduleTest {
	public static String CRON_EXP = '0 0 0 15 3 ? 2022';
    static testmethod void testCreateTimePeriodRecordsSchedule() {
     	List<Time_Periods__c> allTimePeriods = new List<Time_Periods__c>();
        allTimePeriods = [Select Id from Time_Periods__c Limit 1];
        if(allTimePeriods.isEmpty()) {
            Time_Periods__c thisTimePeriod;
            Date limitTill = date.today().addyears(3);
            //Default values
            
            Date firstDayOfWeekCurrentMonth3YearsAfter =  date.newInstance(system.now().year(), 1, 1).adddays(7).toStartOfWeek();
            
            
            while(firstDayOfWeekCurrentMonth3YearsAfter <= limitTill) {
                thisTimePeriod = new Time_Periods__c();
                thisTimePeriod.Friday_Date__c = firstDayOfWeekCurrentMonth3YearsAfter.addDays(5);
                thisTimePeriod.Monday_Date__c = firstDayOfWeekCurrentMonth3YearsAfter.addDays(1);
                thisTimePeriod.Saturday_Date__c = firstDayOfWeekCurrentMonth3YearsAfter.addDays(6);
                thisTimePeriod.Sunday_Date__c = firstDayOfWeekCurrentMonth3YearsAfter;
                thisTimePeriod.Thursday_Date__c = firstDayOfWeekCurrentMonth3YearsAfter.addDays(4);
                thisTimePeriod.Tuesday_Date__c = firstDayOfWeekCurrentMonth3YearsAfter.addDays(2);
                thisTimePeriod.Wednesday_Date__c = firstDayOfWeekCurrentMonth3YearsAfter.addDays(3);
                thisTimePeriod.Name = thisTimePeriod.Wednesday_Date__c.format() + ' through ' + thisTimePeriod.Wednesday_Date__c.addDays(7).format();
                Datetime dt = datetime.newInstance(thisTimePeriod.Wednesday_Date__c.year(), thisTimePeriod.Wednesday_Date__c.month(),thisTimePeriod.Wednesday_Date__c.day());
                thisTimePeriod.Wednesday_Upsert_Key__c = dt.format('mmddyyyy');
                system.debug('thisTimePeriod: ---->>>>' + thisTimePeriod);
                firstDayOfWeekCurrentMonth3YearsAfter = firstDayOfWeekCurrentMonth3YearsAfter.addDays(7);
                allTimePeriods.add(thisTimePeriod);
            }
            
            if(! allTimePeriods.isEmpty()) {
                insert allTimePeriods;
            }
        }
        Test.startTest();
        String jobId = System.schedule('ScheduleApexClassTest1234',
                        CRON_EXP, 
                        new CreateTimePeriodRecordsSchedule());
    	Test.stopTest();
    }
}