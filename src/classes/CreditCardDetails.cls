public with sharing class CreditCardDetails {
	private String securityCode;
	private String creditCardNumber;
	private String zipCode;
	private Integer expirationYear;
	private Integer expirationMonth;
	private String customerName;
    private String countryCode;
    private String address1;
    private String address2;
    private String city;
    private String state;

	public CreditCardDetails(String customerName, Integer expirationMonth, Integer expirationYear, String zipCode,
	String state, String securityCode){
		this.state = state;
		setCustomerName(customerName);
		setExpirationMonth(expirationMonth);
		setExpirationYear(expirationYear);
		setZipCode(zipCode);
		setSecurityCode(securityCode);
	}
    public CreditCardDetails(String customerName, Integer expirationMonth, Integer expirationYear, String zipCode, String address1, String address2,
    String city, String state, String securityCode) {
    	this.address1 = address1;
    	this.address2 = address2;
    	this.city = city;
    	this.state = state;
		setCustomerName(customerName);
		setExpirationMonth(expirationMonth);
		setExpirationYear(expirationYear);
		setZipCode(zipCode);
		setSecurityCode(securityCode);
	}

    public CreditCardDetails(String customerName, Integer expirationMonth, Integer expirationYear, String zipCode, String address1, String address2,
    String city, String state, String securityCode, String countryCode) {
        this(customerName, expirationMonth, expirationYear, zipCode, address1, address2, city, state, securityCode);
        setCountryCode(countryCode);
    }
    
    public void setCreditCardNumber(String creditCardNumber)
    {
    	this.creditCardNumber = creditCardNumber;
    }
    
    public void setCity(String city)
    {
    	this.city = city;
    }
    
    public String getCreditCardNumber()
    {
    	return creditCardNumber;
    }

    public String getCountryCode() {
        return countryCode;
    }

    private void setCountryCode(String countryCode) {
        if (countryCode == null || countryCode.length() == 0 || !('AU'.equals(countryCode) ||'NZ'.equals(countryCode))) {
            countryCode = 'US';
		}
        this.countryCode = countryCode;
    }
    
    public String getState()
    {
    	return state;
    }
    
    public String getCity()
    {
    	return city;
    }
    
    public String getAddress1(){
    	return address1;
    }
    
    public String getAddress2(){
    	return address2;
    }

    public String getSecurityCode() {
		return securityCode;
	}

	public String getZipCode() {
		return zipCode;
	}

	public Integer getExpirationYear() {
		return expirationYear;
	}

	public Integer getExpirationMonth() {
		return expirationMonth;
	}

	public String getCustomerName() {
		return customerName;
	}

	public void setSecurityCode(String securityCode) {
		this.securityCode = securityCode;
	}

    /**
     * Accepts 5 digit zipCodes or anything ending in a 9 digit zipcodes (78924-4542), inclusive. All other values
     *  will be set to empty string.
      * @param zipCode
     */
    private void setZipCode(String zipCode) {
        if (zipCode == null) {
            this.zipCode = '';
            return;
        }

        if (Pattern.matches('^\\d{5}$', zipCode)) {
           this.zipCode = zipCode;
        } else {
            String cleanedZipcode = cleanZipcode(zipCode);
            this.zipCode = cleanedZipcode;
        }
	}

    public String cleanZipcode(String zipCode) {
        if (zipCode == null || zipCode.length() == 0) {
            return '';
        }

        String cleanedZipcode = zipCode.trim();

        if (Pattern.matches('^\\d{5}$', cleanedZipcode)) {
            return cleanedZipcode;
        } else {
            //see if zipcode is of the format 45643-3853 or ends in 45643-3853
            if (Pattern.matches('.*\\d{5}-\\d{4}$', cleanedZipcode)) {
                Integer length = cleanedZipcode.length();
                return cleanedZipcode.substring(length-10, length);
            }
        }

        //Otherwise it's too complicated to cleanup in a 100% reliable way, just return empty which won't be submitted
        return '';
    }

    private void setExpirationYear(Integer expirationYear) {
		this.expirationYear = expirationYear;
	}

	private void setExpirationMonth(Integer expirationMonth) {
		if (expirationMonth < 1 || expirationMonth > 12) {
			throw new ExpirationException('Expiration month must be between 1 and 12. Provided: '
					+ expirationMonth);
		}
		this.expirationMonth = expirationMonth;
	}

	private void setCustomerName(String customerName) {
		this.customerName = customerName;
	}

    /**
     * Used by the Paymentech system to convert expiration dates to a MMYY format.
     * @return
     */
    public String getConcatenatedExpiryDate() {
		String expiryMonth = String.valueOf(getExpirationMonth());
		//Pad a zero to the front if it's one digit
		expiryMonth = (getExpirationMonth() < 10) ? '0' + expiryMonth : expiryMonth;
        String expiryYear = String.valueOf(expirationYear);
        Integer len = expiryYear.length();
		return expiryYear + expiryMonth;
	}
	
	public class ExpirationException extends Exception {}
}