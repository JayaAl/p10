/**
 * @name: CreditNote_BatchPostController
 * @desc: Used to update Credit Notes with 'In Progress' status. Following fields are updated:
            A. Period = Open_Period__c
            B. Credit Note Date = c2g__CreditNoteDate__c
          Once updated and the field set Ready to Post, user will be able to Post these credit notes.
 * @author: Lakshman(sfdcace@gmail.com)
 * @date: 25-9-2013
 */
@isTest
public class CreditNote_BatchPostController {
/*
    
    //   item in context from the page
     
    
    public List<SelectOption> creditNoteView {get;set;}
    public String selectedView {get;set;}
    public String pageSize {get;set;}
    public String pageNumber {get;set;}
    public List<CreditNoteWrapper> listCreditNoteWrapper {get;set;}
    //public c2g__codaCreditNote__c objCreditNote {get;set;}
    public Date creditNoteDate {get;set;}
    public Date dueDate {get;set;}
    public c2g__codaCreditNote__c updateCreditNotes {get;set;}
    public Boolean disableButton {get;set;}
    public String filtersApplied{get;set;}
    public String shcStatus{get;set;}
    
       set controller
    
    public ApexPages.StandardSetController setCon{get;set;}
    
    
       generic variables used
    
    private String currentUserId;
     
       constructor
    
    public CreditNote_BatchPostController (){
        this.creditNoteView = new List<SelectOption>();
        this.creditNoteView.add(new SelectOption('In Progress', 'In Progress')); 
        this.creditNoteView.add(new SelectOption('Ready to Post', 'Ready to Post'));
        selectedView = 'In Progress';
        //objCreditNote = new c2g__codaCreditNote__c();
        updateCreditNotes = new c2g__codaCreditNote__c();
        updateCreditNotes.c2g__CreditNoteDate__c = System.today();
        this.listCreditNoteWrapper = new List<CreditNoteWrapper>();
        currentUserId = UserInfo.getUserId();
        
        populateUserLevelDefaults();
        if(ApexPages.currentPage().getParameters().get('view') != null && ApexPages.currentPage().getParameters().get('view') != '') {
            selectedView = ApexPages.currentPage().getParameters().get('view');
        }
        if(ApexPages.currentPage().getParameters().get('pageSize') != null && ApexPages.currentPage().getParameters().get('pageSize') != '') {
            pageSize = ApexPages.currentPage().getParameters().get('pageSize');
        }
        
        pageNumber = '1';
        filtersApplied = '';
        showCreditNotes();
    }
    
    public void populateUserLevelDefaults() {
         //Get the values for the running user.
        BulkUpdatePostCreditNotes__c s = BulkUpdatePostCreditNotes__c.getValues(currentUserId);
        
        if(s == null) {
            // If the instance was null defer to the org value for the defaults.
             //  This assumes there is no profile level in play. 
            s = BulkUpdatePostCreditNotes__c.getOrgDefaults();
            pageSize = s.DefaultPageSIze__c;
            selectedView = s.Default_View__c;
            
        } else {
            // If non-null then use the current setting value to drive the defaults.  
            pageSize = s.DefaultPageSIze__c;
            selectedView = s.Default_View__c;
        }
    
    }
    
    public void showCreditNotes() {
        
        String myQuery;
        if(selectedView == 'In Progress') {
            myQuery = 'Select C2G__PERIOD__C, c2g__CreditNoteDate__c, c2g__DueDate__c, Ready_to_Post__c, c2g__CreditNoteStatus__c, Name, Id From c2g__codaCreditNote__c where c2g__CreditNoteStatus__c = \'In Progress\' AND Ready_to_Post__c = false';
            filtersApplied = 'Credit Note Status: <strong>In Progress</strong><br/>Ready to Post: <strong>false</strong>';            
        } else if(selectedView == 'Ready to Post') {
            myQuery = 'Select C2G__PERIOD__C, c2g__CreditNoteDate__c, c2g__DueDate__c, Ready_to_Post__c, c2g__CreditNoteStatus__c, Name, Id From c2g__codaCreditNote__c where Ready_to_Post__c = true AND c2g__CreditNoteStatus__c = \'In Progress\'';
            filtersApplied = 'Credit Note Status: <strong>In Progress</strong><br/>Ready to Post: <strong>true</strong>';            
        } 

        
        
        myQuery += ' Limit 10000';
        BulkUpdatePostCreditNotes__c configEntry = BulkUpdatePostCreditNotes__c.getOrgDefaults();
        disableButton = configEntry.BatchInProgress__c;
        shcStatus = configEntry.AutoPostScheduler__c ? 'ON' : 'OFF';
        
        system.debug('Query-----------' + myQuery );
        this.setCon = new ApexPages.StandardSetController(Database.getQueryLocator(myQuery));
        // Here defining pagination step size
        this.setCon.setPageSize(integer.valueOf(pageSize));
        this.setCon.setpageNumber(integer.valueOf(pageNumber));
        // Fill out the list to be used at the search page
        listCreditNoteWrapper = getCreditNotes();
    }
    
    public List<CreditNoteWrapper> getCreditNotes(){
        
        List<CreditNoteWrapper> rows = new List<CreditNoteWrapper>();
        
        for(sObject r : this.setCon.getRecords()){
            c2g__codaCreditNote__c a = (c2g__codaCreditNote__c)r;
            CreditNoteWrapper row = new CreditNoteWrapper(a, false);
            rows.add(row);            
        }
                
        return rows;
        
    }
    
     
     //  get selected creditNotes
     
    public List<CreditNoteWrapper> getSelectedCreditNotes() {
        List<CreditNoteWrapper> creditNotesSelected = new List<CreditNoteWrapper>();
        for(CreditNoteWrapper iw: listCreditNoteWrapper) {
            if(iw.IsSelected) {
                creditNotesSelected.add(iw);
            }
        }
        return creditNotesSelected;
    }
    
     
    //    update selected creditNotes
     
    public PageReference updateSelectedCreditNotes() {
        
        List<Id> creditNoteToUpdate = new List<Id>();
        for(CreditNoteWrapper iw: listCreditNoteWrapper) {
            if(iw.IsSelected) {
                creditNoteToUpdate.add(iw.tCreditNote.Id);
            }
        }
        if(! creditNoteToUpdate.isEmpty()) {
            BulkUpdatePostCreditNotes__c configEntry =BulkUpdatePostCreditNotes__c.getOrgDefaults();
            configEntry.BatchInProgress__c = true;
            update configEntry;
            system.debug('*********updateCreditNotes' + updateCreditNotes);
            Database.executeBatch(new CreditNote_BatchUpdateClass(creditNoteToUpdate, updateCreditNotes), 25);
            updateCreditNotes = new c2g__codaCreditNote__c();
            updateCreditNotes.C2G__CreditNoteDATE__C = System.today();
        }
        PageReference pageRef = new PageReference('/apex/CreditNote_BatchPost?pageSize='+pageSize+'&view='+selectedView);
        
        pageRef.setRedirect(true);
        return pageRef;
    }
    
     
     //   post selected creditNotes
     
    public PageReference postSelectedCreditNotes() {
        List<Id> creditNoteIdsToPost = new List<Id>();
        for(CreditNoteWrapper iw: listCreditNoteWrapper) {
            if(iw.IsSelected && iw.tCreditNote.Ready_to_Post__c) {
                creditNoteIdsToPost.add(iw.tCreditNote.Id);
            }
        }
        if(! creditNoteIdsToPost.isEmpty()) {
            BulkUpdatePostCreditNotes__c configEntry =BulkUpdatePostCreditNotes__c.getOrgDefaults();
            configEntry.BatchInProgress__c = true;
            update configEntry;
            SalesCreditNoteBulkPost SalesCreditNotePostBatch = new SalesCreditNoteBulkPost(creditNoteIdsToPost);
            SalesCreditNotePostBatch.isFromCreditNoteVF = true;
            Database.executeBatch(SalesCreditNotePostBatch, 25); 
        }
        PageReference pageRef = new PageReference('/apex/CreditNote_BatchPost?pageSize='+pageSize+'&view='+selectedView);
        
        pageRef.setRedirect(true);
        return pageRef;
    }
    
     
    //   update and post selected creditNotes
     
    public PageReference updateAndPostSelectedCreditNotes() {
        List<Id> creditNoteToUpdate = new List<Id>();
        for(CreditNoteWrapper iw: listCreditNoteWrapper) {
            if(iw.IsSelected) {
                creditNoteToUpdate.add(iw.tCreditNote.Id);
            }
        }
        if(! creditNoteToUpdate.isEmpty()) {
            BulkUpdatePostCreditNotes__c configEntry =BulkUpdatePostCreditNotes__c.getOrgDefaults();
            configEntry.BatchInProgress__c = true;
            update configEntry;
            system.debug('*********updateCreditNotes' + updateCreditNotes);
            updateCreditNotes.Ready_To_Post__c = true;
            CreditNote_BatchUpdateClass ibup = new CreditNote_BatchUpdateClass(creditNoteToUpdate, updateCreditNotes);
            ibup.isPost = true;
            Database.executeBatch(ibup, 25);
            updateCreditNotes = new c2g__codaCreditNote__c();
            updateCreditNotes.C2G__CreditNoteDATE__C = System.today();
        }
        PageReference pageRef = new PageReference('/apex/CreditNote_BatchPost?pageSize='+pageSize+'&view='+selectedView);
        
        pageRef.setRedirect(true);
        return pageRef;
    }
    
    public void toggleSchedulerStatus() {
        BulkUpdatePostCreditNotes__c configEntry =BulkUpdatePostCreditNotes__c.getOrgDefaults();
        configEntry.AutoPostScheduler__c = !configEntry.AutoPostScheduler__c;
        update configEntry;
        shcStatus = configEntry.AutoPostScheduler__c ? 'ON' : 'OFF';
    }
    
     
    //    advance to next page
     
    public void doNext(){
        
        if(this.setCon.getHasNext()) {
            this.setCon.next();
            pageNumber = string.valueOf(this.setCon.getPageNumber());
            listCreditNoteWrapper = getCreditNotes();
        }
 
    }
    
     
   //     advance to previous page
    
    public void doPrevious(){
        
        if(this.setCon.getHasPrevious()) {
            this.setCon.previous();
            pageNumber = string.valueOf(this.setCon.getPageNumber());
            listCreditNoteWrapper = getCreditNotes();
        }
                
    }
    
     
   //     return whether previous page exists
     
    public Boolean getHasPrevious(){
        
        return this.setCon.getHasPrevious();
        
    }
    
     
    //    return whether next page exists
    
    public Boolean getHasNext(){
        
        return this.setCon.getHasNext();
    
    }
    
     
    //    return page number
     
    public Integer getPageNumber(){
        
        return this.setCon.getPageNumber();
        
    }
    
     
     //   for record limits
     
    public List<SelectOption> getItems(){
        List<SelectOption> options = new List<SelectOption>();
        options.add(new SelectOption('200','200'));
        options.add(new SelectOption('500','500'));
        options.add(new SelectOption('700','700'));
        options.add(new SelectOption('1000','1000'));
        return options;
    }
    
      
    //     return total pages
     
    Public Integer getTotalPages(){
    
        Decimal totalSize = this.setCon.getResultSize();
        Decimal pageSize = this.setCon.getPageSize();
        
        Decimal pages = totalSize/pageSize;
        
        return (Integer)pages.round(System.RoundingMode.CEILING);
    }
    
     
    //    helper class that represents a row
      
    public class CreditNoteWrapper{
        
        public c2g__codaCreditNote__c  tCreditNote {get;set;}
        public Boolean IsSelected{get;set;}
        
        public CreditNoteWrapper(c2g__codaCreditNote__c a, Boolean s){
            this.tCreditNote = a;
            this.IsSelected = s;
        }
        
    } 
    */
}