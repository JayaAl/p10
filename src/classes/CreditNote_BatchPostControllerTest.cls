/**
 * @name: CreditNote_BatchPostControllerTest
 * @desc: Test class for CreditNote_BatchPostController
 * @date: 22-10-2013
 */
@isTest(SeeAllData=true)
public class CreditNote_BatchPostControllerTest {
    /*
    private static testMethod void testBatchPostInProgress() {
        try{
            // initial setup
            FF_BLNG_TestUtil_InvoiceRollups util = new FF_BLNG_TestUtil_InvoiceRollups();
            util.setup();
            
            // avoid mixed dml from adding running user to test company
            // owner group
            system.runAs(new User(id = UserInfo.getUserId())) {
                
                
                ApexPages.currentPage().getParameters().put('view','In Progress');
                ApexPages.currentPage().getParameters().put('pageSize','200');
                CreditNote_BatchPostController testController = new CreditNote_BatchPostController ();
                
                
                
                
                
                if(testController.setCon.getResultSize() > 0) {
                    testController.updateCreditNotes.Open_Period__c  = util.testPeriod.Id;
                    testController.updateCreditNotes.C2G__CreditNoteDATE__C = System.today();
                    testController.updateCreditNotes.Ready_to_Post__c = true;
                    testController.listCreditNoteWrapper[0].IsSelected = true;
                    testController.getSelectedCreditNotes();
                    Test.startTest();
                    testController.updateSelectedCreditNotes();
                    Test.stopTest();
                } else {
                    ApexPages.currentPage().getParameters().put('view','Ready to Post');
                    testController = new CreditNote_BatchPostController ();
                    if(testController.setCon.getResultSize() > 0) {
                        testController.updateCreditNotes.Open_Period__c  = util.testPeriod.Id;
                        testController.updateCreditNotes.C2G__CreditNoteDATE__C = System.today();
                        testController.updateCreditNotes.Ready_to_Post__c = true;
                        testController.listCreditNoteWrapper[0].IsSelected = true;
                        testController.listCreditNoteWrapper[0].tCreditNote.Ready_to_Post__c = false;
                        update testController.listCreditNoteWrapper[0].tCreditNote;
                        testController.getSelectedCreditNotes();
                        Test.startTest();
                        testController.updateSelectedCreditNotes();
                        Test.stopTest();
                    }
                    
                }
            }
            }catch(Exception ex) {
                system.debug('Exception caught=' + ex);
            }
    }
    
    
    private static testMethod void testBatchUpdateAndPostInProgress() {
        try{
            // initial setup
            FF_BLNG_TestUtil_InvoiceRollups util = new FF_BLNG_TestUtil_InvoiceRollups();
            util.setup();
            
            // avoid mixed dml from adding running user to test company
            // owner group
        system.runAs(new User(id = UserInfo.getUserId())) {
            
            
            ApexPages.currentPage().getParameters().put('view','In Progress');
            ApexPages.currentPage().getParameters().put('pageSize','200');
            CreditNote_BatchPostController testController = new CreditNote_BatchPostController ();
            
            
            
            
            
            if(testController.setCon.getResultSize() > 0) {
                testController.updateCreditNotes.Open_Period__c  = util.testPeriod.Id;
                testController.updateCreditNotes.C2G__CreditNoteDATE__C = System.today();
                testController.updateCreditNotes.Ready_to_Post__c = true;
                testController.listCreditNoteWrapper[0].IsSelected = true;
                testController.getSelectedCreditNotes();
                Test.startTest();
                testController.updateAndPostSelectedCreditNotes();
                Test.stopTest();
            } else {
                ApexPages.currentPage().getParameters().put('view','Ready to Post');
                testController = new CreditNote_BatchPostController ();
                if(testController.setCon.getResultSize() > 0) {
                    testController.updateCreditNotes.Open_Period__c  = util.testPeriod.Id;
                    testController.updateCreditNotes.C2G__CreditNoteDATE__C = System.today();
                    testController.updateCreditNotes.Ready_to_Post__c = true;
                    testController.listCreditNoteWrapper[0].IsSelected = true;
                    testController.listCreditNoteWrapper[0].tCreditNote.Ready_to_Post__c = false;
                    update testController.listCreditNoteWrapper[0].tCreditNote;
                    testController.getSelectedCreditNotes();
                    Test.startTest();
                    testController.updateAndPostSelectedCreditNotes();
                    Test.stopTest();
                }
                
            }
            }
        } catch(Exception ex) {
            system.debug('Exception caught=' +ex);
        }
    }
     
    private static testMethod void testBatchPostReadyToPost() {
        try{
            // initial setup
            FF_BLNG_TestUtil_InvoiceRollups util = new FF_BLNG_TestUtil_InvoiceRollups();
            util.setup();
            
            // avoid mixed dml from adding running user to test company
            // owner group
            system.runAs(new User(id = UserInfo.getUserId())) {
                
                
                ApexPages.currentPage().getParameters().put('view','Ready to Post');
                CreditNote_BatchPostController testController = new CreditNote_BatchPostController ();
    
                if(testController.setCon.getResultSize() > 0) {
                    testController.listCreditNoteWrapper[0].IsSelected = true;
                    testController.getSelectedCreditNotes();
                    Test.startTest();
                    testController.postSelectedCreditNotes();
                    Test.stopTest();
                } else {
                    ApexPages.currentPage().getParameters().put('view','In Progress');
                    testController = new CreditNote_BatchPostController ();
                    if(testController.setCon.getResultSize() > 0) {
                        testController.listCreditNoteWrapper[0].IsSelected = true;
                        testController.listCreditNoteWrapper[0].tCreditNote.Ready_to_Post__c = true;
                        update testController.listCreditNoteWrapper[0].tCreditNote;
                        testController.getSelectedCreditNotes();
                        Test.startTest();
                        testController.postSelectedCreditNotes();
                        Test.stopTest();
                    }
                    
                }
                }
            } catch(Exception ex) {
                system.debug('Exception caught=' + ex);
            }
    }
    
    private static testMethod void testAutoBatchPostReadyToPost() {
        try {
            // initial setup
            FF_BLNG_TestUtil_InvoiceRollups util = new FF_BLNG_TestUtil_InvoiceRollups();
            util.setup();
            
            // avoid mixed dml from adding running user to test company
            // owner group
            system.runAs(new User(id = UserInfo.getUserId())) {
                
                
                ApexPages.currentPage().getParameters().put('view','Ready to Post');
                CreditNote_BatchPostController testController = new CreditNote_BatchPostController ();
                testController.toggleSchedulerStatus();
                BulkUpdatePostCreditNotes__c configEntry =BulkUpdatePostCreditNotes__c.getOrgDefaults();
                configEntry.AutoPostScheduler__c = true;
                update configEntry;
                
                if(testController.setCon.getResultSize() > 0) {
                    testController.listCreditNoteWrapper[0].IsSelected = true;
                    testController.getSelectedCreditNotes();
                    Test.startTest();
                    String qry =  'Select Id From c2g__codaCreditNote__c where Id = \'' + testController.listCreditNoteWrapper[0].tCreditNote.Id + '\' AND Ready_to_Post__c = true';
                    CreditNote_BatchReadyToPost b = new CreditNote_BatchReadyToPost(qry); 
                    database.executebatch(b, 1);
                    Test.stopTest();
                } else {
                    ApexPages.currentPage().getParameters().put('view','In Progress');
                    testController = new CreditNote_BatchPostController ();
                    if(testController.setCon.getResultSize() > 0) {
                        testController.listCreditNoteWrapper[0].IsSelected = true;
                        testController.listCreditNoteWrapper[0].tCreditNote.Ready_to_Post__c = true;
                        update testController.listCreditNoteWrapper[0].tCreditNote;
                        testController.getSelectedCreditNotes();
                        Test.startTest();
                        String qry =  'Select Id From c2g__codaCreditNote__c where Id = \'' + testController.listCreditNoteWrapper[0].tCreditNote.Id + '\' AND Ready_to_Post__c = true';
                        CreditNote_BatchReadyToPost b = new CreditNote_BatchReadyToPost(qry); 
                        
                        database.executebatch(b, 1);
                        Test.stopTest();
                    }
                    
                }
                }
            }catch(Exception ex) {
                system.debug('Exception caught=' + ex);
            }
    }
    private static testMethod void testPagination() {
        try{
        // initial setup
        FF_BLNG_TestUtil_InvoiceRollups util = new FF_BLNG_TestUtil_InvoiceRollups();
        util.setup();
        system.runAs(new User(id = UserInfo.getUserId())) {
            
            ApexPages.currentPage().getParameters().put('view','In Progress');
            CreditNote_BatchPostController testController = new CreditNote_BatchPostController();
            //Pagination action test
            testController.doNext();
            testController.doPrevious();
            testController.getHasPrevious();
            testController.getHasNext();
            testController.getPageNumber();
            testController.getItems();
            testController.getTotalPages();
       }
       } catch(Exception ex) {
           system.debug('***********' + ex);
       }
   }
    */
}