/**
 * @name: CreditNote_BatchUpdateClass 
 * @desc: Used to Batch Update list of Credit Notes sent from VF
 * @author: Lakshman(sfdcace@gmail.com)
 * @date: 12-8-2013
 */
global class CreditNote_BatchUpdateClass implements Database.Batchable<sObject>, Database.Stateful{

   private final List<Id> m_ids;
   public c2g__codaCreditNote__c updateCreditNotes;
   // To maintain all failed records with error message
   global String errormsgs;
   global Integer failureCounter;
   global Integer totalRecords;
   global CreditNoteBatchLog__c objBatchLog;
   global Boolean isPost;
   global List<Id> successIds;
   global CreditNote_BatchUpdateClass(List<Id> ids, c2g__codaCreditNote__c i){
      m_ids=ids; 
      isPost = false;
      updateCreditNotes = i;
      errormsgs = '';
      failureCounter = 0;
      totalRecords = 0;
      successIds = new List<Id>();
      objBatchLog = new CreditNoteBatchLog__c();
   }

   global Database.QueryLocator start(Database.BatchableContext BC){
      return Database.getQueryLocator('Select C2G__PERIOD__C, c2g__CreditNoteDate__c, c2g__DueDate__c, Ready_to_Post__c, c2g__CreditNoteStatus__c, Name, Id From c2g__codaCreditNote__c where Id In :m_ids');
   }

   global void execute(Database.BatchableContext BC, List<sObject> scope){
     List<c2g__codaCreditNote__c> creditNotesToUpdate = (List<c2g__codaCreditNote__c>)scope;
     for(c2g__codaCreditNote__c s : creditNotesToUpdate){
        s.C2G__PERIOD__C = updateCreditNotes.Open_Period__c;
        s.c2g__CreditNoteDate__c = updateCreditNotes.c2g__CreditNoteDate__c;
        s.Ready_to_Post__c = updateCreditNotes.Ready_to_Post__c;
        s.C2G__DUEDATE__C = updateCreditNotes.c2g__CreditNoteDate__c;
     }
    system.debug('***********creditNotesToUpdate' + creditNotesToUpdate);
    if(! creditNotesToUpdate.isEmpty()){
        totalRecords += creditNotesToUpdate.size();
        Database.SaveResult[] lsr = Database.update(creditNotesToUpdate,false);
        Integer recordid = 0;
        for (Database.SaveResult SR : lsr) {
            if (!SR.isSuccess()) {
                this.errormsgs += 'Credit Note Number: ' + creditNotesToUpdate[recordid].Name + ', CreditNote Id: ' + creditNotesToUpdate[recordid].Id + ', Error: ' + SR.getErrors()[0].getMessage() + '<br/>';
                failureCounter++;
            } else {
                successIds.add(creditNotesToUpdate[recordid].Id);
            }
            
            recordid++;
        }
    }
    }

   global void finish(Database.BatchableContext BC){
      // Get the ID of the AsyncApexJob representing this batch job  
      // from Database.BatchableContext.    
      // Query the AsyncApexJob object to retrieve the current job's information.  
      
      AsyncApexJob a = [Select Id, Status, NumberOfErrors, JobItemsProcessed, CreatedDate, CompletedDate, 
       TotalJobItems, CreatedBy.Email, CreatedById, CreatedBy.Name
       from AsyncApexJob where Id =:BC.getJobId()];
    
      // Send an email to the Apex job's submitter notifying of job completion.  
      Messaging.SingleEmailMessage mail = new Messaging.SingleEmailMessage();
      String[] toAddresses = new String[] {a.CreatedBy.Email};
      mail.setToAddresses(toAddresses);
      mail.setSubject('CreditNote Batch Update ' + a.Status);
      String body = 'Hi, <br/>Credit Note Batch Update is ' + a.Status;
      body += '<br/>The batch Apex job was created by '+a.CreatedBy.Name+' ('+a.CreatedBy.Email+') processed '+a.TotalJobItems+' batches with '+a.NumberOfErrors+' failures. The process began at '+a.CreatedDate+' and finished at '+a.CompletedDate+'.';
      body += '<br/>Job Id ==>' + a.Id;
      body += '<br/>Total Records Processed ==>' + totalRecords; 
      if(this.errormsgs != ''){
          body += '<br/>Total Records Failed ==>' + failureCounter +'<br/>Following Errors were addressed:<br/>'; 
          body += this.errormsgs;
          //Create Log Entry
          objBatchLog.ErrorMessage__c = '<br/>Total Records Failed ==>' + failureCounter +'<br/>Following Errors were addressed:<br/>' + this.errormsgs;
          objBatchLog.JobCreator__c = a.CreatedById;
          insert objBatchLog;
      }
      mail.setHtmlBody(body);
    
      Messaging.sendEmail(new Messaging.SingleEmailMessage[] { mail });
      
      if(isPost) {
          if(! successIds.isEmpty()) {
                SalesCreditNoteBulkPost SalesCreditNotePostBatch = new SalesCreditNoteBulkPost(successIds);
                SalesCreditNotePostBatch.isFromCreditNoteVF = true;
                Database.executeBatch(SalesCreditNotePostBatch, 25); 
          }
      } else {
          BulkUpdatePostCreditNotes__c configEntry =BulkUpdatePostCreditNotes__c.getOrgDefaults();
          configEntry.BatchInProgress__c = false;
          update configEntry;
      }
      
   }
}