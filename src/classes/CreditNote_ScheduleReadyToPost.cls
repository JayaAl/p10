/**
 * @name: CreditNote_ScheduleReadyToPost
 * @desc: Used to schedule post Ready To Post CreditNotes
 * @author: Lakshman(sfdcace@gmail.com)
 * @date: 23-9-2013
 */
global class CreditNote_ScheduleReadyToPost implements Schedulable {
    // This test runs a scheduled job at midnight Sept. 1st. 2022
    public static String CRON_EXP = '0 0 0 1 9 ? 2022';
    global void execute(SchedulableContext sc) {
        BulkUpdatePostCreditNotes__c configEntry =BulkUpdatePostCreditNotes__c.getOrgDefaults();
        if(configEntry.AutoPostScheduler__c) {     
            //query from Ready To Post view
            String qry =  'Select Id From c2g__codaCreditNote__c where Ready_to_Post__c = true AND c2g__CreditNoteStatus__c = \'In Progress\'';
            CreditNote_BatchReadyToPost b = new CreditNote_BatchReadyToPost(qry); 
            database.executebatch(b, 25);
        }
    }
}