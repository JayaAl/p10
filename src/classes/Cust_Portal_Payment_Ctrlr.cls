public class Cust_Portal_Payment_Ctrlr{

    public string active_type_filter {get;set;} 
    public String selectedValue {get;set;}
    public String headerLabelStr {get;set;}
    private Map<String,String> headerLabel;
    public List<SelectOption> gettype_filter_options(){
        
        List<SelectOption> type_filter_options = new List<SelectOption>();
        headerLabel = new Map<String,String>();
        // Construct type filter
        type_filter_options.add(new SelectOption('sales_invoices','Invoices'));
        type_filter_options.add(new SelectOption('prepayorders','Prepay Orders'));
        type_filter_options.add(new SelectOption('credit_notes','Credit Notes'));
        type_filter_options.add(new SelectOption('cash_entries','Payments'));
        type_filter_options.add(new SelectOption('sales_receipts','Receipts'));
        type_filter_options.add(new SelectOption('open_orders','Open Orders'));
        headerLabel.put('sales_invoices','Invoices');
        headerLabel.put('prepayorders','Prepay Orders');
        headerLabel.put('open_orders','Open Orders');
        headerLabel.put('credit_notes','Credit Notes');
        headerLabel.put('sales_receipts','Receipts');
        headerLabel.put('cash_entries','Payments');
        active_type_filter = 'sales_invoices';
        selectedValue = active_type_filter;

        return type_filter_options; 
    }

    public Cust_Portal_Payment_Ctrlr(){
        headerLabel = new Map<String,String>();
        headerLabel.put('sales_invoices','Invoices');
        selectedValue = active_type_filter;  
        headerLabelStr = headerLabel.get(active_type_filter);       
    }

    public void rerenderView(){
        selectedValue = active_type_filter;
        headerLabelStr = headerLabel.get(active_type_filter); 
    }
}