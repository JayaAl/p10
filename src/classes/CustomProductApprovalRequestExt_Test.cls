/*
Developer: Vardhan Gupta (vgupta@pandora.com)
Description:
  Test methods for CustomProductApprovalRequestExt.cls
*/
@isTest 
private class CustomProductApprovalRequestExt_Test {

  /* Test Variables */

  static Case testCase;
  
  /* Before Test Actions */
  
  // Create two accounts and generate a case with a related 
  // opportunity and a record type matching the trigger filters
  static {
    Account testAccount1 = UTIL_TestUtil.generateAccount();
    insert new Account[] {testAccount1};
    Opportunity testOpportunity = UTIL_TestUtil.generateOpportunity(testAccount1.id);
    insert testOpportunity;
     User u = [Select Id from User where Isactive = true and Profile.Name = 'System Administrator' limit 1];
    
    // generate case using 'Product_Approval_Request' record type
    //populate all required fields on the layout.
    testCase = UTIL_TestUtil.generateCase();
    String recordTypeDevName = 'Product_Approval_Request'; //Product_Approval_Request
    testCase.recordTypeId = UTIL_CaseRecordTypeHelper.getRecordTypeId(recordTypeDevName);
    testCase.opportunity__c = testOpportunity.id;
    testCase.Account = testAccount1;
    testCase.Product_Start_Date__c = Date.today();
    testCase.Product_End_Date__c = Date.today();
    testCase.RVP__c= u.id;
    testCase.Priority = 'Medium';
    testCase.Status = 'Pending Review-click Submit for Approval';
    testCase.Product_Name1__c = 'Amplified - Add to Calendar';
    testCase.Product_Line_Item_Amount__c = 1000;
    testCase.Client_Type__c = 'National';
    testCase.Product_Justification__c = 'test';
    
    
  }
  
  /* Test Methods */
  //Test to see if the custom button on the layout is tiggering the apex class.
  
  @isTest
  private static void testAutoRun() {
  
    Test.startTest();
    
      // insert test case
    insert testCase;
    
    //instantiate the custom page created to simulate the approval button.
       // PageReference VFpage = Page.Case_Product_Approval_Request;
       //  test.setCurrentPage(VFpage);
    
    //instantiate the custom controller created to trigger approvals.
   //     ApexPages.StandardController VFpage_Extn = new ApexPages.StandardController(testCase);
   //      CustomProductApprovalRequestExt CLS = new CustomProductApprovalRequestExt(VFpage_Extn);
        
        //call the controller function to submit the record for approval.
  //  cls.autoRun();
    
    //confirm that the record was submitted for approvals and the status now refects pending approvals.
     //system.assertEquals(testCase.Status, 'Pending Approval');
    
    Test.stopTest();
   

  }
  

  

}