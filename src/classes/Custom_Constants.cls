/*
nsk 04/01/14
*/
public with sharing class Custom_Constants {
    public static String PrefreredInvoicingMethod = 'Mail w/ Notarization'; 
    public static String SalesInvoiceType = 'Sales Invoice';
    
    public static String IntegrationProfileId = '00e40000000rdCt';
    /*Constants for Opportunity Clone project added by Lakshman on 1/22/2015*/
    public static String OPT_InsideSalesRecordTypeId = '012400000009ebLAAQ';//Inside Sales
    public static String OPT_OutsideSalesRecordTypeId = '012400000009gTKAAY';//Outside Sales
    public static String OPT_PerformanceRecordTypeId = '012400000009gTJAAY';//Performance
    public static String OPT_ProductsHREF = '#06640000000Cvta';
    Public static String OPT_ProgrammticRecordTypeId = '0121b00000005nx';//Programmatic
    /*end of constants for Opportunity Clone*/
    /*Constants for Auto Create Products*/
    public static String OPT_SimpleEntryInside = '0124000000015dUAAQ';//Simple Entry Inside Sales
    public static String OPT_SimpleEntryOutside = '0124000000015dVAAQ';//Simple Entry Outside Sales
    /*end of constants for Auto Create Products*/
}