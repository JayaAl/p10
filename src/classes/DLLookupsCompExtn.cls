/**
* ─────────────────────────────────────────────────────────────────────────────────────────────────┐
* DLLookupsCompExtn: controller for DLLookupsComp component
* ──────────────────────────────────────────────────────────────────────────────────────────────────
* @author         Sridharan Subramanian
* @maintainedBy   Sridharan Subramanian
* @version        1.0
* @created        2018-02-15
* @modified       
* ──────────────────────────────────────────────────────────────────────────────────────────────────
* @changes        Awanish kumar <akumar3@pandora.com>
* v1.1            
* 2018-01-30      remove filter condition on contact.
* ──────────────────────────────────────────────────────────────────────────────────────────────────
*/
public with sharing class DLLookupsCompExtn {
    public String advertiserFilterStr {get;set;}
    public String agencyFilterStr {get;set;}
    public String contactFilterStr {get;set;}
    public String filterStr {get;set;}
    public String accountValue{get;set{
    	system.debug('inside here' + value);
	    	if(value!=null){
	    		list<account> actList = new list<Account>();
	    		actList = [select name from account where id=:value];
	    		if(actList.size()>0)
	    			accountValue = actList[0].name + '(Advertiser)';
	    		else {
	    			accountValue = null;
	    		}
	    	}

    	}
	}
    public String agencyValue{get;set{
    	system.debug('inside here agency' + value);
	    	if(value!=null){
		    		list<account> agencyList = new list<Account>();
		    		agencyList = [select name from account where id=:value];
		    		if(agencyList.size()>0)
		    			agencyValue = agencyList[0].name + '(Agency)';
		    		else {
		    			agencyValue = null;
		    		}
		    }
		}
    	/*if(value!=null)
    		agencyValue = [select name from account where id=:value].name + '(Ad Agency)';

    	}*/
    }
    public String contactValue{get;set{

	    	if(value!=null){
	    		list<contact> contactList = new list<contact>();
	    		contactList = [select name from contact where id=:value];
	    		if(contactList.size()>0)
	    			contactValue = contactList[0].name;
	    		else {
	    			contactValue = null;
	    		}
		    }
    		/*if(value!=null)
    			contactValue = [select name from contact where id=:value].name;*/

    	}
    }
    
    
    public DLLookupsCompExtn() {
        
        //Set the Qualifying filter for the Agency drop down
        agencyFilterStr = ' AND Type IN (\'Ad Agency\', \'Reseller\')';
        label = 'AccountName';
        uniqueComponentAgencyId = 'Agency';
        uniqueComponentContactId = 'Contact';


        system.debug('This is the Component Call');
    }
    /*
    * public variables
    **/
    public String objectName{get;
        set{
            if(objectName != value){
                objectName = value;
                objectLabelPlural = Schema.getGlobalDescribe().get(ObjectName).getDescribe(). getLabelPlural();
                
            }
        }
        
    }
    
    public String label{
        get;
        set{
            label = value;
            uniqueComponentId = label.replace(' ', '');//.toLowerCase();
            agencyFilterStr = ' AND Type IN (\'Ad Agency\', \'Reseller\')';
        }
    }


    
    public String uniqueComponentId{get;set;}/*{
    		system.debug('This is the value set for account' + value);
    		uniqueComponentId = value;

    		}
    	}*/
    public String uniqueComponentAgencyId{get;set;}/*{
    		system.debug('This is the value set' + value);
    		uniqueComponentAgencyId = value;

    		}
    }*/
    public String uniqueComponentContactId{get;set;}
    public String objectLabelPlural{get;set;}
    
    /**
     * Remote action method to send list of records
     * @param  searchText 
     * @return  List<Wrapper>
     */
    @RemoteAction
    public static List<Wrapper> search(String objectName, String displayFieldNames, String fieldsPattern, String photoValue,string accountId, string agencyId,string filterText,String searchText) {
        System.debug('&&&agencyId&&&&'+agencyId);
        String query;
        List<String> displayFieldNamesLst;
        String photoFieldName;
        List<Wrapper> results = new List<Wrapper>();
        String finalQuery;
        String photoToDisplay;
        string agencyFilterStr = ' AND Type IN (\'Ad Agency\', \'Reseller\')';
    /* v1.1 removing filter condition on Contact  */
     
        string contactFilterStr = '';



        //if(!string.isEmpty(agencyId))
        if((string.isEmpty(accountId)) && (!string.isEmpty(agencyId)))
            contactFilterStr = ' and accountId =:agencyId ';
        else if((!string.isEmpty(accountId)) && (string.isEmpty(agencyId)))
            contactFilterStr = ' and (accountId =:accountId ) ';
        else if((!string.isEmpty(accountId)) && (!string.isEmpty(agencyId)))
            contactFilterStr = ' and ((accountId =:agencyId) or (accountId =:accountId ) )';
    

        string objectType = '';

        //system.debug('This is Account Id' + accountId);
        //system.debug('This is Agency Id' + agencyId);

        if(String.isBlank(fieldsPattern)){
            fieldsPattern = displayFieldNames;
        }
        
        //prepare a where clause
        displayFieldNamesLst = displayFieldNames.split(',');
        String whereClause = '';
        /*for(String fieldName : displayFieldNamesLst){
            whereClause += String.isBlank(whereClause) ? ' WHERE ' + fieldName + ' LIKE \'%{text}%\'' : ' OR ' + fieldName + ' LIKE \'%{text}%\'' ;
        }*/


        
        if(objectName=='Contact')
            whereClause = ' WHERE Name  LIKE \'%{text}%\'' ;
        else {
            whereClause = ' WHERE name  LIKE \'%{text}%\'' ;
        }
        
        //add Id field to field names if necessary
        if(!displayFieldNames.toLowerCase().contains('id')){
            displayFieldNames += ', Id';
        }
        
        //add photo field if not added
        if(photoValue.toLowerCase().contains('field')){
            List<String> photoValueLst = photoValue.split('->');
            if(photoValueLst.size() > 1 && !displayFieldNames.toLowerCase().contains(photoValueLst[1].toLowerCase())){
                photoFieldName = photoValueLst[1];
                displayFieldNames += ', '+photoValueLst[1];
            }
        }else if(photoValue.toLowerCase().contains('url')){
            List<String> photoValueLst = photoValue.split('->');
            if(photoValueLst.size() > 1){
                photoToDisplay = photoValueLst[1];
            }
        }
        System.debug('*****agencyFilterStr*****'+agencyFilterStr);
        System.debug('*****contactFilterStr*****'+contactFilterStr);
        System.debug('*****agencyId*****'+agencyId);
        if(filterText=='AgencyFilter'){
            query = 'SELECT ' + displayFieldNames + ' FROM ' + objectName + whereClause  + ' ' + agencyFilterStr;
            objectType ='Agency';
        }
        else if(filterText=='ContactFilter')
        {
            query = 'SELECT ' + displayFieldNames + ' FROM ' + objectName + whereClause ;
            
            if(contactFilterStr<>null && contactFilterStr<>'')
            	 query+= ' ' + contactFilterStr;
            
            objectType ='Contact';
            
        }
        else {
        	
            	query = 'SELECT ' + displayFieldNames + ' FROM ' + objectName + whereClause  + ' ' + filterText;
            	objectType ='Advertiser';
        	
        }
        if(!String.isEmpty(query)){
            finalQuery = query.replace('{text}', searchText);
    
            system.debug('THIS IS FINAL filterText' + filterText);
            system.debug('THIS IS FINAL finalQuery' + finalQuery);
            
            for(Sobject sobj : database.query(finalQuery)){
                String displayValue = fieldsPattern;
                for(String fieldName : displayFieldNamesLst){
                    String fieldValue = sobj.get(fieldName) == null ? '' : String.valueOf(sobj.get(fieldName));
                    displayValue = displayValue.replace(fieldName, fieldValue);
                    if(String.isNotBlank(photoFieldName) && sobj.get(photoFieldName) != null){
                        photoToDisplay = String.valueOf(sobj.get(photoFieldName));
                    }
                }
                results.add(new Wrapper(String.valueOf(sobj.get('Id')), displayValue, photoToDisplay,objectType));
            }    
        }
        return results;
    }
    
    
    /*************************************************************************
     ******************************* WRAPPER **************************************
     ****************************************************************************/
    public class Wrapper{
        public Id recordId{get;set;}
        public String displayValue{get;set;}
        public String photoUrl{get;set;}
        public string objectType{get;set;}
        public Wrapper(Id recordId, String displayValue, String photoUrl,string objectType){
            this.recordId = recordId;
            this.displayValue = displayValue;
            this.photoUrl = photoUrl;
            this.objectType = objectType;
        }
    }
}