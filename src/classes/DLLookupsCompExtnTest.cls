/**
 * ─────────────────────────────────────────────────────────────────────────────────────────────────┐
 * DLLookupsCompExtnTest: Test Class for DLLookupsCompExtn 
 * ──────────────────────────────────────────────────────────────────────────────────────────────────
 * @author         Awanish Kumar
 * @maintainedBy   Awanish Kumar
 * @version        1.0
 * @created        2018-02-18
 * @modified      This test class covers the code coveage for DLLookupsCompExtn class. Based on record Account,
 * 				  Agency and Contact is searched in the database and result is returned.
 * ──────────────────────────────────────────────────────────────────────────────────────────────────
 *       
 */
@isTest
public class DLLookupsCompExtnTest {

    @testSetup
    static void testDLLookupsCompExtn() {

        // Create Agency
        Account accAgency = generateAccount('DLLookupsCompExtnAccountTest DLS1',7,'94441');
        accAgency.Type = 'Ad Agency';
        accAgency.D_U_N_S__c = 'TestDLS11';
        insert accAgency;
        System.assertNotEquals(null, accAgency.Id);
        
        // Create Account
        Account acc = generateAccount('DLLookupsCompExtnTestAccount DLS1',8,'94442');
        acc.Type = 'Advertiser';
        acc.D_U_N_S__c = 'TestDLS12';
        acc.Agency_Client_direct__c = accAgency.id;
        insert acc;
        System.assertNotEquals(null, acc.Id);

        // Create Contact
        Contact c = generateContact('testContact DLS1', acc.Id);
        insert c;
        System.assertNotEquals(null, c.Id);

    }

    public static Account generateAccount( String accountName,integer numberLength,string postalCode) {
    //Updated by Lakshman 20/3/2013: added required fields for inserting Account
        return new Account(
            name = accountName,   //v1.1 Added the parameter value as account name to provide unique name 
            Website = generateRandomString(8)+'.com',
            BillingStreet = generateRandomString(numberLength),
            BillingCity = generateRandomString(numberLength),
            BillingState = 'CA',
            BillingPostalCode = postalCode,
            c2g__CODAAccountTradingCurrency__c = 'USD'//Add by Lakshman on 10/28/2014(ISS-11405)
        );
    }

    public static Contact generateContact(String contactName, Id accountId) {
        return new Contact(lastName = contactName, accountId = accountId, MailingStreet = 'MyStreet1', MailingCountry = 'USA', MailingState = 'CA', MailingCity = 'Fremont', Email = 'myemail1@mydomain.com', Title = 'MyTitle1', firstname = 'MyFristName1');
    }
    private static Set<String> priorRandoms;
    public static String generateRandomString(Integer length){
        if(priorRandoms == null)
            priorRandoms = new Set<String>();

        if(length == null) length = 1+Math.round( Math.random() * 8 );
        String characters = 'abcdefghijklmnopqrstuvwxyz1234567890';
        String returnString = '';
        while(returnString.length() < length){
            Integer charpos = Math.round( Math.random() * (characters.length()-1) );
            returnString += characters.substring( charpos , charpos+1 );
        }
        if(priorRandoms.contains(returnString)) {
            return generateRandomString(length);
        } else {
            priorRandoms.add(returnString);
            return returnString;
        }
    }

    @istest static void testDLLookupsCompExtnForAccount() {

        Account accAccount = [Select id from Account where name = 'DLLookupsCompExtnTestAccount DLS1'
            Limit 1
        ];
        DLLookupsCompExtn DLLookupsCompExtnObj = new DLLookupsCompExtn();
        DLLookupsCompExtnObj.objectLabelPlural = 'Accounts';
        DLLookupsCompExtnObj.objectName = 'Account';
        DLLookupsCompExtnObj.accountValue =accAccount.id;
        String displayFieldNames = 'Name,NumberOfEmployees,Account_Revenue__c,Priority__c';
        String fieldsPattern = '';
        String photoValue = 'field->Category__c';
        String filterText = '';
        String SearchText = 'DLLookupsCompExtnTestAccount';
        Test.startTest();
        List < DLLookupsCompExtn.Wrapper > accountSeachResult = DLLookupsCompExtn.search(DLLookupsCompExtnObj.objectName, displayFieldNames, fieldsPattern, photoValue, accAccount.id, '', filterText, searchText);
        Test.stopTest();
       // Verify if Account is fetched in the search result 
        System.assert(!accountSeachResult.isEmpty());
    }
    @istest static void testDLLookupsCompExtnForAdAgency() {
        Account accAccount = [Select id from Account where name = 'DLLookupsCompExtnTestAccount DLS1'
            Limit 1
        ];
        Account agencyAccount = [Select id from Account where name = 'DLLookupsCompExtnAccountTest DLS1'
            Limit 1
        ];
        DLLookupsCompExtn DLLookupsCompExtnObj = new DLLookupsCompExtn();
        DLLookupsCompExtnObj.objectLabelPlural = 'Accounts';
        DLLookupsCompExtnObj.objectName = 'Account';
        DLLookupsCompExtnObj.agencyValue =agencyAccount.Id;
        String displayFieldNames = 'Name,NumberOfEmployees,Account_Revenue__c,Priority__c';
        String fieldsPattern = '';
        String photoValue = 'field->Category__c';
        String filterText = 'AgencyFilter';
        String SearchText = 'DLLookupsCompExtnAccountTest';
        Test.startTest();
        List < DLLookupsCompExtn.Wrapper > results = DLLookupsCompExtn.search(DLLookupsCompExtnObj.objectName, displayFieldNames, fieldsPattern, photoValue, agencyAccount.id, accAccount.id, filterText, searchText);
        Test.stopTest();
        // Verify if Ad Agency is fetched in the search result. 
        System.assert(!results.isEmpty());
    }
    @istest static void testDLLookupsCompExtnForContact() {
        Account accAccount = [Select id from Account where name = 'DLLookupsCompExtnTestAccount DLS1'
            Limit 1
        ];
        Account agencyAccount = [Select id from Account where name = 'DLLookupsCompExtnAccountTest DLS1'
            Limit 1
        ];
        Contact ContactTest = [Select id from Contact where Lastname = 'testContact DLS1'
            Limit 1
        ];
        DLLookupsCompExtn DLLookupsCompExtnObj = new DLLookupsCompExtn();
        DLLookupsCompExtnObj.objectLabelPlural = 'Contacts';
        DLLookupsCompExtnObj.objectName = 'Contact';
        DLLookupsCompExtnObj.agencyValue ='DLLookupsCompExtnAccountTest';
        String displayFieldNames = 'Description,Comments__c,Event__c,Territory__c';
        DLLookupsCompExtnObj.contactValue =ContactTest.Id;
        String fieldsPattern = '';
        String photoValue = 'field->ATG_Website__c';
        String filterText = 'ContactFilter';
        String SearchText = 'testContact';
        filterText = 'ContactFilter';
        Test.startTest();
        List < DLLookupsCompExtn.Wrapper > results = DLLookupsCompExtn.search(DLLookupsCompExtnObj.objectName, displayFieldNames, fieldsPattern, photoValue, accAccount.id, agencyAccount.id, filterText, searchText);
        Test.stopTest();
        // Verify if Contact is fetched in the search result.
        System.assert(!results.isEmpty());
    }

}