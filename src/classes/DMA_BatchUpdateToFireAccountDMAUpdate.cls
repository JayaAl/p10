global class DMA_BatchUpdateToFireAccountDMAUpdate implements Database.Batchable<sObject>{
    
    public String query{get; set;}
    
    global void setDefaultQuery(){
        this.query = 'SELECT RecordType.Name, RecordTypeId, BillingState, BillingPostalCode, BillingCity FROM Account WHERE RecordType.Name = \'default\'';
    }
    
    global database.querylocator start(Database.BatchableContext BC){
            return Database.getQueryLocator(query);
    }
    
  global void execute(Database.BatchableContext BC, List<sObject> scope){
        List<Account> accns = new List<Account>();

        for(sObject s : scope){
            Account a = (Account)s;
            if(a.BillingPostalCode !=null ||(a.BillingCity!=null && a.BillingState != null)){
                a.DMA_Name__c = 'update';   
                accns.add(a);
            }
        }

        update accns;
    }

    global void finish(Database.BatchableContext BC){
         
    }
}