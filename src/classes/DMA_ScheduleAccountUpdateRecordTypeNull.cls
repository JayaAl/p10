global class DMA_ScheduleAccountUpdateRecordTypeNull implements Schedulable{
	global void execute(SchedulableContext SC) {
      DMA_BatchUpdateToFireAccountDMAUpdate bu = new DMA_BatchUpdateToFireAccountDMAUpdate();
			bu.query = 'SELECT RecordType.Name, RecordTypeId, BillingState, BillingPostalCode, BillingCity FROM Account WHERE RecordTypeID = null';
			ID batchprocessid = Database.executeBatch(bu);
   }
}