/**
 * This class contains unit tests for validating the behavior of Apex classes
 * and triggers.
 *
 * Unit tests are class methods that verify whether a particular piece
 * of code is working properly. Unit test methods take no arguments,
 * commit no data to the database, and are flagged with the testMethod
 * keyword in the method definition.
 *
 * All test methods in an organization are executed whenever Apex code is deployed
 * to a production organization to confirm correctness, ensure code
 * coverage, and prevent regressions. All Apex classes are
 * required to have at least 75% code coverage in order to be deployed
 * to a production organization. In addition, all triggers must have some code coverage.
 * 
 * The @isTest class annotation indicates this class only contains test
 * methods. Classes defined with the @isTest annotation do not count against
 * the organization size limit for all Apex scripts.
 *
 * See the Apex Language Reference for more information about Testing and Code Coverage.
 */
@isTest(seealldata=false)
private class DMA_TEST_BatchUpdateToFireAccountDMA {

    static testMethod void myUnitTest() {
        //nsk: 10/23 Adding below logic to control trigger execution via custom label
        if(Label.Disable_Account_Triggers!='YES'){        
        
            RecordType rt = [Select SobjectType, Name From RecordType where Name='default' and sObjectType='Account'];
            
            DMA__c dma1 = new DMA__c();
            dma1.City_Name__c = 'TESTCITY1';
            dma1.State__c = 'T1';
            dma1.DMA_Code__c = 111;
            dma1.Name = 'NAME1';
            dma1.Zip_Code__c = '00001';
            insert dma1;
    
                    list <Account> accounts = new list<Account>();
                    for(Integer i =0; i<200; i++){
                        Account a = new Account();
                        a.Name = 'TEST'+String.valueOf(i);
                        a.RecordTypeId = rt.id;
                        a.BillingPostalCode = '00001';
                        a.BillingCity = 'testcity1';
                        a.BillingState='T1';
                        accounts.add(a);
                    }        
                insert accounts;
                
                test.startTest();
                
                DMA_BatchUpdateToFireAccountDMAUpdate bu = new DMA_BatchUpdateToFireAccountDMAUpdate();
                bu.setDefaultQuery();
                bu.query = 'SELECT RecordType.Name, RecordTypeId, BillingState, BillingPostalCode, BillingCity FROM Account WHERE RecordType.Name = \'default\' and BillingState = \'T1\'';
                ID batchprocessid = Database.executeBatch(bu);
                Test.StopTest();
                    
                System.AssertEquals(database.countquery('SELECT count()'
                       +' FROM Account WHERE DMA_Name__c = \'NAME1\''), 200); 
          }        
      }
}