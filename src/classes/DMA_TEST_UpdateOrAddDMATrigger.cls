/**
 * This class contains unit tests for validating the behavior of Apex classes
 * and triggers.
 *
 * Unit tests are class methods that verify whether a particular piece
 * of code is working properly. Unit test methods take no arguments,
 * commit no data to the database, and are flagged with the testMethod
 * keyword in the method definition.
 *
 * All test methods in an organization are executed whenever Apex code is deployed
 * to a production organization to confirm correctness, ensure code
 * coverage, and prevent regressions. All Apex classes are
 * required to have at least 75% code coverage in order to be deployed
 * to a production organization. In addition, all triggers must have some code coverage.
 * 
 * The @isTest class annotation indicates this class only contains test
 * methods. Classes defined with the @isTest annotation do not count against
 * the organization size limit for all Apex scripts.
 *
 * See the Apex Language Reference for more information about Testing and Code Coverage.
 */
@isTest(seealldata=false)
private class DMA_TEST_UpdateOrAddDMATrigger {

    static testMethod void myUnitTest() {
        //nsk: 10/23 Adding below logic to control trigger execution via custom label
        if(Label.Disable_Account_Triggers!='YES'){        
    
            DMA__c dma1 = new DMA__c();
            DMA__c dma2 = new DMA__c();
            DMA__c dma3 = new DMA__c();
            RecordType rt = [Select SobjectType, Name From RecordType where Name='default' and sObjectType='Account'];
            Account a1 = new Account();
            Account a2 = new Account();
            Account a3 = new Account();
            
            dma1.City_Name__c = 'TESTCITY1';
            dma1.State__c = 'T1';
            dma1.DMA_Code__c = 111;
            dma1.Name = 'NAME1';
            dma1.Zip_Code__c = '00001';
            insert dma1;
            
            dma2.City_Name__c = 'TESTCITY2';
            dma2.State__c = 'T2';
            dma2.DMA_Code__c = 222;
            dma2.Name = 'NAME2';
            dma2.Zip_Code__c = '00002';
            insert dma2;
            
            dma3.City_Name__c = 'TESTCITY3';
            dma3.State__c = 'T3';
            dma3.DMA_Code__c = 333;
            dma3.Name = 'NAME3';
            dma3.Zip_Code__c = '00003';
            insert dma3;
            
            test.startTest();    
            
            a1.Name = 'TEST1';
            insert a1;
            a1 = [select Name, BillingPostalCode, DMA_Code__c, DMA_Name__c, RecordTypeId from Account where Id = :a1.Id];
            System.debug('!!!!!!!!!!'+a1.RecordTypeId);
            System.assert(a1.DMA_Code__c == null);
            
            
            a2.Name = 'TEST2';
            a2.BillingPostalCode = '00001-0000';
            a2.RecordTypeId = rt.Id;
            insert a2;
            a2 = [select Name, BillingPostalCode, DMA_Code__c, DMA_Name__c from Account where Id = :a2.Id];
            System.assert(a2.DMA_Code__c == 111);
            System.assert(a2.DMA_Name__c == 'Name1');
            
            a2.BillingPostalCode = '00002';
            update a2;
            a2 = [select Name, BillingPostalCode, DMA_Code__c, DMA_Name__c from Account where Id = :a2.Id];
            System.debug('!!!!!!!!!!!'+a2.DMA_Code__c);
            System.assert(a2.DMA_Code__c == 222);
            System.assert(a2.DMA_Name__c == 'Name2');
            
            a2.BillingPostalCode = '00009';
            update a2;
            a2 = [select Name, BillingPostalCode, DMA_Code__c, DMA_Name__c from Account where Id = :a2.Id];
            System.assert(a2.DMA_Code__c == null);
            System.assert(a2.DMA_Name__c == null);
            
            a3.Name='Test3';
            a3.BillingState = 't3';
            a3.BillingCity = 'TeStCitY3';
            a3.RecordTypeId = rt.Id;
            insert a3;
            a3 = [select Name, BillingPostalCode, DMA_Code__c, DMA_Name__c from Account where Id = :a3.Id];
            System.assert(a3.DMA_Code__c == 333);
            System.assert(a3.DMA_Name__c == 'NAME3');
            
            a3.BillingState = 't2';
            a3.BillingCity = 'TeStCitY2';
            a3.RecordTypeId = rt.Id;
            update a3;
            a3 = [select Name, BillingPostalCode, DMA_Code__c, DMA_Name__c from Account where Id = :a3.Id];
            System.assert(a3.DMA_Code__c == 222);
            System.assert(a3.DMA_Name__c == 'NAME2');
            
            
            test.stopTest();        
        }
    }
}