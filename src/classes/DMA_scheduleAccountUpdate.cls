global class DMA_scheduleAccountUpdate implements Schedulable{
   global void execute(SchedulableContext SC) {
      DMA_BatchUpdateToFireAccountDMAUpdate bu = new DMA_BatchUpdateToFireAccountDMAUpdate();
			bu.setDefaultQuery();
			ID batchprocessid = Database.executeBatch(bu);
   }
}