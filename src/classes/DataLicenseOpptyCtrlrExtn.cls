/**
* ─────────────────────────────────────────────────────────────────────────────────────────────────┐
* DataLicenseOpptyCtrlrExtn: controller for DataLicenseOppty page
* ──────────────────────────────────────────────────────────────────────────────────────────────────
* @author         Jaya Alaparthi
* @maintainedBy   Jaya Alaparthi
* @version        1.0
* @created        2017-04-13
* @modified       
* @systemLayer    Service
* @see            ????
* @see            ????
* ──────────────────────────────────────────────────────────────────────────────────────────────────
* @changes
* v1.1            Jaya Alaparthi
* 2018-01-30      Adding functionality to create Data licensing ESS-43633
* ──────────────────────────────────────────────────────────────────────────────────────────────────
* @changes
* v1.2            Jaya Alaparthi
* 2017-06-06      
* ─────────────────────────────────────────────────────────────────────────────────────────────────┘
*/
public with sharing class DataLicenseOpptyCtrlrExtn {

    
    public Opportunity opptyRec;
    public Opportunity opptyRecord {set;get;}
    public String opptyName {get;set;}
    public Id accountId {get;set;}
    public Id agencyId {get;set;}
    public Id primaryContactId {get;set;}
    public Boolean isValidIP = true;
    public list<PageGeneratorWrapper> pageGenWrapperList {get;set;} 
    public list<PLIWrapper> wrapperPliList {get;set;}
    public list<SelectOption> mediumOptions {get;set;}
    public Integer recordIndex {get;set;}
    public Integer recordToRemove {get;set;}
    // The extension constructor initializes the private member
    // variable mysObject by using the getRecord method from the standard
    // controller.
    public DataLicenseOpptyCtrlrExtn(ApexPages.StandardController stdController) {
        
        wrapperPliList = new list<PLIWrapper>();
        recordIndex = 0;
        // getvalid porducts
        mediumOptions = getProducts();
        this.opptyRec = (Opportunity)stdController.getRecord();      
        // initiate wrapperPliList 
        wrapperPliList.add(new PLIWrapper(0,new OpportunityLineItem())); 
    }

    private list<SelectOption> getProducts() {

        list<SelectOption> medium = new list<SelectOption>();

        Schema.DescribeFieldResult mediumOptions = OpportunityLineItem.Medium__c.getDescribe();
        for (Schema.Picklistentry picklistEntry : mediumOptions.getPicklistValues()) {

            medium.add(new SelectOption(pickListEntry.getValue(),pickListEntry.getLabel()));
        }
        return medium;
    }

    public void addPLIAction() {

        recordIndex = recordIndex +1;
        OpportunityLineItem pli = new OpportunityLineItem();
        PLIWrapper pliWrapperRef = new PLIWrapper(recordIndex,pli);
        System.debug('recordIndex:'+recordIndex);
        System.debug('pliWrapperRef:'+pliWrapperRef);
        System.debug('wrapperPliList:'+wrapperPliList);
        wrapperPliList.add(pliWrapperRef); 
        System.debug('wrapperPliList:'+wrapperPliList);

    }
    public void removePLIAction() {

        list<PLIWrapper> pliWrapperList = new list<PLIWrapper>();
        System.debug('recordToRemove:'+recordToRemove);
        for(PLIWrapper pliW : wrapperPliList) {
            if(pliW.index != recordToRemove) {
                pliWrapperList.add(pliW);
            }
        }
        wrapperPliList = pliWrapperList;

    }

    public PageReference save(){

        List<OpportunityLineItem> pliList = new List<OpportunityLineItem>();
        PageReference pref;

        validateDataInput();
        System.debug('isValidIP:'+isValidIP);
        if(isValidIP) {
            try {

                Id opptyRecordTypeId = Schema.SObjectType.Opportunity.getRecordTypeInfosByName().get('Data Licensing').getRecordTypeId();
                opptyRec.Name = opptyName;
                opptyRec.RecordTypeId = opptyRecordTypeId;
                opptyRec.StageName = 'In Negotiation';
                opptyRec.Budget_Source__c = 'Digital';
                opptyRec.CurrencyIsoCode = 'USD';
                opptyRec.Initiative_Location__c = 'US';
                opptyRec.Initiative_Type__c = 'None';
                opptyRec.Opportunity_Type__c = 'Repeat Client / Renewal';
                opptyRec.CloseDate = Date.today();
                opptyRec.Confirm_direct_relationship__c = true;
                if(!String.isBlank(agencyId)) {
                    opptyRec.Agency__c = agencyId;
                    opptyRec.Confirm_direct_relationship__c = false;
                }
                if(!String.isBlank(accountId)) {
                    opptyRec.AccountId = accountId;
                }
                if(!String.isBlank(primaryContactId)) {
                    opptyRec.Primary_Contact__c = primaryContactId;
                }
                if(!String.isBlank(opptyRec.DSP__c) && opptyRec.DSP__c == 'DMB') {
                    opptyRec.Preferred_Invoicing_Method__c = 'Transfer from SFDC';

                } else if(!String.isBlank(opptyRec.DSP__c) && opptyRec.DSP__c == 'The Trade Desk') {
                    opptyRec.Preferred_Invoicing_Method__c = 'email';
                }
                System.debug('opptyRec:'+opptyRec);
                insert opptyRec;

                // update Products list with Pricebookentery and related Oppty and update them.
                // get pbe for datalicensing
                PriceBookEntry pbe = new PriceBookEntry();
                pbe = getDataLicensePriceBookEntry();
                System.debug('pbe Retrived:'+pbe+'wrapperPliList:'+wrapperPliList+'opptyRec:'+opptyRec);
                for(PLIWrapper pliWrapper : wrapperPliList) {

                    OpportunityLineItem pli = pliWrapper.productLine;
                    pli.PricebookEntryId = pbe.Id;
                    pli.OpportunityId = opptyRec.Id;
                    if(pli.UnitPrice == null || pli.UnitPrice == 0) {
                        pli.UnitPrice = 0;
                    } 
                    pliList.add(pli);
                    System.debug('pli:'+pli);
                }
                System.debug('pliList:'+pliList);
                if(!pliList.isEmpty()) {

                    System.debug('pliList:'+pliList);
                    insert pliList;
                }
                //ApexPages.Message successfullMsg = new ApexPages.Message(ApexPages.Severity.INFO,'Opportunity Created ');
                pref = new PageReference('/' + opptyRec.Id);
            } catch(DMLException e) {
                
                pref = null;
                ApexPages.addMessages(e);
            }
        }
        return pref;

    }
    // Supporting only USD PBE
    private PriceBookEntry getDataLicensePriceBookEntry() {

        PriceBookEntry pbe = [SELECT Id FROM PriceBookEntry
                                WHERE Product2.Name = 'Data License' 
                                AND Pricebook2.Name='Standard Price Book' 
                                AND Pricebook2.CurrencyIsoCode = 'USD' Limit 1];
        return pbe;
    }
    public void validateDataInput() {

        //Boolean validateFlag = true;
        System.debug('opptyname:'+opptyName+'accountId:'+accountId
                    +'agency:'+agencyId+'primaryContactId:'+primaryContactId);
        if(String.isBlank(opptyName)) {

            ApexPages.addmessage(new ApexPages.message(ApexPages.severity.WARNING,
                                'Please enter Opportunity Name.'));
            isValidIP = false;
        } if(String.isBlank(accountId)) {

            ApexPages.addmessage(new ApexPages.message(ApexPages.severity.WARNING,
                                'Please select Advertiser.'));
            isValidIP = false;
        } if(String.isBlank(agencyId)) {

            ApexPages.addmessage(new ApexPages.message(ApexPages.severity.WARNING,
                                'Please select Agency.'));
            isValidIP = false;
        } if(String.isBlank(primaryContactId)) {

            ApexPages.addmessage(new ApexPages.message(ApexPages.severity.WARNING,
                                'Please select Contact.'));
            isValidIP = false;
        }
        System.debug('in validateDataInput isValidIP:'
            +isValidIP+'primary contact'+primaryContactId
            +'Messages:'+ApexPages.getMessages());
        //return null;
    }
    // wrapper class for PLI's
    public class PLIWrapper {

        public Integer index {get;set;}
        public OpportunityLineItem  productLine {get;set;}
        public PLIWrapper(Integer i, OpportunityLineItem pli) {

            System.debug('in constructor:i='+i+'pli:'+pli);
            index = i;
            productLine = pli;
            productLine.Quantity = 1;
            productLine.ServiceDate = Date.Today();
            productLine.End_Date__c = Date.Today();
            productLine.Offering_Type__c = 'Data Licensing';
        }

    }
}