/***************************************************************************************************************
*  Name                 : DataLicenseOpptyCtrlrExtn
*  Author               : Awanish Kumar
*  Created Date         : 02/22/2018
*  Description          : Test Class for DataLicenseOpptyCtrlrExtn batch class.
                          This test class covers the code coverage for DataLicenseOpptyCtrlrExtn.
						  This test class covers the following scenarios:
						 1. If all the mandatory details are present, an opportunity will be created.
						 2.If neither of Account and Agency is selected,it will through a message.
						 3.If Opportunuty name and Account/Agency is selected and Contact is not selected,
						   it will through a message.
                          
*                          
*                          
************************************************************************************************************************/
@isTest
public class DataLicenseOpptyCtrlrExtnTest
{     
    @isTest(seealldata=true)
    static void testDLLookupsCompExtn() {
        String TEST_STRING = 'aaaa';
        // Create Agency
        Account accAgency = generateAccount('DataLicenseOpptyCtrlrExtnAgencyTestDL1',4,'94538');
        accAgency.Type = 'Ad Agency';
        accAgency.D_U_N_S__c = 'TestDL1';
        insert accAgency;
        System.assertNotEquals(null, accAgency.Id);
        
        // Create Account
        Account acc = generateAccount('DataLicenseOpptyCtrlrExtnAccountTestDL2',5,'94539');
        acc.Type = 'Advertiser';
        acc.D_U_N_S__c = 'TestDL2';
        acc.Agency_Client_direct__c = accAgency.id;
        insert acc;
        System.assertNotEquals(null, acc.Id);

        // Create Contact
        Contact c = generateContact('testContactDL1', acc.Id);
        insert c;
        System.assertNotEquals(null, c.Id);
        
        Opportunity opp = new Opportunity();
        // Create Product         
        Product2 prod = new Product2(Name = 'Data License', CurrencyIsoCode = 'USD',
                                   Family = 'Hardware');
        insert prod;
        System.assertNotEquals(null, prod.Id);
        //Create Pricebook
         Id pricebookId = Test.getStandardPricebookId();

         Pricebook2 pb = [select name from Pricebook2 where id=:pricebookId];

         system.debug('pricebook name' + pb);
		//Create PricebookEntry
        PricebookEntry standardPrice = new PricebookEntry(
          Pricebook2Id = pricebookId, Product2Id = prod.Id,
          UnitPrice = 10000, IsActive = true, CurrencyIsoCode = 'USD');

        insert standardPrice;
        System.assertNotEquals(null, standardPrice.Id);

        

        ApexPages.StandardController sc = new ApexPages.StandardController(opp);
        DataLicenseOpptyCtrlrExtn testopp = new DataLicenseOpptyCtrlrExtn(sc);
        testopp.accountId = acc.id;
        testopp.agencyId=accAgency.id;
        testopp.primaryContactId=c.id;
        testopp.opptyRecord=opp;
        testopp.opptyName ='test Opportunity DL 1';
        testopp.addPLIAction();
        testopp.Save();
        testopp.removePLIAction();
        Opportunity currOppObj = [SELECT Id,StageName,Probability,accountId 
                                    FROM Opportunity 
                                    WHERE Id = :opp.id ];
        System.assert(currOppObj.id!= null);
  
    }


    public static Account generateAccount( String accountName,integer numberLength,string postalCode) {
    //Updated by Lakshman 20/3/2013: added required fields for inserting Account
        return new Account(
            name = accountName,   //v1.1 Added the parameter value as account name to provide unique name 
            Website = generateRandomString(8)+'.com',
            BillingStreet = generateRandomString(numberLength),
            BillingCity = generateRandomString(numberLength),
            BillingState = 'CA',
            BillingPostalCode = postalCode,
            c2g__CODAAccountTradingCurrency__c = 'USD'//Add by Lakshman on 10/28/2014(ISS-11405)
        );
    }

    public static Contact generateContact(String contactName, Id accountId) {
        return new Contact(lastName = contactName, accountId = accountId, MailingStreet = 'MyStreet1', MailingCountry = 'USA', MailingState = 'CA', MailingCity = 'Fremont', Email = 'myemail1@mydomain.com', Title = 'MyTitle1', firstname = 'MyFristName1');
    }
    private static Set<String> priorRandoms;
    public static String generateRandomString(Integer length){
        if(priorRandoms == null)
            priorRandoms = new Set<String>();

        if(length == null) length = 1+Math.round( Math.random() * 8 );
        String characters = 'abcdefghijklmnopqrstuvwxyz1234567890';
        String returnString = '';
        while(returnString.length() < length){
            Integer charpos = Math.round( Math.random() * (characters.length()-1) );
            returnString += characters.substring( charpos , charpos+1 );
        }
        if(priorRandoms.contains(returnString)) {
            return generateRandomString(length);
        } else {
            priorRandoms.add(returnString);
            return returnString;
        }
    }






   @isTest 
    static void testDLLookupsCompExtnForAccountorAgency() {
        String TEST_STRING = 'aaaa';
        // Create Agency
        Account accAgency = generateAccount('DataLicenseOpptyCtrlrExtnAgencyTestDL3',6,'94540');
        accAgency.Type = 'Ad Agency';
        accAgency.D_U_N_S__c = 'TestDL3';
        insert accAgency;
        System.assertNotEquals(null, accAgency.Id);
        
        // Create Contact
        Contact c = generateContact('testContactDL3', accAgency.Id);
        insert c;
        System.assertNotEquals(null, c.Id);
        
        Opportunity opp = new Opportunity();
        Product2 prod = new Product2(Name = 'Data License', CurrencyIsoCode = 'USD',
                                   Family = 'Hardware');
        insert prod;
        System.assertNotEquals(null, prod.Id);

         Id pricebookId = Test.getStandardPricebookId();

        PricebookEntry standardPrice = new PricebookEntry(
          Pricebook2Id = pricebookId, Product2Id = prod.Id,
          UnitPrice = 10000, IsActive = true, CurrencyIsoCode = 'USD');

        insert standardPrice;         
        
      System.assertNotEquals(null, standardPrice.Id);
   ApexPages.StandardController sc = new ApexPages.StandardController(opp);
  
  DataLicenseOpptyCtrlrExtn testopp = new DataLicenseOpptyCtrlrExtn(sc);
  testopp.primaryContactId=c.id;
  testopp.opptyRecord=opp;
  testopp.opptyName ='test Opportunity DL4';
  testopp.Save();
 List<Opportunity> currOppObj = [SELECT Id,StageName,Probability,accountId 
                                    FROM Opportunity 
                                     WHERE Primary_Contact__c=:c.id];
 System.assert(currOppObj.size()== 0);
 /*list<apexpages.Message> messageError = new list<apexpages.Message>();
        system.assertNotEquals(0, messageError.size());
        Boolean messageFound = false;
        for(apexpages.Message msg : messageError)
        {
            if(msg.getDetail() =='Advertiser or Agency is Mandatory. Please select either one of them'&& msg.getSeverity() == ApexPages.Severity.WARNING)    
            messageFound = true;
        }
        system.assert(messageFound); 
*/
    }
  
    /*@isTest 
    static void testDLLookupsCompExtnForContact() {
        String TEST_STRING = 'aaaa';
        // Create Agency
        Account accAgency = generateAccount('DataLicenseOpptyCtrlrExtnAgencyTest');
        accAgency.Type = 'Ad Agency';
        accAgency.D_U_N_S__c = 'Test';
        insert accAgency;
        System.assertNotEquals(null, accAgency.Id);
        
        // Create Account
        Account acc = generateAccount('DataLicenseOpptyCtrlrExtnAccountTest');
        acc.Type = 'Advertiser';
        acc.D_U_N_S__c = 'Test';
        acc.Agency_Client_direct__c = accAgency.id;
        insert acc;
        System.assertNotEquals(null, acc.Id);
        // Create Contact
        Contact c = generateContact('testContact', acc.Id);
        insert c;
        System.assertNotEquals(null, c.Id);
        //intialize Opportunity
        Opportunity opp = new Opportunity();
        //Create product
        Product2 prod = new Product2(Name = 'Data License', CurrencyIsoCode = 'USD',
                                   Family = 'Hardware');
        insert prod;
        System.assertNotEquals(null, prod.Id);
         Id pricebookId = Test.getStandardPricebookId();
		
        PricebookEntry standardPrice = new PricebookEntry(
          Pricebook2Id = pricebookId, Product2Id = prod.Id,
          UnitPrice = 10000, IsActive = true, CurrencyIsoCode = 'USD');

        insert standardPrice;         
        
      System.assertNotEquals(null, standardPrice.Id);
   ApexPages.StandardController sc = new ApexPages.StandardController(opp);
  
  DataLicenseOpptyCtrlrExtn testopp = new DataLicenseOpptyCtrlrExtn(sc);
  //testopp.primaryContactId=c.id;
  testopp.accountId = acc.id;
  testopp.opptyRecord=opp;
  testopp.opptyName ='test Opportunity';
 testopp.Save();
 list<Opportunity> currOppObj = [SELECT Id,StageName,Probability,accountId 
                                    FROM Opportunity 
                                     WHERE Id = :opp.id];
 System.assert(currOppObj.size()== 0);
  
    }*/
    }