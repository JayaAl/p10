public with sharing class DispExistingAttchmentCompCtrlr {

	public String parentIdStr;
	
	public void setparentIdStr(String argId){
	parentIdStr = argId;    
	} 
	
	public String getparentIdStr(){
		return parentIdStr;
	}

	public List<Attachment> selectedAttachmentLst {get{system.debug('*****selectedAttachmentLst Getter *******'); return selectedAttachmentLst;} set{ system.debug('*****selectedAttachmentLst Setter *******');}}

	public DispExistingAttchmentCompCtrlr() {
		system.debug('parentIdStr ::::'+parentIdStr);
		selectedAttachmentLst = [Select Id,Name,description from Attachment where id =: parentIdStr];
		system.debug('selectedAttachmentLst ::::'+selectedAttachmentLst);
	}
}