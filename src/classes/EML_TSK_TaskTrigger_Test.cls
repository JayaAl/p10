/*
Developer: Ralph Callaway <ralph.w.callaway@gmail.com>
Story: https://na2.salesforce.com/a0I40000004Juoi
Description:
	Test methods for EML_TSK_TaskTrigger.trigger
*/
@isTest
private class EML_TSK_TaskTrigger_Test {

	/* Constants */

	private static final Integer BATCH_SIZE = 3;

	/* Before Test Actions */
	
	// Create an email task process custom setting and generate
	// a list of tasks with matching subjects to custom setting
	static final String subjectSearchTerm = 'asd438084fndf';
	static final String newStatus = 'hdfsehiwo';
	static final String originalStatus = 'jhhdfdf';
	static final Integer dueDateOffset = 3;
	static final Date originalDate = System.today();
	static final Date expectedDueDate = System.today().addDays(dueDateOffset);
	static Email_Task_Process__c processCustomSetting;
	static List<Task> tasks;
	static {
		processCustomSetting = new Email_Task_Process__c(
			  name = subjectSearchTerm
			, due_date_offset_days__c = dueDateOffset
			, new_status__c = newStatus
		);
		insert processCustomSetting;
		tasks = new List<Task>();
		for(Integer i = 0; i < BATCH_SIZE; i++) {
			tasks.add(new Task(
				  subject = 'hfhdjfjdf' + subjectSearchTerm + '4845jdhsdf'
				, status = originalStatus
				, activityDate = originalDate
			));
		}
	}
	
	@isTest
	private static void testEmailTaskTriggerPositive() {
		// insert tasks with matching email process
		Test.startTest();
		insert tasks;
		Test.stopTest();
		
		// validate task status and due dates have been updated
		for(Task task : [
			select status, activityDate
			from Task
			where id in :tasks
		]) {
			system.assertEquals(expectedDueDate, task.activityDate);
			system.assertEquals(newStatus, task.status);
		}
	}
	
	@isTest
	private static void testEmailTaskTriggerNegative() {
		// clear custom setting
		delete processCustomSetting;
		
		// insert tasks that no longer match an email process
		Test.startTest();
		insert tasks;
		Test.stopTest();
		
		// validate task statuses and due dates haven't been changed
		for(Task task : [
			select status, activityDate
			from Task
			where id in :tasks
		]) {
			system.assertEquals(system.today(), task.activityDate);
			system.assertEquals(originalStatus, task.status);
		}
	}

}