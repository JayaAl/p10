public without sharing class ERP_BLNG_CreditAggregator {
    
    /* Variables */
    private Map<Id, Opportunity> opptyMap;
    
    /* Constructors */
    
    // Constructor for triggers, compares NEW ERP_Credit_Note__c values against the OLD ERP_Credit_Note__c, gathers the Opportunities wherer there is a change or a new record.
    public ERP_BLNG_CreditAggregator(List<ERP_Credit_Note__c> newList, Map<Id, ERP_Credit_Note__c> oldMap) {
        Set<Id> opptyIdsToUpdate = new Set<Id>();
        
        for(ERP_Credit_Note__c newCreditNote : newList) {
            ERP_Credit_Note__c oldCreditNote = (oldMap == null) ? null : oldMap.get(newCreditNote.id);
            if(oldCreditNote == null || newCreditNote.Total_Credit_Note__c != oldCreditNote.Total_Credit_Note__c) {
                opptyIdsToUpdate.add(newCreditNote.Opportunity_ID__c);
            }
        }
        
        opptyMap = new Map<Id, Opportunity>([
            select id, Credited_To_Date__c 
            from Opportunity 
            where Id in :opptyIdsToUpdate
        ]);
    }
    
    // Overloaded Constructor for batch, input is a List of Opportunities.
    // Opportunity records should contain at a minimum Id and Credited_To_Date__c
    public ERP_BLNG_CreditAggregator(List<Opportunity> opptys) {
        opptyMap = new Map<Id, Opportunity>(opptys);
    }
    
    
    /* Public methods */
    
    public void updateCreditedToDate() {
        updateCreditedToDate(opptyMap);
    }
    
    /* Support Methods */
    
    private void updateCreditedToDate(Map<Id,Opportunity>opptyMap) {
        
        // For each Opportunity in the opptyMap query both historical c2g__codaCreditNote__c and current ERP_Credit_Note__c records
        
        if(! opptyMap.isEmpty() ) {
            
            // First, clear any existing Credited_To_Date__c value on the Opportunities
            for(Opportunity opp: opptyMap.values()) {
                opp.Credited_To_Date__c = 0;
            }
            
            // Perform the Queries, note that results may be empty
            AggregateResult[] creditTotals = aggERPCreditNote(); // ERP_Credit_Note__c
            AggregateResult[] creditTotals2 = aggFFCreditNote(); // c2g__codaCreditNote__c
            
            // Loop through the results, updating opptyMap values
            updateOppMapFromAggTotals(creditTotals, -1); // ERP_Credit_Note__c
            updateOppMapFromAggTotals(creditTotals2, 1); // c2g__codaCreditNote__c
        }
        
        // Finally try updating the unique Opportunities in opptyMap.values
        // Capture any errors using logger helper 
        try{
            if(!opptyMap.isEmpty()){
                update opptyMap.values();
            }
        } catch (Exception e) { // Write error log
            logger.logMessage('ERP_BLNG_CreditAggregator', 'updateCreditedToDate', e.getMessage(), 'Opportunity', '', '');
        }
    }
    
    /* Private method to Query ERP_Credit_Note__c values, returning as AggregateResult */
    private List<AggregateResult> aggERPCreditNote(){
        List<AggregateResult> queryResults = [select Opportunity_ID__c oId,Opportunity_ID__r.CurrencyISOCode,CurrencyISOCode currISOCode, SUM(Total_Credit_Note__c) credTotal
                                              from ERP_Credit_Note__c
                                              where Opportunity_ID__c in :opptyMap.keySet()
                                              AND (NOT NAME Like 'SCR%' )
                                              group by Opportunity_ID__c,Opportunity_ID__r.CurrencyISOCode,CurrencyISOCode
                                             ];
        return queryResults;
    }
    
    /* Private method to Query c2g__codaCreditNote__c values, returning as AggregateResult */
    private List<AggregateResult> aggFFCreditNote(){
        List<AggregateResult> queryResults = [select c2g__Opportunity__c oId, c2g__Opportunity__r.CurrencyISOCode,CurrencyISOCode currISOCode, SUM(c2g__CreditNoteTotal__c) credTotal
                                              from c2g__codaCreditNote__c
                                              where c2g__Opportunity__c in :opptyMap.keySet()
                                              group by c2g__Opportunity__c,c2g__Opportunity__r.CurrencyISOCode,CurrencyISOCode
                                             ];
        return queryResults;
    }
    
    /* Private method to loop through AggregateResults, updating opptyMap values. Multiplier used to correct for ERP and FF records recording the Credit Note Value differently */
    private void updateOppMapFromAggTotals(List<AggregateResult> creditTotals, Integer multiplier){
    double createDate;
    ERP_BLNG_InvoiceAggregator agg = new ERP_BLNG_InvoiceAggregator();
        for (AggregateResult cur : creditTotals)  {
            if (opptyMap.containsKey((Id)cur.get('oId'))) {
                createDate = (double)cur.get('credTotal') * multiplier;
            
                 double amt = agg.convertCurrencyWithApexCode((String)cur.get('CurrencyISOCode'),(String)cur.get('currISOCode'),createDate);
                     system.debug('...............................'+amt);
                //opptyMap.get((Id)cur.get('oId')).Credited_To_Date__c += (Decimal)cur.get('credTotal') * multiplier;
                opptyMap.get((Id)cur.get('oId')).Credited_To_Date__c += amt;
                system.debug('........................'+ opptyMap.get((Id)cur.get('oId')).Credited_To_Date__c);
            }
        }
    }
   
}