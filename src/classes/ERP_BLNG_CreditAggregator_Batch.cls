/*
Developer: Lakshman Ace(sfdcace@gmail.com
Description:
	This class allows an administrator to update all or a subset of the opportunities in the
	database with their current value of Credited To Date.
*/
global class ERP_BLNG_CreditAggregator_Batch implements Database.Batchable<sObject> {	
	
	private final String queryString;
	
	/* This constructor allows you to specify the query string */
	public ERP_BLNG_CreditAggregator_Batch(String q) {
		queryString = q;
	}
	
	global Database.QueryLocator start(Database.BatchableContext BC){
		return Database.getQueryLocator(queryString);
	}
	
	global void execute(Database.BatchableContext BC, List<sObject> scope){
		ERP_BLNG_CreditAggregator rollup =
			new ERP_BLNG_CreditAggregator((List<Opportunity>) scope);
		rollup.updateCreditedToDate();
	}     
	
	global void finish(Database.BatchableContext BC) {}
}