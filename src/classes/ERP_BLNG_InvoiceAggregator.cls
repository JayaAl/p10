/*
Developer: Lakshman Ace(sfdcace@gmail.com)
Description:
This class aggregates and updates the Invoice line item totals for a given set of invoices,
then updates the related opportunities.  This class is utilized by the following classes and triggers:

Related Files:
ERP_BLNG_InvoiceAggregator_Batch.cls
ERP_BLNG_InvoiceRollups_Test.cls----To be Completed
TriggerOnERPInvoice.trigger
*/
public without sharing class ERP_BLNG_InvoiceAggregator {
    
    /* Variables */
    
    private Map<Id, Opportunity> opptyMap;
    
    /* Constructors */
    Public ERP_BLNG_InvoiceAggregator(){
    }
    // Constructor for triggers
    public ERP_BLNG_InvoiceAggregator(List<ERP_Invoice__c> newList, Map<Id, ERP_Invoice__c> oldMap) {
        // Get the ids of the opportunities that need updating. Also collect a set of the invoice ids
        Set<Id> opptyIdsToUpdate = new Set<Id>();
        for(ERP_Invoice__c newInvoice : newList) {
            ERP_Invoice__c oldInvoice = (oldMap == null)
                ? null : oldMap.get(newInvoice.id);
            if(oldInvoice == null || newInvoice.Invoice_Total__c != oldInvoice.Invoice_Total__c)
                opptyIdsToUpdate.add(newInvoice.Opportunity_ID__c);
        }
        
        opptyMap = new Map<Id, Opportunity>([
            select id, Invoiced_To_Date__c 
            from Opportunity 
            where Id in :opptyIdsToUpdate
        ]);
    }
    
    // Constructor for batch
    public ERP_BLNG_InvoiceAggregator(List<Opportunity> opptys) {
        opptyMap = new Map<Id, Opportunity>(opptys);
    }
    
    /* Public Methods */
    
    public void updateInvoicedToDate() {
        updateInvoicedToDate(opptyMap);
    }
    
    /* Support Methods */
    
    public void updateInvoicedToDate(Map<Id,Opportunity>opptyMap) {
        if(! opptyMap.isEmpty()) {
            //TO DO - Invoice Name should not start with SIN //Opportunity_Id__r.CurrencyISOCode curr,
            AggregateResult[] invoiceTotals = [select Opportunity_ID__c oId,Opportunity_Id__r.CurrencyISOCode, CurrencyISOCode currISOCode, SUM(Invoice_Total__c) invTotal
                                               from ERP_Invoice__c
                                               where Opportunity_ID__c in :opptyMap.keySet() AND (NOT Name Like 'SIN%')
                                               group by Opportunity_ID__c,Opportunity_Id__r.CurrencyISOCode,CurrencyISOCode];
            
            
            Set<Id> processedOppIds = new Set<Id>();
            if (invoiceTotals != null && !invoiceTotals.isEmpty()) {
                for (AggregateResult cur : invoiceTotals)  {
                    if (opptyMap.containsKey((Id)cur.get('oId'))) {
                    double invAmt =(double)cur.get('invTotal');
                    double amt = convertCurrencyWithApexCode((String)cur.get('CurrencyISOCode'),(String)cur.get('currISOCode'),invAmt);
                    opptyMap.get((Id)cur.get('oId')).Invoiced_To_Date__c = amt;
                       // opptyMap.get((Id)cur.get('oId')).Invoiced_To_Date__c = (Decimal)cur.get('invTotal');
                        processedOppIds.add((Id)cur.get('oId'));
                    }
                }   
                AggregateResult[] invoiceTotals2 = [select c2g__Opportunity__c oId,c2g__Opportunity__r.CurrencyISOCode,CurrencyISOCode currISOCode, SUM(c2g__InvoiceTotal__c) invTotal
                                                    from c2g__codaInvoice__c
                                                    where c2g__Opportunity__c in :opptyMap.keySet()
                                                    group by c2g__Opportunity__c,c2g__Opportunity__r.CurrencyISOCode,CurrencyISOCode];
                for (AggregateResult cur : invoiceTotals2)  {
                    if (opptyMap.containsKey((Id)cur.get('oId')) && processedOppIds.contains((Id)cur.get('oId'))) {
                         double invAmt =(double)cur.get('invTotal');
                        double amt = convertCurrencyWithApexCode((String)cur.get('CurrencyISOCode'),(String)cur.get('currISOCode'),invAmt);
                        
                        opptyMap.get((Id)cur.get('oId')).Invoiced_To_Date__c = (opptyMap.get((Id)cur.get('oId')).Invoiced_To_Date__c <> null) ? 
                            (opptyMap.get((Id)cur.get('oId')).Invoiced_To_Date__c + amt) : 
                        opptyMap.get((Id)cur.get('oId')).Invoiced_To_Date__c;
                        
                    }
                }   
                
            } else {
                AggregateResult[] invoiceTotals2 = [select c2g__Opportunity__c oId,c2g__Opportunity__r.CurrencyISOCode,CurrencyISOCode currISOCode, SUM(c2g__InvoiceTotal__c) invTotal
                                                    from c2g__codaInvoice__c
                                                    where c2g__Opportunity__c in :opptyMap.keySet()
                                                    group by c2g__Opportunity__c,c2g__Opportunity__r.CurrencyISOCode,CurrencyISOCode];
                for (AggregateResult cur : invoiceTotals2)  {
                    if (opptyMap.containsKey((Id)cur.get('oId')) ) {
                         double invAmt =(double)cur.get('invTotal');
                        double amt = convertCurrencyWithApexCode((String)cur.get('CurrencyISOCode'),(String)cur.get('currISOCode'),invAmt);
                        
                        opptyMap.get((Id)cur.get('oId')).Invoiced_To_Date__c = amt;
                        //opptyMap.get((Id)cur.get('oId')).Invoiced_To_Date__c = (Decimal)cur.get('invTotal');
                        processedOppIds.add((Id)cur.get('oId'));
                    }
                }
            }
            for(Opportunity opp: opptyMap.values()) {
                if(!processedOppIds.contains(opp.Id))
                    opp.Invoiced_To_Date__c = 0;
            }
            update opptyMap.values();
        }
    }
     public Double convertCurrencyWithApexCode(String oCurrency, String nCurrency, Double amount){
        Set<String> isoCodes = new Set<String>();
        Map<String,Double> conversion_rates = new Map<String,Double>();
        Double conversionRate;
         System.debug('oCurrency====='+oCurrency);
          System.debug('nCurrency====='+nCurrency);
           System.debug('amount$$$$$$$$'+amount);
        isoCodes.add(oCurrency);
        isoCodes.add(nCurrency);
        
        for(CurrencyType curr: [SELECT IsoCode,ConversionRate 
                                             FROM CurrencyType 
                                             WHERE IsoCode in: isoCodes]){          
            conversion_rates.put(curr.IsoCode,curr.ConversionRate);        
        }
        //Convert incoming Opportuntiy Amount into USD...
        //conversionRate = conversion_rates.get(oCurrency);
        //amount = amount / conversionRate ;
        
        //convert amount as per new currency. 
        if(nCurrency != 'USD'){
            conversionRate = conversion_rates.get(nCurrency);
            amount = amount * conversionRate;
        }
        
        System.debug('amount====='+amount);
        return amount;
    }
}