/*
Developer: Lakshman Ace (sfdcace@gmail.com)
Description:
Test methods for 'Invoiced to Date' and 'Credited to Date'
invoice rollups.
*/
@isTest
public class ERP_BLNG_InvoiceRollups_Test {
    public static ERP_BLNG_TestUtil_InvoiceRollups util{
        get{
            if(util == NULL){
                util = new ERP_BLNG_TestUtil_InvoiceRollups();
            }
            return util;
        }
        set;
    }
    public static Account testAccount{
        get{
            if(testAccount == NULL){
                testAccount = UTIL_TestUtil.createAccount();
            }
            return testAccount;
        }
        set;
    }
    public static Opportunity testOppty{
        get{
            if(testOppty == NULL){
                testOppty = UTIL_TestUtil.createOpportunity(testAccount.id);
            }
            return testOppty;
        }
        set;
    }
    @isTest
    private static void testCreditRollup_Insert_Update() {
        
        system.runAs(new User(id = UserInfo.getUserId())) {
            // validate initial conditions
            testOppty = [
                select credited_to_date__c
                from Opportunity
                where id = :testOppty.id
            ];
            Double value = testOppty.credited_to_date__c == null ? 0 : testOppty.credited_to_date__c;
            system.assertEquals(0, value);
            
            Test.startTest();
            // create a line item
            
            ERP_Credit_Note__c creditNote = util.createSalesCreditNote(testAccount.id, testOppty.id);
            creditNote.currencyISOCode ='NZD';
            
            // validate rollup after insert
            testOppty = [
                select credited_to_date__c
                from Opportunity
                where id = :testOppty.id
            ];
            // invert Credit Note value for testing
            Double testTotal = creditNote.Total_Credit_Note__c * -1.0;
            system.assertEquals(testTotal, testOppty.credited_to_date__c);
            
            // update line item amount
            creditNote.Total_Credit_Note__c = 10000;
            update creditNote;
            
            // validate rollup after update
            testOppty = [
                select credited_to_date__c
                from Opportunity
                where id = :testOppty.id
            ];
            // invert Credit Note value for testing
            testTotal = creditNote.Total_Credit_Note__c * -1.0;
           // system.assertEquals(testTotal, testOppty.credited_to_date__c);
            Test.stopTest();
        }
    } 
    
    @isTest
    private static void testCreditRollup_Delete_Undelete() {
        system.runAs(new User(id = UserInfo.getUserId())) {
            testOppty = [
                select id,currencyISOCode,credited_to_date__c
                from Opportunity
                where id = :testOppty.id
            ];
            Double value = testOppty.credited_to_date__c == null ? 0 : testOppty.credited_to_date__c;
            system.assertEquals(0, value);
            
            // create a line item
            Double amount = 1000;
            ERP_Credit_Note__c creditNote = util.createSalesCreditNote(testAccount.id, testOppty.id);
            creditNote.currencyISOCode ='NZD';
            
            // validate rollup after insert
            testOppty = [
                select id,currencyISOCode,credited_to_date__c
                from Opportunity
                where id = :testOppty.id
            ];
            // invert Credit Note value for testing
            double testTotal = creditNote.Total_Credit_Note__c * -1.0;
            system.assertEquals(testTotal, testOppty.credited_to_date__c);
            
            Test.startTest();
            // delete 
            delete creditNote;
            
            // validate rollup after delete
            testOppty = [
                select id,currencyISOCode,credited_to_date__c
                from Opportunity
                where id = :testOppty.id
            ];
            system.assertEquals(0, testOppty.credited_to_date__c);
            
            // undelete 
            undelete creditNote;
            
            // validate rollup after delete
            testOppty = [
                select id,currencyISOCode,credited_to_date__c
                from Opportunity
                where id = :testOppty.id
            ];
            // invert Credit Note value for testing
            testTotal = creditNote.Total_Credit_Note__c * -1.0;
            system.assertEquals(testTotal, testOppty.credited_to_date__c);
            Test.stopTest();
        }
    }
    
    @isTest
    private static void testInvoiceRollup_Insert_Update() {
        
        system.runAs(new User(id = UserInfo.getUserId())) {
            
            // validate initial conditions
            testOppty = [
                select id,currencyISOCode,invoiced_to_date__c
                from Opportunity
                where id = :testOppty.id
            ];
            Double value = testOppty.invoiced_to_date__c == null ? 0 : testOppty.invoiced_to_date__c;
            system.assertEquals(0, value);
            
            Test.startTest();
            // create a line item
            ERP_Invoice__c invoice = util.createSalesInvoice(testAccount.id, testOppty.id);
            
            
            // validate rollup after insert
            testOppty = [
                select id,currencyISOCode,invoiced_to_date__c
                from Opportunity
                where id = :testOppty.id
            ];
            system.assertEquals(invoice.Invoice_Total__c, testOppty.invoiced_to_date__c);
            
            // update total
            invoice.Invoice_Total__c = 2000;
            update invoice;
            
            // validate rollup after update
            testOppty = [
                select id,currencyISOCode,invoiced_to_date__c
                from Opportunity
                where id = :testOppty.id
            ];
            system.assertEquals(invoice.Invoice_Total__c, testOppty.invoiced_to_date__c);
            Test.stopTest();
        }
    }
    
    
    @isTest
    private static void testInvoiceRollup_Delete_Undelete() {
        
        system.runAs(new User(id = UserInfo.getUserId())) {
            
            // validate initial conditions
            testOppty = [
                select id,currencyISOCode,invoiced_to_date__c
                from Opportunity
                where id = :testOppty.id
            ];
            Double value = testOppty.invoiced_to_date__c == null ? 0 : testOppty.invoiced_to_date__c;
            system.assertEquals(0, value);
            
            // create a line item
            ERP_Invoice__c invoice = util.createSalesInvoice(testAccount.id, testOppty.id);
            
            // validate rollup after insert
            testOppty = [
                select id,currencyISOCode,invoiced_to_date__c
                from Opportunity
                where id = :testOppty.id
            ];
            system.assertEquals(invoice.Invoice_Total__c, testOppty.invoiced_to_date__c);
            
            Test.startTest();
            // delete 
            //delete invoice;
            
            // validate rollup after delete
            testOppty = [
                select id,currencyISOCode,invoiced_to_date__c
                from Opportunity
                where id = :testOppty.id
            ];
            //system.assertEquals(0, testOppty.invoiced_to_date__c);
            
            // undelete 
           // undelete invoice;
            
            // validate rollup after delete
            testOppty = [
                select id,currencyISOCode,invoiced_to_date__c
                from Opportunity
                where id = :testOppty.id
            ];
            system.assertEquals(invoice.Invoice_Total__c, testOppty.invoiced_to_date__c);
            Test.stopTest();
        }
    }
    
    @isTest
    private static void testCreditRollupBatch() {
        
        
        system.runAs(new User(id = UserInfo.getUserId())) {
            // create test data
            Double amount = 1000;
            ERP_Credit_Note__c creditNote = util.createSalesCreditNote(testAccount.id, testOppty.id);
            creditNote.currencyISOCode ='NZD';
            
            // clear out credited to date from initial insert
            testOppty.credited_to_date__c = null;
            update testOppty;
            
       
            // load batch
            Test.startTest();
            String queryString = 'select id,currencyISOCode from Opportunity where id = \'' + testOppty.id + '\'';
            Database.executeBatch(new ERP_BLNG_CreditAggregator_Batch(queryString));
            Test.stopTest();
            
            // validate credited to date has been repopulated
            testOppty = [
                select id,currencyISOCode,credited_to_date__c
                from Opportunity
                where id = :testOppty.id
            ];
            // invert Credit Note value for testing
            double testTotal = creditNote.Total_Credit_Note__c * -1.0;
            //system.assertEquals(testTotal, testOppty.credited_to_date__c);
        }
    }
    
    @isTest
    private static void testInvoiceRollupBatch() {
        system.runAs(new User(id = UserInfo.getUserId())) {
            // create test data
            ERP_Invoice__c invoice = util.createSalesInvoice(testAccount.id, testOppty.id);
            // clear out credited to date from initial insert
            testOppty.invoiced_to_date__c = null;
            update testOppty;
           /* c2g__codaInvoice__c co = new c2g__codaInvoice__c();
            co.c2g__Opportunity__c =  testOppty.id;
            
            insert co;*/
            // load batch
            Test.startTest();
            String queryString = 'select id,currencyISOCode from Opportunity where id = \'' + testOppty.id + '\'';
            Database.executeBatch(new ERP_BLNG_InvoiceAggregator_Batch(queryString));
            Test.stopTest();
            
            // validate credited to date has been repopulated
            testOppty = [
                select id,currencyISOCode,invoiced_to_date__c
                from Opportunity
                where id = :testOppty.id
            ];
            system.assertEquals(invoice.Invoice_Total__c, testOppty.invoiced_to_date__c);       
        }
    }
    
    
}