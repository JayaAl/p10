/*
Developer: Lakshman Ace (sfdcace@gmail.com)
Description:
    Utility method for testing invoice rollups.
    
    
*/
@isTest
public class ERP_BLNG_TestUtil_InvoiceRollups {


    /* Variables */

    public Id testUserId = UserInfo.getUserId();

    /* Setup Methods */

    
    /* Object Creation Methods */
    
    // Sales Credit Notes
    public ERP_Credit_Note__c createSalesCreditNote(Id accountId, Id opportunityId) {
        ERP_Credit_Note__c salesCreditNote = generateSalesCreditNote(accountId, opportunityId);
        insert salesCreditNote;
        return salesCreditNote;
    }
    public ERP_Credit_Note__c generateSalesCreditNote(Id accountId, Id opportunityId) {
        return new ERP_Credit_Note__c(
              Total_Credit_Note__c = 2000
            , Opportunity_ID__c = opportunityId
            , Name = 'Testing 987'
        );
    }
    
    
    // Sales Invoices
    public ERP_Invoice__c createSalesInvoice(Id accountId, Id opportunityId) {
        ERP_Invoice__c salesInvoice = generateSalesInvoice(accountId, opportunityId);
        insert salesInvoice;
        return salesInvoice;
    }
    public ERP_Invoice__c generateSalesInvoice(Id accountId, Id opportunityId) {
        return new ERP_Invoice__c(
              Opportunity_ID__c = opportunityId
            , Invoice_Total__c = 1000
			, Name = 'Testing 124'
        );
    }
    
    
    

    /* Test Methods */

    @isTest
    private static void testCreationFunctions() {
        ERP_BLNG_TestUtil_InvoiceRollups util = new ERP_BLNG_TestUtil_InvoiceRollups();

        // avoid mixed dml errors
        system.runAs(new User(id = util.testUserId)) {
		Test.startTest();
            Account testAccount = UTIL_TestUtil.createAccount();
        	Opportunity testOpportunity = UTIL_TestUtil.createOpportunity(testAccount.id);
        Test.stopTest();
            ERP_Invoice__c testInvoice = util.createSalesInvoice(testAccount.id, testOpportunity.id);
            system.assertNotEquals(null, testInvoice.id);
            ERP_Credit_Note__c testCreditNote = util.createSalesCreditNote(testAccount.id, testOpportunity.id);
            system.assertNotEquals(null, testCreditNote.id);
        }
    }

}