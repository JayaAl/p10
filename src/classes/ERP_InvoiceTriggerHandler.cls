public class ERP_InvoiceTriggerHandler{

    // public ERP_InvoiceTriggerHandler(){}

    // before update, no DML operation necessary as this happens before update
    public static void onBeforeUpdate(Map<Id,ERP_Invoice__c> newMap, Map<Id,ERP_Invoice__c> oldMap){
        // gather any Accounts that were changed
        Set<Id> setChangedAccounts = new Set<Id>();
        for(Id i:newMap.keySet()){
        	// Advertiser__c
            if(newMap.get(i).Advertiser__c != oldMap.get(i).Advertiser__c){
                setChangedAccounts.add(newMap.get(i).Advertiser__c);
            }
        	// Company__c
        	if(newMap.get(i).Company__c != oldMap.get(i).Company__c){
                setChangedAccounts.add(newMap.get(i).Company__c);
            }
        }
        // Query [Select Id from Account where isDeleted = FALSE]
        Map<Id, Account> mapCurrentAccounts = new Map<Id, Account>([Select Id from Account where Id IN :setChangedAccounts AND isDeleted = FALSE]);
    
        // Loop back through newMap, check the Accounts. If they exist leave alone, otherwise revert to their prior values
        for(Id i:newMap.keySet()){
        	// Advertiser__c
            if(!mapCurrentAccounts.containsKey(newMap.get(i).Advertiser__c)){
                 newMap.get(i).Advertiser__c = oldMap.get(i).Advertiser__c;
            }
        	// Company__c
            if(!mapCurrentAccounts.containsKey(newMap.get(i).Company__c)){
                newMap.get(i).Company__c = oldMap.get(i).Company__c;
            }
        }
    }
}