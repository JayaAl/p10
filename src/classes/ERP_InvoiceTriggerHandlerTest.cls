@isTest
public class ERP_InvoiceTriggerHandlerTest {

    
    //onBeforeUpdate
    // Create new ERP_Invoice__c
    // update ERP_Invoice__c, do not change Accounts
    // update ERP_Invoice__c, change Accounts to another existing Account
    // update ERP_Invoice__c, change Accounts to a deleted Account
	
    static testmethod void testupdateInvoiceNoAccount(){
        ERP_InvoiceTriggerHandlerTest mytest = new ERP_InvoiceTriggerHandlerTest();
        mytest.setupTestData();
        mytest.sampleInvoice.BATCH_ID__c = UTIL_TestUtil.generateRandomString(10);
        update mytest.sampleInvoice; // Should not cause an error
    }
    static testmethod void testupdateInvoice(){
        ERP_InvoiceTriggerHandlerTest mytest = new ERP_InvoiceTriggerHandlerTest();
        mytest.setupTestData();
        mytest.sampleInvoice.Advertiser__c = mytest.toKeep.Id;
        mytest.sampleInvoice.Company__c = mytest.toKeep.Id;
        update mytest.sampleInvoice; // Should not cause an error
        
        List<ERP_Invoice__c> theList = [Select Id, Advertiser__c, Company__c from ERP_Invoice__c];
        system.assert(theList[0].Advertiser__c == mytest.toKeep.Id);
        system.assert(theList[0].Company__c == mytest.toKeep.Id);
    }
    static testmethod void testupdateInvoiceKeepAcct(){
        ERP_InvoiceTriggerHandlerTest mytest = new ERP_InvoiceTriggerHandlerTest();
        mytest.setupTestData();
        mytest.sampleInvoice.Advertiser__c = mytest.toKeep.Id;
        mytest.sampleInvoice.Company__c = mytest.toKeep.Id;
        update mytest.sampleInvoice; // Should not cause an error
        
        test.startTest();
            mytest.sampleInvoice.Advertiser__c = mytest.toKeep2.Id;
            mytest.sampleInvoice.Company__c = mytest.toKeep2.Id;
            update mytest.sampleInvoice; // Should not cause an error
        test.stopTest();
        
        List<ERP_Invoice__c> theList = [Select Id, Advertiser__c, Company__c from ERP_Invoice__c];
        system.assert(theList[0].Advertiser__c == mytest.toKeep2.Id);
        system.assert(theList[0].Company__c == mytest.toKeep2.Id);
    }
    static testmethod void testcreateInvoiceDelAccount(){
        ERP_InvoiceTriggerHandlerTest mytest = new ERP_InvoiceTriggerHandlerTest();
        mytest.setupTestData();
        mytest.sampleInvoice.Advertiser__c = mytest.toKeep.Id;
        mytest.sampleInvoice.Company__c = mytest.toKeep.Id;
        update mytest.sampleInvoice; // Should not cause an error
        Id deletedId = mytest.toDelete.Id;
        delete mytest.toDelete;
        
        test.startTest();
            mytest.sampleInvoice.Advertiser__c = mytest.toDelete.Id;
            mytest.sampleInvoice.Company__c = mytest.toDelete.Id;
            update mytest.sampleInvoice; // Should not cause an error
        test.stopTest();
        
        List<ERP_Invoice__c> theList = [Select Id, Advertiser__c, Company__c from ERP_Invoice__c];
        system.assert(theList[0].Advertiser__c == mytest.toKeep.Id);
        system.assert(theList[0].Company__c == mytest.toKeep.Id);
    }    
    
    private Account toKeep;
	private Account toKeep2;
    private Account toDelete;
    private ERP_Invoice__c sampleInvoice;
    private void setupTestData(){
        toKeep = UTIL_TestUtil.createAccount();
        toKeep2 = UTIL_TestUtil.createAccount();
        toDelete = UTIL_TestUtil.createAccount();
        sampleInvoice = new ERP_Invoice__c();
        insert sampleInvoice;
    }
}