/*
 * @name: ERP_PrePaymentTrackerExt
 * @desc: This is the controller class for page ERP_PrePaymentTracker. It serves for all remoting actions invoked from the page.
 *        Test  Class= ERP_PrePaymentTrackerExtTest
 * @author: Lakshman(sfdcace@gmail.com)
 * @date: 12-01-2016
 */

global with sharing class ERP_PrePaymentTrackerExt {
    private static String exceptionRctDate = 'Receipt Date cannot be in the future!';
    private static String exceptionSchedAmt = 'The Total Schedule Amount cannot be greater than Opportunity Amount!';
    private static String exceptionSchedDate = 'Schedule Payment Date can not fall outside of the Campaign Dates!';
    private static String exceptionRctDataMissing = 'Both Receipt Date and Receipt Amount are required when entering a receipt!';
    private static String exceptionRctConfirmNumMissing = 'Receipt Confirmation # is required when "Receipt Amount" or "Receipt Date" are populated';
    private static String exceptionRequirePayType = 'Payment Type is required when Receipt Amount or Receipt Date are present!';
    private static String exceptionOmitPayType = 'Payment Type must remain blank when Receipt Amount or Receipt Date are empty!';
    private static String exceptionSchedDataMissing = 'Schedule Payment Date and/or Schedule Payment Amount may not be empty!';
    
    public String IODetailList { get; set; }
    public static IO_Detail__c objIO{get;set;}
    public static boolean readOnly{get;set;}
    public static List<RJWrapper> lstwrap{get;set;}    
    public static List<SelectOption> getPaymentTypes(){
        List<SelectOption> options = new List<SelectOption>();
        Schema.DescribeFieldResult fieldResult = Receipt__c.Payment_Type__c.getDescribe();
        List<Schema.PicklistEntry> ple = fieldResult.getPicklistValues();
        for( Schema.PicklistEntry f : ple){
            options.add(new SelectOption(f.getLabel(), f.getValue(),f.getLabel()=='Portal'));
        }   
        return options;
    }
    private static Set<String> readOnlyPaymentTerms = new Set<String>{'Pre-Pay'}; // {'Pre-Pay','two','three'...}
        
    global ERP_PrePaymentTrackerExt() {
        readOnly = false;
        objIO = [
            Select Id, Name, Total_Paid_Amount_From_Portal__c, Total_IO_Amount__c, Payment_Terms_Preferred__c,
                Contract_Start_Date__c, Contract_End_Date__c
            from IO_Detail__c  
            where Id =: ApexPages.currentPage().getParameters().get('io_id')
        ];            
        // Only populate display for the proper IO Payment Terms, else readonly and no details provided. 
        lstwrap = new List<RJWrapper>();
        if(!readOnlyPaymentTerms.contains(objIO.Payment_Terms_Preferred__c)){
            readOnly = true;
        }
    }
    
    //Subclass : Wrapper Class 
    global class RJWrapper {
        //Static Variables 
        global string id;
        global string name;
        global string receiptName;
        global string paidAmount;
        global string paymentStatus;
        global string ioDetailId;
        global String receiptId;
        global string paymentType;
        global string receiptDate;
        global string scheduleDate;
        global string scheduleAmount;
        global string notes;
        global string ccAuth;
        global Boolean ccLocked;
        global string receiptConfirm;
        global string oppName;
        global string oppId;
        global string ccNotes;
        global string csNotes;
        global string placeholder;
        //Wrapper  Class Controller
        public RJWrapper() {
            paidAmount = '0';
            paymentStatus = '';
            scheduleAmount = '0';
            notes = '';
        }
    }
    public static String getobjIODetails() {
        return JSON.serialize(objIO);
    }
    //Method to bring the list of IO Detail and Serialize Wrapper Object as JSON
    public  static String getlstRJWrap() {
        lstwrap = new List<RJWrapper>();
        if(!readOnly){
            List<Receipt_Junction__c > lstio = [
                SELECT Id, Name, Amount__c, Scheduled_Payment_Amount__c , Scheduled_Payment_Date__c, 
                    Credit_Collections_Notes__c, Client_Services_Notes__c,Receipt_Confirmation__c,
                    IO_Approval__r.Name, IO_Approval__r.Total_Paid_Amount_From_Portal__c, 
                    IO_Approval__r.Total_IO_Amount__c, IO_Approval__r.PortalPaymentStatus__c,
                    IO_Approval__r.Payment_Terms_Preferred__c,
                    IO_Approval__r.Opportunity__c, IO_Approval__r.Opportunity__r.Name, 
                    IO_Approval__r.Opportunity__r.Amount,
                    Receipt__c, Receipt__r.Name, Receipt__r.Payment_Type__c,
                    Receipt__r.Notes__c, Receipt__r.Date_Of_Transaction__c, Receipt__r.Authorization__c
                FROM Receipt_Junction__c 
                WHERE IO_Approval__c =: objIO.Id
                ORDER by Receipt__r.Name, Receipt__r.Payment_Type__c
            ];
            for (Receipt_Junction__c a: lstio) {
                // Populate the wrapper list
                RJWrapper awrap = new RJWrapper();
                    awrap.id = a.id;
                    awrap.name = a.name;
                    awrap.receiptName = a.Receipt__r.Name;
                    awrap.receiptId = a.Receipt__c;
                    awrap.ioDetailId = objIO.Id;
                    if (a.IO_Approval__r.PortalPaymentStatus__c != null) {
                        awrap.paymentStatus = a.IO_Approval__r.PortalPaymentStatus__c;
                    }
                    if (a.Amount__c != null) {
                        awrap.paidAmount = String.valueOf(a.Amount__c);
                    }
                    awrap.paymentType       = a.Receipt__r.Payment_Type__c              != null ? a.Receipt__r.Payment_Type__c : '';
                    awrap.notes             = a.Receipt__r.Notes__c                     != null ? a.Receipt__r.Notes__c : '';
                    awrap.ccAuth            = a.Receipt__r.Authorization__c             != null ? a.Receipt__r.Authorization__c : '';
                    awrap.ccLocked          = a.Receipt__r.Authorization__c             != null ? true : false;
                    awrap.receiptConfirm    = a.Receipt_Confirmation__c                 != null ? a.Receipt_Confirmation__c : '';
                    awrap.scheduleDate      = a.Scheduled_Payment_Date__c               != null ? String.valueOf(a.Scheduled_Payment_Date__c)  : '';
                    awrap.scheduleAmount    = a.Scheduled_Payment_Amount__c             != null ? String.valueOf(a.Scheduled_Payment_Amount__c)  : '';
                    awrap.receiptDate       = a.Receipt__r.Date_Of_Transaction__c       != null ? String.valueOf(a.Receipt__r.Date_Of_Transaction__c)  : '';
                    awrap.ccNotes           = a.Credit_Collections_Notes__c             != null ? String.valueOf(a.Credit_Collections_Notes__c)  : '';
                    awrap.csNotes           = a.Client_Services_Notes__c                != null ? String.valueOf(a.Client_Services_Notes__c)  : '';
                    awrap.oppId             = a.IO_Approval__r.Opportunity__c           != null ? String.valueOf(a.IO_Approval__r.Opportunity__c)  : '';
                    awrap.oppName           = a.IO_Approval__r.Opportunity__r.Name      != null ? String.valueOf(a.IO_Approval__r.Opportunity__r.Name)  : '';
                lstwrap.add(awrap);
            }
        } else {
            lstwrap.add(new RJWrapper()); // add an empty wrapper to the list
        }
        return JSON.serialize(lstwrap);
     }
    
    @RemoteAction 
    global static IO_Detail__c fetchIODetail(String ioDetailId) {
        return [
            Select Id, Name, Total_Paid_Amount_From_Portal__c, Total_IO_Amount__c 
            from IO_Detail__c  
            where Id =: ioDetailId
        ];
    }
    
    @RemoteAction
    global static RJWrapper createReceipt(RJWrapper wrap, IO_Detail__c ioDetail){
        system.debug('****wrap' + wrap);
        
        if(validateInsertUpdate(wrap, ioDetail)){ 
            return null; // Validate records, on error return null
        } else {
            Receipt__c receipt = new Receipt__c();
            receipt.Date_Of_Transaction__c          = wrap.receiptDate <> null      ? Date.valueOf(wrap.receiptDate)        : null;
            receipt.Notes__c                        = wrap.notes <> null                ? wrap.notes                            : null;
            receipt.Payment_Type__c                 = wrap.paymentType <> null      ? wrap.paymentType                      : null;
            receipt.Notes__c                        = wrap.notes <> null                ? wrap.notes                            : null;
            // DO NOT WRITE THIS VALUE, DOING SO WILL LOSE OUR RECORD OF PORTAL CREDIT CARD PAYMENTS
            // receipt.Authorization__c             = wrap.ccAuth <> null           ? wrap.ccAuth                           : null;
            
            insert receipt;
            
            Receipt__c r = [Select Id, Name from Receipt__c where Id= :receipt.Id]; // gather additional details from the new Receipt__c 
            wrap.receiptName = r.Name;
            wrap.receiptId = r.Id;
            // wrap.paymentType = 'Check';
            
            Receipt_Junction__c rjWrap = new Receipt_Junction__c(
                Receipt__c=receipt.Id,
                Opportunity__c = ioDetail.Opportunity__c
            );
            rjWrap.Amount__c = wrap.paidAmount <> null ? Decimal.valueOf(wrap.paidAmount) : null;
            rjWrap.IO_Approval__c = wrap.ioDetailId;
            rjWrap.Scheduled_Payment_Amount__c  = wrap.scheduleAmount <> null   ? Decimal.valueOf(wrap.scheduleAmount)  : null;
            rjWrap.Scheduled_Payment_Date__c    = wrap.scheduleDate <> null         ? Date.valueOf(wrap.scheduleDate)       : null; 
            rjWrap.Receipt_Confirmation__c      = wrap.receiptConfirm <> null   ? wrap.receiptConfirm                   : null;
            rjWrap.Client_Services_Notes__c     = wrap.csNotes <> null          ? wrap.csNotes                          : null;
            rjWrap.Credit_Collections_Notes__c  = wrap.ccNotes <> null          ? wrap.ccNotes                          : null;
            insert rjWrap;
            
            if(rjWrap.Amount__c <> null) {
                // Update IO_Detail Total_Paid_Amount_From_Portal__c
                updateIOTotalPaid(ioDetail.Id);
            }
            Receipt_Junction__c rj = [Select Id, Name from Receipt_Junction__c where Id = :rjWrap.Id];
            wrap.id = rj.Id;
            wrap.name = rj.Name;
            return wrap;
        }
    }
    @RemoteAction
    global static RJWrapper updateReceipt(RJWrapper wrap, IO_Detail__c ioDetail){
        
        system.debug('****wrap' + wrap);
        system.debug('****ioDetail' + ioDetail);
        
        if(validateInsertUpdate(wrap, ioDetail)){ 
            return null; // Validate records, on error return null
        } else {
            Receipt__c receipt = new Receipt__c(Id=wrap.receiptId);
            receipt.Payment_Type__c = wrap.paymentType <> null ? wrap.paymentType : null;
            receipt.Date_Of_Transaction__c = wrap.receiptDate <> null ? Date.valueOf(wrap.receiptDate) : null;
            receipt.Notes__c = wrap.notes <> 'null' ? wrap.notes : null;
            // receipt.Authorization__c = wrap.ccAuth <> 'null' ? wrap.ccAuth : null;  // DO NOT WRITE THIS VALUE, DOING SO WILL LOSE OUR RECORD OF PORTAL CREDIT CARD PAYMENTS
            update receipt;
            
            Receipt_Junction__c rjWrap = new Receipt_Junction__c(Id=wrap.Id);
            rjwrap.Receipt_Confirmation__c = wrap.receiptConfirm <> null ? wrap.receiptConfirm : null;
            rjWrap.Client_Services_Notes__c = wrap.csNotes <> null ? wrap.csNotes : null;
            rjWrap.Credit_Collections_Notes__c = wrap.ccNotes <> null ? wrap.ccNotes : null;
            rjWrap.Scheduled_Payment_Amount__c = wrap.scheduleAmount <> null ? Decimal.valueOf(wrap.scheduleAmount) : null;
            rjWrap.Scheduled_Payment_Date__c = wrap.scheduleDate <> null ? Date.valueOf(wrap.scheduleDate) : null;
            rjWrap.Amount__c = wrap.paidAmount <> null ? Decimal.valueOf(wrap.paidAmount) : null;
            update rjWrap;
            if(rjWrap.Amount__c <> null) {
                //we need to query the result again
                // Update IO_Detail Total_Paid_Amount_From_Portal__c
                updateIOTotalPaid(ioDetail.Id);
            }
            return wrap;
        }
    }
    @RemoteAction
    global static void deleteReceipt(string id,string receiptId, String ioDetailId){
        Receipt__c receipt = [
            select id 
            from Receipt__c 
            where id =: receiptId
        ];
        delete receipt;
        
        // Update IO_Detail Total_Paid_Amount_From_Portal__c
        updateIOTotalPaid(ioDetailId);
    }
    
    private static void updateIOTotalPaid(Id ioDetailId){
        // Update IO_Detail Total_Paid_Amount_From_Portal__c
        AggregateResult[] ar = [
            SELECT SUM(Amount__c) Paid_Amount , MAX(IO_Approval__r.Total_IO_Amount__c) IO_Total
            FROM Receipt_Junction__c 
            WHERE IO_Approval__c =: ioDetailId
        ];

        Decimal Paid_Amount = (Decimal)ar[0].get('Paid_Amount');
        Decimal IO_Total = (Decimal)ar[0].get('IO_Total');
        
        String PaymentStatus = 'Unpaid';
        if(Paid_Amount >= IO_Total){
            PaymentStatus = 'Paid';
        } else if(Paid_Amount > 0.00){
            PaymentStatus = 'Partial Paid';
        }
        
        IO_Detail__c ioDetail = new IO_Detail__c(
            Id=ioDetailId, 
            PortalPaymentStatus__c = PaymentStatus,
            Total_Paid_Amount_From_Portal__c = Paid_Amount
        );
        update ioDetail;
    }
    
    private static boolean validateInsertUpdate(RJWrapper wrap, IO_Detail__c ioDetail){
        boolean hasErrors = false;
        Date rctDate = wrap.receiptDate <> null ? Date.valueOf(wrap.receiptDate) : null;
        Date schedDate = wrap.scheduleDate <> null ? Date.valueOf(wrap.scheduleDate) : null;
        String paymentType = wrap.paymentType <> null ? wrap.paymentType : null;
        Decimal thisPaidAmount = wrap.paidAmount <> null ? Double.valueOf(wrap.paidAmount) : null; // paidAmount = 'Receipt Amount'
        
        
            
        // Schedule Payment Date and/or Schedule Payment Amount may not be empty
        if(
            String.isBlank(wrap.scheduleDate) 
            || String.isBlank(wrap.scheduleAmount)
            || String.isBlank(wrap.scheduleDate) 
            || String.isBlank(wrap.scheduleAmount)
        ){
            throw new RequiredFieldException(exceptionSchedDataMissing);
            hasErrors = true; 
        }
        
        // Amount cannot be > opportunity amount
        AggregateResult[] ar = [
            Select SUM(Scheduled_Payment_Amount__c), SUM(Amount__c) 
            from Receipt_Junction__c 
            where IO_Approval__c =: ioDetail.Id
            AND Id != :wrap.Id
        ];
        Decimal sumSchedAmt = (Decimal)ar.get(0).get('expr0') <> null ? (Decimal)ar.get(0).get('expr0') : 0;
        Decimal  totalSchAmt = sumSchedAmt + (
            wrap.scheduleAmount <> 'null' 
            ? Decimal.valueOf(wrap.scheduleAmount) 
            : 0
        );
        if(totalSchAmt > ioDetail.Total_IO_Amount__c) {
            throw new RequiredFieldException(exceptionSchedAmt);
            hasErrors = true; 
        }
        
        // Receipt Date cannot be > TODAY
        // system.assert(false,'found date: '+rctDate);
        if(rctDate!= null && rctDate > date.today()){
            throw new RequiredFieldException(exceptionRctDate);
            hasErrors = true;   
        }
        
        // "Schedule Payment Date" cannot be past campaign dates
        if(schedDate!= null && (schedDate < ioDetail.Opportunity__r.ContractStartDate__c || schedDate > ioDetail.Opportunity__r.ContractEndDate__c)){
            throw new RequiredFieldException(exceptionSchedDate);
            hasErrors = true; 
        }
        
        // Both Receipt Date and Receipt Amount are required when entering a receipt
        if(
            (rctDate!=null && thisPaidAmount == null)
            ||(rctDate==null && thisPaidAmount != null)
        ){
            throw new RequiredFieldException(exceptionRctDataMissing);
            hasErrors = true; 
        }
        
        // Receipt Confirmation # is required when "Receipt Amount" or "Receipt Date" are populated
        if(
            (rctDate!=null || thisPaidAmount != null)
            && String.isBlank(wrap.receiptConfirm)
        ){
            throw new RequiredFieldException(exceptionRctConfirmNumMissing);
            hasErrors = true; 
        }
        
        // Payment Type is required when "Receipt Amount" or "Receipt Date" are populated
        if(
            (rctDate!=null || thisPaidAmount != null) 
            && ( String.isBlank(paymentType) )
        ){
            throw new RequiredFieldException(exceptionRequirePayType);
            hasErrors = true; 
        }
        
        // Payment Type must remain blank when "Receipt Amount" or "Receipt Date" are empty
        /*
        if(
            (rctDate!=null || thisPaidAmount == null)
            && String.isNotBlank(paymentType)
        ){
            throw new RequiredFieldException(exceptionOmitPayType);
            hasErrors = true; 
        }
        */
        
        return hasErrors;
    }
}