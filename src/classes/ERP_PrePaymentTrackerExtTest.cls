/*
 * @name: ERP_PrePaymentTrackerExtTest
 * @desc: Test Class for ERP_PrePaymentTrackerExt, Code coverage 95%. 
 * @author: Lakshman(sfdcace@gmail.com)
 * @date: 28-01-2016
 */
@isTest
public class ERP_PrePaymentTrackerExtTest {
    static testMethod void myUnitTest () {
        Opportunity opp = UTIL_TestUtil.generateOpportunity(UTIL_TestUtil.createAccount().Id);
        opp.Amount = 10000;
        insert opp;
        IO_Detail__c io = new IO_Detail__c();
        io.Opportunity__c = opp.Id;
        io.Paperwork_Origin__c = 'Pandora Paperwork - no changes';
        io.Payment_Terms_Preferred__c = 'Pre-Pay';
        io.PortalPaymentStatus__c = 'Unpaid';
        insert io;
        io = [
            Select Total_IO_Amount__c, Id, Opportunity__c,Payment_Terms_Preferred__c, Total_Paid_Amount_From_Portal__c,
            	Opportunity__r.ContractStartDate__c, Opportunity__r.ContractEndDate__c
            from IO_Detail__c 
            where Id=: io.Id
        ];
        Test.startTest();
        ApexPages.currentPage().getParameters().put('io_id',io.Id);
        ERP_PrePaymentTrackerExt testExt = new ERP_PrePaymentTrackerExt();
        ERP_PrePaymentTrackerExt.RJWrapper awrap = new ERP_PrePaymentTrackerExt.RJWrapper();
        awrap.ioDetailId = io.Id;
        
        
        awrap.paidAmount = '1000';
        awrap.notes = 'Test Notes';
        awrap.scheduleDate = '2016-1-1';
        awrap.scheduleAmount = '1000';
        awrap.receiptDate = '2016-1-1';
        awrap.receiptConfirm = 'Sample Receipt Confirmation'; // Receipt Confirmation # is required when "Receipt Amount" or "Receipt Date" are populated.
        awrap.paymentType = 'Sample Payment Type'; // Payment Type is required when Receipt Amount or Receipt Date are present!
        
        
        ERP_PrePaymentTrackerExt.createReceipt(awrap,io);
        ERP_PrePaymentTrackerExt.fetchIODetail(io.Id);
        //re-initialize the constructor
        testExt = new ERP_PrePaymentTrackerExt();
        String str = ERP_PrePaymentTrackerExt.getlstRJWrap();
        // Deserialize the list of invoices from the JSON string.
        List<ERP_PrePaymentTrackerExt.RJWrapper> deserializedrjWrap = 
          (List<ERP_PrePaymentTrackerExt.RJWrapper>)JSON.deserialize(str, List<ERP_PrePaymentTrackerExt.RJWrapper>.class);
        System.assertEquals(1, deserializedrjWrap.size());
        ERP_PrePaymentTrackerExt.updateReceipt(deserializedrjWrap.get(0), io);
        ERP_PrePaymentTrackerExt.deleteReceipt(deserializedrjWrap.get(0).id, deserializedrjWrap.get(0).receiptId, io.Id);
        ERP_PrePaymentTrackerExt.getobjIODetails();
        ERP_PrePaymentTrackerExt.getPaymentTypes();
        Test.stopTest();
    }
}