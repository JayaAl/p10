/**
* @name: EmailServiceHandler
 * @desc: This Email Handler is Used to automate the process the contents, headers, and attachments of inbound email which contains CSV file
* @author: Priyanka Ghanta
* @date: 05/11/2015
*/
    
global with sharing class EmailServiceHandler  implements Messaging.InboundEmailHandler{

    global Messaging.InboundEmailResult handleInboundEmail(Messaging.InboundEmail email,Messaging.InboundEnvelope envelope) {

        Messaging.InboundEmailResult result = new Messaging.InboundEmailresult();

            //Processing field mapping from custom setting 
            Map<String,CSVFieldsMapping__c> fieldMapping= CSVFieldsMapping__c.getAll();
            
            
            //system.debug('*email*'+email);
            if(email.binaryAttachments <> null){
            //Save any Binary Attachment
            for (Messaging.Inboundemail.BinaryAttachment bAttachment : email.binaryAttachments) {
                
                //system.debug('**bAttachment**'+bAttachment.body);
                //if attachment is type of csv
                if(bAttachment.filename <> null && bAttachment.filename.endsWith('.csv')){
                    
                    string csvAsString = EmailServiceHandler.blobToUtfString(bAttachment.body,'UTF-8');
                    //String[]  csvFileLines = csvAsString.split('\n');
                    //system.debug('***csvAsString**'+csvAsString);
                    List<List<String>> parsedCSVRecords = EmailServiceHandler.parseCSV(csvAsString,false);
                    List<String> headers = parsedCSVRecords[0];
                    system.debug('**headers**'+headers);
                    //Map to store csv field header and it's column order
                    Map<String,Integer> mapofheaderfieldnameandcolumnorder = new Map<String,Integer>();
                    if(headers <> null && headers.size() > 0){
                        for(Integer csvColumnOrder=0;csvColumnOrder<headers.size() ; csvColumnOrder++){
                            String headerfieldname = headers[csvColumnOrder].replaceall('"','').trim();
                            
                            if(headerfieldname <> null){                                
                            //storing column order of the csv file in a map
                              mapofheaderfieldnameandcolumnorder.put(headerfieldname.toupperCase(),csvColumnOrder);
                            }
                        }
                    
                    }
                      //system.debug('***mapofheaderfieldnameandcolumnorder**'+mapofheaderfieldnameandcolumnorder);                                 
                     Map<String,Integer> mapofobjfieldapiandorder = new  Map<String,Integer>();
                                        
                    for(String csvfield : mapofheaderfieldnameandcolumnorder.keyset()){
                        Integer columnorder =mapofheaderfieldnameandcolumnorder.get(csvfield);
                        for(String fieldApi : fieldMapping.keyset()){
                          String csvheaderfieldname = fieldMapping.get(fieldApi).CSVFieldsHeader__c.touppercase();
                          csvheaderfieldname = csvheaderfieldname.replaceall(' ','').trim();
                          csvfield = csvfield.replaceall(' ','').trim();
                          // system.debug('***csvheaderfieldname**'+csvheaderfieldname+'***csvfield***'+csvfield);
                            if(csvheaderfieldname==csvfield){                           
                                mapofobjfieldapiandorder.put(fieldApi,columnorder);
                            }
                          }
                        system.debug('***columnorder***'+columnorder);
                    }
                    
                                        
                    system.debug('***mapofobjfieldapiandorder**'+mapofobjfieldapiandorder);
                    List<AdxLog__c> listofLogs = new List<AdxLog__c>();
                    
                    for(Integer recordindex=0 ;recordindex<parsedCSVRecords.size();recordindex++){
                        if(recordindex>0){
                            AdxLog__c newLog = new AdxLog__c();
                            //get all fields of log object
                            for(String adxLogFieldApiName : mapofobjfieldapiandorder.keyset()){
                                //system.debug('**csvHeader**'+csvHeader);
                                   //system.debug('**adxLogFieldApiName**'+csvHeader);
                                        String fieldValue = parsedCSVRecords[recordindex][mapofobjfieldapiandorder.get(adxLogFieldApiName)];
                                         String trimmedvalue = fieldValue.replaceall('\"','').trim();
                                  try{
                                        if(fieldMapping.get(adxLogFieldApiName).FieldType__c == 'STRING'){ 
                                          // String stringdata =    String.valueof(removeSpecialCharacter(trimmedvalue,'�'));
                                           newLog.put(adxLogFieldApiName,trimmedvalue);
                                        }
                                        else if(fieldMapping.get(adxLogFieldApiName).FieldType__c == 'DECIMAL'){
                                          DOUBLE doublefieldValue =     DOUBLE.valueof(trimmedvalue.replaceall(',',''));
                                          newLog.put(adxLogFieldApiName,doublefieldValue);
                                        }
                                        else if(fieldMapping.get(adxLogFieldApiName).FieldType__c == 'DATE'){
                                            Date newDate = Date.valueof(trimmedvalue);                                   
                                            newLog.put(adxLogFieldApiName,newDate);
                                            
                                        }
                                     }catch(Exception e){
                                        system.debug('*e**'+e.getmessage()+'-->'+adxLogFieldApiName+'--'+trimmedvalue);
                                    }                                     
                                   }                            
                            
                            //system.debug('***newLog***'+newLog);
                            listofLogs.add(newLog);
                        }
                    }
                    //system.debug('***listofLogs*'+listofLogs);
                    Database.insert(listofLogs);
                   
                }
            }
            }

        result.success = true;
        return result;
    }
    
 /*
 * @ CSV parse logic
 */   
 public static List<List<String>> parseCSV(String contents,Boolean skipHeaders) {
   //system.debug('**contents**'+contents);
       List<List<String>> allFields = new List<List<String>>();

    // replace instances where a double quote begins a field containing a comma
    // in this case you get a double quote followed by a doubled double quote
    // do this for beginning and end of a field
    contents = contents.replaceAll(',"""',',"DBLQT').replaceall('""",','DBLQT",');
    // now replace all remaining double quotes - we do this so that we can reconstruct
    // fields with commas inside assuming they begin and end with a double quote
    contents = contents.replaceAll('""','DBLQT');
    // we are not attempting to handle fields with a newline inside of them
    // so, split on newline to get the spreadsheet rows
    List<String> lines = new List<String>();
    try {
        lines = contents.split('\n');
    } catch (System.ListException e) {
        System.debug('Limits exceeded?' + e.getMessage());
    }
    Integer num = 0;
    for(String line : lines) {
        // check for blank CSV lines (only commas)
        if (line.replaceAll(',','').trim().length() == 0) break;
        //system.debug('*line**'+line);
        List<String> fields = line.split('\\t');    
        List<String> cleanFields = new List<String>();
        String compositeField;
        Boolean makeCompositeField = false;
        //System.deug('fields======='+fields);
        for(String field : fields) {
            field = field.trim();
            if (field.startsWith('"') && field.endsWith('"')) {
                cleanFields.add(field.replaceAll('DBLQT','"'));
            } else if (field.startsWith('"')) {
                makeCompositeField = true;
                compositeField = field;
            } else if (field.endsWith('"')) {
                compositeField += ',' + field;
                cleanFields.add(compositeField.replaceAll('DBLQT','"'));
                makeCompositeField = false;
            } else if (makeCompositeField) {
                compositeField +=  ',' + field;
            } else {
                cleanFields.add(field.replaceAll('DBLQT','"'));
            }
        }
        
        allFields.add(cleanFields);
    }
    if (skipHeaders) allFields.remove(0);
    return allFields;   
}  
    
    
 /*
 * @function to convert blob file to proper format like UTF-8
 */   
  public static String blobToUtfString(Blob input,String inCharset ){
    String hex = EncodingUtil.convertToHex(input);
    //System.assertEquals(0, hex.length() & 1);
    final Integer bytesCount = hex.length() >> 1;
    String[] bytes = new String[bytesCount];
    //system.debug('**bytes*'+bytes);
    for(Integer i = 0; i < bytesCount; ++i){
        bytes[i] =  hex.mid(i << 1, 2);
    }   
   return EncodingUtil.urlDecode('%' + String.join(bytes, '%'), inCharset);
 }
 
}