/*
*@Author: Abhishek 
 @Description : Used as util to send Email using apex
*
*/
public class EmailUtil{
    
    /*
    *@Author: Abhishek 
     @Description : Gemnerates email based on parameters and returns SingleEmailmessage Record    
    */  
    public static Messaging.SingleEmailMessage generateEmail( Id whatId, String senderDispName, Id templateId, List<String> toAddressLst , Id targetObjId){

        Messaging.SingleEmailMessage singleEmailMsg = new Messaging.SingleEmailMessage();
        singleEmailMsg.setWhatId(whatId);       
        singleEmailMsg.setSenderDisplayName(senderDispName);
        singleEmailMsg.setSaveAsActivity(false);
        singleEmailMsg.setTemplateId(templateId);
        singleEmailMsg.setToAddresses(toAddressLst); 
        singleEmailMsg.setTargetObjectId(targetObjId);

        return singleEmailMsg;
    }
    
     /*
    *@Author: Abhishek 
     @Description : Gemnerates email based on parameters and returns SingleEmailmessage Record    
    */  
    public static Messaging.SingleEmailMessage generateEmail( String senderDispName, String HTMLbody,String subjStr,  List<String> toAddressLst , Id targetObjId){

        Messaging.SingleEmailMessage singleEmailMsg = new Messaging.SingleEmailMessage();
        singleEmailMsg.setSubject(subjStr);       
        singleEmailMsg.setSenderDisplayName(senderDispName);
        singleEmailMsg.setSaveAsActivity(false);
        singleEmailMsg.setHTMLBody(HTMLbody);
        if(toAddressLst != null)
        singleEmailMsg.setToAddresses(toAddressLst); 
        singleEmailMsg.setTargetObjectId(targetObjId);

        return singleEmailMsg;
    }
     /*
    *@Author: Abhishek 
     @Description : Sends emails
    */ 
    public static void sendEmail(List<Messaging.SingleEmailMessage> emailLst){
        try{
           if(!emailLst.isEmpty() && !Test.isrunningTest() && (Limits.getEmailInvocations() <= Limits.getLimitEmailInvocations())) 
                    Messaging.sendEmail(emailLst);
        }catch(System.EmailException ex){
            system.debug('Exception : '+ex.getMessage()+ex.getStacktraceString());
            Logger.logMessage('EmailUtil','sendEmail',ex.getMessage(),null,null,null);            
        } 
    }
}