public class EnterpriseCapabilitiesController{
	
    // Empty user used to create multiselect picklist field for data entry
    public List<EntCapDefault__c> listSavedECD{get;set;}
    public Boolean displayNewECD{get{if(displayNewECD==null){displayNewECD=false;}return displayNewECD;}set;}
    public List<EntCapDefault__c> listNewECD{get;set;}
    
    public List<User> listUsersForDisplay{get;set;}
    public integer firstRow{get;set;}
    public integer pageSize{
        get{ 
            if(pageSize==NULL){pageSize = 20;}
            return pageSize;
        }
        set{}
    }
    public void nextN(){
        if(firstRow+pageSize >= listUsersForDisplay.size()){
            return;
        }
        firstRow+=pageSize;
    }
    public void lastN(){
        if(firstRow-pageSize <= 0){
            return;
        }
        firstRow-=pageSize;
    }
    
    public integer skiptoInt;
    public void skipToN(){
        firstRow = firstRow-1;
    }

    // Visualforce page controller, loading default values for display
    Map<String, EntCapDefault__c> mapECD;
    public EnterpriseCapabilitiesController(){
		// Visualforce page simply displays the current ECDs
        generateECDDisplay();
    }
    
    public void generateECDDisplay(){
        mapECD = EntCapDefault__c.getAll();
        listSavedECD = new List<EntCapDefault__c>();
        listSavedECD.addAll(mapECD.values());
        listUsersForDisplay = new List<User>();
        for(EntCapDefault__c e:listSavedECD){
            User u = new User(
                FirstName = e.name,
                LastName = e.id,
                Enterprise_Capabilities__c = e.Enterprise_Capabilities__c,
                DNBi_Login_User__c = false, // used to mark the record for deletion
                IsActive = false // used to mark the record to be edited
            );
            listUsersForDisplay.add( u );
        }
        listUsersForDisplay.sort();
        firstRow = 0;
    }
    
    public void addRow(){ 
        User u = new User(
            // Placeholder User field							// EntCapDefault__c field
            FirstName = generateRandomString(16), 				// Name
            LastName = null, 									// Id
            Enterprise_Capabilities__c = '', 					// Enterprise_Capabilities__c
            StayInTouchNote = '',								// Role_Name__c
            StayInTouchSignature = '',							// Profile_Name__c
            DNBi_Login_User__c = false, 						// used to mark the record for deletion
            IsActive = true 									// used to mark the record to be edited
        );
        if(listUsersForDisplay.isEmpty()){
            listUsersForDisplay = new List<User>();
        }
        listUsersForDisplay.add( u );
    }
    
    public void saveChangedECD(){
        // gather just those users that have been changed, add them to a list and upsert the records
        List<EntCapDefault__c> listToUpsert = new List<EntCapDefault__c>();
        for(User u: listUsersForDisplay){
            if(u.IsActive && u.FirstName != ''){
                EntCapDefault__c e = new EntCapDefault__c(
                	id = u.LastName,
                    name = u.FirstName,
                    Enterprise_Capabilities__c = u.Enterprise_Capabilities__c
                );
                listToUpsert.add(e);
            }
        }
        // now find those just marked for deletion and delete them, ignore any that were also marked for edit
        List<EntCapDefault__c> listToDelete = new List<EntCapDefault__c>();
        for(User u: listUsersForDisplay){
            if(!u.IsActive && u.DNBi_Login_User__c && u.Lastname!=null){
                EntCapDefault__c e = new EntCapDefault__c(
                	id = u.LastName
                );
                listToDelete.add(e);
            }
        }
        
        // Perform the DML
        if(!listToUpsert.IsEmpty()){
            upsert listToUpsert;
        }
        if(!listToDelete.IsEmpty()){
            delete listToDelete;
        }
         
        generateECDDisplay();
    }
    
    // Method invoked from the User trigger used to update each user record to add all new 
    public void updateEnterpriseCapabilities(List<User> userList){
        mapRoleNameToId = populateMapRoleNameToId();
        mapRoleIdToEC = populateMapRoleIdToEC();
        // for each user get their current Enterprise Capabilities, 
        // feed that and the determined Enterprise Capabilities from above into the same set, 
        // and then save the combined series back to the user record.
        system.debug('---> Start Before User process');
        for(User u:userList){
            // only process if user is not to be excluded from updates and we have a mapRoleToEC entry for the user's Role
            if(!u.Enterprise_Capabilities_Exception__c && mapRoleIdToEC.containsKey(u.UserRoleId)){
                system.debug('---> mapRoleIdToEC: '+mapRoleIdToEC);
                // Edits theough the UI use ',' as a delimiter, edits through the API use ';'
                Set<String> userECs = splitString(u.Enterprise_Capabilities__c,',|;');
                userECs.addAll(mapRoleIdToEC.get(u.UserRoleId));
                u.Enterprise_Capabilities__c = concatString(userECs,';');
            }
        }
    }
    
    public static Map<String,Id> mapRoleNameToId{get;set;}
    private Map<String,Id> populateMapRoleNameToId(){
        system.debug('---> Get mapRoleNameToId');
        if(mapRoleNameToId == NULL){
            mapRoleNameToId = new Map<String,Id>();
            for(UserRole r:[Select Id, Name, PortalType from UserRole Where PortalType='None']){
                system.debug('---> loop Roles: '+r);
                mapRoleNameToId.put(r.Name,r.Id);
            }
        }
        return mapRoleNameToId;
    }
    
    // create a map of Role Id => Enterprise Capabilities values
    public static Map<Id,Set<String>> mapRoleIdToEC{get;set;}
    private Map<Id,Set<String>> populateMapRoleIdToEC(){
        system.debug('---> Get mapRoleIdToEC');
        if(mapRoleIdToEC==NULL){
            mapRoleIdToEC = new Map<Id,Set<String>>();
            // for each Role in the EntCapDefault__c Custom Setting split the 
            // Enterprise_Capabilities__c value into the mapRoleToEC Map
            mapECD = EntCapDefault__c.getAll();                
            for(String s:mapECD.keyset()){
                // only process if we find the listed Role Name in mapRoleNameToId
                if(mapRoleNameToId.containsKey(s)){
                    Id thisId = mapRoleNameToId.get(s);
                    Set<String> thisSet = splitString(mapECD.get(s).Enterprise_Capabilities__c,';');
                    mapRoleIdToEC.put(thisId,thisSet);
                }
            }
        }
        return mapRoleIdToEC;
    }
    
    // utility method to get the available picklist options for the Enterprise Capabilities field on the user record
    // Get a list of picklist values from an existing object field.
    public static List<SelectOption> getECDPicklistValues(){
        List<SelectOption> theList = new list<SelectOption>();
        // Describe the User Object
        list<Schema.PicklistEntry> values = User.Enterprise_Capabilities__c.getDescribe().getPickListValues();     
        // Add these values to the list.
        for (Schema.PicklistEntry a : values){ 
            theList.add(new SelectOption(a.getLabel(), a.getValue())); 
        }
        return theList;
    }
    
    // utility method to split a long string based on supplied regex delimiter
    public Set<String> splitString(String source,String regexDelimiter){
        Set<String> thisSet = new Set<String>();
        if(source!=NULL && source.length()>0){
            for(String s:source.split(regexDelimiter)){
                String thisString = s.trim();
                if(thisString!=''){
                    thisSet.add(thisString);
                }
            }
        }
        return thisSet;
    }
    
    // utility method to concatenate a Set of Strings with supplied delimiter 
    public String concatString(Set<String> source, String delimiter){
        String thisString = '';
        if(!source.isEmpty()){
            List<String> thisList = new List<String>();
            thisList.addAll(source);
            for(integer i=0;i<thisList.size();i++){
                String s = thisList.get(i);
                thisString = thisString + s;
                if(i<thisList.size()-1){
                    thisString = thisString + delimiter;
                }
            }
        }
        return thisString;
    }
    
    // Utility to return a random text String of N character length 
    private static Set<String> priorRandoms;
    public static String generateRandomString(Integer length){
        if(priorRandoms == null)
            priorRandoms = new Set<String>();
        
        if(length == null) length = 1+Math.round( Math.random() * 8 );
        String characters = 'abcdefghijklmnopqrstuvwxyz1234567890';
        String returnString = '';
        while(returnString.length() < length){
            Integer charpos = Math.round( Math.random() * (characters.length()-1) );
            returnString += characters.substring( charpos , charpos+1 );
        }
        if(priorRandoms.contains(returnString)) {
            return generateRandomString(length);
        } else {
            priorRandoms.add(returnString);
            return returnString;
        }
    }
}