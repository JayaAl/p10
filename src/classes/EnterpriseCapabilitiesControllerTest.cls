@isTest
public class EnterpriseCapabilitiesControllerTest{
    
    private integer testCount = 12;
    
/* Properties (7)
public  Boolean displayNewECD
public  LIST listNewECD
 public  LIST listSavedECD
public  LIST listUsersForDisplay
private MAP mapECD
public  static MAP> mapRoleIdToEC
public  static MAP mapRoleNameToId
*/

/* Constructors (1)
public  EnterpriseCapabilitiesController()
*/
    private List<UserRole> roles;
    private List<EntCapDefault__c> ECDInsert;
    private map<String, Id> mapRoleNameToId;
    public void prepareRecords(){
        roles = [Select Id, Name, PortalType from UserRole Where PortalType='None' LIMIT :testCount];
        ECDInsert = new List<EntCapDefault__c>();
        mapRoleNameToId = new map<String, Id>();
        for(UserRole r:roles){
            ECDInsert.add( new EntCapDefault__c(
                Name = UTIL_TestUtil.generateRandomString(16),
                Profile_Name__c = '',
                Role_Name__c = r.Name,
                Enterprise_Capabilities__c = 'ECD1; ECD2; ECD3'
            ));
            mapRoleNameToId.put(r.Name, r.Id);
        }
        insert ECDInsert;
    }
    
/*
private void generateECDDisplay()
	// Setup util method to create N custom settings, based on the 1st X Roles in Salesforce
		// give the ECD records the following values: ECD1; ECD2; ECD3
	// confirm that listUsersForDisplay is X elements long
*/
    static testmethod void TEST_generateECDDisplay(){
        EnterpriseCapabilitiesControllerTest testcontroller = new EnterpriseCapabilitiesControllerTest();
        testcontroller.prepareRecords();
        
        EnterpriseCapabilitiesController controller = new EnterpriseCapabilitiesController();
        controller.generateECDDisplay();
        
        system.assertEquals(testcontroller.testCount,controller.listUsersForDisplay.Size());
    }

    

    static testmethod void TEST_addRow(){
        EnterpriseCapabilitiesControllerTest testcontroller = new EnterpriseCapabilitiesControllerTest();
        testcontroller.prepareRecords();
        
        EnterpriseCapabilitiesController controller = new EnterpriseCapabilitiesController();
        controller.generateECDDisplay();
        
        // Add another row, give the ECD record the following values: ECD1; ECD2; ECD3
        controller.addRow();
        
        // confirm that listUsersForDisplay is X+1 elements long
        system.assertEquals(testcontroller.testCount+1,controller.listUsersForDisplay.Size());
    }
    

    static testmethod void TEST_saveChangedECD(){
        EnterpriseCapabilitiesControllerTest testcontroller = new EnterpriseCapabilitiesControllerTest();
        testcontroller.prepareRecords();
        
        EnterpriseCapabilitiesController controller = new EnterpriseCapabilitiesController();
        controller.generateECDDisplay();
        
        for(Integer i = 0; i < controller.listUsersForDisplay.Size(); i++){
        	Integer modi = Math.mod(i, 3);
            User u = controller.listUsersForDisplay.get(i);
            if(modi == 0){
                // Change X/3 listUsersForDisplay records:
                // 		change ECD value to UPDATE1, UPDATE2, UPDATE3
                // 		mark the IsActive value as true
                u.Enterprise_Capabilities__c = 'UPDATE1, UPDATE2, UPDATE3';
            	u.IsActive = true;
            } else if(modi == 1){
                // Of those that remain select a further X/3 records
                // 		mark the DNBi_Login_User__c value as true
            	u.DNBi_Login_User__c = true;
            } else {
                // Of Those that remain select a further X/3 records 
                // 		change ECD value to UPDATE1, UPDATE2, UPDATE3
                // 		mark the IsActive value as true
                // 		mark the DNBi_Login_User__c value as true
            	u.Enterprise_Capabilities__c = 'UPDATE1, UPDATE2, UPDATE3';
            	u.IsActive = true;
                u.DNBi_Login_User__c = true;
            }
            controller.listUsersForDisplay[i] = u;
        }
        
        // commit the changes using the saveChangedECD() method
        controller.saveChangedECD();
            
        // confirm that there are only 2/3 records remaining
        system.assertEquals(8, controller.listUsersForDisplay.Size());
        
        // confirm that all remaining records have ECD value of UPDATE1, UPDATE2, UPDATE3
        for(user u:controller.listUsersForDisplay){
            System.assertEquals('UPDATE1, UPDATE2, UPDATE3', u.Enterprise_Capabilities__c);
        }
    }    

/*
private MAP> populateMapRoleIdToEC()
private MAP populateMapRoleNameToId()

private String concatString(SET source, String delimiter)
private SET splitString(String source, String regexDelimiter)
	// Create a Set<String> with the following values
	// 		One
	// 		Two
	// 		Three
	// 	Pass that Set along with the delimiter "#"
	// 	Confirm that the resulting string is 13 characters long
	// 	Confirm that the resulting string contains each of teh following sub-strings
	// 		One
	// 		Two
	// 		Three
	// 	Pass the generated String to splitString() with the delimiter "#"
	// 	Confirm that a Set containing 3 elements is returned
	// 	Confirm that the set contains each of the following substrings:
	// 		One
	// 		Two
	// 		Three
*/

    static testmethod void TEST_updateEnterpriseCapabilities(){
        EnterpriseCapabilitiesControllerTest testcontroller = new EnterpriseCapabilitiesControllerTest();
        testcontroller.prepareRecords();
        
        EnterpriseCapabilitiesController controller = new EnterpriseCapabilitiesController();
        controller.generateECDDisplay();// Create some number of users, and confirm that their Enterprise Capabilities are updated correctly
        // delete
        // update
        // remove all values
        // give them other values
        List<User> listUsers = new List<User>();
        for(EntCapDefault__c e:testcontroller.ECDInsert){
            user newUser = UTIL_TestUtil.generateUser();
            newUser.UserRoleId = testcontroller.mapRoleNameToId.get(e.Role_Name__c);
            newUser.Enterprise_Capabilities__c = 'This is a test; Second entry ; ; last entry after empty entry';
            listUsers.add(newUser);
        }
        Test.startTest();
        User thisUser = [ select Id from User where Id = :UserInfo.getUserId() ];
        System.runAs ( thisUser ) {
            insert listUsers;
            for(user u:listUsers){
                u.Enterprise_Capabilities__c = 'This is a test; Second entry ; ; last entry after empty entry';
            }
            update listUsers;
        }        
        Test.stopTest();
    }
    
    static testmethod void TEST_concatString(){
        EnterpriseCapabilitiesController controller = new EnterpriseCapabilitiesController();
        set<string> theStrings = new Set<String>{'One', 'Two', 'Three'};
        String retString = controller.concatString(theStrings, '%');
        // System.assertEquals('One%Two%Three', retString); we cannot assert this as a Set can be returned in any order, as a result teh literal assertion fails
        System.assert(retString.contains('One'));
        System.assert(retString.contains('Two'));
        System.assert(retString.contains('Three'));
        System.assertEquals(13,retString.length()); // total length should be the sum of all strings, and the delimiters between each
    }
    
    static testmethod void TEST_splitString(){
        EnterpriseCapabilitiesController controller = new EnterpriseCapabilitiesController();
        String theString = 'One;Tow,Three';
        Set<String> splitStr = controller.splitString(theString,';|,');
        System.assert(splitStr.contains('One'));
        System.assert(splitStr.contains('One'));
        System.assert(splitStr.contains('One'));
        System.assertEquals(3,splitStr.size()); // Final collection should be three elements
    }
    
    /*
    public static List<SelectOption> getECDPicklistValues(){
        List<SelectOption> theList = new list<SelectOption>();
        // Describe the User Object
        list<Schema.PicklistEntry> values = User.Enterprise_Capabilities__c.getDescribe().getPickListValues();     
        // Add these values to the list.
        for (Schema.PicklistEntry a : values){ 
            theList.add(new SelectOption(a.getLabel(), a.getValue())); 
        }
        return theList;
    }
     */
}