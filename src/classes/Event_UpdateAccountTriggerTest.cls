@isTest
public class Event_UpdateAccountTriggerTest {

    public static testMethod void Event_UpdateAccountTriggerTestMethod() {
        
        Account testAccount = UTIL_TestUtil.createAccount();
        
        Event testEvent = UTIL_TestUtil.generateEvent();
        testEvent.whatid = testAccount.Id;
        insert testEvent;
        
        Account assertTestAccount = [Select last_updated_activity__c from Account where Id=: testAccount.Id];
        System.assertEquals(assertTestAccount.last_updated_activity__c , system.today());
    
    
    }
}