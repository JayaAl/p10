global class ExcludeOldOpportunitiesBatch implements database.batchable<Sobject>, database.stateful {
	public list<opportunity> lstOpportunitiesToUpdate = new List<opportunity>();
	public ExcludeOldOpportunitiesBatch() {
		
	}

   
   class MarketingLeadsWithDUNSException extends Exception {}
   
   global Database.QueryLocator start(database.BatchableContext bc) {
       date yearOldDate = system.today()-365;
       
       //Leads_Settings__c leadProcessStartFromDate = Leads_Settings__c.getValues('Lead Processed Until');
       //datetime leadProcessedUntil;// = leadProcessStartFromDate.Lead_Process_Start_Date__c;
       
       
       //String name = 'SRITODAY';
       //String val='\'%' + String.escapeSingleQuotes(name) + '%\'';

      //List<sobject> lineItems = new List<sObject>();
      string query = 'SELECT Id,Last_12_months_Closed_Won_Oppty__c';
             query += ' FROM opportunity ';
             query += ' WHERE  ';
             query += ' Last_12_months_Closed_Won_Oppty__c = true ';
             query += ' AND CloseDate < LAST_N_DAYS:365 ';
             query += ' order by createddate asc';
             
      system.debug('This si query' + query);
      
      return database.getQueryLocator(query);

   }
   
   global void execute(database.BatchableContext bc,List<Sobject> scope) {
   	system.debug('This is the scope size' + scope.size());
       lstOpportunitiesToUpdate = new List<opportunity>();
      	for(opportunity opt:(list<opportunity>)scope)
      	{
      		opt.Last_12_months_Closed_Won_Oppty__c = false;
      		lstOpportunitiesToUpdate.add(opt);
      		system.debug('This is the scope' + opt);
      	}
      	system.debug('This is the scope' + lstOpportunitiesToUpdate);
      	update lstOpportunitiesToUpdate;
    }

   global void finish(database.BatchableContext bc) {
      
      
        
   }
}