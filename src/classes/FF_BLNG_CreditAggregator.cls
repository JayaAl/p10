//TO DO - Delete this class
/*
Developer: Ralph Callaway <ralph.w.callaway@gmail.com>
Description:
	This class aggregates and updates the Sales Credit Note line item totals for a given set of credit notes,
	then updates the related opportunities.  This class is utilized by the following classes and triggers:
	
Related Files:
	FF_BLNG_CreditAggregator_Batch.cls
	FF_BLNG_InvoiceRollups_Test.cls
	FF_BLNG_SalesCreditNote.trigger
*/
public without sharing class FF_BLNG_CreditAggregator {
/*
	// Variables  

	private Map<Id, Opportunity> opptyMap;

	//Constructors  

	// Constructor for triggers
	public FF_BLNG_CreditAggregator(List<c2g__codaCreditNote__c> newList, Map<Id, c2g__codaCreditNote__c> oldMap) {
		Set<Id> opptyIdsToUpdate = new Set<Id>();
		
		for(c2g__codaCreditNote__c newCreditNote : newList) {
			c2g__codaCreditNote__c oldCreditNote = (oldMap == null)
				? null : oldMap.get(newCreditNote.id);
			if(oldCreditNote == null || newCreditNote.c2g__CreditNoteTotal__c != oldCreditNote.c2g__CreditNoteTotal__c) {
				opptyIdsToUpdate.add(newCreditNote.c2g__Opportunity__c);
			}
		}
		
		opptyMap = new Map<Id, Opportunity>([
			select id, Credited_To_Date__c 
			from Opportunity 
			where Id in :opptyIdsToUpdate
		]);
	}
	
	// Constructor for batch
	public FF_BLNG_CreditAggregator(List<Opportunity> opptys) {
		opptyMap = new Map<Id, Opportunity>(opptys);
	}
	
	// Public methods  
	
	public void updateCreditedToDate() {
		updateCreditedToDate(opptyMap);
	}
	
	// Support Methods  
	
	private void updateCreditedToDate(Map<Id,Opportunity>opptyMap) {
		
		AggregateResult[] creditTotals = [select c2g__Opportunity__c oId, SUM(c2g__CreditNoteTotal__c) credTotal
											  from c2g__codaCreditNote__c
											  where c2g__Opportunity__c in :opptyMap.keySet()
											  group by c2g__Opportunity__c];
		if (creditTotals != null && !creditTotals.isEmpty()) {
			for (AggregateResult cur : creditTotals)  {
				if (opptyMap.containsKey((Id)cur.get('oId'))) {
					opptyMap.get((Id)cur.get('oId')).Credited_To_Date__c = (Decimal)cur.get('credTotal');
				}
			}	
			update opptyMap.values();
		}
	}
*/
}