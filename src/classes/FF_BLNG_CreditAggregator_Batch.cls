//TO DO - Delete this class
/*
Developer: Ralph Callaway <ralph.w.callaway@gmail.com>
Description:
	This class allows an administrator to update all or a subset of the opportunities in the
	database with their current value of Credited To Date.
*/
global class FF_BLNG_CreditAggregator_Batch /*implements Database.Batchable<sObject>*/ {	
/*	
	private final String queryString;
	
	//This constructor allows you to specify the query string 
	public FF_BLNG_CreditAggregator_Batch(String q) {
		queryString = q;
	}
	
	global Database.QueryLocator start(Database.BatchableContext BC){
		return Database.getQueryLocator(queryString);
	}
	
	global void execute(Database.BatchableContext BC, List<sObject> scope){
		FF_BLNG_CreditAggregator rollup =
			new FF_BLNG_CreditAggregator((List<Opportunity>) scope);
		rollup.updateCreditedToDate();
	}     
	
	global void finish(Database.BatchableContext BC) {}
*/
}