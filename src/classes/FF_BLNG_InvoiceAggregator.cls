//TO DO - Delete this class
/*
Developer: Ralph Callaway <ralph.w.callaway@gmail.com>
Description:
	This class aggregates and updates the Invoice line item totals for a given set of invoices,
	then updates the related opportunities.  This class is utilized by the following classes and triggers:

Related Files:
	FF_BLNG_InvoiceAggregator_Batch.cls
	FF_BLNG_InvoiceRollups_Test.cls
	FF_BLNG_SalesInvoice.trigger
*/
public without sharing class FF_BLNG_InvoiceAggregator {
/*
	// Variables  
	
	private Map<Id, Opportunity> opptyMap;
	
	// Constructors  

	// Constructor for triggers
	public FF_BLNG_InvoiceAggregator(List<c2g__codaInvoice__c> newList, Map<Id, c2g__codaInvoice__c> oldMap) {
		// Get the ids of the opportunities that need updating. Also collect a set of the invoice ids
		Set<Id> opptyIdsToUpdate = new Set<Id>();
		for(c2g__codaInvoice__c newInvoice : newList) {
			c2g__codaInvoice__c oldInvoice = (oldMap == null)
				? null : oldMap.get(newInvoice.id);
			if(oldInvoice == null || newInvoice.c2g__InvoiceTotal__c != oldInvoice.c2g__InvoiceTotal__c)
				opptyIdsToUpdate.add(newInvoice.c2g__Opportunity__c);
		}
		
		opptyMap = new Map<Id, Opportunity>([
			select id, Invoiced_To_Date__c 
			from Opportunity 
			where Id in :opptyIdsToUpdate
		]);
	}
	
	// Constructor for batch
	public FF_BLNG_InvoiceAggregator(List<Opportunity> opptys) {
		opptyMap = new Map<Id, Opportunity>(opptys);
	}
	
	// Public Methods  
	
	public void updateInvoicedToDate() {
		updateInvoicedToDate(opptyMap);
	}
	
	// Support Methods  

	private void updateInvoicedToDate(Map<Id,Opportunity>opptyMap) {
		
		AggregateResult[] invoiceTotals = [select c2g__Opportunity__c oId, SUM(c2g__InvoiceTotal__c) invTotal
											  from c2g__codaInvoice__c
											  where c2g__Opportunity__c in :opptyMap.keySet()
											  group by c2g__Opportunity__c];
		if (invoiceTotals != null && !invoiceTotals.isEmpty()) {
			for (AggregateResult cur : invoiceTotals)  {
				if (opptyMap.containsKey((Id)cur.get('oId'))) {
					opptyMap.get((Id)cur.get('oId')).Invoiced_To_Date__c = (Decimal)cur.get('invTotal');
				}
			}	
			update opptyMap.values();
		}
	}
*/
}