//TO DO - Delete this class
/*
Developer: Ralph Callaway <ralph.w.callaway@gmail.com>
Description:
    Place all data creation utility functions in this class so they can be shared
    across multiple tests.  That way if new validation rules break a test you only
    have to make a change in one place.
*/
public class FF_BLNG_TestUtil {
/*    
    static {
        system.assert(Test.isRunningTest(), 'FF_BLNG_TestUtil class may only be referenced in test classes');
    }

    public static String TEST_STRING = generateRandomString();
    public static Date TEST_DATE = System.today();

    //Standard Object Functions  

    // Accounts
    public static Account createAccount() {
        Account account = generateAccount();
        insert account;
        return account;
    }
    public static Account generateAccount() {
        return new Account(
              type = 'Advertiser' // required for Opportunity Account lookup filter
            , name = TEST_STRING
            , c2g__CODAAccountTradingCurrency__c = 'USD'//Add by Lakshman on 10/28/2014(ISS-11405)
        );
    }
    
    // Case
    public static Case createCase() {
        Case testCase = generateCase();
        insert testCase;
        return testCase;
    }
    public static Case generateCase() {
        return new Case(subject = TEST_STRING, description = TEST_STRING);
    }
    
    // Contacts
    public static Contact createContact(Id accountId) {
        Contact contact = generateContact(accountId);
        insert contact;
        return contact;
    }
    public static Contact generateContact(Id accountId) {
        return new Contact(lastName = TEST_STRING, accountId = accountId);
    }
    
    // Leads
    public static Lead createLead() {
        Lead lead = generateLead();
        insert lead;
        return lead;
    }
    public static Lead generateLead() {
        return new Lead(lastName = TEST_STRING, company = TEST_STRING, PostalCode = '94612');
    }
         
    // Opportunity
    public static Opportunity createOpportunity(Id accountId) {
        Opportunity opportunity = generateOpportunity(accountId);
        insert opportunity;
        return opportunity;
    }
    public static Opportunity generateOpportunity(Id accountId) {
        return new Opportunity(name = TEST_STRING, closeDate = TEST_DATE, stageName = TEST_STRING, accountId = accountId,Confirm_direct_relationship__c=true,Industry_Sub_Category__c='Political Campaign Marketing',Type='Advertiser');
    }
    
    // User
    public static User createUser() {
        User testUser = generateUser();
        insert testUser;
        return testUser;
    }
    public static User generateUser() {
        String testString = generateRandomString(8);
        String testEmail = generateRandomEmail();
        return new User(lastName = testString,
            userName = testEmail,
            profileId = SYSADMIN_PROFILE_ID,
            alias = testString,
            email = testEmail,
            emailEncodingKey = 'ISO-8859-1',
            languageLocaleKey = 'en_US',
            localeSidKey = 'en_US',
            timeZoneSidKey = 'America/Los_Angeles'
        );
    }

    //Memoized Properties 

    public static Id SYSADMIN_PROFILE_ID
    {
        get {
            if(null == SYSADMIN_PROFILE_ID) {
                SYSADMIN_PROFILE_ID = [select id from Profile where name = 'System Administrator'][0].id;
            }
            return SYSADMIN_PROFILE_ID; 
        }
        set;
    }
        
    //Random Functions

    private static Set<String> priorRandoms;
        public static String generateRandomString(){return generateRandomString(null);}
        public static String generateRandomString(Integer length){
            if(priorRandoms == null)
                priorRandoms = new Set<String>();

            if(length == null) length = 1+Math.round( Math.random() * 8 );
            String characters = 'abcdefghijklmnopqrstuvwxyz1234567890';
            String returnString = '';
            while(returnString.length() < length){
                Integer charpos = Math.round( Math.random() * (characters.length()-1) );
                returnString += characters.substring( charpos , charpos+1 );
            }
            if(priorRandoms.contains(returnString)) {
                return generateRandomString(length);
            } else {
                priorRandoms.add(returnString);
                return returnString;
            }
        }
    public static String generateRandomEmail(){return generateRandomEmail(null);}
    public static String generateRandomEmail(String domain){
        if(domain == null || domain == '')
            domain = generateRandomString() + '.com';
        return generateRandomString() + '@' + domain;
    }

    public static String generateRandomUrl() {
        return 'http://' + generateRandomString() + '.com'; 
    }

    // Test Functions 

    @isTest
    private static void testStandardObjects() {
        User testUser = createUser();
        Case testCase = createCase();
        Lead testLead = createLead();
        Account testAccount = createAccount();
        Contact testContact = createContact(testAccount.id);
        Opportunity testOpportunity = createOpportunity(testAccount.id);
    }

    @isTest
    private static void testRandomFunctions() {
        String randomString = generateRandomString();
        String randomEmail = generateRandomEmail();
        String randomUrl = generateRandomUrl();
    }
*/
}