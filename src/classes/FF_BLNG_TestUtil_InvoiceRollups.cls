//TO DO - Delete this class
/*
Developer: Ralph Callaway <ralph.w.callaway@gmail.com>
Description:
    Utility method for testing invoice rollups.
    
    Due to the complexity of the FinancialForce datamodel, testing has been set up
    to use existing objects.  Unfortunately this means testing outside of a full
    copy sandbox environment is not possible.
    
    Only available to test methods.
    
Usage Notes:
    To use create the test util and call setup.  Then call system.runAs() to
    start another transaction.  This is required to avoid mix dml errors
    as the setup method creates a GroupMember object.
    
    E.g
        FF_BLNG_TestUtil_InvoiceRollups util = new FF_BLNG_TestUtil_InvoiceRollups();
        util.setup();
        
        system.runAs(new User(id = UserInfo.getUserId())) {
            // more code
        }       
*/

public class FF_BLNG_TestUtil_InvoiceRollups {
/*
    // Test class lock
    static { system.assert(Test.isRunningTest(), 'FF_BLNG_TestUtil_InvoiceRollups may only be used in test methods'); }

    //Variables  

    public Id testUserId = UserInfo.getUserId();
    public String companyName = '100-Pandora US';    
    public c2g__codaCompany__c testCompany;
    public c2g__codaPeriod__c testPeriod;
    public c2g__codaAccountingCurrency__c testCurrency; 

    //Setup Methods  

    public void setup() {
        loadCompany();
        loadPeriod();
        loadCurrency();
        addTestUserToCompanyOwnerGroup();
    }
    
    //Object Creation Methods 
    
    // Sales Credit Notes
    public c2g__codaCreditNote__c createSalesCreditNote(Id accountId, Id opportunityId) {
        c2g__codaCreditNote__c salesCreditNote = generateSalesCreditNote(accountId, opportunityId);
        insert salesCreditNote;
        return salesCreditNote;
    }
    public c2g__codaCreditNote__c generateSalesCreditNote(Id accountId, Id opportunityId) {
        return new c2g__codaCreditNote__c(
              c2g__Account__c = accountId
            , c2g__Opportunity__c = opportunityId
            , c2g__OwnerCompany__c = testCompany.id
            , c2g__CreditNoteCurrency__c = testCurrency.id
            , c2g__Period__c = testPeriod.id
            , Department_of_Origin__c = FF_BLNG_TestUtil.TEST_STRING
            , Reason__c = FF_BLNG_TestUtil.TEST_STRING
            , Revenue_Impact_Code__c = FF_BLNG_TestUtil.TEST_STRING
        );
    }
    
    // Sales Credit Note Line Items
    public c2g__codaCreditNoteLineItem__c createSalesCreditNoteLineItem(Id productId, Id salesCreditNoteId) {
        c2g__codaCreditNoteLineItem__c salesCreditNoteLineItem = 
            generateSalesCreditNoteLineItem(productId, salesCreditNoteId);
        insert salesCreditNoteLineItem;
        return salesCreditNoteLineItem;
    }
    public c2g__codaCreditNoteLineItem__c generateSalesCreditNoteLineItem(Id productId, Id salesCreditNoteId) {
        return new c2g__codaCreditNoteLineItem__c(
              c2g__OwnerCompany__c = testCompany.id
            , c2g__CreditNote__c = salesCreditNoteId
            , c2g__Product__c = productId
            , c2g__Quantity__c = 1
            , c2g__UnitPrice__c = 1
        );
    }
    
    // Sales Invoices
    public c2g__codaInvoice__c createSalesInvoice(Id accountId, Id opportunityId) {
        c2g__codaInvoice__c salesInvoice = generateSalesInvoice(accountId, opportunityId);
        // system.assert(false,salesInvoice);
        insert salesInvoice;
        return salesInvoice;
    }
    public c2g__codaInvoice__c generateSalesInvoice(Id accountId, Id opportunityId) {
        return new c2g__codaInvoice__c(
              c2g__Opportunity__c = opportunityId
            , c2g__Period__c = testPeriod.id
            , c2g__DerivePeriod__c = false
            , c2g__OwnerCompany__c = testCompany.id
            , c2g__InvoiceCurrency__c = testCurrency.id
            , c2g__DueDate__c = date.today()
            , c2g__InvoiceDate__c = date.today()
            , c2g__Account__c = accountId
            , IO_Number__c = FF_BLNG_TestUtil.TEST_STRING
            , Advertiser__c = accountId
            , Flight_Dates_Start__c = date.today()
            , Flight_Dates_End__c = date.today()
            , Billing_Terms__c = FF_BLNG_TestUtil.TEST_STRING
        );
    }
    
    // Sales Invoice Line Items
    public c2g__codaInvoiceLineItem__c createSalesInvoiceLineItem(Id invoiceId, Id productId) {
        c2g__codaInvoiceLineItem__c salesInvoiceLineItem =
            generateSalesInvoiceLineItem(invoiceId, productId);
        insert salesInvoiceLineItem;
        return salesInvoiceLineItem;
    }
    public c2g__codaInvoiceLineItem__c generateSalesInvoiceLineItem(Id invoiceId, Id productId) {
        return new c2g__codaInvoiceLineItem__c(
              c2g__OwnerCompany__c = testCompany.id
            , c2g__Invoice__c = invoiceId
            , c2g__Product__c = productId
            , c2g__Quantity__c = 1
            , c2g__UnitPrice__c = 1
        );
    }
    
    //Support Methods  
    
    private void addTestUserToCompanyOwnerGroup() {
        insert new GroupMember(userOrGroupId = testUserId, groupId = testCompany.ownerId);
    }
    
    private void loadCompany() {
        if(testCompany == NULL){
            List<c2g__codaCompany__c> companies = [select id,ownerId from c2g__codaCompany__c where name = :companyName limit 1];
            system.assertEquals(1, companies.size(), 'Test company with name, ' + companyName + ', not found.  Test company presence is required to run this test.');
            testCompany = companies[0];
        }
    }

    private void loadCurrency() {
        if(testCurrency == NULL){
            List<c2g__codaAccountingCurrency__c> currencies = [
                select id 
                from c2g__codaAccountingCurrency__c 
                where c2g__OwnerCompany__c = :testCompany.id 
                and name = 'USD'
                limit 1
            ];
            system.assertEquals(1, currencies.size(), 'No currency found for test company.  Unable to complete this test.');
            testCurrency = currencies[0];
        }
    }
    
    private void loadPeriod() {
        if(testPeriod == NULL){
            // updating test as during FF transition the current month is no longer open.
            // and c2g__StartDate__c <= :System.Today()
            // and c2g__EndDate__c >= :System.Today()
            List<c2g__codaPeriod__c> periods = [
                select id
                from c2g__codaPeriod__c
                where c2g__OwnerCompany__c = :testCompany.id
                and c2g__Closed__c = false
                and ffps_pandora__ClosedForSalesInvoices__c = false
                and ffps_pandora__CloseForSalesCreditNotes__c = false
                and ffps_pandora__ClosedForPayableCreditNotes__c = false
                limit 1
            ];
            system.assertEquals(1, periods.size(), 'No period found for current date for test company.  Unable to complete this test.');
            testPeriod = periods[0];
        }
    }

    //Test Methods 

    @isTest
    private static void testFF_BLNG_TestUtil_InvoiceRollups_Load() {
        FF_BLNG_TestUtil_InvoiceRollups util = new FF_BLNG_TestUtil_InvoiceRollups();
        Boolean exceptionThrown = false;
        try {
            util.setup();
        } catch(Exception e) {
            exceptionThrown = true;
        }
        system.assert(!exceptionThrown);
        system.assertNotEquals(null, util.testCompany);
        system.assertNotEquals(null, util.testCompany.id);
        system.assertNotEquals(null, util.testPeriod);
        system.assertNotEquals(null, util.testPeriod.id);
        system.assertNotEquals(null, util.testCurrency);
        system.assertNotEquals(null, util.testCurrency.id);
    }

    @isTest
    private static void testCreationFunctions() {
        FF_BLNG_TestUtil_InvoiceRollups util = new FF_BLNG_TestUtil_InvoiceRollups();
        util.setup();

        // avoid mixed dml errors
        system.runAs(new User(id = util.testUserId)) {
        Test.startTest();
            Account testAccount = FF_BLNG_TestUtil.createAccount();
            Opportunity testOpportunity = FF_BLNG_TestUtil.createOpportunity(testAccount.id);
        Test.stopTest();
            c2g__codaInvoice__c testInvoice = util.createSalesInvoice(testAccount.id, testOpportunity.id);
            system.assertNotEquals(null, testInvoice.id);
            c2g__codaCreditNote__c testCreditNote = util.createSalesCreditNote(testAccount.id, testOpportunity.id);
            system.assertNotEquals(null, testCreditNote.id);
            FF_BLNG_TestUtil_ProductHelper prodHelper = new FF_BLNG_TestUtil_ProductHelper();
            c2g__codaCreditNoteLineItem__c testCreditLineItem = 
                util.createSalesCreditNoteLineItem(prodHelper.testProduct.id, testCreditNote.id);
            system.assertNotEquals(null, testCreditLineItem.id);
            c2g__codaInvoiceLineItem__c testInvoiceLineItem =
                util.createSalesInvoiceLineItem(testInvoice.id, prodHelper.testProduct.id);
        
        }
    }
*/
}