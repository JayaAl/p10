//TO DO - Delete this class
/*
Developer Name: Ralph Callaway <ralph.w.callaway@gmail.com>
Description: Testing helper class for testing product related code.
*/

public class FF_BLNG_TestUtil_ProductHelper {
/*
	static {
		system.assert(Test.isRunningTest(), 'FF_BLNG_TestUtil_ProductHelper class may only be referenced in test classes');
	}

	public static final Decimal DEFAULT_PRICE = 1;
	
	public Pricebook2 pricebook { get; set; }
	
	public Product2 testProduct { get; set; }
	
	public PricebookEntry testPricebookEntry { get; set; }
	
	public FF_BLNG_TestUtil_ProductHelper() {
		// create pricebook
		pricebook = new Pricebook2(name = FF_BLNG_TestUtil.TEST_STRING, isActive = true);
		insert pricebook;
		
		// create products
		testProduct = new Product2(name = FF_BLNG_TestUtil.TEST_STRING);
		insert testProduct;
		
		// query standard pricebook
		Pricebook2 standardPricebook = [select id from Pricebook2 where isStandard = true];
		
		// create standard pricebook entries
		List<PricebookEntry> standardPricebookEntries = new List<PricebookEntry>();
		PricebookEntry testPricebookEntryStandard =
			new PricebookEntry(pricebook2Id = standardPricebook.id, unitPrice = DEFAULT_PRICE,
				isActive = true, product2Id = testProduct.id);
		standardPricebookEntries.add(testPricebookEntryStandard);
		insert standardPricebookEntries;
		
		// create test pricebook entries
		List<PricebookEntry> testPricebookEntries = new List<PricebookEntry>();
		testPricebookEntry =
			new PricebookEntry(pricebook2Id = pricebook.id, unitPrice = DEFAULT_PRICE,
				isActive = true, product2Id = testProduct.id);
		testPricebookEntries.add(testPricebookEntry);
		insert testPricebookEntries;
	}
	
	public static OpportunityLineItem createOpportunityLineItem(Id opportunityId, Id pricebookEntryId) {
		OpportunityLineItem lineItem = generateOpportunityLineItem(opportunityId, pricebookEntryId);
		insert lineItem;
		return lineItem;
	}
	
	public static OpportunityLineItem generateOpportunityLineItem(Id opportunityId, Id pricebookEntryId) {
		return new OpportunityLineItem(
			  opportunityId = opportunityId
			, pricebookEntryId = pricebookEntryId
			, quantity = 1
			, totalPrice = DEFAULT_PRICE
			, end_date__c = System.Today().addMonths(2)
		);
	}
	
	@isTest
	private static void testProductHelper() {
		// create product helper
		Test.startTest();
		FF_BLNG_TestUtil_ProductHelper productHelper = new FF_BLNG_TestUtil_ProductHelper();
		Test.stopTest();
		
		// verify everything got inserted
		System.assertNotEquals(null, productHelper.pricebook.id);
		System.assertNotEquals(null, productHelper.testProduct.id);
		System.assertNotEquals(null, productHelper.testPricebookEntry.id);
		
		// test OpportunityLineItem creation
		Account testAccount = FF_BLNG_TestUtil.createAccount();
		Opportunity testOpportunity = FF_BLNG_TestUtil.generateOpportunity(testAccount.id);
		testOpportunity.pricebook2Id = productHelper.pricebook.id;
		insert testOpportunity;
		OpportunityLineItem testLineItem = FF_BLNG_TestUtil_ProductHelper.createOpportunityLineItem(testOpportunity.id, productHelper.testPricebookEntry.id);
	}
*/
}