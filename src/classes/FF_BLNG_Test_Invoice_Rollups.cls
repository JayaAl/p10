//TO DO - Delete this class
/*
Developer: Ralph Callaway <ralph.w.callaway@gmail.com>
Description:
    Test methods for 'Invoiced to Date' and 'Credited to Date'
    invoice rollups.
*/

@isTest
private class FF_BLNG_Test_Invoice_Rollups {
   /*
    @isTest
    private static void testCreditRollup_Insert_Update() {

        system.runAs(new User(id = UserInfo.getUserId())) {
            // validate initial conditions
            testOppty = [
                select credited_to_date__c
                from Opportunity
                where id = :testOppty.id
            ];
            Double value = testOppty.credited_to_date__c == null ? 0 : testOppty.credited_to_date__c;
            system.assertEquals(0, value);
            
            Test.startTest();
                // create a line item
                Double amount = 1000;
                c2g__codaCreditNote__c creditNote = util.createSalesCreditNote(testAccount.id, testOppty.id);
                FF_BLNG_TestUtil_ProductHelper prodHelper = new FF_BLNG_TestUtil_ProductHelper();
                c2g__codaCreditNoteLineItem__c creditNoteLineItem = 
                    util.generateSalesCreditNoteLineItem(prodHelper.testProduct.id, creditNote.id);
                creditNoteLineItem.c2g__UnitPrice__c = amount;
                insert creditNoteLineItem;
                
                // validate rollup after insert
                testOppty = [
                    select credited_to_date__c
                    from Opportunity
                    where id = :testOppty.id
                ];
                system.assertEquals(creditNoteLineItem.c2g__UnitPrice__c * creditNoteLineItem.c2g__Quantity__c, testOppty.credited_to_date__c);
                
                // update line item amount
                creditNoteLineItem.c2g__UnitPrice__c = amount * 2;
                update creditNoteLineItem;
                
                // validate rollup after update
                testOppty = [
                    select credited_to_date__c
                    from Opportunity
                    where id = :testOppty.id
                ];
                system.assertEquals(creditNoteLineItem.c2g__UnitPrice__c * creditNoteLineItem.c2g__Quantity__c, testOppty.credited_to_date__c);
        	Test.stopTest();
        }
    } 
    
    @isTest
    private static void testCreditRollup_Delete_Undelete() {
        system.runAs(new User(id = UserInfo.getUserId())) {
            testOppty = [
                select credited_to_date__c
                from Opportunity
                where id = :testOppty.id
            ];
            Double value = testOppty.credited_to_date__c == null ? 0 : testOppty.credited_to_date__c;
            system.assertEquals(0, value);
            
            // create a line item
            Double amount = 1000;
            c2g__codaCreditNote__c creditNote = util.createSalesCreditNote(testAccount.id, testOppty.id);
            FF_BLNG_TestUtil_ProductHelper prodHelper = new FF_BLNG_TestUtil_ProductHelper();
            c2g__codaCreditNoteLineItem__c creditNoteLineItem = 
                util.generateSalesCreditNoteLineItem(prodHelper.testProduct.id, creditNote.id);
            creditNoteLineItem.c2g__UnitPrice__c = amount;
            insert creditNoteLineItem;
            
            // validate rollup after insert
            testOppty = [
                select credited_to_date__c
                from Opportunity
                where id = :testOppty.id
            ];
            system.assertEquals(creditNoteLineItem.c2g__UnitPrice__c * creditNoteLineItem.c2g__Quantity__c, testOppty.credited_to_date__c);
            
            Test.startTest();
                // delete line item
                delete creditNoteLineItem;
                
                // validate rollup after delete
                testOppty = [
                    select credited_to_date__c
                    from Opportunity
                    where id = :testOppty.id
                ];
                system.assertEquals(0, testOppty.credited_to_date__c);
                
                // undelete line item
                undelete creditNoteLineItem;
                
                // validate rollup after delete
                testOppty = [
                    select credited_to_date__c
                    from Opportunity
                    where id = :testOppty.id
                ];
                system.assertEquals(creditNoteLineItem.c2g__UnitPrice__c * creditNoteLineItem.c2g__Quantity__c, testOppty.credited_to_date__c);
        	Test.stopTest();
        }
    }
    
    @isTest
    private static void testInvoiceRollup_Insert_Update() {

        system.runAs(new User(id = UserInfo.getUserId())) {
            
            // validate initial conditions
            testOppty = [
                select invoiced_to_date__c
                from Opportunity
                where id = :testOppty.id
            ];
            Double value = testOppty.invoiced_to_date__c == null ? 0 : testOppty.invoiced_to_date__c;
            system.assertEquals(0, value);
            
			Test.startTest();
				// create a line item
				c2g__codaInvoice__c invoice = util.createSalesInvoice(testAccount.id, testOppty.id);
				FF_BLNG_TestUtil_ProductHelper prodHelper = new FF_BLNG_TestUtil_ProductHelper();
				c2g__codaInvoiceLineItem__c invoiceLineItem = 
					util.createSalesInvoiceLineItem(invoice.id, prodHelper.testProduct.id);
				
				// validate rollup after insert
				testOppty = [
					select invoiced_to_date__c
					from Opportunity
					where id = :testOppty.id
				];
				system.assertEquals(invoiceLineItem.c2g__UnitPrice__c * invoiceLineItem.c2g__Quantity__c, testOppty.invoiced_to_date__c);
				
				// update line item amount
				invoiceLineItem.c2g__UnitPrice__c = 1000;
				update invoiceLineItem;
				
				// validate rollup after update
				testOppty = [
					select invoiced_to_date__c
					from Opportunity
					where id = :testOppty.id
				];
				system.assertEquals(invoiceLineItem.c2g__UnitPrice__c * invoiceLineItem.c2g__Quantity__c, testOppty.invoiced_to_date__c);
			Test.stopTest();
		}
    }
			
			
    @isTest
    private static void testInvoiceRollup_Delete_Undelete() {

        system.runAs(new User(id = UserInfo.getUserId())) {
            
            // validate initial conditions
            testOppty = [
                select invoiced_to_date__c
                from Opportunity
                where id = :testOppty.id
            ];
            Double value = testOppty.invoiced_to_date__c == null ? 0 : testOppty.invoiced_to_date__c;
            system.assertEquals(0, value);
            
            // create a line item
            c2g__codaInvoice__c invoice = util.createSalesInvoice(testAccount.id, testOppty.id);
            FF_BLNG_TestUtil_ProductHelper prodHelper = new FF_BLNG_TestUtil_ProductHelper();
            c2g__codaInvoiceLineItem__c invoiceLineItem = 
                util.createSalesInvoiceLineItem(invoice.id, prodHelper.testProduct.id);
            
            // validate rollup after insert
            testOppty = [
                select invoiced_to_date__c
                from Opportunity
                where id = :testOppty.id
            ];
            system.assertEquals(invoiceLineItem.c2g__UnitPrice__c * invoiceLineItem.c2g__Quantity__c, testOppty.invoiced_to_date__c);
            
            Test.startTest();
				// delete line item
				delete invoiceLineItem;
				
				// validate rollup after delete
				testOppty = [
					select invoiced_to_date__c
					from Opportunity
					where id = :testOppty.id
				];
				system.assertEquals(0, testOppty.invoiced_to_date__c);
				
				// undelete line item
				undelete invoiceLineItem;
				
				// validate rollup after delete
				testOppty = [
					select invoiced_to_date__c
					from Opportunity
					where id = :testOppty.id
				];
				system.assertEquals(invoiceLineItem.c2g__UnitPrice__c * invoiceLineItem.c2g__Quantity__c, testOppty.invoiced_to_date__c);
            Test.stopTest();
        }
    }
    
    @isTest
    private static void testCreditRollupBatch() {
        // initial setup
        // FF_BLNG_TestUtil_InvoiceRollups util = new FF_BLNG_TestUtil_InvoiceRollups();
        // util.setup();
        //setupUtil();
        
        system.runAs(new User(id = UserInfo.getUserId())) {
            // create test data
//          Account testAccount = FF_BLNG_TestUtil.createAccount();
//          Opportunity testOppty = FF_BLNG_TestUtil.createOpportunity(testAccount.id);
            Double amount = 1000;
            c2g__codaCreditNote__c creditNote = util.createSalesCreditNote(testAccount.id, testOppty.id);
            FF_BLNG_TestUtil_ProductHelper prodHelper = new FF_BLNG_TestUtil_ProductHelper();
            c2g__codaCreditNoteLineItem__c creditNoteLineItem = 
                util.generateSalesCreditNoteLineItem(prodHelper.testProduct.id, creditNote.id);
            creditNoteLineItem.c2g__UnitPrice__c = amount;
            insert creditNoteLineItem;
            
            // clear out credited to date from initial insert
            testOppty.credited_to_date__c = null;
            update testOppty;
            
            // load batch
            Test.startTest();
            String queryString = 'select id from Opportunity where id = \'' + testOppty.id + '\'';
            Database.executeBatch(new FF_BLNG_CreditAggregator_Batch(queryString));
            Test.stopTest();
            
            // validate credited to date has been repopulated
            testOppty = [
                select credited_to_date__c
                from Opportunity
                where id = :testOppty.id
            ];
            system.assertEquals(creditNoteLineItem.c2g__UnitPrice__c * creditNoteLineItem.c2g__Quantity__c, testOppty.credited_to_date__c);     
        }
    }
    
    @isTest
    private static void testInvoiceRollupBatch() {
        // initial setup
        // FF_BLNG_TestUtil_InvoiceRollups util = new FF_BLNG_TestUtil_InvoiceRollups();
        // util.setup();
        //setupUtil();
        
        system.runAs(new User(id = UserInfo.getUserId())) {
            // create test data
//          Account testAccount = FF_BLNG_TestUtil.createAccount();
//          Opportunity testOppty = FF_BLNG_TestUtil.createOpportunity(testAccount.id);
//          c2g__codaInvoice__c invoice = util.createSalesInvoice(testAccount.id, testOppty.id);
            FF_BLNG_TestUtil_ProductHelper prodHelper = new FF_BLNG_TestUtil_ProductHelper();
            c2g__codaInvoiceLineItem__c invoiceLineItem = 
                util.createSalesInvoiceLineItem(invoice.id, prodHelper.testProduct.id);
            
            // clear out credited to date from initial insert
            testOppty.invoiced_to_date__c = null;
            update testOppty;
            
            // load batch
            Test.startTest();
            String queryString = 'select id from Opportunity where id = \'' + testOppty.id + '\'';
            Database.executeBatch(new FF_BLNG_InvoiceAggregator_Batch(queryString));
            Test.stopTest();
            
            // validate credited to date has been repopulated
            testOppty = [
                select invoiced_to_date__c
                from Opportunity
                where id = :testOppty.id
            ];
            system.assertEquals(invoiceLineItem.c2g__UnitPrice__c * invoiceLineItem.c2g__Quantity__c, testOppty.invoiced_to_date__c);       
        }
    }
    */
    /* Author: Casey Grooms
     * email: cgrooms@pandora.com
     * Date: 2013-12-12
     * Description: Updates to methods to consolidate common objects and classes
    */
    /*
    public static FF_BLNG_TestUtil_InvoiceRollups util{
        get{
            if(util == NULL){
                util = new FF_BLNG_TestUtil_InvoiceRollups();
                util.setup();
            }
            return util;
        }
        set;
    }
    public static Account testAccount{
        get{
            if(testAccount == NULL){
                testAccount = FF_BLNG_TestUtil.createAccount();
            }
            return testAccount;
        }
        set;
    }
    public static Opportunity testOppty{
        get{
            if(testOppty == NULL){
                testOppty = FF_BLNG_TestUtil.createOpportunity(testAccount.id);
            }
            return testOppty;
        }
        set;
    }
    public static c2g__codaInvoice__c invoice{
        get{
            if(invoice == NULL){
                invoice = util.createSalesInvoice(testAccount.id, testOppty.id);
            }
            return invoice;
        }
        set;
    }
*/
}