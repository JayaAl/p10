public with sharing class FF_C_OptSalesInvoiceEmailComController {

    public class InvoicePageRow {
        public Decimal LineNumber {get;set;} 
        public String Description {get;set;}
        public Double GrossValue {get;set;}
        public Double NetValue {get;set;}
        public String ProductName {get;set;}
        public String CreativeName {get;set;}
        public Decimal Length {get;set;}
    }

    public class InvoicePage {
        public String PageBreak {get;set;}
        public List<InvoicePageRow> PageList {get;set;}
    }

    public Id salesInvoiceId{get;set;}
    public ID salesInvoiceID1{get;set;}
    public String taxType{get;set;}
    public String isoCode{get;set;}
    
    public Double netTotal{get;set;}
    public Double taxTotal{get;set;}
    public Double invoiceTotal{get;set;}
    
    public Integer invoicePage1Of1{get;set;}
    public Integer invoicePage1OfX{get;set;}
    public Integer invoicePageXOfX{get;set;}
    public Integer invoiceLastPage{get;set;}
    
    public String PrintedText1Heading{get;set;}
    public String PrintedText1Text{get;set;}
    public String PrintedText2Heading{get;set;}
    public String PrintedText2Text{get;set;}
    public String PrintedText3Heading{get;set;}
    public String PrintedText3Text{get;set;}
    public String PrintedText4Heading{get;set;}
    public String PrintedText4Text{get;set;}
    public String PrintedText5Heading{get;set;}
    public String PrintedText5Text{get;set;}
    public String EditablePrintedTextHeading{get;set;}
    public String EditablePrintedText{get;set;}
    
    private Boolean Initialized = false;

    public Boolean ShowGross;
    public Boolean ShowNet;
    public Boolean UseInvoiceTemplate;
    public Boolean ShowZero;
    public Double GrossAmount;

    public Boolean RadioInvoice;
    public Double AgencyCommission;
    public String AdditionalInfo;

    public List<InvoicePage> InvoicePageLines = new List<InvoicePage>();

    private void initialize() {
        if (!this.Initialized) {
            initializeDataElements();
            initializePages();
            this.Initialized = true;
        }
    }
    private void initializeDataElements() {
        System.debug('Entering initializeDataElements');
        List<c2g__codaInvoice__c> l_list = [SELECT c.Id, c.Show_Gross_Total__c FROM c2g__codaInvoice__c c where c.Id = :salesInvoiceId Limit 1];
        if (l_list.size() > 0) {
            this.ShowGross = l_list[0].Show_Gross_Total__c;
        } else {
            this.ShowGross = false;
        }
        this.ShowNet = !this.ShowGross;

        l_list = [SELECT c.Id, c.Use_Invoice_Template_Lines__c FROM c2g__codaInvoice__c c where c.Id = :salesInvoiceId Limit 1];
        if (l_list.size() > 0) {
            this.UseInvoiceTemplate = l_list[0].Use_Invoice_Template_Lines__c;
        } else {
            this.UseInvoiceTemplate = false;
        }

        l_list = [SELECT c.Id, c.ffdc_ShowZeros__c FROM c2g__codaInvoice__c c where c.Id = :salesInvoiceId Limit 1];
        if (l_list.size() > 0) {
            this.ShowZero = l_list[0].ffdc_ShowZeros__c;
        } else {
            this.ShowZero = false;
        }

        setGrossAmount();
        setRadioInvoice();
        setAgencyCommission();
        setAdditionalInfo();
        System.debug('Leaving initializeDataElements');
    }
    private void initializePages() {
        System.debug('Entering initializePages');
        if (!this.UseInvoiceTemplate) {
            List<c2g__codaInvoiceLineItem__c[]> l_list = paginateInvoiceLines();

            for (Integer index=0; index<l_list.size(); index++) {
                InvoicePage iPage = new InvoicePage();
                if (index < l_list.size()-1) {
                    iPage.PageBreak = 'page-break-after:always;';
                } else {
                    iPage.PageBreak = 'page-break-after:auto;';
                }

                c2g__codaInvoiceLineItem__c[] page_list = l_list.get(index);
                List<InvoicePageRow> invoice_page_list = new List<InvoicePageRow>();
                for (c2g__codaInvoiceLineItem__c line : page_list) {
                    InvoicePageRow iPageRow = new InvoicePageRow();
                    iPageRow.LineNumber = line.Alt_Line_Number__c;
                    iPageRow.Description = line.c2g__LineDescription__c;
                    iPageRow.GrossValue = line.Gross_Amt__c;
                    iPageRow.NetValue = line.c2g__NetValue__c;
                    iPageRow.ProductName = line.SProduct_Name__c;
                    iPageRow.CreativeName = line.Creative_Name__c;
                    iPageRow.Length = line.Length__c;
                    invoice_page_list.add(iPageRow);
                }
                iPage.PageList = invoice_page_list;

                InvoicePageLines.add(iPage);
            }
        } else {
            List<Invoice_Template_Line__c[]> l_list = paginateInvoiceTemplateLines();

            for (Integer index=0; index<l_list.size(); index++) {
                InvoicePage iPage = new InvoicePage();
                if (index < l_list.size()-1) {
                    iPage.PageBreak = 'page-break-after:always;';
                } else {
                    iPage.PageBreak = 'page-break-after:auto;';
                }

                Invoice_Template_Line__c[] page_list = l_list.get(index);
                List<InvoicePageRow> invoice_page_list = new List<InvoicePageRow>();
                for (Invoice_Template_Line__c line : page_list) {
                    InvoicePageRow iPageRow = new InvoicePageRow();
                    iPageRow.LineNumber = line.Line_Number__c;
                    iPageRow.Description = line.Agency_IO_Placement_Name__c;
                    iPageRow.GrossValue = line.Gross_Value__c;
                    iPageRow.NetValue = line.Net_Value__c;
                    iPageRow.ProductName = line.SProduct_Name__c;
                    iPageRow.CreativeName = line.Creative_Name__c;
                    iPageRow.Length = line.Length__c;
                    invoice_page_list.add(iPageRow);
                }
                iPage.PageList = invoice_page_list;

                InvoicePageLines.add(iPage);
            }
        }
        System.debug('Leaving initializePages');
    }
    private void setAdditionalInfo() {
        System.debug('Entering setAdditionalInfo');
        c2g__codaInvoice__c line = [SELECT c.Radio_Additional_Info__c FROM c2g__codaInvoice__c c WHERE c.Id = :salesInvoiceId][0];
        this.AdditionalInfo = line.Radio_Additional_Info__c;
        System.debug('Leaving setAdditionalInfo');
    }
    private void setAgencyCommission() {
        System.debug('Entering setAgencyCommission');
        List<c2g__codaInvoice__c> lines = [SELECT c2g__InvoiceTotal__c, Gross_Invoice_Amount_Total__c FROM c2g__codaInvoice__c WHERE Id = :salesInvoiceId];
        if (lines.size() > 0) {
            Double net = lines[0].c2g__InvoiceTotal__c;
            Double gross = lines[0].Gross_Invoice_Amount_Total__c;
            if (gross > net) {
                this.AgencyCommission = gross - net;
            } else {
                this.AgencyCommission = 0.0;
            }
        }
        System.debug('Leaving setAgencyCommission');
    }
    private void setRadioInvoice() {
        System.debug('Entering setRadioInvoice');
        c2g__codaInvoice__c line = [SELECT c.c2g__Opportunity__r.Budget_Source__c, c.c2g__Opportunity__r.Slingshot_Eligible__c FROM c2g__codaInvoice__c c WHERE c.Id = :salesInvoiceId][0];
        if (line.c2g__Opportunity__r.Budget_Source__c == 'Radio' && line.c2g__Opportunity__r.Slingshot_Eligible__c) {
            this.RadioInvoice = True;
        } else {
            this.RadioInvoice = False;
        }

        if (RadioInvoice) {
            System.debug('Resetting page limits for radio invoice');
            invoicePage1Of1 -= 6;
            invoicePage1Ofx -= 6;
        }
        System.debug('Leaving setRadioInvoice');
    }
    private void setGrossAmount() {
        System.debug('Entering setGrossAmount');
        if (ShowGross) {
            this.GrossAmount = [SELECT  c.Gross_Invoice_Amount_Total__c  From c2g__codaInvoice__c c where c.Id = :salesInvoiceId][0].Gross_Invoice_Amount_Total__c;
            if (this.GrossAmount == Null || this.GrossAmount == 0) {
                this.GrossAmount = InvoiceTotal;
            }
        }
        System.debug('Leaving setGrossAmount');
    }
    private Integer calculateNumLines() {
        System.debug('Entering calculateNumLines');
        Integer TotalLines = 0;
        if (!this.UseInvoiceTemplate) {
            if (salesInvoiceId != null) {
                if (ShowZero == true) {
                    TotalLines = [Select c.c2g__LineNumber__c From c2g__codaInvoiceLineItem__c c where c.c2g__Invoice__c = :salesInvoiceId].size();
                } else {
                    TotalLines = [Select c.c2g__LineNumber__c From c2g__codaInvoiceLineItem__c c WHERE (c.c2g__Invoice__c = :salesInvoiceId AND c.c2g__NetValue__c  <> 0)].size();
                }
            }
        } else {
           if (salesInvoiceId != null) {
                if (ShowZero == true) {
                    TotalLines =  [Select Line_Number__c From Invoice_Template_Line__c where Invoice_Number__c = :salesInvoiceId].size();
                } else {
                    TotalLines =  [Select Line_Number__c From Invoice_Template_Line__c WHERE (Invoice_Number__c = :salesInvoiceId AND Net_Value__c <> 0)].size();
                }
            }
        }
        System.debug('Leaving calculateNumLines: ' + TotalLines);
       return TotalLines;
    }
    private Integer calculateNumPages(Integer TotalLines) {
        System.debug('Entering calculateNumPages');
        Integer TotalPages = 0;
        if (TotalLines <= invoicePage1Of1) {
            TotalPages = 1;
        } else if (TotalLines <= (invoicePage1OfX + invoiceLastPage)) {
            TotalPages = 2;
        } else {
            decimal qdec = (decimal.valueOf(TotalLines - invoicePage1OfX - invoiceLastPage)/invoicePageXOfX);
            TotalPages = 2 + Integer.ValueOf(qdec.ROUND(System.RoundingMode.UP)); 
        }
        System.debug('Leaving calculateNumPages: ' + TotalPages);
        return TotalPages;
    }
    private List<c2g__codaInvoiceLineItem__c[]> paginateInvoiceLines() {
        System.debug('Entering paginateInvoiceLines');
        Integer TotalLines = calculateNumLines();
        Integer TotalPages = calculateNumPages(TotalLines);        
        
        List<c2g__codaInvoiceLineItem__c[]> pageBrokenInvoiceLines = new List<c2g__codaInvoiceLineItem__c[]>();  
        c2g__codaInvoiceLineItem__c[] pageOfInvoiceItems = new c2g__codaInvoiceLineItem__c[]{};
        List<c2g__codaInvoiceLineItem__c> lineItems = null;

        if (salesInvoiceId != null) {
            lineItems =  [Select c.c2g__UsePartPeriods__c, c.c2g__UnitPrice__c, c.c2g__UnitOfWork__c, c.c2g__TaxValueTotal__c, c.c2g__TaxValue3__c, c.c2g__TaxValue2__c, c.c2g__TaxValue1__c, c.c2g__TaxRateTotal__c, 
                               c.c2g__TaxRate3__c, c.c2g__TaxRate2__c, c.c2g__TaxRate1__c, c.c2g__TaxCode3__c, c.c2g__TaxCode2__c, c.c2g__TaxCode1__r.Name, c.c2g__TaxCode2__r.Name, c.c2g__TaxCode3__r.Name, 
                               c.c2g__TaxCode1__c, c.c2g__StartDate__c, c.c2g__Scheduled__c, c.c2g__Quantity__c, c.c2g__Product__r.ProductCode, c.c2g__Product__r.Name, c.c2g__Product__c, c.c2g__PeriodInterval__c, 
                               c.c2g__OwnerCompany__c, c.c2g__NumberofJournals__c, c.c2g__NetValue__c, c.Gross_Amt__c, c.c2g__LineNumber__c, c.Alt_Line_Number__c, c.Current_Page_No__c, c.c2g__LineDescription__c, c.c2g__Invoice__c, c.c2g__IncomeSchedule__c, c.c2g__IncomeScheduleGroup__c, 
                               c.c2g__GeneralLedgerAccount__c, c.c2g__ExternalId__c, c.c2g__Dimension4__c, c.c2g__Dimension3__c, c.c2g__Dimension2__c, c.c2g__Dimension1__c, c.SystemModstamp, c.Name, c.sProduct_Name__c, c.LastModifiedDate, 
                               c.LastModifiedById, c.IsDeleted, c.Id, /* c.CurrencyIsoCode, */ c.CreatedDate, c.CreatedById, c.Creative_Name__c, c.Length__c
                               From c2g__codaInvoiceLineItem__c c 
                               where c.c2g__Invoice__c = :salesInvoiceId order by c.c2g__LineNumber__c];

            Integer index = 1;
            Integer currentPage = 1;
            for (c2g__codaInvoiceLineItem__c li:  LineItems) {
                if (!ShowZero && li.c2g__NetValue__c == 0) {
                    continue;
                }
                li.Alt_Line_Number__c = index;
                index++;

                pageOfInvoiceItems.add(li);
                if (TotalPages == 1) {
                    //only one page - so no checks need to be performed
                } else if (TotalPages == 2) {
                    if (currentPage == 1 && pageOfInvoiceItems.size() >= InvoicePage1OfX) {
                        pageBrokenInvoiceLines.add(pageOfInvoiceItems);
                        pageOfInvoiceItems = new c2g__codaInvoiceLineItem__c[]{};
                        currentPage++;
                    } else {
                        //do nothing - we're either on the last page or the page isn't big enough yet
                    }
                } else {
                    //multiple pages
                    if ((currentPage == 1 && pageOfInvoiceItems.size() >= InvoicePage1OfX) ||
                        (currentPage > 1 && pageOfInvoiceItems.size() >= InvoicePageXOfX)) {
                        pageBrokenInvoiceLines.add(pageOfInvoiceItems);
                        pageOfInvoiceItems = new c2g__codaInvoiceLineItem__c[]{};
                        currentPage++;
                    } else {
                        //do nothing
                    }
                }
            }
            if (!pageOfInvoiceItems.isEmpty()) {
                pageBrokenInvoiceLines.add(pageOfInvoiceItems);
            }
        }
        System.debug('Leaving paginateInvoiceLines: ' + pageBrokenInvoiceLines.size());
        return pageBrokenInvoiceLines;      
    }
    private List<Invoice_Template_Line__c[]> paginateInvoiceTemplateLines() {
        System.debug('Entering paginateInvoiceTemplateLines');
        Integer TotalLines = calculateNumLines();
        Integer TotalPages = calculateNumPages(TotalLines);        
        
        List<Invoice_Template_Line__c[]> pageBrokenInvoiceLines = new List<Invoice_Template_Line__c[]>();
        Invoice_Template_Line__c[] pageOfInvoiceItems = new Invoice_Template_Line__c[]{};
        List<Invoice_Template_Line__c> lineItems = null;

        if (salesInvoiceId != null) {
            lineItems =  [Select Agency_IO_Placement_Name__c, Gross_Value__c, Invoice_Number__c, Line_Number__c, Net_Value__c, SProduct_Name__c, Creative_Name__c, Length__c
                            From Invoice_Template_Line__c
                            where Invoice_Number__c = :salesInvoiceId order by Line_Number__c];
            
            Integer index = 1;
            Integer currentPage = 1;
            for (Invoice_Template_Line__c li:  LineItems) {
                if (!ShowZero && li.Net_Value__c == 0) {
                    continue;
                }
                li.Line_Number__c = index;
                index++;

                pageOfInvoiceItems.add(li);
                if (TotalPages == 1) {
                    //only one page - so no checks need to be performed
                } else if (TotalPages == 2) {
                    if (currentPage == 1 && pageOfInvoiceItems.size() >= InvoicePage1OfX) {
                        pageBrokenInvoiceLines.add(pageOfInvoiceItems);
                        pageOfInvoiceItems = new Invoice_Template_Line__c[]{};
                        currentPage++;
                    } else {
                        //do nothing - we're either on the last page or the page isn't big enough yet
                    }
                } else {
                    //multiple pages
                    if ((currentPage == 1 && pageOfInvoiceItems.size() >= InvoicePage1OfX) ||
                        (currentPage > 1 && pageOfInvoiceItems.size() >= InvoicePageXOfX)) {
                        pageBrokenInvoiceLines.add(pageOfInvoiceItems);
                        pageOfInvoiceItems = new Invoice_Template_Line__c[]{};
                        currentPage++;
                    } else {
                        //do nothing
                    }
                }
            }
            if (!pageOfInvoiceItems.isEmpty()) {
                pageBrokenInvoiceLines.add(pageOfInvoiceItems);
            }
        }
        System.debug('Leaving paginateInvoiceTemplateLines: ' + pageBrokenInvoiceLines.size() + ', ' + pageBrokenInvoiceLines.get(0).size());
        return pageBrokenInvoiceLines;      
    }


    /*
        Getters
    */
    public String getinvoiceCurrencySymbol(){
        initialize();
        return isoCode;
    }
    public String getAdditionalInfo() {
        initialize();
        return this.AdditionalInfo;
    }
    public Double getAgencyCommission() {
        initialize();
        return this.AgencyCommission;
    }
    public Boolean getShowGross() {
        initialize();
        return this.ShowGross;
    }
    public Boolean getRadioInvoice() {
        initialize();
        return this.RadioInvoice;
    }
    public Boolean getBasicInvoice() {
        initialize();
        return !this.RadioInvoice;
    }
    public Boolean getShowNet() {
        initialize();
        return this.ShowNet;
    }
    public Boolean getUseInvoiceTemplateLines() {
        initialize();
        return this.UseInvoiceTemplate;
    }
    public Boolean getUseInvoiceLines() {
        initialize();
        return !this.UseInvoiceTemplate;
    }
    public Boolean getShowZero() {
        initialize();
        return this.ShowZero;
    }
    public Double getGrossAmount() {
        initialize();
        return this.GrossAmount;
    }
    public List<InvoicePage> getInvoicePageLines() {
        return InvoicePageLines;
    }
    public List<Integer> getBlankLines() {
        System.debug('Entering getBlankLines');
        initialize();
        Integer R1 = 0;
        Integer H1 = 0;
        Integer H2 = 0;
        Integer H3 = 0;
        Integer H4 = 0;
        Integer H5 = 0;
        Integer H6 = 0;
        Integer L1 = 0;
        Integer L2 = 0;
        Integer L3 = 0;
        Integer L4 = 0;
        Integer L5 = 0;
        Integer L6 = 0;
        Integer MinusLines;
        
        if ( RadioInvoice ) {R1 = 6;}
        if ( PrintedText1Heading == '' ) {H1 = 0;} else {H1 = 1;}
        if ( PrintedText2Heading == '' ) {H2 = 0;} else {H2 = 1;}
        if ( PrintedText3Heading == '' ) {H3 = 0;} else {H3 = 1;} 
        if ( PrintedText4Heading == '' ) {H4 = 0;} else {H4 = 1;}
        if ( PrintedText5Heading == '' ) {H5 = 0;} else {H5 = 1;}
        if ( EditablePrintedTextHeading == '' ) {H6 = 0;} else {H6 = 1;}
        if ( PrintedText1Text != Null && PrintedText1text != '') {L1 = Integer.valueOf(decimal.valueof(PrintedText1Text.Length()/140).Round(System.RoundingMode.UP));} else {L1 = 0;} 
        if ( PrintedText2Text != Null && PrintedText2text != '') {L2 = Integer.valueOf(decimal.valueof(PrintedText2Text.Length()/140).Round(System.RoundingMode.UP));} else {L2 = 0;} 
        if ( PrintedText3Text != Null && PrintedText3text != '') {L3 = Integer.valueOf(decimal.valueof(PrintedText3Text.Length()/140).Round(System.RoundingMode.UP));} else {L3 = 0;} 
        if ( PrintedText4Text != Null && PrintedText4text != '') {L4 = Integer.valueOf(decimal.valueof(PrintedText4Text.Length()/140).Round(System.RoundingMode.UP));} else {L4 = 0;} 
        if ( PrintedText5Text != Null && PrintedText5text != '') {L5 = Integer.valueOf(decimal.valueof(PrintedText5Text.Length()/140).Round(System.RoundingMode.UP));} else {L5 = 0;} 
        if ( EditablePrintedText != Null && EditablePrintedText != '') {L6 = Integer.valueOf(decimal.valueof(EditablePrintedText.Length()/140).Round(System.RoundingMode.UP));} else {L6 = 0;} 
        if ((R1 + L1 + L2 + L3 + L4 + L5 + L6 + H1 + H2 + H3 + H4 + H5 + H6 - 3) <= 0) {
            MinusLines = 0;
        } else {
            MinusLines = (R1 + L1 + L2 + L3 + L4 + L5 + L6 + H1 + H2 + H3 + H4 + H5 + H6 - 3);
        }
        
        
        Integer TotalLines = calculateNumLines();
        Integer TotalPages = calculateNumPages(TotalLines);        
        Integer Blanks = 0;
        if (TotalLines <= invoicePage1Of1) { 
            Blanks = invoicePage1Of1 - TotalLines - 1 - MinusLines;
        } else if (TotalLines <= (invoicePage1Of1 + invoiceLastPage)) {
            Blanks = InvoicePage1OfX + InvoiceLastPage - TotalLines - 1 - MinusLines;
        } else { 
            Blanks = InvoicePage1OfX + InvoiceLastPage + invoicePageXOfX * (TotalPages - 2) - TotalLines - MinusLines;
        }

        List<Integer> BlankLinesToAdd = new List<Integer>();
        for (Integer i=0; i<Blanks; i++) {
            BlankLinesToAdd.add(i);
        }
        System.debug('Leaving getBlankLines: ' + BlankLinesToAdd);
        return BlankLinesToAdd;           
    }
}