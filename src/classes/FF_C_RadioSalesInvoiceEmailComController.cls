public with sharing class FF_C_RadioSalesInvoiceEmailComController {

    public class RadioSalesInvoiceLine {
        public Boolean LastLine {get;set;}
        public Radio_Campaign_Detail_Line__c Line {get;set;}
    }

    public class RadioSalesInvoicePage {
        public String PageBreakBefore {get;set;}
        public RadioSalesInvoiceLine[] Page {get;set;}
    }

    public Id salesInvoiceId{get;set;}
    public ID salesInvoiceID1{get;set;}
    
    public Integer invoicePage1Of1{get;set;}
    public Integer invoicePage1OfX{get;set;}
    public Integer invoicePageXOfX{get;set;}
    public Integer invoiceLastPage{get;set;}

    public String campaignName{get;set;}
    public String billingPeriod{get;set;}
    public Double AgencyDiscount = 0.0;
    public Double NetDue = 0.0;

    private Boolean Initialized = false;

    public Decimal PaidTotalOrderedImps = 0;
    public Double PaidTotalOrderedGRPs = 0;
    public Decimal PaidTotalDeliveredImps = 0;
    public Double PaidTotalDeliveredGRPs = 0;
    public Decimal PaidTotalBillableImps = 0;
    public Double PaidTotalBillableGRPs = 0;

    public Decimal AVTotalOrderedImps = 0;
    public Double AVTotalOrderedGRPs = 0;
    public Decimal AVTotalDeliveredImps = 0;
    public Double AVTotalDeliveredGRPs = 0;
    public Decimal AVTotalBillableImps = 0;
    public Double AVTotalBillableGRPs = 0;

    public List<RadioSalesInvoicePage> RadioInvoicePageLines = new List<RadioSalesInvoicePage>();

    private void initialize() {
        if (!this.Initialized) {
            initializeDataElements();
            initializePages();
            this.Initialized = true;
        }
    }
    private void initializeDataElements() {
        System.debug('Entering FF_C_RadioSalesInvoiceEmailComController::initializeDataElements');
        SetAgencyDiscount();
        SetNetDue();
        System.debug('Leaving FF_C_RadioSalesInvoiceEmailComController::initializeDataElements');
    }
    private void initializePages() {
        System.debug('Entering FF_C_RadioSalesInvoiceEmailComController::initializePages');
        paginateRadioInvoiceLines();
        System.debug('Leaving FF_C_RadioSalesInvoiceEmailComController::initializePages');
    }
    private void SetAgencyDiscount() {
        List<c2g__codaInvoice__c> lines = [SELECT c2g__InvoiceTotal__c, Gross_Invoice_Amount_Total__c FROM c2g__codaInvoice__c WHERE Id = :salesInvoiceId];
        if (lines.size() > 0) {
            Double net = lines[0].c2g__InvoiceTotal__c;
            Double gross = lines[0].Gross_Invoice_Amount_Total__c;
            if (gross > net) {
                AgencyDiscount = gross - net;
            } else {
                AgencyDiscount = 0.0;
            }
        }
    }
    private void SetNetDue() {
        List<c2g__codaInvoice__c> lines = [SELECT c2g__NetTotal__c FROM c2g__codaInvoice__c WHERE Id = :salesInvoiceId];
        if (lines.size() > 0) {
            NetDue = lines[0].c2g__NetTotal__c;
        }
    }
    private Integer calculateNumLines() {
        System.debug('Entering FF_C_RadioSalesInvoiceEmailComController::calculateNumLines');
        Integer TotalLines = 0;
        if (salesInvoiceId != null) {
            TotalLines = [Select c.Ad_Name__c From Radio_Campaign_Detail_Line__c c WHERE (c.Sales_Invoice__c = :salesInvoiceId)].size();
        }
        System.debug('Leaving FF_C_RadioSalesInvoiceEmailComController::calculateNumLines: ' + TotalLines);
       return TotalLines;
    }
    private Integer calculateNumPages(Integer TotalLines) {
        System.debug('Entering FF_C_RadioSalesInvoiceEmailComController::calculateNumPages');
        Integer TotalPages = 0;
        if (TotalLines <= invoicePage1Of1) {
            TotalPages = 1;
        } else if (TotalLines <= (invoicePage1OfX + invoiceLastPage)) {
            TotalPages = 2;
        } else {
            decimal qdec = (decimal.valueOf(TotalLines - invoicePage1OfX - invoiceLastPage)/invoicePageXOfX);
            TotalPages = 2 + Integer.ValueOf(qdec.ROUND(System.RoundingMode.UP)); 
        }
        System.debug('Leaving FF_C_RadioSalesInvoiceEmailComController::calculateNumPages: ' + TotalPages);
        return TotalPages;
    }
    private void paginateRadioInvoiceLines() {
        System.debug('Entering FF_C_RadioSalesInvoiceEmailComController::paginateRadioInvoiceLines');
        Integer TotalLines = calculateNumLines();
        Integer TotalPages = calculateNumPages(TotalLines);        
        
        RadioSalesInvoiceLine[] pageOfInvoiceItems = new RadioSalesInvoiceLine[]{};
        List<Radio_Campaign_Detail_Line__c> lineItems = null;

        if (salesInvoiceId != null) {
            lineItems =  [Select Ad_Name__c, ISCI_Code__c, Start_Date__c, End_Date__c, CPM_Rate__c,
                          Ordered_Impressions__c, Ordered_GRP__c, Delivered_Impressions__c, Delivered_GRP__c, 
                          Billable_Impressions__c, Billable_Gross_Due__c, Agency_IO_Placement_Name__c
                          From Radio_Campaign_Detail_Line__c WHERE (Sales_Invoice__c = :salesInvoiceId)];

            Integer currentPage = 1;
            for (Radio_Campaign_Detail_Line__c li:  LineItems) {
                if (Double.ValueOf(li.CPM_Rate__c) <> 0) {
                    this.PaidTotalOrderedImps += li.Ordered_Impressions__c;
                    this.PaidTotalOrderedGRPs += li.Ordered_GRP__c;
                    this.PaidTotalDeliveredImps += li.Delivered_Impressions__c;
                    this.PaidTotalDeliveredGRPs += li.Delivered_GRP__c;
                    this.PaidTotalBillableImps += li.Billable_Impressions__c;
                    this.PaidTotalBillableGRPs += li.Billable_Gross_Due__c;
                } else {
                    this.AVTotalOrderedImps += li.Ordered_Impressions__c;
                    this.AVTotalOrderedGRPs += li.Ordered_GRP__c;
                    this.AVTotalDeliveredImps += li.Delivered_Impressions__c;
                    this.AVTotalDeliveredGRPs += li.Delivered_GRP__c;
                    this.AVTotalBillableImps += li.Billable_Impressions__c;
                    this.AVTotalBillableGRPs += li.Billable_Gross_Due__c;
                }

                RadioSalesInvoiceLine line = new RadioSalesInvoiceLine();
                line.LastLine = false;
                line.Line = li;
                pageOfInvoiceItems.add(line);
                if (TotalPages == 1) {
                    //only one page - so no checks need to be performed
                } else if (TotalPages == 2) {
                    if (currentPage == 1 && pageOfInvoiceItems.size() >= InvoicePage1OfX) {
                        RadioSalesInvoicePage page = new RadioSalesInvoicePage();
                        page.Page = pageOfInvoiceItems;
                        if (RadioInvoicePageLines.size() > 0) {
                            page.PageBreakBefore = 'page-break-before:always;';
                        } else {
                            page.PageBreakBefore = 'page-break-before:never;';
                        }
                        RadioInvoicePageLines.add(page);
                        pageOfInvoiceItems = new RadioSalesInvoiceLine[]{};
                        currentPage++;
                    } else {
                        //do nothing - we're either on the last page or the page isn't big enough yet
                    }
                } else {
                    //multiple pages
                    if ((currentPage == 1 && pageOfInvoiceItems.size() >= InvoicePage1OfX) ||
                        (currentPage > 1 && pageOfInvoiceItems.size() >= InvoicePageXOfX)) {
                        RadioSalesInvoicePage page = new RadioSalesInvoicePage();
                        page.Page = pageOfInvoiceItems;
                        if (RadioInvoicePageLines.size() > 0) {
                            page.PageBreakBefore = 'page-break-before:always;';
                        } else {
                            page.PageBreakBefore = 'page-break-before:never;';
                        }
                        RadioInvoicePageLines.add(page);
                        pageOfInvoiceItems = new RadioSalesInvoiceLine[]{};
                        currentPage++;
                    } else {
                        //do nothing
                    }
                }
            }
            RadioSalesInvoiceLine line = new RadioSalesInvoiceLine();
            line.LastLine = true;
            pageOfInvoiceItems.add(line);
            if (!pageOfInvoiceItems.isEmpty()) {
                RadioSalesInvoicePage page = new RadioSalesInvoicePage();
                page.Page = pageOfInvoiceItems;
                if (RadioInvoicePageLines.size() > 0) {
                    page.PageBreakBefore = 'page-break-before:always;';
                } else {
                    page.PageBreakBefore = 'page-break-before:never;';
                }
                RadioInvoicePageLines.add(page);
            }
        }
        System.debug('Leaving FF_C_RadioSalesInvoiceEmailComController::paginateRadioInvoiceLines: ' + RadioInvoicePageLines.size());
    }


    /*
        Getters
    */
    public Decimal getPaidTotalOrderedImps() {
        initialize();
        return this.PaidTotalOrderedImps;
    }
    public Double getPaidTotalOrderedGRPs() {
        initialize();
        return this.PaidTotalOrderedGRPs;
    }
    public Decimal getPaidTotalDeliveredImps() {
        initialize();
        return this.PaidTotalDeliveredImps;
    }
    public Double getPaidTotalDeliveredGRPs() {
        initialize();
        return this.PaidTotalDeliveredGRPs;
    }
    public Decimal getPaidTotalBillableImps() {
        initialize();
        return this.PaidTotalBillableImps;
    }
    public Double getPaidTotalBillableGRPs() {
        initialize();
        return this.PaidTotalBillableGRPs;
    }
    public Decimal getAVTotalOrderedImps() {
        initialize();
        return this.AVTotalOrderedImps;
    }
    public Double getAVTotalOrderedGRPs() {
        initialize();
        return this.AVTotalOrderedGRPs;
    }
    public Decimal getAVTotalDeliveredImps() {
        initialize();
        return this.AVTotalDeliveredImps;
    }
    public Double getAVTotalDeliveredGRPs() {
        initialize();
        return this.AVTotalDeliveredGRPs;
    }
    public Decimal getAVTotalBillableImps() {
        initialize();
        return this.AVTotalBillableImps;
    }
    public Double getAVTotalBillableGRPs() {
        initialize();
        return this.AVTotalBillableGRPs;
    }
    public Double getNetDue() {
        initialize();
        return this.NetDue;
    }
    public Double getAgencyDiscount() {
        initialize();
        return this.AgencyDiscount;
    }
    public List<RadioSalesInvoicePage> getRadioInvoicePageLines() {
        initialize();
        return RadioInvoicePageLines;
    }
}