public with sharing class FF_C_SalesCreditNoteEmailComController 
{
    public Id salesCreditNoteId {get;set;}
    public Id salesInvoiceId{get;set;}
    public Id salesCreditNoteID1{get;set;}
   // public String taxType{get;set;}
    public String isoCode{get;set;}
    
    public Boolean ShowGross{get;set;}
    public Boolean ShowZero{get;set;}
 
 //   public Double netTotal{get;set;}.NegativeNetTotal__c
    public Double taxTotal{get;set;}
    public Double CreditNoteTotal{get;set;}
    
    public Integer invoicePage1Of1{get;set;}
    public Integer invoicePage1OfX{get;set;}
    public Integer invoicePageXOfX{get;set;}
    public Integer invoiceLastPage{get;set;}
    
    Public String PrintedText1Heading{get;set;}//
    Public String PrintedText1Text{get;set;}
    Public String PrintedText2Heading{get;set;}
    Public String PrintedText2Text{get;set;}
    Public String PrintedText3Heading{get;set;}
    Public String PrintedText3Text{get;set;}
    Public String PrintedText4Heading{get;set;}
    Public String PrintedText4Text{get;set;}
    Public String PrintedText5Heading{get;set;}
    Public String PrintedText5Text{get;set;}
    
    
    
 
//PAGINATION VARIABLES   
    public list<c2g__codaCreditNoteLineItem__c[]> pageBrokenCreditNoteLines;
    public list<c2g__codaCreditNoteLineItem__c[]> pageBrokenCreditNoteLinesLast;
    public c2g__codaCreditNoteLineItem__c[] pageOfCreditNoteItems = new c2g__codaCreditNoteLineItem__c[]{};

    public integer TotalLines;
    public integer TotalPages;
    public integer CurrentLine;
    public integer LastLoop;
    public integer LastBlankLines;
    
    public Boolean ShowZero1;

    Public Boolean ShowGross1;
    public Boolean ShowNet;
    Public Boolean ShowNet1;
    
    
    Public Double GrossAmount;
    Public Double GrossAmount1;

    private static List<c2g__codaCreditNoteLineItem__c> LineItems1;
    private static List<c2g__codaCreditNoteLineItem__c> lineItems;
    private static List<c2g__codaCreditNoteLineItem__c> GrossAmount;
    

    private ApexPages.StandardController controller;   
    public List<c2g__CODACreditNote__c> CreditNoteInfo;  
    c2g__CODACreditNote__c CreditNoteInfo1;      
    public c2g__CODACreditNOte__c CODACreditNoteInfo;  
 
public PageReference refresh(){
    Boolean Success = false;
    Boolean Success1 = false;
    
    if (salesCreditNOteID ==null) {
        return Null;
    }
    if (ShowGross != Null) {Success1 = true;} 

    if (PageBrokenCreditNoteLines != null) {Success = true;}                          
    return Null; 
    } 
    
// SHOW GROSS OPTION BOOLEAN        
public boolean getShowGross() {
        Return ShowGross;    }    
        

// GET GROSS AMOUNT        
public Double getGrossAmount() {
   Double GrossAmount = 0; 
   return GrossAmount;
   }    


public string getCreditNoteCurrencySymbol(){
    return isoCode;
    }

// SHOW NET OPTION BOOLEAN        
public boolean getShowNet() {
        if (ShowGross == true) {ShowNet = False;}
        else {ShowNet = true;}   
                Return ShowNet;
                    }

//  COUNTS TOTAL NUMBER OF PRINTED LINES
    public integer getTotalLines(){
           if(lineItems == null && salesCreditNoteId != null){                 
            integer l=0;
            if (ShowZero == true) {lineItems1 =  [Select c.c2g__LineNumber__c, c.c2g__CreditNote__c From c2g__codaCreditNoteLineItem__c c where c.c2g__CreditNote__c = :salesCreditNoteId];}
            else {lineItems1 =  [Select c.c2g__LineNumber__c, c.c2g__CreditNote__c From c2g__codaCreditNoteLineItem__c c WHERE (c.c2g__CreditNote__c = :salesCreditNoteId AND c.c2g__NetValue__c  <> 0)] ;}
            for (c2g__codaCreditNoteLineItem__c lz:  LineItems1){ l++;}
            TotalLines = l;
        }
        return totalLines;
        }
    
//  CALCULATES IF FIRST MORE THAN ONE PAGE
    public Boolean getMoreThanOnePage(){
        boolean tf=true;        
        return tf;        
    }    
              
        
//  CALCULATES TOTAL NUMBER OF PAGES
    public integer getTotalPages(){    
        integer p;
        decimal idec;
        Integer Pages2 = InvoicePage1OfX + InvoiceLastPage;
              
        if (TotalLines <= InvoicePage1Of1){p=1;}
        else if (TotalLines <= Pages2){p=2;}
        else {
                p = integer.ValueOf((decimal.valueOf(TotalLines - InvoicePage1OfX - InvoicelastPage)/InvoicePageXofX).Round(System.RoundingMode.UP))+2;
            }
        TotalPages = p;
        return TotalPages;   
    }
    
// CALCULATES LAST LINE NUMBER TO INCLUDE IN LOOP 
    Public integer getLastLoop(){
        integer ll;
        if (TotalPages == 1) {ll = TotalLines;}
        else {
            if (TotalPages == 2) {ll = TotalLines - InvoicePage1OfX;}
            else {ll = (TotalLines - InvoicePage1OfX  - (TotalPages - 2)*InvoicePageXOfX);     
        } 
        }        
        if (ll <= 2 ) {LastLoop = (TotalLines - 2);}
             else 
                   {LastLoop = TotalLines - ll;}
        Return LastLoop;
       }
       
    Public double getNegativeNetAmount(){
        List<c2g__codaCreditNoteLineItem__c> na1 = [SELECT c.c2g__NetValue__c, c.GrossTotal__c FROM c2g__codaCreditNoteLineItem__c c WHERE c.c2g__CreditNote__c = :salesCreditNoteId];
        Double z = 0;
        for (c2g__codaCreditNoteLineItem__c na1n : na1){
            z = z - na1n.c2g__NetValue__c;
        }
        return z;
    }

    Public double getNegativeGrossAmount(){
        List<c2g__codaCreditNoteLineItem__c> na2 = [SELECT c.c2g__NetValue__c, c.GrossTotal__c FROM c2g__codaCreditNoteLineItem__c c WHERE c.c2g__CreditNote__c = :salesCreditNoteId];
        Double z = 0;
        for (c2g__codaCreditNoteLineItem__c na2n : na2){
            z = z - na2n.GrossTotal__c;
        }
        return z;
    }


//%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    Public List<Integer> getBlankLines(){
        integer i=0;        
        Integer Blanks = 0;
        Integer Blanks1 = 0;
        List<Integer> BlankLinesToAdd = new List<Integer>();
        
        Integer H1 = 0;
        Integer H2 = 0;
        Integer H3 = 0;
        Integer H4 = 0;
        Integer H5 = 0;
        Integer L1 = 0;
        Integer L2 = 0;
        Integer L3 = 0;
        Integer L4 = 0;
        Integer L5 = 0;
        Integer MinusLines;
        Integer MinusLines1;
        
        if ( PrintedText1Heading == '' ) {H1 = 0;} else {H1 = 1;}
        if ( PrintedText2Heading == '' ) {H2 = 0;} else {H2 = 1;}
        if ( PrintedText3Heading == '' ) {H3 = 0;} else {H3 = 1;} 
        if ( PrintedText4Heading == '' ) {H4 = 0;} else {H4 = 1;}
        if ( PrintedText4Heading == '' ) {H5 = 0;} else {H5 = 1;}
        if ( PrintedTExt1Text != Null && PrintedText1text != '') {L1 = integer.valueOf(decimal.valueof(PrintedText1Text.Length()/140).Round(System.RoundingMode.UP));} else {L1 = 0;} 
        if ( PrintedTExt2Text != Null && PrintedText2text != '') {L2 = integer.valueOf(decimal.valueof(PrintedText2Text.Length()/140).Round(System.RoundingMode.UP));} else {L2 = 0;} 
        if ( PrintedTExt3Text != Null && PrintedText3text != '') {L3 = integer.valueOf(decimal.valueof(PrintedText3Text.Length()/140).Round(System.RoundingMode.UP));} else {L3 = 0;} 
        if ( PrintedTExt4Text != Null && PrintedText4text != '') {L4 = integer.valueOf(decimal.valueof(PrintedText4Text.Length()/140).Round(System.RoundingMode.UP));} else {L4 = 0;} 
        if ( PrintedTExt5Text != Null && PrintedText5text != '') {L5 = integer.valueOf(decimal.valueof(PrintedText5Text.Length()/140).Round(System.RoundingMode.UP));} else {L5 = 0;} 
        MinusLines1 = L1 + L2 + L3 + L4 + L5 + H1 + H2 + H3 + H4 + H5 - 3;
        if (MinusLines1 <= 0) {MinusLines = 0;} else { MinusLines = MinusLines1;}
        
        
        if(lineItems == null && salesCreditNoteId != null){                   
            integer l=0;
            if (ShowZero == true) {lineItems1 =  [Select c.c2g__LineNumber__c, c.c2g__CreditNote__c From c2g__codaCreditNoteLineItem__c c where c.c2g__CreditNote__c = :salesCreditNoteId];}
            else {lineItems1 =  [Select c.c2g__LineNumber__c, c.c2g__CreditNote__c From c2g__codaCreditNoteLineItem__c c WHERE (c.c2g__CreditNote__c = :salesCreditNoteId AND c.c2g__NetValue__c  <> 0)] ;}
            for (c2g__codaCreditNoteLineItem__c lz:  LineItems1){ l++;}
            TotalLines = l;
        }

                // CALCULATES TOTAL NUMBER OF PAGES        
        integer q;
        decimal qdec;      
        if (TotalLines <= invoicePage1Of1){q=1;}
        else{
            if (TotalLines <= (invoicePage1OfX + invoiceLastPage)) {q=2;}
                else {
                    qdec = decimal.valueOf(TotalLines - invoicePage1OfX - invoiceLastPage)/invoicePageXOfX;
                    q = integer.ValueOf(2 + qdec.ROUND(System.RoundingMode.UP));  //System.RoundingMode.UP
                }
        }
        TotalPages = q; 
        
        if (TotalLines <= invoicePage1Of1){ Blanks = invoicePage1Of1 - TotalLines-1 - MinusLines;}
        else { if ( TotalLines <= (invoicePage1Of1 + invoiceLastPage)) {Blanks = InvoicePage1OfX + InvoiceLastPage - TotalLines-1 - MinusLines;}
               else { Blanks = InvoicePage1OfX + InvoiceLastPage + invoicePageXOfX * (TotalPages - 2) - TotalLines - MinusLines;
               }
        }
        if (Blanks <= 0) {Blanks1 = 0;} else {Blanks1 = Blanks;}
           
            while ( i <= Blanks1 ){
                i++;
                BlankLinesToAdd.add(i);
            }         
            return BlankLinesToAdd;           
    }

//%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

//  GET INVOICE LINE ITEMS AS DETERMINED BY GET GROSS AMOUNT AND SHOW ZERO OPTIONS    
    public List<c2g__codaCreditNoteLineItem__c[]> getPageBrokenCreditNoteLines1()
    {
        integer i = 1;
        integer j = 1;
        decimal qdec1;
        integer LineNumber;
        string ProductName;
        string LineDescription;
        double NetValue;
        
        if (ShowGross == null) {ShowGross = false;}
        if (ShowZero == Null) {ShowZero = True; }
        
        
        
    // CALCULATES TOTAL NUMBER OF LINE ITEMS       
        if(lineItems == null && salesCreditNoteId != null){                   
            integer l=0;
            if (ShowZero == true) {lineItems1 =  [Select c.c2g__LineNumber__c, c.c2g__CreditNote__c From c2g__codaCreditNoteLineItem__c c where c.c2g__CreditNote__c = :salesCreditNoteId];}
            else {lineItems1 =  [Select c.c2g__LineNumber__c, c.c2g__CreditNote__c, c.GrossTotal__c  From c2g__codaCreditNoteLineItem__c c WHERE (c.c2g__CreditNote__c = :salesCreditNoteId AND c.c2g__NetValue__c  <> 0)] ;}
            for (c2g__codaCreditNoteLineItem__c lz:  LineItems1){ l++;}
            TotalLines = l;
        }

      // CALCULATES TOTAL NUMBER OF PAGES        
        integer p;
        decimal idec;
              
        if (TotalLines <= InvoicePage1Of1){p=1;}
        else{
            if (TotalLines <= (InvoicePage1OfX + InvoiceLastPage)) {p=2;}
            else{
                p = integer.ValueOf((decimal.valueOf(TotalLines - InvoicePage1OfX - InvoicelastPage)/InvoicePageXofX).Round(System.RoundingMode.UP))+2;
            }
        }
        TotalPages = p;
  
        
        
    // CALCULATES LAST LINE NUMBER TO INCLUDE IN LOOP 
        integer ll;
        if (TotalPages == 1) {ll = TotalLines;}
        else {
            if (TotalPages == 2) {ll = TotalLines - InvoicePage1OfX;}
            else {ll = (TotalLines - InvoicePage1OfX  - (TotalPages - 2)*InvoicePageXOfX);     
        } 
        }        
        if (ll <= 2 ) {
            LastLoop = (TotalLines - 2);}
        else 
                   {LastLoop = TotalLines - ll;}
                        

        
        pageBrokenCreditNoteLines = New List<c2g__codaCreditNOteLineItem__c[]>();  
        c2g__codaCreditNoteLineItem__c[] pageOfCreditNoteItems = new c2g__codaCreditNoteLineItem__c[]{};
        Integer counter = 0;
        boolean firstBreakFound = false;
        boolean setSubSeqBreak = false;
        integer breakpoint = invoicePage1Of1;
        
        if (totalPages == 1){breakPoint = InvoicePage1Of1;}
        else {
                if (ll <= 2 && totalPages == 2 ) {
                    breakpoint = InvoicePage1OfX - 2;}
                else {breakpoint = invoicePage1OfX;}
        }    
        
       
        if(lineItems == null ) //&& salesInvoiceId != null)
        {
        if (ShowZero == true) {
          lineItems =  [Select c.c2g__LineNumber__c, c.c2g__Product__r.Name, c.c2g__LineDescription__c, c.c2g__Quantity__c, c.c2g__NetValue__c, NegativeNetValue__c, c.GrossTotal__c, c.sProductName__c  
                               From c2g__codaCreditNoteLineItem__c c 
                               where c.c2g__CreditNote__c = :salesCreditNoteId order by c.c2g__LineNumber__c];
                              }
        else                  {
            lineItems = [Select c.c2g__LineNumber__c, c.c2g__Product__r.Name, c.c2g__LineDescription__c, c.c2g__Quantity__c, c.c2g__NetValue__c, NegativeNetValue__c, c.GrossTotal__c, c.sProductName__c  
                               From c2g__codaCreditNoteLineItem__c c 
                               where ( c.c2g__CreditNote__c = :salesCreditNoteId AND c.c2g__NetValue__c  <> 0) order by c.c2g__LineNumber__c];
                                }
        }
      //try {  
      for (c2g__codaCreditNoteLineItem__c li:  LineItems) {
           li.Alt_Line_Number__c = i;
             CurrentLine = i;
            i++;
           
            if (TotalPages == 1){}
            else {
            if (i-1 <= LastLoop) {
            
                if (counter <= breakpoint) {
                    pageOfCreditNoteItems.add(li);
                    counter++;
                
                    if (counter == breakpoint) {
                        j++;
                        if (!firstBreakFound) {
                            firstBreakFound = true;
                            setSubSeqBreak = true;
                        }
                        if (setSubSeqBreak) {
                            if (j == TotalPages ){breakpoint=invoiceLastPage;}
                            else {
                                breakpoint = invoicePageXOfX;
                                setSubSeqBreak = false;
                            }                           
                        }
                        pageBrokenCreditNoteLines.add(pageOfCreditNoteItems);
                        pageOfCreditNoteItems = new c2g__codaCreditNoteLineItem__c[]{};
                        counter = 0;
                }
            }            
          }
          }                                                 
        }
      //  }
      //  catch (system.NullPointerException e) {
      //  }   
        if(!pageOfCreditNoteItems.isEmpty()){ 
            pageBrokenCreditNoteLines.add(pageOfCreditNoteItems);
        }            
        return PageBrokenCreditNoteLines;          
}


//%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
//  GET INVOICE LINE ITEMS AS DETERMINED BY GET GROSS AMOUNT AND SHOW ZERO OPTIONS    
   public List<c2g__codaCreditNoteLineItem__c[]> getPageBrokenCreditNoteLinesLast()
   {
        integer i = 1;
        integer LineNumber;
        string ProductName;
        string LineDescription;
        double NetValue;
        
        if (ShowGross == null) {ShowGross = false;}
        if (ShowZero == Null) {ShowZero = True; }

        // CALCULATES NUMBER OF LINES                   
        if(lineItems == null && salesCreditNoteId != null){                    
            integer l=0;
            if (ShowZero == true) {lineItems1 =  [Select c.c2g__LineNumber__c, c.c2g__CreditNote__c From c2g__codaCreditNoteLineItem__c c where c.c2g__CreditNote__c = :salesCreditNoteId];}
            else {lineItems1 =  [Select c.c2g__LineNumber__c, c.c2g__CreditNote__c From c2g__codaCreditNoteLineItem__c c WHERE (c.c2g__CreditNote__c = :salesCreditNoteId AND c.c2g__NetValue__c  <> 0)] ;}
            for (c2g__codaCreditNoteLineItem__c lz:  LineItems1){ l++;}
            TotalLines = l;
            
        }
       
        
    // CALCULATES TOTAL NUMBER OF PAGES        
        integer p;
        decimal idec;
              
        if (TotalLines <= InvoicePage1Of1){p=1;}
        else{
            if (TotalLines <= (InvoicePage1OfX + InvoiceLastPage)) {p=2;}
            else{
                idec = decimal.valueOf(TotalLines - InvoicePage1OfX - InvoicelastPage)/InvoicePageXofX;
                p = integer.ValueOf(2 + idec.ROUND(System.RoundingMode.UP));  //System.RoundingMode.UP
            }
        }
        TotalPages = p;
  
       
        
        
        // CALCULATES LAST LINE NUMBER TO INCLUDE IN LOOP 
        integer ll;
        if (TotalPages == 1) {ll = TotalLines;}
        else {
            if (TotalPages == 2) {ll = TotalLines - InvoicePage1OfX;}
            else {ll = (TotalLines - InvoicePage1OfX  - (TotalPages - 2)*InvoicePageXOfX);
            }
        }         
        if (ll <= 2 ) {LastLoop = (TotalLines - 2);}
             else 
             LastLoop= TotalLines - ll;
                      

        
        pageBrokenCreditNoteLinesLast = New List<c2g__codaCreditNoteLineItem__c[]>();  
        c2g__codaCreditNoteLineItem__c[] pageOfCreditNoteItemsLast = new c2g__codaCreditNoteLineItem__c[]{};
        Integer counter = 0;
        boolean firstBreakFound = false;
        boolean setSubSeqBreak = false;
        integer breakPoint = InvoicePage1Of1;
        
        
         if(lineItems == null)  // && salesInvoiceId != null)
        {                  
          if (ShowZero == true) {
          lineItems =  [Select c.c2g__LineNumber__c, c.c2g__Product__r.Name, c.c2g__LineDescription__c, c.c2g__Quantity__c, c.c2g__NetValue__c, NegativeNetValue__c, c.GrossTotal__c, c.sProductName__c
            
                               From c2g__codaCreditNoteLineItem__c c 
                               where c.c2g__CreditNote__c = :salesCreditNoteId order by c.c2g__LineNumber__c];
                              }
        else                  {
            lineItems = [Select c.c2g__LineNumber__c, c.c2g__Product__r.Name, c.c2g__LineDescription__c, c.c2g__Quantity__c, c.c2g__NetValue__c, NegativeNetValue__c, c.GrossTotal__c, c.sProductName__c  
                               From c2g__codaCreditNoteLineItem__c c 
                               where ( c.c2g__CreditNote__c = :salesCreditNoteId AND c.c2g__NetValue__c  <> 0) order by c.c2g__LineNumber__c];
                                }
        }
      

      try {
      for (c2g__codaCreditNoteLineItem__c li:  LineItems) {
            li.Alt_Line_Number__c = i;
            CurrentLine = i;
            i++;                       
            if (i-1 > LastLoop) {
                    pageOfCreditNoteItemsLast.add(li);
            }
      
      }
      }
       catch (system.NullPointerException e) {
      }
        PageBrokenCreditNoteLinesLast.add(pageOfCreditNoteItemsLast);                                                                  
        if(!pageOfCreditNoteItems.isEmpty()){ 
            pageBrokenCreditNoteLinesLast.add(pageOfCreditNoteItemsLast);
        }
                   
        return PageBrokenCreditNoteLinesLast;          
    }
    
 


}