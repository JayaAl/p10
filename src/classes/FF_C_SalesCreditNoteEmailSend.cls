global class FF_C_SalesCreditNoteEmailSend {
     
      webservice static id newSalesCreditNoteEmailSend(String tso, String User1, Boolean sta1){
          string wid = [SELECT id FROM c2g__CODACreditNote__c WHERE ID=:tso LIMIT 1][0].Id;
          final string template ='CODASalesCreditNote';
          User currentUser = [SELECT Id FROM User WHERE UserName =:UserInfo.getUserName() LIMIT 1];
          String UserT = User1;
          String UserT1 = [SELECT c2g__Invoice__r.Billing_Contact_Email_Address__c from c2g__CODACreditNote__c WHERE ID=:tso LIMIT 1][0].c2g__Invoice__r.Billing_Contact_Email_Address__c;
       
         if (sta1 == true){
              UserT = UserT1;
          }
          else {
              userT = User1;
          }
          messaging.singleEmailMessage message = new messaging.SingleEmailMessage();
          message.setSaveAsActivity(false);
          message.setTemplateID([SELECT Id FROM EmailTemplate WHERE Name =: template].Id);
          message.setToAddresses(new String[] {UserT});
          message.setTargetObjectID(currentUser.ID);
          message.setWhatID(wid);
          messaging.sendEmail(new Messaging.SingleEmailMessage[] {message});
      return null;
      
  }
}