public with sharing class FF_C_SalesInvoiceEmailComController {

    public Id salesInvoiceId{get;set;}
    public ID salesInvoiceID1{get;set;}
    public String taxType{get;set;}
    public String isoCode{get;set;}
    
    public Double netTotal{get;set;}
    public Double taxTotal{get;set;}
    public Double invoiceTotal{get;set;}
    
    public Integer invoicePage1Of1{get;set;}
    public Integer invoicePage1OfX{get;set;}
    public Integer invoicePageXOfX{get;set;}
    public Integer invoiceLastPage{get;set;}
    
    Public String PrintedText1Heading{get;set;}
    Public String PrintedText1Text{get;set;}
    Public String PrintedText2Heading{get;set;}
    Public String PrintedText2Text{get;set;}
    Public String PrintedText3Heading{get;set;}
    Public String PrintedText3Text{get;set;}
    Public String PrintedText4Heading{get;set;}
    Public String PrintedText4Text{get;set;}
    Public String PrintedText5Heading{get;set;}
    Public String PrintedText5Text{get;set;}
    
    
    
 
//PAGINATION VARIABLES   
    public list<c2g__codaInvoiceLineItem__c[]> pageBrokenInvoiceLines;
    public list<c2g__codaInvoiceLineItem__c[]> pageBrokenInvoiceLinesLast;
    public c2g__codaInvoiceLineItem__c[] pageOfInvoiceItems = new c2g__codaInvoiceLineItem__c[]{};

    public integer TotalLines;
    public integer TotalPages;
    public integer CurrentLine;
    public integer LastLoop;
    public integer LastBlankLines;
    
    public Boolean ShowZero;
    public Boolean ShowZero1;
    
    public Boolean ShowGross;
    Public Boolean ShowGross1;
    public Boolean ShowNet;
    Public Boolean ShowNet1;
    
    
    Public Double GrossAmount;
    Public Double GrossAmount1;

    private static List<c2g__codaInvoiceLineItem__c> LineItems1;
    private static List<c2g__codaInvoiceLineItem__c> lineItems;
    private static List<c2g__codaInvoiceLineItem__c> GrossAmount;
    

    private ApexPages.StandardController controller;   
    public List<c2g__CODAInvoice__c> InvoiceInfo;  
    c2g__CODAInvoice__c InvoiceInfo1;      
    public c2g__CODAInvoice__c CODAInvoiceInfo;  
 
public PageReference refresh(){
    Boolean Success = false;
    Boolean Success1 = false;
    
    if (salesInvoiceID ==null) {
        return Null;
    }
    if (ShowGross != Null) {Success1 = true;} 

    if (PageBrokenInvoiceLines != null) {Success = true;}                          
    return Null; 
    } 
 
public string getinvoiceCurrencySymbol(){
    return isoCode;
    }
    
    
// SHOW GROSS OPTION BOOLEAN    
    public boolean getShowGross() {
        ShowGross=false;
        ShowGross1=false;
        ShowGross1 = [SELECT c.Id, c.Show_Gross_Total__c FROM c2g__codaInvoice__c c where c.Id = :salesInvoiceId Limit 1][0].Show_Gross_Total__c;
          if (ShowGross1 == True) {ShowGross = True;} Else {ShowZero = False;}
    Return ShowGross;
    }
    

// SHOW NET OPTION BOOLEAN    
    public boolean getShowNet() {
        ShowNet=True;
        ShowNet1=true;
        ShowNet1 = [SELECT c.Id, c.Show_Gross_Total__c FROM c2g__codaInvoice__c c where c.Id = :salesInvoiceId Limit 1][0].Show_Gross_Total__c;
          if (ShowGross1 == True) {ShowNet = False;} Else {ShowNet = True;}
    Return ShowNet;
    }
    

// GET GROSS AMOUNT    
    public Double getGrossAmount() {
     
          if (ShowGross == true) { GrossAmount = InvoiceTotal;
          GrossAmount1 = [SELECT  c.Gross_Invoice_Amount_Total__c  From c2g__codaInvoice__c c 
                               where c.Id = :salesInvoiceId][0].Gross_Invoice_Amount_Total__c;
          If ((GrossAmount1 == Null || GrossAmount == 0)) {
              GrossAmount = InvoiceTotal;
                               }
              Else { GrossAmount=GrossAmount1;}                              
          }  
    return GrossAmount;
    }

//  COUNTS TOTAL NUMBER OF PRINTED LINES
    public integer getTotalLines(){
           if(lineItems == null && salesInvoiceId != null){
            ShowZero1 = [SELECT c.Id, c.ffdc_ShowZeros__c FROM c2g__codaInvoice__c c where c.Id = :salesInvoiceId Limit 1][0].ffdc_ShowZeros__c;
              if (ShowZero1 == True) {ShowZero = True;} Else {ShowZero = False;}                    
            integer l=0;
            if (ShowZero == true) {lineItems1 =  [Select c.c2g__LineNumber__c, c.c2g__Invoice__c From c2g__codaInvoiceLineItem__c c where c.c2g__Invoice__c = :salesInvoiceId];}
            else {lineItems1 =  [Select c.c2g__LineNumber__c, c.c2g__Invoice__c From c2g__codaInvoiceLineItem__c c WHERE (c.c2g__Invoice__c = :salesInvoiceId AND c.c2g__NetValue__c  <> 0)] ;}
            for (c2g__codaInvoiceLineItem__c lz:  LineItems1){ l++;}
            TotalLines = l;
        }
        return totalLines;
        }
    
//  CALCULATES IF FIRST MORE THAN ONE PAGE
    public Boolean getMoreThanOnePage(){
        boolean tf=true;        
        return tf;        
    }    
              
        
//  CALCULATES TOTAL NUMBER OF PAGES
    public integer getTotalPages(){    
        integer p;
        decimal idec;
        Integer Pages2 = InvoicePage1OfX + InvoiceLastPage;
              
        if (TotalLines <= InvoicePage1Of1){p=1;}
        else if (TotalLines <= Pages2){p=2;}
        else {
                p = integer.ValueOf((decimal.valueOf(TotalLines - InvoicePage1OfX - InvoicelastPage)/InvoicePageXofX).Round(System.RoundingMode.UP))+2;
                //p = integer.ValueOf(2 + idec.ROUND(System.RoundingMode.UP));  //System.RoundingMode.UP
            }
        TotalPages = p;
        return TotalPages;   
    }
    
// CALCULATES LAST LINE NUMBER TO INCLUDE IN LOOP 
    Public integer getLastLoop(){
        integer ll;
        if (TotalPages == 1) {ll = TotalLines;}
        else {ll = (TotalLines - InvoicePage1OfX) - ((TotalPages - 2) * (InvoiceLastPage));}     
       
        if (ll == 0) {LastLoop = (TotalLines - 5);}
        else {if (ll == 1 && TotalPages >= 2){LastLoop = (TotalLines - 6);}
                else LastLoop= TotalLines - ll;
                }                
        return LastLoop;
    }
    
        

//%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    Public List<Integer> getBlankLines(){
        integer i=0;        
        Integer Blanks = 0;
        Integer Blanks1 = 0;
        List<Integer> BlankLinesToAdd = new List<Integer>();
        
        Integer H1 = 0;
        Integer H2 = 0;
        Integer H3 = 0;
        Integer H4 = 0;
        Integer H5 = 0;
        Integer L1 = 0;
        Integer L2 = 0;
        Integer L3 = 0;
        Integer L4 = 0;
        Integer L5 = 0;
        Integer MinusLines;
        Integer MinusLines1;
        
        if ( PrintedText1Heading == '' ) {H1 = 0;} else {H1 = 1;}
        if ( PrintedText2Heading == '' ) {H2 = 0;} else {H2 = 1;}
        if ( PrintedText3Heading == '' ) {H3 = 0;} else {H3 = 1;} 
        if ( PrintedText4Heading == '' ) {H4 = 0;} else {H4 = 1;}
        if ( PrintedText4Heading == '' ) {H5 = 0;} else {H5 = 1;}
        if ( PrintedTExt1Text != Null && PrintedText1text != '') {L1 = integer.valueOf(decimal.valueof(PrintedText1Text.Length()/140).Round(System.RoundingMode.UP));} else {L1 = 0;} 
        if ( PrintedTExt2Text != Null && PrintedText2text != '') {L2 = integer.valueOf(decimal.valueof(PrintedText2Text.Length()/140).Round(System.RoundingMode.UP));} else {L2 = 0;} 
        if ( PrintedTExt3Text != Null && PrintedText3text != '') {L3 = integer.valueOf(decimal.valueof(PrintedText3Text.Length()/140).Round(System.RoundingMode.UP));} else {L3 = 0;} 
        if ( PrintedTExt4Text != Null && PrintedText4text != '') {L4 = integer.valueOf(decimal.valueof(PrintedText4Text.Length()/140).Round(System.RoundingMode.UP));} else {L4 = 0;} 
        if ( PrintedTExt5Text != Null && PrintedText5text != '') {L5 = integer.valueOf(decimal.valueof(PrintedText5Text.Length()/140).Round(System.RoundingMode.UP));} else {L5 = 0;} 
        MinusLines1 = L1 + L2 + L3 + L4 + L5 + H1 + H2 + H3 + H4 + H5 - 3;
        if (MinusLines1 <= 0) {MinusLines = 0;} else { MinusLines = MinusLines1;}
        
        
        if(lineItems == null && salesInvoiceId != null){
            ShowZero1 = [SELECT c.Id, c.ffdc_ShowZeros__c FROM c2g__codaInvoice__c c where c.Id = :salesInvoiceId Limit 1][0].ffdc_ShowZeros__c;
              if (ShowZero1 == True) {ShowZero = True;} Else {ShowZero = False;}                    
            integer l=0;
            if (ShowZero == true) {lineItems1 =  [Select c.c2g__LineNumber__c, c.c2g__Invoice__c From c2g__codaInvoiceLineItem__c c where c.c2g__Invoice__c = :salesInvoiceId];}
            else {lineItems1 =  [Select c.c2g__LineNumber__c, c.c2g__Invoice__c From c2g__codaInvoiceLineItem__c c WHERE (c.c2g__Invoice__c = :salesInvoiceId AND c.c2g__NetValue__c  <> 0)] ;}
            for (c2g__codaInvoiceLineItem__c lz:  LineItems1){ l++;}
            TotalLines = l;
        }
        
                // CALCULATES TOTAL NUMBER OF PAGES        
        integer q;
        decimal qdec;      
        if (TotalLines <= invoicePage1Of1){q=1;}
        else{
            if (TotalLines <= (invoicePage1OfX + invoiceLastPage)) {q=2;}
                else {
                    qdec = decimal.valueOf(TotalLines - invoicePage1OfX - invoiceLastPage)/invoicePageXOfX;
                    q = integer.ValueOf(2 + qdec.ROUND(System.RoundingMode.UP));  //System.RoundingMode.UP
                }
        }
        TotalPages = q; 
        
        if (TotalLines <= invoicePage1Of1){ Blanks = invoicePage1Of1 - TotalLines-1 - MinusLines;}
        else { if ( TotalLines <= (invoicePage1Of1 + invoiceLastPage)) {Blanks = InvoicePage1OfX + InvoiceLastPage - TotalLines-1 - MinusLines;}
               else { Blanks = InvoicePage1OfX + InvoiceLastPage + invoicePageXOfX * (TotalPages - 2) - TotalLines - MinusLines;
               }
        }
        if (Blanks <= 0) {Blanks1 = 0;} else {Blanks1 = Blanks;}
           
            while ( i <= Blanks1 ){
                i++;
                BlankLinesToAdd.add(i);
            }         
            return BlankLinesToAdd;           
    }

//%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

//  GET INVOICE LINE ITEMS AS DETERMINED BY GET GROSS AMOUNT AND SHOW ZERO OPTIONS    
    public List<c2g__codaInvoiceLineItem__c[]> getPageBrokenInvoiceLines1()
    {
        integer i = 1;
        integer j = 1;
        decimal qdec1;
        integer LineNumber;
        string ProductName;
        string LineDescription;
        double NetValue;
        
    // CALCULATES TOTAL NUMBER OF LINE ITEMS       
        if(lineItems == null && salesInvoiceId != null){
            ShowZero1 = [SELECT c.Id, c.ffdc_ShowZeros__c FROM c2g__codaInvoice__c c where c.Id = :salesInvoiceId Limit 1][0].ffdc_ShowZeros__c;
              if (ShowZero1 == True) {ShowZero = True;} Else {ShowZero = False;}                    
            integer l=0;
            if (ShowZero == true) {lineItems1 =  [Select c.c2g__LineNumber__c, c.c2g__Invoice__c From c2g__codaInvoiceLineItem__c c where c.c2g__Invoice__c = :salesInvoiceId];}
            else {lineItems1 =  [Select c.c2g__LineNumber__c, c.c2g__Invoice__c From c2g__codaInvoiceLineItem__c c WHERE (c.c2g__Invoice__c = :salesInvoiceId AND c.c2g__NetValue__c  <> 0)] ;}
            for (c2g__codaInvoiceLineItem__c lz:  LineItems1){ l++;}
            TotalLines = l;
        }

      // CALCULATES TOTAL NUMBER OF PAGES        
        integer p;
        decimal idec;
              
        if (TotalLines <= InvoicePage1Of1){p=1;}
        else{
            if (TotalLines <= (InvoicePage1OfX + InvoiceLastPage)) {p=2;}
            else{
                p = integer.ValueOf((decimal.valueOf(TotalLines - InvoicePage1OfX - InvoicelastPage)/InvoicePageXofX).Round(System.RoundingMode.UP))+2;
                //p = integer.ValueOf(2 + idec.ROUND(System.RoundingMode.UP));  //System.RoundingMode.UP
            }
        }
        TotalPages = p;
  
        
        
    // CALCULATES LAST LINE NUMBER TO INCLUDE IN LOOP 
        integer ll;
        if (TotalPages == 1) {ll = TotalLines;}
        else {
            if (TotalPages == 2) {ll = TotalLines - InvoicePage1OfX;}
            else {ll = (TotalLines - InvoicePage1OfX  - (TotalPages - 2)*InvoicePageXOfX);     
        } 
        }        
        if (ll <= 2 ) {LastLoop = (TotalLines - 2);}
             else 
                   {LastLoop = TotalLines - ll;}
                        

        
        pageBrokenInvoiceLines = New List<c2g__codaInvoiceLineItem__c[]>();  
        c2g__codaInvoiceLineItem__c[] pageOfInvoiceItems = new c2g__codaInvoiceLineItem__c[]{};
        Integer counter = 0;
        boolean firstBreakFound = false;
        boolean setSubSeqBreak = false;
        integer breakpoint = invoicePage1Of1;
        
        if (totalPages == 1){
            breakPoint = invoicePage1Of1;
        }
        else {
            if (ll <=2 && totalPages ==2){
                breakpoint = invoicePage1OfX - 2;}
            else {breakpoint = InvoicePage1OfX;}    
            breakPoint = invoicePage1OfX;
        }    
        
        
        if(lineItems == null && salesInvoiceId != null)
        {
          ShowZero1 = [SELECT c.Id, c.ffdc_ShowZeros__c FROM c2g__codaInvoice__c c where c.Id = :salesInvoiceId Limit 1][0].ffdc_ShowZeros__c;
          if (ShowZero1 == True) {ShowZero = True;} Else {ShowZero = False;}                   

          if (ShowZero == true) {
          lineItems =  [Select c.c2g__UsePartPeriods__c, c.c2g__UnitPrice__c, c.c2g__UnitOfWork__c, c.c2g__TaxValueTotal__c, c.c2g__TaxValue3__c, c.c2g__TaxValue2__c, c.c2g__TaxValue1__c, c.c2g__TaxRateTotal__c, 
                               c.c2g__TaxRate3__c, c.c2g__TaxRate2__c, c.c2g__TaxRate1__c, c.c2g__TaxCode3__c, c.c2g__TaxCode2__c, c.c2g__TaxCode1__r.Name, c.c2g__TaxCode2__r.Name, c.c2g__TaxCode3__r.Name, 
                               c.c2g__TaxCode1__c, c.c2g__StartDate__c, c.c2g__Scheduled__c, c.c2g__Quantity__c, c.c2g__Product__r.ProductCode, c.c2g__Product__r.Name, c.c2g__Product__c, c.c2g__PeriodInterval__c, 
                               c.c2g__OwnerCompany__c, c.c2g__NumberofJournals__c, c.c2g__NetValue__c, c.Gross_Amt__c, c.c2g__LineNumber__c, c.Alt_Line_Number__c, c.Current_Page_No__c, c.c2g__LineDescription__c, c.c2g__Invoice__c, c.c2g__IncomeSchedule__c, c.c2g__IncomeScheduleGroup__c, 
                               c.c2g__GeneralLedgerAccount__c, c.c2g__ExternalId__c, c.c2g__Dimension4__c, c.c2g__Dimension3__c, c.c2g__Dimension2__c, c.c2g__Dimension1__c, c.SystemModstamp, c.Name, c.sProduct_Name__c, c.LastModifiedDate, 
                               c.LastModifiedById, c.IsDeleted, c.Id, /* c.CurrencyIsoCode, */ c.CreatedDate, c.CreatedById 
                               From c2g__codaInvoiceLineItem__c c 
                               where c.c2g__Invoice__c = :salesInvoiceId order by c.c2g__LineNumber__c];
                              }
        else                  {
            lineItems =  [Select c.c2g__UsePartPeriods__c, c.c2g__UnitPrice__c, c.c2g__UnitOfWork__c, c.c2g__TaxValueTotal__c, c.c2g__TaxValue3__c, c.c2g__TaxValue2__c, c.c2g__TaxValue1__c, c.c2g__TaxRateTotal__c, 
                                 c.c2g__TaxRate3__c, c.c2g__TaxRate2__c, c.c2g__TaxRate1__c, c.c2g__TaxCode3__c, c.c2g__TaxCode2__c, c.c2g__TaxCode1__r.Name, c.c2g__TaxCode2__r.Name, c.c2g__TaxCode3__r.Name, 
                                 c.c2g__TaxCode1__c, c.c2g__StartDate__c, c.c2g__Scheduled__c, c.c2g__Quantity__c, c.c2g__Product__r.ProductCode, c.c2g__Product__r.Name, c.c2g__Product__c, c.c2g__PeriodInterval__c, 
                                 c.c2g__OwnerCompany__c, c.c2g__NumberofJournals__c, c.c2g__NetValue__c, c.Gross_Amt__c, c.c2g__LineNumber__c, c.Alt_Line_Number__c, c.Current_Page_No__c, c.c2g__LineDescription__c, c.c2g__Invoice__c, c.c2g__IncomeSchedule__c, c.c2g__IncomeScheduleGroup__c, 
                                 c.c2g__GeneralLedgerAccount__c, c.c2g__ExternalId__c, c.c2g__Dimension4__c, c.c2g__Dimension3__c, c.c2g__Dimension2__c, c.c2g__Dimension1__c, c.SystemModstamp, c.Name, c.sProduct_Name__c, c.LastModifiedDate, 
                                 c.LastModifiedById, c.IsDeleted, c.Id, /* c.CurrencyIsoCode, */ c.CreatedDate, c.CreatedById 
                                 From c2g__codaInvoiceLineItem__c c 
                                 WHERE (c.c2g__Invoice__c = :salesInvoiceId AND c.c2g__NetValue__c  <> 0) order by c.c2g__LineNumber__c];
                                }
        }
      for (c2g__codaInvoiceLineItem__c li:  LineItems) {
            li.Alt_Line_Number__c = i;
            CurrentLine = i;
            i++;
           
            if (TotalPages == 1){}
            else {
            if (i-1 <= LastLoop) {
            
                if (counter <= breakpoint) {
                    pageOfInvoiceItems.add(li);
                    counter++;
                
                    if (counter == breakpoint) {
                        j++;
                        if (!firstBreakFound) {
                            firstBreakFound = true;
                            setSubSeqBreak = true;
                        }
                        if (setSubSeqBreak) {
                            if (j == TotalPages ){breakpoint=invoiceLastPage;}
                            else {
                                breakpoint = invoicePageXOfX;
                                setSubSeqBreak = false;
                            }                           
                        }
                        pageBrokenInvoiceLines.add(pageOfInvoiceItems);
                        pageOfInvoiceItems = new c2g__codaInvoiceLineItem__c[]{};
                        counter = 0;
                }
            }            
          }
          }                                                 
        }    
        if(!pageOfInvoiceItems.isEmpty()){ 
            pageBrokenInvoiceLines.add(pageOfInvoiceItems);
        }            
        return PageBrokenInvoiceLines;          
}


//%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
//  GET INVOICE LINE ITEMS AS DETERMINED BY GET GROSS AMOUNT AND SHOW ZERO OPTIONS    
   public List<c2g__codaInvoiceLineItem__c[]> getPageBrokenInvoiceLinesLast()
   {
        integer i = 1;
        integer LineNumber;
        string ProductName;
        string LineDescription;
        double NetValue;
        
        ShowZero1 = [SELECT c.Id, c.ffdc_ShowZeros__c FROM c2g__codaInvoice__c c where c.Id = :salesInvoiceId Limit 1][0].ffdc_ShowZeros__c;
          if (ShowZero1 == True) {ShowZero = True;} Else {ShowZero = False;} 

        // CALCULATES NUMBER OF LINES                   
        if(lineItems == null && salesInvoiceId != null){
            ShowZero1 = [SELECT c.Id, c.ffdc_ShowZeros__c FROM c2g__codaInvoice__c c where c.Id = :salesInvoiceId Limit 1][0].ffdc_ShowZeros__c;
              if (ShowZero1 == True) {ShowZero = True;} Else {ShowZero = False;}                    
            integer l=0;
            if (ShowZero == true) {lineItems1 =  [Select c.c2g__LineNumber__c, c.c2g__Invoice__c From c2g__codaInvoiceLineItem__c c where c.c2g__Invoice__c = :salesInvoiceId];}
            else {lineItems1 =  [Select c.c2g__LineNumber__c, c.c2g__Invoice__c From c2g__codaInvoiceLineItem__c c WHERE (c.c2g__Invoice__c = :salesInvoiceId AND c.c2g__NetValue__c  <> 0)] ;}
            for (c2g__codaInvoiceLineItem__c lz:  LineItems1){ l++;}
            TotalLines = l;
        }
       
        
    // CALCULATES TOTAL NUMBER OF PAGES        
        integer p;
        decimal idec;
              
        if (TotalLines <= InvoicePage1Of1){p=1;}
        else{
            if (TotalLines <= (InvoicePage1OfX + InvoiceLastPage)) {p=2;}
            else{
                idec = decimal.valueOf(TotalLines - InvoicePage1OfX - InvoicelastPage)/InvoicePageXofX;
                p = integer.ValueOf(2 + idec.ROUND(System.RoundingMode.UP));  //System.RoundingMode.UP
            }
        }
        TotalPages = p;
  
       
        
        
        // CALCULATES LAST LINE NUMBER TO INCLUDE IN LOOP 
        integer ll;
        if (TotalPages == 1) {ll = TotalLines;}
        else {
            if (TotalPages == 2) {ll = TotalLines - InvoicePage1OfX;}
            else {ll = (TotalLines - InvoicePage1OfX  - (TotalPages - 2)*InvoicePageXOfX);
            }
        }         
        if (ll <= 2 ) {LastLoop = (TotalLines - 2);}
             else LastLoop= TotalLines - ll;
                      

        
        pageBrokenInvoiceLinesLast = New List<c2g__codaInvoiceLineItem__c[]>();  
        c2g__codaInvoiceLineItem__c[] pageOfInvoiceItemsLast = new c2g__codaInvoiceLineItem__c[]{};
        Integer counter = 0;
        boolean firstBreakFound = false;
        boolean setSubSeqBreak = false;
        integer breakPoint = InvoicePage1Of1;
        
        
        if(lineItems == null && salesInvoiceId != null)
        {
          ShowZero1 = [SELECT c.Id, c.ffdc_ShowZeros__c FROM c2g__codaInvoice__c c where c.Id = :salesInvoiceId Limit 1][0].ffdc_ShowZeros__c;
          if (ShowZero1 == True) {ShowZero = True;} Else {ShowZero = False;}                   

          if (ShowZero == true) {
          lineItems =  [Select c.c2g__UsePartPeriods__c, c.c2g__UnitPrice__c, c.c2g__UnitOfWork__c, c.c2g__TaxValueTotal__c, c.c2g__TaxValue3__c, c.c2g__TaxValue2__c, c.c2g__TaxValue1__c, c.c2g__TaxRateTotal__c, 
                               c.c2g__TaxRate3__c, c.c2g__TaxRate2__c, c.c2g__TaxRate1__c, c.c2g__TaxCode3__c, c.c2g__TaxCode2__c, c.c2g__TaxCode1__r.Name, c.c2g__TaxCode2__r.Name, c.c2g__TaxCode3__r.Name, 
                               c.c2g__TaxCode1__c, c.c2g__StartDate__c, c.c2g__Scheduled__c, c.c2g__Quantity__c, c.c2g__Product__r.ProductCode, c.c2g__Product__r.Name, c.c2g__Product__c, c.c2g__PeriodInterval__c, 
                               c.c2g__OwnerCompany__c, c.c2g__NumberofJournals__c, c.c2g__NetValue__c,c.Gross_Amt__c,c.c2g__LineNumber__c, c.Alt_Line_Number__c, c.Current_Page_No__c, c.c2g__LineDescription__c, c.c2g__Invoice__c, c.c2g__IncomeSchedule__c, c.c2g__IncomeScheduleGroup__c, 
                               c.c2g__GeneralLedgerAccount__c, c.c2g__ExternalId__c, c.c2g__Dimension4__c, c.c2g__Dimension3__c, c.c2g__Dimension2__c, c.c2g__Dimension1__c, c.SystemModstamp, c.Name, c.sProduct_Name__c, c.LastModifiedDate, 
                               c.LastModifiedById, c.IsDeleted, c.Id, /* c.CurrencyIsoCode, */ c.CreatedDate, c.CreatedById 
                               From c2g__codaInvoiceLineItem__c c 
                               where c.c2g__Invoice__c = :salesInvoiceId order by c.c2g__LineNumber__c];
                              }
        else                  {
            lineItems =  [Select c.c2g__UsePartPeriods__c, c.c2g__UnitPrice__c, c.c2g__UnitOfWork__c, c.c2g__TaxValueTotal__c, c.c2g__TaxValue3__c, c.c2g__TaxValue2__c, c.c2g__TaxValue1__c, c.c2g__TaxRateTotal__c, 
                                 c.c2g__TaxRate3__c, c.c2g__TaxRate2__c, c.c2g__TaxRate1__c, c.c2g__TaxCode3__c, c.c2g__TaxCode2__c, c.c2g__TaxCode1__r.Name, c.c2g__TaxCode2__r.Name, c.c2g__TaxCode3__r.Name, 
                                 c.c2g__TaxCode1__c, c.c2g__StartDate__c, c.c2g__Scheduled__c, c.c2g__Quantity__c, c.c2g__Product__r.ProductCode, c.c2g__Product__r.Name, c.c2g__Product__c, c.c2g__PeriodInterval__c, 
                                 c.c2g__OwnerCompany__c, c.c2g__NumberofJournals__c, c.c2g__NetValue__c,c.Gross_Amt__c,c.c2g__LineNumber__c, c.Alt_Line_Number__c, c.Current_Page_No__c, c.c2g__LineDescription__c, c.c2g__Invoice__c, c.c2g__IncomeSchedule__c, c.c2g__IncomeScheduleGroup__c, 
                                 c.c2g__GeneralLedgerAccount__c, c.c2g__ExternalId__c, c.c2g__Dimension4__c, c.c2g__Dimension3__c, c.c2g__Dimension2__c, c.c2g__Dimension1__c, c.SystemModstamp, c.Name, c.sProduct_Name__c, c.LastModifiedDate, 
                                 c.LastModifiedById, c.IsDeleted, c.Id, /* c.CurrencyIsoCode, */ c.CreatedDate, c.CreatedById 
                                 From c2g__codaInvoiceLineItem__c c 
                                 WHERE (c.c2g__Invoice__c = :salesInvoiceId AND c.c2g__NetValue__c  <> 0) order by c.c2g__LineNumber__c];
                                }
        }
      

   
      for (c2g__codaInvoiceLineItem__c li:  LineItems) {
            li.Alt_Line_Number__c = i;
            CurrentLine = i;
            i++;                       
            if (i-1 > LastLoop) {
                    pageOfInvoiceItemsLast.add(li);
            }
      }
      
      
        PageBrokenInvoiceLinesLast.add(pageOfInvoiceItemsLast);                                                                  
        if(!pageOfInvoiceItems.isEmpty()){ 
            pageBrokenInvoiceLinesLast.add(pageOfInvoiceItemsLast);
        }
                   
        return PageBrokenInvoiceLinesLast;          
    }
    
}