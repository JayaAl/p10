@isTest
private class FF_C_TEST_SalesCreditNoteEmailComContr{
    static PageReference Page;
    static FF_C_SalesCreditNoteEmailComController controller;
    static User JimUser;
    static c2g__CODACreditNote__c TestSalesCreditNoteID;
    List<c2g__CODACreditNote__c> NewCreditNoteRecord = New List<c2g__CODACreditNote__c>();
   
    static ID TestID1; 
    static ID TestID2;
    static ID TestID3;
    static ID TestID4;
    
    static {JimUser = [SELECT ID FROM User WHERE  Profile.Name  = 'System Administrator' AND isActive = true LIMIT 1];
            TestID1  = [SELECT ID FROM c2g__CODACreditNote__c WHERE Lines__c > 0 LIMIT 1][0].ID;           
                init1();}
                
    static {JimUser = [SELECT ID FROM User WHERE Profile.Name = 'System Administrator' AND isActive = true  LIMIT 1];
            TestID2 = [SELECT ID FROM c2g__CODACreditNote__c WHERE Lines__c > 0  LIMIT 1][0].ID;           
                init2();}            

    static {JimUser = [SELECT ID FROM User WHERE Profile.Name = 'System Administrator' AND isActive = true LIMIT 1];
            TestID3  = [SELECT ID FROM c2g__CODACreditNote__c WHERE Lines__c > 0 LIMIT 1][0].ID;           
                init3();}            
                   
    static {JimUser = [SELECT ID FROM User WHERE Profile.Name = 'System Administrator' AND isActive = true   LIMIT 1];
             TestID4  = [SELECT ID FROM c2g__CODACreditNOte__c WHERE Lines__c > 0 LIMIT 1][0].ID;           
                init4();}            
                   
  
    
private static void init1() { 
    controller = new FF_C_SalesCreditNoteEmailComController(); 
    ID testID = [SELECT ID FROM c2g__CODACreditNote__c LIMIT 1][0].ID;   
    controller.salesCreditNoteID = TestID1;        
    controller.invoicePage1Of1 = 8;
    controller.invoicePage1OfX = 12;
    controller.invoicePageXOfX = 25;
    controller.invoiceLastPage = 20;
    controller.ShowZero = true;   
    TestSalesCreditNoteID = New c2g__CODACreditNote__c(ID = TestID1);
}

private static void init2() { 
    controller = new FF_C_SalesCreditNoteEmailComController(); 
    ID testID = [SELECT ID FROM c2g__CODACreditNote__c LIMIT 1][0].ID;   
    controller.salesCreditNoteID = TestID2;        
    controller.invoicePage1Of1 = 8;
    controller.invoicePage1OfX = 12;
    controller.invoicePageXOfX = 25;
    controller.invoiceLastPage = 20;   
    TestSalesCreditNoteID = New c2g__CODACreditNote__c(ID = TestID2);
}

private static void init3() { 
    controller = new FF_C_SalesCreditNoteEmailComController(); 
    ID testID = [SELECT ID FROM c2g__CODACreditNote__c LIMIT 1][0].ID;   
    controller.salesCreditNoteID = TestID3;        
    controller.invoicePage1Of1 = 8;
    controller.invoicePage1OfX = 12;
    controller.invoicePageXOfX = 25;
    controller.invoiceLastPage = 20;   
    TestSalesCreditNoteID = New c2g__CODACreditNote__c(ID = TestID3);
}

private static void init4() { 
    controller = new FF_C_SalesCreditNoteEmailComController(); 
    ID testID = [SELECT ID FROM c2g__CODACreditNote__c LIMIT 1][0].ID;   
    controller.salesCreditNoteID = TestID4;        
    controller.invoicePage1Of1 = 8;
    controller.invoicePage1OfX = 12;
    controller.invoicePageXOfX = 25;
    controller.invoiceLastPage = 20;   
    TestSalesCreditNoteID = New c2g__CODACreditNote__c(ID = TestID4);
}

Static testMethod void testAsUser1() {
    System.runAs(JimUser) {
        init1();          
        controller.invoicePage1Of1 = 8;
        controller.invoicePage1OfX = 12;
        controller.invoicePageXOfX = 25;
        controller.invoiceLastPage = 20;
        controller.salesInvoiceID = TestID1;
        controller.Refresh();
        controller.getBlankLines();
        controller.getGrossAmount();
        controller.getNegativeNetAmount();
        controller.getNegativeGrossAmount();
        controller.getPageBrokenCreditNoteLines1();
        controller.getShowGross();
        controller.getShowNet();
        controller.getTotalLines();
        controller.getMoreThanOnePage();
        controller.getTotalPages();
        controller.getLastLoop();
        controller.getPageBrokenCreditNoteLinesLast();
        system.assert(1==1); 
    }
}

Static testMethod void testAsUser2() {
    System.runAs(JimUser) {
        init1();      
    controller.invoicePage1Of1 = 8;
    controller.invoicePage1OfX = 12;
    controller.invoicePageXOfX = 25;
    controller.invoiceLastPage = 20;
    controller.salesInvoiceID = TestID2;
    controller.Refresh();
    controller.getBlankLines();
    controller.getPageBrokenCreditNoteLines1();
    controller.getShowGross();
    controller.getShowNet();
    controller.getGrossAmount();
    controller.getNegativeNetAmount();
    controller.getNegativeGrossAmount();
    controller.getTotalLines();
    controller.getMoreThanOnePage();
    controller.getTotalPages();
    controller.getLastLoop();
    controller.getPageBrokenCreditNoteLinesLast();
        system.assert(1==1); 
    }

}

Static testMethod void testAsUser3() {
    System.runAs(JimUser) {
        init1();      
        controller.invoicePage1Of1 = 8;
        controller.invoicePage1OfX = 12;
        controller.invoicePageXOfX = 25;
        controller.invoiceLastPage = 20;
        controller.salesInvoiceID = TestID3;
        controller.Refresh();
        controller.getBlankLines();
        controller.getGrossAmount();
        controller.getPageBrokenCreditNoteLines1();
        controller.getNegativeNetAmount();
        controller.getNegativeGrossAmount();
        controller.getShowGross();
        controller.getShowNet();
        controller.getTotalLines();
        controller.getMoreThanOnePage();
        controller.getTotalPages();
        controller.getLastLoop();
        controller.getPageBrokenCreditNoteLinesLast();
        system.assert(1==1); 
    }
}    
Static testMethod void testAsUser4() {
    System.runAs(JimUser) {
        init1();      
        controller.invoicePage1Of1 = 8;
        controller.invoicePage1OfX = 12;
        controller.invoicePageXOfX = 25;
        controller.invoiceLastPage = 20;
        controller.salesInvoiceID = TestID4;
        controller.Refresh();
        controller.getBlankLines();
        controller.getGrossAmount();
        controller.getPageBrokenCreditNoteLines1();
        controller.getNegativeNetAmount();
        controller.getNegativeGrossAmount();
        controller.getShowGross();
        controller.getShowNet();
        controller.getTotalLines();
        controller.getMoreThanOnePage();
        controller.getTotalPages();
        controller.getLastLoop();
        controller.getPageBrokenCreditNoteLinesLast();
        system.assert(1==1); 
    }    
}
    

}