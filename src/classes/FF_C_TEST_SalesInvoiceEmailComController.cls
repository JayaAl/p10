@isTest
private class FF_C_TEST_SalesInvoiceEmailComController{
    static PageReference Page;
    static FF_C_SalesInvoiceEmailComController controller;
    static User JimUser;
    static c2g__CODAInvoice__c TestSalesInvoiceID;
    List<c2g__CODAInvoice__c> NewInvoiceRecord = New List<c2g__CODAInvoice__c>();
   
    static ID TestID1; 
    static ID TestID2;
    static ID TestID3;
    static ID TestID4;
    
    static {JimUser = [SELECT ID FROM User WHERE  Profile.Name  = 'System Administrator' AND isActive = true  LIMIT 1];
            TestID1  = [SELECT ID FROM c2g__CODAInvoice__c WHERE ffdc_ShowZeros__c = false AND Show_Gross_Total__c = false LIMIT 1][0].ID;           
                init1();}
                
    static {JimUser = [SELECT ID FROM User WHERE Profile.Name = 'System Administrator' AND isActive = true LIMIT 1];
            TestID2 = [SELECT ID FROM c2g__CODAInvoice__c WHERE ffdc_ShowZeros__c = true AND Show_Gross_Total__c = false LIMIT 1][0].ID;           
                init2();}            

    static {JimUser = [SELECT ID FROM User WHERE Profile.Name = 'System Administrator' AND isActive = true  LIMIT 1];
            TestID3  = [SELECT ID FROM c2g__CODAInvoice__c WHERE ffdc_ShowZeros__c = false AND Show_Gross_Total__c = true LIMIT 1][0].ID;           
                init3();}            
                   
    static {JimUser = [SELECT ID FROM User WHERE Profile.Name = 'System Administrator' AND isActive = true LIMIT 1];
             TestID4  = [SELECT ID FROM c2g__CODAInvoice__c WHERE ffdc_ShowZeros__c = true AND Show_Gross_Total__c = true LIMIT 1][0].ID;           
                init4();}            
                   
  
    
private static void init1() { 
    controller = new FF_C_SalesInvoiceEmailComController(); 
    ID testID = [SELECT ID FROM c2g__CODAInvoice__c LIMIT 1][0].ID;   
    controller.salesInvoiceID = TestID1;        
    controller.invoicePage1Of1 = 8;
    controller.invoicePage1OfX = 12;
    controller.invoicePageXOfX = 25;
    controller.invoiceLastPage = 20;   
    TestSalesInvoiceID = New c2g__CODAInvoice__c(ID = TestID1);
}

private static void init2() { 
    controller = new FF_C_SalesInvoiceEmailComController(); 
    ID testID = [SELECT ID FROM c2g__CODAInvoice__c LIMIT 1][0].ID;   
    controller.salesInvoiceID = TestID2;        
    controller.invoicePage1Of1 = 8;
    controller.invoicePage1OfX = 12;
    controller.invoicePageXOfX = 25;
    controller.invoiceLastPage = 20;   
    TestSalesInvoiceID = New c2g__CODAInvoice__c(ID = TestID2);
}

private static void init3() { 
    controller = new FF_C_SalesInvoiceEmailComController(); 
    ID testID = [SELECT ID FROM c2g__CODAInvoice__c LIMIT 1][0].ID;   
    controller.salesInvoiceID = TestID3;        
    controller.invoicePage1Of1 = 8;
    controller.invoicePage1OfX = 12;
    controller.invoicePageXOfX = 25;
    controller.invoiceLastPage = 20;   
    TestSalesInvoiceID = New c2g__CODAInvoice__c(ID = TestID3);
}

private static void init4() { 
    controller = new FF_C_SalesInvoiceEmailComController(); 
    ID testID = [SELECT ID FROM c2g__CODAInvoice__c LIMIT 1][0].ID;   
    controller.salesInvoiceID = TestID4;        
    controller.invoicePage1Of1 = 8;
    controller.invoicePage1OfX = 12;
    controller.invoicePageXOfX = 25;
    controller.invoiceLastPage = 20;   
    TestSalesInvoiceID = New c2g__CODAInvoice__c(ID = TestID4);
}

Static testMethod void testAsUser1() {
    System.runAs(JimUser) {
        init1();      
    controller.invoicePage1Of1 = 8;
    controller.invoicePage1OfX = 12;
    controller.invoicePageXOfX = 25;
    controller.invoiceLastPage = 20;
   controller.salesInvoiceID = TestID1;
        controller.Refresh();
        controller.getBlankLines();
        controller.getPageBrokenInvoiceLines1();
        controller.getShowGross();
        controller.getShowNet();
        controller.getGrossAmount();
        controller.getTotalLines();
        controller.getMoreThanOnePage();
        controller.getTotalPages();
        controller.getLastLoop();
        controller.getPageBrokenInvoiceLinesLast();
        system.assert(1==1); 
    }
}

Static testMethod void testAsUser2() {
    System.runAs(JimUser) {
        init1();      
    controller.invoicePage1Of1 = 8;
    controller.invoicePage1OfX = 12;
    controller.invoicePageXOfX = 25;
    controller.invoiceLastPage = 20;
   controller.salesInvoiceID = TestID2;
        controller.Refresh();
        controller.getBlankLines();
        controller.getPageBrokenInvoiceLines1();
        controller.getShowGross();
        controller.getShowNet();
        controller.getGrossAmount();
        controller.getTotalLines();
        controller.getMoreThanOnePage();
        controller.getTotalPages();
        controller.getLastLoop();
        controller.getPageBrokenInvoiceLinesLast();
        system.assert(1==1); 
    }

}

Static testMethod void testAsUser3() {
    System.runAs(JimUser) {
        init1();      
    controller.invoicePage1Of1 = 8;
    controller.invoicePage1OfX = 12;
    controller.invoicePageXOfX = 25;
    controller.invoiceLastPage = 20;
   controller.salesInvoiceID = TestID3;
        controller.Refresh();
        controller.getBlankLines();
        controller.getPageBrokenInvoiceLines1();
        controller.getShowGross();
        controller.getShowNet();
        controller.getGrossAmount();
        controller.getTotalLines();
        controller.getMoreThanOnePage();
        controller.getTotalPages();
        controller.getLastLoop();
        controller.getPageBrokenInvoiceLinesLast();
        system.assert(1==1); 
    }
}    
Static testMethod void testAsUser4() {
    System.runAs(JimUser) {
        init1();      
    controller.invoicePage1Of1 = 8;
    controller.invoicePage1OfX = 12;
    controller.invoicePageXOfX = 25;
    controller.invoiceLastPage = 20;
   controller.salesInvoiceID = TestID4;
        controller.Refresh();
        controller.getBlankLines();
        controller.getPageBrokenInvoiceLines1();
        controller.getShowGross();
        controller.getShowNet();
        controller.getGrossAmount();
        controller.getTotalLines();
        controller.getMoreThanOnePage();
        controller.getTotalPages();
        controller.getLastLoop();
        controller.getPageBrokenInvoiceLinesLast();
        system.assert(1==1); 
    }    
}
	

}