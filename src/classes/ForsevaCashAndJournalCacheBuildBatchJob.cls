/**
* Copyright 2013 Forseva, LLC.  All rights reserved.
*/

global class ForsevaCashAndJournalCacheBuildBatchJob implements Database.Batchable<SObject>, Schedulable {
    
    // Global
    global ForsevaCashAndJournalCacheBuildBatchJob() {}
    
    global void execute(SchedulableContext sc) {
        Database.executeBatch(this, 200);
    }
    
    global Database.QueryLocator start(Database.BatchableContext bc) {
        Database.QueryLocator ql;
        try {
            ql = Database.getQueryLocator(getQuery());
        }
        catch (Exception e) {
            ForsevaUtilities.decodeExceptionAndSendEmail(e, 'Exception in ForsevaCashAndJournalCacheBuildBatchJob.start()', '', 'Forseva Administrators');
            throw e;
        }
        return ql;
    }
    
    global void execute(Database.BatchableContext bc, List<SObject> scope) {
    }

    global void finish(Database.BatchableContext bc) {
        AsyncApexJob job = [select Id, Status, NumberOfErrors, JobItemsProcessed, TotalJobItems, CreatedBy.Email
                            from   AsyncApexJob where Id = :bc.getJobId()];
        //ForsevaUtilities.sendEmailToGroup('Forseva Cash and Journal Cache Build Batch Job notification.', 
        //                                  'The batch Apex job processed ' + job.TotalJobItems + ' batches with ' + job.NumberOfErrors + ' failures.', 
        //                                  'Forseva Administrators', job.CreatedBy.Email);
        Database.executeBatch(new ForsevaCashAndJournalExtractBatchJob(), 200);
    }
    
    // Public
    public String getQuery() {
        
        // RP - 4/1/14: replace CurrencyIsoCode with c2g__DocumentCurrency__r.Name
        //String query = 'select Id, Name, c2g__Transaction__r.c2g__TransactionDate__c, DocNumComb__c, c2g__OwnerCompany__c, ' +
        String query = 'select Id, Name, c2g__DocumentCurrency__r.Name, c2g__Transaction__r.c2g__TransactionDate__c, DocNumComb__c, c2g__OwnerCompany__c, ' +
                       '       c2g__Transaction__r.c2g__TransactionType__c, c2g__DueDate__c, c2g__DocumentValue__c, ' + 
                       '       c2g__DocumentOutstandingValue__c, c2g__Account__c, c2g__Transaction__r.c2g__Period__r.Name, ' +
                       '       c2g__Account__r.c2g__CODACreditManager__c, Aged_Category__c, c2g__LineType__c, ' + 
                       '       c2g__GeneralLedgerAccount__r.Name, c2g__Transaction__r.LastModifiedDate, CurrencyIsoCode ' +
                       'from   c2g__codaTransactionLineItem__c ' +
                       'where  c2g__Transaction__r.c2g__TransactionType__c in (\'Journal\', \'Cash\') ' +
                       'and    c2g__LineType__c = \'Account\' ' +
                       'and    c2g__GeneralLedgerAccount__r.Name in (\'Accounts Receivable\', \'Consolidation\')';

        if (System.Test.isRunningTest()) {
            query += ' limit 1';
        }
        return query;
    }
    
    // Private
}