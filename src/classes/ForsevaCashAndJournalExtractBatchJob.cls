/**
* Copyright 2013 Forseva, LLC.  All rights reserved.
*/

global class ForsevaCashAndJournalExtractBatchJob implements Database.Batchable<SObject>, Schedulable {
    
    // Global
    global ForsevaCashAndJournalExtractBatchJob () {}
    
    global void execute(SchedulableContext sc) {
        Database.executeBatch(this, 200);
    }
    
    global Database.QueryLocator start(Database.BatchableContext bc) {
        Database.QueryLocator ql;
        try {
            ql = Database.getQueryLocator(getQuery());
        }
        catch (Exception e) {
            ForsevaUtilities.decodeExceptionAndSendEmail(e, 'Exception in ForsevaCashAndJournalExtractBatchJob.start()', '', 'Forseva Administrators');
            throw e;
        }
        return ql;
    }
    
    global void execute(Database.BatchableContext bc, List<SObject> scope) {
        Decimal paidAmount;
        Account acc;
        Map<Id, Account> accMap = new Map<Id, Account>();
        c2g__codaTransactionLineItem__c ffTli;
        forseva1__FInvoice__c inv;
        List<forseva1__FInvoice__c> invs = new List<forseva1__FInvoice__c>();
        String uploadId = String.valueOf(System.today());
        Integer uploadBatchId = Integer.valueOf(uploadId.replaceAll('-', '')); 
        Date invRptDate = System.today();
        
        //Boolean testemailsent = false;
        
        try {
            for (SObject sobj : scope) {
                ffTli = (c2g__codaTransactionLineItem__c)sobj;
                paidAmount = ffTli.c2g__DocumentValue__c - ffTli.c2g__DocumentOutstandingValue__c;
                // RP - 4/1/14: replace CurrencyIsoCode with c2g__DocumentCurrency__r.Name
                //inv = new forseva1__FInvoice__c(forseva1__Invoice_Number__c = ffTli.Name, CurrencyIsoCode = ffTli.CurrencyIsoCode,
                inv = new forseva1__FInvoice__c(forseva1__Invoice_Number__c = ffTli.Name, CurrencyIsoCode = ffTli.c2g__DocumentCurrency__r.Name,
                                                forseva1__Account__c = ffTli.c2g__Account__c, Transaction_Line_Item__c = ffTli.Id,
                                                forseva1__Invoice_Date__c = ffTli.c2g__Transaction__r.c2g__TransactionDate__c, 
                                                forseva1__Invoice_Due_Date__c = ffTli.c2g__DueDate__c, forseva1__Invoice_Report_Date__c = invRptDate, 
                                                forseva1__Invoice_Total__c = ffTli.c2g__DocumentValue__c, forseva1__Paid_Amount__c = paidAmount,
                                                forseva1__Upload_Batch_ID__c = uploadBatchId, Company__c = ffTli.c2g__OwnerCompany__c,
                                                Transaction_Type__c = ffTli.c2g__Transaction__r.c2g__TransactionType__c, 
                                                Billing_Period__c = ffTli.c2g__Transaction__r.c2g__Period__r.Name,
                                                OutstandingValue__c = ffTli.c2g__DocumentOutstandingValue__c);
                invs.add(inv);
                if (inv.forseva1__Account__c != null) {
                    acc = accMap.get(inv.forseva1__Account__c);
                }
                if (acc == null) {
                    acc = new Account(Id = inv.forseva1__Account__c, forseva1__Collector__c = ffTli.c2g__Account__r.c2g__CODACreditManager__c);
                    accMap.put(acc.Id, acc);
                }
            }
            upsert invs forseva1__Invoice_Number__c;
            update accMap.values();
        }
        catch (Exception e) {
            String listOfIds = '';
            for (forseva1__FInvoice__c finv : invs) {
                listOfIds += finv.Transaction_Line_Item__c + '\n';
            }
            ForsevaUtilities.decodeExceptionAndSendEmail(e, 'Error in ForsevaCashAndJournalExtractBatchJob()', listOfIds, 'Forseva Administrators');
            throw e;
       }
    }

    global void finish(Database.BatchableContext bc) {
        AsyncApexJob job = [select Id, Status, NumberOfErrors, JobItemsProcessed, TotalJobItems, CreatedBy.Email
                            from   AsyncApexJob where Id = :bc.getJobId()];
        ForsevaUtilities.sendEmailToGroup('Forseva Cash and Journal Extract Batch Job notification.', 
                                          'The batch Apex job processed ' + job.TotalJobItems + ' batches with ' + job.NumberOfErrors + ' failures.', 
                                          'Forseva Administrators', job.CreatedBy.Email);
    }
    
    // Public
    public String getQuery() {
        
        // RP - 4/1/14: replace CurrencyIsoCode with c2g__DocumentCurrency__r.Name
        //String query = 'select Id, Name, c2g__Transaction__r.c2g__TransactionDate__c, DocNumComb__c, c2g__OwnerCompany__c, ' +
        String query = 'select Id, Name, c2g__DocumentCurrency__r.Name, c2g__Transaction__r.c2g__TransactionDate__c, DocNumComb__c, c2g__OwnerCompany__c, ' +
                       '       c2g__Transaction__r.c2g__TransactionType__c, c2g__DueDate__c, c2g__DocumentValue__c, ' + 
                       '       c2g__DocumentOutstandingValue__c, c2g__Account__c, c2g__Transaction__r.c2g__Period__r.Name, ' +
                       '       c2g__Account__r.c2g__CODACreditManager__c, CurrencyIsoCode, c2g__Transaction__r.LastModifiedDate ' +
                       'from   c2g__codaTransactionLineItem__c ' +
                       'where  c2g__Transaction__r.c2g__TransactionType__c in (\'Journal\', \'Cash\') ' +
                       'and    c2g__LineType__c = \'Account\' ' +
                       'and    c2g__GeneralLedgerAccount__r.Name in (\'Accounts Receivable\', \'Consolidation\') ' +
                       'and    (c2g__DocumentOutstandingValue__c != 0 ' +
                       'or     (Aged_Category__c = \'Matched\' ' +
                       'and     c2g__DocumentOutstandingValue__c = 0 ' +
                       //'and     c2g__Transaction__r.LastModifiedDate >= last_n_days:1))';
                       'and     c2g__Transaction__r.LastModifiedDate >= ' + Label.FinForce_to_BC_Last_N_Days + '))';
        if (System.Test.isRunningTest()) {
            query += ' limit 1';
        }
        return query;
    }
    
    // Private
}