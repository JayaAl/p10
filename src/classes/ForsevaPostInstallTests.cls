/**
* Copyright 2013 Forseva, LLC.  All rights reserved.
*/

@isTest (SeeAllData=true)
private class ForsevaPostInstallTests {

    static testMethod void testForsevaSalesInvoiceExtractBatchJob() {
        Test.startTest();

        ForsevaSalesInvoiceExtractBatchJob batch = new ForsevaSalesInvoiceExtractBatchJob();
        Database.executeBatch(batch, 1);
        String query = batch.getQuery();
        List<SObject> scope = Database.query(query);
        System.assertEquals(1, scope.size(), 'testForsevaSalesInvoiceExtractBatchJob() failed');
        c2g__codaInvoice__c ffInv = (c2g__codaInvoice__c)scope.get(0);
        System.assert(ffInv.c2g__InvoiceDate__c != null, 'testForsevaSalesInvoiceExtractBatchJob() failed');
        
        Test.stopTest();
    }

    static testMethod void testCreditVerificationInForseva() {
        
        Test.startTest();
        
        String corteraLinkId = 'A1B2C3D4E5';
        Account acc = UTIL_TestUtil.generateAccount();
        acc.Name = 'Testing Forseva Invoice123';
        acc.forseva1__Cortera_Link_Id__c = corteraLinkId;
        insert acc;
        /*
        Account acc = new Account(Name = 'Test Credit Account', BillingStreet = '123 Main St', BillingCity = 'Chicago', BillingState = 'IL',
                                  BillingPostalCode = '60603', forseva1__Cortera_Link_Id__c = corteraLinkId);
        insert acc;
        */
        String cvId = '';
        forseva1__CorteraCPR__c cr = new forseva1__CorteraCPR__c(forseva1__Account__c = acc.Id, forseva1__BasicDataLinkId__c = corteraLinkId, 
                                                                 forseva1__CprIndexRating__c = 646, forseva1__CprIndexSegment__c = '5', 
                                                                 CorporateFinancialSummaryNetWorth__c = 100000, CorporateFinancialSummaryLiabilities__c = 200000);
        insert cr;
        forseva1__CorteraCPR__c cr2 = new forseva1__CorteraCPR__c(forseva1__Account__c = acc.Id, forseva1__BasicDataLinkId__c = '1122334455', 
                                                                 forseva1__CprIndexRating__c = 646, forseva1__CprIndexSegment__c = '5');
        insert cr2;

        cr.forseva1__F_Credit_Quality_Rating__c = 9;
        cr.forseva1__F_Credit_Limit_Approved__c = 50000;
        cr.forseva1__F_Credit_Review_Status__c = 'Passed';
        update cr;
        String result = creditVerificationInitiation.applyForCreditInForseva(acc.Id, cvId);
        System.assert(result != null, 'testCreditVerificationInForseva() failed');

        Account acc2 = new Account(Name = 'Test Credit Account 2', BillingStreet = '123 Main St', BillingCity = 'Chicago', BillingState = 'IL',
                                   BillingPostalCode = '60603');
        insert acc2;
        result = creditVerificationInitiation.applyForCreditInForseva(acc2.Id, cvId);
        System.assert(result != null, 'testCreditVerificationInForseva() failed');
        
        Test.stopTest();
    }
    
    static testMethod void testForsevaTriggers() {
        Test.startTest();

        // Test Collections Case Update trigger.
        Account acc = new Account(Name = 'Test Collections Account', BillingStreet = '123 Main St', BillingCity = 'Chicago', BillingState = 'IL',
                                  BillingPostalCode = '60603');
        insert acc;
        forseva1__CollectionsCase__c cc = new forseva1__CollectionsCase__c(forseva1__Account__c = acc.Id, forseva1__CaseStatus2__c = 'Open',
                                                                           forseva1__PromiseToPayAmount__c = 1000);
        insert cc;
        try {
            cc.forseva1__PromiseToPayAmount__c = 5000;
            update cc;
        }
        catch (Exception e) {
        }
        cc = [select Id, Name, forseva1__PromiseToPayAmount__c 
              from   forseva1__CollectionsCase__c 
              where Id = :cc.Id];
        System.assertEquals(1000, cc.forseva1__PromiseToPayAmount__c, 'testForsevaTriggers failed');

        try {
            cc.forseva1__PromiseToPayAmount__c = 5000;
            cc.forseva1__ExpectedPaymentDate__c = Date.today().addDays(10);
            update cc;
        }
        catch (Exception e) {
        }

        try {
            cc.forseva1__PromiseToPayAmount__c = 5000;
            cc.forseva1__Follow_Up_Date__c = Date.today().addDays(5);
            cc.forseva1__ExpectedPaymentDate__c = null;
            update cc;
        }
        catch (Exception e) {
        }

        cc.forseva1__PromiseToPayAmount__c = 5000;
        cc.forseva1__Follow_Up_Date__c = Date.today().addDays(5);
        cc.forseva1__ExpectedPaymentDate__c = Date.today().addDays(10);
        update cc;
        cc = [select Id, Name, forseva1__PromiseToPayAmount__c 
              from   forseva1__CollectionsCase__c 
              where Id = :cc.Id];
        System.assertEquals(5000, cc.forseva1__PromiseToPayAmount__c, 'testForsevaTriggers failed');
        
        // Test Contact upsert trigger.
        Contact cntct = new Contact(AccountId = acc.Id, FirstName = 'Test', LastName = 'Contact', Role__c = 'Billing Contact');
        insert cntct;
        integer cnt = [select count() 
                       from   AccountContactRole 
                       where  AccountId = :acc.Id 
                       and    ContactId = :cntct.Id];
        System.assertEquals(1, cnt, 'testForsevaTriggers failed');
        
        cntct.Phone = '312-312-1000';
        update cntct;
        
        cntct.Role__c = null;
        update cntct;

        cnt = [select count() 
               from   AccountContactRole 
               where  AccountId = :acc.Id 
               and    ContactId = :cntct.Id];
        System.assertEquals(0, cnt, 'testForsevaTriggers failed');
        
        Test.stopTest();
    }

    static testMethod void testOpportunityUpdateTrigger() {
        Test.startTest();

        List<AggregateResult> ffInvs = [select Advertiser__c, sum(c2g__OutstandingValue__c)
                                        from   c2g__codaInvoice__c
                                        where  c2g__OutstandingValue__c <> 0
                                        and    c2g__InvoiceStatus__c = 'Complete'
                                        group by Advertiser__c 
                                        limit 1];
        
        AggregateResult ar = ffInvs.get(0);
        Id accId = (Id)ar.get('Advertiser__c');
        Double totalAR = (Double)ar.get('expr0');
          
        Account acc = [select Id, Name, Open_Balance__c, Pending_Approval__c, Available_Credit__c
                       from   Account
                       where  Id = :accId];
        Opportunity opp = new Opportunity(AccountId = acc.Id, Name = 'Test Opportunity', Amount = 10000, StageName = 'In Negotiation', Probability = 50,
                                          CloseDate = Date.today().addDays(15));
        insert opp;
        
        opp.StageName = 'Verbal Agreement';
        opp.Probability = 75;
        opp.Bill_on_Broadcast_Calendar2__c = 'Yes';
        update opp;
        
        Account accMod = [select Id, Name, Open_Balance__c, Pending_Approval__c, Available_Credit__c
                          from   Account
                          where  Id = :accId];
        acc.Pending_Approval__c = acc.Pending_Approval__c == null ? 0 : acc.Pending_Approval__c;
        System.assert(acc.Pending_Approval__c != null, 'testOpportunityUpdateTrigger failed');
        
        Test.stopTest();
    }
    
    static testMethod void testForsevaUtilities() {
        Test.startTest();

        try {
            ForsevaSalesInvoiceExtractBatchJob batch = null;
            Database.executeBatch(batch, 1);
        }
        catch (Exception e) {
            ForsevaUtilities.decodeExceptionAndSendEmail(e, 'Error executing Sales Invoice Extract batch job', '', 'Forseva Administrators');
        }
        
        Account acc = [select Id, Name from Account where Id != null limit 1];
        Account acc2 = (Account)ForsevaUtilities.getCompleteSObject(Account.SObjectType, acc.Id);
        System.assertEquals(acc.Name, acc2.Name, 'testForsevaUtilities() failed');
        
        Test.stopTest();
    }
    
    static testMethod void testPendingApprovalAmountBatchJob() {
        Test.startTest();

        PendingApprovalAmountBatchJob batch = new PendingApprovalAmountBatchJob();
        Database.executeBatch(batch, 1);
        String query = batch.getQuery();
        List<SObject> scope = Database.query(query);
        System.assertEquals(1, scope.size(), 'testPendingApprovalAmountBatchJob() failed');
        query = query.replace('limit 1', 'limit 75');
        scope = Database.query(query);
        batch.calculatePendingApprovalAmount(scope);
        Opportunity opp = (Opportunity)scope.get(0);
        System.assert(opp.ContractStartDate__c != null, 'testPendingApprovalAmountBatchJob() failed');
        
        Test.stopTest();    	
    }

}