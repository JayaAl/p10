/**
* Copyright 2013 Forseva, LLC.  All rights reserved.
*/

global class ForsevaSalesCreditNoteExtractBatchJob implements Database.Batchable<SObject>, Schedulable {
    
    // Global
    global ForsevaSalesCreditNoteExtractBatchJob() {}
    
    global void execute(SchedulableContext sc) {
        Database.executeBatch(this, 200);
    }
    
    global Database.QueryLocator start(Database.BatchableContext bc) {
        Database.QueryLocator ql;
        try {
            ql = Database.getQueryLocator(getQuery());
        }
        catch (Exception e) {
            ForsevaUtilities.decodeExceptionAndSendEmail(e, 'Exception in ForsevaSalesCreditNoteExtractBatchJob.start()', '', 'Forseva Administrators');
            throw e;
        }
        return ql;
    }
    
    global void execute(Database.BatchableContext bc, List<SObject> scope) {
        Decimal paidAmount;
        Account acc;
        Map<Id, Account> accMap = new Map<Id, Account>();
        c2g__codaCreditNote__c cnInv;
        forseva1__FInvoice__c inv;
        List<forseva1__FInvoice__c> invs = new List<forseva1__FInvoice__c>();
        String uploadId = String.valueOf(System.today());
        Integer uploadBatchId = Integer.valueOf(uploadId.replaceAll('-', '')); 
        Date invRptDate = System.today();
        try {
            for (SObject sobj : scope) {
                cnInv = (c2g__codaCreditNote__c)sobj;
                paidAmount = cnInv.c2g__CreditNoteTotal__c - cnInv.c2g__OutstandingValue__c;
                if (cnInv.c2g__Account__c != null) {
                    // RP - 4/1/14: replace CurrencyIsoCode with c2g__CreditNoteCurrency__r.Name
                    //inv = new forseva1__FInvoice__c(forseva1__Invoice_Number__c = cnInv.Name, CurrencyIsoCode = cnInv.CurrencyIsoCode,
                    inv = new forseva1__FInvoice__c(forseva1__Invoice_Number__c = cnInv.Name, CurrencyIsoCode = cnInv.c2g__CreditNoteCurrency__r.Name,
                                                    forseva1__Account__c = cnInv.c2g__Account__c, forseva1__Invoice_Date__c = cnInv.c2g__InvoiceDate__c, 
                                                    forseva1__Invoice_Due_Date__c = cnInv.c2g__DueDate__c, forseva1__Invoice_Report_Date__c = invRptDate, 
                                                    forseva1__Invoice_Total__c = cnInv.c2g__CreditNoteTotal__c, forseva1__Paid_Amount__c = paidAmount,
                                                    forseva1__Upload_Batch_ID__c = uploadBatchId, Transaction_Type__c = 'Sales Credit Note', 
                                                    Company__c = cnInv.c2g__OwnerCompany__c, Advertiser__c = cnInv.c2g__Invoice__r.Advertiser__c, 
                                                    Opportunity__c = cnInv.c2g__Opportunity__c, Billing_Terms__c = cnInv.c2g__Invoice__r.Billing_Terms__c, 
                                                    Operative_Order_ID__c = cnInv.c2g__Invoice__r.Operative_Order_ID__c, Billing_Period__c = cnInv.Billing_Period__c,
                                                    OutstandingValue__c = cnInv.c2g__OutstandingValue__c, Sales_Credit_Note__c = cnInv.Id, 
                                                    X1st_Salesperson__c = cnInv.c2g__Opportunity__r.X1st_Salesperson__c, 
                                                    X1st_Salesperson_Territory__c = cnInv.c2g__Opportunity__r.X1st_Salesperson_Territory__c,
                                                    Status__c = cnInv.c2g__CreditNoteStatus__c, Payment_Status__c = cnInv.c2g__PaymentStatus__c);
                    invs.add(inv);
                    acc = accMap.get(inv.forseva1__Account__c);
                    if (acc == null) {
                        acc = new Account(Id = inv.forseva1__Account__c, forseva1__Collector__c = cnInv.c2g__Account__r.c2g__CODACreditManager__c);
                        accMap.put(acc.Id, acc);
                    }
                }
            }
            upsert invs forseva1__Invoice_Number__c;
            update accMap.values();
        }
        catch (Exception e) {
            String listOfIds = '';
            for (forseva1__FInvoice__c finv : invs) {
                listOfIds += finv.Sales_Credit_Note__c + '\n';
            }
            ForsevaUtilities.decodeExceptionAndSendEmail(e, 'Error in ForsevaSalesCreditNoteExtractBatchJob()', listOfIds, 'Forseva Administrators');
            throw e;
       }
    }

    global void finish(Database.BatchableContext bc) {
        AsyncApexJob job = [select Id, Status, NumberOfErrors, JobItemsProcessed, TotalJobItems, CreatedBy.Email
                            from   AsyncApexJob where Id = :bc.getJobId()];
        ForsevaUtilities.sendEmailToGroup('Forseva Sales Credit Note Extract Batch Job notification.', 
                                          'The batch Apex job processed ' + job.TotalJobItems + ' batches with ' + job.NumberOfErrors + ' failures.', 
                                          'Forseva Administrators', job.CreatedBy.Email);
        if (job.NumberOfErrors == 0) {
            Database.executeBatch(new ForsevaCashAndJournalCacheBuildBatchJob(), 200);
        }
    }
    
    // Public
    public String getQuery() {
        // RP - 4/1/14: replace CurrencyIsoCode with c2g__CreditNoteCurrency__r.Name
        String query = 'select Id, Name, c2g__CreditNoteCurrency__r.Name, c2g__InvoiceDate__c, c2g__OwnerCompany__c, c2g__DueDate__c, c2g__CreditNoteTotal__c, c2g__CreditNoteStatus__c, ' + 
                       '       c2g__OutstandingValue__c, c2g__Account__c, c2g__Invoice__r.Advertiser__c, c2g__Invoice__r.Operative_Order_ID__c, Billing_Period__c, ' + 
                       '       c2g__Opportunity__c, c2g__Opportunity__r.X1st_Salesperson__c, c2g__Opportunity__r.X1st_Salesperson_Territory__c, ' +
                       '       c2g__Invoice__r.Billing_Terms__c, c2g__Account__r.c2g__CODACreditManager__c, CurrencyIsoCode, c2g__PaymentStatus__c  ' +
                       'from   c2g__codaCreditNote__c  ' +
                       'where  c2g__OutstandingValue__c != 0 ' +
                       'or     (c2g__PaymentStatus__c = \'Paid\'  ' +
                       //'and     c2g__Transaction__r.LastModifiedDate >= last_n_days:1)';
                       'and     c2g__Transaction__r.LastModifiedDate >= ' + Label.FinForce_to_BC_Last_N_Days + ')';
        if (System.Test.isRunningTest()) {
            query += ' limit 1';
        }
        return query;
    }
    
    // Private
}