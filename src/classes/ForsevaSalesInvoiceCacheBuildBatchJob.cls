/**
* Copyright 2013 Forseva, LLC.  All rights reserved.
*/

global class ForsevaSalesInvoiceCacheBuildBatchJob implements Database.Batchable<SObject>, Schedulable {
    
    // Global
    global ForsevaSalesInvoiceCacheBuildBatchJob() {}
    
    global void execute(SchedulableContext sc) {
        Database.executeBatch(this, 200);
    }
    
    global Database.QueryLocator start(Database.BatchableContext bc) {
        Database.QueryLocator ql;
        try {
            ql = Database.getQueryLocator(getQuery());
        }
        catch (Exception e) {
            ForsevaUtilities.decodeExceptionAndSendEmail(e, 'Exception in ForsevaSalesInvoiceCacheBuildBatchJob.start()', '', 'Forseva Administrators');
            throw e;
        }
        return ql;
    }
    
    global void execute(Database.BatchableContext bc, List<SObject> scope) {
    }

    global void finish(Database.BatchableContext bc) {
        AsyncApexJob job = [select Id, Status, NumberOfErrors, JobItemsProcessed, TotalJobItems, CreatedBy.Email
                            from   AsyncApexJob where Id = :bc.getJobId()];
        //ForsevaUtilities.sendEmailToGroup('Forseva Sales Invoice Cache Build Batch Job notification.', 
        //                                  'The batch Apex job processed ' + job.TotalJobItems + ' batches with ' + job.NumberOfErrors + ' failures.', 
        //                                  'Forseva Administrators', job.CreatedBy.Email);
        if (job.NumberOfErrors == 0) {
            Database.executeBatch(new ForsevaSalesInvoiceExtractBatchJob(), 200);
        }
    }
    
    // Public
    public String getQuery() {
        // RP - 4/1/14: replace CurrencyIsoCode with c2g__InvoiceCurrency__c.
        //String query = 'select Id, Name, c2g__InvoiceDate__c, c2g__OwnerCompany__c, c2g__DueDate__c, c2g__InvoiceTotal__c, ' +
        String query = 'select Id, Name, c2g__InvoiceCurrency__r.Name, c2g__InvoiceDate__c, c2g__OwnerCompany__c, c2g__DueDate__c, c2g__InvoiceTotal__c, ' + 
                       '       c2g__OutstandingValue__c, c2g__Account__c, Advertiser__c, Operative_Order_ID__c, Billing_Period__c, ' +
                       '       c2g__Opportunity__c, c2g__Opportunity__r.X1st_Salesperson__c, c2g__Opportunity__r.X1st_Salesperson_Territory__c, ' +
                       '       Billing_Terms__c, c2g__Account__r.c2g__CODACreditManager__c, CurrencyIsoCode, ' +
                       '       c2g__PaymentStatus__c, c2g__InvoiceStatus__c, c2g__Transaction__r.LastModifiedDate ' +
                       'from   c2g__codaInvoice__c '; 
                       //'where  c2g__Transaction__r.LastModifiedDate >= last_n_days:24';

        if (System.Test.isRunningTest()) {
            query += ' limit 1';
        }
        return query;
    }
    
    // Private
}