/**
* Copyright 2013 Forseva, LLC.  All rights reserved.
*/

global class ForsevaSalesInvoiceExtractBatchJob implements Database.Batchable<SObject>, Schedulable {
    
    // Global
    global ForsevaSalesInvoiceExtractBatchJob() {}
    
    global void execute(SchedulableContext sc) {
        Database.executeBatch(this, 200);
    }
    
    global Database.QueryLocator start(Database.BatchableContext bc) {
        Database.QueryLocator ql;
        try {
            ql = Database.getQueryLocator(getQuery());
        }
        catch (Exception e) {
            ForsevaUtilities.decodeExceptionAndSendEmail(e, 'Exception in ForsevaSalesInvoiceExtractBatchJob.start()', '', 'Forseva Administrators');
            throw e;
        }
        return ql;
    }
    
    global void execute(Database.BatchableContext bc, List<SObject> scope) {
        Decimal paidAmount;
        Account acc;
        Map<Id, Account> accMap = new Map<Id, Account>();
        c2g__codaInvoice__c ffInv;
        forseva1__FInvoice__c inv;
        List<forseva1__FInvoice__c> invs = new List<forseva1__FInvoice__c>();
        String uploadId = String.valueOf(System.today());
        Integer uploadBatchId = Integer.valueOf(uploadId.replaceAll('-', '')); 
        Date invRptDate = System.today();
        try {
            for (SObject sobj : scope) {
                ffInv = (c2g__codaInvoice__c)sobj;
                paidAmount = ffInv.c2g__InvoiceTotal__c - ffInv.c2g__OutstandingValue__c;
                // RP - 4/1/14: replace CurrencyIsoCode with c2g__InvoiceCurrency__c.
                //inv = new forseva1__FInvoice__c(forseva1__Invoice_Number__c = ffInv.Name, CurrencyIsoCode = ffInv.CurrencyIsoCode,
                inv = new forseva1__FInvoice__c(forseva1__Invoice_Number__c = ffInv.Name, CurrencyIsoCode = ffInv.c2g__InvoiceCurrency__r.Name,
                                                forseva1__Account__c = ffInv.c2g__Account__c, forseva1__Invoice_Date__c = ffInv.c2g__InvoiceDate__c, 
                                                forseva1__Invoice_Due_Date__c = ffInv.c2g__DueDate__c, forseva1__Invoice_Report_Date__c = invRptDate, 
                                                forseva1__Invoice_Total__c = ffInv.c2g__InvoiceTotal__c, forseva1__Paid_Amount__c = paidAmount,
                                                forseva1__Upload_Batch_ID__c = uploadBatchId, Transaction_Type__c = 'Invoice', 
                                                Company__c = ffInv.c2g__OwnerCompany__c, Advertiser__c = ffInv.Advertiser__c, 
                                                Opportunity__c = ffInv.c2g__Opportunity__c, Billing_Terms__c = ffInv.Billing_Terms__c, 
                                                Operative_Order_ID__c = ffInv.Operative_Order_ID__c, Billing_Period__c = ffInv.Billing_Period__c,
                                                OutstandingValue__c = ffInv.c2g__OutstandingValue__c, Sales_Invoice__c = ffInv.Id, 
                                                X1st_Salesperson__c = ffInv.c2g__Opportunity__r.X1st_Salesperson__c, 
                                                X1st_Salesperson_Territory__c = ffInv.c2g__Opportunity__r.X1st_Salesperson_Territory__c,
                                                Status__c = ffInv.c2g__InvoiceStatus__c, Payment_Status__c = ffInv.c2g__PaymentStatus__c);
                invs.add(inv);
                String ffId = ffInv.Id;
                acc = accMap.get(inv.forseva1__Account__c);
                if (acc == null) {
                    acc = new Account(Id = inv.forseva1__Account__c, forseva1__Collector__c = ffInv.c2g__Account__r.c2g__CODACreditManager__c);
                    accMap.put(acc.Id, acc);
                }
            }
            upsert invs forseva1__Invoice_Number__c;
            update accMap.values();
        }
        catch (Exception e) {
            String listOfIds = '';
            for (forseva1__FInvoice__c finv : invs) {
                listOfIds += finv.Sales_Invoice__c + '\n';
            }
            ForsevaUtilities.decodeExceptionAndSendEmail(e, 'Error in ForsevaSalesInvoiceExtractBatchJob()', listOfIds, 'Forseva Administrators');
            throw e;
       }
    }

    global void finish(Database.BatchableContext bc) {
        AsyncApexJob job = [select Id, Status, NumberOfErrors, JobItemsProcessed, TotalJobItems, CreatedBy.Email
                            from   AsyncApexJob where Id = :bc.getJobId()];
        ForsevaUtilities.sendEmailToGroup('Forseva Sales Invoice Extract Batch Job notification.', 
                                          'The batch Apex job processed ' + job.TotalJobItems + ' batches with ' + job.NumberOfErrors + ' failures.', 
                                          'Forseva Administrators', job.CreatedBy.Email);
        if (job.NumberOfErrors == 0) {
            Database.executeBatch(new ForsevaSalesCreditNoteExtractBatchJob(), 200);
        }
    }
    
    // Public
    public String getQuery() {
        // RP - 4/1/14: add c2g__InvoiceCurrency__r.Name to the result set.
        String query = 'select Id, Name, c2g__InvoiceCurrency__r.Name, c2g__InvoiceDate__c, c2g__OwnerCompany__c, c2g__DueDate__c, c2g__InvoiceTotal__c, ' + 
                       '       c2g__OutstandingValue__c, c2g__Account__c, Advertiser__c, Operative_Order_ID__c, Billing_Period__c, ' +
                       '       c2g__Opportunity__c, c2g__Opportunity__r.X1st_Salesperson__c, c2g__Opportunity__r.X1st_Salesperson_Territory__c, ' +
                       '       Billing_Terms__c, c2g__Account__r.c2g__CODACreditManager__c, CurrencyIsoCode, c2g__Transaction__r.LastModifiedDate, ' +
                       '       c2g__PaymentStatus__c, c2g__InvoiceStatus__c ' +
                       'from   c2g__codaInvoice__c ' +
                       'where  c2g__OutstandingValue__c != 0 ' +
                       'or     (c2g__PaymentStatus__c = \'Paid\' ' +
                       //'and     c2g__Transaction__r.LastModifiedDate >= last_n_days:1)';
                       'and     c2g__Transaction__r.LastModifiedDate >= ' + Label.FinForce_to_BC_Last_N_Days + ')';
        if (System.Test.isRunningTest()) {
            query += ' limit 1';
        }
        return query;
    }
    
    // Private
}