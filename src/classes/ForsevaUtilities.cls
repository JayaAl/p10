/**
* Copyright 2013 Forseva, LLC.  All rights reserved.
*/
public with sharing class ForsevaUtilities {

    private static boolean firstRun = true;

    public static boolean isFirstRun() {
        return firstRun;
    }

    public static void setFirstRunFalse(){
        firstRun = false;
    }

    // Public

    @future
    public static void sendEmailNotifications(List<Id> userIds, List<Id> oppIds, List<Id> templateIds, String ccEmail) {
        integer numberOfEmails = userIds.size();
            for (integer idx = 0; idx < numberOfEmails; idx++) {
                sendEmailUsingTemplate(userIds.get(idx), oppIds.get(idx), templateIds.get(idx), ccEmail);
            }
    }

    public static void sendEmailUsingTemplate(Id userId, Id whatId, Id templateId, String ccEmail) {
   
        Messaging.SingleEmailMessage mail = new Messaging.SingleEmailMessage();
        mail.setTargetObjectId(userId);
        mail.setTemplateId(templateId);
        mail.setWhatId(whatId);
        mail.setBccSender(false);
        mail.setUseSignature(false);
        //nsk: Commenting below lines for CC as part of ticket#ISS-4610
        //String[] ccAddresses = new String[] {ccEmail};
        //mail.setCCAddresses(ccAddresses);
        mail.setSenderDisplayName('Pandora AR');
        mail.setSaveAsActivity(false); 
        Messaging.sendEmail(new Messaging.SingleEmailMessage[] { mail });
    }
          
    public static void decodeExceptionAndSendEmail(Exception e, String subjectText, String listOfIds, String groupToNotify) {
        String errMsg = 'Forseva exception caught: error is ' + e.getMessage() + '\n';
        if (e.getCause() != null) {
            errMsg += 'Cause = ' + e.getCause() + '\n';
            errMsg += 'Cause type name = ' + e.getTypeName() + '\n';
            errMsg += 'Cause message = ' + e.getCause().getMessage() + ' \n';
        }
        errMsg += listOfIds;
        List<String> toAddresses = getEmailsForMembersOfGroup(groupToNotify);
        if (toAddresses.size() == 0) {
            User usr = [select Id, Email from User where Id = :UserInfo.getUserId()];
            toAddresses.add(usr.Email);
        }
        if (toAddresses.size() > 0) {
            sendEmailNotification(subjectText, errMsg, toAddresses);
        }
        else {
            System.debug('ForsevaUtilities.decodeExceptionAndSendEmail(): ' + errMsg);
        }
    }

    public static void sendEmailNotification(String subjectText, String messageText, String[] toAddresses) {
        Messaging.SingleEmailMessage mail = new Messaging.SingleEmailMessage();
        mail.setToAddresses(toAddresses);
        mail.setSubject(subjectText);
        mail.setPlainTextBody(messageText);
        Messaging.sendEmail(new Messaging.SingleEmailMessage[] { mail });
    }

    public static void sendEmailToGroup(String subjectText, String messageText, String groupToNotify, String currentUserEmail) {
        Messaging.SingleEmailMessage mail = new Messaging.SingleEmailMessage();
        List<String> toAddresses = getEmailsForMembersOfGroup(groupToNotify);
        if (toAddresses == null || toAddresses.size() == 0) {
            toAddresses = new List<String>{currentUserEmail};
        }
        mail.setToAddresses(toAddresses);
        mail.setSubject(subjectText);
        mail.setPlainTextBody(messageText);
        Messaging.sendEmail(new Messaging.SingleEmailMessage[] { mail });
    }
    
    public static String[] getEmailsForMembersOfGroup(String groupName) {
        String[] userEmails = new List<String>();
        try {
            List<Id> userIds = new List<Id>();
            Group grp = [select id, Name 
                         from Group 
                         where  Name = :groupName];
            List<GroupMember> groupMembers = [select UserOrGroupId, GroupId 
                                              from   GroupMember 
                                              where  groupId = :grp.id];
            for (GroupMember gp : groupMembers) {
                userIds.add(gp.UserOrGroupId);
            }
            List<User> users = [select Id, Name, Email 
                                from   User 
                                where  Id in :userIds];
            
            for (User usr : users) {
                userEmails.add(usr.email);
            }
        }
        catch (Exception e) {
        }
        return userEmails;
    }

    public static SObject getCompleteSObject(Schema.SObjectType sot, ID objectId) {
        String query, columnList = '';
        Map<String, Schema.SObjectField> fields = sot.getDescribe().fields.getMap();
        Set<String> nameSet = fields.keySet();
        List<String> nameList = new List<String>();
        nameList.addAll(nameSet);
        for(String columnName : nameList) {
            if(columnList.length() > 0) {
                columnList += ',';
            }
            columnList += columnName;
        }
        query = 'select ' + columnList + ' from ' + sot.getDescribe().getName() + ' where id=\'' + String.escapeSingleQuotes(objectId) + '\'';
        SObject so = Database.query(query);
        return so;
    }

    // Private

}