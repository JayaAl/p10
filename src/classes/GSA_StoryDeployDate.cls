public with sharing class GSA_StoryDeployDate {
    public static void clearDeployEvent(Event[] oldEvent, String location){
        // Search for the ID of the story and (1) delete any StoryDeliveryDate.
        set<ID> sIds = new set<ID>();
        set<ID> p10SIds = new set<ID>();
        set<ID> p1SIds = new set<ID>();
        List<Story__c> stories = new List<Story__c>();
        List<Case> caseList = new List<Case>();
        for (Integer i=0; i<oldEvent.size(); i++)    
        {
            if (oldEvent[i].WhatId != null && oldEvent[i].StartDateTime != null)
            {
                sIds.add(oldEvent[i].WhatId);
            }
        }
        for(Story__c s : [select id, Date_Dev_Sandbox_Assigned__c, Date_Story_Moved_to_Staging__c, Production_deployment_Date__c from Story__c where id in :sIds]){
            if(location == 'P10'){
                s.Date_Dev_Sandbox_Assigned__c = null;
                stories.add(s);
            } else if (location == 'P1'){
                s.Date_Story_Moved_to_Staging__c = null;
                stories.add(s);             
            } else if (location == 'Prod'){
                s.Production_deployment_Date__c = null;
                stories.add(s);             
            }
        }
        update stories; 
        for(Case c : [select id, Related_Story__c, Date_Story_Moved_to_Staging__c, Date_Development_Complete__c, Production_deployment_Date__c from Case where Related_Story__c in :sIds]){
            if(location == 'P10'){
                c.Date_Development_Complete__c = null;
                caseList.add(c);
            } else if (location == 'P1'){
                c.Date_Story_Moved_to_Staging__c = null;
                caseList.add(c);                
            } else if (location == 'Prod'){
                c.Production_deployment_Date__c = null;
                caseList.add(c);                
            }
        }
        update caseList;        
    }
    public static void updateDeployDate(Event[] newEvent, String location){
        // Update Story and Case Date fields related to deployments w/ one Method.
        set<ID> sIds = new set<ID>();
        map<ID, Datetime> storyData = new map<ID, Datetime>();
        List<Story__c> stories = new List<Story__c>();
        List<Case> caseList = new List<Case>();
        for (Integer i=0; i<newEvent.size(); i++)    
        {
            if (newEvent[i].WhatId != null && newEvent[i].StartDateTime != null)
            {
                sIds.add(newEvent[i].WhatId);
                // Set the time zone of the story deploy time to local user time.
                Datetime dtSet = datetime.newInstance(newEvent[i].StartDateTime.date(), newEvent[i].StartDateTime.time());
                storyData.put(newEvent[i].WhatId, dtSet);
            }
        }
        // We need to update stories and any associated Change Service Requests as well as vice versa.
        for(Story__c s : [select id, Date_Dev_Sandbox_Assigned__c, Date_Story_Moved_to_Staging__c, Production_deployment_Date__c from Story__c where id in :sIds]){
            if(location == 'P10'){
                System.debug('*********************Story ID => ' + s.id);
                s.Date_Dev_Sandbox_Assigned__c = storyData.get(s.id);
                stories.add(s);
            } else if (location == 'P1'){
                s.Date_Story_Moved_to_Staging__c = storyData.get(s.id);
                stories.add(s);             
            } else if (location == 'Prod'){
                s.Production_deployment_Date__c = storyData.get(s.id);
                stories.add(s);             
            }
        }
        update stories;
        for(Case c : [select id, Related_Story__c, Date_Story_Moved_to_Staging__c, Date_Development_Complete__c, Production_deployment_Date__c from Case where Related_Story__c in :sIds or id in :sIds]){
            if(location == 'P10' && storyData.get(c.Related_Story__c)!=null){
                System.debug('*********************Case ID => ' + c.id + ' ' + c.Related_Story__c);
                c.Date_Development_Complete__c = storyData.get(c.Related_Story__c);
                caseList.add(c);
            } else if(location == 'P10'){
                c.Date_Development_Complete__c = storyData.get(c.id);
                caseList.add(c);                
            }else if (location == 'P1' && storyData.get(c.Related_Story__c)!=null){
                c.Date_Story_Moved_to_Staging__c = storyData.get(c.Related_Story__c);
                caseList.add(c);                
            } else if(location == 'P1'){
                c.Date_Story_Moved_to_Staging__c = storyData.get(c.id);
                caseList.add(c);                                
            }else if (location == 'Prod' && storyData.get(c.Related_Story__c)!=null){
                c.Production_deployment_Date__c = storyData.get(c.Related_Story__c);
                caseList.add(c);                
            }else if (location == 'Prod'){
                c.Production_deployment_Date__c = storyData.get(c.id);
                caseList.add(c);            
            }
        }
        update caseList;        
            System.debug('Total Number of SOQL Queries allowed in this apex code context: ' +  Limits.getQueries());
            System.debug('Total Number of records that can be queried  in this apex code context: ' +  Limits.getDmlRows());
            System.debug('Total Number of DML statements allowed in this apex code context: ' +  Limits.getDmlStatements() );
            System.debug('Total Number of script statements allowed in this apex code context: ' +  Limits.getScriptStatements());
    }
}