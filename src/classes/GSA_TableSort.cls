public class GSA_TableSort {

/* Written by: Shapoor Hashemi

   Date: 12/27/2011
   Background: Supports the GSA_Sort_Classic page for sprint planning and prioritization.  Next generation should be more drag and drop using jQuery. */
   
   
    List<Story__C> strys;
    public String sortField {get; set;}
    public String previousSortField {get; set;}
    public String timeBlockSelected;
    
    /* Overwrite save method to refresh back to the pageref below */
    public PageReference save() {
       try {
           upsert(strys);
       } catch(System.DMLException e) {
           ApexPages.addMessages(e);
           return null;
       }
       PageReference pageRef = new PageReference('/apex/gsa_sort_classic');
       return pageRef ;
    }
    
    /* This does the query which populates the id=table section of the page.
       The first condition is structured to enable the sort feature.  The 
       second kicks in when the Sprint dropdown is selected. */
    public List<Story__c> getStrys() {
        if (strys == null){
            strys = [select id, name, Business_Unit_Stack_Rank__c, Sprint_Stack_Rank__c, status__c, Business_Unit_Sortable__c from Story__c where LastModifiedDate < today ORDER BY LastModifiedDate DESC NULLS LAST Limit 45];
        } else if (strys == null && timeBlockSelected != null){
            strys = [select id, name, Business_Unit_Stack_Rank__c, Sprint_Stack_Rank__c, status__c, Business_Unit_Sortable__c from Story__c where Sprint__C =:timeBlockSelected];
        }
        return strys;
    }

    /* The table sort was copied from this site: http://wiki.developerforce.com/page/Sorting_Tables */
    public void doSort(){
        String order = 'asc';
        
        /*This checks to see if the same header was click two times in a row, if so 
        it switches the order.*/
        if(previousSortField == sortField){
            order = 'desc';
            previousSortField = null;
        }else{
            previousSortField = sortField;
        }
       
        //To sort the table we simply need to use this one line, nice!
        GSA_SuperSort.sortList(strys,sortField,order);
    }
           
    /* The picklist function for the sprint is enabled by the timeBlock and Items properties */
    public void settimeBlock(String s){
            timeBlockSelected = s;            
    }


    public String gettimeBlock(){
        return timeBlockSelected;
    }
        
    public List<SelectOption> getItems() {
        List<SelectOption> options = new List<SelectOption>();
        List<Sprint__C> sprintList = new List<Sprint__C>();
        sprintList = [select id, name from Sprint__c Where Sprint_Start_Date__c >= LAST_90_DAYS ];
        for (Integer j=0; j<sprintList.size(); j++)
        {
            /* The id is handed back when a particular sprint name is selected.  
            This is important for completing the Strys query. */
            options.add(new SelectOption(sprintList[j].id, sprintList[j].Name));
        }

        return options;
    }


    /* This enables the picklist selection action. */
    public PageReference doSearch() {
      gettimeBlock();
      strys = [select id, name, Business_Unit_Stack_Rank__c, Sprint_Stack_Rank__c, status__c, Business_Unit_Sortable__c from Story__c where Sprint__C = :timeBlockSelected];
      return null;
   }

}