public with sharing class GSA_TaskSum {
    public static void updateStoryTaskSum(Task[] oldTasks, Task[] newTasks)
    {
       Set<Id> storyIds = new Set<Id>();
     //System.debug('Task Size ' + newTasks.size());
       for (Integer i=0; i<newTasks.size(); i++)    
       {
           if ( oldTasks == null ||
                oldTasks[i].WhatId != newTasks[i].WhatId)
           {
               if (newTasks[i].WhatId != null)
               {
                   storyIds.add(newTasks[i].WhatId);
               }
           }    
       }
       if (storyIds.size() > 0)
       {    
            recalculateStoryTaskSum(storyIds);                                                                                                           
       }      
    }
    
    private static void recalculateStoryTaskSum(Set<Id> storyIds)
    {
        List<Story__c> stry = new List<Story__c>(); 
        for (Story__c c : [Select id,(Select Level_of_effort_in_hours__c, id From Tasks) From Story__c s where Id in :storyIds]) 
        {
            c.Sum_of_Level_of_Effort__c = 0;
            //Rollup the Task numbers on to the Story record.
            for (Task taskRecord : c.Tasks) 
            {
                c.Sum_of_Level_of_Effort__c += taskRecord.Level_of_effort_in_hours__c;
            }
            stry.add(c);
        }
        if (stry.size() > 0)            
        {
            update stry;    
        }
    }
    
}