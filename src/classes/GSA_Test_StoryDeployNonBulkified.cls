/**
 * This class contains unit tests for validating the behavior of Apex classes
 * and triggers.
 *
 * Unit tests are class methods that verify whether a particular piece
 * of code is working properly. Unit test methods take no arguments,
 * commit no data to the database, and are flagged with the testMethod
 * keyword in the method definition.
 *
 * All test methods in an organization are executed whenever Apex code is deployed
 * to a production organization to confirm correctness, ensure code
 * coverage, and prevent regressions. All Apex classes are
 * required to have at least 75% code coverage in order to be deployed
 * to a production organization. In addition, all triggers must have some code coverage.
 * 
 * The @isTest class annotation indicates this class only contains test
 * methods. Classes defined with the @isTest annotation do not count against
 * the organization size limit for all Apex scripts.
 *
 * See the Apex Language Reference for more information about Testing and Code Coverage.
 */
@isTest
private class GSA_Test_StoryDeployNonBulkified {

    static testMethod void storyInsertTest() {
        // Create 3 stories.  Create 3 events NOT on deployment calendar.
        // Insert to make sure stories aren't updated.  Create 3 events on deployment
        // calendar.  Insert to make sure stories are updated.
        Datetime dt = System.today()+7;
        List<Event> events = new List<Event>();
        List<Story__c> stories = new List<Story__c>();
        List<Story__c> storyAfterProcessing = new List<Story__c>();
        CustomSetting_GSA_Public_Calendar__c prodCalendarID = CustomSetting_GSA_Public_Calendar__c.getValues('Production Deployment Calendar ID');
        Event evnt = new Event();
        // These stories, with null Production deployment date info.
        // will be updated with release date dt.
        for (integer i=0; i<3; i++){
            stories.add(new Story__c(name='test'+1,Production_deployment_Date__c=null));
        }
        Test.startTest();
        insert stories;
        // These tasks should not fire off the GSA class for story
        // update because OwnerId is not set to Production Calender ID.
        for (integer i=0; i<1; i++){
            events.add(new Event(subject='test'+i, StartDateTime=dt, DurationInMinutes=120, Location='Prod', WhatId=stories[i].id));
        }
        for (integer i=1; i<2; i++){
            events.add(new Event(subject='test'+i, StartDateTime=dt, DurationInMinutes=120, Location='P1', WhatId=stories[i].id));
        }
        for (integer i=2; i<3; i++){
            events.add(new Event(subject='test'+i, StartDateTime=dt, DurationInMinutes=120, Location='P10', WhatId=stories[i].id));
        }
        insert events;
        System.debug('echo story ID: ' + stories[1].id + ' and associated Production deploy date: ' + stories[1].Production_deployment_Date__c);
        storyAfterProcessing = [select id, Date_Dev_Sandbox_Assigned__c, Date_Story_Moved_to_Staging__c, Production_deployment_Date__c from Story__c where id in :stories];
        System.debug('11111GSA********** echo story ID: ' + stories[1].id + ' and associated Production deploy date after: ' + storyAfterProcessing[1].Production_deployment_Date__c);
        System.assertEquals(storyAfterProcessing[0].Production_deployment_Date__c, null);
        System.assertEquals(storyAfterProcessing[0].Date_Story_Moved_to_Staging__c , null);
        System.assertEquals(storyAfterProcessing[0].Date_Dev_Sandbox_Assigned__c, null);
        System.assertEquals(storyAfterProcessing[1].Production_deployment_Date__c, null);
        System.assertEquals(storyAfterProcessing[1].Date_Story_Moved_to_Staging__c , null);
        System.assertEquals(storyAfterProcessing[1].Date_Dev_Sandbox_Assigned__c, null);
        System.assertEquals(storyAfterProcessing[2].Production_deployment_Date__c, null);
        System.assertEquals(storyAfterProcessing[2].Date_Story_Moved_to_Staging__c , null);
        System.assertEquals(storyAfterProcessing[2].Date_Dev_Sandbox_Assigned__c, null);
        // These tasks should fire the GSA class update action.
        events.clear();
        for (integer i=0; i<1; i++){
            events.add(new Event(OwnerId=prodCalendarID.Production_deployment_Calendar_ID__c, subject='test'+i, StartDateTime=dt, DurationInMinutes=120, Location='Prod', WhatId=stories[i].id));
        }
        for (integer i=1; i<2; i++){
            events.add(new Event(OwnerId=prodCalendarID.Production_deployment_Calendar_ID__c, subject='test'+i, StartDateTime=dt, DurationInMinutes=120, Location='P1', WhatId=stories[i].id));
        }
        for (integer i=2; i<3; i++){
            events.add(new Event(OwnerId=prodCalendarID.Production_deployment_Calendar_ID__c, subject='test'+i, StartDateTime=dt, DurationInMinutes=120, Location='P10', WhatId=stories[i].id));
        }
        insert events;
        Test.stopTest();
        System.debug('echo story ID: ' + stories[0].id + ' and associated Production deploy date: ' + stories[0].Production_deployment_Date__c);
        storyAfterProcessing = [select id, Date_Dev_Sandbox_Assigned__c, Date_Story_Moved_to_Staging__c, Production_deployment_Date__c from Story__c where id in :stories];
        System.debug('22222GSA********** echo story ID: ' + stories[0].id + ' and associated Production deploy date after: ' + storyAfterProcessing[0].Production_deployment_Date__c);
        System.assertEquals(storyAfterProcessing[0].Production_deployment_Date__c, dt);
        System.assertEquals(storyAfterProcessing[1].Date_Story_Moved_to_Staging__c, dt);
        System.assertEquals(storyAfterProcessing[2].Date_Dev_Sandbox_Assigned__c, dt);
        System.assertEquals(storyAfterProcessing[1].Production_deployment_Date__c, null);
        System.assertEquals(storyAfterProcessing[0].Date_Story_Moved_to_Staging__c, null);
        System.assertEquals(storyAfterProcessing[1].Date_Dev_Sandbox_Assigned__c, null);
        System.assertEquals(storyAfterProcessing[2].Production_deployment_Date__c, null);
        System.assertEquals(storyAfterProcessing[2].Date_Story_Moved_to_Staging__c, null);
        System.assertEquals(storyAfterProcessing[0].Date_Dev_Sandbox_Assigned__c, null);
    }
    
    static testMethod void storyUpsertTest() {
        // Create 6 stories.  Create 6 events on deployment calendar.
        // Update all events with a description change only.  Verify stories weren't 
        // updated.  Update all events w/ deployment date change.  Verify stories
        // were updated.
        Datetime dt = System.today()+7;
        List<Event> events = new List<Event>();
        List<Story__c> stories = new List<Story__c>();
        List<Case> cases = new List<Case>();
        List<Case> caseAfterProcessing = new List<Case>();
        List<Case> caseAfterProcessing2 = new List<Case>();
        List<Case> caseAfterProcessing3 = new List<Case>();
        List<Story__c> storyAfterProcessing = new List<Story__c>();
        List<Story__c> storyAfterProcessing2 = new List<Story__c>();
        List<Story__c> storyAfterProcessing3 = new List<Story__c>();
        CustomSetting_GSA_Public_Calendar__c prodCalendarID = CustomSetting_GSA_Public_Calendar__c.getValues('Production Deployment Calendar ID');
        for (integer i=0; i<6; i++){
            stories.add(new Story__c(name='test'+1,Production_deployment_Date__c=null));
        }
        Test.startTest();
        insert stories;
        // This insert (storyAfterProcessing) should update the story records with new 
        // dates as well as the even record of the index of 6.
        for (integer i=0; i<1; i++){
            cases.add(new Case(RecordTypeId='0124000000015C4', subject='test'+i, Type='CSR', Status='New', Related_Story__C=stories[i].Id));
        }
        for (integer i=1; i<2; i++){
            cases.add(new Case(RecordTypeId='0124000000015C4', subject='test'+i, Type='CSR', Status='New'));
        }
        for (integer i=2; i<3; i++){
            cases.add(new Case(RecordTypeId='0124000000015C4', subject='test'+i, Type='CSR', Status='New', Related_Story__C=stories[i].Id));
        }
        for (integer i=3; i<4; i++){
            cases.add(new Case(RecordTypeId='0124000000015C4', subject='test'+i, Type='CSR', Status='New'));
        }
        for (integer i=4; i<5; i++){
            cases.add(new Case(RecordTypeId='0124000000015C4', subject='test'+i, Type='CSR', Status='New', Related_Story__C=stories[i].Id));
        }
        for (integer i=5; i<6; i++){
            cases.add(new Case(RecordTypeId='0124000000015C4', subject='test'+i, Type='CSR', Status='New'));
        }
        insert cases;
        for (integer i=0; i<1; i++){
            events.add(new Event(OwnerId=prodCalendarID.Production_deployment_Calendar_ID__c, subject='test'+i, StartDateTime=dt, DurationInMinutes=120, location='Prod', WhatId=stories[i].id));
        }
        for (integer i=1; i<2; i++){
            events.add(new Event(OwnerId=prodCalendarID.Production_deployment_Calendar_ID__c, subject='test'+i, StartDateTime=dt, DurationInMinutes=120, location='Prod', WhatId=cases[i].id));
        }
        for (integer i=2; i<4; i++){
            events.add(new Event(OwnerId=prodCalendarID.Production_deployment_Calendar_ID__c, subject='test'+i, StartDateTime=dt, DurationInMinutes=120, location='P1', WhatId=stories[i].id));
        }
        for (integer i=4; i<6; i++){
            events.add(new Event(OwnerId=prodCalendarID.Production_deployment_Calendar_ID__c, subject='test'+i, StartDateTime=dt, DurationInMinutes=120, location='P10', WhatId=stories[i].id));
        }
        insert events;
        storyAfterProcessing = [select id, Date_Dev_Sandbox_Assigned__c, Date_Story_Moved_to_Staging__c, Production_deployment_Date__c, createdDate, lastModifiedDate from Story__c where id in :stories];
        caseAfterProcessing = [Select c.Related_Story__c, c.RecordTypeId, c.Production_deployment_Date__c, c.Date_Story_Moved_to_Staging__c, c.Date_Development_Complete__c From Case c where id in :cases];
        // This update (storyAfterProcessing2) should not have any impact on any
        // of the records.
        for (integer i=0; i<6; i++){
            events[i].description = 'foo'+i;
        }
        
        update events;
        storyAfterProcessing2 = [select id, Date_Dev_Sandbox_Assigned__c, Date_Story_Moved_to_Staging__c, Production_deployment_Date__c, createdDate, lastModifiedDate from Story__c where id in :stories];
        caseAfterProcessing2 = [Select c.Related_Story__c, c.RecordTypeId, c.Production_deployment_Date__c, c.Date_Story_Moved_to_Staging__c, c.Date_Development_Complete__c From Case c where id in :cases];
        // This update (storyAfterProcessing3) should update the story 
        // records as well as the cases related to the updated events.
        for (integer i=1; i<2; i++){
            events[i].WhatId = cases[i].Id;
        }
        for (integer i=3; i<4; i++){
            events[i].WhatId = cases[i].Id;
        }
        for (integer i=5; i<6; i++){
            events[i].WhatId = cases[i].Id;
        }
        
        for (integer i=0; i<6; i++){
            events[i].StartDateTime = dt+7;
        }
        update events;
        Test.stopTest();
        storyAfterProcessing3 = [select id, Date_Dev_Sandbox_Assigned__c, Date_Story_Moved_to_Staging__c, Production_deployment_Date__c, createdDate, lastModifiedDate from Story__c where id in :stories];
        caseAfterProcessing3 = [Select id, c.Related_Story__c, c.RecordTypeId, c.Production_deployment_Date__c, c.Date_Story_Moved_to_Staging__c, c.Date_Development_Complete__c From Case c where id in :cases];

        System.assertEquals(dt, storyAfterProcessing2[0].Production_deployment_Date__c);
        System.assertEquals(dt, storyAfterProcessing2[3].Date_Story_Moved_to_Staging__c);
        System.assertEquals(dt, storyAfterProcessing2[5].Date_Dev_Sandbox_Assigned__c);
        /* Below story record not updated because the event WhatID was changed to case before event update.*/
        System.assertEquals(dt, storyAfterProcessing3[3].Date_Story_Moved_to_Staging__c);
        System.assertEquals(dt, storyAfterProcessing3[5].Date_Dev_Sandbox_Assigned__c);

        /* Below story/case records updated to dt+7 based on Event WhatID relationship and location*/
        System.assertEquals(dt+7, storyAfterProcessing3[0].Production_deployment_Date__c);
        System.assertEquals(null, storyAfterProcessing3[0].Date_Story_Moved_to_Staging__c);
        System.assertEquals(null, storyAfterProcessing3[0].Date_Dev_Sandbox_Assigned__c);
        System.assertEquals(dt+7, caseAfterProcessing3[0].Production_deployment_Date__c);
        System.assertEquals(null, caseAfterProcessing3[0].Date_Story_Moved_to_Staging__c);
        System.assertEquals(null, caseAfterProcessing3[0].Date_Development_Complete__c);
        System.assertEquals(null, caseAfterProcessing3[2].Production_deployment_Date__c);
        System.assertEquals(dt+7, caseAfterProcessing3[2].Date_Story_Moved_to_Staging__c);
        System.assertEquals(null, caseAfterProcessing3[2].Date_Development_Complete__c);
        System.assertEquals(null, caseAfterProcessing3[3].Production_deployment_Date__c);
        System.assertEquals(dt+7, caseAfterProcessing3[3].Date_Story_Moved_to_Staging__c);
        System.assertEquals(null, caseAfterProcessing3[3].Date_Development_Complete__c);
    }
  
    static testMethod void storyDeployDateClearTest() {
        // Create 6 stories.  Create 6 events on deployment calendar.
        // Create 6 cases; 3 associated with events and 3 cases w/ related stories.
        // Verify stories and 4 cases updated with deployment date.  Delete 6 events.
        // Verify stories and 4 cases updated with null deployment date.
        Datetime dt = System.today()+7;
        List<Event> events = new List<Event>();
        List<Case> cases = new List<Case>();
        List<Case> caseAfterProcessing = new List<Case>();
        List<Case> caseAfterProcessing2 = new List<Case>();
        List<Story__c> stories = new List<Story__c>();
        List<Story__c> storyAfterProcessing = new List<Story__c>();
        List<Story__c> storyAfterProcessing2 = new List<Story__c>();
        CustomSetting_GSA_Public_Calendar__c prodCalendarID = CustomSetting_GSA_Public_Calendar__c.getValues('Production Deployment Calendar ID');
        for (integer i=0; i<6; i++){
            stories.add(new Story__c(name='test'+1,Production_deployment_Date__c=null));
        }
        Test.startTest();
        insert stories;
        for (integer i=0; i<1; i++){
            cases.add(new Case(RecordTypeId='0124000000015C4', subject='test'+i, Type='CSR', Status='New', Related_Story__C=stories[i].Id));
        }
        for (integer i=1; i<2; i++){
            cases.add(new Case(RecordTypeId='0124000000015C4', subject='test'+i, Type='CSR', Status='New', Production_deployment_Date__c=dt+7));
        }
        for (integer i=2; i<3; i++){
            cases.add(new Case(RecordTypeId='0124000000015C4', subject='test'+i, Type='CSR', Status='New', Related_Story__C=stories[i].Id));
        }
        for (integer i=3; i<4; i++){
            cases.add(new Case(RecordTypeId='0124000000015C4', subject='test'+i, Type='CSR', Status='New', Date_Story_Moved_to_Staging__c=dt+7));
        }
        for (integer i=4; i<5; i++){
            cases.add(new Case(RecordTypeId='0124000000015C4', subject='test'+i, Type='CSR', Status='New', Related_Story__C=stories[i].Id));
        }
        for (integer i=5; i<6; i++){
            cases.add(new Case(RecordTypeId='0124000000015C4', subject='test'+i, Type='CSR', Status='New', Date_Development_Complete__c=dt+7));
        }
        insert cases;
        for (integer i=0; i<2; i++){
            events.add(new Event(OwnerId=prodCalendarID.Production_deployment_Calendar_ID__c, subject='test'+i, StartDateTime=dt, DurationInMinutes=120, location='Prod', WhatId=stories[i].id));
        }
        for (integer i=2; i<4; i++){
            events.add(new Event(OwnerId=prodCalendarID.Production_deployment_Calendar_ID__c, subject='test'+i, StartDateTime=dt, DurationInMinutes=120, location='P1', WhatId=stories[i].id));
        }
        for (integer i=4; i<6; i++){
            events.add(new Event(OwnerId=prodCalendarID.Production_deployment_Calendar_ID__c, subject='test'+i, StartDateTime=dt, DurationInMinutes=120, location='P10', WhatId=stories[i].id));
        }
        insert events;
        storyAfterProcessing = [select id, Date_Dev_Sandbox_Assigned__c, Date_Story_Moved_to_Staging__c, Production_deployment_Date__c, createdDate, lastModifiedDate from Story__c where id in :stories];
        caseAfterProcessing = [Select c.Related_Story__c, c.RecordTypeId, c.Production_deployment_Date__c, c.Date_Story_Moved_to_Staging__c, c.Date_Development_Complete__c From Case c where id in :cases];
        System.assertEquals(dt, storyAfterProcessing[0].Production_deployment_Date__c);
        System.assertEquals(dt, storyAfterProcessing[3].Date_Story_Moved_to_Staging__c);
        System.assertEquals(dt, storyAfterProcessing[5].Date_Dev_Sandbox_Assigned__c);
        System.assertEquals(dt, caseAfterProcessing[0].Production_deployment_Date__c);
        System.assertEquals(null, caseAfterProcessing[0].Date_Story_Moved_to_Staging__c);
        System.assertEquals(null, caseAfterProcessing[0].Date_Development_Complete__c);
        System.assertEquals(null, caseAfterProcessing[2].Production_deployment_Date__c);
        System.assertEquals(dt, caseAfterProcessing[2].Date_Story_Moved_to_Staging__c);
        System.assertEquals(null, caseAfterProcessing[2].Date_Development_Complete__c);
        System.assertEquals(null, caseAfterProcessing[4].Production_deployment_Date__c);
        System.assertEquals(null, caseAfterProcessing[4].Date_Story_Moved_to_Staging__c);
        System.assertEquals(dt, caseAfterProcessing[4].Date_Development_Complete__c);
        // cases from odd index records have no story associated so event insert should not update dates.
        System.assertEquals(dt+7, caseAfterProcessing[1].Production_deployment_Date__c);
        System.assertEquals(null, caseAfterProcessing[1].Date_Story_Moved_to_Staging__c);
        System.assertEquals(null, caseAfterProcessing[1].Date_Development_Complete__c);
        System.assertEquals(dt+7, caseAfterProcessing[3].Date_Story_Moved_to_Staging__c);
        System.assertEquals(null, caseAfterProcessing[3].Production_deployment_Date__c);
        System.assertEquals(null, caseAfterProcessing[5].Production_deployment_Date__c);
        System.assertEquals(null, caseAfterProcessing[5].Date_Story_Moved_to_Staging__c);
        System.assertEquals(dt+7, caseAfterProcessing[5].Date_Development_Complete__c);
        
        delete events;      
        Test.stopTest();
        storyAfterProcessing2 = [select id, Date_Dev_Sandbox_Assigned__c, Date_Story_Moved_to_Staging__c, Production_deployment_Date__c, createdDate, lastModifiedDate from Story__c where id in :stories];
        caseAfterProcessing2 = [Select c.Production_deployment_Date__c, c.Date_Story_Moved_to_Staging__c, c.Date_Development_Complete__c From Case c where id in :cases];

        System.assertEquals(null, storyAfterProcessing2[0].Production_deployment_Date__c);
        System.assertEquals(null, storyAfterProcessing2[3].Date_Story_Moved_to_Staging__c);
        System.assertEquals(null, storyAfterProcessing2[5].Date_Dev_Sandbox_Assigned__c);
        System.assertEquals(null, caseAfterProcessing2[0].Production_deployment_Date__c);
        System.assertEquals(null, caseAfterProcessing2[2].Date_Story_Moved_to_Staging__c);
        System.assertEquals(null, caseAfterProcessing2[4].Date_Development_Complete__c);
        System.assertEquals(dt+7, caseAfterProcessing2[1].Production_deployment_Date__c);
        System.assertEquals(dt+7, caseAfterProcessing2[3].Date_Story_Moved_to_Staging__c);
        System.assertEquals(dt+7, caseAfterProcessing2[5].Date_Development_Complete__c);
    }
}