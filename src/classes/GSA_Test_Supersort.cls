public Class GSA_Test_Supersort {
    /*Some test methods that provide 100% coverage */
    public static testMethod void sortAscendingTest(){
        
        List<Opportunity> opps = new List<Opportunity>();
        for(integer i = 0; i<4; i++){
            opps.add(new Opportunity(Name = 'test' + i, Amount = 1000 * Math.random()));
        }
        
        Test.startTest();
        Long start = system.currentTimeMillis();
        GSA_Supersort.sortList(opps,'Amount','asc');
        system.debug(system.currentTimeMillis() - start);
        Test.stopTest();
        
        //Assert the list was sorted correctly
        Decimal assertValue = -1;
        for(Opportunity o : opps) {
            System.debug('Opp value: ' + o.amount);
            System.assert(assertValue <= o.amount);
            assertValue = o.amount;
        }  
    }
    
    public static testMethod void sortDescendingTest(){
        
        List<Opportunity> opps = new List<Opportunity>();
        for(integer i = 0; i<4; i++){
            opps.add(new Opportunity(Name = 'test' + i, Amount = 1000 * Math.random()));
        }
        
        Test.startTest();
        GSA_Supersort.sortList(opps,'Amount','desc');
        Test.stopTest();
        
        //Assert the list was sorted correctly
        Decimal assertValue = 1001;
        for(Opportunity o : opps) {
            System.debug('Opp value: ' + o.amount);
            System.assert(assertValue >= o.amount);
            assertValue = o.amount;
        }  
    }
    
    public static testMethod void conditionTest(){
    	
    	List<Story__c> strys = new List<Story__c>();
    	Product_Backlog__c pBacklog = new Product_Backlog__c(name='Pandora Backlog');
    	GSA_Supersort tCon = new GSA_Supersort();
    	
        Test.startTest();
    	insert pBacklog;
    	Sprint__c pSprint = new Sprint__c(name='2011-10 October', Product_Backlog__c = pBacklog.Id);
    	Sprint__c pSprint2 = new Sprint__c(name=null, Product_Backlog__c = pBacklog.Id);
    	insert pSprint;
    	insert pSprint2;
    	
        for(integer i = 0; i<2; i++){
            strys.add(new Story__c(Name = 'test' + i, Sprint__C = pSprint.Id, Product_Backlog__c = pBacklog.Id));
        }
        for(integer i = 2; i<4; i++){
            strys.add(new Story__c(Name = null, Sprint__C = null));
        }
        for(integer i = 4; i<6; i++){
            strys.add(new Story__c(Name = 'test', Sprint__C = pSprint2.Id, Product_Backlog__c = pBacklog.Id));
        }
        
        
        
        insert strys;
        System.debug('------------> Expecting Null story Name: ' + strys[2].Name);
        GSA_Supersort.sortList(strys,'Sprint__c','desc');
        Test.stopTest();
        
    }
}