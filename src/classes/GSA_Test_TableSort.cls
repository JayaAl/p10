/**
 * This class contains unit tests for validating the behavior of Apex classes
 * and triggers.
 *
 * Unit tests are class methods that verify whether a particular piece
 * of code is working properly. Unit test methods take no arguments,
 * commit no data to the database, and are flagged with the testMethod
 * keyword in the method definition.
 *
 * All test methods in an organization are executed whenever Apex code is deployed
 * to a production organization to confirm correctness, ensure code
 * coverage, and prevent regressions. All Apex classes are
 * required to have at least 75% code coverage in order to be deployed
 * to a production organization. In addition, all triggers must have some code coverage.
 * 
 * The @isTest class annotation indicates this class only contains test
 * methods. Classes defined with the @isTest annotation do not count against
 * the organization size limit for all Apex scripts.
 *
 * See the Apex Language Reference for more information about Testing and Code Coverage.
 */
@isTest
private class GSA_Test_TableSort {

    static testMethod void myUnitTest() {
        /*  Create 1 Product Backlog.  Create 2 sprints w/ Id, name, start_date.
            Create 10 stories w/ 5 of them associated to a sprint and following fields
            Id, 
            name, 
            Business_Unit_Stack_Rank__c,
            Sprint_Rank__c, 
            status__c, 
            Business_Unit_Sortable__c
            
            ?Set the timeBlock equal to the first Sprint name.?
            Set a pageReference ===> PageReference pageRef = new PageReference('/apex/gsa_sort_classic');
            Call the doSearch() method.
        */
        List<Sprint__c> sprntList = new List<Sprint__c>();
        List<Story__c> stryList = new List<Story__c>();
        String selectedSprint;
        PageReference pageRef = Page.gsa_sort_classic;
        PageReference pageRef2 = Page.gsa_sort_classic;
		Test.setCurrentPageReference(pageRef);
		
        GSA_TableSort tSortC = new GSA_TableSort();
        Test.startTest();

        selectedSprint = tSortC.gettimeBlock();
		System.assertEquals(selectedSprint, null);
        List<Story__c>pStories = tSortC.getStrys();
        tSortC.getItems();
        tSortC.settimeBlock('2011-11 November');
        selectedSprint = tSortC.gettimeBlock();
		System.assertEquals(selectedSprint, '2011-11 November');
        List<Story__c>pStories2 = tSortC.getStrys();
			        System.debug('---------------->size of Story ' + pStories2.size());
			        System.debug('---------------->pageRef before search ' + pageRef);
        tSortC.previousSortField = 'Name';
        selectedSprint = tSortC.gettimeBlock();
        tSortC.sortField = 'Name';
        tSortC.doSort();
        tSortC.previousSortField = 'Name';
        tSortC.sortField = 'Status__c';
        tSortC.doSort();
        pageRef = tSortC.doSearch();
		System.assertEquals(pageRef, null);
        tSortC.timeBlockSelected = '2012-02 February';
        selectedSprint = tSortC.gettimeBlock();
		System.assertEquals(selectedSprint, '2012-02 February');
        pageRef = new PageReference('/apex/gsa_sort_classic');
        pageRef2 = tSortC.save();


        Test.stopTest();
    }
}