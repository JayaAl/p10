/**
 * This class contains unit tests for validating the behavior of Apex classes
 * and triggers.
 *
 * Unit tests are class methods that verify whether a particular piece
 * of code is working properly. Unit test methods take no arguments,
 * commit no data to the database, and are flagged with the testMethod
 * keyword in the method definition.
 *
 * All test methods in an organization are executed whenever Apex code is deployed
 * to a production organization to confirm correctness, ensure code
 * coverage, and prevent regressions. All Apex classes are
 * required to have at least 75% code coverage in order to be deployed
 * to a production organization. In addition, all triggers must have some code coverage.
 * 
 * The @isTest class annotation indicates this class only contains test
 * methods. Classes defined with the @isTest annotation do not count against
 * the organization size limit for all Apex scripts.
 *
 * See the Apex Language Reference for more information about Testing and Code Coverage.
 */
@isTest
private class GSA_Test_TaskSum {
    public static Story__c stry = new Story__c(Name='Apex_Test_Story', Sum_of_Level_of_Effort__c=100);
    public static Story__c stryNew = new Story__c(Name='Apex_Test2_Story', Sum_of_Level_of_Effort__c=2000);
    //public static Task tstTask = new Task(Name='Apex_Test_Task', Level_of_Effort__c='3');
    //public static Task tstTaskNew = new Task(Name='Apex_Test2_Task', Level_of_Effort__c='7');
    
    public static testMethod void testTaskInsert(){
        //Test the insert process.
        insert stry;
        insert stryNew;
        //insert tstTask;
        //insert tstTaskNew;
        //System.debug('**********************value of inserted Story__c ID ' + tstTask.id + ' ' + tstTask.FirstName);
        
        List<Task> testTaskList = new List<Task>();
        for (integer i=0; i<12; i++){
            testTaskList.add(new Task(Subject = 'Apex_Test_TaskName', Level_of_effort_in_hours__c=3.0, WhatID = stry.Id));
        }
        insert testTaskList;
        
        Story__c stry1 = [Select id, Sum_of_Level_of_Effort__C from Story__c where id = :stry.id];
        Story__c stry2 = [Select id, Sum_of_Level_of_Effort__C from Story__c where id = :stryNew.id];        
        
        System.assertEquals(36, stry1.Sum_of_Level_of_Effort__C);
        System.assertEquals(2000,stryNew.Sum_of_Level_of_Effort__C);
        
   }// end of testTaskInsert() Method.

    public static testMethod void testTaskDelete(){
        //Test the delete process.
        insert stry;
        //insert tstTask;
        List<Task> testTaskList = new List<Task>();
        for (integer i=0; i<12; i++){
            testTaskList.add(new Task(Subject = 'Apex_Test_TaskName', Level_of_effort_in_hours__c=3.0, WhatID = stry.Id));
       }
        insert testTaskList;

        Story__c stry1 = [Select id, Sum_of_Level_of_Effort__C from Story__c where id = :stry.id];        
        System.assertEquals(36, stry1.Sum_of_Level_of_Effort__C);
        
        delete testTaskList[10];
        delete testTaskList[0];
        
        stry1 = [Select id, Sum_of_Level_of_Effort__C from Story__c where id = :stry.id];
        System.assertEquals(30, stry1.Sum_of_Level_of_Effort__C);
        
        undelete testTaskList[0];

        stry1 = [Select id, Sum_of_Level_of_Effort__C from Story__c where id = :stry.id];
        System.assertEquals(33, stry1.Sum_of_Level_of_Effort__C);
    }// end of testTaskDelete() Method.

}