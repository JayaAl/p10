public without sharing class GeoCodeInfoAtachmentCtrlr {
	
	private id quoteId;
	public List<GeoCodeWrapper>  wrapperVarLst {get;set;} 
	public GeoCodeInfoAtachmentCtrlr() {
		
		quoteId = ApexPages.currentPage().getParameters().get('QID');
		wrapperVarLst = new List<GeoCodeWrapper>();

		List<SBQQ__Quote__c> quoteLst = [select id,Name,ATG_Campaign_Name__c,OwnerId from SBQQ__Quote__c where id=:quoteId];
		if(quoteLst != null && !quoteLst.isEmpty()){
			SBQQ__Quote__c quote = quoteLst[0];
			String quoteName = quote.ATG_Campaign_Name__c;
	      	Map<Id,SBQQ__QuoteLine__c> qliMap = new Map<Id,SBQQ__QuoteLine__c>( [select id,name,ATS_Zip__c from SBQQ__QuoteLine__c where SBQQ__Quote__c =: quote.Id] );

	      	if(qliMap != null && qliMap.values().size() > 0){
	            for(ATG_Quote_Line_GeoData__c geoDataVar : [Select id,ATG_County_Name__c,ATB_State_Name__c,ATG_Quote_Line__c,ATG_DMA__c,ATG_DMA__r.Name from ATG_Quote_Line_GeoData__c where ATG_Quote_Line__c IN: qliMap.keySet() ]){
	              wrapperVarLst.add(new GeoCodeWrapper(quoteName,(qliMap.get(geoDataVar.ATG_Quote_Line__c)).Name,((qliMap.get(geoDataVar.ATG_Quote_Line__c)).ATS_Zip__c != null ? (qliMap.get(geoDataVar.ATG_Quote_Line__c)).ATS_Zip__c : 'N/A'),(geoDataVar.ATB_State_Name__c != null ? geoDataVar.ATB_State_Name__c : 'N/A'),(geoDataVar.ATG_County_Name__c != null ? geoDataVar.ATG_County_Name__c : 'N/A'),(geoDataVar.ATG_DMA__c != null ? geoDataVar.ATG_DMA__r.Name : 'N/A')));
	            }
	        }

		}
      	

	}


	public class GeoCodeWrapper{

		public string quoteName {get;set;}
		public string quoteLineName {get;set;}
		public string zipCodeStr {get;set;}
		public string stateStr {get;set;}
		public string countyStr {get;set;}
		public string MarketStr {get;set;}

		public GeoCodeWrapper(String quoteNameParam,String quoteLineNameParam, String zipCodeStrParam, String stateStrParam,String countyStrParam, String MarketStrParam){
			
			quoteName = quoteNameParam;
			quoteLineName = quoteLineNameParam;
			zipCodeStr = zipCodeStrParam;
			stateStr = stateStrParam;
			countyStr = countyStrParam;
			MarketStr = MarketStrParam;		
		}		
	}
}