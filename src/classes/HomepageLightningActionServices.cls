/**
* ─────────────────────────────────────────────────────────────────────────────────────────────────┐
* This class services lightning action  and services for home Page.
* ──────────────────────────────────────────────────────────────────────────────────────────────────
* @author         Jaya Alaparthi
* @maintainedBy   Jaya Alaparthi
* @version        1.0
* @created        2017-05-17
* @modified       
* @systemLayer    Service
* @see            ????
* @see            ????
* ──────────────────────────────────────────────────────────────────────────────────────────────────
* @changes
* vX.X            Jaya Alaparthi
* YYYY-MM-DD      Each change to this file should be documented by incrementing the version number,
*                 and adding a new entry to this @changes list. Note that there is a single blank
*                 line between each @changes entry.
* ─────────────────────────────────────────────────────────────────────────────────────────────────┘
*/
public with sharing class HomepageLightningActionServices {
	
	//─────────────────────────────────────────────────────────────────────────┐
	// validateForJIRACreation: validate Jira creation based on user Profile.
	// @param opptyId  opportunity Id
	// @return String 
    //─────────────────────────────────────────────────────────────────────────┘
	/*@AuraEnabled
	public static String buildAnalyticsUrl() {

		return getserviceUrl();
	}

	//─────────────────────────────────────────────────────────────────────────┐
	// getserviceUrl: this is used for birst url generation
	// @param 
	// @return String 
    //─────────────────────────────────────────────────────────────────────────┘
	private static String getserviceUrl() {

		String currentURL = URL.getCurrentRequestUrl().toExternalForm();
		System.debug('currentURL:'+currentURL);
		currentURL = currentURL.substring(0,currentURL.indexOf('salesforce.com')+14);
		System.debug('currentURL:'+currentURL);
		currentURL += '/services/Soap/c/25.0/'+UserInfo.getOrganizationId().substring(0,15);
		currentURL += '&sessionid='+UserInfo.getSessionId();
		System.debug('currentURL:'+currentURL);
		return currentURL;
	}
	*/
	//─────────────────────────────────────────────────────────────────────────┐
	// getClasicHost: 
	// @param   
	// @return String 
    //─────────────────────────────────────────────────────────────────────────┘
	@AuraEnabled
	public static String getClasicHost() {

		String currentURL = URL.getCurrentRequestUrl().toExternalForm();
		System.debug('currentURL:'+currentURL);
		currentURL = currentURL.substring(0,currentURL.indexOf('salesforce.com')+14);
		System.debug('currentURL:'+currentURL);
		return currentURL;
	}
}