@isTest
private class HomepageLightningActionServicesTest {
	
	@testSetUp static void testDataSetup() {

		// create Account
		/*Account account =  new Account();
		account = UTIL_TestUtil.generateAccount();
		account.Name = 'opptyLightActSErvicesTEST';
		insert account;

		Id opptyRecordTypeId = Schema.SObjectType.Opportunity.getRecordTypeInfosByName().get('Opportunity - Inside/Local Full').getRecordTypeId();
        
		System.debug('opptyRecordTypeId:'+opptyRecordTypeId);
		Opportunity opportunity = new Opportunity();
		opportunity = UTIL_TestUtil.generateOpportunity(account.Id);
		opportunity.Name = 'OpptyLighServicesTest';
		opportunity.CurrencyIsoCode = 'USD';
		opportunity.RecordTypeId = opptyRecordTypeId;
		opportunity.TTR_User_Data__c = 'Email Address';
		insert opportunity;*/
	}
	
	@isTest
	static void getClasicHostTest() {

		String instanceNameStr;
		Organization org = [SELECT IsSandbox, InstanceName
							FROM Organization
							LIMIT 1];
		if(org.IsSandbox) {
			instanceNameStr = org.InstanceName+'.my.salesforce.com';
		} else {
			instanceNameStr = '.my.salesforce.com';
		}
		Test.startTest();
			
		String urlTest = HomepageLightningActionServices.getClasicHost();
		System.debug('urlTest:'+urlTest);
		System.debug('instanceNameStr:'+instanceNameStr);
		System.assert(urlTest.contains(instanceNameStr.toLowerCase()));

		Test.stopTest();
	}
	
}