/**
* This class is used to crete a new Case object.
* @Author: Bharath Kumar Gadiyaram. 
*/
public with sharing class IOAPR_CaseCreateController {
    public Opportunity opportunity {get;set;}
    public Case newCase {get;set;}
    public Account partnerAcct {get;set;} 
    public IOAPR_CaseCreateController()
    {
        
    }
    
    public IOAPR_CaseCreateController(ApexPages.StandardController controller)
    {
        this.opportunity = (Opportunity)controller.getRecord();
        // Retrieving relevant fields from Opportunity.
        /*this.opportunity = [Select id,Account.Name,Amount,ContractEndDate__c,ContractStartDate__c 
                                from Opportunity where id = :this.opportunity.id];
        */
        this.opportunity = [Select o.AccountId,id,Account.Name,Amount,ContractEndDate__c,ContractStartDate__c , 
        (Select AccountToId From AccountPartners limit 1) From Opportunity o where o.Id = :this.opportunity.id];
        this.newCase = new Case();
        this.partnerAcct = new Account();
    }    
   
    public PageReference doSave(){
        this.newCase.Status = 'Pending IO approval';
        
        //Do we have a partner account for the Opty?  then add the ad agency info to the case.
        try {
            this.newCase.Advertiser__c = this.opportunity.Account.Name;
            this.newCase.Total_IO_Amount_deprecated__c = this.opportunity.Amount;
            this.newCase.Opportunity__c = this.opportunity.Id;
            this.newCase.Campaign_Start_Date2__c = this.opportunity.ContractStartDate__c;
            this.newCase.Campaign_End_Date2__c = this.opportunity.ContractEndDate__c;
            this.newCase.Origin = 'Opportunity Record';
            QueueSobject queueObject = [Select q.QueueId From QueueSobject q where q.Queue.Name = 'Legal - IO queue'];
            if(queueObject != null){
                this.newCase.OwnerId = queueObject.QueueId;
            }
            //Use if you want the creater to be the owner
            //this.newCase.OwnerId = Userinfo.getUserId();
            this.newCase.Ad_Agency__c = this.opportunity.AccountPartners[0].AccountToId;
            RecordType recordType = [Select id from RecordType where Name='Legal AM to IO approval request'];
            if(recordType != null){
                this.newCase.RecordTypeId = recordType.id;
            }
            insert newCase;
            // redirecting the user back to the opportunity page.
            return new PageReference('/'+opportunity.Id);
        } catch (exception e){
            this.newCase.Advertiser__c = this.opportunity.Account.Name;
            //this.newCase.Total_IO_Amount__c = this.opportunity.Amount;
            this.newCase.Opportunity__c = this.opportunity.Id;
            this.newCase.Campaign_Start_Date2__c = this.opportunity.ContractStartDate__c;
            this.newCase.Campaign_End_Date2__c = this.opportunity.ContractEndDate__c;
            this.newCase.Origin = 'Opportunity Record';
            QueueSobject queueObject = [Select q.QueueId From QueueSobject q where q.Queue.Name = 'Legal - IO queue'];
            if(queueObject != null){
                this.newCase.OwnerId = queueObject.QueueId;
            }
            //Use if you want the creater to be the owner
            //this.newCase.OwnerId = Userinfo.getUserId();
            RecordType recordType = [Select id from RecordType where Name='Legal AM to IO approval request'];
            if(recordType != null){
                this.newCase.RecordTypeId = recordType.id;
            }
            insert newCase;
            // redirecting the user back to the opportunity page.
            return new PageReference('/'+opportunity.Id);
        }

    }
    
    public PageReference doCancel(){
        return new PageReference('/'+opportunity.Id);
    }
    
    
}