/**
* This is a general utility class for Case.
* @Author: Bharath Kumar Gadiyaram.
*/
public class IOAPR_CaseUtil {

    // This method updates two fields on related Opportunity based on the case field values.
    // Only cases belonging to 'Legal AM to IO approval' recordType are considered for this operation.
    // Used by CaseAfter trigger.
    public static void updateOpportunityFields(List<Case> caseList){
        Map<Id,Case> opportunityIdCaseMap = new Map<Id,Case>();
        
        // Fetching the record type to compare ids.
        Map<Id, RecordType> recordTypes = 
            new Map<Id,RecordType>([Select id, Name,DeveloperName from RecordType where Name ='Legal AM to IO approval request' 
                OR DeveloperName  = 'Legal_IO_Approval_Request']);
        
        for(Case caseObject:caseList) {
            if (caseObject.Opportunity__c == null) continue;
            if (recordTypes.keySet().contains(caseObject.RecordTypeId)) {
                opportunityIdCaseMap.put(caseObject.Opportunity__c,caseObject);
            }
        }
        // Retrieving related opportunities.
        List<Opportunity> opportunityList = 
            [Select id from Opportunity where id in :opportunityIdCaseMap.keySet()];
            
        // Iterating over opportunities and updating the fields.    
        for(Opportunity opportunity:opportunityList){
            Case caseObject = opportunityIdCaseMap.get(opportunity.Id);
            opportunity.Legal_IO_Approval_Status__c = caseObject.Status;
            opportunity.Finance_IO_Approval_Status__c = caseObject.Finance_status__c;
            
            Id caseObjectRT = caseObject.RecordTypeId;
            if (recordTypes.get(caseObjectRT) != null && recordTypes.get(caseObjectRT).DeveloperName == 'Legal_IO_Approval_Request')
            {
                opportunity.Legal_IO_Approval_Status__c = caseObject.Legal_approval_status__c;
                opportunity.Revenue_IO_Approval_Status__c = caseObject.Revenue_IO_Approval__c;
                opportunity.Sales_Operations_Approvals__c = caseObject.Sales_Operations_Approvals__c;
                opportunity.Billing_IO_Approval_Status__c = caseObject.Billing_approval_status__c;
                opportunity.Payment_IO_Approval_Status__c = caseObject.Payment_approval__c;
            }
            
            //added by Rohit - 10/17/2011
            opportunity.IO_Signature_Status__c = caseObject.IO_Signature_Status__c;             
            
            
            // The field Legal_IO_Approved__c on opportunity is set to true when the Case status is closed. 
            if(caseObject.Status == 'Closed'){
                opportunity.Legal_IO_Approved__c = true;
            }else{
                opportunity.Legal_IO_Approved__c = false;
            }
            // The field Finance_IO_Approved__c on opportunity is set to true when the Case Finance status is "Approved".   
            if(caseObject.Finance_status__c == 'Approved'){
                opportunity.Finance_IO_Approved__c = true;
            }else{
                opportunity.Finance_IO_Approved__c = false;
            }
        }
        update opportunityList;
    }
    
    public static Boolean hasChanges(Set<String> fieldNames, SObject oldRecord, SObject newRecord) {
        if (oldRecord == null) {
          return true;
        }
        Boolean changed = false;
        for (String field : fieldNames) {
          changed = (oldRecord.get(field) != newRecord.get(field));
          if (changed) break;
        }
        return changed;
    }
}