/*
Developer: Ralph Callaway <ralph@callawaycloudconsulting.com>
Description:
	Populate case account from related opportunity if not already popualted
*/
public class IOAPR_PopulateCaseAccount {

	/* Constants, Properties, and Variables */
	
	private List<Case> newList;
	
	public static final Set<String> RT_DEV_NAMES = new Set<String> {
		  'Broadcast_IO'
		, 'Campaign_Clearance' 
		, 'COGS_Approval_Create'
		, 'COGS_Approval_Details'
		, 'Legal_IO_Approval_Case_Simple_Entry' // IO Approval Case
		, 'Legal_IO_Approval_Request' // IO Approval Details
		, 'Legal_General_request'
		, 'Legal_NDA_request'
		, 'Logo_Permission_Request'
		, 'Logo_Permission_Request_Initial'
		, 'Yield_Approval_Request'
	};
	
	/* Constructor */
	
	public IOAPR_PopulateCaseAccount(List<Case> newList) {
		this.newList = newList;
	}
	
	/* Public Functions */
	
	// NB: This function is designed to be called from a before trigger
	// and does not make any explicit commits
	public void populate() {
		// Collect cases with no accounts and there related opportunities
		Set<Id> relatedOpptyIds = new Set<Id>();
		List<Case> noAccountCases = new List<Case>();
		for(Case record : newList) {
			String rtDevName = UTIL_CaseRecordTypeHelper.getRecordTypeDevName(record.recordTypeId);
			if(
				record.accountId == null 
			 && record.opportunity__c != null 
			 && RT_DEV_NAMES.contains(rtDevName)
			) {
				relatedOpptyIds.add(record.opportunity__c);
				noAccountCases.add(record);
			}
		}
		
		// Query opportunity accounts
		Map<Id, Opportunity> opptyMap = new Map<Id, Opportunity>([
			select accountId
			from Opportunity
			where id in :relatedOpptyIds
			and accountId != null
		]);
		
		// Populate case accounts
		for(Case record : noAccountCases) {
			if(opptyMap.containsKey(record.opportunity__c)) {
				record.accountId = opptyMap.get(record.opportunity__c).accountId;	
			}
		}
	}

}