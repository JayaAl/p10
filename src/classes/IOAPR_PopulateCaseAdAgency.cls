/*
Developer: Ralph Callaway <ralph@callawaycloudconsulting.com>
Description:
	Populates case ad agency from opporutnity if not already populated
*/
public class IOAPR_PopulateCaseAdAgency {

	/* Constants, Properties, and Variables */

	private List<Case> newList;
	
	public static final Set<String> RT_DEV_NAMES = new Set<String> {
		  'Broadcast_IO'
		, 'Legal_IO_Approval_Case_Simple_Entry' // IO Approval Case
		, 'Yield_Approval_Request'
	};
	
	
	/* Constructor */
	
	public IOAPR_PopulateCaseAdAgency(List<Case> newList) {
		this.newList = newList;
	}
	
	/* Public Functions */
	
	// NB: this method is intended to be called from a before trigger
	// and does not make any explicit commits
	public void populate() {
		
		// collect cases that don't have ad agency populated and store
		// their related opportunities
		Set<Id> refOpptyIds = new Set<Id>();
		List<Case> adAgencyNotPopulated = new List<Case>();
		for(Case record : newList) {
			String devRtName = UTIL_CaseRecordTypeHelper.getRecordTypeDevName(record.recordTypeId);
			if(
				record.ad_agency__c == null 
			 && record.opportunity__c != null
			 && RT_DEV_NAMES.contains(devRtName)
			) {
				adAgencyNotPopulated.add(record);
				refOpptyIds.add(record.opportunity__c);
			}
		}
		
		// query ad agency from related opptys
		Map<Id, Opportunity> opptyMap = new Map<Id, Opportunity>([
			select agency__c
			from Opportunity
			where id in :refOpptyIds
			and agency__c != null
		]);
		
		// populate ad agency on the case
		for(Case record : adAgencyNotPopulated) {
			if(opptyMap.containsKey(record.opportunity__c)) {
				record.ad_agency__c = opptyMap.get(record.opportunity__c).agency__c;
			}
		}
		
	}

}