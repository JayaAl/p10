/*
Developer: Ralph Callaway <ralph@callawaycloudconsulting.com>
Description:
	Test methods for IOAPR_PopulateCaseAdAgency.cls
*/
@isTest
private class IOAPR_PopulateCaseAdAgency_Test {

	/* Test Variables */
	
	static Account adAgency1;
	static Account adAgency2;
	static Case testCase;
	
	/* Before Test Actions */
	
	// create two ad agencies and generate a case with an opportunity
	// that has a related ad agency and has a record type matching
	// the trigger filters
	static {
		// create oppty account and ad agencies
		Account testAccount = UTIL_TestUtil.generateAccount();
		adAgency1 = UTIL_TestUtil.generateAccount();
		adAgency1.type = 'Ad Agency';
		adAgency2 = UTIL_TestUtil.generateAccount();
		adAgency2.type = 'Ad Agency';
		insert new Account[] { testAccount, adAgency1, adAgency2 };
		Opportunity testOpportunity = UTIL_TestUtil.generateOpportunity(testAccount.id);
		testOpportunity.agency__c = adAgency1.id;
		testOpportunity.Confirm_direct_relationship__c = false;
		insert testOpportunity;
		
		// generate case with record type that is available for running 
		// user and valid for trigger
		testCase = UTIL_TestUtil.generateCase();
		for(RecordTypeInfo rtInfo : UTIL_SchemaHelper.getRecordTypeInfos('Case')) {
			if(rtInfo.isAvailable()) {
				Id recordTypeId = rtInfo.getRecordTypeId();
				String devName = UTIL_CaseRecordTypeHelper.getRecordTypeDevName(recordTypeId);
				if(IOAPR_PopulateCaseAdAgency.RT_DEV_NAMES.contains(devName)) {
					testCase.recordTypeId = recordTypeId;
					break;
				}
			}
		}
		testCase.opportunity__c = testOpportunity.id;
		testCase.ad_agency__c = null;
		system.assertNotEquals(null, testCase.recordTypeId, 'None of the IOAPR_PopulateCaseAdAgency record types are available for running user');
	}
	
	/* Test Methods */

	@isTest
	private static void testPopulateAdAgency() {
		// insert case
		Test.startTest();
		insert testCase;
		Test.stopTest();
		
		// validate ad agency populated
		testCase = [select ad_agency__c from Case where id = :testCase.id];
		system.assertEquals(adAgency1.id, testCase.ad_agency__c);
	} 

	@isTest
	private static void testAdAgencyNotPopulatedIfAlreadySpecified() {
		// link case to an ad agency before insert
		testCase.ad_agency__c = adAgency2.id;
		
		// insert case
		Test.startTest();
		insert testCase;
		Test.stopTest();
		
		// validate ad agency not overwritten
		testCase = [select ad_agency__c from Case where id = :testCase.id];
		system.assertEquals(adAgency2.id, testCase.ad_agency__c);
	}
	
	@isTest
	private static void testAdAgencyNotPopulatedForNonSupportedRecordTypes() {
		// change case record type to one NOT supported by trigger
		Id initialRecordTypeId = testCase.recordTypeId;
		for(RecordTypeInfo rtInfo : UTIL_SchemaHelper.getRecordTypeInfos('Case')) {
			if(rtInfo.isAvailable()) {
				Id recordTypeId = rtInfo.getRecordTypeId();
				String devName = UTIL_CaseRecordTypeHelper.getRecordTypeDevName(recordTypeId);
				if(!IOAPR_PopulateCaseAdAgency.RT_DEV_NAMES.contains(devName)) {
					testCase.recordTypeId = recordTypeId;
					break;
				}
			}
		}
	
		// only test if we found a record type (it's possible that the trigger applies to all 
		// available record types which isn't a reason for this test to fail)
		if(testCase.recordTypeId != initialRecordTypeId) {
			
			Boolean exceptionThrown = false;
			
			// insert case
			Test.startTest();
			try {
				insert testCase;
			} catch(DMLException e) {
				exceptionThrown = true;
			}
			Test.stopTest();
			
			// bail if we get a dml exception given it's likely we'll add validation rules
			// to require account population for case record types specified in this trigger
			if(!exceptionThrown) { 
			
				// validate account not populated
				testCase = [select ad_agency__c from Case where id = :testCase.id];
				system.assertEquals(null, testCase.ad_agency__c);
				
			}
		}
	}

}