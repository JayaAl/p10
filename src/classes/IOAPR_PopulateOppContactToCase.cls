/*
Class Name: IOAPR_PopulateOppContactToCase
Author: Vardhan Gupta.


Description:
    Populates "Primary" Contact from Related Opportunity from the Contact Roles related list.
*/

public class IOAPR_PopulateOppContactToCase {

        /* Constants, Properties, and Variables */

    private List<Case> newList;
    
    public static final Set<String> RT_DEV_NAMES = new Set<String> {
          'Legal_IO_Approval_Case_Simple_Entry' // IO Approval Case
        , 'Legal_IO_Approval_Request' // IO Approval Details
    };
    
    
    /* Constructor */
    
    public IOAPR_PopulateOppContactToCase(List<Case> newList) {
        this.newList = newList;
    }
    
    /* Public Functions */
    
    // NB: this method is intended to be called from a before trigger
    // and does not make any explicit commits
    public void populate() {
        
        // collect cases that don't have ad agency populated and store
        // their related opportunities
        Set<Id> refOpptyIds = new Set<Id>();
        Map<ID, Contact> oppPrimaryContacts = new Map<ID, Contact>();
        List<OpportunityContactRole> CRs = new List<OpportunityContactRole>();
        List<Id> primaryContactIDs = new List<Id>();
        Map<ID, ID> opptyContactMapIDs = new Map<ID, ID>();
        ID accountID_related_to_contact;
        
        for(Case record : newList) {
            String devRtName = UTIL_CaseRecordTypeHelper.getRecordTypeDevName(record.recordTypeId);
            if(
                record.opportunity__c != null
             && RT_DEV_NAMES.contains(devRtName)
            ) {
                refOpptyIds.add(record.opportunity__c);
            }
        }
        
        if (refOpptyIds.size() > 0) {
        CRs = [select OpportunityId, ContactId, Role, IsPrimary from OpportunityContactRole where role = 'Billing Contact' and OpportunityId in :refOpptyIds limit 1];
        }
        
        if(CRs.size() > 0) {
        
        System.debug('---Contact Roles List Size := ' + CRs.size());
                    
            for(integer i = 0; i < CRs.size(); i++) {
                 primaryContactIDs.add(CRs[i].contactID);
                opptyContactMapIDs.put(CRs[i].OpportunityID, CRs[i].contactID);
            }
          }
        

       
        if (primaryContactIDs.size() > 0){
            for(Contact con:[select ID, AccountId, FirstName, LastName, Title, Email, Fax, Phone, MailingStreet, MailingCity, MailingState, MailingCountry, MailingPostalCode from Contact where ID in :primaryContactIDs])
            oppPrimaryContacts.put(con.ID, con);
        }
        
       
        
        // Populate the related contact on the case
        //Check if the case is for the related opportunity and the billing contact is associated with the 
        
        if (opptyContactMapIDs.size() > 0){
        for(Case record : newList) {
            
            accountID_related_to_contact = oppPrimaryContacts.get(opptyContactMapIDs.get(record.opportunity__c)).AccountID;
            
            if(opptyContactMapIDs.containsKey(record.opportunity__c) && 
            (record.AccountId == accountID_related_to_contact || record.Ad_Agency__c == accountID_related_to_contact))
            {
            
System.debug('---Contact First Name :=' + oppPrimaryContacts.get(opptyContactMapIDs.get(record.opportunity__c)).FirstName);  

             
            record.Billing_Contact__c =  (oppPrimaryContacts.get(opptyContactMapIDs.get(record.opportunity__c)).FirstName != null ? oppPrimaryContacts.get(opptyContactMapIDs.get(record.opportunity__c)).FirstName + ' ' : '') + 
               (oppPrimaryContacts.get(opptyContactMapIDs.get(record.opportunity__c)).LastName != null ? oppPrimaryContacts.get(opptyContactMapIDs.get(record.opportunity__c)).LastName + '\n' : '') +
               (oppPrimaryContacts.get(opptyContactMapIDs.get(record.opportunity__c)).Title != null ? oppPrimaryContacts.get(opptyContactMapIDs.get(record.opportunity__c)).Title + '\n' : '') +
               (oppPrimaryContacts.get(opptyContactMapIDs.get(record.opportunity__c)).MailingStreet != null ? oppPrimaryContacts.get(opptyContactMapIDs.get(record.opportunity__c)).MailingStreet + '\n' : '') +
                (oppPrimaryContacts.get(opptyContactMapIDs.get(record.opportunity__c)).MailingCity != null ? oppPrimaryContacts.get(opptyContactMapIDs.get(record.opportunity__c)).MailingCity + ',' : '') +
                (oppPrimaryContacts.get(opptyContactMapIDs.get(record.opportunity__c)).MailingState != null ? oppPrimaryContacts.get(opptyContactMapIDs.get(record.opportunity__c)).MailingState + ',' : '') +
                 (oppPrimaryContacts.get(opptyContactMapIDs.get(record.opportunity__c)).MailingPostalCode  != null ? oppPrimaryContacts.get(opptyContactMapIDs.get(record.opportunity__c)).MailingPostalCode  + '\n' : '') +
                (oppPrimaryContacts.get(opptyContactMapIDs.get(record.opportunity__c)).MailingCountry != null ? oppPrimaryContacts.get(opptyContactMapIDs.get(record.opportunity__c)).MailingCountry  + '\n' : '') +
                 (oppPrimaryContacts.get(opptyContactMapIDs.get(record.opportunity__c)).Email != null ? oppPrimaryContacts.get(opptyContactMapIDs.get(record.opportunity__c)).Email + '\n' : '') +
               (oppPrimaryContacts.get(opptyContactMapIDs.get(record.opportunity__c)).Phone != null ? oppPrimaryContacts.get(opptyContactMapIDs.get(record.opportunity__c)).Phone + '\n' : '');

            }
            else {
                //record.Billing_Contact__c = ' <img src="/img/samples/color_red.gif" alt="red" height="30" width="30" border="0"/>' ;
           
                record.Billing_Contact__c = '';
            }
            
            
            //if(opptyMap.containsKey(record.opportunity__c)) {
                //record.ad_agency__c = opptyMap.get(record.opportunity__c).agency__c;
            }
        
    }
        
        
        }
        
    }