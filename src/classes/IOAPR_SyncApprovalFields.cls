/*
Developer: Ralph Callaway <ralph@callawaycloudconsulting.com>
Description:
	Sync changes to case approval fields to the related opportunity
*/
public class IOAPR_SyncApprovalFields {

	/* Constants, Properties, and Variables */
	
	private List<Case> newList;
	private Map<Id, Case> oldMap;
	
	public static final Set<String> RT_DEV_NAMES = new Set<String> {
		  'Broadcast_IO'
		, 'Legal_IO_Approval_Request' // IO Approval Details
	};
	
	/* Constructor */
	
	public IOAPR_SyncApprovalFields(List<Case> newList, Map<Id, Case> oldMap) {
		this.newList = newList;
		this.oldMap = oldMap;
	}
	
	/* Public Functions */
	
	// NB: This method should be called from after triggers so that workflows
	// have had a chance to update the approval fields
	public void sync() {
		
		// Collect cases that have changed approval fields
		List<Case> changedCases = new List<Case>();
		for(Case record : newList) {
			Case oldCase = (oldMap != null) ? oldMap.get(record.id) : null;
			String rtDevName = UTIL_CaseRecordTypeHelper.getRecordTypeDevName(record.recordTypeId);
			if(
				record.opportunity__c != null
			 && RT_DEV_NAMES.contains(rtDevName)
			 && (
			 		oldCase == null // indicates an insert
			 	||	(
			 			record.Billing_approval_status__c != oldCase.Billing_approval_status__c
			 		 || record.IO_Signature_Status__c != oldCase.IO_Signature_Status__c
			 		 || record.Legal_Approval_Status__c != oldCase.Legal_Approval_Status__c
			 		 || record.Payment_approval__c != oldCase.Payment_approval__c
			 		 || record.Revenue_IO_Approval__c != oldCase.Revenue_IO_Approval__c
			 		 || record.Sales_Operations_Approvals__c != oldCase.Sales_Operations_Approvals__c 
			 		)
			 	)
			) {
				changedCases.add(record);
			}
		}
		
		// Create update opportunitie.  Organize by id on the off chance more than one io approval 
		// case is related to a particular opportunity
		Map<Id, Opportunity> opptyMap = new Map<Id, Opportunity>();
		for(Case record : changedCases) {
			opptyMap.put(record.opportunity__c, new Opportunity(
				  id = record.opportunity__c
				, Billing_IO_Approval_Status__c = record.Billing_approval_status__c
				, IO_Signature_Status__c = record.IO_Signature_Status__c
				, Legal_IO_Approval_Status__c = record.Legal_approval_status__c
				, Payment_IO_Approval_Status__c = record.Payment_approval__c
				, Revenue_IO_Approval_Status__c = record.Revenue_IO_Approval__c
				, Sales_Operations_Approvals__c = record.Sales_Operations_Approvals__c
			));
		} 
		
		// commit to database
		update opptyMap.values();
	}

}