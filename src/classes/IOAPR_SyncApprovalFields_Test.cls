/*
Developer: Ralph Callaway <ralph@callawaycloudconsulting.com>
Description:
	Test methods for IOAPR_SyncApprovalFields.cls
*/
@isTest 
private class IOAPR_SyncApprovalFields_Test {

	/* Test Variables */
	
	static Opportunity testOppty;
	static Case testCase;
	static String value1 = UTIL_TestUtil.generateRandomString(3);
	static String value2 = UTIL_TestUtil.generateRandomString(3);
	static String value3 = UTIL_TestUtil.generateRandomString(3);
	
	/* Before Test Actions */
	
	// Create an opportunity with all approval fields set to a random 
	// intial value (value1), then generate a related case with 
	// a record type matching the trigger filters, and all approval fields set
	// to a random initial value (value2)
	static {
		Account testAccount = UTIL_TestUtil.createAccount();
		testOppty = UTIL_TestUtil.generateOpportunity(testAccount.id);
		testOppty.Billing_IO_Approval_Status__c = value1;
		testOppty.IO_Signature_Status__c = value1;
		testOppty.Legal_IO_Approval_Status__c = value1;
		testOppty.Payment_IO_Approval_Status__c = value1;
		testOppty.Revenue_IO_Approval_Status__c = value1;
		testOppty.Sales_Operations_Approvals__c = value1;
		insert testOppty;
		
		// generate case with record type that is available for running 
		// user and valid for trigger
		testCase = UTIL_TestUtil.generateCase();
		for(RecordTypeInfo rtInfo : UTIL_SchemaHelper.getRecordTypeInfos('Case')) {
			if(rtInfo.isAvailable()) {
				Id recordTypeId = rtInfo.getRecordTypeId();
				String devName = UTIL_CaseRecordTypeHelper.getRecordTypeDevName(recordTypeId);
				if(IOAPR_SyncApprovalFields.RT_DEV_NAMES.contains(devName)) {
					testCase.recordTypeId = recordTypeId;
					break;
				}
			}
		}
		testCase.opportunity__c = testOppty.id;
		system.assertNotEquals(null, testCase.recordTypeId, 'None of the IOAPR_PopulateCaseAdAgency record types are available for running user');
		testCase.Billing_approval_status__c = value2;
		testCase.IO_Signature_Status__c = value2;
		testCase.Legal_approval_status__c = value2;
		testCase.Payment_approval__c = value2;
		testCase.Revenue_IO_Approval__c = value2;
		testCase.Sales_Operations_Approvals__c = value2;
	}
	
	/* Test Methods */

	@isTest
	private static void testSyncOnInsert() {
		// insert case
		Test.startTest();
		insert testCase;
		Test.stopTest();
		
		// validate approval fields got synced
		assertAllSyncedOpptyFields(value2);
	}
	
	@isTest
	private static void testBillingSyncOnUpdate() {
		// insert case
		insert testCase;
		
		// update billing field
		Test.startTest();
		testCase.Billing_approval_status__c = value3;
		update testCase;
		Test.stopTest();
		
		// assert only the billing field changed
		assertOneSyncedOpptyField('Billing_IO_Approval_Status__c', value3, value2);
	}
	
	@isTest 
	private static void testIOSigStatusSyncOnUpdate() {
		// insert case
		insert testCase;
		
		// update io signature status field
		Test.startTest();
		testCase.IO_Signature_Status__c = value3;
		update testCase;
		Test.stopTest();
		
		// assert only the io signature status field changed
		assertOneSyncedOpptyField('IO_Signature_Status__c', value3, value2);
	}
	
	@isTest 
	private static void testLegalSyncOnUpdate() {
		// insert case
		insert testCase;
		
		// update legal field
		Test.startTest();
		testCase.Legal_approval_status__c = value3;
		update testCase;
		Test.stopTest();
		
		// assert only the legal field changed
		assertOneSyncedOpptyField('Legal_IO_Approval_Status__c', value3, value2);
	}
	
	@isTest 
	private static void testPaymentSyncOnUpdate() {
		// insert case
		insert testCase;
		
		// update payment field
		Test.startTest();
		testCase.Payment_approval__c = value3;
		update testCase;
		Test.stopTest();
		
		// assert only the payment field changed
		assertOneSyncedOpptyField('Payment_IO_Approval_Status__c', value3, value2);
	}
	
	@isTest 
	private static void testRevenueSyncOnUpdate() {
		// insert case
		insert testCase;
		
		// update revenue field
		Test.startTest();
		testCase.Revenue_IO_Approval__c = value3;
		update testCase;
		Test.stopTest();
		
		// assert only the revenue field changed
		assertOneSyncedOpptyField('Revenue_IO_Approval_Status__c', value3, value2);
	}
	
	@isTest 
	private static void testSalesOpSyncOnUpdate() {
		// insert case
		insert testCase;
		
		// update sales op field
		Test.startTest();
		testCase.Sales_Operations_Approvals__c = value3;
		update testCase;
		Test.stopTest();
		
		// assert only the sales op field changed
		assertOneSyncedOpptyField('Sales_Operations_Approvals__c', value3, value2);
	}
	
	@isTest 
	private static void testNoSyncForOtherRecordTypes() {
		// change case record type to one NOT supported by trigger
		Id initialRecordTypeId = testCase.recordTypeId;
		for(RecordTypeInfo rtInfo : UTIL_SchemaHelper.getRecordTypeInfos('Case')) {
			if(rtInfo.isAvailable()) {
				Id recordTypeId = rtInfo.getRecordTypeId();
				String devName = UTIL_CaseRecordTypeHelper.getRecordTypeDevName(recordTypeId);
				if(!IOAPR_SyncApprovalFields.RT_DEV_NAMES.contains(devName)) {
					testCase.recordTypeId = recordTypeId;
					break;
				}
			}
		}
		
		// only test if we found a record type (it's possible that the trigger applies to all 
		// available record types which isn't a reason for this test to fail)
		if(testCase.recordTypeId != initialRecordTypeId) {
			
			Boolean exceptionThrown = false;
			
			// insert case
			Test.startTest();
			try {
				insert testCase;
			} catch(DMLException e) {
				exceptionThrown = true;
			}
			Test.stopTest();
	
			// bail if we get a dml exception given it's likely we'll add validation rules
			// to require account population for case record types specified in this trigger
			if(!exceptionThrown) {
			
				// validate oppty still has initial values
				assertAllSyncedOpptyFields(value1);
			}
		}
	}

	/* Test Helpers */
	
	private static void assertAllSyncedOpptyFields(String expectedValue) {
		testOppty = reQueryOppty();
		system.assertEquals(expectedValue, testOppty.Billing_IO_Approval_Status__c);
		system.assertEquals(expectedValue, testOppty.IO_Signature_Status__c);
		system.assertEquals(expectedValue, testOppty.Legal_IO_Approval_Status__c);
		system.assertEquals(expectedValue, testOppty.Payment_IO_Approval_Status__c);
		system.assertEquals(expectedValue, testOppty.Revenue_IO_Approval_Status__c);
		system.assertEquals(expectedValue, testOppty.Sales_Operations_Approvals__c);
	}
	
	private static void assertOneSyncedOpptyField(String fieldName, String expectedValue, String otherExpectedValue) {
		testOppty = reQueryOppty();
		if(fieldName.equalsIgnoreCase('Billing_IO_Approval_Status__c')) {
			system.assertEquals(expectedValue, testOppty.Billing_IO_Approval_Status__c);
		} else {
			system.assertEquals(otherExpectedValue, testOppty.Billing_IO_Approval_Status__c);
		}
		if(fieldName.equalsIgnoreCase('IO_Signature_Status__c')) {
			system.assertEquals(expectedValue, testOppty.IO_Signature_Status__c);
		} else {
			system.assertEquals(otherExpectedValue, testOppty.IO_Signature_Status__c);
		}
		if(fieldName.equalsIgnoreCase('Legal_IO_Approval_Status__c')) {
			system.assertEquals(expectedValue, testOppty.Legal_IO_Approval_Status__c);
		} else {
			system.assertEquals(otherExpectedValue, testOppty.Legal_IO_Approval_Status__c);
		}
		if(fieldName.equalsIgnoreCase('Payment_IO_Approval_Status__c')) {
			system.assertEquals(expectedValue, testOppty.Payment_IO_Approval_Status__c);
		} else {
			system.assertEquals(otherExpectedValue, testOppty.Payment_IO_Approval_Status__c);
		}
		if(fieldName.equalsIgnoreCase('Revenue_IO_Approval_Status__c')) {
			system.assertEquals(expectedValue, testOppty.Revenue_IO_Approval_Status__c);
		} else {
			system.assertEquals(otherExpectedValue, testOppty.Revenue_IO_Approval_Status__c);
		}
		if(fieldName.equalsIgnoreCase('Sales_Operations_Approvals__c')) {
			system.assertEquals(expectedValue, testOppty.Sales_Operations_Approvals__c);
		} else {
			system.assertEquals(otherExpectedValue, testOppty.Sales_Operations_Approvals__c);
		}
	}
	
	private static Opportunity reQueryOppty() {
		return [
			select
				  Billing_IO_Approval_Status__c
				, IO_Signature_Status__c
				, Legal_IO_Approval_Status__c
				, Payment_IO_Approval_Status__c
				, Revenue_IO_Approval_Status__c
				, Sales_Operations_Approvals__c
			from Opportunity
			where id = :testOppty.id
		];
	}

}