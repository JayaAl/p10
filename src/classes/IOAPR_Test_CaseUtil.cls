/**
 * This class contains unit tests for validating the behavior of Apex classes
 * and triggers.
 *
 * Unit tests are class methods that verify whether a particular piece
 * of code is working properly. Unit test methods take no arguments,
 * commit no data to the database, and are flagged with the testMethod
 * keyword in the method definition.
 *
 * All test methods in an organization are executed whenever Apex code is deployed
 * to a production organization to confirm correctness, ensure code
 * coverage, and prevent regressions. All Apex classes are
 * required to have at least 75% code coverage in order to be deployed
 * to a production organization. In addition, all triggers must have some code coverage.
 * 
 * The @isTest class annotation indicates this class only contains test
 * methods. Classes defined with the @isTest annotation do not count against
 * the organization size limit for all Apex scripts.
 *
 * See the Apex Language Reference for more information about Testing and Code Coverage.
 */
@isTest
private class IOAPR_Test_CaseUtil {
/**
    static testMethod void ioReviewInsert() {
       Opportunity o = new Opportunity();
        o.Finance_IO_Approval_Status__c = 'Empty';
        o.Finance_IO_Approved__c = true;
        o.Legal_IO_Approval_Status__c = 'Empty';
        o.Legal_IO_Approved__c = true;
        o.Name = 'testOpty';
        o.StageName = 'Pending';
        o.CloseDate = Date.today().addDays(7);
        insert o;
        RecordType rt = [Select id from RecordType where Name='Legal AM to IO approval request'];
        System.assertEquals('0124000000013no', rt.id);
        System.debug('this is the record type: ' + rt.id);
        Case c = new Case(recordTypeID=rt.id, Opportunity__c=o.Id, Subject='testCase', Finance_status__c='Pending Approval', status='Pending Approval');
        insert c;
        Opportunity newO = [Select Legal_IO_Approved__c, Finance_IO_Approved__c, Legal_IO_Approval_Status__c, Finance_IO_Approval_Status__c from Opportunity where id = :o.id];
        System.debug('this is the o id: ' + o.id);
        System.assertEquals(false, newO.Legal_IO_Approved__c);
        System.assertEquals(false, newO.Finance_IO_Approved__c);
    }
**/
}