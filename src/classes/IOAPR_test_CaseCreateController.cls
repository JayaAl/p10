/**
 * This class contains unit tests for validating the behavior of Apex classes
 * and triggers.
 *
 * Unit tests are class methods that verify whether a particular piece
 * of code is working properly. Unit test methods take no arguments,
 * commit no data to the database, and are flagged with the testMethod
 * keyword in the method definition.
 *
 * All test methods in an organization are executed whenever Apex code is deployed
 * to a production organization to confirm correctness, ensure code
 * coverage, and prevent regressions. All Apex classes are
 * required to have at least 75% code coverage in order to be deployed
 * to a production organization. In addition, all triggers must have some code coverage.
 * 
 * The @isTest class annotation indicates this class only contains test
 * methods. Classes defined with the @isTest annotation do not count against
 * the organization size limit for all Apex scripts.
 *
 * See the Apex Language Reference for more information about Testing and Code Coverage.
 */
@isTest
private class IOAPR_test_CaseCreateController {

    public static testMethod void myUnitTest() {

        //PageReference pageRef = Page.ShowCaseForm;
        //Test.setCurrentPage(pageRef);
        IOAPR_CaseCreateController controller = new IOAPR_CaseCreateController();
        
        //Example of calling an Action method. Same as calling any other Apex method. 
        //Normally this is executed by a user clicking a button or a link from the Visualforce
        //page, but in the test method, just test the action method the same as any 
        //other method by calling it directly. 

        //The .getURL will return the page url the Save() method returns.
        //String nextPage = controller.dosave().getUrl();

        //Check that the save() method returns the proper URL.
        //System.assertEquals('/apex/failure?error=noParam', nextPage);
        

        Opportunity opty = new Opportunity( name='OptyTest', StageName='Prospecting', CloseDate=System.today().addDays(7));
        insert opty;
        Case nc = new Case(RecordTypeId = '0124000000013no', Governing_Terms__c='Net 30', General_Terms_Comments__c='foo', CGD_options__c='bar');
        //insert nc;
      
        controller = new IOAPR_CaseCreateController();
        //pageRef = controller.doSave();
        controller.newCase = nc;
        controller.opportunity = opty;
        PageReference expectedPR=new PageReference('/'+opty.Id);
        System.assertEquals(expectedPr.getUrl(), controller.doSave().getUrl());
        RecordType recordType = [Select id from RecordType where Name='Legal AM to IO approval request'];
        System.assertEquals(recordType.id, nc.RecordTypeId);    
        System.assertEquals(expectedPr.getUrl(), controller.doCancel().getUrl());
    }
    public static testMethod void emptyMethodTest() {

        //PageReference pageRef = Page.ShowCaseForm;
        //Test.setCurrentPage(pageRef);
        IOAPR_CaseCreateController controller = new IOAPR_CaseCreateController();
        
        //Example of calling an Action method. Same as calling any other Apex method. 
        //Normally this is executed by a user clicking a button or a link from the Visualforce
        //page, but in the test method, just test the action method the same as any 
        //other method by calling it directly. 

        //The .getURL will return the page url the Save() method returns.
        //String nextPage = controller.dosave().getUrl();

        //Check that the save() method returns the proper URL.
        //System.assertEquals('/apex/failure?error=noParam', nextPage);
        

        controller = null;
        //pageRef = controller.doSave();
    }
}