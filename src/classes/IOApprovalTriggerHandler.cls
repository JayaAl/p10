/**
 * @name : IOApprovalTriggerHandler 
 * @desc : Handler for IO Approval DML operations
 * @version : 1.0
 * @date : 10/10/2014
 * @author : Lakshman(sfdcace@gmail.com)
 */
public class IOApprovalTriggerHandler {
    
    /**
     * SOQL Used: 1
     */
    
    public static void handleCurrencyForInsertOperation(List<IO_Detail__c> newList) {
        Set<Id> setOfOpp = new Set<Id>();
        for(IO_Detail__c aIO: newList) {
            setOfOpp.add(aIO.Opportunity__c);//Capture Opportunity Id's
        }

        Map<Id, Opportunity> mapOpoortunity = new Map<Id, Opportunity>([Select Id, CurrencyISOCode from Opportunity where Id =: setOfOpp ]);//Query CurrencyISOCode and store in Map
        for(IO_Detail__c aIO: newList) {
            aIO.CurrencyISOCode = mapOpoortunity.get(aIO.Opportunity__c).CurrencyISOCode;//Copy Opportunity CurrencyISOCode
        }
    }
}