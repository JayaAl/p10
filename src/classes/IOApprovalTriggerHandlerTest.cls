@isTest
public class IOApprovalTriggerHandlerTest {
	static testMethod void testHandleCurrencyForInsertOperation(){
    	Account acc = UTIL_TestUtil.createAccount();
        Opportunity opp = UTIL_TestUtil.createOpportunity(acc.Id);
        
		IO_Detail__c io = new IO_Detail__c();  
        io.Opportunity__c = opp.Id;  
        io.CC_Authorized__c = true;
        io.Payment_Terms_Preferred__c = 'Pre-Pay';
        
        insert io ;
		
    }
}