/*
Developer: Ralph Callaway <ralph@callawaycloudconsulting.com>
Description:
	Business logic for populating case legal 1st reponse user and
	legal 1st response date on the first update or case comment
	by a user with a legal role.
*/
public class IOLGL_PopulateLegalFirstResponse {

	/* Constants, Properties, and Variables */

	public static final String LEGAL_ROLE_DEV_NAME = 'Legal';

	private Set<Id> caseIds;
	private List<Case> newList;
	
	/* Constructors */
	
	public IOLGL_PopulateLegalFirstResponse(List<Case> newList) {
		this.newList = newList;
	}
	
	public IOLGL_PopulateLegalFirstResponse(List<CaseComment> newList) {
		caseIds = new Set<Id>();
		for(CaseComment comment : newList) {
			caseIds.add(comment.parentId);
		}
	}
	
	/* Public Functions */
	
	// NB: This function behaves differently depending on whether it's initiated
	// from a case comment trigger or a case trigger.  For the case trigger we 
	// expect to be called from a before update trigger and won't make any explicit
	// database commits.  For the case comment trigger it's best to call from an 
	// after trigger (although before should work as well), and we will be making
	// an explicit update call.
	public void populate() {
		if(currentUserHasLegalRole()) {
			
			// did we start from a case comment triger?
			if(caseIds != null) {
				
				// query all cases with blank first response dates and
				// set the first response date and first response user
				newList = [
					select Legal_1st_Response_Date__c
					from Case
					where id in :caseIds
					and Legal_1st_Response_Date__c = null
				];				
			} 
			
			// set the legal first repsonse date and first reponse
			// user if the legal first response date is blank
			for(Case record : newList) {
				if(record.Legal_1st_Response_Date__c == null) {
					record.Legal_1st_Response_Date__c = DateTime.now();
					record.X1st_Response_User__c = UserInfo.getUserId();
				}
			}
			
			// commit back to the database if we started from a case comment trigger
			if(caseIds != null) {
				update newList;
			}
		}
	}
	
	/* Private Functions */

	private static Boolean currentUserHasLegalRole() {
		for(UserRole userRole : [
			select id 
			from UserRole
			where id = :UserInfo.getUserRoleId()
			and developerName = :LEGAL_ROLE_DEV_NAME
		]) {
			return true;
		}
		return false;
	}
}