/*
Developer: Ralph Callaway <ralph@callawaycloudconsulting.com>
Description: 
	Test methods for IOLGL_PopulateLegalFirstResponse.cls
*/
@isTest 
private class IOLGL_PopulateLegalFirstResponse_Test {

	/* Test Variables */

	static User legalUser;
	static User nonLegalUser;
	static DateTime refDateTime = DateTime.now();
	static Case testCase;
	static CaseComment testComment;

	/* Before Test Actions */

	// Creates two users, one with a legal role, and one with no role.
	// If we can't find a legal role the test will fail.  Create a case
	// with a blank first reponse date, and generate a test comment for
	// that case.  NB: take care that creating non legal user is done 
	// in a system.runAs statement to avoid MIXED_DML errors.
	static {
		// Create test case and generate case comment
		testCase = UTIL_TestUtil.generateCase();
		testCase.Legal_1st_Response_Date__c = null;
		testCase.X1st_Response_User__c = null;
		insert testCase;
		testComment = new CaseComment(
			  parentId = testCase.id
			, commentBody = 'Required'
		);
		
		// Create users
		nonLegalUser = UTIL_TestUtil.generateUser();
		legalUser = UTIL_TestUtil.generateUser();
		insert new User[] { nonLegalUser, legalUser };
		
		// Set legal user role in runAs to avoid mixed dml.
		system.runAs(nonLegalUser) {
			for(UserRole userRole : [
				select id
				from UserRole
				where developerName = :IOLGL_PopulateLegalFirstResponse.LEGAL_ROLE_DEV_NAME
			]) {
				legalUser.userRoleId = userRole.id;
			}
			update legalUser;
		}
		
		// Fail test if we couldn't find the legal role
		system.assertNotEquals(null, legalUser.userRoleId); 
		
		// Strip the milliseconds from the refDateTime to accomodate
		// comparison with date times returned from the database which
		// don't have millisecond precision
		refDateTime = DateTime.newInstance(
			  refDateTime.year()
			, refDateTime.month()
			, refDateTime.day()
			, refDateTime.hour()
			, refDateTime.minute()
			, refDateTime.second()
		);
	}

	/* Test Methods */

	@isTest 
	private static void testPopulateLegalResponseOnLegalCaseUpdate() {
		// validate initial conditions
		assertLegalResponseNotPopulated();
		
		// update case as legal user
		system.runAs(legalUser) {
			Test.startTest();
			update testCase;
			Test.stopTest();
		}
		
		// validate legal response populated
		assertLegalResponsePopulated();
	}
	
	@isTest
	private static void testLegalResponseNotPopulateOnNonLegalCaseUpdate() {
		// validate initial conditions
		assertLegalResponseNotPopulated();
		
		// update case as non-legal user
		system.runAs(nonLegalUser) {
			Test.startTest();
			update testCase;
			Test.stopTest();
		}
		
		// validate legal response NOT populated
		assertLegalResponseNotPopulated();
	}
	
	@isTest
	private static void testPopulateLegalResponseOnLegalCaseCommentInsert() {
		// validate initial conditions
		assertLegalResponseNotPopulated();
		
		// insert case comment as legal user
		system.runAs(legalUser) {
			Test.startTest();
			insert testComment;
			Test.stopTest();
		}
		
		// validate legal response populated
		assertLegalResponsePopulated();
	}
	
	@isTest
	private static void testLegalResponseNotPopulatedOnNonLegalCommentInsert() {
		// validate initial conditions
		assertLegalResponseNotPopulated();
		
		// insert case comment as non-legal user
		system.runAs(nonLegalUser) {
			Test.startTest();
			insert testComment;
			Test.stopTest();
		}
		
		// validate legal response not populated
		assertLegalResponseNotPopulated();
	}

	/* Test Helpers */
	
	private static void assertLegalResponseNotPopulated() {
		requeryCase();
		system.assertEquals(null, testCase.Legal_1st_Response_Date__c);
		system.assertEquals(null, testCase.X1st_Response_User__c);
	}
	
	private static void assertLegalResponsePopulated() {
		requeryCase();
		system.assertNotEquals(null, testCase.Legal_1st_Response_Date__c);
		system.assert(testCase.Legal_1st_Response_Date__c >= refDateTime);
		system.assertEquals(legalUser.id, testCase.X1st_Response_User__c);
	}
	
	private static void requeryCase() {
		testCase = [
			select Legal_1st_Response_Date__c, X1st_Response_User__c
			from Case
			where id = :testCase.id
		];
	}

}