@isTest
private class IOLGL_Test_CaseLegalStatus{
    static testmethod void testCase(){
        Case c = new Case(Legal_approval_status__c ='Pending legal', Paperwork_Origin__c = 'Pandora Paperwork - with changes');
        insert c;
        c.Legal_approval_status__c = 'Final IO Approved';
        update c; 
        CaseComment cs = new CaseComment (CommentBody = 'Test', ParentId = c.Id);
        insert cs; 
    }
}