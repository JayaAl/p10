public class IO_UpdatePaymentApprvlStatusOnCase {


    private Set<Id> oppIds = new Set<Id>();
    private List<Case> caseList = new List<Case>();
     private List<Case> caseUpdateList = new List<Case>();
    
        public static final Set<String> RT_CASE_NAMES = new Set<String> {
          'Legal_IO_Approval_Case_Simple_Entry' // IO Approval Case
        , 'Legal_IO_Approval_Request' // IO Approval Details
    };
    
         /* Constructor */
    
    public IO_UpdatePaymentApprvlStatusOnCase (Set<Id> oppIds) {
     
        this.oppIds = oppIds;
    }

//If the IO Case is either closed or the Contact & Order Properties approval status is set to 'Final IO Approved' then change the status to Open
  public void updateCases (Set<Id> oppIds) {
    
    if (caseList.size() == 0) caseList =[Select id,recordTypeId, Payment_approval__c, Status, Billing_Type__c,Billing_Contact__c, Ad_Agency__c, AccountId, opportunity__c  From Case  where Opportunity__c in:oppIds and recordTypeId = '0124000000014lw'];

      
       if(caseList.size()> 0 ) {
                 for(Case record : caseList) {
                     String rtDevName = UTIL_CaseRecordTypeHelper.getRecordTypeDevName(record.recordTypeId);
                     
                     System.debug('--- In IO_UpdatePaymentApprvlStatusOnCase - CONTROLLER - recordTypeName := ' + rtDevName + ' record.Status = ' +record.Status + ' record.Payment_approval__c = ' + record.Payment_approval__c);  
                     
                     if (RT_CASE_NAMES.contains(rtDevName) && 
                            ( (record.Status != null && record.Status.equalsIgnoreCase('Closed')) || 
                              (record.Payment_approval__c != null && record.Payment_approval__c.equalsIgnoreCase('Final IO Approved')) //updated by VG 2/1/2013
                            ) 
                        ) {
                     record.Status = 'Open';
                     record.Payment_approval__c = 'Pending payment approval';    
                     }
                     
                     caseUpdateList.add(record);
                     
                     } //end of FOR loop 
       
       } //end of CASELIST if Block
      
      
      //Get the list of IO Cases to update.
       if(caseUpdateList != null && caseUpdateList.size()>0){
           try{
               update caseUpdateList; 
               
               System.debug('--- In IO_UpdatePaymentApprvlStatusOnCase - CONTROLLER - caseUpdateList :=' + caseUpdateList.size());  
           }catch(Exception e){
               System.debug('In Catch'+e.getmessage());    
           }
       } 
      
      
} //end of updateCases method
    

} //end of IO_UpdatePaymentApprvlStatusOnCase classe