/**
* ─────────────────────────────────────────────────────────────────────────────────────────────────┐
* This class is  .
* ──────────────────────────────────────────────────────────────────────────────────────────────────
* @author         Jaya Alaparthi
* @maintainedBy   Jaya Alaparthi
* @version        1.0
* @created        2017-10-05
* @modified       
* @systemLayer    Util
* @see            ????
* @see            ????
* ──────────────────────────────────────────────────────────────────────────────────────────────────
* @changes
* v1.1            Jaya Alaparthi
* 2017-10-05      
* ──────────────────────────────────────────────────────────────────────────────────────────────────
* @changes
* v1.2            Jaya Alaparthi
* 2017-06-06      
* ─────────────────────────────────────────────────────────────────────────────────────────────────┘
*/
public with sharing class ISTaskEmail {
	
	// triggering from Contact TriggerHandler
    public static void updateTaskEmail(map<Id,Contact> newContactsMap,
    									map<Id,Contact> oldContactsMap) {

    	map<Id,String> contactIdToOldNewEmailMap = new map<Id,String>();
    	String taskRecordTypeID = '';
		
		taskRecordTypeID = AutoEmailSendUtil.getRecordTypeId('Task',AutoEmailSendUtil.TASK_IS_RT);
    	for(Contact contact: newContactsMap.values()) {

    		
    		if(!String.isBlank(contact.Email) && !oldContactsMap.isEmpty()
    			&& oldContactsMap.containsKey(contact.Id)) {

    			Contact oldContact = new Contact();
    			oldContact = oldContactsMap.get(contact.Id);

    			if(oldContact.Email != contact.Email && contact.Email != null) {

    				String mapEmailValue;

    				mapEmailValue = oldContact.Email+'|'+contact.Email;
    				contactIdToOldNewEmailMap.put(contact.Id,mapEmailValue);
    			}
    		}
    	}
    	System.debug('contactIdToOldNewEmailMap:'+contactIdToOldNewEmailMap);
    	// 
    	if(!contactIdToOldNewEmailMap.isEmpty()) {

    		Set<Id> contactIds = new Set<Id>();
    		List<Task> tasksEmailUpdateList = new List<Task>();
    		contactIds = contactIdToOldNewEmailMap.keySet();

    		System.debug('contactIds:'+contactIds);
    		for(Task taskRec: [SELECT Id,EmailTo__c,AutoEmailAccount__c,
    								PrimaryContact__c
    							FROM Task
    							WHERE PrimaryContact__c != null
    							AND PrimaryContact__c IN: contactIds
    							AND Status !=: AutoEmailSendUtil.TASK_SENT
    							AND RecordTypeId =: taskRecordTypeID]) {

    			System.debug('taskRec:'+taskRec);
    			if(contactIdToOldNewEmailMap.containsKey(taskRec.PrimaryContact__c)) {
    			
    				String emailStr = taskRec.EmailTo__c;
    				String contactsEmails = contactIdToOldNewEmailMap.get(taskRec.PrimaryContact__c);
    				
    				System.debug('contactsEmails:'+contactsEmails);
    				String oldEmail = contactsEmails.substring(0,contactsEmails.indexOf('|'));
    				String newEmail = contactsEmails.substring(contactsEmails.indexOf('|')+1,contactsEmails.length());
    				String updatedEmails = emailStr.replace(oldEmail,newEmail);
	    			tasksEmailUpdateList.add(new Task(Id = taskRec.Id,
    												EmailTo__c = updatedEmails));
	    		}

    		}
    		if(!tasksEmailUpdateList.isEmpty()) {

    			update tasksEmailUpdateList;
    		}
    	}
    }
}