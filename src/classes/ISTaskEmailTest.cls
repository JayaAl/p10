@isTest
private class ISTaskEmailTest {
	
	@testSetup static void opportunityTestDataSetup() {
		// Implement test code
		list<Account> accountsList = new list<Account>();
		list<Contact> contactsList = new list<Contact>();
		// create Account default
		for(Integer i=0; i<100; i++) {

			Account acct = new Account();
			acct.name = 'AutoEmailCntTestN'+i;
            acct.Website = 'AutoEmailCntWebTest'+i+'.com';
            acct.BillingStreet = 'AutoEmailCntWebTest'+i;
            acct.BillingCity = 'AutoEmailCntWebTest'+i;
            acct.BillingState = 'CA';
            acct.BillingPostalCode = '45897643';
            acct.c2g__CODAAccountTradingCurrency__c = 'USD';
			accountsList.add(acct);
		}
		insert accountsList;
		// create Contact
		for(Integer i=0;i<100;i++) {

			Contact contact = new Contact();
			contact.lastName = 'autoLNCnt'+i;
			contact.accountId = accountsList[i].Id;
			contact.MailingStreet = 'MyStreet1';
			contact.MailingCountry = 'USA';
			contact.MailingState = 'CA';
			contact.MailingCity = 'Fremont';
			contact.Email = 'autoEmailCnt'+i+'@mydomain.com';
			contact.Title = 'autoEmailCnt'+i;
			contact.firstname = 'autoEmailFN'+i;
			contact.Industry_Category__c = 'Ad Network';
			contact.GCLID__c = 'autoEmailTEst';
			contactsList.add(contact);
		}
		insert contactsList;
	}
	
	@isTest static void contactEmailChangeTest() {
		
		// create opportunities
		list<Account> accountList = new list<Account>();
		list<Contact> contactList = new list<Contact>();
		list<Opportunity> opportunityList = new list<Opportunity>();
		list<Task> taskList = new list<Task>();

		accountList = [SELECT Id FROM Account];
		contactList = [SELECT Id,AccountId FROM Contact];
		Id opptyRecordTypeId = AutoEmailSendUtil.getRecordTypeId('Opportunity',
											AutoEmailSendUtil.INSIDE_SALES_RT);

		for(Integer i=0;i<10;i++) {

			Id accountId = contactList[i].AccountId;
			Id contactId = contactList[i].Id;

			Date currentDT = Date.today();
			Date contractEndDate = currentDT.addDays(5);

			Opportunity oppty = new Opportunity(
									RecordTypeId = opptyRecordTypeId,
									accountId = accountId,
									Industry_Category__c = 'Ad Network',
									Budget_Source__c = 'TV',
									Radio_Type__c = 'Local',
									Initiative_Location__c = 'US',
						            closeDate = System.today(),
						            name = 'autoEmailOpptyCntTest'+i,
						            stageName = 'Closed Won',
						            Primary_Billing_Contact__c = contactId,
						            Primary_Contact__c = contactId,
						            ContractStartDate__c = currentDT,
									ContractEndDate__c = contractEndDate,
									Preferred_Invoicing_Method__c = 'Email',
									Bill_on_Broadcast_Calendar2__c = 'No',
									Lead_Campaign_Manager__c = UserInfo.getUserId());
			opportunityList.add(oppty);
		}
		Profile sysAdminProfile = [SELECT Id 
									FROM Profile 
									WHERE name = 'System Administrator' 
									LIMIT 1];
		User autoEmailTest = new User(lastName = 'LNCntAEmail',
					            userName = 'LNCntAE@email.com',
					            profileId = sysAdminProfile.Id,
					            alias = 'aETC',
					            email = 'LNCntAE@email.com',
					            emailEncodingKey = 'ISO-8859-1',
					            languageLocaleKey = 'en_US',
					            localeSidKey = 'en_US',
					            timeZoneSidKey = 'America/Los_Angeles',
					            DisableAutoEmailSendOut__c = false);
        System.runAs(autoEmailTest) {

        	insert opportunityList;
			Test.startTest();

				list<Contact> updatedEmailList = new list<Contact>();
				for(Contact contactRec :contactList) {

					contactRec.Email = 'cntAutoEmail@email.com';
					updatedEmailList.add(contactRec);
				}
				if(!updatedEmailList.isEmpty()) {

					update updatedEmailList;
				}
			Test.stopTest();
			// validate task creation
			list<Id> opptyListIds = new list<Id>();
			map<Id,Integer> taskPerOpptyMap = new map<Id,Integer>();

			for(Opportunity oppty : opportunityList) {
				opptyListIds.add(oppty.Id);
			}
			list<Task> taskObjList = [SELECT Id,Subject,WhatId 
									FROM Task 
									WHERE Subject LIKE 'Campaign%' 
									AND WhatId != null 
									AND EmailTo__c LIKE '%cntAutoEmail@email.com%'];
			System.assert(taskObjList.size()>0);
		}
	}
	
}