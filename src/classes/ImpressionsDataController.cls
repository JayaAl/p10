/**
 * @name: ImpressionsDataController 
 * @desc: 
 * @author: Srikanth
 * @date: 03/31/15
 */
public with sharing class ImpressionsDataController {

    //list of all account team members, existing and blank for the page
    public List<Impressions_Data__c> impressionsData { get; set; }
    public String oppProId;

    public ImpressionsDataController(ApexPages.StandardController stdController) {
        //grab the opp product Id from the record we're on
        oppProId = String.valueOf(stdController.getId());
        id opportunityProductId = oppProId;
        system.debug('****** ' + oppProId );
        impressionsData = [
            SELECT Id, X3rd_Party_Conversions__c,X3rd_Party_Impressions__c,X3rd_Party_Revenue__c,X3rd_Party_Clicks__c,DFP_CTR__c,DFP_CVR__c,Cost_Type__c,
                Date__c,Description__c,DFP_Clicks__c, DFP_eCPM__c,DFP_Impressions__c,DFP_Revenue__c,ImpressionsKey__c,
                Opportunity__c,OpportunityProductId__c,Rate__c from Impressions_Data__c 
            WHERE OpportunityProductId__c LIKE :opportunityProductId + '%' ORDER BY Date__c
        ];
        // system.assert(false, oppProId + ': ->' + impressionsData);
    }

}