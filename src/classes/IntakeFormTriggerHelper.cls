public class IntakeFormTriggerHelper{
    
    public static void AfterUpdate(List<Intake_Form__c> newLst, Map<Id,Intake_Form__c> oldMap, Map<Id,Intake_Form__c> newMap ){
        
        List<Opportunity> opptyLstToInsert = new List<Opportunity>();
        List<Intake_Form__c> eligibleINtakeFOrm = new List<Intake_Form__c>();
        Id recordtypeId = [Select id from RecordType where developerName = 'Opportunity_Artist_Marketing' limit 1].Id;
        Id defaultMMGAcctId; 
        try{
            defaultMMGAcctId = [Select id,name from Account where name = 'Default MMG Account' and Type = 'Advertiser' limit 1].Id;
        }catch(Exception ex){
            Account acct = new Account();
            acct.RecordTypeId = '012400000009noZ';
            acct.Name = 'Default MMG Account';
            acct.Type = 'Advertiser';
            insert acct;
            defaultMMGAcctId = acct.Id;
            
        }
        for(Intake_Form__c intakeVar: newLst){
            
            if(oldMap.get(intakeVar.Id).status__c != newMap.get(intakeVar.Id).status__c && 'Confirmed'.equalsIgnoreCase(newMap.get(intakeVar.Id).status__c)){
                eligibleINtakeFOrm.add(newMap.get(intakeVar.Id));   
            }
        }
        
        for(Intake_Form__c formVar : eligibleINtakeFOrm){
            
            Opportunity oppty = new Opportunity();
            oppty.RecordTypeId = recordtypeId; // TODO : Remove hardcoding 
            oppty.Name = 'Opportunity-'+formVar.Name;
            oppty.AccountId = defaultMMGAcctId;
            oppty.CloseDate = Date.Today();
            oppty.stageName = 'New';
            oppty.launch_date__c = formVar.launch_date__c;
            oppty.Intake_Form__c = formVar.id;
            oppty.Intake_form_submitted__c = Date.Today();
            oppty.Marketing_Ops_Manager__c = formVar.ProgramManager__c;
            oppty.Content_Lead__c = formVar.CreativeOpsLead__c;
            
            opptyLstToInsert.add(oppty);
        }
        
        insert opptyLstToInsert;
        
    }
}