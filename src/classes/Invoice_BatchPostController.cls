/**
 * @name: Invoice_BatchPostController
 * @desc: Used to update Invoices with 'In Progress' status. Following fields are updated:
            A. Period = Open_Period__c
            B. Invoice Date = C2G__INVOICEDATE__C
          Once updated and the field set Ready to Post, user will be able to Post these invoices.
 * @author: Lakshman(sfdcace@gmail.com)
 * @date: 7-8-2013
 */
@isTest
public class Invoice_BatchPostController {

    /*
    *   item in context from the page
    */ 
    /*
    public List<SelectOption> invoiceView {get;set;}
    public String selectedView {get;set;}
    public String pageSize {get;set;}
    public String pageNumber {get;set;}
    public List<InvoiceWrapper> listInvoiceWrapper {get;set;}
    //public c2g__codaInvoice__c objInvoice {get;set;}
    public Date invoiceDate {get;set;}
    public Date dueDate {get;set;}
    public c2g__codaInvoice__c updateInvoices {get;set;}
    public Boolean disableButton {get;set;}
    public String filtersApplied{get;set;}
    public String shcStatus{get;set;}

    
    //set controller
    
    
    public ApexPages.StandardSetController setCon{get;set;}
    
    
    //   generic variables used
    
    private String currentUserId;
     
    //   constructor
    
    public Invoice_BatchPostController (){
        this.invoiceView = new List<SelectOption>();
        this.invoiceView.add(new SelectOption('In Progress', 'In Progress')); 
        this.invoiceView.add(new SelectOption('Ready to Post', 'Ready to Post'));
        this.invoiceView.add(new SelectOption('Excluded', 'Excluded'));
        selectedView = 'In Progress';
        //objInvoice = new c2g__codaInvoice__c();
        updateInvoices = new c2g__codaInvoice__c();
        updateInvoices.C2G__INVOICEDATE__C = System.today();
        this.listInvoiceWrapper = new List<InvoiceWrapper>();
        currentUserId = UserInfo.getUserId();
        
        populateUserLevelDefaults();
        if(ApexPages.currentPage().getParameters().get('view') != null && ApexPages.currentPage().getParameters().get('view') != '') {
            selectedView = ApexPages.currentPage().getParameters().get('view');
        }
        if(ApexPages.currentPage().getParameters().get('pageSize') != null && ApexPages.currentPage().getParameters().get('pageSize') != '') {
            pageSize = ApexPages.currentPage().getParameters().get('pageSize');
        }
        
        pageNumber = '1';
        filtersApplied = '';
        showInvoices();
    }
    
    public void populateUserLevelDefaults() {
        // Get the values for the running user. 
        BulkUpdatePostInvoices__c s = BulkUpdatePostInvoices__c.getValues(currentUserId);
        
        if(s == null) {
            // If the instance was null defer to the org value for the defaults.
            //   This assumes there is no profile level in play. 
            s = BulkUpdatePostInvoices__c.getOrgDefaults();
            pageSize = s.DefaultPageSIze__c;
            selectedView = s.Default_View__c;
            
        } else {
            // If non-null then use the current setting value to drive the defaults. 
            pageSize = s.DefaultPageSIze__c;
            selectedView = s.Default_View__c;
        }
    
    }
    
    public void showInvoices() {
        
        String myQuery;
        if(selectedView == 'In Progress') {
            myQuery = 'Select C2G__PERIOD__C, c2g__InvoiceDate__c, c2g__DueDate__c, Ready_to_Post__c, c2g__InvoiceStatus__c, Name, Id From c2g__codaInvoice__c where c2g__InvoiceStatus__c = \'In Progress\' AND Ready_to_Post__c = false AND Billed_Complete__c = true AND Gross_Invoice_Amount_Total__c <= 50000 ';
            filtersApplied = 'Invoice Status: <strong>In Progress</strong><br/>Ready to Post: <strong>false</strong><br/>Billed Complete: <strong>true</strong><br/>Gross Invoice Amount Total: <strong> <= 50K</strong>';            
        } else if(selectedView == 'Ready to Post') {
            myQuery = 'Select C2G__PERIOD__C, c2g__InvoiceDate__c, c2g__DueDate__c, Ready_to_Post__c, c2g__InvoiceStatus__c, Name, Id From c2g__codaInvoice__c where Ready_to_Post__c = true AND c2g__InvoiceStatus__c = \'In Progress\'';
            filtersApplied = 'Invoice Status: <strong>In Progress</strong><br/>Ready to Post: <strong>true</strong>';            
        } else if(selectedView == 'Excluded') {
            myQuery = 'Select C2G__PERIOD__C, c2g__InvoiceDate__c, c2g__DueDate__c, Ready_to_Post__c, c2g__InvoiceStatus__c, Name, Id From c2g__codaInvoice__c where c2g__InvoiceStatus__c = \'In Progress\' AND Ready_to_Post__c = false AND (Billed_Complete__c = false OR Gross_Invoice_Amount_Total__c > 50000) ';
            filtersApplied = 'Invoice Status: <strong>In Progress</strong><br/>Ready to Post: <strong>false</strong><br/>(Billed Complete: <strong>false</strong> OR <br/>Gross Invoice Amount Total: <strong> > 50K</strong>)';            
        }
        /*system.debug('******' + objInvoice.C2G__PERIOD__C );
        if (objInvoice.C2G__PERIOD__C != null)
          myQuery += ' and C2G__PERIOD__C = \''+objInvoice.C2G__PERIOD__C+'\'';
        if (objInvoice.Flight_Dates_Start__c != null) {
          String dateStr = DateTime.newInstance(objInvoice.Flight_Dates_Start__c.year(),objInvoice.Flight_Dates_Start__c.month(),objInvoice.Flight_Dates_Start__c.day()).format('yyyy-MM-dd');
          myQuery += ' and C2G__INVOICEDATE__C = ' + dateStr;
        }
        if (objInvoice.Flight_Dates_End__c != null) {
          String dateStr = DateTime.newInstance(objInvoice.Flight_Dates_End__c.year(),objInvoice.Flight_Dates_End__c.month(),objInvoice.Flight_Dates_End__c.day()).format('yyyy-MM-dd');
          myQuery += ' and C2G__DUEDATE__C = ' + dateStr;
        }/
        
        
        myQuery += ' Limit 10000';
        BulkUpdatePostInvoices__c configEntry = BulkUpdatePostInvoices__c.getOrgDefaults();
        disableButton = configEntry.BatchInProgress__c;
        shcStatus = configEntry.AutoPostScheduler__c ? 'ON' : 'OFF';
        
        system.debug('Query-----------' + myQuery );
        this.setCon = new ApexPages.StandardSetController(Database.getQueryLocator(myQuery));
        // Here defining pagination step size
        this.setCon.setPageSize(integer.valueOf(pageSize));
        this.setCon.setpageNumber(integer.valueOf(pageNumber));
        // Fill out the list to be used at the search page
        listInvoiceWrapper = getInvoices();
    }
    
    public List<InvoiceWrapper> getInvoices(){
        
        List<InvoiceWrapper> rows = new List<InvoiceWrapper>();
        
        for(sObject r : this.setCon.getRecords()){
            c2g__codaInvoice__c a = (c2g__codaInvoice__c)r;
            InvoiceWrapper row = new InvoiceWrapper(a, false);
            rows.add(row);            
        }
                
        return rows;
        
    }
    
    
    //   get selected invoices
    
    public List<InvoiceWrapper> getSelectedInvoices() {
        List<InvoiceWrapper> invoicesSelected = new List<InvoiceWrapper>();
        for(InvoiceWrapper iw: listInvoiceWrapper) {
            if(iw.IsSelected) {
                invoicesSelected.add(iw);
            }
        }
        return invoicesSelected;
    }
    
    
    //   update selected invoices
    
    public PageReference updateSelectedInvoices() {
        
        List<Id> invoiceToUpdate = new List<Id>();
        for(InvoiceWrapper iw: listInvoiceWrapper) {
            if(iw.IsSelected) {
                invoiceToUpdate.add(iw.tInvoice.Id);
            }
        }
        if(! invoiceToUpdate.isEmpty()) {
            BulkUpdatePostInvoices__c configEntry =BulkUpdatePostInvoices__c.getOrgDefaults();
            configEntry.BatchInProgress__c = true;
            update configEntry;
            system.debug('*********updateInvoices' + updateInvoices);
            Database.executeBatch(new Invoice_BatchUpdateClass(invoiceToUpdate, updateInvoices), 25);
            updateInvoices = new c2g__codaInvoice__c();
            updateInvoices.C2G__INVOICEDATE__C = System.today();
        }
        PageReference pageRef = new PageReference('/apex/Invoice_BatchPost?pageSize='+pageSize+'&view='+selectedView);
        
        pageRef.setRedirect(true);
        return pageRef;
    }
    
    
    //   post selected invoices
    
    public PageReference postSelectedInvoices() {
        List<Id> invoiceIdsToPost = new List<Id>();
        for(InvoiceWrapper iw: listInvoiceWrapper) {
            if(iw.IsSelected && iw.tInvoice.Ready_to_Post__c) {
                invoiceIdsToPost.add(iw.tInvoice.Id);
            }
        }
        if(! invoiceIdsToPost.isEmpty()) {
            BulkUpdatePostInvoices__c configEntry =BulkUpdatePostInvoices__c.getOrgDefaults();
            configEntry.BatchInProgress__c = true;
            update configEntry;
            SalesInvoiceBulkPost SalesInvoicePostBatch = new SalesInvoiceBulkPost(invoiceIdsToPost);
            SalesInvoicePostBatch.isFromInvoiceVF = true;
            Database.executeBatch(SalesInvoicePostBatch, 25);   
        }
        PageReference pageRef = new PageReference('/apex/Invoice_BatchPost?pageSize='+pageSize+'&view='+selectedView);
        
        pageRef.setRedirect(true);
        return pageRef;
    }
    
    
    //  update and post selected invoices
    
    public PageReference updateAndPostSelectedInvoices() {
        List<Id> invoiceToUpdate = new List<Id>();
        for(InvoiceWrapper iw: listInvoiceWrapper) {
            if(iw.IsSelected) {
                invoiceToUpdate.add(iw.tInvoice.Id);
            }
        }
        if(! invoiceToUpdate.isEmpty()) {
            BulkUpdatePostInvoices__c configEntry =BulkUpdatePostInvoices__c.getOrgDefaults();
            configEntry.BatchInProgress__c = true;
            update configEntry;
            system.debug('*********updateInvoices' + updateInvoices);
            updateInvoices.Ready_To_Post__c = true;
            Invoice_BatchUpdateClass ibup = new Invoice_BatchUpdateClass(invoiceToUpdate, updateInvoices);
            ibup.isPost = true;
            Database.executeBatch(ibup, 25);
            updateInvoices = new c2g__codaInvoice__c();
            updateInvoices.C2G__INVOICEDATE__C = System.today();
        }
        PageReference pageRef = new PageReference('/apex/Invoice_BatchPost?pageSize='+pageSize+'&view='+selectedView);
        
        pageRef.setRedirect(true);
        return pageRef;
    }
    
    public void toggleSchedulerStatus() {
        BulkUpdatePostInvoices__c configEntry =BulkUpdatePostInvoices__c.getOrgDefaults();
        configEntry.AutoPostScheduler__c = !configEntry.AutoPostScheduler__c;
        update configEntry;
        shcStatus = configEntry.AutoPostScheduler__c ? 'ON' : 'OFF';
    }
    
    
    //   advance to next page
    
    public void doNext(){
        
        if(this.setCon.getHasNext()) {
            this.setCon.next();
            pageNumber = string.valueOf(this.setCon.getPageNumber());
            listInvoiceWrapper = getInvoices();
        }
 
    }
    
    
    //   advance to previous page
    
    public void doPrevious(){
        
        if(this.setCon.getHasPrevious()) {
            this.setCon.previous();
            pageNumber = string.valueOf(this.setCon.getPageNumber());
            listInvoiceWrapper = getInvoices();
        }
                
    }
    
    
    //   return whether previous page exists
    
    public Boolean getHasPrevious(){
        
        return this.setCon.getHasPrevious();
        
    }
    
    
    //  return whether next page exists
    
    public Boolean getHasNext(){
        
        return this.setCon.getHasNext();
    
    }
    
    
    //   return page number
    
    public Integer getPageNumber(){
        
        return this.setCon.getPageNumber();
        
    }
    
    
    //   for record limits
    
    public List<SelectOption> getItems(){
        List<SelectOption> options = new List<SelectOption>();
        options.add(new SelectOption('200','200'));
        options.add(new SelectOption('500','500'));
        options.add(new SelectOption('700','700'));
        options.add(new SelectOption('1000','1000'));
        return options;
    }
    
    
     //   return total pages
    
    Public Integer getTotalPages(){
    
        Decimal totalSize = this.setCon.getResultSize();
        Decimal pageSize = this.setCon.getPageSize();
        
        Decimal pages = totalSize/pageSize;
        
        return (Integer)pages.round(System.RoundingMode.CEILING);
    }
    
    
    //   helper class that represents a row
    
    public class InvoiceWrapper{
        
        public c2g__codaInvoice__c  tInvoice {get;set;}
        public Boolean IsSelected{get;set;}
        
        public InvoiceWrapper(c2g__codaInvoice__c a, Boolean s){
            this.tInvoice = a;
            this.IsSelected = s;
        }
        
    } 
    */
}