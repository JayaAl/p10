/**
 * @name: Invoice_BatchPostControllerTest
 * @desc: Test class for Invoice_BatchPostController
 * @date: 11-9-2013
 */
@isTest(SeeAllData=true)
public class Invoice_BatchPostControllerTest {
    /*
    private static testMethod void testBatchPostInProgress() {
        try{
            // initial setup
            FF_BLNG_TestUtil_InvoiceRollups util = new FF_BLNG_TestUtil_InvoiceRollups();
            util.setup();
            
            // avoid mixed dml from adding running user to test company
            // owner group
            system.runAs(new User(id = UserInfo.getUserId())) {
                
                
                ApexPages.currentPage().getParameters().put('view','In Progress');
                ApexPages.currentPage().getParameters().put('pageSize','200');
                Invoice_BatchPostController testController = new Invoice_BatchPostController ();
                
                
                
                
                
                if(testController.setCon.getResultSize() > 0) {
                    testController.updateInvoices.Open_Period__c  = util.testPeriod.Id;
                    testController.updateInvoices.C2G__INVOICEDATE__C = System.today();
                    testController.updateInvoices.Ready_to_Post__c = true;
                    testController.listInvoiceWrapper[0].IsSelected = true;
                    testController.getSelectedInvoices();
                    Test.startTest();
                    testController.updateSelectedInvoices();
                    Test.stopTest();
                } else {
                    ApexPages.currentPage().getParameters().put('view','Ready to Post');
                    testController = new Invoice_BatchPostController ();
                    if(testController.setCon.getResultSize() > 0) {
                        testController.updateInvoices.Open_Period__c  = util.testPeriod.Id;
                        testController.updateInvoices.C2G__INVOICEDATE__C = System.today();
                        testController.updateInvoices.Ready_to_Post__c = true;
                        testController.listInvoiceWrapper[0].IsSelected = true;
                        testController.listInvoiceWrapper[0].tInvoice.Ready_to_Post__c = false;
                        update testController.listInvoiceWrapper[0].tInvoice;
                        testController.getSelectedInvoices();
                        Test.startTest();
                        testController.updateSelectedInvoices();
                        Test.stopTest();
                    }
                    
                }
            }
            }catch(Exception ex) {
                system.debug('Exception caught=' + ex);
            }
    }
    
    
    private static testMethod void testBatchUpdateAndPostInProgress() {
        try{
        // initial setup
        FF_BLNG_TestUtil_InvoiceRollups util = new FF_BLNG_TestUtil_InvoiceRollups();
        util.setup();
        
        // avoid mixed dml from adding running user to test company
        // owner group
        system.runAs(new User(id = UserInfo.getUserId())) {
            
            
            ApexPages.currentPage().getParameters().put('view','In Progress');
            ApexPages.currentPage().getParameters().put('pageSize','200');
            Invoice_BatchPostController testController = new Invoice_BatchPostController ();
            
            
            
            
            
            if(testController.setCon.getResultSize() > 0) {
                testController.updateInvoices.Open_Period__c  = util.testPeriod.Id;
                testController.updateInvoices.C2G__INVOICEDATE__C = System.today();
                testController.updateInvoices.Ready_to_Post__c = true;
                testController.listInvoiceWrapper[0].IsSelected = true;
                testController.getSelectedInvoices();
                Test.startTest();
                testController.updateAndPostSelectedInvoices();
                Test.stopTest();
            } else {
                ApexPages.currentPage().getParameters().put('view','Ready to Post');
                testController = new Invoice_BatchPostController ();
                if(testController.setCon.getResultSize() > 0) {
                    testController.updateInvoices.Open_Period__c  = util.testPeriod.Id;
                    testController.updateInvoices.C2G__INVOICEDATE__C = System.today();
                    testController.updateInvoices.Ready_to_Post__c = true;
                    testController.listInvoiceWrapper[0].IsSelected = true;
                    testController.listInvoiceWrapper[0].tInvoice.Ready_to_Post__c = false;
                    update testController.listInvoiceWrapper[0].tInvoice;
                    testController.getSelectedInvoices();
                    Test.startTest();
                    testController.updateAndPostSelectedInvoices();
                    Test.stopTest();
                }
                
            }
            }
        } catch(Exception ex) {
            system.debug('Exception caught=' +ex);
        }
    }
     
    private static testMethod void testBatchPostReadyToPost() {
        try{
            // initial setup
            FF_BLNG_TestUtil_InvoiceRollups util = new FF_BLNG_TestUtil_InvoiceRollups();
            util.setup();
            
            // avoid mixed dml from adding running user to test company
            // owner group
            system.runAs(new User(id = UserInfo.getUserId())) {
                
                
                ApexPages.currentPage().getParameters().put('view','Ready to Post');
                Invoice_BatchPostController testController = new Invoice_BatchPostController ();
    
                if(testController.setCon.getResultSize() > 0) {
                    testController.listInvoiceWrapper[0].IsSelected = true;
                    testController.getSelectedInvoices();
                    Test.startTest();
                    testController.postSelectedInvoices();
                    Test.stopTest();
                } else {
                    ApexPages.currentPage().getParameters().put('view','In Progress');
                    testController = new Invoice_BatchPostController ();
                    if(testController.setCon.getResultSize() > 0) {
                        testController.listInvoiceWrapper[0].IsSelected = true;
                        testController.listInvoiceWrapper[0].tInvoice.Ready_to_Post__c = true;
                        update testController.listInvoiceWrapper[0].tInvoice;
                        testController.getSelectedInvoices();
                        Test.startTest();
                        testController.postSelectedInvoices();
                        Test.stopTest();
                    }
                    
                }
                }
            } catch(Exception ex) {
                system.debug('Exception caught=' + ex);
            }
    }
    
    private static testMethod void testAutoBatchPostReadyToPost() {
        try {
            // initial setup
            FF_BLNG_TestUtil_InvoiceRollups util = new FF_BLNG_TestUtil_InvoiceRollups();
            util.setup();
            
            // avoid mixed dml from adding running user to test company
            // owner group
            system.runAs(new User(id = UserInfo.getUserId())) {
                
                
                ApexPages.currentPage().getParameters().put('view','Ready to Post');
                Invoice_BatchPostController testController = new Invoice_BatchPostController ();
                testController.toggleSchedulerStatus();
                BulkUpdatePostInvoices__c configEntry =BulkUpdatePostInvoices__c.getOrgDefaults();
                configEntry.AutoPostScheduler__c = true;
                update configEntry;
                if(testController.setCon.getResultSize() > 0) {
                    testController.listInvoiceWrapper[0].IsSelected = true;
                    testController.getSelectedInvoices();
                    Test.startTest();
                    String qry =  'Select Id From c2g__codaInvoice__c where Id = \'' + testController.listInvoiceWrapper[0].tInvoice.Id + '\' AND Ready_to_Post__c = true';
                    Invoice_BatchReadyToPost b = new Invoice_BatchReadyToPost(qry); 
                    database.executebatch(b, 1);
                    Test.stopTest();
                } else {
                    ApexPages.currentPage().getParameters().put('view','In Progress');
                    testController = new Invoice_BatchPostController ();
                    if(testController.setCon.getResultSize() > 0) {
                        testController.listInvoiceWrapper[0].IsSelected = true;
                        testController.listInvoiceWrapper[0].tInvoice.Ready_to_Post__c = true;
                        update testController.listInvoiceWrapper[0].tInvoice;
                        testController.getSelectedInvoices();
                        Test.startTest();
                        String qry =  'Select Id From c2g__codaInvoice__c where Id = \'' + testController.listInvoiceWrapper[0].tInvoice.Id + '\' AND Ready_to_Post__c = true';
                        Invoice_BatchReadyToPost b = new Invoice_BatchReadyToPost(qry); 
                        
                        database.executebatch(b, 1);
                        Test.stopTest();
                    }
                    
                }
                }
            }catch(Exception ex) {
                system.debug('Exception caught=' + ex);
            }
    }
    
    private static testMethod void testBatchPostExcluded() {
        try{
        // initial setup
        FF_BLNG_TestUtil_InvoiceRollups util = new FF_BLNG_TestUtil_InvoiceRollups();
        util.setup();
        system.runAs(new User(id = UserInfo.getUserId())) {
            
            ApexPages.currentPage().getParameters().put('view','Excluded');
            Invoice_BatchPostController testController = new Invoice_BatchPostController ();
            //Pagination action test
            testController.doNext();
            testController.doPrevious();
            testController.getHasPrevious();
            testController.getHasNext();
            testController.getPageNumber();
            testController.getItems();
            testController.getTotalPages();
            
            
            if(testController.setCon.getResultSize() > 0) {
                testController.updateInvoices.Open_Period__c  = util.testPeriod.Id;
                testController.updateInvoices.C2G__INVOICEDATE__C = System.today();
                testController.updateInvoices.Ready_to_Post__c = true;
                testController.listInvoiceWrapper[0].IsSelected = true;
                
                testController.getSelectedInvoices();
                Test.startTest();
                //testController.updateSelectedInvoices();
                if(!testController.listInvoiceWrapper[0].tInvoice.Ready_to_Post__c) {
                    testController.listInvoiceWrapper[0].tInvoice.Ready_to_Post__c = true;
                    update testController.listInvoiceWrapper[0].tInvoice;
                }
                testController.listInvoiceWrapper[0].IsSelected = true;
                testController.postSelectedInvoices();
                Test.stopTest();
            } 
            
       }
       } catch(Exception ex) {
           system.debug('***********' + ex);
       }
   }
*/
}