/**
 * @name: Invoice_BatchReadyToPost 
 * @desc: Batch class for scheduler Invoice_ScheduleReadyToPost 
 * @author: Lakshman(sfdcace@gmail.com)
 * @date: 23-9-2013
 */
global class Invoice_BatchReadyToPost implements Database.Batchable<sObject>{

   global final String Query;

   global Invoice_BatchReadyToPost(String q){

      Query=q; 
   }

   global Database.QueryLocator start(Database.BatchableContext BC){
      return Database.getQueryLocator(query);
   }

    global void execute(Database.BatchableContext ctx, List<SObject> records)
    {
        // Build list of Purchase Invoice API references
        List<c2g.CODAAPICommon.Reference> refs = new List<c2g.CODAAPICommon.Reference>();
        for(SObject sobj : records)
        { 
            c2g.CODAAPICommon.Reference ref = new c2g.CODAAPICommon.Reference();
            ref.Id = sobj.id;
            refs.add(ref);
        }
        
        // Call FinancialForce API to bulk post given Purchase Invoices (users current company)
        if(!SalesInvoiceBulkPostWebService.TestMode)
            c2g.CODAAPISalesInvoice_3_0.BulkPostInvoice(null, refs);    
    }
   
    global void finish(Database.BatchableContext ctx)
    {
        AsyncApexJob a = [Select Id, Status, NumberOfErrors, JobItemsProcessed, CreatedDate, CompletedDate, 
        TotalJobItems, CreatedBy.Email, CreatedById, CreatedBy.Name
        from AsyncApexJob where Id =:ctx.getJobId()];
        
        // Send an email to the Apex job's submitter notifying of job completion.  
        Messaging.SingleEmailMessage mail = new Messaging.SingleEmailMessage();
        String[] toAddresses = new String[] {a.CreatedBy.Email};
        mail.setToAddresses(toAddresses);
        mail.setSubject('Scheduled Ready To Post Invoice Batch ' + a.Status);
        String body = 'Hi, <br/>The Scheduled Ready To Post Invoice Batch is ' + a.Status;
        body += '<br/>The batch Apex job was created by '+a.CreatedBy.Name+' ('+a.CreatedBy.Email+') processed '+a.TotalJobItems+' batches with '+a.NumberOfErrors+' failures. The process began at '+a.CreatedDate+' and finished at '+a.CompletedDate+'.';
        body += '<br/>Job Id ==>' + a.Id;
        mail.setHtmlBody(body);
        
        Messaging.sendEmail(new Messaging.SingleEmailMessage[] { mail });
    } 
}