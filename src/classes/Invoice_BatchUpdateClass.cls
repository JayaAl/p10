/**
 * @name: Invoice_BatchUpdateClass 
 * @desc: Used to Batch Update list of Invoices sent from VF
 * @author: Lakshman(sfdcace@gmail.com)
 * @date: 12-8-2013
 */
global class Invoice_BatchUpdateClass implements Database.Batchable<sObject>, Database.Stateful{

   private final List<Id> m_ids;
   public c2g__codaInvoice__c updateInvoices;
   public Map<String, BillingTermsMapping__c> mapOfBillingTermsValueToDays;
   // To maintain all failed records with error message
   global String errormsgs;
   global Integer failureCounter;
   global Integer totalRecords;
   global InvoiceBatchLog__c objBatchLog;
   global Boolean isPost;
   global List<Id> successIds;
   global Invoice_BatchUpdateClass(List<Id> ids, c2g__codaInvoice__c i){
      m_ids=ids; 
      isPost = false;
      updateInvoices = i;
      errormsgs = '';
      failureCounter = 0;
      totalRecords = 0;
      successIds = new List<Id>();
      objBatchLog = new InvoiceBatchLog__c();
      mapOfBillingTermsValueToDays = BillingTermsMapping__c.getAll();
   }

   global Database.QueryLocator start(Database.BatchableContext BC){
      return Database.getQueryLocator('Select C2G__PERIOD__C, c2g__InvoiceDate__c, Billing_Terms__c, c2g__DueDate__c, Ready_to_Post__c, c2g__InvoiceStatus__c, Name, Id From c2g__codaInvoice__c where Id In :m_ids');
   }

   global void execute(Database.BatchableContext BC, List<sObject> scope){
     List<c2g__codaInvoice__c> invoicesToUpdate = (List<c2g__codaInvoice__c>)scope;
     for(c2g__codaInvoice__c s : invoicesToUpdate){
        s.C2G__PERIOD__C = updateInvoices.Open_Period__c;
        s.C2G__INVOICEDATE__C = updateInvoices.C2G__INVOICEDATE__C;
        s.Ready_to_Post__c = updateInvoices.Ready_to_Post__c;
        if(s.Billing_Terms__c != null && mapOfBillingTermsValueToDays.containsKey(s.Billing_Terms__c.toUpperCase())) {
            s.C2G__DUEDATE__C = updateInvoices.C2G__INVOICEDATE__C.addDays(integer.ValueOf(mapOfBillingTermsValueToDays.get(s.Billing_Terms__c.toUpperCase()).Days__c));
        } else {
            s.C2G__DUEDATE__C = updateInvoices.C2G__INVOICEDATE__C;
        }
     }
    system.debug('***********invoicesToUpdate' + invoicesToUpdate);
    if(! invoicesToUpdate.isEmpty()){
        totalRecords += invoicesToUpdate.size();
        Database.SaveResult[] lsr = Database.update(invoicesToUpdate,false);
        Integer recordid = 0;
        for (Database.SaveResult SR : lsr) {
            if (!SR.isSuccess()) {
                this.errormsgs += 'Invoice Number: ' + invoicesToUpdate[recordid].Name + ', Invoice Id: ' + invoicesToUpdate[recordid].Id + ', Error: ' + SR.getErrors()[0].getMessage() + '<br/>';
                failureCounter++;
            } else {
                successIds.add(invoicesToUpdate[recordid].Id);
            }
            
            recordid++;
        }
    }
    }

   global void finish(Database.BatchableContext BC){
      // Get the ID of the AsyncApexJob representing this batch job  
      // from Database.BatchableContext.    
      // Query the AsyncApexJob object to retrieve the current job's information.  
      
      AsyncApexJob a = [Select Id, Status, NumberOfErrors, JobItemsProcessed, CreatedDate, CompletedDate, 
       TotalJobItems, CreatedBy.Email, CreatedById, CreatedBy.Name
       from AsyncApexJob where Id =:BC.getJobId()];
    
      // Send an email to the Apex job's submitter notifying of job completion.  
      Messaging.SingleEmailMessage mail = new Messaging.SingleEmailMessage();
      String[] toAddresses = new String[] {a.CreatedBy.Email};
      mail.setToAddresses(toAddresses);
      mail.setSubject('Invoice Batch Update ' + a.Status);
      String body = 'Hi, <br/>Invoice Batch Update is ' + a.Status;
      body += '<br/>The batch Apex job was created by '+a.CreatedBy.Name+' ('+a.CreatedBy.Email+') processed '+a.TotalJobItems+' batches with '+a.NumberOfErrors+' failures. The process began at '+a.CreatedDate+' and finished at '+a.CompletedDate+'.';
      body += '<br/>Job Id ==>' + a.Id;
      body += '<br/>Total Records Processed ==>' + totalRecords; 
      if(this.errormsgs != ''){
          body += '<br/>Total Records Failed ==>' + failureCounter +'<br/>Following Errors were addressed:<br/>'; 
          body += this.errormsgs;
          //Create Log Entry
          objBatchLog.ErrorMessage__c = '<br/>Total Records Failed ==>' + failureCounter +'<br/>Following Errors were addressed:<br/>' + this.errormsgs;
          objBatchLog.JobCreator__c = a.CreatedById;
          insert objBatchLog;
      }
      mail.setHtmlBody(body);
    
      Messaging.sendEmail(new Messaging.SingleEmailMessage[] { mail });
      
      if(isPost) {
          if(! successIds.isEmpty()) {
                SalesInvoiceBulkPost SalesInvoicePostBatch = new SalesInvoiceBulkPost(successIds);
                SalesInvoicePostBatch.isFromInvoiceVF = true;
                Database.executeBatch(SalesInvoicePostBatch, 25); 
          }
      } else {
          BulkUpdatePostInvoices__c configEntry =BulkUpdatePostInvoices__c.getOrgDefaults();
          configEntry.BatchInProgress__c = false;
          update configEntry;
      }
      
   }
}