/*
Developer: Vardhan Gupta (vgupta@pandora.com)
Created Date: 03/20/2013
Used by: Invoice_BeforeInsert_BeforeUpdate trigger
Description:
Populates Billing Contact from the Oppotunity before the Invoice is created /updated.
Change Logs:
-> Added Null checks for Primary Bliing Conctacts Account Id and Billing Contacts Account Id before assigning to Sales Invoices Account Id on line #77 and line #152
-> Modifed logic so that the code works only for Invoices with Status 'In Progress'
*/

/*
* 2014-05-29
* Code cleanup and componentization
* Updated trigger functionality to additionally trigger on any edit by a Finance user
*/
@isTest //intentional
public class Invoice_PopulateBillingContact{
    /*
    private Set<Id> setAuthProfileIds(){
        Set<String> profileNames = new Set<String>{
           'Finance - Sales Invoicing User' , 
           'Finance - Sales Invoicing Manager User' ,
           'Finance - Cash Entry User',
           'Finance - Cash Entry Manager User'
        };
        Set<Id> theSet = new Set<Id>();
        for(Profile p:[Select Id from Profile Where Name In :profileNames ]){
            theSet.add(p.Id);
        }
        return theSet;
    }
    
    
    private List<c2g__codaInvoice__c> newInvoiceList;
    
    // Constructor  
    public Invoice_PopulateBillingContact(List<c2g__codaInvoice__c> newInvoiceList) {
        this.newInvoiceList = newInvoiceList;
    }
    
    // NB: This function is designed to be called from a before trigger
    // and does not make any explicit commits
    public void insertBillingContact() {
        
        Set<Id> relatedOpptyIds = new Set<Id>();
        List<c2g__codaInvoice__c> invoiceList = new List<c2g__codaInvoice__c>();
        
        Set<Id> setPermissiveProfileIds = setAuthProfileIds();
        
        for(c2g__codaInvoice__c invoice_record : newInvoiceList) {
            // ensure the Opportunity ID is not null.
            // updated by Lakshman on 15-11-2013 to ensure that Invoice Status is 'In Progress'
            // 
            if( invoice_record.c2g__Opportunity__c != null 
               &&( 
                   invoice_record.c2g__InvoiceStatus__c == 'In Progress' 
                   || setPermissiveProfileIds.contains(UserInfo.getProfileId())
               )
              ){
                  relatedOpptyIds.add(invoice_record.c2g__Opportunity__c); //Add it to the set of Oppty IDs.
                  invoiceList.add(invoice_record); //Add to invoice list if opportunity is present.
              }
        }
        
        // Query Primary Billing Contact related fields from the primary billing contact on the opportunity.
        //put in Opportunity map.
        //This is done for the first time insert only.
        if(relatedOpptyIds.size() > 0){
            Map<Id, Opportunity> opptyMap = new Map<Id, Opportunity>([
                select Primary_Billing_Contact__c , 
                Opportunity.Primary_Billing_Contact__r.Accountid, //updated by VG 05/05/2013 - Get the billing contact's parent account and update it on insert. SS will not be doing this going forward.
                Opportunity.Primary_Billing_Contact__r.id, 
                Opportunity.Primary_Billing_Contact__r.Title, 
                Opportunity.Primary_Billing_Contact__r.FirstName, 
                Opportunity.Primary_Billing_Contact__r.LastName, 
                Opportunity.Primary_Billing_Contact__r.MailingStreet, 
                Opportunity.Primary_Billing_Contact__r.MailingCity, 
                Opportunity.Primary_Billing_Contact__r.MailingCountry,
                Opportunity.Primary_Billing_Contact__r.MailingState, 
                Opportunity.Primary_Billing_Contact__r.MailingPostalCode, 
                Opportunity.Primary_Billing_Contact__r.Email,
                Opportunity.Primary_Billing_Contact__r.Fax,
                Opportunity.Primary_Billing_Contact__r.MobilePhone,
                Opportunity.Primary_Billing_Contact__r.Phone 
                from Opportunity
                where id in :relatedOpptyIds
                and Primary_Billing_Contact__c != null
            ]);
            
            // Populate the Billing Contact info from the Opportunity on the newly created invoice.
            //check all fields if they are null or not.
            if(invoiceList.size() > 0 && opptyMap.size() > 0) { 
                for(c2g__codaInvoice__c record : invoiceList) {
                    if(opptyMap.containsKey(record.c2g__Opportunity__c)) {
                        record.Billing_Contact__c = opptyMap.get(record.c2g__Opportunity__c).Primary_Billing_Contact__c;
                        record.c2g__Account__c = opptyMap.get(record.c2g__Opportunity__c).Primary_Billing_Contact__r.accountid != null ? opptyMap.get(record.c2g__Opportunity__c).Primary_Billing_Contact__r.accountid : record.c2g__Account__c; //updated by Laskhman on 14-11-2013 - added null checks before assignment; updated by VG 05/05/2013 - Get the billing contact's parent account
                        
                        record.Billing_Contact_Name__c = (opptyMap.get(record.c2g__Opportunity__c).Primary_Billing_Contact__r.FirstName != null && opptyMap.get(record.c2g__Opportunity__c).Primary_Billing_Contact__r.LastName != null 
                                                          ? opptyMap.get(record.c2g__Opportunity__c).Primary_Billing_Contact__r.FirstName + ' ' + opptyMap.get(record.c2g__Opportunity__c).Primary_Billing_Contact__r.LastName 
                                                          : '');
                        
                        record.Billing_Contact_Address_1__c = (opptyMap.get(record.c2g__Opportunity__c).Primary_Billing_Contact__r.MailingStreet != null ? opptyMap.get(record.c2g__Opportunity__c).Primary_Billing_Contact__r.MailingStreet : '');
                        record.Billing_Contact_City__c = (opptyMap.get(record.c2g__Opportunity__c).Primary_Billing_Contact__r.MailingCity != null ? opptyMap.get(record.c2g__Opportunity__c).Primary_Billing_Contact__r.MailingCity : '');
                        record.Billing_Contact_State__c = (opptyMap.get(record.c2g__Opportunity__c).Primary_Billing_Contact__r.MailingState != null ? opptyMap.get(record.c2g__Opportunity__c).Primary_Billing_Contact__r.MailingState : '');
                        record.Billing_Contact_Country__c = (opptyMap.get(record.c2g__Opportunity__c).Primary_Billing_Contact__r.MailingCountry != null ? opptyMap.get(record.c2g__Opportunity__c).Primary_Billing_Contact__r.MailingCountry : '');
                        record.Billing_Contact_Postal_Code__c = (opptyMap.get(record.c2g__Opportunity__c).Primary_Billing_Contact__r.MailingPostalCode != null ? opptyMap.get(record.c2g__Opportunity__c).Primary_Billing_Contact__r.MailingPostalCode : '');
                        record.Billing_Contact_Fax__c = (opptyMap.get(record.c2g__Opportunity__c).Primary_Billing_Contact__r.Fax != null ? opptyMap.get(record.c2g__Opportunity__c).Primary_Billing_Contact__r.Fax : '');
                        record.Billing_Contact_Phone__c = (opptyMap.get(record.c2g__Opportunity__c).Primary_Billing_Contact__r.Phone != null ? opptyMap.get(record.c2g__Opportunity__c).Primary_Billing_Contact__r.Phone : '');
                        record.Billing_Contact_Mobile__c = (opptyMap.get(record.c2g__Opportunity__c).Primary_Billing_Contact__r.MobilePhone != null ? opptyMap.get(record.c2g__Opportunity__c).Primary_Billing_Contact__r.MobilePhone : '');
                        record.Billing_Contact_Email_Address__c = (opptyMap.get(record.c2g__Opportunity__c).Primary_Billing_Contact__r.Email != null ? opptyMap.get(record.c2g__Opportunity__c).Primary_Billing_Contact__r.Email : '');
                    } //end of if block.
                } //end of For Loop
            } //end of if block - check for invoicelist size      
        } // end of outer if block - check for relatedOppIds size
    } // end of insertBillingContact function
    
    public void updateBillingContact(){
        
        Set<Id> relatedBillingContactIds = new Set<Id>();
        List<c2g__codaInvoice__c> invoiceList = new List<c2g__codaInvoice__c>();
        
        Set<Id> setPermissiveProfileIds = setAuthProfileIds();
        
        for(c2g__codaInvoice__c invoice_record : newInvoiceList) {
            //ensure the BillingContact ID is not null.
            //updated by Lakshman on 15-11-2013 to ensure that Invoice Status is 'In Progress'
            if( invoice_record.Billing_Contact__c != null 
               && ( 
                   invoice_record.c2g__InvoiceStatus__c == 'In Progress' 
                   || setPermissiveProfileIds.contains(UserInfo.getProfileId())
               )
              ){
                  relatedBillingContactIds.add(invoice_record.Billing_Contact__c); //Add it to the set of BillingContact IDs.
                  invoiceList.add(invoice_record); //Add to invoice list if Billing Contact is present.
              }
        }
        
        // Query Primary Billing Contact related fields from the contact object and put in Contact Map.
        if(relatedBillingContactIds.size() > 0){
            Map<Id, Contact> contactMap = new Map<Id, Contact>([
                select id, 
                accountid, //updated by VG 05/05/2013 - Get the billing contact's parent account and update it on insert. SS will not be doing this going forward.
                Title, 
                FirstName, 
                LastName, 
                MailingStreet, 
                MailingCity, 
                MailingCountry,
                MailingState, 
                MailingPostalCode, 
                Email,
                Fax,
                MobilePhone,
                Phone 
                from Contact
                where id in :relatedBillingContactIds
            ]);
            
            if(invoiceList.size() > 0 && contactMap.size() > 0) {
                for(c2g__codaInvoice__c record : invoiceList) {
                    if(contactMap.containsKey(record.Billing_Contact__c)) {
                        record.Billing_Contact_Name__c = (contactMap.get(record.Billing_Contact__c).FirstName != null && contactMap.get(record.Billing_Contact__c).LastName != null 
                                                          ? contactMap.get(record.Billing_Contact__c).FirstName + ' ' + contactMap.get(record.Billing_Contact__c).LastName 
                                                          : '');
                        record.c2g__Account__c = contactMap.get(record.Billing_Contact__c).accountid != null ? contactMap.get(record.Billing_Contact__c).accountid : record.c2g__Account__c;  //updated by Laskhman on 14-11-2013 - added null checks before assignment; updated by VG 05/05/2013 - Get the billing contact's parent account                              
                        record.Billing_Contact_Address_1__c = (contactMap.get(record.Billing_Contact__c).MailingStreet != null ? contactMap.get(record.Billing_Contact__c).MailingStreet : '');
                        record.Billing_Contact_City__c = (contactMap.get(record.Billing_Contact__c).MailingCity != null ? contactMap.get(record.Billing_Contact__c).MailingCity : '');
                        record.Billing_Contact_State__c = (contactMap.get(record.Billing_Contact__c).MailingState != null ? contactMap.get(record.Billing_Contact__c).MailingState : '');
                        record.Billing_Contact_Country__c = (contactMap.get(record.Billing_Contact__c).MailingCountry != null ? contactMap.get(record.Billing_Contact__c).MailingCountry : '');
                        record.Billing_Contact_Postal_Code__c = (contactMap.get(record.Billing_Contact__c).MailingPostalCode != null ? contactMap.get(record.Billing_Contact__c).MailingPostalCode : '');
                        record.Billing_Contact_Fax__c = (contactMap.get(record.Billing_Contact__c).Fax != null ? contactMap.get(record.Billing_Contact__c).Fax : '');
                        record.Billing_Contact_Phone__c = (contactMap.get(record.Billing_Contact__c).Phone != null ? contactMap.get(record.Billing_Contact__c).Phone : '');
                        record.Billing_Contact_Mobile__c = (contactMap.get(record.Billing_Contact__c).MobilePhone != null ? contactMap.get(record.Billing_Contact__c).MobilePhone : '');
                        record.Billing_Contact_Email_Address__c = (contactMap.get(record.Billing_Contact__c).Email != null ? contactMap.get(record.Billing_Contact__c).Email : '');
                    } //end of if block.
                } //end of For Loop
            } // end of if block - check for contactMap size
        } // end of outer if block - check for relatedBillingContactIds size
    } // end of updateBillingContact function  
*/
}