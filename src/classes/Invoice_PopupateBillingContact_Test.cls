/**
Developer: Vardhan Gupta.
Date: 03/220/2013
Test Class for Invoice_PopulateBillingContact.
**/
@isTest
public class Invoice_PopupateBillingContact_Test{
    /*
    //Test Variables       
    static Account testAccount1;       
    Static Contact testconObj1; 
    Static Contact testconObj2;    
    Static Opportunity testOpportunity1;
    Static c2g__codaInvoice__c testInvoice1;
    Static c2g__codaInvoice__c testInvoice2;
    // Create two accounts and generate a case with a related 
    // opportunity and a record type matching the trigger filters
    static {
        testAccount1 = UTIL_TestUtil.generateAccount();
        insert testAccount1;
        testOpportunity1 = UTIL_TestUtil.generateOpportunity(testAccount1.id);
        insert testOpportunity1;
        
        //Create Test Contact.
        testconObj1 = UTIL_TestUtil.generateContact(testAccount1.id);
        testconObj2 = UTIL_TestUtil.generateContact(testAccount1.id);
        insert testconObj1;
        insert testconObj2;
        testOpportunity1.Primary_Billing_Contact__c = testconObj1.id;
        update testOpportunity1;
        Account customerAccount = [
            SELECT id 
            FROM Account 
            WHERE c2g__CODAAccountsReceivableControl__c != null 
            AND c2g__CODAAccountTradingCurrency__c ='USD' 
            LIMIT 1];//Add by Lakshman on 10/28/2014(ISS-11405)
        FF_BLNG_TestUtil_InvoiceRollups util = new FF_BLNG_TestUtil_InvoiceRollups();
        util.setup();
        testInvoice1 = util.createSalesInvoice(customerAccount.id, testOpportunity1.id);
        
    }
    
    @isTest (SeeAllData=true)
    private static void testPopulateInvoice() {
        // insert case
        Test.startTest();
        // insert testInvoice1;
        testInvoice1.Billing_Contact__c = testconObj2.id;
        update testInvoice1;
        Test.stopTest();
    }
    */
}