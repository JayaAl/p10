/**
 * @name: Invoice_ScheduleReadyToPost
 * @desc: Used to schedule post Ready To Post Invoices
 * @author: Lakshman(sfdcace@gmail.com)
 * @date: 23-9-2013
 */
@isTest
global class Invoice_ScheduleReadyToPost /*implements Schedulable*/ {
    /*// This test runs a scheduled job at midnight Sept. 1st. 2022
    public static String CRON_EXP = '0 0 0 1 9 ? 2022';
    global void execute(SchedulableContext sc) {
        BulkUpdatePostInvoices__c configEntry =BulkUpdatePostInvoices__c.getOrgDefaults();   
        if(configEntry.AutoPostScheduler__c) {     
            //query from Ready To Post view
            String qry =  'Select Id From c2g__codaInvoice__c where Ready_to_Post__c = true AND c2g__InvoiceStatus__c = \'In Progress\'';
            Invoice_BatchReadyToPost b = new Invoice_BatchReadyToPost(qry); 
            database.executebatch(b, 25);
        }
    }
*/
}