/***********************************************
    Class: CSV_ACHTypeController
    This class creates CSV files for Electronic (ACH)
    Type PaymentMethod from Payment Object 
    Author – Cleartask
    Date – 29/08/2011    
    Revision History    
 ***********************************************/
 
public class JPMCSV_ACHTypeController {
    
    //Changed by ROhit - 02082013. Space prevents excel from formatting the column as a number.
    public final static String VENDOR_REF_DELIMITER = ', ';

    /* method to get rows for Electronic type*/
    public static String paymentAsCSVforACHType(c2g__codaPayment__c paymentRecord) {
      
        String csvString = '';
        String bankAccountNumber = '';
        
        if(paymentRecord == null){
            return csvString; 
        } else if(paymentRecord != null){
            
            List<AggregateResult> paymentDetailList = JPMCSV_Util.getPaymentDetailSummary(paymentRecord.Id);
          
            if(paymentDetailList != null && paymentDetailList.size() > 0){   
                
                List<c2g__codaPaymentMediaDetail__c> mediaDetailList = [
                    Select c2g__Account__c , c.c2g__VendorReference__c, c.c2g__DocumentNumber__c 
                    From c2g__codaPaymentMediaDetail__c c 
                    where c2g__PaymentMediaSummary__r.c2g__PaymentMediaControl__r.c2g__Payment__c = :paymentRecord.id AND c.c2g__VendorReference__c != null
                    Order By CreatedDate
                ];
                
                Map<Id, String> m = new Map<Id, String>();
                for(c2g__codaPaymentMediaDetail__c pmd : mediaDetailList) {
                    String accountId = pmd.c2g__Account__c;
                    accountId = (accountId.length() > 15) ? accountId.substring(0, 15) : accountId;
                    String vendorReference = m.get(pmd.c2g__Account__c) == null ? '' : m.get(pmd.c2g__Account__c) + VENDOR_REF_DELIMITER;
                    vendorReference += pmd.c2g__VendorReference__c;
                    
                    m.put(accountId, vendorReference);
                }
                
                
                for(AggregateResult result :paymentDetailList){
                    csvString += paymentAsCSVforACHTypeFirstRow(result, m);
                    csvString += JPMCSV_Constants.NEW_LINE_CHARACTER;
                }    
           } 
       }
       return csvString;   
    }

    /* method for First row for Electronic Type of Excel Sheet */
    public static String paymentAsCSVforACHTypeFirstRow(AggregateResult result, 
        Map<Id, String> m) 
    { 
        String csvString = '';
        if(result == null){
            return csvString; 
        }
        String accountName = result.get('accountName') == null ? '' : (String) result.get('accountName');
        String accountId = result.get('accountId') == null ? '' : (String) result.get('accountId');
        Decimal transactionValue = result.get('transactionValue') == null ? 0 : (Decimal)result.get('transactionValue');
        
        //New fields - Added in the SOQL - JPMCSV_Util.getPaymentDetailSummary
        String accountRef = result.get('accountRef') == null ? '' : (String) result.get('accountRef');
        //pad left
        if(accountRef != '')
        accountRef = '"'+padLeft(accountRef, 9)+'"';
        String accountNum = result.get('accountNum') == null ? '' : (String) result.get('accountNum');
        
    /*
    nsk: 06/23/14 -ISS-8100 - Retaining commented code for @@temporary-reference
    */
        /*
        accountName = JPMCSV_Util.spiltComma(accountName);
        csvString += accountName.length() > 22 ?  accountName.substring(0, 22) : accountName;  
        csvString += JPMCSV_Constants.COMMA_STRING; 
     
        if (accountId.length() > 15) accountId = accountId.substring(0, 15);
        csvString += accountId;
        csvString += JPMCSV_Constants.COMMA_STRING;
 
        csvString += -transactionValue;
        csvString += JPMCSV_Constants.COMMA_STRING;

        csvString += JPMCSV_Constants.COMMA_STRING;
        csvString += JPMCSV_Constants.COMMA_STRING;
        */
        accountName = JPMCSV_Util.spiltComma(accountName);
        csvString += accountName.length() > 22 ?  accountName.substring(0, 22) : accountName;  
        csvString += JPMCSV_Constants.COMMA_STRING; 
     
        //Switching back to 15 char ID based on the recent inputs from Finance team
        if (accountId.length() > 15) accountId = accountId.substring(0, 15);
        csvString += accountId;
        csvString += JPMCSV_Constants.COMMA_STRING;
 
        csvString += -transactionValue;
        csvString += JPMCSV_Constants.COMMA_STRING;

        csvString += accountRef;
        csvString += JPMCSV_Constants.COMMA_STRING;
        csvString += accountNum;
        csvString += JPMCSV_Constants.COMMA_STRING;
        
        String vendorReference = m.get(accountId) == null ? '' : m.get(accountId);
        if (vendorReference.length() > 75) {
            vendorReference = vendorReference.substring(0, 75);
            Integer index = vendorReference.lastIndexOf(VENDOR_REF_DELIMITER);
            if (index != -1) {
                vendorReference = vendorReference.substring(0, index);
            }
        }
        csvString += '"' + vendorReference + '"';
        csvString += JPMCSV_Constants.COMMA_STRING;
        csvString += 'Checking';
        return csvString; 
    }
 
    public static String padLeft(String textToPad, integer paddingLength) {
        if(textToPad.length() < paddingLength)
        for(integer i = 0; i < paddingLength-textToPad.length()+1; i++)
            textToPad = '0'+textToPad;
        return textToPad;
    }

       
}