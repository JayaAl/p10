/***********************************************
    Class: CSV_ACHtypeGDFFformat
    This class creates CSV files for Electronic (ACH) in GDFF file format
    Type PaymentMethod from Payment Object 
    Author – Lior Gotesman - Clear Task
    Date – 22/04/2013    
    Revision History    
   //nsk - 10/20: Applying changes based on the reqs from CR - ISS-11296

 ***********************************************/
 
public with sharing class JPMCSV_ACHtypeGDFFformatController {
    
    public static String paymentAsCSVforACHTypeGDFFformat(c2g__codaPayment__c paymentRecord) {
        List<String> rowList = new List<String>();
        if (paymentRecord == null) {
            return rowListConcat(rowList);
        } else {
            List<AggregateResult> paymentDetailList = JPMCSV_Util.getPaymentDetailSummary(paymentRecord.Id);
            if(paymentDetailList != null && paymentDetailList.size() > 0){   
            
                String accountId = (String)paymentDetailList[0].get('accountId');
                //accountId = (accountId.length() > 16) ? accountId.substring(0, 16) : accountId;
                accountId = (accountId.length() > 15) ? accountId.substring(0, 15) : accountId;
                
                String destinationBankId = (String)paymentDetailList[0].get('destinationBankId');
                destinationBankId = destinationBankId.left(3) + '-' + destinationBankId.right(3);               
                
            //  Create File Header    
                //rowList.add(createFileHeaderRow());
                rowList.add(createFileHeaderRow(paymentRecord));
                    
            
                Integer transactionCount = 0; //  Number of transactions in file
                Integer totalLineCount = 2;   //  Starts with 2 to include Header and Trailer rows                                
                Integer amountHashTotal = 0;  //  Total payment amount in batch, ignores currencies but observes decimal places 
                for(AggregateResult result :paymentDetailList){
                
                      String acctID = (String)result.get('accountId');
                      acctID = (acctID.length() > 15) ? acctID.substring(0, 15) : acctID;

                      String destBankID = (String)result.get('destinationBankId');
                      Date payDate = (Date)result.get('paymentDate');                      

                
                    //Create Transaction Row  
                    //rowList.add(CreateTransactionRow(result, paymentRecord, accountId, destinationBankId));
                    rowList.add(newCreateTransactionRow(result, paymentRecord, acctId, destBankId, payDate));
                    
                    transactionCount += 1;
                    totalLineCount += 1;
                    amountHashTotal += result.get('transactionValue') == null ? 0 : Integer.valueOf((Decimal)result.get('transactionValue')*-100);
                }  
            //  Create File Trailer Row
                rowList.add(createFileTrailerRow(transactionCount, totalLineCount, amountHashTotal));
            }           
            
        }
        return rowListConcat(rowList);
    }

//  Creates the First Row of the CSV file
    public static String createFileHeaderRow(c2g__codaPayment__c paymentRecord) {
        List<String> valList = new List<String>();
    //  File Header
        valList.add('FH');
    //  Client Identifier
        valList.add('Pandora');
    //  File Create Date  
        //valList.add(JPMCSV_Util.format(Date.valueOf(System.now()), 'yyyyMMdd')); 
        valList.add(JPMCSV_Util.format(paymentRecord.c2g__PaymentDate__c, 'yyyyMMdd'));
        
    //  File Create Time
        valList.add(JPMCSV_Util.formatTime(System.now(), 'hhMMss'));  
    //  Format Version
        valList.add('01100');         
        return stringListToCSVRow(valList);
    }

//  Creates CSV row for a transaction   
    public static String newCreateTransactionRow(AggregateResult result, c2g__codaPayment__c paymentRecord, String accountId, String destinationBankId, Date payDate) {
    
        List<String> valList = new List<String>();
    //  1 Record Id
        valList.add('TR');
    //  2 Originator's REF. No
        valList.add(accountId);
    //  3 Value Date
        //valList.add(JPMCSV_Util.format(Date.valueOf(System.now()), 'yyyyMMdd')); 
        valList.add(JPMCSV_Util.format(payDate, 'yyyyMMdd')); 
        
    //  4 Destination Country Code
        String countryCode = paymentRecord.c2g__BankAccount__r.c2g__BankAccountCurrency__r.CurrencyIsoCode.left(2);
        /*
        String countryByVAT = JPMCSV_Constants.vatIdCountryCodeMap.get(paymentRecord.c2g__OwnerCompany__r.c2g__VATRegistrationNumber__c);
        String countryBySUT = JPMCSV_Constants.taxIdCountryCodeMap.get(paymentRecord.c2g__OwnerCompany__r.c2g__TaxIdentificationNumber__c);
        String countryCode = '';
        if (countryBySUT != null) {
            countryCode = countryBySUT;
        } else if (countryByVAT != null) {
            countryCode = countryByVAT;
        }
        */
        valList.add(countryCode);
    //  5 Destination Bank BIC 
        valList.add('');
    //  6 Destination Bank Id 
        destinationBankId = countryCode == 'NZ' ? destinationBankId.remove('-') : destinationBankId;
        valList.add(destinationBankId);
    //  7 Destination A/C 
        valList.add((String)result.get('accountNumber'));   
    //  8 Reserved 
        valList.add('');
    //  9 Transaction Amount
        Integer transactionValue = result.get('transactionValue') == null ? 0 : Integer.valueOf((Decimal)result.get('transactionValue')*-100);   
        valList.add(String.valueOf(transactionValue));  
    //  10 Currency
        valList.add(countryCode + 'D');
        //valList.add(paymentRecord.c2g__BankAccount__r.c2g__BankAccountCurrency__r.CurrencyIsoCode);
    //  11 Payment Method
        valList.add('GIR');
    //  12 Transaction Type - Assumption is Direct Credit
        valList.add('01');
    //  13 Transaction Category - Assumption is Direct Credit
        if (countryCode == 'HK') {
            valList.add('20');  
        } else if (countryCode == 'AU' || countryCode == 'NZ') {
            valList.add('50');
        } else {
            valList.add('');
        }
    //  14 Originator's A/C No.
        valList.add(paymentRecord.c2g__BankAccount__r.c2g__AccountNumber__c);   
        if (countryCode == 'NZ') {
        //  15 - 16 Reserved
            valList = addNBlankVals(valList, 3);
        } else if (countryCode == 'HK' || countryCode == 'AU') {
        //  15 Bank Charges
            valList.add('R');
        //  16 Correspondence Charges
            valList.add('B');
        }       
    //  17 Destination A/C Name
        String accountName = result.get('accountName') == null ? '' : (String) result.get('accountName');
        accountName = JPMCSV_Util.spiltComma(accountName);
        valList.add(accountName.length() > 20 ?  accountName.substring(0, 20) : accountName);
    //  18 - 20 Reserved
        valList = addNBlankVals(valList, 4);
    //  21 Mailing Instructions N/A
        valList.add('');
    //  22 - 25 Bene/Reciever's Address 1 - 4
        valList = addNBlankVals(valList, 5);
    //  26 - 27 Reserved
        valList = addNBlankVals(valList, 3);
    //  28 - 31 Sender's Text 1 - 4
        valList = addNBlankVals(valList, 5);
    //  32 - 34 Bene/Reciever's Bank Name/Address 1 - 3
        valList = addNBlankVals(valList, 4);
    //  35 - 38 Reserved
        valList = addNBlankVals(valList, 5);
    //  39 - 40 Advising Media 1 & Advising Media Value 1
        valList = addNBlankVals(valList, 3);        
    //  41 By Order Of
        valList.add('');
    //  42 - 50 Reserved
        valList = addNBlankVals(valList, 10);
    //  51 - 54 Advising Media
        valList = addNBlankVals(valList, 5);
    //  55 Reserved
        valList.add('');                                                                                                
        return stringListToCSVRow(valList);
    }
 
//  Creates CSV row for a transaction   
    public static String createTransactionRow(AggregateResult result, c2g__codaPayment__c paymentRecord, String accountId, String destinationBankId) {
        List<String> valList = new List<String>();
    //  1 Record Id
        valList.add('TR');
    //  2 Originator's REF. No
        //valList.add(accountId);
        valList.add(accountId);
    //  3 Value Date
        valList.add(JPMCSV_Util.format(Date.valueOf(System.now()), 'yyyyMMdd')); 
        
    //  4 Destination Country Code
        String countryCode = paymentRecord.c2g__BankAccount__r.c2g__BankAccountCurrency__r.CurrencyIsoCode.left(2);
        /*
        String countryByVAT = JPMCSV_Constants.vatIdCountryCodeMap.get(paymentRecord.c2g__OwnerCompany__r.c2g__VATRegistrationNumber__c);
        String countryBySUT = JPMCSV_Constants.taxIdCountryCodeMap.get(paymentRecord.c2g__OwnerCompany__r.c2g__TaxIdentificationNumber__c);
        String countryCode = '';
        if (countryBySUT != null) {
            countryCode = countryBySUT;
        } else if (countryByVAT != null) {
            countryCode = countryByVAT;
        }
        */
        valList.add(countryCode);
    //  5 Destination Bank BIC 
        valList.add('');
    //  6 Destination Bank Id 
        destinationBankId = countryCode == 'NZ' ? destinationBankId.remove('-') : destinationBankId;
        valList.add(destinationBankId);
    //  7 Destination A/C 
        valList.add((String)result.get('accountNumber'));   
    //  8 Reserved 
        valList.add('');
    //  9 Transaction Amount
        Integer transactionValue = result.get('transactionValue') == null ? 0 : Integer.valueOf((Decimal)result.get('transactionValue')*-100);   
        valList.add(String.valueOf(transactionValue));  
    //  10 Currency
        valList.add(countryCode + 'D');
        //valList.add(paymentRecord.c2g__BankAccount__r.c2g__BankAccountCurrency__r.CurrencyIsoCode);
    //  11 Payment Method
        valList.add('GIR');
    //  12 Transaction Type - Assumption is Direct Credit
        valList.add('01');
    //  13 Transaction Category - Assumption is Direct Credit
        if (countryCode == 'HK') {
            valList.add('20');  
        } else if (countryCode == 'AU' || countryCode == 'NZ') {
            valList.add('50');
        } else {
            valList.add('');
        }
    //  14 Originator's A/C No.
        valList.add(paymentRecord.c2g__BankAccount__r.c2g__AccountNumber__c);   
        if (countryCode == 'NZ') {
        //  15 - 16 Reserved
            valList = addNBlankVals(valList, 3);
        } else if (countryCode == 'HK' || countryCode == 'AU') {
        //  15 Bank Charges
            valList.add('R');
        //  16 Correspondence Charges
            valList.add('B');
        }       
    //  17 Destination A/C Name
        String accountName = result.get('accountName') == null ? '' : (String) result.get('accountName');
        accountName = JPMCSV_Util.spiltComma(accountName);
        valList.add(accountName.length() > 20 ?  accountName.substring(0, 20) : accountName);
    //  18 - 20 Reserved
        valList = addNBlankVals(valList, 4);
    //  21 Mailing Instructions N/A
        valList.add('');
    //  22 - 25 Bene/Reciever's Address 1 - 4
        valList = addNBlankVals(valList, 5);
    //  26 - 27 Reserved
        valList = addNBlankVals(valList, 3);
    //  28 - 31 Sender's Text 1 - 4
        valList = addNBlankVals(valList, 5);
    //  32 - 34 Bene/Reciever's Bank Name/Address 1 - 3
        valList = addNBlankVals(valList, 4);
    //  35 - 38 Reserved
        valList = addNBlankVals(valList, 5);
    //  39 - 40 Advising Media 1 & Advising Media Value 1
        valList = addNBlankVals(valList, 3);        
    //  41 By Order Of
        valList.add('');
    //  42 - 50 Reserved
        valList = addNBlankVals(valList, 10);
    //  51 - 54 Advising Media
        valList = addNBlankVals(valList, 5);
    //  55 Reserved
        valList.add('');                                                                                                
        return stringListToCSVRow(valList);
    }
 
 
 
//  Creates the last Row of the CSV file
    public static String createFileTrailerRow(Integer transactionCount, Integer totalLineCount, Integer amountHashTotal) {
        List<String> valList = new List<String>{
            'FT',
            String.valueOf(transactionCount),
            String.valueOf(totalLineCount),
            String.valueOf(amountHashTotal)
        };
        return stringListToCSVRow(valList);
    }
    
//  Adds N blank values to value list
    public static List<String> addNBlankVals(List<String> valList, Integer n) {
        for (Integer i=1; i < n; i++) valList.add('');
        return valList;
    }   
    
//  Returns string from string list joined by comma, ending in line break   
    public static String stringListToCSVRow(List<String> valList) {
        if(valList.isEmpty()) return null;
        String result = valList[0];
        valList.remove(0);
        while(!valList.isEmpty()) {
            result += JPMCSV_Constants.COMMA_STRING + valList[0];
            valList.remove(0);
        }       
        return result + JPMCSV_Constants.NEW_LINE_CHARACTER;
    }
    
    public static String rowListConcat(List<String> rowList) {
        String csvString = '';
        for (String row : rowList) {
            csvString += row;
        }
        return csvString;
    }
    
    
}