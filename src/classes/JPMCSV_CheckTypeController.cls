/***********************************************
    Class: CSV_CheckTypeController
    This class creates CSV files for Check Type
    PaymentMethod from Payment Object 
    Author – Cleartask
    Date – 29/08/2011    
    Revision History    
 ***********************************************/
public class JPMCSV_CheckTypeController {    
    
    /* method to get rows for Check type */
    public static String paymentAsCSVforCheckType(c2g__codaPayment__c paymentRecord) {        
        String csvString ='';
        Integer index = 0;
        String bankAccountNumber = '';
        if(paymentRecord == null){
            return csvString; 
        }
            
        //Map<Id, Decimal> paymentSummaryMap = JPMCSV_Util.getPaymentSummaryMap(paymentRecord.Id);
        Map<Id, Decimal> paymentTotalValueMap = new Map<Id, Decimal>();
        Map<Id, String> accdDupMap = new Map<Id, String>();
        List<c2g__codaPaymentMediaSummary__c> mediaSummaryList;
        Set<Id> mediaControlIds = new Set<Id>();
        
        List<c2g__codaPaymentLineItem__c> paymentDetailList = new  List<c2g__codaPaymentLineItem__c>(JPMCSV_Util.getPaymentDetail(paymentRecord.Id));
        
        for(c2g__codaPaymentLineItem__c m :paymentDetailList){
            if(paymentTotalValueMap != null && paymentTotalValueMap.containsKey(m.c2g__Account__c)){
                Decimal amt = paymentTotalValueMap.get(m.c2g__Account__c);
                amt += m.c2g__TransactionValue__c;
                paymentTotalValueMap.put(m.c2g__Account__c, amt);
            }else{
                paymentTotalValueMap.put(m.c2g__Account__c, m.c2g__TransactionValue__c);
            }
        }
        
        c2g__codaBankAccount__c bankAccountRecord = new c2g__codaBankAccount__c();  
        
        if(paymentRecord.c2g__BankAccount__c != null){
            bankAccountRecord = [Select c.c2g__ZipPostalCode__c, c.c2g__UnitOfWork__c, 
                c.c2g__Street__c, c.c2g__StateProvince__c, c.c2g__SortCode__c, 
                c.c2g__ReportingCode__c, c.c2g__Phone__c ,c.c2g__BankName__c, 
                c.c2g__AccountNumber__c, c.c2g__AccountName__c, c.Name, c.Id 
                From c2g__codaBankAccount__c c where Id = :paymentRecord.c2g__BankAccount__c]; 
            if (bankAccountRecord != null){
                bankAccountNumber = bankAccountRecord.c2g__AccountNumber__c;    
            }
            
        }
        List<c2g__codaPaymentMediaControl__c> mediaControlList = JPMCSV_Util.getMediaList(paymentRecord);
        if(mediaControlList != null && mediaControlList.size() > 0){
            for(c2g__codaPaymentMediaControl__c i :mediaControlList){                
                mediaControlIds.add(i.Id);
            }
            mediaSummaryList = JPMCSV_Util.getMediaSummaryList(mediaControlIds);
        }
        
        Map<String, c2g__codaPaymentMediaSummary__c> mediaSummaryMap = new Map<String, c2g__codaPaymentMediaSummary__c>();
        for (c2g__codaPaymentMediaSummary__c mediaSummary :  mediaSummaryList) {
            mediaSummaryMap.put(mediaSummary.c2g__Account__c, mediaSummary);
        }
        
        List<c2g__codaPaymentMediaDetail__c> mediaDetailList = [Select c2g__Account__c , c.c2g__VendorReference__c, c.c2g__DocumentNumber__c From c2g__codaPaymentMediaDetail__c c where c2g__PaymentMediaSummary__r.c2g__PaymentMediaControl__r.c2g__Payment__c = :paymentRecord.id];
        Map<String, String> mapReferenceNumber = new Map<String, String>();
        for(c2g__codaPaymentMediaDetail__c pmd :mediaDetailList){
            mapReferenceNumber.put(pmd.c2g__DocumentNumber__c, pmd.c2g__VendorReference__c);
        }
        
        if(paymentDetailList != null && paymentDetailList.size() > 0){
            
            /* call method for First Row */
            csvString += paymentAsCSVFirstRow();
            index ++;
            
            for(c2g__codaPaymentLineItem__c m :paymentDetailList){
                if(!accdDupMap.containsKey(m.c2g__Account__c)){
                    
                    /* call method for Second Row */
                    if(paymentTotalValueMap != null){
                        csvString += paymentAsCSVSecondRow(paymentRecord, paymentTotalValueMap, m, bankAccountNumber, mediaSummaryMap);
                        index ++;
                    }
                    
                    /* call method for Third Row */
                    csvString += paymentAsCSVThirdRow(m);
                    index ++;
                    
                    /* call method for Fourth and Fifth Row */ 
                    csvString += paymentAsCSVFourthRow(m);
                    index ++;
                    index ++;
                    
                    /* call method for Sixth Row */ 
                    csvString += paymentAsCSVSixthRow(m);
                    index ++;
                    
                    /* call method for Seventh Row */
                    csvString += paymentAsCSVSeventhRow(m, mapReferenceNumber);
                    index ++;
                    
                    accdDupMap.put(m.c2g__Account__c, csvString);
                }else if(accdDupMap.containsKey(m.c2g__Account__c)){
                     
                    /* call method for Seventh Row (if more than one records for a single account) */
                    csvString += paymentAsCSVSeventhRow(m, mapReferenceNumber);
                    index ++;
                }
            }
            /* Last Row -- Start */
            csvString += paymentAsCSVLastRow(index);
        }
 
        return csvString;
    }
    
    /* method for Second row for Check type of Excel Sheet */
    public static String paymentAsCSVSecondRow(c2g__codaPayment__c paymentRecord, Map<Id, Decimal> paymentTotalValueMap, 
        c2g__codaPaymentLineItem__c paymentDetail, String bankAccountNumber, 
        Map<String, c2g__codaPaymentMediaSummary__c> mediaSummaryMap) 
    {   
        String csvSecString = '';
        if(paymentDetail == null){
            return csvSecString;
        }else if (paymentDetail != null){
            csvSecString += JPMCSV_Constants.NEW_LINE_CHARACTER;
            String secondColumn = '';
            String thirdColumn = '';
            secondColumn = JP_Morgan_CSV_File__c.getValues('PMTHDR').Second_Column__c;
            thirdColumn = JP_Morgan_CSV_File__c.getValues('PMTHDR').Third_Column__c;
            csvSecString += 'PMTHDR';
            csvSecString += JPMCSV_Constants.COMMA_STRING; 
            csvSecString += secondColumn;
            csvSecString += JPMCSV_Constants.COMMA_STRING; 
            csvSecString += thirdColumn;
            csvSecString += JPMCSV_Constants.COMMA_STRING; 
            if(paymentRecord.c2g__PaymentDate__c != null){
                csvSecString += JPMCSV_Util.format(paymentRecord.c2g__PaymentDate__c, 'MM/dd/yyyy'); 
            }
            csvSecString += JPMCSV_Constants.COMMA_STRING; 
            if(paymentTotalValueMap != null && paymentTotalValueMap.containskey(paymentDetail.c2g__Account__c)){
                Decimal paymentValue = paymentTotalValueMap.get(paymentDetail.c2g__Account__c);
                csvSecString += -paymentValue;
            }
            csvSecString += JPMCSV_Constants.COMMA_STRING; 
            if(bankAccountNumber != null){         
                csvSecString += bankAccountNumber;  
            }   
            csvSecString += JPMCSV_Constants.COMMA_STRING;
            /*if(mediaSummaryList != null && mediaSummaryList.size() > 0 && mediaSummaryList[0].c2g__PaymentReference__c != null){
                 csvSecString += mediaSummaryList[0].c2g__PaymentReference__c;     
            }*/
            c2g__codaPaymentMediaSummary__c mediaSummary = mediaSummaryMap.get(paymentDetail.c2g__Account__c);
            if (mediaSummary != null) {
                csvSecString += mediaSummary.c2g__PaymentReference__c;
            }
        }
        return csvSecString;
    }
    
    
    /* method for first row for Check type of Excel Sheet */
    public static String paymentAsCSVFirstRow() {   
        String csvStrString = ''; 
        csvStrString += 'FILHDR,PWS,Pandora,' + JPMCSV_Util.format(System.today(), 'MM/dd/yyyy') ;
        csvStrString += JPMCSV_Constants.COMMA_STRING;
        Integer hour = System.now().hour();
        String hourValue;
        if(hour <= 9){
            hourValue = '0'+ String.valueof(hour);
            csvStrString += hourValue;       
        }else{
            hourValue = String.valueof(hour);
            csvStrString += hourValue;  
        } 
                    
        Integer min = System.now().minute();
        String minValue;
        if(min <= 9){
            minValue = '0'+ String.valueof(min);
            csvStrString += minValue;    
        } else{
            minValue = String.valueof(min);
            csvStrString += minValue;   
        }
        return csvStrString;
    }
    
    
    /* method for Third row for Check type of Excel Sheet */
    public static String paymentAsCSVThirdRow(c2g__codaPaymentLineItem__c paymentDetail) {   
        String csvThirdString = '';
        if(paymentDetail == null){
            return csvThirdString;
        }else if (paymentDetail!= null){
            csvThirdString += JPMCSV_Constants.NEW_LINE_CHARACTER;
            csvThirdString += 'PAYENM';
            csvThirdString += JPMCSV_Constants.COMMA_STRING;
            String lastName = '';
            String firstname = '';
            if(paymentDetail.c2g__Account__r.Name != null){
                 String accName =  JPMCSV_Util.spiltComma(paymentDetail.c2g__Account__r.Name);
                 if(accName != null && accName.length() > 35){
                       firstname = accName.substring(0, 35);    
                       lastName = accName.substring(35);
                       csvThirdString += firstname; 
                       csvThirdString += JPMCSV_Constants.COMMA_STRING;
                       csvThirdString += lastName ; 
                       
                 }else{
                     csvThirdString += accName; 
                     csvThirdString += JPMCSV_Constants.COMMA_STRING;
                 }
            }
            csvThirdString += JPMCSV_Constants.COMMA_STRING;
            csvThirdString += '1'; //payee Id...always hard coded to '1"
          /*  if(paymentDetail.c2g__Account__c != null){
                csvThirdString += paymentDetail.c2g__Account__c;
            }*/
        }
        return csvThirdString;
    }
    
    /* method for fourth & Fifth row for Check type of Excel Sheet */
    public static String paymentAsCSVFourthRow(c2g__codaPaymentLineItem__c paymentDetail) {   
        String csvStringFourth = '';
        if(paymentDetail == null){
            return csvStringFourth;
        }else if (paymentDetail != null){
            csvStringFourth += JPMCSV_Constants.NEW_LINE_CHARACTER;
            csvStringFourth += 'PYEADD';
            csvStringFourth += JPMCSV_Constants.COMMA_STRING;
            String billingStreet = '' ;
            billingStreet = JPMCSV_Util.spiltComma(paymentDetail.c2g__Account__r.BillingStreet);
            List<String> l = billingStreet.split('\r\n');
            String s = l.get(0);
            String s1 = (l.size() > 1) ? l.get(1) : '';
            String lastAdd = '';
            String firstAdd = '' ;
            csvStringFourth += (s.length() > 35) ? s.substring(0,35) : s; 
            csvStringFourth += JPMCSV_Constants.COMMA_STRING;
            csvStringFourth += (s1.length() > 35) ? s1.substring(0,35) : s1;

            csvStringFourth += JPMCSV_Constants.NEW_LINE_CHARACTER;
            csvStringFourth += 'ADDPYE';
            csvStringFourth += JPMCSV_Constants.COMMA_STRING;
            csvStringFourth += JPMCSV_Constants.COMMA_STRING;
        }
        return csvStringFourth;
    }
    
    /* method for Sixth row for Check type of Excel Sheet */
    public static String paymentAsCSVSixthRow(c2g__codaPaymentLineItem__c paymentDetail) {   
        String csvStringSixth = '';
        if(paymentDetail == null){
            return csvStringSixth;
        }else if (paymentDetail != null){
            csvStringSixth += JPMCSV_Constants.NEW_LINE_CHARACTER;
            csvStringSixth += 'PYEPOS';
            csvStringSixth += JPMCSV_Constants.COMMA_STRING;  
            if(paymentDetail.c2g__Account__r.BillingCity!= null){
                csvStringSixth += paymentDetail.c2g__Account__r.BillingCity; 
            }    
            csvStringSixth += JPMCSV_Constants.COMMA_STRING; 
            if(paymentDetail.c2g__Account__r.BillingState!= null){
                csvStringSixth += paymentDetail.c2g__Account__r.BillingState; 
            }
            csvStringSixth += JPMCSV_Constants.COMMA_STRING;  
            if(paymentDetail.c2g__Account__r.BillingPostalCode!= null){
                csvStringSixth += paymentDetail.c2g__Account__r.BillingPostalCode; 
            }
            csvStringSixth += JPMCSV_Constants.COMMA_STRING;  
            if(paymentDetail.c2g__Account__r.BillingCountry!= null){
               String country = JPMCSV_Constants.countryCodeMap.get(paymentDetail.c2g__Account__r.BillingCountry);
               if(country  != null){
                   csvStringSixth += country;   
               } else {
                   csvStringSixth += paymentDetail.c2g__Account__r.BillingCountry;
               }
           }
        }
        return csvStringSixth;
    }
    
    /* method for Seventh row for Check type of Excel Sheet */
    public static String paymentAsCSVSeventhRow(c2g__codaPaymentLineItem__c paymentDetail, Map<String, String> mapReferenceNumber) {   
        String csvStringSeventh = '';
        if(paymentDetail == null){
            return csvStringSeventh;
        }else if (paymentDetail != null){
            csvStringSeventh += JPMCSV_Constants.NEW_LINE_CHARACTER;
            csvStringSeventh += 'RMTDTL';
            csvStringSeventh += JPMCSV_Constants.COMMA_STRING; 
            
            if(mapReferenceNumber != null && mapReferenceNumber.containsKey(paymentDetail.c2g__DocumentNumber__c)){
                String str = mapReferenceNumber.get(paymentDetail.c2g__DocumentNumber__c);
                str = (str == null) ? '' : str;
                if(str.length() > 35){
                    csvStringSeventh += mapReferenceNumber.get(paymentDetail.c2g__DocumentNumber__c).substring(0, 35);
                }else{
                    csvStringSeventh += mapReferenceNumber.get(paymentDetail.c2g__DocumentNumber__c);
                }
            }
            csvStringSeventh += JPMCSV_Constants.COMMA_STRING;   

            if(paymentDetail.c2g__DocumentNumber__c != null){
                csvStringSeventh += paymentDetail.c2g__DocumentNumber__c; 
            }
            csvStringSeventh += JPMCSV_Constants.COMMA_STRING;

           /* if(paymentDetail.c2g__TransactionDate__c != null){
                String month = paymentDetail.c2g__TransactionDate__c.month() <= 9 ? String.valueOf('0' + paymentDetail.c2g__TransactionDate__c.month()) : String.valueOf(paymentDetail.c2g__TransactionDate__c.month());
                String day = paymentDetail.c2g__TransactionDate__c.day() <= 9 ? String.valueOf('0' + paymentDetail.c2g__TransactionDate__c.day()) : String.valueOf(paymentDetail.c2g__TransactionDate__c.day());
                csvStringSeventh += month + '/' + day + '/' + paymentDetail.c2g__TransactionDate__c.year(); 
                
            }*/
            csvStringSeventh += JPMCSV_Util.format(paymentDetail.c2g__TransactionDate__c, 'MM/dd/yyyy');
            csvStringSeventh += JPMCSV_Constants.COMMA_STRING;   
            if(paymentDetail.c2g__TransactionValue__c != null){
                csvStringSeventh += -paymentDetail.c2g__TransactionValue__c;
            } 
            csvStringSeventh += JPMCSV_Constants.COMMA_STRING;    
            if(paymentDetail.c2g__TransactionValue__c != null){
                csvStringSeventh += -paymentDetail.c2g__TransactionValue__c;
            } 
            csvStringSeventh += JPMCSV_Constants.COMMA_STRING; 
            csvStringSeventh += '0';  
        }
        return csvStringSeventh;
    }
    
    /* method for last row for Check type of Excel Sheet */
    public static String paymentAsCSVLastRow(Integer index) { 
        String strCsvString = '';
        strCsvString += JPMCSV_Constants.NEW_LINE_CHARACTER;
        index ++;
        strCsvString += 'FILTRL';
        strCsvString += JPMCSV_Constants.COMMA_STRING;
        strCsvString += String.valueof(index); 
        return  strCsvString;   
    }
}