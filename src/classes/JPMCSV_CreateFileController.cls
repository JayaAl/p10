/***********************************************
    Class: JPMCSV_CreateFileController
    This class creates different CSV files depending upon 
    different PaymentMethod from Payment Object 
    Author – Cleartask
    Date – 29/08/2011    
    Revision History    
 ***********************************************/
public with sharing class JPMCSV_CreateFileController {
    
    /* variable for StandardSetController */
    ApexPages.StandardSetController setCon;
    
    /* list of payment records which are selected */
    public List<c2g__codaPayment__c> paymentSelectedList { get;set; }
    
    /* list of created documents */
    public List<Document> docList { get; set;}
    
    /* boolean variable to render the section on page */
    public Boolean flag { get; set; } 
    
    /* boolean variable to render the buttons on page */
    public Boolean showButton { get; set; } 
    
    /* list of wrapper class */
    public List<CSVWrapper> wrapList { get; set; } 

    /* Map payment Id and boolean indicating whether file format GDFF */
    private Map<String, Boolean> paymentGDFFMap = new Map<String, Boolean>();
    
    public JPMCSV_CreateFileController(ApexPages.StandardSetController controller) {
        setCon = controller;
        
        /* show error message if no record is selected */
        if(setCon.getSelected().size() <= 0){
            ApexPages.Message msg = new ApexPages.Message(ApexPages.Severity.FATAL, Label.CSV_No_Record_Selected);
            ApexPages.addMessage(msg);
            showButton = true;
        }
        paymentRecordList();
    }
    
    /* method to query payment record and related payment line item record */
    public void paymentRecordList(){
        wrapList = new List<CSVWrapper>();
        
        /* set of selected payment record id */
        Set<Id> paymentId = new Set<Id>();
        
        /* list for related payment line items */
        List<c2g__codaPaymentLineItem__c> paymentDetailList = new  List<c2g__codaPaymentLineItem__c>();
        
        /* list of selected payment records */
        List<c2g__codaPayment__c> paymentSelected = setCon.getSelected(); 
        
        for(c2g__codaPayment__c c :paymentSelected){
            paymentId.add(c.Id);    
        }
        System.debug('paymentId = ' + paymentId);
        if(paymentId != null && paymentId.size()>0){
            paymentSelectedList = [Select c.c2g__UnitOfWork__c, c.c2g__Status__c, 
                c.c2g__SettlementDiscountReceived__c, c.c2g__SDRDimension4__c, c.c2g__SDRDimension3__c, 
                c.c2g__SDRDimension2__c, c.c2g__SDRDimension1__c, c.c2g__Period__c, 
                c.c2g__PaymentValue__c, c.c2g__PaymentValueTotal__c, c.c2g__PaymentValueRollup__c, 
                c.c2g__PaymentTypes__c, c.c2g__PaymentTemplate__c, c.c2g__PaymentMethod__c, 
                c.c2g__PaymentMediaTypes__c, c.c2g__PaymentDate__c, c.c2g__PaymentCurrency__c, 
                c.c2g__OwnerCompany__c, c.c2g__OwnerCompany__r.c2g__TaxIdentificationNumber__c, 
                c.c2g__GrossValue__c, c.c2g__GrossValueTotal__c, c.c2g__OwnerCompany__r.c2g__VATRegistrationNumber__c,
                c.c2g__GrossValueRollup__c, c.c2g__GeneralLedgerAccount__c, c.c2g__ExternalId__c, 
                c.c2g__ErrorControl__c, c.c2g__DueDate__c, c.c2g__DocumentCurrency__c, 
                c.c2g__Discount__c, c.c2g__DiscountTotal__c, c.c2g__DiscountRollup__c, 
                c.c2g__DiscardReason__c, c.c2g__Description__c, c.c2g__CurrencyWriteOff__c, 
                c.c2g__CurrencyMode__c, c.c2g__ClickedPay__c, c.c2g__CWODimension4__c, 
                c.c2g__CWODimension3__c, c.c2g__CWODimension2__c, c.c2g__CWODimension1__c, 
                c.c2g__BankAccount__c, c.c2g__AccountCurrency__c, c.c2g__BankAccount__r.c2g__BankAccountCurrency__r.CurrencyIsoCode, 
                c.Name, c.Unique_Id__c, c.Id, c.c2g__BankAccount__r.c2g__AccountNumber__c, c.c2g__BankAccount__r.c2g__ReportingCode__c,
                c.CurrencyIsoCode From c2g__codaPayment__c c
                where id in :paymentId ];  //and c2g__Status__c = 'Matched'
            
            System.debug('paymentSelectedList = ' + paymentSelectedList.size());
            /* show error message if the selected records status is not 'Matched' */
            if(paymentSelectedList.isEmpty()){
                ApexPages.Message msg = new ApexPages.Message(ApexPages.Severity.FATAL, Label.CSV_Status_Should_Be_Equal_To_Matched);
                ApexPages.addMessage(msg);
                showButton = true;
            }
        
            paymentDetailList = [Select c.c2g__UnitOfWork__c, c.c2g__Transaction__c, 
                c.c2g__TransactionValue__c, c.c2g__TransactionSelected__c, c.c2g__TransactionLineItem__c, 
                c.c2g__TransactionDate__c, c.c2g__Payment__c, c.c2g__PaymentSummary__c, 
                c.c2g__PaymentAccountLineItem__c, c.c2g__LineNumber__c, c.c2g__GrossValue__c, 
                c.c2g__ExternalId__c, c.c2g__DueDate__c, c.c2g__DocumentNumber__c, c.c2g__Discount__c, 
                c.c2g__BelongToCurrentRefine__c, c.c2g__Account__c, c.Name, c.Id, c.c2g__Account__r.Name, 
                c.c2g__Account__r.c2g__CODABankAccountNumber__c, c.c2g__Account__r.Description, 
                c.c2g__Account__r.BillingStreet, c.c2g__Account__r.BillingState, 
                c.c2g__Account__r.BillingCountry, c.c2g__Account__r.BillingPostalCode, 
                c.c2g__Account__r.BillingCity, c.c2g__Account__r.AccountNumber, 
                c.c2g__Account__r.c2g__CODABankAccountReference__c, 
                c.c2g__PaymentSummary__r.c2g__PaymentValue__c, 
                c2g__Payment__r.c2g__BankAccount__r.c2g__AccountNumber__c, 
                c.c2g__Account__r.c2g__CODABankSortCode__c
                From c2g__codaPaymentLineItem__c c where c2g__Payment__c = :paymentId  
                And ((c2g__DocumentNumber__c like 'PIN%' AND c2g__Payment__r.c2g__GeneralLedgerAccount__r.Name = 'Accounts Payable')
                    OR c2g__Payment__r.c2g__GeneralLedgerAccount__r.Name = 'Accounts Receivable')
                and c2g__TransactionSelected__c = true 
                  and c2g__PaymentSummary__r.c2g__AccountSelected__c = true 
                order by c.c2g__Account__r.Name];
            
            System.debug('paymentDetailList = ' + paymentDetailList.size());
            /* map to combine the list of payment line items record per Account */
            Map<Id, List<c2g__codaPaymentLineItem__c>> accPaymentMap = new Map<Id, List<c2g__codaPaymentLineItem__c>>();
            List<c2g__codaPaymentLineItem__c> paymentList = new List<c2g__codaPaymentLineItem__c>(); 
            
            if(paymentDetailList!= null && paymentDetailList.size()>0){   
                for(c2g__codaPaymentLineItem__c m :paymentDetailList){
                    if(accPaymentMap != null && accPaymentMap.containsKey(m.c2g__Payment__c)){
                        paymentList = accPaymentMap.get(m.c2g__Payment__c);
                        if(m.c2g__TransactionValue__c < 0){
                            m.c2g__TransactionValue__c = Math.abs(m.c2g__TransactionValue__c);
                        }else{
                            m.c2g__TransactionValue__c = -Math.abs(m.c2g__TransactionValue__c);
                        }
                        paymentList.add(m);
                        accPaymentMap.put(m.c2g__Payment__c, paymentList);
                    }else{
                        paymentList = new List<c2g__codaPaymentLineItem__c>();
                        if(m.c2g__TransactionValue__c < 0){
                            m.c2g__TransactionValue__c = Math.abs(m.c2g__TransactionValue__c);
                        }else{
                            m.c2g__TransactionValue__c = -Math.abs(m.c2g__TransactionValue__c);
                        }
                        paymentList.add(m);
                        accPaymentMap.put(m.c2g__Payment__c, paymentList);
                    }
                }
            }
            
            for(c2g__codaPayment__c c :paymentSelectedList){
                paymentGDFFMap.put(c.Id, JPMCSV_Constants.taxVatIdsGDFF.contains(c.c2g__OwnerCompany__r.c2g__TaxIdentificationNumber__c) || JPMCSV_Constants.taxVatIdsGDFF.contains(c.c2g__OwnerCompany__r.c2g__VATRegistrationNumber__c));
                List<c2g__codaPaymentLineItem__c> paymentdetailRecordList = new List<c2g__codaPaymentLineItem__c>();
                if(accPaymentMap != null && accPaymentMap.containsKey(c.Id)){
                    paymentdetailRecordList = accPaymentMap.get(c.Id);
                }
                List<selectOption> paymentOptions = paymentMethodType(c.c2g__PaymentMethod__c);
                Decimal grossValueTotal = Math.abs(c.c2g__GrossValueTotal__c);
                wrapList.add(new CSVWrapper(c, paymentdetailRecordList, paymentOptions, grossValueTotal)); 
            }
        }
    }
    
    public List<selectOption> paymentMethodType(String paymentMethod){
        List<selectOption> paymentOptions = new List<selectOption>();
        paymentOptions.add(new selectOption('', '--- None ---'));
        if(paymentMethod.equals(JPMCSV_Constants.CHECK)){
            paymentOptions.add(new SelectOption(JPMCSV_Constants.CHECK, JPMCSV_Constants.CHECK));
            paymentOptions.add(new SelectOption('Pos', 'Pos'));
        }
        if(paymentMethod.equals('Electronic')){
            paymentOptions.add(new SelectOption(JPMCSV_Constants.ACH, 'ACH'));
            paymentOptions.add(new SelectOption(JPMCSV_Constants.WIRE, 'Wire'));
        }
        return paymentOptions;
    }
    
    /* method to create the CSV files for different payment methods */
    public PageReference save(){
        
        /* show error message if no selected lists are available */
        if(paymentSelectedList == null || paymentSelectedList.isEmpty()){
            ApexPages.Message msg = new ApexPages.Message(ApexPages.Severity.FATAL, Label.CSV_No_List_Available);
            ApexPages.addMessage(msg);
            return null;
        }
        
        Map<Id, String> mapPaymentMethodType = new Map<Id, String>();
        System.debug('---wrapList---' + wrapList);
        for(CSVWrapper w :wrapList){
            System.debug('------' + w.selectedType);
            if(w.selectedType == null || w.selectedType == ''){
                ApexPages.Message msg = new ApexPages.Message(ApexPages.Severity.FATAL, 'Please select Payment Method Type.');
                ApexPages.addMessage(msg);
                return null;
            }
            mapPaymentMethodType.put(w.paymentRecord.id, w.selectedType);
        }
        
        showButton = true;
        List<Folder> folderList = [Select f.Type, f.SystemModstamp, f.NamespacePrefix, f.Name, 
        f.LastModifiedDate, f.LastModifiedById, f.IsReadonly, f.Id, f.DeveloperName, f.CreatedDate, 
        f.CreatedById, f.AccessType From Folder f where f.Name = :JPMCSV_Constants.FOLDER_NAME LIMIT 1];
        docList = new List<Document>();
        List<c2g__codaPayment__c> paymentListToUpdate = new List<c2g__codaPayment__c>();
        for(c2g__codaPayment__c s :paymentSelectedList){
            Document  doc = new Document ();
            String fileName = '';
            String content = '';
            if(mapPaymentMethodType != null && mapPaymentMethodType.get(s.id).equals(JPMCSV_Constants.CHECK)){
                fileName = 'Check Print' + s.Name + JPMCSV_Constants.CSV_EXTENSION;
                content = JPMCSV_CheckTypeController.paymentAsCSVforCheckType(s);  
            }else if (mapPaymentMethodType != null && mapPaymentMethodType.get(s.id).equals(JPMCSV_Constants.ACH)){
                fileName = 'ACH' + s.Name + JPMCSV_Constants.CSV_EXTENSION;
            	// Check if payment requires a csv in GDFF format
                if (paymentGDFFMap.get(s.Id)) {
                	content = JPMCSV_ACHtypeGDFFformatController.paymentAsCSVforACHTypeGDFFformat(s);
                } else {
                	content = JPMCSV_ACHTypeController.paymentAsCSVforACHType(s);	
                }     
          
            }else if (mapPaymentMethodType != null && mapPaymentMethodType.get(s.id).equals('Pos')){
                fileName = 'Pos' + s.Name + JPMCSV_Constants.CSV_EXTENSION;
                content = JPMCSV_POSTypeController.paymentAsCSVforPWSPosPayType(s);     

            }else if (mapPaymentMethodType != null && mapPaymentMethodType.get(s.id).equals(JPMCSV_Constants.WIRE)){
                fileName = 'JPM Wire' + s.Name + JPMCSV_Constants.CSV_EXTENSION;
                content = JPMCSV_WireTypeController.paymentAsCSVforWireType(s);     
            }
            doc.Body = (Blob.valueof(content));
            doc.ContentType  = JPMCSV_Constants.CSV_CONTENT_TYPE;
            doc.Name = fileName;
            doc.FolderId = folderList[0].Id;
            docList.add(doc);
            //s.Last_File_Run__c = System.now();
            paymentListToUpdate.add(s);
        }
        try{
            if(docList != null && docList.size() > 0){
                insert docList;
                if(docList[0].Id != null){
                    flag = true;
                }
             }
             if(paymentListToUpdate != null && paymentListToUpdate.size() > 0){
                 //update paymentListToUpdate;
             }
        }catch(Exception e){
                //ApexPages.Message msg = new ApexPages.Message(ApexPages.Severity.FATAL, e.getMessage());
                //ApexPages.addMessage(msg);
        }
        return  null;        
    }
    
    
    
    /*Wrapper Class*/
    public class CSVWrapper {
        
        public c2g__codaPayment__c paymentRecord { get;set; }
        public List<c2g__codaPaymentLineItem__c> paymentDetailList { get;set; }
        public String selectedType { get;set; }
        /* picklist options for Payment method type */
        public List<selectOption> paymentOptions { get;set; }
        public Decimal grossValueTotal { get;set; }
        
        
        public CSVWrapper (c2g__codaPayment__c paymentRecord, List<c2g__codaPaymentLineItem__c> paymentDetailList, List<selectOption> paymentOptions, Decimal grossValueTotal) {
            this.grossValueTotal = grossValueTotal;
            this.paymentRecord = paymentRecord;
            this.paymentDetailList = paymentDetailList;
            this.paymentOptions = paymentOptions;
            this.selectedType = selectedType;
        }
    }
}