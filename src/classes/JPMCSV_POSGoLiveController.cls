/***********************************************
    Class: CSV_POSGoLiveController
    This class creates CSV files for POS Type
    PaymentMethod one time when the system 
    goes live. 
    Author – Cleartask
    Date – 29/08/2011    
    Revision History    
 ***********************************************/
public with sharing class JPMCSV_POSGoLiveController {
    
    /* list of created documents */
    public List<Document> docList { get; set;}
    
    /* list of Transaction Line Item Records */
    public List<c2g__codaTransactionLineItem__c> transactionLineItemList { get; set;}
    
    /* flag to render section of created files */
    public Boolean flag { get; set;}
    
    public JPMCSV_POSGoLiveController() {
        transactionLineItemRecords();
    }
    
    /* method to query Transaction Line Item */
    public void transactionLineItemRecords(){
        transactionLineItemList = new List<c2g__codaTransactionLineItem__c>(
            [Select c.c2g__LineType__c, c.c2g__BankReconciliationStatus__c, 
            c.c2g__BankAccount__c, c.c2g__Account__r.Name, c.c2g__Account__c, 
            c.c2g__BankAccountValue__c, c.c2g__Transaction__r.c2g__DocumentReference__c, 
            c.c2g__Transaction__r.c2g__TransactionDate__c,
            c.c2g__BankAccount__r.c2g__AccountNumber__c 
            From c2g__codaTransactionLineItem__c c where c2g__LineType__c = :JPMCSV_Constants.LINE_TYPE_ANALYSIS 
            and c2g__BankAccount__r.Name = :JPMCSV_Constants.BANK_ACCOUNT 
            and c2g__Transaction__r.c2g__TransactionType__c = :JPMCSV_Constants.CASH
            and c2g__Transaction__r.c2g__DocumentReference__c != ''
            and c2g__BankReconciliationStatus__c != :JPMCSV_Constants.STATUS_COMPLETE]);
    }
    
    
    /* create positive pay file */
    public PageReference save(){
        flag = true;
        List<Folder> folderList = [Select f.Type, f.SystemModstamp, f.NamespacePrefix, f.Name, 
            f.LastModifiedDate, f.LastModifiedById, f.IsReadonly, f.Id, f.DeveloperName, f.CreatedDate, 
            f.CreatedById, f.AccessType From Folder f where f.Name = :JPMCSV_Constants.FOLDER_NAME LIMIT 1];
        docList = new List<Document>();
        Document  doc = new Document ();
        String fileName = '';
        String content = '';
        String csvString = '';
        for(c2g__codaTransactionLineItem__c s :transactionLineItemList){
            Boolean isNumber = numericMatch(s.c2g__Transaction__r.c2g__DocumentReference__c);
            if(isNumber){
                csvString += 'I';
                csvString += JPMCSV_Constants.COMMA_STRING;
                if(s.c2g__BankAccount__r.c2g__AccountNumber__c != null){
                    csvString += s.c2g__BankAccount__r.c2g__AccountNumber__c;
                }
                csvString += JPMCSV_Constants.COMMA_STRING;
                if(s.c2g__Transaction__r.c2g__DocumentReference__c != null){
                    csvString += s.c2g__Transaction__r.c2g__DocumentReference__c;
                }
                csvString += JPMCSV_Constants.COMMA_STRING;
                /*if(s.c2g__Transaction__r.c2g__TransactionDate__c != null){
                    Date transactionDate = s.c2g__Transaction__r.c2g__TransactionDate__c;
                    String month = transactionDate.month() <= 9 ? String.valueOf('0' + transactionDate.month()) : String.valueOf(transactionDate.month());
                    String day = transactionDate.day() <= 9 ? String.valueOf('0' + transactionDate.day()) : String.valueOf(transactionDate.day());
                    csvString +=  month + day + String.valueOf(transactionDate.year()).substring(2,4); 
                }*/
                csvString +=  JPMCSV_Util.format(s.c2g__Transaction__r.c2g__TransactionDate__c, 'MMddyy');
                csvString += JPMCSV_Constants.COMMA_STRING;
                if(s.c2g__BankAccountValue__c != null){
                    csvString += -s.c2g__BankAccountValue__c;
                }
                csvString += JPMCSV_Constants.COMMA_STRING;
                csvString += JPMCSV_Constants.COMMA_STRING;
                
                String accName = '';
                String firstName = '';
                String lastName = '';
                if(s.c2g__Account__r.Name != null){
                    if(s.c2g__Account__r.Name.contains(',')){
                        if(s.c2g__Account__r.Name.startsWith('"') && s.c2g__Account__r.Name.endsWith('"')){
                            accName = s.c2g__Account__r.Name;
                            if(s.c2g__Account__r.Name.length() > 50){
                                firstName = s.c2g__Account__r.Name.subString(0, 49);
                                lastName = s.c2g__Account__r.Name.substring(50);
                                accName = firstName + JPMCSV_Constants.COMMA_STRING + lastName;
                            }else{
                                accName = s.c2g__Account__r.Name;
                            }
                        }else{
                            accName = JPMCSV_Util.spiltComma(s.c2g__Account__r.Name);
                        }
                    }else{
                        if(s.c2g__Account__r.Name.length() > 50){
                                firstName = s.c2g__Account__r.Name.subString(0, 50);
                                lastName = s.c2g__Account__r.Name.substring(50);
                                accName = firstName + JPMCSV_Constants.COMMA_STRING + lastName;
                            }else{
                                accName = s.c2g__Account__r.Name;
                        }    
                    }
                    csvString += accName;
                }
                csvString += JPMCSV_Constants.NEW_LINE_CHARACTER;
            }    
        }
        
        fileName = 'Pos' + JPMCSV_Constants.CSV_EXTENSION;
        content = csvString;  
        doc.Body = (Blob.valueof(content));
        doc.ContentType  = JPMCSV_Constants.CSV_CONTENT_TYPE;
        doc.Name = fileName;
        doc.FolderId = folderList[0].Id;
        docList.add(doc);
        try{
            if(docList != null && docList.size() > 0){
                insert docList;
            }
        }catch(Exception e){
        }      
        return null;
    }
    
    public Boolean numericMatch(String value){
        Pattern p = Pattern.compile('[A-Z,a-z,&%$@!*^]');
        Matcher m = p.matcher(value);
        if (m.find()) {
            return false;
        }
        return true;
    }
    
    public PageReference goToHome(){
        return new PageReference('/home/home.jsp');
    }
}