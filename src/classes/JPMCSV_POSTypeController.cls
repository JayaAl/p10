/***********************************************
    Class: CSV_POSTypeController
    This class creates CSV files for POS Type
    PaymentMethod from Payment Object 
    Author – Cleartask
    Date – 29/08/2011    
    Revision History    
 ***********************************************/
public class JPMCSV_POSTypeController {
    
    /* method to get rows for POS Type */
    public static String paymentAsCSVforPWSPosPayType(c2g__codaPayment__c paymentRecord) {
        String csvString = '';
        String bankAccountNumber = '';
        
        if(paymentRecord == null){
            return csvString; 
        }else if(paymentRecord != null){
            Map<Id, String> dupMap = new   Map<Id, String >();
            Map<Id, List<c2g__codaPaymentLineItem__c>> accPaymentMap = new Map<Id, List<c2g__codaPaymentLineItem__c>>();
            List<c2g__codaPaymentLineItem__c> paymentList = new List<c2g__codaPaymentLineItem__c>();
            List<c2g__codaPaymentLineItem__c> paymentDetailList = new List<c2g__codaPaymentLineItem__c>(JPMCSV_Util.getPaymentDetail(paymentRecord.Id));
            List<c2g__codaPaymentMediaSummary__c> mediaSummaryList = new List<c2g__codaPaymentMediaSummary__c>();
            Set<Id> mediaControlIds = new Set<Id>();
            List <c2g__codaPaymentMediaControl__c> mediaControlList = JPMCSV_Util.getMediaList(paymentRecord);
            List<c2g__codaPaymentMediaDetail__c> mediaDetailList = [Select c2g__Account__c , c.c2g__VendorReference__c, c.c2g__DocumentNumber__c From c2g__codaPaymentMediaDetail__c c where c2g__PaymentMediaSummary__r.c2g__PaymentMediaControl__r.c2g__Payment__c = :paymentRecord.id];
            Map<String, String> mapReferenceNumber = new Map<String, String>();
            for(c2g__codaPaymentMediaDetail__c pmd :mediaDetailList){
                String vendorNumber = '';
                if(mapReferenceNumber != null && mapReferenceNumber.containsKey(pmd.c2g__Account__c)){
                    vendorNumber = mapReferenceNumber.get(pmd.c2g__Account__c);
                    vendorNumber += '-' + pmd.c2g__VendorReference__c;
                    mapReferenceNumber.put(pmd.c2g__Account__c, vendorNumber);
                }else{
                    vendorNumber += pmd.c2g__VendorReference__c;
                    mapReferenceNumber.put(pmd.c2g__Account__c, vendorNumber);
                }
            }
            if(mediaControlList  != null && mediaControlList.size() > 0){
                for(c2g__codaPaymentMediaControl__c i :mediaControlList){                
                    mediaControlIds.add(i.Id);
                }
                mediaSummaryList = JPMCSV_Util.getMediaSummaryList(mediaControlIds);
            }
            c2g__codaBankAccount__c bankAccountRecord = new c2g__codaBankAccount__c();
            

        
            Map<String, c2g__codaPaymentMediaSummary__c> mediaSummaryMap = new Map<String, c2g__codaPaymentMediaSummary__c>();
            for (c2g__codaPaymentMediaSummary__c mediaSummary :  mediaSummaryList) {
                mediaSummaryMap.put(mediaSummary.c2g__Account__c, mediaSummary);
            }            
                
            if(paymentRecord.c2g__BankAccount__c != null){
                bankAccountRecord = [Select c.c2g__ZipPostalCode__c, c.c2g__UnitOfWork__c, c.c2g__Street__c, 
                    c.c2g__StateProvince__c, c.c2g__SortCode__c, c.c2g__ReportingCode__c, c.c2g__Phone__c, 
                    c.c2g__BankName__c, c.c2g__AccountNumber__c, c.c2g__AccountName__c, c.Name, c.Id 
                    From c2g__codaBankAccount__c c where Id = :paymentRecord.c2g__BankAccount__c]; 
                
                if (bankAccountRecord != null){
                    bankAccountNumber = bankAccountRecord.c2g__AccountNumber__c;    
                }
            }
            Map<String, Decimal> mapAccountTotalValue = new Map<String, Decimal>();
            if(paymentDetailList!= null && paymentDetailList.size()>0){   
                
                for(c2g__codaPaymentLineItem__c m :paymentDetailList){
                    Decimal totalValue = m.c2g__TransactionValue__c;
                    if(mapAccountTotalValue != null && mapAccountTotalValue.containsKey(m.c2g__Account__c)){
                        totalValue += mapAccountTotalValue.get(m.c2g__Account__c);
                    }
                    mapAccountTotalValue.put(m.c2g__Account__c, totalValue);
                }    
            }
            
            if(paymentDetailList!= null && paymentDetailList.size()>0){   
                
                for(c2g__codaPaymentLineItem__c m :paymentDetailList){
                    if(dupMap != null && !dupMap.containsKey(m.c2g__Account__c)){
                        csvString += paymentAsCSVforPWSPosPayTypeFirstRow(m, bankAccountNumber, mediaSummaryMap, paymentRecord, mapReferenceNumber, mapAccountTotalValue);
                        csvString += JPMCSV_Constants.NEW_LINE_CHARACTER;
                        dupMap.put(m.c2g__Account__c, csvString);
                    }
                }    
            }
            
            
        }
        return csvString;   
    }
    
    /* method for First row for PWS Pos Pay Type of Excel Sheet */
    public static String paymentAsCSVforPWSPosPayTypeFirstRow(c2g__codaPaymentLineItem__c paymentDetail, String bankAccountNumber, 
        Map<String, c2g__codaPaymentMediaSummary__c> mediaSummaryMap, 
        c2g__codaPayment__c paymentRecord, Map<String, String> mapReferenceNumber,
        Map<String, Decimal> mapAccountTotalValue) 
    { 
        Map<Id, List<c2g__codaPaymentLineItem__c>>paymentDetailMap = new  Map<Id, List<c2g__codaPaymentLineItem__c>>();
        List<c2g__codaPaymentLineItem__c> paymentDetailList = new List<c2g__codaPaymentLineItem__c>();
        
        String csvStringPOS = '';
        if(paymentDetail == null){
            return csvStringPOS; 
        }else{
            csvStringPOS += 'I';   // first column
            csvStringPOS += JPMCSV_Constants.COMMA_STRING;
            
            if(bankAccountNumber != null){
                csvStringPOS += bankAccountNumber.substring(0, 9);   // second column
            }
            csvStringPOS += JPMCSV_Constants.COMMA_STRING;
            
            c2g__codaPaymentMediaSummary__c mediaSummary = mediaSummaryMap.get(paymentDetail.c2g__Account__c);
            if (mediaSummary != null) {
                csvStringPOS += mediaSummary.c2g__PaymentReference__c;  // third column
            }
           /* if(mediaSummaryList != null && mediaSummaryList.size() > 0 && mediaSummaryList[0].c2g__PaymentReference__c != null){
                 csvStringPOS += mediaSummaryList[0].c2g__PaymentReference__c;     
            }*/
            csvStringPOS += JPMCSV_Constants.COMMA_STRING;
            
           
            csvStringPOS +=  JPMCSV_Util.format(paymentRecord.c2g__PaymentDate__c, 'MMddyy');  // fourth column
            csvStringPOS += JPMCSV_Constants.COMMA_STRING;
            
            //Decimal totalValue = paymentDetail.c2g__TransactionValue__c == null ? 0 : -paymentDetail.c2g__TransactionValue__c; 
            
            if(mapAccountTotalValue != null && mapAccountTotalValue.containsKey(paymentDetail.c2g__Account__c)){
                csvStringPOS += -mapAccountTotalValue.get(paymentDetail.c2g__Account__c);     // fifth column
            }
        
            csvStringPOS += JPMCSV_Constants.COMMA_STRING; 
            csvStringPOS += JPMCSV_Constants.COMMA_STRING;
            
            String accName = '';
            String firstName = '';
            String lastName = '';
            if(paymentDetail.c2g__Account__r.Name != null){
                if(paymentDetail.c2g__Account__r.Name.contains(',')){
                    if(paymentDetail.c2g__Account__r.Name.startsWith('"') && paymentDetail.c2g__Account__r.Name.endsWith('"')){
                        accName = paymentDetail.c2g__Account__r.Name;
                        if(paymentDetail.c2g__Account__r.Name.length() > 50){
                            firstName = paymentDetail.c2g__Account__r.Name.subString(0, 50);
                            lastName = paymentDetail.c2g__Account__r.Name.substring(50);
                            accName = firstName + JPMCSV_Constants.COMMA_STRING + lastName;
                        }else{
                            accName = paymentDetail.c2g__Account__r.Name;
                        }
                    }else{
                        accName = JPMCSV_Util.spiltComma(paymentDetail.c2g__Account__r.Name);
                    }
                }else{
                    if(paymentDetail.c2g__Account__r.Name.length() > 50){
                            firstName = paymentDetail.c2g__Account__r.Name.subString(0, 50);
                            lastName = paymentDetail.c2g__Account__r.Name.substring(50);
                            accName = firstName + JPMCSV_Constants.COMMA_STRING + lastName;
                        }else{
                            accName = paymentDetail.c2g__Account__r.Name;
                    }    
                }
                csvStringPOS += accName;
            }
           /* csvStringPOS += JPMCSV_Constants.COMMA_STRING;
            if(mapReferenceNumber != null && mapReferenceNumber.containsKey(paymentDetail.c2g__Account__c)){
                String refNum = mapReferenceNumber.get(paymentDetail.c2g__Account__c);
                if(refNum.length() > 100){
                    csvStringPOS += refNum.subString(0, 100);
                    csvStringPOS += JPMCSV_Constants.COMMA_STRING;
                    csvStringPOS += refNum.subString(100);
                }else{
                    csvStringPOS += refNum;
                }
            }*/
            
         }
         return csvStringPOS; 
    }
}