/***********************************************
    Class: JPMCSV_TestCreateFileController
    This test class is for all CSV files 
    creation controllers.
    Author – Cleartask
    Date – 29/08/2011    
    Revision History    
 ***********************************************/
@isTest
private class JPMCSV_TestCreateFileController {
    
    /* test method for Check Type file */
    static testMethod void testMethodCheckFile(){
        
        c2g__codaPayment__c paymentRecord = getPayment('Check');  //a1z400000004Nsy
        List<c2g__codaPayment__c> objList = new List<c2g__codaPayment__c>();
        objList.add(paymentRecord);
        List<c2g__codaPaymentLineItem__c> objItemList = JPMCSV_Util.getPaymentDetail(paymentRecord.id);
        
        ApexPages.StandardSetController controller = new ApexPages.StandardSetController(objList);
        controller.setSelected(objList);
        JPMCSV_CreateFileController objCreateEmployee  = new JPMCSV_CreateFileController(controller);
        List<selectOption> paymentOptions = new List<selectOption>();
        Decimal amtValue;
        JPMCSV_CreateFileController.CSVWrapper wrap = new  JPMCSV_CreateFileController.CSVWrapper(paymentRecord, objItemList, paymentOptions, amtValue);
        for(JPMCSV_CreateFileController.CSVWrapper w :objCreateEmployee.wrapList){
            w.selectedType = 'Check';            
        }
        objCreateEmployee.save();
    }
    
    /* test method for Pos Type file */
    static testMethod void testMethodPosFile(){
        
        c2g__codaPayment__c paymentRecord = getPayment('Check');  //a1z400000004Nsy
        List<c2g__codaPayment__c> objList = new List<c2g__codaPayment__c>();
        objList.add(paymentRecord);
        List<c2g__codaPaymentLineItem__c> objItemList = JPMCSV_Util.getPaymentDetail(paymentRecord.id);
        
        ApexPages.StandardSetController controller = new ApexPages.StandardSetController(objList);
        controller.setSelected(objList);
        JPMCSV_CreateFileController objCreateEmployee  = new JPMCSV_CreateFileController(controller);
        List<selectOption> paymentOptions = new List<selectOption>();
        Decimal amtValue;
        JPMCSV_CreateFileController.CSVWrapper wrap = new  JPMCSV_CreateFileController.CSVWrapper(paymentRecord, objItemList, paymentOptions, amtValue);
        for(JPMCSV_CreateFileController.CSVWrapper w :objCreateEmployee.wrapList){
            w.selectedType = 'Pos';            
        }
        objCreateEmployee.save();
    }
    
    /* test method for ACH Type file */
    static testMethod void testMethodACHFile(){
        
        c2g__codaPayment__c paymentRecord = getPayment('Electronic');  //a1zV0000000Cald
        List<c2g__codaPayment__c> objList = new List<c2g__codaPayment__c>();
        objList.add(paymentRecord);
        List<c2g__codaPaymentLineItem__c> objItemList = JPMCSV_Util.getPaymentDetail(paymentRecord.id);
        
        ApexPages.StandardSetController controller = new ApexPages.StandardSetController(objList);
        controller.setSelected(objList);
        JPMCSV_CreateFileController objCreateEmployee  = new JPMCSV_CreateFileController(controller);
        List<selectOption> paymentOptions = new List<selectOption>();
        Decimal amtValue;
        JPMCSV_CreateFileController.CSVWrapper wrap = new  JPMCSV_CreateFileController.CSVWrapper(paymentRecord, objItemList, paymentOptions, amtValue);
        for(JPMCSV_CreateFileController.CSVWrapper w :objCreateEmployee.wrapList){
            w.selectedType = 'ACH';            
        }
        objCreateEmployee.save();
    }
    
    /* test method for ACH Type file in GDFF format */
    static testMethod void testMethodACHFileGDFFformat(){
        c2g__codaPayment__c paymentRecord = getPayment('Electronic');  //a1zV0000000Cald
        paymentRecord.c2g__OwnerCompany__r.c2g__VATRegistrationNumber__c = '86161232239'; // Australia VAT Id
        JPMCSV_ACHtypeGDFFformatController.paymentAsCSVforACHTypeGDFFformat(paymentRecord);
    }    
    
    /* test method for Wire Type file */
    static testMethod void testMethodWireFile(){
        
        c2g__codaPayment__c paymentRecord = getPayment('Electronic');  //a1zV0000000Cald
        List<c2g__codaPayment__c> objList = new List<c2g__codaPayment__c>();
        objList.add(paymentRecord);
        List<c2g__codaPaymentLineItem__c> objItemList = JPMCSV_Util.getPaymentDetail(paymentRecord.id);
        
        ApexPages.StandardSetController controller = new ApexPages.StandardSetController(objList);
        controller.setSelected(objList);
        JPMCSV_CreateFileController objCreateEmployee  = new JPMCSV_CreateFileController(controller);
        List<selectOption> paymentOptions = new List<selectOption>();
        Decimal amtValue;
        JPMCSV_CreateFileController.CSVWrapper wrap = new  JPMCSV_CreateFileController.CSVWrapper(paymentRecord, objItemList, paymentOptions, amtValue);
        for(JPMCSV_CreateFileController.CSVWrapper w :objCreateEmployee.wrapList){
            w.selectedType = 'Wire';            
        }
        objCreateEmployee.save();
    }
    
    /* test method for one time Pos Type file */
/*    static testMethod void testMethodPOSSingleFile(){
        JPMCSV_POSGoLiveController objCreateEmployee  = new JPMCSV_POSGoLiveController();
        objCreateEmployee.save();
        objCreateEmployee.goToHome();
    }*/
    
    /* method to get pyment record */
    static c2g__codaPayment__c getPayment(String paymentType){
        List<c2g__codaPayment__c> paymentRecords = [Select c.c2g__UnitOfWork__c, c.c2g__Status__c, 
                c.c2g__SettlementDiscountReceived__c, c.c2g__SDRDimension4__c, 
                c.c2g__SDRDimension3__c, c.c2g__SDRDimension2__c, c.c2g__SDRDimension1__c, 
                c.c2g__Period__c, c.c2g__PaymentValue__c, c.c2g__PaymentValueTotal__c, 
                c.c2g__PaymentValueRollup__c, c.c2g__PaymentTypes__c, c.c2g__PaymentTemplate__c, 
                c.c2g__PaymentMethod__c, c.c2g__PaymentMediaTypes__c, c.c2g__PaymentDate__c, 
                c.c2g__PaymentCurrency__c, c.c2g__OwnerCompany__c, c.c2g__GrossValue__c, 
                c.c2g__GrossValueTotal__c, c.c2g__GrossValueRollup__c, c.c2g__OwnerCompany__r.c2g__VATRegistrationNumber__c,
                c.c2g__GeneralLedgerAccount__c, c.c2g__ExternalId__c, c.c2g__ErrorControl__c, c.c2g__OwnerCompany__r.c2g__TaxIdentificationNumber__c, 
                c.c2g__DueDate__c, c.c2g__DocumentCurrency__c, c.c2g__Discount__c, c.c2g__BankAccount__r.c2g__AccountNumber__c,
                c.c2g__DiscountTotal__c, c.c2g__DiscountRollup__c, c.c2g__DiscardReason__c, c.c2g__BankAccount__r.c2g__BankAccountCurrency__r.CurrencyIsoCode,
                c.c2g__Description__c, c.c2g__CurrencyWriteOff__c, c.c2g__CurrencyMode__c, 
                c.c2g__ClickedPay__c, c.c2g__CWODimension4__c, c.c2g__CWODimension3__c, 
                c.c2g__CWODimension2__c, c.c2g__CWODimension1__c, c.c2g__BankAccount__c, 
                c.c2g__AccountCurrency__c, c.Unique_Id__c, c.SystemModstamp, c.OwnerId, c.Name, 
                c.LastModifiedDate, c.LastModifiedById, c.LastActivityDate, c.IsDeleted, c.Id, 
                c.CurrencyIsoCode, c.CreatedDate, c.CreatedById From c2g__codaPayment__c c 
                where c2g__PaymentMethod__c = :paymentType and c2g__Status__c = 'Matched' limit 1];
        
        if (paymentRecords.size() > 0) 
        {
        //JPMCSV_Util.getPaymentSummaryMap(paymentRecords[0].Id);
        	return paymentRecords.get(0);
        }
       
          
        return null;
       
       
    }
    
   /*  static testMethod void unit(){
       User u = [Select Id from user where username = 'molude@pandora.com.pandora10'];
        System.runAs(u) {
            c2g__codaPayment__c paymentRecord = new c2g__codaPayment__c(c2g__PaymentDate__c = Date.today(), c2g__DueDate__c = Date.today(),
                c2g__PaymentMethod__c = 'Check', c2g__PaymentMediaTypes__c = 'Check', c2g__AccountCurrency__c = 'a0v400000008cQ1',
                c2g__BankAccount__c = 'a0z40000000ESC6', c2g__SettlementDiscountReceived__c = 'a1P4000000099ft', 
                c2g__CurrencyWriteOff__c = 'a1P4000000099ft' );
            insert paymentRecord;
            
            JPMCSV_Util.getPaymentSummaryMap(paymentRecord.Id);
        //}
    }*/
}