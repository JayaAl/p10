/***********************************************
    Class: CSV_Util
    This class consists of useful methods that 
    are used by different classes.
    Author – Cleartask
    Date – 29/08/2011    
    Revision History    
 ***********************************************/
public class JPMCSV_Util {

    public static List<c2g__codaPaymentMediaControl__c> getMediaList(c2g__codaPayment__c paymentRecord){
         List<c2g__codaPaymentMediaControl__c> mediaControlList  = new List<c2g__codaPaymentMediaControl__c>();
         if(paymentRecord != null){
             mediaControlList = [Select c.c2g__ZipPostCode__c, c.c2g__UnitOfWork__c, c.c2g__Street__c, 
                 c.c2g__StateProvince__c, c.c2g__Payment__c, c.c2g__PaymentValue__c, 
                 c.c2g__PaymentMethod__c, c.c2g__PaymentDate__c, c.c2g__PaymentControlNumber__c, 
                 c.c2g__PayeeName__c, c.c2g__OwnerCompany__c, c.c2g__NumberofAccounts__c, 
                 c.c2g__GrossValue__c, c.c2g__ExternalId__c, c.c2g__DiscountValue__c, 
                 c.c2g__DirectDebitOriginatorReference__c, c.c2g__Country__c, c.c2g__CompanyName__c, 
                 c.c2g__City__c, c.c2g__BankZipPostalCode__c, c.c2g__BankSwiftNumber__c, 
                 c.c2g__BankStreet__c, c.c2g__BankStateProvince__c, c.c2g__BankSortCode__c, 
                 c.c2g__BankName__c, c.c2g__BankIbanNumber__c, c.c2g__BankCountry__c, 
                 c.c2g__BankCity__c, c.c2g__BankAccountNumber__c, c.c2g__AccountingCurrency__c, 
                 c.c2g__Account__c, c.Name, c.Id, c.CurrencyIsoCode 
                 From c2g__codaPaymentMediaControl__c c 
                 where c2g__Payment__c = :paymentRecord.Id];    
         } 
         return  mediaControlList;   
    }
    
    public static List<c2g__codaPaymentMediaSummary__c> getMediaSummaryList(Set<Id> mediaControlIds){
        List<c2g__codaPaymentMediaSummary__c> mediaSummaryList = new List<c2g__codaPaymentMediaSummary__c>(); 
        if(mediaControlIds!= null){
             mediaSummaryList = [Select c.c2g__UnitOfWork__c, c.c2g__ReportingCode__c, 
                 c.c2g__ReasonCode__c, c.c2g__PaymentValue__c, c.c2g__PaymentValueTotal__c, 
                 c.c2g__PaymentReference__c, c.c2g__PaymentMediaControl__c, 
                 c.c2g__PaymentControlCurrency__c, c.c2g__PayeeName__c, c.c2g__OwnerCompany__c, 
                 c.c2g__LineNumber__c, c.c2g__Indicator__c, c.c2g__GrossValue__c, 
                 c.c2g__GrossValueTotal__c, c.c2g__ExternalId__c, c.c2g__DiscountValue__c, 
                 c.c2g__DiscountValueTotal__c, c.c2g__BillingZipPostCode__c, 
                 c.c2g__BillingStreet__c, c.c2g__BillingStateProvince__c, c.c2g__BillingCountry__c, 
                 c.c2g__BillingCity__c, c.c2g__BankZipPostalCode__c, c.c2g__BankSwiftNumber__c, 
                 c.c2g__BankStreet__c, c.c2g__BankStateProvince__c, c.c2g__BankSortCode__c, 
                 c.c2g__BankName__c, c.c2g__BankIbanNumber__c, c.c2g__BankCountry__c, 
                 c.c2g__BankCity__c, c.c2g__BankAccountReference__c, c.c2g__BankAccountNumber__c, 
                 c.c2g__Account__c, c.c2g__Account__r.c2g__CODABankAccountNumber__c, 
                 c.c2g__Account__r.Name, c.c2g__Account__r.Description, 
                 c.c2g__Account__r.BillingStreet, c.c2g__Account__r.BillingState, 
                 c.c2g__Account__r.BillingPostalCode, c.c2g__Account__r.BillingCity, 
                 c.c2g__Account__r.BillingCountry, c.Name, c.Id 
                 From c2g__codaPaymentMediaSummary__c c 
                 where c2g__PaymentMediaControl__c in :mediaControlIds];
        }
        return  mediaSummaryList;   
    } 
    
    public static List<c2g__codaPaymentLineItem__c> getPaymentDetail(Id paymentRecordId){
        List<c2g__codaPaymentLineItem__c> paymentDetailList = new  List<c2g__codaPaymentLineItem__c>();
        if(paymentRecordId == null){
            return paymentDetailList;
        }else if (paymentRecordId != null){
            paymentDetailList = [
            Select c.c2g__UnitOfWork__c, c.c2g__Transaction__c, 
                c.c2g__TransactionValue__c, c.c2g__TransactionSelected__c, 
                c.c2g__TransactionLineItem__c, c.c2g__TransactionDate__c, c.c2g__Payment__c, 
                c.c2g__PaymentSummary__c, c.c2g__PaymentAccountLineItem__c, c.c2g__LineNumber__c, 
                c.c2g__GrossValue__c, c.c2g__ExternalId__c, c.c2g__DueDate__c, c.c2g__DocumentNumber__c, 
                c.c2g__Discount__c, c.c2g__BelongToCurrentRefine__c, c.c2g__Account__c, c.Name, c.Id, 
                c.c2g__Account__r.Name, c.c2g__Account__r.c2g__CODABankAccountNumber__c, 
                c.c2g__Account__r.Description, c.c2g__Account__r.BillingStreet, 
                c.c2g__Account__r.BillingState, c.c2g__Account__r.c2g__CODABankAccountReference__c, 
                c.c2g__Account__r.BillingCountry, c.c2g__Account__r.AccountNumber, 
                c.c2g__Account__r.BillingPostalCode, c.c2g__Account__r.BillingCity, 
                c2g__PaymentSummary__r.c2g__PaymentValue__c, c2g__Payment__r.c2g__BankAccount__r.c2g__AccountNumber__c, c.c2g__Account__r.c2g__CODABankName__c,
                c.c2g__Account__r.c2g__CODABankStreet__c, c.c2g__Account__r.c2g__CODABankCity__c, c.c2g__Account__r.c2g__CODABankZipPostalCode__c, 
                c.c2g__Account__r.c2g__CODABankCountry__c, c.c2g__Account__r.c2g__CODABankStateProvince__c, 
                c.c2g__Account__r.c2g__CODABankSortCode__c  
            From c2g__codaPaymentLineItem__c c 
            where c2g__Payment__c = :paymentRecordId
                and c2g__TransactionSelected__c = true 
                And ((c2g__DocumentNumber__c like 'PIN%' AND c2g__Payment__r.c2g__GeneralLedgerAccount__r.Name = 'Accounts Payable')
                    OR c2g__Payment__r.c2g__GeneralLedgerAccount__r.Name = 'Accounts Receivable')
                And c2g__PaymentSummary__r.c2g__AccountSelected__c = true 
            Order by c.c2g__Account__r.Name ];
        }
        return paymentDetailList; 
    }
    
/*    public static Map<Id, Decimal> getPaymentSummaryMap(Id paymentRecordId){
          Map<Id, Decimal> paymentSummaryMap = new   Map<Id, Decimal>();
          if(paymentRecordId == null){
              return paymentSummaryMap; 
          }else if (paymentSummaryMap  != null){
              List<c2g__codaPaymentAccountLineItem__c> paymentSummaryList = [Select c.c2g__UnitOfWork__c, 
                  c.c2g__Payment__c, c.c2g__PaymentValue__c, c.c2g__PaymentValueWithAll__c, 
                  c.c2g__PaymentValueTotal__c, c.c2g__LineNumber__c, c.c2g__GrossValue__c, 
                  c.c2g__GrossValueWithAll__c, c.c2g__GrossValueTotal__c, c.c2g__ExternalId__c, 
                  c.c2g__Discount__c, c.c2g__DiscountWithAll__c, c.c2g__DiscountTotal__c, 
                  c.c2g__DetailModified__c, c.c2g__BelongToCurrentRefine__c, c.c2g__Account__c, 
                  c.c2g__AccountSelected__c, c.Name 
                  From c2g__codaPaymentAccountLineItem__c c 
                  where c2g__Payment__c = :paymentRecordId
                  ];
                  
              for(c2g__codaPaymentAccountLineItem__c c :paymentSummaryList){
                  if(c.c2g__Account__c != null && c.c2g__PaymentValue__c != null){
                      paymentSummaryMap.put(c.c2g__Account__c, c.c2g__PaymentValue__c);
                  }  
              }
          }
          return paymentSummaryMap; 
    } */
    
    /*
    nsk: 06/23/14 -ISS-8100 - Adding c2g__Account__r.c2g__CODABankAccountReference__c, c2g__Account__r.c2g__CODABankAccountNumber__c to SOQL
    */
    public static List<AggregateResult> getPaymentDetailSummary(Id paymentRecordId){

        if(paymentRecordId == null){
            return new List<AggregateResult>();
        }
        
        return [
            Select sum(c.c2g__TransactionValue__c) transactionValue,
                c.c2g__Account__c accountId,
                c.c2g__Account__r.c2g__CODABankAccountReference__c destinationBankId,
                c.c2g__Account__r.Name accountName,
                c.c2g__Account__r.c2g__CODABankAccountNumber__c accountNumber,
                c.c2g__Account__r.c2g__CODABankAccountReference__c accountRef, 
                c.c2g__Account__r.c2g__CODABankAccountNumber__c accountNum,
                c.c2g__Payment__r.c2g__PaymentDate__c paymentDate
            From c2g__codaPaymentLineItem__c c 
            where c2g__Payment__c = :paymentRecordId
                and c2g__TransactionSelected__c = true 
                And c2g__DocumentNumber__c like 'PIN%' 
                And c2g__PaymentSummary__r.c2g__AccountSelected__c = true
            Group By  c.c2g__Account__c, c.c2g__Account__r.Name, 
                c.c2g__Account__r.c2g__CODABankAccountReference__c, c.c2g__Account__r.c2g__CODABankAccountNumber__c, c.c2g__Payment__r.c2g__PaymentDate__c                 
            Order By c.c2g__Account__r.Name 
        ];
    }
 
/*   
    public static Map<Id, c2g__codaPaymentAccountLineItem__c> getPaymentSummaryMap(Id paymentRecordId){
          Map<Id, c2g__codaPaymentAccountLineItem__c> paymentSummaryMap = new   Map<Id, c2g__codaPaymentAccountLineItem__c>();

          if(paymentRecordId == null){
              return paymentSummaryMap; 
          }
          
          List<c2g__codaPaymentAccountLineItem__c> paymentSummaryList = [Select c.c2g__UnitOfWork__c, 
              c.c2g__Payment__c, c.c2g__PaymentValue__c, c.c2g__PaymentValueWithAll__c, 
              c.c2g__PaymentValueTotal__c, c.c2g__LineNumber__c, c.c2g__GrossValue__c, 
              c.c2g__GrossValueWithAll__c, c.c2g__GrossValueTotal__c, c.c2g__ExternalId__c, 
              c.c2g__Discount__c, c.c2g__DiscountWithAll__c, c.c2g__DiscountTotal__c, 
              c.c2g__DetailModified__c, c.c2g__BelongToCurrentRefine__c, c.c2g__Account__c, 
              c.c2g__AccountSelected__c, c.Name 
              From c2g__codaPaymentAccountLineItem__c c 
              where c2g__Payment__c = :paymentRecordId
          ];
              
          for(c2g__codaPaymentAccountLineItem__c c :paymentSummaryList){
              if(c.c2g__Account__c != null && c.c2g__PaymentValue__c != null){
                  paymentSummaryMap.put(c.c2g__Account__c, c);
              }  
          }

          return paymentSummaryMap; 
    }
*/    
    /*spiltComma for spilt comma */
    public static String  spiltComma(String csvString){
        String strAccountName;
        if(csvString == null){
            return '';
        }
        String[] splitResultComma = csvString.split('\\,');
        if(splitResultComma != null && splitResultComma.size() > 0){
            for(String s :splitResultComma){
                if(strAccountName != null){
                     strAccountName = strAccountName + s;    
                }else if(strAccountName == null){
                     strAccountName = s; 
                }
            }
        }else if(splitResultComma == null){
            strAccountName = csvString;        
        }
        return strAccountName; 
    }
    
    public static String format(Date d, String pattern) {
        if (d == null) return '';
        DateTime dt = DateTime.newInstance(d.year(), d.month(), d.day());
        return dt.format(pattern);     //example - 'MM/dd/yyyy'
    }
    
    public static String formatTime(DateTime d, String pattern) {
        return d.format(pattern);     //example - 'hhMMss'
    }    
}