/***********************************************
    Class: CSV_WireTypeController
    This class creates CSV files for Wire Type
    PaymentMethod from Payment Object 
    Author – Cleartask
    Date – 29/08/2011    
    Revision History    
 ***********************************************/
public class JPMCSV_WireTypeController {
    
    public static String month = System.now().month() <= 9 ? String.valueOf('0' + System.now().month()) : String.valueOf(System.now().month());
    public static String day = System.now().day() <= 9 ? String.valueOf('0' + System.now().day()) : String.valueOf(System.now().day());
    public static String hour = System.now().hour() <= 9 ? String.valueOf('0' + System.now().hour()) : String.valueOf(System.now().hour());
    public static String minute = System.now().minute() <= 9 ? String.valueOf('0' + System.now().minute()) : String.valueOf(System.now().minute());
    public static String second = System.now().second() <= 9 ? String.valueOf('0' + System.now().second()) : String.valueOf(System.now().second());
    
    /* method to get rows for Wire type */
    public static String paymentAsCSVforWireType(c2g__codaPayment__c paymentRecord) {
        
        String csvString = '';
        if(paymentRecord == null){
            return csvString; 
        }
        
        List<c2g__codaPaymentMediaDetail__c> mediaDetailList = [Select c2g__Account__c , c.c2g__VendorReference__c, c.c2g__DocumentNumber__c From c2g__codaPaymentMediaDetail__c c where c2g__PaymentMediaSummary__r.c2g__PaymentMediaControl__r.c2g__Payment__c = :paymentRecord.id];
        
        Map<String, String> mapReferenceNumber = new Map<String, String>();
        for(c2g__codaPaymentMediaDetail__c pmd :mediaDetailList){
            String vendorNumber = '';
            if( mapReferenceNumber.containsKey(pmd.c2g__DocumentNumber__c)){
                vendorNumber = mapReferenceNumber.get(pmd.c2g__DocumentNumber__c);
                vendorNumber += '-' + pmd.c2g__VendorReference__c;
            }else{
                vendorNumber += pmd.c2g__VendorReference__c;
            }
            mapReferenceNumber.put(pmd.c2g__DocumentNumber__c, vendorNumber);
        }
        
        Map<Id, String> accdDupMap = new Map<Id, String>();
        
        csvString += getHeaderRow(paymentRecord);

        List<c2g__codaPaymentLineItem__c> paymentDetailList = new  List<c2g__codaPaymentLineItem__c>(JPMCSV_Util.getPaymentDetail(paymentRecord.Id));
        Integer index = 0;
        Decimal totalAmount = 0.0;
        if(paymentDetailList != null && paymentDetailList.size()>0){
            
            
            for(c2g__codaPaymentLineItem__c m :paymentDetailList){
                totalAmount += m.c2g__TransactionValue__c;
                
                
                    index ++;
                    
                    csvString += '4' + JPMCSV_Constants.COMMA_STRING;        //1
                    if(m.c2g__Account__r.c2g__CODABankSortCode__c != null){
                        csvString += m.c2g__Account__r.c2g__CODABankSortCode__c;         //2
                    }
                    csvString += JPMCSV_Constants.COMMA_STRING;                            //3
                    csvString += JPMCSV_Constants.COMMA_STRING + '020' + JPMCSV_Constants.COMMA_STRING;    //4
                    if(m.c2g__Payment__r.c2g__BankAccount__r.c2g__AccountNumber__c != null){
                        csvString += m.c2g__Payment__r.c2g__BankAccount__r.c2g__AccountNumber__c;      //5 Pandora- Banik Account#
                    }  
                    csvString += JPMCSV_Constants.COMMA_STRING;
                    csvString += 'USD' + JPMCSV_Constants.COMMA_STRING;                    //6
                   /* if(m.c2g__PaymentSummary__r.c2g__PaymentValue__c != null){
                        csvString += Math.abs(m.c2g__PaymentSummary__r.c2g__PaymentValue__c) + JPMCSV_Constants.COMMA_STRING; //7 - Amount
                    }*/
                    if (m.c2g__TransactionValue__c != null) {
                        csvString += -m.c2g__TransactionValue__c + JPMCSV_Constants.COMMA_STRING; //7 - Amount 
                    }
                   
                    csvString += System.today().year() + month + day + JPMCSV_Constants.COMMA_STRING;    //8
                    
                    csvString += JPMCSV_Constants.COMMA_STRING;        //9
                    csvString += JPMCSV_Constants.COMMA_STRING + JPMCSV_Constants.COMMA_STRING + JPMCSV_Constants.COMMA_STRING;    //10-11-12
                    csvString += JPMCSV_Constants.COMMA_STRING + JPMCSV_Constants.COMMA_STRING + JPMCSV_Constants.COMMA_STRING;    //13-15
                    
                    if(m.c2g__Account__r.c2g__CODABankAccountReference__c != null){
                        csvString += m.c2g__Account__r.c2g__CODABankAccountReference__c;    //16
                    }    
                    csvString += JPMCSV_Constants.COMMA_STRING;
                    
                    if(m.c2g__Account__r.c2g__CODABankName__c != null){
                        csvString += m.c2g__Account__r.c2g__CODABankName__c;    //17
                    }    
                    csvString += JPMCSV_Constants.COMMA_STRING;

                    if(m.c2g__Account__r.c2g__CODABankStreet__c != null){
                        csvString += m.c2g__Account__r.c2g__CODABankStreet__c;    //18
                    }    
                    csvString += JPMCSV_Constants.COMMA_STRING;

                    String city = (m.c2g__Account__r.c2g__CODABankCity__c != null) ? m.c2g__Account__r.c2g__CODABankCity__c : '';
                    String state = (m.c2g__Account__r.c2g__CODABankStateProvince__c != null) ? m.c2g__Account__r.c2g__CODABankStateProvince__c : '';
                    String zip = (m.c2g__Account__r.c2g__CODABankZipPostalCode__c != null) ? m.c2g__Account__r.c2g__CODABankZipPostalCode__c : '';
                    String address2 = (city + (city.equals('') ? state : ' ' + state)).trim();
                    address2 += (address2.equals('') ? zip : ' ' + zip).trim();
                    
                    csvString += address2 + JPMCSV_Constants.COMMA_STRING;    //19
                    if(m.c2g__Account__r.c2g__CODABankCountry__c != null){
                        csvString += m.c2g__Account__r.c2g__CODABankCountry__c;    //20
                    }    
                    csvString += JPMCSV_Constants.COMMA_STRING;
                    
                    csvString += JPMCSV_Constants.COMMA_STRING + JPMCSV_Constants.COMMA_STRING;       //21-22
                    
                    if(m.c2g__Account__r.c2g__CODABankAccountNumber__c != null){
                        csvString += m.c2g__Account__r.c2g__CODABankAccountNumber__c;    //23
                    }    
                    csvString += JPMCSV_Constants.COMMA_STRING;
                    
                    if(m.c2g__Account__r.Name != null){
                        String strAccName = JPMCSV_Util.spiltComma(m.c2g__Account__r.Name);    //24
                        csvString+= strAccName;
                    }
                    csvString += JPMCSV_Constants.COMMA_STRING;
                    
                    if(m.c2g__Account__r.BillingCity != null){
                        String strAccName = JPMCSV_Util.spiltComma(m.c2g__Account__r.BillingCity);    //25
                        csvString+= strAccName;
                    }
                    
                    csvString += JPMCSV_Constants.COMMA_STRING; //25
                    csvString += 'US' + JPMCSV_Constants.COMMA_STRING; //26
                    csvString += JPMCSV_Constants.COMMA_STRING + JPMCSV_Constants.COMMA_STRING + JPMCSV_Constants.COMMA_STRING + JPMCSV_Constants.COMMA_STRING;
                    csvString += JPMCSV_Constants.COMMA_STRING + JPMCSV_Constants.COMMA_STRING; //27-32
                    
                    String vendorRef = '';
                    if(mapReferenceNumber != null && mapReferenceNumber.containsKey(m.c2g__DocumentNumber__c)){
                        vendorRef = mapReferenceNumber.get(m.c2g__DocumentNumber__c);
                    }
                    csvString += getRows33To36(m.c2g__DocumentNumber__c, vendorRef);    //33-36

                    csvString += JPMCSV_Constants.COMMA_STRING + JPMCSV_Constants.COMMA_STRING + JPMCSV_Constants.COMMA_STRING; //37-39
                    csvString += JPMCSV_Constants.COMMA_STRING + JPMCSV_Constants.COMMA_STRING + JPMCSV_Constants.COMMA_STRING + JPMCSV_Constants.COMMA_STRING + JPMCSV_Constants.COMMA_STRING + JPMCSV_Constants.COMMA_STRING; //40-45
                    csvString += JPMCSV_Constants.COMMA_STRING + JPMCSV_Constants.COMMA_STRING + JPMCSV_Constants.COMMA_STRING + JPMCSV_Constants.COMMA_STRING + JPMCSV_Constants.COMMA_STRING + JPMCSV_Constants.COMMA_STRING; //46-51
                    csvString += JPMCSV_Constants.COMMA_STRING + JPMCSV_Constants.COMMA_STRING + JPMCSV_Constants.COMMA_STRING + JPMCSV_Constants.COMMA_STRING + JPMCSV_Constants.COMMA_STRING + JPMCSV_Constants.COMMA_STRING; //52-57
                    csvString += JPMCSV_Constants.COMMA_STRING + JPMCSV_Constants.COMMA_STRING + 'US' + JPMCSV_Constants.COMMA_STRING + 'US' + JPMCSV_Constants.COMMA_STRING; //58-61
                    csvString += JPMCSV_Constants.NEW_LINE_CHARACTER;
                
            } 
            csvString += getFooterRow(index, totalAmount);
       } 

        return csvString;
    }
    
    public static String getHeaderRow(c2g__codaPayment__c paymentRecord) {
        String headerRow = '';
        headerRow += 'CI' + JPMCSV_Constants.COMMA_STRING;
        headerRow += System.now().year() + month + day + hour + minute + second;
        headerRow += JPMCSV_Constants.COMMA_STRING;
        headerRow += JPMCSV_Constants.COMMA_STRING;
        
        if(paymentRecord.Unique_Id__c != null){
            headerRow += paymentRecord.Unique_Id__c;
        }
        headerRow += JPMCSV_Constants.NEW_LINE_CHARACTER;
        return headerRow;
    }
    
    public static String getFooterRow(Integer index, Decimal totalAmount) {
        String footerRow = '99' + JPMCSV_Constants.COMMA_STRING;
        footerRow += String.valueOf(index) + JPMCSV_Constants.COMMA_STRING;
        footerRow += -totalAmount;
        return footerRow;
    
    }
    
    public static String getRows33To36(String docNumber, String refNumber) {
        String rows33To36 = '';
        if(docNumber.length() > 35){
            rows33To36 += docNumber.substring(0, 35);
        }else{
            rows33To36 += docNumber;
        }
        rows33To36 += JPMCSV_Constants.COMMA_STRING;
        if(refNumber.length() > 35){
            rows33To36 += refNumber.substring(0, 35);
        }else{
            rows33To36 += refNumber;
        }
        rows33To36 += JPMCSV_Constants.COMMA_STRING;
        rows33To36 += JPMCSV_Constants.COMMA_STRING;
        
        return rows33To36;
    }
    
    
    public String getPart(String value, Integer part, Integer maxSize) {
        if (value == null || value.equals('')) return '';
        if (value.length() > part * maxSize) return '';
        return value.substring((part - 1) * maxSize, part * maxSize);
    }
}