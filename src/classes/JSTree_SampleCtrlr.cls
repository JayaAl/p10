global class JSTree_SampleCtrlr{
     
    @RemoteAction
     global static List<JSTreeWrapper> getStateCountyRelation() {
         Map<Id,ATG_State__c> statMap = new Map<Id,ATG_State__c>([Select id,name,ATG_State_Code__c from ATG_State__c]);
         List<ATG_County__c> countyLst = [Select id,name,ATG_State__c,ATG_County_Name__c from ATG_County__c where ATG_State__c IN : statMap.keySet()];
        
         Map<Id, List<ATG_County__c>> stateCountyMap = new Map<Id, List<ATG_County__c>>();
         for(ATG_County__c countyVar : countyLst){
            if(stateCountyMap.containsKey(countyVar.ATG_State__c)){
                stateCountyMap.get(countyVar.ATG_State__c).add(countyVar);
            }else{
                stateCountyMap.put(countyVar.ATG_State__c,new list<ATG_County__c>{countyVar});
            } 
         }
          
         List<JSTreeWrapper> wrapperLst = new List<JSTreeWrapper>(); 

         for(Id stateId : stateCountyMap.keySet()){
            JSTreeWrapper wrapper = new JSTreeWrapper();
            wrapper.stateVar = statMap.get(stateId);
            wrapper.countyLst = stateCountyMap.get(stateId);
            wrapperLst.add(wrapper);
         } 
         
         system.debug('wrapperLst ::::'+wrapperLst);
         String jsonStr = JSON.serialize(wrapperLst, true);
         system.debug('jsonStr ::: '+jsonStr);
         return wrapperLst;
     }
     
     @RemoteAction
     global static List<ATG_DMA__c> getDMARecords(){
        return [Select id,name from ATG_DMA__c ];
     } 

     global class JSTreeWrapper{
         global List<ATG_County__c> countyLst;
         global ATG_State__c  stateVar ;           
     }
}