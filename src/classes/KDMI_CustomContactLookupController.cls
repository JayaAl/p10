/*
 *  @name: KeyDecisionMakerEditController
 *  @desc: Edit VF for Key Decision Maker
 *  @author: Lakshman(sfdcace@gmail.com)
 *  @date: 06/07/2014 
 */
public without sharing class KDMI_CustomContactLookupController {

  public Contact contact {get;set;} // new contact to create
  public List<Contact> results{get;set;} // search results
  public string searchString{get;set;} // search keyword
  private String accDevPlanId;
  private Set<Id> setAccId;
  public KDMI_CustomContactLookupController() {
    contact = new Contact();
    // get the current search string
    searchString = System.currentPageReference().getParameters().get('lksrch');
    accDevPlanId = System.currentPageReference().getParameters().get('accDevPlanId');
    runSearch();  
  }

  // performs the keyword search
  public PageReference search() {
    runSearch();
    return null;
  }

  // prepare the query and issue the search command
  private void runSearch() {
    // TODO prepare query string for complex serarches & prevent injections
    results = performSearch(searchString);               
  } 

  // run the search and return the records found. 
  private List<Contact> performSearch(string searchString) {

    String soql = 'select id, name, Account.Name from contact';
    if(accDevPlanId != '' &&  accDevPlanId != null) {
      setAccId = new Set<Id>();
      for(ADP_Associated_Accounts__c objAAA  :[Select Id, Account__c from ADP_Associated_Accounts__c where Account_Development_Plan__c =: accDevPlanId]) {
		setAccId.add(objAAA.Account__c);      	
      }
    }
    soql = soql +  ' where AccountId =: setAccId AND AccountId != null';
    if(searchString != '' && searchString != null) {
      soql = soql +  ' AND Name LIKE \'%' + searchString + '%\'';    
    }
    soql = soql + ' limit 25';
    System.debug(soql);
    return database.query(soql); 

  }

  // used by the visualforce page to send the link to the right dom element
  public string getFormTag() {
    return System.currentPageReference().getParameters().get('frm');
  }

  // used by the visualforce page to send the link to the right dom element for the text box
  public string getTextBox() {
    return System.currentPageReference().getParameters().get('txt');
  }

}