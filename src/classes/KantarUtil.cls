public class KantarUtil{
    public static Set<Integer> Q1 = new Set<Integer>{1,2,3};
    public static Set<Integer> Q2 = new Set<Integer>{4,5,6};
    public static Set<Integer> Q3 = new Set<Integer>{7,8,9};
    public static Set<Integer> Q4 = new Set<Integer>{10,11,12};
    public static date settingsDate{get;set;}
    
    public KantarUtil()
    {
        Custom_Constants__c kantarDate = Custom_Constants__c.getValues('Kantar Date');
        if(kantarDate!=null && kantarDate.Value__c!=null)
        {
            settingsDate = date.valueOf(kantarDate.Value__c);
        }
        
    }
    //public static List<Kantar__c> getKantarDetails(id accountId) {
    public static AggregateResult[] getKantarDetails(id accountId) {
        List<Kantar__c> kantarList = new List<Kantar__c>();
        //string query='select sum(spend__C),media__C,Quarter__C,year__C FROM KANTAR__C where Account__c=:accountId and Quarter__c<>null ';
        //query +=' and (DATE__C>= LAST_N_QUARTERS:4)  order by year__C desc,quarter__c desc,month__c desc,media__c desc';
        //and account__c=:acct.Id 
        
        string query = 'select account__c,sum(spend__c),CALENDAR_YEAR(ad_date__C), ';
               query +=' CALENDAR_QUARTER(ad_date__C),media__c from kantar__c where account__c=:accountId  group by media__c, ';
               query +=' CALENDAR_YEAR(ad_date__C), CALENDAR_QUARTER(ad_date__C),account__c  ';
               //query +=' where account__c=:accountId  ';
               query +=' order by media__c,account__c,CALENDAR_QUARTER(ad_date__C) ';
               
        AggregateResult[] groupedResults = database.query(query);
        
        return groupedResults;
        
        
        //kantarList = database.query(query);
        //return kantarList;
    }
    
    public static string getQuarter()
    {
        
        Integer cMonth = 1;
        if(settingsDate == null)
            cMonth = Date.today().month();
        else
            cMonth = settingsDate.month();
        
        string quarter = '';
        if(Q1.contains(cMonth))
            quarter =  'Q1';
        else if(Q2.contains(cMonth))
            quarter =  'Q2';
        else if(Q3.contains(cMonth))
            quarter =  'Q3';
        else if(Q4.contains(cMonth))
            quarter =  'Q4';
            
        return quarter;    
    }
    
    public static integer getCurrentYear()
    {
        return Date.today().year();
    }
    
    public static string getCurrentQuarter()
    {
        string currentQuarter = '';
        currentQuarter =  getQuarter() + ' ' + getCurrentYear();
        return currentQuarter;    
    }
    
    
    public static string getPreviousQuarter()
    {
        string previousQuarter = '';
        if(getQuarter()=='Q1')
            previousQuarter =  'Q4 ' + ' ' + (getCurrentYear() -1);
        else if(getQuarter()=='Q2')
            previousQuarter =  'Q1 ' + ' ' + getCurrentYear();
        else if(getQuarter()=='Q3')
            previousQuarter =  'Q2 ' + ' ' + getCurrentYear();
        else if(getQuarter()=='Q4')
            previousQuarter =  'Q3 ' + ' ' + getCurrentYear();
            
        return previousQuarter;    
    }
    
    public static string getPreviousButOneQuarter()
    {
        string previousButOneQuarter = '';
        if(getQuarter()=='Q1')
            previousButOneQuarter =  'Q3 ' + ' ' + (getCurrentYear() -1);
        else if(getQuarter()=='Q2')
            previousButOneQuarter =  'Q4 ' + ' ' + (getCurrentYear() -1);
        else if(getQuarter()=='Q3')
            previousButOneQuarter =  'Q1 ' + ' ' + getCurrentYear();
        else if(getQuarter()=='Q4')
            previousButOneQuarter =  'Q2 ' + ' ' + getCurrentYear();
            
        return previousButOneQuarter;     
    }
    
    public static string getPreviousButTwoQuarter()
    {
        string previousButTwoQuarter = '';
        if(getQuarter()=='Q1')
            previousButTwoQuarter =  'Q2 ' + ' ' + (getCurrentYear() -1);
        else if(getQuarter()=='Q2')
            previousButTwoQuarter =  'Q3 ' + ' ' + (getCurrentYear() -1);
        else if(getQuarter()=='Q3')
            previousButTwoQuarter =  'Q4 ' + ' ' + (getCurrentYear() -1);
        else if(getQuarter()=='Q4')
            previousButTwoQuarter =  'Q1 ' + ' ' + getCurrentYear();
            
        return previousButTwoQuarter;     
    }
    
    public static string getPreviousButThreeQuarter()
    {
        string previousButThreeQuarter = '';
        if(getQuarter()=='Q1')
            previousButThreeQuarter =  'Q1 ' + ' ' + (getCurrentYear() -1);
        else if(getQuarter()=='Q2')
            previousButThreeQuarter =  'Q2 ' + ' ' + (getCurrentYear() -1);
        else if(getQuarter()=='Q3')
            previousButThreeQuarter =  'Q3 ' + ' ' + (getCurrentYear() -1);
        else if(getQuarter()=='Q4')
            previousButThreeQuarter =  'Q4 ' + ' ' + (getCurrentYear() -1);
            
        return previousButThreeQuarter;     
    }
    
    public static string getPreviousButFourQuarter()
    {
        string previousButFourQuarter = '';
        if(getQuarter()=='Q1')
            previousButFourQuarter =  'Q4 ' + ' ' + (getCurrentYear() -2);
        else if(getQuarter()=='Q2')
            previousButFourQuarter =  'Q1 ' + ' ' + (getCurrentYear() -1);
        else if(getQuarter()=='Q3')
            previousButFourQuarter =  'Q2 ' + ' ' + (getCurrentYear() -1);
        else if(getQuarter()=='Q4')
            previousButFourQuarter =  'Q3 ' + ' ' + (getCurrentYear() -1);
            
        return previousButFourQuarter;     
    }
    
    
    
    


}