public class KantarWrapper
{
    public string media{get;set;}
    public string year{get;set;}
    public decimal quarter1Spend1{get;set;}
    public decimal quarter2Spend2{get;set;}
    public decimal quarter3Spend3{get;set;}
    public decimal quarter4Spend4{get;set;}
    public decimal quarter5Spend5{get;set;}
    public decimal quarter6Spend6{get;set;}

}