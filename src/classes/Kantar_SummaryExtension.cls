public class Kantar_SummaryExtension
{
    public account acct{get;set;}
    public id accountId{get;set;}
    public string mediaType {get;set;}
    public integer year{get;set;}
    //pubic integer year2{get;set;}
    public string currentQuarter{get;set;}
    public string previousQuarter{get;set;}
    public string previousButOneQuarter{get;set;}
    public string previousButTwoQuarter{get;set;}
    public string previousButThreeQuarter{get;set;}
    public string previousButFourQuarter{get;set;}
    public list<Kantar__c> kantarList{get;set;}
    public list<KantarWrapper> kantarWrapperList  {get;set;}
    
    public map<string,map<string,decimal>> keyMediaSpendByQuarter{get;set;}
    public Kantar_SummaryExtension(ApexPages.StandardController stdController)
    {
        this.acct = (Account)stdController.getRecord();
        accountId = this.acct.Id;
        kantarList = new list<kantar__C>();
        currentQuarter = KantarUtil.getCurrentQuarter();//'Q2 2017';
        previousQuarter = KantarUtil.getPreviousQuarter();//'Q1 2017';
        previousButOneQuarter = KantarUtil.getPreviousButOneQuarter();//'Q4 2016';
        previousButTwoQuarter = KantarUtil.getPreviousButTwoQuarter();//'Q3 2016';
        previousButThreeQuarter = KantarUtil.getPreviousButThreeQuarter();//'Q2 2016';
        previousButFourQuarter = KantarUtil.getPreviousButFourQuarter();//'Q2 2016';
        
        
        
        
        //string query='select Brand__c,spend__C,Category__C,media__C,Quarter__C,year__C FROM KANTAR__C where Account__c=:accountId and Quarter__c<>null ';
        //query +=' and (DATE__C>= LAST_N_QUARTERS:4)  order by year__C desc,quarter__c desc,month__c desc,media__c desc';
        //and account__c=:acct.Id 
        //kantarList = KantarUtil.getKantarDetails(accountId);
        
        AggregateResult[] groupedResults = KantarUtil.getKantarDetails(accountId);
        SYSTEM.DEBUG('SIZE ' + kantarList);
        kantarWrapperList = new list<KantarWrapper>();
        map<string,decimal> keySpend = new map<string,decimal>();
        keyMediaSpendByQuarter = new map<string,map<string,decimal>>();
        
        map<string,decimal> mediaSpend = new map<string,decimal>();
        /*for(kantar__c kc:kantarList)
        {            
            
            if(!keyMediaSpendByQuarter.containsKey(kc.Media__c))
            {
                mediaSpend = new map<string,decimal>();
                system.debug('kc.Quarter__c' + kc.Media__c + '----' + kc.Quarter__c + '~' +  kc.Year__c + '--' +kc.Spend__c);
                mediaSpend.put(kc.Quarter__c + '~' +  kc.Year__c,kc.Spend__c);
                system.debug('keySpend IF' + mediaSpend);
                keyMediaSpendByQuarter.put(kc.Media__c,mediaSpend);
                system.debug('keyMediaSpendByQuarter ' + keyMediaSpendByQuarter);
            }
            else
            {
                //mediaSpend = new map<string,decimal>();
                //mediaSpend.putall(keyMediaSpendByQuarter.get(kc.Media__c));
                string key = kc.quarter__c + '~' + kc.year__c;
                    system.debug('kc.Quarter__c1' + kc.Media__c);
                    system.debug('kc.Quarter__c1' + key);
                    system.debug('kc.Quarter__c1' + (keyMediaSpendByQuarter.get(kc.Media__c).containsKey(key)));
                    system.debug('kc.Quarter__c1' + (keyMediaSpendByQuarter.get(kc.Media__c).get(key)) + '----' + (kc.spend__c));
                if(keyMediaSpendByQuarter.get(kc.Media__c).containsKey(key)){
                    mediaSpend.put(key,keyMediaSpendByQuarter.get(kc.Media__c).get(key) + kc.spend__c);
                    //keyMediaSpendByQuarter.put(kc.Media__c,mediaSpend);
                    }
                else{
                    mediaSpend = new Map<string,decimal>();
                    mediaSpend.putall(keyMediaSpendByQuarter.get(kc.Media__c));
                    mediaSpend.put(key, kc.spend__c);
                    
                    
                }
                keyMediaSpendByQuarter.put(kc.Media__c,mediaSpend);
                
                system.debug('keyMediaSpendByQuarter.get(kc.Media__c) ' + mediaSpend);
                system.debug('keyMediaSpendByQuarter.get(kc.Media__c) ' + keyMediaSpendByQuarter);
                
                
            }
            
            //
        }*/
        
        for(AggregateResult ar :groupedResults)
        {
            system.debug('ar.data' + ar.get('Media__c') + '----' + ar.get('expr0') + '~' +  ar.get('expr1') + '--' +ar.get('expr2'));
        }
        
        for(AggregateResult ar :groupedResults)
        {            
            
            if(!keyMediaSpendByQuarter.containsKey((string)ar.get('Media__c')))
            {
                mediaSpend = new map<string,decimal>();
                system.debug('data' + ar.get('Media__c') + '----' + ar.get('expr2') + '~' +  ar.get('expr1') + '--' +ar.get('expr0'));
                mediaSpend.put(string.valueof(ar.get('expr2')) + '~' +  string.valueof(ar.get('expr1')),(decimal)ar.get('expr0'));
                system.debug('keySpend IF' + mediaSpend);
                keyMediaSpendByQuarter.put((string)ar.get('Media__c'),mediaSpend);
                system.debug('keyMediaSpendByQuarter ' + keyMediaSpendByQuarter);
            }
            else
            {
                //mediaSpend = new map<string,decimal>();
                //mediaSpend.putall(keyMediaSpendByQuarter.get(ar.get('Media__c')));
                string key = ar.get('expr2') + '~' + ar.get('expr1');
                    system.debug('kc.Quarter__c1' + ar.get('Media__c'));
                    system.debug('kc.Quarter__c1' + key);
                    system.debug('kc.Quarter__c1' + (keyMediaSpendByQuarter.get((string)ar.get('Media__c')).containsKey(key)));
                    system.debug('kc.Quarter__c1' + (keyMediaSpendByQuarter.get((string)ar.get('Media__c')).get(key)) + '----' + (ar.get('expr0')));
                if(keyMediaSpendByQuarter.get((string)ar.get('Media__c')).containsKey(key)){
                    mediaSpend.put(key,keyMediaSpendByQuarter.get((string)ar.get('Media__c')).get(key) + (decimal)ar.get('expr0'));
                    //keyMediaSpendByQuarter.put(ar.get('Media__c'),mediaSpend);
                    }
                else{
                    mediaSpend = new Map<string,decimal>();
                    mediaSpend.putall(keyMediaSpendByQuarter.get((string)ar.get('Media__c')));
                    mediaSpend.put(key, (decimal)ar.get('expr0'));
                    
                    
                }
                keyMediaSpendByQuarter.put((string)ar.get('Media__c'),mediaSpend);
                
                //system.debug('keyMediaSpendByQuarter.get(ar.get('Media__c'))  + mediaSpend);
                //system.debug('keyMediaSpendByQuarter.get(ar.get('Media__c')) ' + keyMediaSpendByQuarter);
                
                
            }
            
            //
        }
        
        system.debug('SIZE ' + keyMediaSpendByQuarter.size());
        system.debug('SIZE ' + keyMediaSpendByQuarter);
        
        date settingsDate;
        Custom_Constants__c kantarDate = Custom_Constants__c.getValues('Kantar Date');
        if(kantarDate!=null && kantarDate.Value__c!=null)
        {
            settingsDate = date.valueOf(kantarDate.Value__c);
        }
        
        for(string media:keyMediaSpendByQuarter.keyset())
        {
            KantarWrapper kd = new KantarWrapper();
            kd.media = media;
            keySpend = new map<string,decimal>();
            keySpend = keyMediaSpendByQuarter.get(media);
            System.debug('Condition ' + media);
            System.debug('Condition ' + keySpend);
            //integer currentQuarter = CEILING( MONTH ( system.today() ) / 3 );
            
            Date todayDate;
            if(settingsDate==null)
                todayDate = System.today().toStartOfMonth();
            else
                todayDate = settingsDate;
                
            integer year =  todayDate.year();
            string currentQtr = Decimal.valueOf(todayDate.month()).divide(3.0, 0, System.RoundingMode.UP).intValue() + '~' + year; 
            string previouButOneQtr;
            string previouButTwoQtr;
            string previouButThreeQtr;
            string previouButFourQtr;
            string previouButFiveQtr;
            if(currentQtr==4 + '~' +year)
            {
                previouButOneQtr = 3 + '~' +year;
                previouButTwoQtr = 2 + '~' +year;
                previouButThreeQtr = 1 + '~' +year;
                previouButFourQtr = 4 + '~' +(year-1);
                previouButFiveQtr = 3 + '~' + (Year-1);
            }
            else if(currentQtr==3 + '~' +year)
            {
                previouButOneQtr = 2 + '~' +year;
                previouButTwoQtr = 1 + '~' +year;
                previouButThreeQtr = 4 + '~' +(year-1);
                previouButFourQtr = 3 + '~' +(year-1);
                previouButFiveQtr = 2 + '~' + (Year-1);
            }
            else if(currentQtr==2 + '~' +year)
            {
                previouButOneQtr = 1 + '~' +year;
                previouButTwoQtr = 4 + '~' +(year-1);
                previouButThreeQtr = 3 + '~' +(year-1);
                previouButFourQtr = 2 + '~' +(year-1);
                previouButFiveQtr = 1 + '~' + (Year-1);
                
            }
            else if(currentQtr==1 + '~' +year)
            {
                previouButOneQtr = 4 + '~' +(year-1);
                previouButTwoQtr = 3 + '~' +(year-1);
                previouButThreeQtr = 2 + '~' +(year-1);
                previouButFourQtr = 1 + '~' +(year-1);
                previouButFiveQtr = 4 + '~' + (Year-2);
            }
            for(string s:keySpend.keyset()){
                string[] spl = s.split('~');
                system.debug('spl' + spl);
                
                kd.year = spl[1];
                
                /*system.debug('Condition 1 ' + (integer.valueof(spl[0])==currentQtr && kd.quarter1Spend1==NULL));
                system.debug('Condition 1 ' + kd.quarter1Spend1);
                
                
                system.debug('Condition 2 ' + (integer.valueof(spl[0])==previouButOneQtr && kd.quarter2Spend2==NULL));
                system.debug('Condition 2 ' + kd.quarter2Spend2);
                
                system.debug('Condition 3 ' + (integer.valueof(spl[0])==previouButTwoQtr && kd.quarter3Spend3==NULL));
                system.debug('Condition 3 ' + kd.quarter3Spend3);
               
                system.debug('Condition 4 ' + (integer.valueof(spl[0])==previouButThreeQtr && kd.quarter4Spend4==NULL));
                system.debug('Condition 4 ' + kd.quarter4Spend4);
                
                system.debug('Condition 4 ' + (integer.valueof(spl[0])==previouButFourQtr && kd.quarter5Spend5==NULL));
                system.debug('Condition 4 ' + kd.quarter5Spend5);
                */
                
                
                
                if(spl[0]+'~'+spl[1]==currentQtr && kd.quarter1Spend1==NULL)
                {
                    kd.quarter1Spend1 = keySpend.get(s);
                }
                else if(spl[0]+'~'+spl[1]==(previouButOneQtr) && kd.quarter2Spend2==NULL)
                {
                    kd.quarter2Spend2 = keySpend.get(s);
                }
                else if(spl[0]+'~'+spl[1]==(previouButTwoQtr) && kd.quarter3Spend3==NULL)
                {
                    kd.quarter3Spend3 = keySpend.get(s);
                }
                else if(spl[0]+'~'+spl[1]==(previouButThreeQtr) && kd.quarter4Spend4==NULL)
                {
                    kd.quarter4Spend4 = keySpend.get(s);
                }
                else if(spl[0]+'~'+spl[1]==(previouButFourQtr) && kd.quarter5Spend5==NULL)
                {
                    kd.quarter5Spend5 = keySpend.get(s);
                }
                else if(spl[0]+'~'+spl[1]==(previouButFiveQtr) && kd.quarter6Spend6==NULL)
                {
                    kd.quarter6Spend6 = keySpend.get(s);
                }
            }
            kantarWrapperList.add(kd);
            
            
        }
        
        
        
    
    }
    
    
    


}