@isTest(seeAlldata = false)
public class Kantar_SummaryExtensionTest{
    public static Account testAccount;
    public static Kantar__c testKantar;
    public static list<Kantar__c> lstKantarRecords;
    
    
    // Accounts
    public static Kantar__c createKantar(id accountId,integer month, integer year,integer quarter,date adDate) {
        return new Kantar__c(
            Account__c = accountId,
            Brand__c = 'TEST BRAND',
            Category__c = 'TEST CATEGORY',
            Market__c = 'COLUMBIA,SC',
            Spend__c = 10000,
            Media__c = 'TEST RADIO',
            Month__c = month,
            Quarter__c = quarter,
            Year__c = year,
            Ad_Date__c = adDate
        );
    }
    
    public static void setUpdata(){
      
      testAccount = UTIL_TestUtil.createAccount();
      date dt1 = date.parse('02/01/2017');
      lstKantarRecords = new list<Kantar__c>();
      testKantar = createKantar(testAccount.id,2,2017,1,dt1);
      lstKantarRecords.Add(testKantar);
      date dt2 = date.parse('12/01/2016');
      testKantar = createKantar(testAccount.id,12,2016,4,dt2);
      lstKantarRecords.Add(testKantar);
      date dt3 = date.parse('08/01/2016');
      testKantar = createKantar(testAccount.id,08,2016,3,dt3);
      lstKantarRecords.Add(testKantar);
      date dt4 = date.parse('05/01/2016');
      testKantar = createKantar(testAccount.id,05,2016,2,dt4);
      lstKantarRecords.Add(testKantar);
      date dt5 = date.parse('02/01/2016');
      testKantar = createKantar(testAccount.id,02,2016,1,dt5);
      lstKantarRecords.Add(testKantar);
      date dt6 = date.parse('12/01/2015');
      testKantar = createKantar(testAccount.id,12,2015,4,dt6);
      lstKantarRecords.Add(testKantar);
      
      
      
      date dt7 = date.parse('08/01/2015');
      testKantar = createKantar(testAccount.id,08,2015,3,dt7);
      lstKantarRecords.Add(testKantar);
      date dt8 = date.parse('05/01/2015');
      testKantar = createKantar(testAccount.id,05,2015,2,dt8);
      lstKantarRecords.Add(testKantar);
      date dt9 = date.parse('02/01/2015');
      testKantar = createKantar(testAccount.id,02,2015,1,dt9);
      lstKantarRecords.Add(testKantar);
      
      date dt10 = date.parse('12/01/2014');
      testKantar = createKantar(testAccount.id,12,2014,4,dt10);
      lstKantarRecords.Add(testKantar);
      date dt11 = date.parse('08/01/2014');
      testKantar = createKantar(testAccount.id,08,2014,3,dt11);
      lstKantarRecords.Add(testKantar);
      date dt12 = date.parse('05/01/2014');
      testKantar = createKantar(testAccount.id,05,2014,2,dt12);
      lstKantarRecords.Add(testKantar);
      date dt13 = date.parse('02/01/2014');
      testKantar = createKantar(testAccount.id,02,2014,1,dt13);
      lstKantarRecords.Add(testKantar);
      
      date dt14 = date.parse('12/01/2013');
      testKantar = createKantar(testAccount.id,12,2013,4,dt14);
      lstKantarRecords.Add(testKantar);
      date dt15 = date.parse('08/01/2013');
      testKantar = createKantar(testAccount.id,08,2013,3,dt15);
      lstKantarRecords.Add(testKantar);
      date dt16 = date.parse('05/01/2013');
      testKantar = createKantar(testAccount.id,05,2013,2,dt16);
      lstKantarRecords.Add(testKantar);
      date dt17 = date.parse('02/01/2013');
      testKantar = createKantar(testAccount.id,02,2013,1,dt17);
      lstKantarRecords.Add(testKantar);
      date dt18 = date.parse('12/01/2012');
      testKantar = createKantar(testAccount.id,12,2012,4,dt18);
      lstKantarRecords.Add(testKantar);
      
      
      
      insert lstKantarRecords;
      
      
      

      

    }
    public static testMethod void testConstructorWithCurrentDate(){
        
        setUpdata();
        Test.starttest();
        ApexPages.StandardController sc = new ApexPages.StandardController(testAccount);
        Kantar_SummaryExtension kantarSummaryExtensionObj = new Kantar_SummaryExtension(sc);
        
        PageReference pageRef = Page.Kantar_Summary;
        pageRef.getParameters().put('id', String.valueOf(testAccount.Id));
        Test.setCurrentPage(pageRef);
        //ctrlrObj = new Kantar_SummaryExtension();
        Test.stopTest();
        list<Kantar__c> accountRelatedKantarRecords = [select id from Kantar__c where Account__c=:testAccount.Id];
        system.assert(accountRelatedKantarRecords.size()>0);
    
    }
    public static testMethod void testConstructorWithCurrentQuarter(){
        
        Custom_Constants__c cc = new Custom_Constants__c();
        cc.Name='Kantar Date';
        cc.Value__c = '2017-05-01';
        insert cc;
        KantarUtil kt = new KantarUtil();
        setUpdata();
        Test.starttest();
        ApexPages.StandardController sc = new ApexPages.StandardController(testAccount);
        Kantar_SummaryExtension kantarSummaryExtensionObj = new Kantar_SummaryExtension(sc);
        
        PageReference pageRef = Page.Kantar_Summary;
        pageRef.getParameters().put('id', String.valueOf(testAccount.Id));
        Test.setCurrentPage(pageRef);
        //ctrlrObj = new Kantar_SummaryExtension();
        Test.stopTest();
        list<Kantar__c> accountRelatedKantarRecords = [select id from Kantar__c where Account__c=:testAccount.Id];
        system.assert(accountRelatedKantarRecords.size()>0);
    
    }
    
    public static testMethod void testConstructorWithPreviousQuarter(){
        
        Custom_Constants__c cc = new Custom_Constants__c();
        cc.Name='Kantar Date';
        cc.Value__c = '2017-02-01';
        insert cc;
        KantarUtil kt = new KantarUtil();
        setUpdata();
        Test.starttest();
        ApexPages.StandardController sc = new ApexPages.StandardController(testAccount);
        Kantar_SummaryExtension kantarSummaryExtensionObj = new Kantar_SummaryExtension(sc);
        
        PageReference pageRef = Page.Kantar_Summary;
        pageRef.getParameters().put('id', String.valueOf(testAccount.Id));
        Test.setCurrentPage(pageRef);
        //ctrlrObj = new Kantar_SummaryExtension();
        Test.stopTest();
        list<Kantar__c> accountRelatedKantarRecords = [select id from Kantar__c where Account__c=:testAccount.Id];
        system.assert(accountRelatedKantarRecords.size()>0);
    
    }
    
    public static testMethod void testConstructorWithPreviousButOneQuarter(){
        
        Custom_Constants__c cc = new Custom_Constants__c();
        cc.Name='Kantar Date';
        cc.Value__c = '2016-12-01';
        insert cc;
        KantarUtil kt = new KantarUtil();
        setUpdata();
        Test.starttest();
        ApexPages.StandardController sc = new ApexPages.StandardController(testAccount);
        Kantar_SummaryExtension kantarSummaryExtensionObj = new Kantar_SummaryExtension(sc);
        
        PageReference pageRef = Page.Kantar_Summary;
        pageRef.getParameters().put('id', String.valueOf(testAccount.Id));
        Test.setCurrentPage(pageRef);
        //ctrlrObj = new Kantar_SummaryExtension();
        Test.stopTest();
        list<Kantar__c> accountRelatedKantarRecords = [select id from Kantar__c where Account__c=:testAccount.Id];
        system.assert(accountRelatedKantarRecords.size()>0);
    
    }
    
    public static testMethod void testConstructorWithPreviousButTwoQuarter(){
        
        Custom_Constants__c cc = new Custom_Constants__c();
        cc.Name='Kantar Date';
        cc.Value__c = '2016-08-01';
        insert cc;
        KantarUtil kt = new KantarUtil();
        setUpdata();
        Test.starttest();
        ApexPages.StandardController sc = new ApexPages.StandardController(testAccount);
        Kantar_SummaryExtension kantarSummaryExtensionObj = new Kantar_SummaryExtension(sc);
        
        PageReference pageRef = Page.Kantar_Summary;
        pageRef.getParameters().put('id', String.valueOf(testAccount.Id));
        Test.setCurrentPage(pageRef);
        //ctrlrObj = new Kantar_SummaryExtension();
        Test.stopTest();
        list<Kantar__c> accountRelatedKantarRecords = [select id from Kantar__c where Account__c=:testAccount.Id];
        system.assert(accountRelatedKantarRecords.size()>0);
    
    }

    
}