/*
 *  @name: KeyDecisionMakerEditController
 *  @desc: Edit VF for Key Decision Maker
 *  @author: Lakshman(sfdcace@gmail.com)
 *  @date: 06/07/2014 
 */
public without sharing class KeyDecisionMakerEditController {
    
    private Key_Decision_Maker_And_Influencer__c objKDMI;
    private ApexPages.StandardController sController;
    private String retURL;
    
    public KeyDecisionMakerEditController (ApexPages.StandardController controller) {
        sController = controller;
        objKDMI = (Key_Decision_Maker_And_Influencer__c)controller.getRecord();
        
        if(objKDMI.Id == null) {
            objKDMI.OwnerId = UserInfo.getUserId();   
        }
        retURL = ApexPages.currentPage().getParameters().get('retURL');
    }
    
    public PageReference save() {
        PageReference detailPage = sController.save();
        if (detailPage != null) {
            // Construct URL of edit page or whatever other page you want
            PageReference mainPage = new PageReference(retURL);
            return mainPage;
        } else {
            return detailPage;
        }
    }
    
    
    public PageReference saveAndNew() {
        
        
        try {
            // Save the current sObject
            sController.save();

            // Get the Meta Data for objKDMI__c
            Schema.DescribeSObjectResult describeResult = sController.getRecord().getSObjectType().getDescribe();
            // Create PageReference for creating a new sObject and add any inbound query string parameters.
            PageReference pr = new PageReference('/' + describeResult.getKeyPrefix() + '/e?retURL=' + retURL);
            // Don't redirect with the viewstate of the current record.
            pr.setRedirect(true);
            return pr;
        } catch(Exception e) {
            // Don't redirect if something goes wrong. May be a validation or trigger issue on save.
            ApexPages.addMessages(e);
            return null;
        }
    }
}