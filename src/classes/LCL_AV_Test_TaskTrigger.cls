/*
Developer: Ralph Callaway <ralph.w.callaway@gmail.com>
Story: https://na2.salesforce.com/a0I40000003Y6zt
Description:
	Test methods for LCL_AV_TaskTrigger
*/
@isTest
private class LCL_AV_Test_TaskTrigger {

	private static final Integer BATCH_SIZE = 3;

	@isTest
	private static void testStatusAndActivtyDateDefaulting() {
		// insert tasks that meet local avails request definition
		List<Task> tasks = new List<Task>();
		for(Integer i = 0; i < BATCH_SIZE; i++) {
			tasks.add(new Task(
				subject = 'asdf' + Label.LCL_AV_AvailRequestTaskSubject + 'asdf'
			));
		}
		Test.startTest();
		insert tasks;
		Test.stopTest();
		
		// validate task statuses and due dates
		Date tomorrow = System.today().addDays(1);
		for(Task task : [
			select status, activityDate 
			from Task
			where id in :tasks
		]) {
			system.assertEquals(tomorrow, task.activityDate);
			system.assertEquals(Label.LCL_AV_AvailRequestDefaultStatus, task.status);
		}
	}

}