/****************************************************************
 * Name: CreateEmployeeCertification
 * Usage: This Class insert employee Certification from employee List view
 * Author – Clear Task
 * Date – 04/26/2011
 ****************************************************************/
 
public class LEGAL_CERT_CreateEmpCertification{

    /*variable for StandardSetController */
    ApexPages.StandardSetController setCon;
    
    /*flag for showing message*/
    public boolean msgFlag{get; set;}
    
    /*flag for showing error message*/
    public boolean errorFlag{get; set;}
    
    /*flag for showing error message*/
    public boolean showButton{get; set;}
     
    /*Variable for showing message*/
    public String msgInfo {get; set;}
    
    /*Employee Certification */
    public Employee_Certification__c ec {get;set;}
    
    /*flag for showing error message*/
    public boolean reminderBlock{get; set;}
    /*Reminder Scheduling conditions*/
    public List<ConditionRow> conditions { get; private set; }
    private Integer nextId;
    //private static final String EMAIL_TEMPLATE_FOLDER = 'Legal Cert 302'; 
    private static final String EMAIL_TEMPLATE_FOLDER = 'Legal Cert 302 Scheduled Reminders';
    private List <Employee_Certification__c> employeeInsertList;
    
    /*Constructor for showing default values*/ 
    public LEGAL_CERT_CreateEmpCertification(ApexPages.StandardSetController controller) {

        //IE9 Patch
        String browserType = Apexpages.currentPage().getHeaders().get('USER-AGENT'); 
        if(browserType != null && browserType.contains('MSIE')){
            Apexpages.currentPage().getHeaders().put('X-UA-Compatible', 'IE=8');
        }  
        
        setCon = controller;
        ec = new Employee_Certification__c();
        msgFlag = false;
        errorFlag = false;
        showButton = false;
        reminderBlock = true;
        System.debug('In Standard Controller ='+ setCon.getSelected());
        List<Employee__c> employeeList = setCon.getSelected();
        if(employeeList.isEmpty()){
            System.debug('In Empty');
            errorFlag = true;
            showButton = true;
            msgInfo  = System.Label.LEGAL_CERT_Error_Message_for_Employee;
        } 
        conditions = new List<ConditionRow>();
        nextId = 0; 
    }
    
    /*doSave method for inserting employee certification record*/ 
    public PageReference doSave(){
        Set<Id> employeeId = new Set<Id>();
        List<Employee__c> employeeRecords = setCon.getSelected();
        System.debug('employeeRecords '+employeeRecords);
        for(Employee__c e :employeeRecords){
            employeeId.add(e.Id); 
        }
        List<Employee__c> employeeRecordsInsert = [select Email__c , Id , Name, Employee_ID__c  from Employee__c where Id in :employeeId ];
        employeeInsertList = new List <Employee_Certification__c>();
        List<Certification__c> certificationList = [select id ,Name, Due_Date__c,Quarter__c,Fiscal_Year__c,Response_type__c from Certification__c where Id = :ec.Certification__c];
        System.debug('CertificationList: ' + certificationList);
        for(Employee__c e : employeeRecordsInsert){
              String strTokenNumber = '';
              strTokenNumber = LEGAL_CERT_RandomStringUtils.randomGUID();
              Employee_Certification__c employeeCertification = new Employee_Certification__c(
                    Employee__c = e.Id, 
                    Certification__c = ec.Certification__c,  
                    Token__c = strTokenNumber,
                    Email__c = e.Email__c,
                    Due_Date__c = certificationList[0].Due_Date__c, 
                    Fiscal_Quarter__c = certificationList[0].Quarter__c,
                    Fiscal_Year__c = certificationList[0].Fiscal_Year__c,
                    Response_type__c = certificationList[0].Response_type__c 
              );
              employeeInsertList.add(employeeCertification);     
        }
        try{
             if(employeeInsertList != null && employeeInsertList.size() >0){
                 //insert employeeInsertList;
                 //msgFlag = true;
                 //showButton = true;
                 //errorFlag = false;
                 //msgInfo  = System.Label.LEGAL_CERT_Employee_Certifications_Sent_1 + ' ' + certificationList[0].Name + ' ' + System.Label.LEGAL_CERT_Employee_Certifications_Sent_2;
                 return new PageReference('/apex/LEGAL_CERT_EmpCertificationReminder');  
             }
        }catch (DmlException e){
            errorFlag = true;
            showButton = true;
            msgInfo  = System.Label.LEGAL_CERT_Error_Message_for_Employeecertification;
            System.debug(e.getMessage());
        }
        return null;  
    }
    
    public PageReference onLoad() { 
        reminderBlock = true;
        msgFlag = false;           
        if (conditions.size() == 0) {            
            conditions.add(new ConditionRow(String.valueOf(nextId), new Cert_Reminder__c()));
            nextId++;
            conditions.add(new ConditionRow(String.valueOf(nextId), new Cert_Reminder__c()));
            nextId++;
            conditions.add(new ConditionRow(String.valueOf(nextId), new Cert_Reminder__c()));
            nextId++;
        }        
        return null;
    }
    
     public PageReference onAddCondition() {
        Boolean flagForReminder = true;
        errorFlag = false;
        for (Integer i=0; i<conditions.size(); i++) {
            ConditionRow row = conditions.get(i);
            if(row.condition.Date_Time__c == null){
                flagForReminder = false;
                errorFlag = true;                
                ApexPages.Message msg = new ApexPages.Message(ApexPages.Severity.FATAL, 'Please fill in the missing value below or remove the notification completely with by clicking the “Remove Notification” button.');
                ApexPages.addMessage(msg);
                break;    
            }
        }
        
        if (flagForReminder) {
            conditions.add(new ConditionRow(String.valueOf(nextId), new Cert_Reminder__c()));
            nextId++;            
        }
               
        return null;
    }
    
    public PageReference onRemoveCondition() {
        Boolean flagForReminder = true;
        errorFlag = false;        
        if(conditions == null || conditions.size() == 0){
            flagForReminder = false;
            errorFlag = true;                
            ApexPages.Message msg = new ApexPages.Message(ApexPages.Severity.FATAL, 'No notifications exist.');
            ApexPages.addMessage(msg);            
        }else{
            conditions.remove(conditions.size()-1);
            nextId--;            
        }
               
        return null;
    }
    
    public PageReference saveReminder() {
        Boolean flagForReminder = false;
        errorFlag = false;
        for (Integer i=0; i<conditions.size(); i++) {
            ConditionRow row = conditions.get(i);
            if(row.condition.Date_Time__c != null){
                flagForReminder = true;                
                break;    
            }
        }
               
        if(flagForReminder) {
            createEmpCertReminder();                                
        }else{            
            reminderBlock = false;
            errorFlag = true;                
            ApexPages.Message msg = new ApexPages.Message(ApexPages.Severity.WARNING, 'Click ok to confirm that you do not want to send any reminders to the employees selected');
            ApexPages.addMessage(msg);
            return null;    
        }
               
        return null;
    }
    
    public PageReference confirmForReminder(){
        createEmpCertReminder();
        return  new PageReference( '/' + getEmployeeKeyPrefix() + '/o');
    }
    
    /* get the key prefix for the object Employee */
   private String getEmployeeKeyPrefix() {
       Schema.Describesobjectresult result = Schema.Sobjecttype.Employee__c;
       return result.getKeyPrefix();
   }
 
    
    public void createEmpCertReminder(){
        if(employeeInsertList != null && employeeInsertList.size() >0
                 &&  conditions != null && conditions.size() >0)            
         {
             insert employeeInsertList;
             List<Certification__c> certificationList = [select id ,Name, Due_Date__c from Certification__c where Id = :ec.Certification__c];
             List <Emp_Certification_Reminder__c> empCertReminList = new List <Emp_Certification_Reminder__c>();
             for(Employee_Certification__c ec :employeeInsertList){
                 if(ec.id != null){
                    for (Integer i=0; i<conditions.size(); i++) {
                        ConditionRow row = conditions.get(i);
                        if(row.condition.Date_Time__c != null){
                            Emp_Certification_Reminder__c ecr = new Emp_Certification_Reminder__c(
                                Employee_Certification__c = ec.Id,
                                Date_Time__c = row.condition.Date_Time__c,
                                Email_Template__c = row.condition.Email_Template__c
                            );
                            empCertReminList.add(ecr);
                        }
                    }
                 }
             }
             
             if(empCertReminList != null && empCertReminList.size() >0){
                  insert empCertReminList;
                  msgFlag = true;
                  showButton = true;
                  errorFlag = false;
                  msgInfo  = System.Label.LEGAL_CERT_Employee_Certifications_Sent_1 + ' ' + certificationList[0].Name + ' ' + System.Label.LEGAL_CERT_Employee_Certifications_Sent_2;
 
             }
         }    
    }
    
    public PageReference cancleToReminder() {
        reminderBlock = true;
        msgFlag = false;
        errorFlag = false; 
        return null;    
    }
    
    public List<selectOption> getEmailTemplate(){
        List<selectOption> templateOptions = new List<selectOption>();
        List<Folder> folderList = [Select f.Type, f.SystemModstamp, f.NamespacePrefix, f.Name, 
               f.LastModifiedDate, f.LastModifiedById, f.IsReadonly, f.Id, f.DeveloperName, f.CreatedDate, 
               f.CreatedById, f.AccessType From Folder f where f.Name = :EMAIL_TEMPLATE_FOLDER LIMIT 1];
        
        //if(folderList != null && folderList.size()>0){
            List<EmailTemplate> emailTempList = [ Select e.Name, e.Id, e.FolderId From EmailTemplate e where e.Folder.Name = :EMAIL_TEMPLATE_FOLDER ];
            if(emailTempList != null && emailTempList.size()>0){
                for(EmailTemplate et :emailTempList){
                    templateOptions.add(new selectOption(et.Name, et.Name));    
                }
            }
        //} 
        return templateOptions;       
    }
    
    /*
     * sendEmailTemplate method used to send mail to employee and cc/bcc as mentioned by EMAIL template
     */
    public static Messaging.SingleEmailMessage sendEmailTemplate(String strOwnerId, List<String> toMail, EmailTemplate templateName,
            Id ec)
    {
        Messaging.SingleEmailMessage email = new Messaging.SingleEmailMessage();
        email.setToAddresses(toMail);
        email.setTemplateId(templateName.id);
        System.debug('template id = '+ templateName.id);
        email.setTargetObjectId(Label.LEGAL_CERT_EMAIL); 
        email.setOrgWideEmailAddressId(Label.LEGAL_CERT_FROM_EMAIL);
        email.setWhatId(ec);  
        email.setSaveAsActivity(false);  
        return email;
    }  
    
    public class ConditionRow {
        private String id;        
        private Cert_Reminder__c condition;        
        
        public ConditionRow(String id, Cert_Reminder__c condition) {
            this.id = id;
            this.condition = condition;            
        }
        
          public ConditionRow()
          {
            
          }
        
        public String getId() {
            return id;
        }       
        
        public Cert_Reminder__c getCondition() {
            return condition;
        }        
       
    }
}