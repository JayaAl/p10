/****************************************************************
* Name: EmployeeVerificationController
* Usage: This Class Reset the Token 
* Author – Clear Task
* Date – 04/18/2011
****************************************************************/
public without sharing Class LEGAL_CERT_EmpVerificationController{
    
    /*show show in read only mode*/
    public boolean showBlock{get; set;} 
    
    /*flag for error message*/
    public boolean errorFlag{get; set;} 
    
    /*Variable for showing message*/
    public String msgInfo {get; set;}
    
    /* variable for storing exception value*/ 
    public String strException{get; set;} 
    
    /* variable for Employee object */ 
    public Employee__c record {get; set;}
    
    /* variable for Employee Certification object */ 
    public Employee_Certification__c empCertification{get; set;}
    
    /* variable for Employee object */ 
    public List <Employee_Certification__c> employee {get;set;} 
    
    /* variable for checkbox value */ 
    public String response = null;
    
    /* flag for error message */    
    public boolean flag{get;set;}
    
    /* flag for lock record  message */   
    public boolean lockFlag{get;set;} 
    
    /* flag for reset token  message */   
    public boolean resetFlag{get;set;} 
    
    /* variable for employee Certification object */  
    public Employee_Certification__c employeeCertification ;
    
    /* email from selecting employee record */ 
    public String  email = ApexPages.currentPage().getParameters().get('email');
    
    /* email from selecting employee token */ 
    public String  token = ApexPages.currentPage().getParameters().get('token');
    
    /* email from selecting employee Id*/ 
    public String empId = ApexPages.currentPage().getParameters().get('id');
    
    /* email from selecting Certifications Id*/
    public String cert = ApexPages.currentPage().getParameters().get('cert');
    
    public String docURL {get;set;}
    
    /* variable for rendering a section */
    public boolean accept;
    
    public boolean flagResponse{get;set;}
    
    /* variable for storing Content path */
    public  String document{get;set;}
    
    public String previewURL {get;set;} 
    
    /*variable to check if exception is entered when response is I have exceptions*/
    public boolean isExceptionEntered{get;set;}
    
    static final String I_AGREE = 'I confirm and I have no exceptions';
    static final String I_EXCEPTION = 'I have exceptions';
    static final String SUBMITTED = 'Submitted';
    static final String WORKING = 'Working';
    //static final String TEMPLATE_NAME = 'Send Response to Employees no Exceptions';
    //static final String TEMPLATE_NAME_EXCEPTIONS = 'Send Response to Employees with Exceptions';
    static final String TEMPLATE_NAME_RESET = 'Security question reset email';
    
    public String getResponse() {
        return response;
    }   
    
    public void setResponse(String response) { 
        this.response = response; 
    } 
    public Blob attach{get;set;}
    
    public static Boolean getIE9(){
        Boolean IE9 = false;
        String useragent = ApexPages.currentPage().getHeaders().get('User-Agent');
        if(useragent.contains('MSIE 9.0'))IE9 = true;
        return IE9;
    }    
    
    /*Constructor for setting default values */
    public LEGAL_CERT_EmpVerificationController(ApexPages.StandardController stdController){    
        
        //IE9 Patch
        
        /*String browserType = Apexpages.currentPage().getHeaders().get('USER-AGENT'); 
if(browserType != null && browserType.contains('MSIE')) {
System.debug('Setting IE Header');
Apexpages.currentPage().getHeaders().put('X-UA-Compatible', 'IE=8');
}  
System.debug('browser1----' + browser);
if(browser == 'IE9'){
System.debug('browser----' + browser);
Apexpages.currentPage().getHeaders().put('X-UA-Compatible', 'IE=8');
} */ 
        record = (Employee__c) stdController.getRecord();
        System.debug('email'+email);
        System.debug('token '+token);
        resetFlag = false;
        flag = false;    
        isExceptionEntered= true;   
        employee = [Select e.Token__c,e.Status__c, e.Due_Date__c, e.Id, e.Employee__r.Employee_ID__c, e.Exception__c,  e.Employee__r.Answer__c,e.Certification__r.Published_Document_URL__c,e.Certification__r.Preview_Document_URL__c,
                    e.Employee__r.Question__c, e.Employee__r.Email__c,e.Response__c ,e.Employee__r.Department__c,  e.Employee__c, e.Certification__c, e.Certification__r.Due_Date__c , e.Accept__c,
                    e.Employee__r.Name,e.Email__c
                    From Employee_Certification__c e 
                    where e.Token__c = :token and e.Employee__r.Email__c = : email];
        
        
        if(employee.isEmpty()){
            System.debug(LoggingLevel.INFO, 'Employee not found');
            lockFlag  = true;
            msgInfo = System.Label.LEGAL_CERT_authorizedmessage; 
        }
        if(!employee.isEmpty()){
            Id certificationId = employee[0].Certification__c;
            String strDepartment =  employee[0].Employee__r.Department__c;           
            List<ContentVersion > contentList =[select Id , PathOnClient , VersionData , Certification__c ,Department__c from ContentVersion  where Certification__c = :certificationId and Department__c = :strDepartment];
            this.docURL = employee[0].Certification__r.Published_Document_URL__c;
            if( employee[0].Certification__r.Preview_Document_URL__c != '' ){
                this.previewURL = employee[0].Certification__r.Preview_Document_URL__c;
            } else {
                this.previewURL = this.docURL;
            }
            if(!contentList.isEmpty()){
                document =  contentList[0].PathOnClient;
                Blob attach = contentList[0].VersionData; 
                System.debug('document '+document);             
            }
            errorFlag = false;
            lockFlag = false;
            if(employee[0].Employee__r.Question__c == null){
                showBlock = true;  
                response = I_AGREE ;
            }else{
                record.Question__c = employee[0].Employee__r.Question__c;       
                showBlock = false;
            }
            if(employee[0].Status__c != null && employee[0].Status__c.equals('Submitted')){
                datetime t = System.now();            
                date d = Date.newInstance(t.year(),t.month(),t.day());       
                System.debug('Today Date'+d);
                System.debug('Today Date'+t);
                System.debug('Due Date'+employee[0].Due_Date__c);
                if(employee[0].Due_Date__c < d){
                    System.debug('In If');
                    lockFlag  = true;
                    msgInfo = System.Label.LEGAL_CERT_recordLock; 
                }
            }else{
                lockFlag = false;
            }
            if(employee[0].Accept__c){
                flag = false;
                response = I_AGREE ;
                strException = '';
                System.debug('In If'+response);
            }else{
                if(employee[0].Exception__c == null){
                    flag = false;
                    response = I_AGREE ;
                    System.debug('In else If'+response); 
                }else{
                    flag = true;
                    response = I_EXCEPTION ;
                    strException = employee[0].Exception__c ;
                    System.debug('In else'+response);
                }
            }
        }
        
        /*if(strException.equals('')){
System.debug('In strException is null' + strException);
errorFlag = true;
msgInfo = 'Please enter exceptions in the text box';

}  */
        
    }
    
    
    /* save method for checking question & answer*/
    public PageReference save(){
        if(!employee.isEmpty()){
            if(employee[0].Employee__r.Answer__c == null){
                employee[0].Employee__r.Answer__c = record.Answer__c;
                employee[0].Employee__r.Question__c = record.Question__c;
                update employee[0].Employee__r;
                showBlock = true; 
                return page.LEGAL_CERT_EmployeeVerificationSent;    
            }else{
                if(record.Answer__c == employee[0].Employee__r.Answer__c){
                    showBlock = false; 
                    errorFlag = false;
                    return page.LEGAL_CERT_EmployeeVerificationSent;   
                }else{
                    errorFlag = true;
                    msgInfo = System.Label.LEGAL_CERT_ErrorMessage; 
                    return null;
                }
            }
        }
        return null;
    }
    
    
    /*getResponseList method for List of checkbox */     
    public List<SelectOption> getResponseList() {      
        List<SelectOption> options = new List<SelectOption>();
        Schema.DescribeFieldResult responseValues = Employee_Certification__c.Response__c.getDescribe(); 
        List<Schema.PicklistEntry> Ple = responseValues.getPicklistValues();
        for(Schema.PicklistEntry p : ple){
            options.add(new SelectOption(p.getValue(),p.getlabel()));
        }
        return options;
    }
    
    /*showTextFields method for rendering the section*/
    public void showTextFields(){
        if(response != null){
            if(response.equals(I_EXCEPTION)){
                flag = true;
            }else{
                strException = '';
                flag = false;
            }
        }
    }
    
    /*saveAndSubmit  method update record to locked*/
    public  void saveAndSubmit(){   
        System.debug('strException = ' + strException);        
        if(response == I_EXCEPTION){
            if(strException == null || strException.equals('')){
                isExceptionEntered = false;                                                 
            }   
            else{
                isExceptionEntered = true;
            }   
        }
        else{
            isExceptionEntered = true;
        }              
        
        
        
        if(isExceptionEntered){
            
            
            //String TEMPLATE_NAME = 'Send Response to Employees no Exceptions'; 
            System.debug('>>>>>>>>>Start of save and submit employee: ' + employee);
            String TEMPLATE_NAME = 'Send Response to Employees no Exceptions';
            String content = '';
            String subject = '';
            if(!employee.isEmpty()){
                String problem;
                if(strException!= null ){
                    System.debug('In strException != null');
                    if(response.equals(I_EXCEPTION)){
                        accept = false;
                        System.debug('strException'+strException);
                        TEMPLATE_NAME = 'Send Response to Employees with Exceptions';
                    }else{
                        accept = true;
                        System.debug('strException not'+strException);
                        strException = '';
                        problem = '';
                    }
                    System.debug('In If'+accept);
                    problem = strException;
                    System.debug('In If'+strException);
                }else{
                    System.debug('In strException = null');
                    if(response.equals(I_AGREE)){
                        accept = true;
                    }else{
                        accept = false;
                    }
                    problem = null;
                }
                
                
                System.debug('In If'+employeeCertification);
                System.debug('employeeCertification: ' + employeeCertification);
                try{
                    employee[0].Status__c = SUBMITTED;
                    employee[0].Exception__c = problem ;              
                    employee[0].Response__c = response;               
                    employee[0].Accept__c = accept;  
                    
                    
                    
                    LEGAL_CERT_Util.upsertEmployee(employee[0]);
                    
                    System.debug('>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>employee: ' + employee[0]);              
                    
                    
                    
                    
                    //update employee;            
                    errorFlag = true;
                    msgInfo = System.Label.LEGAL_CERT_Success_Message; 
                    
                }catch (DmlException e){
                    System.debug('Error when updating employee certification: ' + e.getMessage());
                }
                System.debug('In Mail'+token + 'Email'+email);
                System.debug('token '+employee[0].Employee__r.Name);
                
                if(accept){
                    content = employee[0].Employee__r.Name +  ', <br /><br /> Your quarterly certification has been successfully submitted.  You have replied "I confirm and I have no exceptions".  <br /><br />Thank you, <br />The Pandora Legal Team ';
                    subject = 'Your quarterly certification has been successfully submitted with no exceptions';
                }
                else{
                    content = employee[0].Employee__r.Name +  ', <br /><br /> Your quarterly certification has been successfully submitted.  You have replied "I have exceptions".  ' + 
                        '<br /><br /> Your exceptions are listed below: <br />' + employee[0].Exception__c + 
                        '<br /><br />Thank you, <br />The Pandora Legal Team ';
                    subject = 'Your quarterly certification has been successfully submitted with exceptions';
                }
                content = content.replaceAll('\r\n', '<br/>');
                // content = content.replaceAll('\n', 'WORLD');
                
                LEGAL_CERT_Util.sendResponseEmail(employee[0] ,subject,content);
                resetFlag = true;
                msgInfo = System.Label.LEGAL_CERT_Success_Message;          
                
            }
        }   
        
        
    }
    
    /*dosave method for updating values*/
    public  PageReference saveForLater(){
        System.debug('saveForLater(): strException = ' + strException); 
        if(!employee.isEmpty()){
            String problem;
            if(strException!= null ){
                if(response.equals(I_EXCEPTION)){
                    accept = false;
                }else{
                    accept = true;
                    strException = '';
                    problem = '';
                }
                System.debug('In If'+accept);
                problem = strException;
                System.debug('In If'+strException);
            }else{
                if(response.equals(I_AGREE)){
                    accept = true;
                }else{
                    accept = false;
                }
                problem = null;
            }
            System.debug('In If '+accept);
            employeeCertification = new Employee_Certification__c(
                Id = employee[0].Id ,
                Employee__c = employee[0].Employee__c,  
                Certification__c = employee[0].Certification__c,
                Exception__c = problem ,
                Accept__c = accept,
                Response__c = response,
                Status__c = WORKING 
            );
            try{
                employee[0].Status__c = WORKING;
                employee[0].Exception__c = problem;
                employee[0].Response__c = response;
                LEGAL_CERT_Util.upsertEmployee(employee[0]);
                System.debug('employee = ' + employee);
                System.debug('employeeCertification = ' + employeeCertification);
                //upsert employeeCertification;
                LEGAL_CERT_Util.upsertCertification(employeeCertification);
                errorFlag = true;
                msgInfo = System.Label.LEGAL_CERT_Save_for_later_Message; 
                return null; 
            }catch (DmlException e){
                System.debug(e.getMessage());
            }
        }
        
        return null;
    }
    
    /* resetToken method for reset to token and send email*/
    public void resetToken(){
        String strTokenNumber = '';
        strTokenNumber = LEGAL_CERT_RandomStringUtils.randomGUID();
        Employee__c employeeUpdate = new Employee__c(
            Id = employee[0].Employee__c,
            Answer__c = null,
            Question__c = null
        );
        
        Employee_Certification__c employeeCertificationforReset = new Employee_Certification__c(
            Id = employee[0].Id ,
            Token__c = strTokenNumber
        );
        try{
            if(employeeUpdate!= null){
                update employeeUpdate;
            } 
            System.debug('employeeCertificationforReset  '+employeeCertificationforReset);
            if(employeeCertificationforReset  != null)
                update employeeCertificationforReset;
            resetFlag = true;
            msgInfo = System.Label.LEGAL_CERT_Reset_Token;  
        }catch (DmlException e){
            System.debug(e.getMessage());
        } 
        Employee_Certification__c employeeUpdateToken = [Select e.Token__c,e.Status__c, e.Id, e.Employee__r.Employee_ID__c, e.Exception__c,  e.Employee__r.Answer__c, 
                                                         e.Employee__r.Question__c, e.Employee__r.Email__c, e.Employee__c, e.Certification__c, e.Accept__c, e.Employee__r.Name 
                                                         From Employee_Certification__c e 
                                                         where e.Token__c = :strTokenNumber and e.Employee__r.Email__c = : email]; 
        String subject = 'Your security question and corresponding answer used to access your quarterly certification have been reset';
        String content = employeeUpdateToken.Employee__r.Name + ', <br /><br /> Your security question and corresponding answer ' + 
            'used to access your quarterly certification have been reset. ' + 
            'Please click the link below to continue your login.<br/><br />' + 
            '<a href=" ' + Label.LEGAL_CERT_SFDC_Domain + '/LEGAL_CERT_EmployeeVerification?email=' + employeeUpdateToken.Employee__r.Email__c + Label.LEGAL_CERT_Ampersand + 'token=' + strTokenNumber + '">Quarterly Certification</a> <br /><br />' + 
            'Thank you,<br />The Pandora Legal Team';
        
        LEGAL_CERT_Util.sendResponseEmail(employeeUpdateToken, subject, content);
        
        /*Messaging.SingleEmailMessage mail = new Messaging.SingleEmailMessage(); 
EmailTemplate template = [select id, body, ownerid ,subject from EmailTemplate where Name = :TEMPLATE_NAME_RESET];
String[] toAddresses = new String[] {employeeUpdateToken.Employee__r.Email__c};
mail.setToAddresses(toAddresses);
//mail.setWhatId(employee[0].Id);
mail.setTemplateId(template.id);
//mail.setTargetObjectId(template.ownerid); 
mail.setTargetObjectId(userinfo.getuserId());       
mail.setWhatId(employeeUpdateToken.Id);  
mail.setSaveAsActivity(false); 
try{
Messaging.SendEmailResult [] r =Messaging.sendEmail(new Messaging.SingleEmailMessage [] {mail}); 
resetFlag = true;
//msgInfo = System.Label.Reset_Token; 
}catch(Exception e){ } */
    }
    
    /*method for docancel*/
    public  PageReference docancel(){
        Id str = employee[0].Id;
        System.debug('record'+str);
        PageReference pr = new PageReference('/'+str);
        pr.setRedirect(true);
        return pr;
    }
    
    public PageReference onLoad() {
        String browserType = Apexpages.currentPage().getHeaders().get('USER-AGENT'); 
        if(browserType != null && browserType.contains('MSIE')) {
            System.debug('IE9 being used');
            //return Page.IE9_Generic_Error;
        }
        return null;    
    }
    
    public String browser {get; set;}
    
    public String getCurrentPageURL() {
        return System.currentPageReference().getURL();
    }
}