global class LEGAL_CERT_RandomStringUtils {

    private static String kHexChars = '0123456789abcdefABCDEF';

    /**
    * Implementation ported from Java Library at http://jug.safehaus.org
    */
    global static String randomGUID(){
        String returnValue = '';
        Integer nextByte = 0;
        for(Integer i = 0; i < 16; i++){
        if(i==4 || i==6 || i==8 || i==10){
        returnValue += '-';
        }
        //generate a "byte"; i.e., number in range [-2^7,2^7-1]
        nextByte = (Math.round(Math.random() * 255)-128) & 255;
        
        if(i==6){
        nextByte = nextByte & 15;
        nextByte = nextByte | (4 << 4);
        }
        if(i==8){
        nextByte = nextByte & 63;
        nextByte = nextByte | 128;
        }
        
        returnValue +=  LEGAL_CERT_StringUtils.charAt(kHexChars,nextByte >> 4);
        returnValue +=  LEGAL_CERT_StringUtils.charAt(kHexChars,nextByte & 15);
        }
        return returnValue; 
    }
    
    global static String randomUserID(){
        String returnValue = 'ws';
        Integer nextByte = 0;
        for(Integer i = 0; i < 6; i++){
        
        //generate a "byte"; i.e., number in range [-2^7,2^7-1]
        nextByte = (Math.round(Math.random() * 10)); // -128) & 255;
        if(nextByte == 10){
            nextByte = 1;
        }
        returnValue += nextByte;
        
        }
        return returnValue; 
    }
    
    public static testMethod void testRandomGUID() {
        LEGAL_CERT_RandomStringUtils.randomGUID();
        LEGAL_CERT_RandomStringUtils.randomUserID();
    }
}