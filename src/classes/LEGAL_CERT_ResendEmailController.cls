public class LEGAL_CERT_ResendEmailController{
    
    public Employee_Certification__c empObj;
     
    public LEGAL_CERT_ResendEmailController(ApexPages.StandardController controller){
        empObj = (Employee_Certification__c)controller.getRecord();   
    }  
       
    public PageReference sendEmail(){
        empObj = [ Select Id, Employee__r.Name, Employee__c, Token__c From Employee_Certification__c where id = :empObj.id]; 
        
        if(empObj.Token__c == null) {
            ApexPages.Message msg = new ApexPages.Message(ApexPages.Severity.CONFIRM, System.Label.TokenNotExist); 
            ApexPages.addMessage(msg);
            return null;
        }
    
        empObj.Send_Employee_Certification__c = true;        
        update empObj;
        
        ApexPages.Message msg = new ApexPages.Message(ApexPages.Severity.CONFIRM, System.Label.EmailSent + ' ' + empObj.Employee__r.Name + '.'); 
        ApexPages.addMessage(msg);
        return null;
    }                
}