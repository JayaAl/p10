global class LEGAL_CERT_StringUtils {

    global static final String EMPTY = '';
    global static final String LF = '\n';
    global static final String CR = '\r';
    global static final Integer INDEX_NOT_FOUND = -1;

    private static final Integer PAD_LIMIT = 8192;
    
    global static String charAt(String str, Integer index) {
        if(str == null){
            return null;
        }
        if(str.length() <= 0){
            return str;
        }
        if(index < 0 || index >= str.length()){
            return null;
        }
        return str.substring(index, index+1);
    }

    public static testmethod void testCharAt(){
        assertCharAt(null, -1, null);
        assertCharAt(null, 0, null);
        assertCharAt('abc', -1, null);
        assertCharAt('abc', 3, null);
        assertCharAt('abc', 0, 'a');
        assertCharAt('abc', 2, 'c');
        assertCharAt('abcde', -2, null);
        assertCharAt('abcde', 0, 'a');
        assertCharAt('abcde', 1, 'b');
        assertCharAt('abcde', 4, 'e');
        assertCharAt('abcde', 5, null);
        assertCharAt('', 0, '');
        assertCharAt(' ', 0, ' ');
    }
    
    public static void assertCharAt(String str, Integer index, String expected){
        String actual = LEGAL_CERT_StringUtils.charAt(str,index);
        System.assert(actual==expected, 'StringUtils.charAt(\'' + str + '\',' + index + ') returned ' + actual);
    }
    

}