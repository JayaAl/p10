@isTest
private class LEGAL_CERT_TestCreateEmpCertification{
   static testmethod void testUnitCase(){
       List<Employee__c> employee = new List<Employee__c>();
       Employee__c emp = new Employee__c(
           Name = 'test', 
           Email__c = 'testing@yahoo.com',
           Employee_ID__c = '7y17',
           Record_lock__c = false,
           Answer__c = 'test',
           Question__c = 'On what street did you grow up?'
           
       );
       //insert emp;
       employee.add(emp);
       
       Employee__c empRecord = new Employee__c(
           Name = 'test1', 
           Email__c = 'test@yahoo.com',
           Employee_ID__c = '7y17rr',
           Record_lock__c = false,
           Answer__c = 'test',
           Question__c = 'On what street did you grow up?'
           
       );
       //insert empRecord;
       employee.add(empRecord );
       insert employee;
       System.debug('In List'+employee); 
       Set<String> employeeName = new Set<String>();
       for(Employee__c e :employee){
           employeeName.add(e.Name);     
       }
       System.debug('In Set'+employeeName); 
       Certification__c cerfication = new Certification__c(
           Name = 'document'
       );
       insert cerfication; 
       
       ApexPages.StandardSetController controller = new ApexPages.StandardSetController(employee);
       controller.setSelected(employee);
       LEGAL_CERT_CreateEmpCertification objCreateEmployee  = new LEGAL_CERT_CreateEmpCertification(controller);
       objCreateEmployee.ec.Certification__c = cerfication.Id;
       objCreateEmployee.doSave();
       objCreateEmployee.onLoad();
       objCreateEmployee.confirmForReminder();
       objCreateEmployee.cancleToReminder();
       objCreateEmployee.onAddCondition();
       objCreateEmployee.onRemoveCondition();
       objCreateEmployee.saveReminder();
       objCreateEmployee.getEmailTemplate();
      LEGAL_CERT_CreateEmpCertification.ConditionRow co = new LEGAL_CERT_CreateEmpCertification.ConditionRow();
      co.getId();
      co.getCondition();
      
       
   }
}