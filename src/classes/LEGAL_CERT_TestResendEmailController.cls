@istest
private class LEGAL_CERT_TestResendEmailController {
  private static testmethod void resendEmail(){
    
    Certification__c objCertification = new Certification__c(
        Name = 'test'
    );
    insert objCertification ;
    
    Employee__c  objEmployee = new Employee__c(
        Name = 'test'
    );
    insert objEmployee ;
    
    Employee_Certification__c  objEmp = new Employee_Certification__c (
        Employee__c = objEmployee.id ,
        Certification__c = objCertification.id,
        Token__c = '1234'
    );
    insert objEmp;
       
    ApexPages.StandardController controller = new ApexPages.StandardController(objEmp);
       
    LEGAL_CERT_ResendEmailController objResendMail = new LEGAL_CERT_ResendEmailController(controller );   
    objResendMail.sendEmail();
    
    objEmp.Token__c = null;
    update objEmp;
    objResendMail.sendEmail();
  }

}