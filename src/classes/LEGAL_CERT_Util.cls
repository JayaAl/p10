public without sharing class LEGAL_CERT_Util {

    public static void upsertCertification(Employee_Certification__c employeeCertification) {
        try{
            upsert employeeCertification;
        }catch(Exception e){
             System.debug('In Catch= ' + e.getMessage());
        }
    }
    public static void upsertEmployee(Employee_Certification__c employee) {
        try{
            upsert employee;
        }catch(Exception e){
            System.debug('In Catch= ' + e.getMessage());
        }
    }
   /* public static void sendResponseEmail(Employee_Certification__c employeeUpdateToken , String TEMPLATE_NAME) {
         Messaging.SingleEmailMessage mail = new Messaging.SingleEmailMessage(); 
            System.debug('employeeUpdateToken ' + employeeUpdateToken );
            System.debug('TEMPLATE_NAME' + TEMPLATE_NAME);
            EmailTemplate template = [select id, body, ownerid ,subject from EmailTemplate where Name = :TEMPLATE_NAME];
            String[] toAddresses = new String[]{employeeUpdateToken.Employee__r.Email__c};
            mail.setToAddresses(toAddresses);
            mail.setTemplateId(template.id);
            mail.setTargetObjectId(template.ownerid);
            mail.setWhatId(employeeUpdateToken.id);
            mail.setSaveAsActivity(false); 
            System.debug('mail' +mail);
            Messaging.SendEmailResult [] r;
            try{
               r  =Messaging.sendEmail(new Messaging.SingleEmailMessage [] {mail}); 
               	
                //resetFlag = true;
                //msgInfo = System.Label.Success_Message; 
            }catch(Exception e){ 
            	System.debug('SendEmailResult: ' + r);
            } 
    }*/
    
    public static void sendResponseEmail(Employee_Certification__c employeeUpdateToken , String subject, String content) {
        
         Messaging.SingleEmailMessage mail = new Messaging.SingleEmailMessage(); 
            System.debug('employeeUpdateToken employee name' + employeeUpdateToken.Employee__r.Name );            
           
            String[] toAddresses = new String[]{employeeUpdateToken.Employee__r.Email__c};
            mail.setToAddresses(toAddresses);            
            mail.setSubject(subject);
            mail.setHtmlBody(content);                
            
            System.debug('mail' +mail);
            Messaging.SendEmailResult [] r;
            try{
               r  =Messaging.sendEmail(new Messaging.SingleEmailMessage [] {mail});               
            }catch(Exception e){ 
            	System.debug('SendEmailResult: ' + r);
            } 
    }
}