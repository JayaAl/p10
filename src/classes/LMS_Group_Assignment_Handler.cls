public class LMS_Group_Assignment_Handler 
{
    public static boolean isRun=false;
    public static Map<id,List<LMS_Public_Group_Mapping_criteria__c>> populateInclusionMap(List<LMS_Public_Group_Mapping__c> All_LMS_Groups)
    {
        Map<id,List<LMS_Public_Group_Mapping_criteria__c>> inclusionMap = new  Map<id,List<LMS_Public_Group_Mapping_criteria__c>>();
        list<LMS_Public_Group_Mapping_criteria__c> lstCriteria;
        for(LMS_Public_Group_Mapping__c pg: All_LMS_Groups)
        {
            if(pg.LMS_Public_Group_Mapping_Criteria__r.size()==0)
            {
                continue;
            }
            lstCriteria = new List<LMS_Public_Group_Mapping_criteria__c>();
            for(LMS_Public_Group_Mapping_Criteria__c criteria : pg.LMS_Public_Group_Mapping_Criteria__r)
            {
                if(!string.isEmpty(criteria.Inclusion_Criteria__c))
                {
                    lstCriteria.Add(criteria);
                }
            }
            if(lstCriteria.size()>0)
                inclusionMap.put(pg.Id,lstCriteria);
            
        }
        return inclusionMap;
    }
   
    public  static void assignPublicGroup(Map<Id, User> mapOldUsers, List<user> newUsers)
    {
        /*
         * 
         * Process 
         * 1. Pull the Mappings.
         * 2. Assignment Logic.
         *      Query all the mappings.
         *      Loop all the  Users in newUsers List.
         *          Compare User values with all the Mappings & add the Group name to the Map<UserId, List<GroupName>>
         *      End of Loop
         *      Query the existing GroupMember with User Ids & Group Name. 
         *          Create a List of GroupsMember already 
         *      Initialize a List<GroupMember> to create new groups. 
         *      Loop through all the Users and create the groupmember if,
         *          GroupName is matched with th user details & No existing Group member found with that User.
         *      End the Loop
         *      insert the new GroupMembers.
         * 
        */
          List<LMS_Public_Group_Mapping__c> All_LMS_Groups= [select id,Name, ManagerId__c,(select id,field_Name__c,inclusion_criteria__c,exclusion_criteria__c from LMS_Public_Group_Mapping_criteria__r), Is_Manager__c, function_group__c,Group_Name__c,Location__c,Cost_Center__c,Title__c,Exclusion_Criteria__C,Inclusion_Criteria__c  from LMS_Public_Group_Mapping__c limit 49999];//LMS_Public_Group_Mapping__c.getAll().values();
        Map<id,List<LMS_Public_Group_Mapping_criteria__c>> inclusionMap = new  Map<id,List<LMS_Public_Group_Mapping_criteria__c>>();
        inclusionMap = populateInclusionMap(All_LMS_Groups);
        Map<Id, Set<String>> newuserToMappedGroup = new Map<Id, Set<String>>();
        Map<Id, Set<String>> olduserToMappedGroup = new Map<Id, Set<String>>();
        Set<String> allGroupNameMapped= new Set<string>();
        Set<String> allNewOrUpdatedUserIds = new Set<string>();
        Set<String> mappedGroups=new Set<String>();
        try  {
        
            for(User curUser : newUsers)
              {
               /*
                 * Assignment logic should be applied only for 
                 * Standard Profile & either New or updated User.
                 * Rest all Users will just continue without undergoing any logic.
                 *   
               */
                boolean isNewOrUpdated=false;
                if(mapOldUsers!=null)  {
                    isNewOrUpdated = (curUser.Is_Manager__c !=mapOldUsers.get(curUser.id).Is_Manager__c  || mapOldUsers.get(curUser.id).function_group__c   != curUser.function_group__c  || mapOldUsers.get(curUser.id).Cost_Center__c   != curUser.Cost_Center__c  || mapOldUsers.get(curUser.id).WD_Location__c   != curUser.WD_Location__c || (mapOldUsers.get(curUser.id).Manager ==null && curUser.Manager !=null) || ((mapOldUsers.get(curUser.id).Manager   != curUser.Manager))  || mapOldUsers.get(curUser.id).Title  != curUser.Title);
                }  else {
                    isNewOrUpdated = true;
                }
                
                  if( !(curUser.UserType=='Standard' && isNewOrUpdated) ) {
                    continue;
                }
                
                /*
                 * If control is here , which mean either User is newly created Or either of the User control fields are updated.
                 *
                 /
                /*Start : Looping all the Mapping to find the exact map*/
                for(LMS_Public_Group_Mapping__c mapping :All_LMS_Groups)
                {
                     if(compareUserGroupMapping(curUser,mapping,inclusionMap))
                   {
                       /*
                        * If Matched,
                        *   Add the group Name to the  newuserToMappedGroup map.Each User can have more than one Public Group.
                        *   And also add the group Name to allGroupNameMapped, this will be used to get the id of each group Name.
                        */
                       
                       if(!newuserToMappedGroup.containsKey(curUser.Id)) {
                           mappedGroups = new Set<String>();
                       }
                       mappedGroups.add(mapping.Group_Name__c);
                       newuserToMappedGroup.put(curUser.Id,mappedGroups);
                       allGroupNameMapped.add(mapping.Group_Name__c);
                       allNewOrUpdatedUserIds.add(curUser.Id);
                   }
                }
                /*END : Looping of Mapping */
                
            }
            /*END : User Loop*/
            if(allNewOrUpdatedUserIds.size()>0 && allGroupNameMapped.Size()>0) {   
                /*Query the Ids with Public Group*/
                Map<String, String> groupNameToIdMap= new Map<String, string>();
                for(Group curGroup : [Select Id, Name From Group where Type='Regular' and Name =:allGroupNameMapped])   {
                    groupNameToIdMap.put(curGroup.Name, curGroup.Id);
                }
                Map<id,GroupMember> groupMemberToDelete = new Map<id,GroupMember>();
                for( GroupMember curGM :  [Select Id, groupId, group.Name, UserOrGroupId From GroupMember where  UserOrGroupId in :(allNewOrUpdatedUserIds)   AND group.Name LIKE 'LMS%'])
                {
                  groupMemberToDelete.put(curGM.Id,curGM) ;             
                }
                
                if(groupMemberToDelete.values().size()>0) {
                    try {
                        delete groupMemberToDelete.values();
                    }  catch(DmlException ex1) {
                        system.debug(logginglevel.info, 'THIS IS dml exception exception ' + ex1 + '---' + ex1.getstacktracestring());
                    }
                      
                }
                List<GroupMember> groupMemberToCreate = new List<GroupMember>();
                for(string userId :  allNewOrUpdatedUserIds){
                    /*Idenitfy What needs to create */
                    Set<String> newallMappedGroupNames=newuserToMappedGroup.get(userId);
                    for(String curGroupName :newallMappedGroupNames) {
                        /*Check if this user is already a member of mapped Group Name*/
                        if(groupNameToIdMap.containsKey(curGroupName) && groupNameToIdMap.get(curGroupName)!=null ) {
                            groupMemberToCreate.add(new GroupMember(UserOrGroupId=userId,GroupId=groupNameToIdMap.get(curGroupName)));
                        }
                    }
                }
                
                if(groupMemberToCreate.size()>0) {
                    upsert groupMemberToCreate;
                }
                
            }/*END of Check if atleast one User to consider.*/
        
        }  catch(exception ex){
            system.debug(logginglevel.info, 'EXCEPTION HERE' + ex + '---' + ex.getstacktracestring());
        }
    }
    
    public static boolean compareUserGroupMapping(User user, LMS_Public_Group_Mapping__c mapping, Map<id,List<LMS_Public_Group_Mapping_criteria__c>> inclusionMap)
    {
        /*String Constant to compare the static values as case in sensitive*/
        String   ALL='All';
        String ANY_STR = 'Any';
        String   TRUE_STR='True';
        String   FALSE_STR='False';
        boolean isValidMapping= false;
        try{
            if(( ALL.equalsIgnoreCase(mapping.Is_Manager__c) || (user.Is_Manager__c==True && TRUE_STR.equalsIgnoreCase(mapping.Is_Manager__c) ) || (user.Is_Manager__c==False && FALSE_STR.equalsIgnoreCase(mapping.Is_Manager__c) ) )
               
               && (mapping.function_group__c   == user.function_group__c  || ALL.equalsIgnoreCase(mapping.function_group__c )  ) 
               && (mapping.Cost_Center__c == user.Cost_Center__c   || ALL.equalsIgnoreCase(mapping.Cost_Center__c) || ANY_STR.equalsIgnoreCase(mapping.Cost_Center__c)) 
               && (mapping.Location__c    == user.WD_Location__c  || ALL.equalsIgnoreCase(mapping.Location__c)) 
               && ((user.Manager!=null && (mapping.ManagerId__c == user.Manager.Id))   || ((mapping.ManagerId__c==null)) )  
               && (mapping.Title__c== user.Title || ALL.equalsIgnoreCase(mapping.Title__c))    )
            {   
              
                isValidMapping = true;
                for(LMS_Public_Group_Mapping_Criteria__c mc: mapping.LMS_Public_Group_Mapping_Criteria__r)
                {
                    if(mc.Field_Name__c=='Title')
                    {

                        if((mc.Exclusion_Criteria__c!='') && (!string.isEmpty(user.Title)) && mc.Exclusion_Criteria__c.contains(user.Title))  {
                            system.debug('INSIDE GROUP MAPPING');
                            isValidMapping= false; break;
                        } else {
                            isValidMapping= true;
                        }
                        
                    }
                    if(mc.Field_Name__c=='Cost_Center__c')   {
                        if((mc.Exclusion_Criteria__c!='') && (!string.isEmpty(user.Cost_Center__c))  && mc.Exclusion_Criteria__c.contains(user.Cost_Center__c))
                        {
                            isValidMapping= false;  break;
                        }else {
                            isValidMapping= true;
                        }
                        
                    }
                    if(mc.Field_Name__c=='function_group__c')
                    {
                        if((mc.Exclusion_Criteria__c!='') && (!string.isEmpty(user.function_group__c))  && mc.Exclusion_Criteria__c.contains(user.function_group__c))
                        {
                            isValidMapping= false;  break;
                        } else {
                            isValidMapping= true;
                        }
                        
                    }
                    if(mc.Field_Name__c=='WD_Location__c')
                    {
                        if((mc.Exclusion_Criteria__c!='') && (!string.isEmpty(user.WD_Location__c))  && mc.Exclusion_Criteria__c.contains(user.WD_Location__c))
                        {
                            isValidMapping= false;   break;
                        } else{
                            isValidMapping= true;
                        }
                        
                    }
                }
                
            }
            else
            {
                if(inclusionMap.containsKey(mapping.Id))
                {

                    for(LMS_Public_Group_Mapping_Criteria__c mc: inclusionMap.get(mapping.Id))
                    {

                        if(( ALL.equalsIgnoreCase(mapping.Is_Manager__c)  || (user.Is_Manager__c==True && TRUE_STR.equalsIgnoreCase(mapping.Is_Manager__c) )|| (user.Is_Manager__c==False && FALSE_STR.equalsIgnoreCase(mapping.Is_Manager__c) ) )
                               && (mapping.function_group__c   == user.function_group__c  || ALL.equalsIgnoreCase(mapping.function_group__c )   )   && (mapping.Cost_Center__c == user.Cost_Center__c   || ALL.equalsIgnoreCase(mapping.Cost_Center__c)  || ANY_STR.equalsIgnoreCase(mapping.Cost_Center__c))
                           && (mapping.Location__c    == user.WD_Location__c  || ALL.equalsIgnoreCase(mapping.Location__c))  && ((user.Manager!=null && (mapping.ManagerId__c == user.Manager.Id))  || (mapping.ManagerId__c==null) )  
                           && (!(ALL.equalsIgnoreCase(mapping.Title__c))) )
                        {
                        
                            if(mc.Field_Name__c=='Title' && (mc.Inclusion_Criteria__c!='') && (!string.isEmpty(user.Title))  && mc.Inclusion_Criteria__c.contains(user.Title))
                            {
                                isValidMapping= true;    break;
                            }
                            
                        }
                        if(( ALL.equalsIgnoreCase(mapping.Is_Manager__c) || (user.Is_Manager__c==True && TRUE_STR.equalsIgnoreCase(mapping.Is_Manager__c) ) || (user.Is_Manager__c==False && FALSE_STR.equalsIgnoreCase(mapping.Is_Manager__c) ) ) && (mapping.function_group__c   == user.function_group__c || ALL.equalsIgnoreCase(mapping.function_group__c ) ) && (mapping.Location__c    == user.WD_Location__c || ALL.equalsIgnoreCase(mapping.Location__c)) && ( (user.Manager!=null && (mapping.ManagerId__c == user.Manager.Id)) || (mapping.ManagerId__c==null) )  && (mapping.Title__c== user.Title || ALL.equalsIgnoreCase(mapping.Title__c))  )
                        {
                            if(mc.Field_Name__c=='Cost_Center__c' && (mc.Inclusion_Criteria__c!='') && (!string.isEmpty(user.Cost_Center__c)) && mc.Inclusion_Criteria__c.contains(user.Cost_Center__c))
                            {
                                isValidMapping= true;    break;
                            }
                        }
                        if(( ALL.equalsIgnoreCase(mapping.Is_Manager__c) || (user.Is_Manager__c==True && TRUE_STR.equalsIgnoreCase(mapping.Is_Manager__c) ) || (user.Is_Manager__c==False && FALSE_STR.equalsIgnoreCase(mapping.Is_Manager__c) ) ) && (mapping.Cost_Center__c == user.Cost_Center__c || ALL.equalsIgnoreCase(mapping.Cost_Center__c)|| ANY_STR.equalsIgnoreCase(mapping.Cost_Center__c)) && (mapping.Location__c    == user.WD_Location__c || ALL.equalsIgnoreCase(mapping.Location__c)) && ( (user.Manager!=null && (mapping.ManagerId__c == user.Manager.Id)) || (mapping.ManagerId__c==null) )  && (mapping.Title__c== user.Title || ALL.equalsIgnoreCase(mapping.Title__c))  )
                        {
                            if(mc.Field_Name__c=='function_group__c' && (mc.Inclusion_Criteria__c!='') && (!string.isEmpty(user.function_group__c)) && mc.Inclusion_Criteria__c.contains(user.function_group__c))
                            {
                                isValidMapping= true;     break;
                            }
                        }
                        if(( ALL.equalsIgnoreCase(mapping.Is_Manager__c) || (user.Is_Manager__c==True && TRUE_STR.equalsIgnoreCase(mapping.Is_Manager__c) ) || (user.Is_Manager__c==False && FALSE_STR.equalsIgnoreCase(mapping.Is_Manager__c) ) ) && (mapping.Cost_Center__c == user.Cost_Center__c || ALL.equalsIgnoreCase(mapping.Cost_Center__c)|| ANY_STR.equalsIgnoreCase(mapping.Cost_Center__c))  && (mapping.function_group__c   == user.function_group__c || ALL.equalsIgnoreCase(mapping.function_group__c ) ) && (  (user.Manager!=null && (mapping.ManagerId__c == user.Manager.Id)) || mapping.ManagerId__c==null)  && (mapping.Title__c== user.Title || ALL.equalsIgnoreCase(mapping.Title__c))  )
                        {
                            if(mc.Field_Name__c=='WD_Location__c' && (mc.Inclusion_Criteria__c!='') && (!string.isEmpty(user.WD_Location__c)) && mc.Inclusion_Criteria__c.contains(user.WD_Location__c))
                            {
                                isValidMapping= true;   break;
                            }
                        }
                    }
                
                }
                
            }
        }    
        catch(exception ex)
        {
            system.debug(logginglevel.info,'This is exception' + ex + '  ' + ex.getStackTraceString());
            system.debug('msg'+ex.getMessage()+ex.getLineNumber());
            
        }
        system.debug(logginglevel.info,'This isValidMapping' + user.Id + ' -- ' +  isValidMapping);
        return isValidMapping;
    }
   
}