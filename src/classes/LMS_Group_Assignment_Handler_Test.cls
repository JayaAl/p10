/**********LMS P10******************/
@isTest
public class LMS_Group_Assignment_Handler_Test {

    public static testmethod void test1()
    {
      LMS_Group_Assignment_Handler handler = new LMS_Group_Assignment_Handler();
        LMS_Public_Group_Mapping__c mapping= new LMS_Public_Group_Mapping__c();
        mapping.Is_Manager__c='All';
        mapping.function_group__c='test function';
        mapping.Cost_Center__c ='test cost center';
        mapping.Location__c ='test Location';
        mapping.Manager__c ='All';
        mapping.Title__c ='title';
        mapping.group_name__c='Test Group';
        //mapping.Name='mapping test';
        insert mapping;
        
        LMS_Public_Group_Mapping__c mapping2= new LMS_Public_Group_Mapping__c();
        mapping2.Is_Manager__c='All';
        mapping2.function_group__c='test function 3';
        mapping2.Cost_Center__c ='test cost center';
        mapping2.Location__c ='test Location';
        mapping2.Manager__c ='All';
        mapping2.Title__c ='title';
        mapping2.group_name__c='Test Group2';
        //mapping2.Name='mapping test2';
        insert mapping2;
        
        Group testGroup = new Group();
        testGroup.Name='Test Group';
        testGroup.Type='Regular';
        insert testGroup;
        
         Group testGroup2 = new Group();
        testGroup2.Name='Test Group2';
        testGroup2.Type='Regular';
        insert testGroup2;         
        List<Profile> profiles = [Select Id from Profile Where Name='Standard User'];
        
        User insertTest = new User(EmailEncodingKey='UTF-8', LastName='Testing', LanguageLocaleKey='en_US',LocaleSidKey='en_US',TimeZoneSidKey='America/Los_Angeles');
        insertTest.function_group__c='test function';
        insertTest.Cost_Center__c='test cost center2';
        insertTest.WD_Location__c='test Location';
        insertTest.Title='title';
        insertTest.Is_Manager__c=True;
        insertTest.ProfileId=profiles[0].id;
        insertTest.Email='testemail@testemail.com';
        insertTest.Username='testemail@testemail.com.pandora';
        insertTest.Alias='test';
        
        
        insert  insertTest;
        List<user> userList = new List<User>();
        userList.add(insertTest);
        
        system.runAs(insertTest)
        {
            User updUser1 = [select id,Title from User where id=:insertTest.Id];
            updUser1.Cost_Center__c='test cost center';
            update updUser1;
            User updUser2 = [select id,Title from User where id=:insertTest.Id];
            updUser2.function_group__c='test function 3';
            update updUser2;
            
        }
        LMS_Group_Assignment_Handler.assignPublicGroup(null,userList);
        
   }
   
   
   public static testmethod void test2()
    {
        LMS_Public_Group_Mapping__c mapping= new LMS_Public_Group_Mapping__c();
        mapping.Is_Manager__c='All';
        mapping.function_group__c='test function';
        mapping.Cost_Center__c ='test cost center';
        mapping.Location__c ='test Location';
        mapping.Manager__c ='All';
        mapping.Title__c ='title';
        mapping.group_name__c='Test Group';
        //mapping.Name='mapping test';
        insert mapping;
        
        LMS_Public_Group_Mapping_criteria__c lc = new LMS_Public_Group_Mapping_criteria__c();
        lc.field_Name__c='Title';
        lc.exclusion_criteria__c='Director';
        lc.LMS_Public_Group_Mapping__c = mapping.id;
        insert lc;
        
        
        LMS_Public_Group_Mapping__c mapping2= new LMS_Public_Group_Mapping__c();
        mapping2.Is_Manager__c='All';
        mapping2.function_group__c='test function 3';
        mapping2.Cost_Center__c ='test cost center';
        mapping2.Location__c ='test Location';
        mapping2.Manager__c ='All';
        mapping2.Title__c ='VP';
        mapping2.group_name__c='Test Group2';
        //mapping2.Name='mapping test2';
        insert mapping2;
        
        LMS_Public_Group_Mapping_criteria__c lc1 = new LMS_Public_Group_Mapping_criteria__c();
        lc1.field_Name__c='Title';
        lc1.Inclusion_Criteria__c='Director';
        lc1.LMS_Public_Group_Mapping__c = mapping2.id;
        insert lc1;
        
        Group testGroup = new Group();
        testGroup.Name='Test Group';
        testGroup.Type='Regular';
        insert testGroup;
        
         Group testGroup2 = new Group();
        testGroup2.Name='Test Group2';
        testGroup2.Type='Regular';
        insert testGroup2;
        List<Profile> profiles = [Select Id from Profile Where Name='System Administrator'];
        User insertTest1 = new User(EmailEncodingKey='UTF-8', LastName='Testing', LanguageLocaleKey='en_US',LocaleSidKey='en_US',TimeZoneSidKey='America/Los_Angeles');
        insertTest1.function_group__c='test function';
        insertTest1.Cost_Center__c='test cost center2';
        insertTest1.WD_Location__c='test Location';
        insertTest1.Title='title';
        insertTest1.Is_Manager__c=True;
        insertTest1.ProfileId=profiles[0].id;
        insertTest1.Email='testemail1@testemail.com';
        insertTest1.Username='testemail1@testemail.com.pandora';
        insertTest1.Alias='test';
        
        
        insert  insertTest1;
        
        system.runAs(insertTest1)
        {
            User updUser = [select id,Title from User where id=:insertTest1.Id];
            updUser.Title = 'Director';
            update updUser;
        }        
   }
   

   private static TestMethod void TestLMSGroupAssignmentHandlerExCriteria() 
   {                                                              
        LMS_Public_Group_Mapping__c mapping1 =getLMSPublicGroupMapping('All',//String Is_Manager,
                                                                       'test function',//String function_group
                                                                       'test cost center',// String Cost_Center
                                                                       'test Location',//String Location
                                                                       'All',//String Manager
                                                                       'title',//String Title
                                                                       'Test Group'//String group_name
                                                                       );
        insert mapping1;

        LMS_Public_Group_Mapping_criteria__c mappingCriteria1;

        mappingCriteria1= getLMS_Public_Group_Mapping_criteria('Title',//fieldname
                                                                //'Director',//criteria,
                                                                //inc-criteria,
                                                                mapping1.id
                                                                );
        mappingCriteria1.exclusion_criteria__c='Director';
        insert mappingCriteria1;

        List<Profile> profiles = [Select Id from Profile Where Name='Standard User'];
        system.assert(profiles.size()>0, 'Profile Not Found.');
        Id ProfileId=Profiles[0].Id;
        
        Group group1= getGroup('Test Group');
        insert group1;
        
        User U=getUser(profileId,//id ProfileId, 
                       'testemail1@testemail.com.pandora',//String UserName,
                       //'test function',//String function_group,
                       //'test cost center',//String Cost_Center,
                       //'test Location',//String WD_Location__c,
                       //'title',//String Title,
                       //True,//String Is_Manager,
                       'testemail1@testemail.com',//String Email,
                       'test', //String Alias
                       mapping1);

        insert U;

   }
   
   private static testMethod void TestLMSGroupAssignmentHandlerExCriteria1()
   {
    LMS_Public_Group_Mapping__c mapping1 =getLMSPublicGroupMapping('All',//String Is_Manager,
                                                                       'test function',//String function_group
                                                                       'test cost center',// String Cost_Center
                                                                       'test Location',//String Location
                                                                       'All',//String Manager
                                                                       'title',//String Title
                                                                       'Test Group'//String group_name
                                                                       );
        insert mapping1;

        LMS_Public_Group_Mapping_criteria__c mappingCriteria1;

        mappingCriteria1= getLMS_Public_Group_Mapping_criteria('Cost Center',//fieldname
                                                                //'Director',//criteria,
                                                                //inc-criteria,
                                                                mapping1.id
                                                                );
        mappingCriteria1.exclusion_criteria__c='Director';
        insert mappingCriteria1;

        List<Profile> profiles = [Select Id from Profile Where Name='System Administrator'];
        system.assert(profiles.size()>0, 'Profile Not Found.');
        Id ProfileId=Profiles[0].Id;

        Group group1= getGroup('Test Group');
        insert group1;

        User U=getUser(profileId,//id ProfileId, 
                       'testemail1@testemail.com.pandora',//String UserName,
                       //'test function',//String function_group,
                       //'test cost center',//String Cost_Center,
                       //'test Location',//String WD_Location__c,
                       //'title',//String Title,
                       //True,//String Is_Manager,
                       'testemail1@testemail.com',//String Email,
                       'test', //String Alias
                       mapping1);

        insert U;

        Update u;
    
   }
   
   private static testMethod void TestLMSGroupAssignmentHandlerInCriteria()
   {
     LMS_Public_Group_Mapping__c mapping1 =getLMSPublicGroupMapping('All',//String Is_Manager,
                                                                       'test function',//String function_group
                                                                       'test cost center',// String Cost_Center
                                                                       'test Location',//String Location
                                                                       'All',//String Manager
                                                                       'title',//String Title
                                                                       'Test Group'//String group_name
                                                                       );
        insert mapping1;

        LMS_Public_Group_Mapping_criteria__c mappingCriteria1;

        mappingCriteria1= getLMS_Public_Group_Mapping_criteria('Title',//fieldname
                                                                //'Director',//criteria,
                                                                //inc-criteria,
                                                                mapping1.id
                                                                );
        mappingCriteria1.Inclusion_Criteria__c='Director';
        insert mappingCriteria1;

        List<Profile> profiles = [Select Id from Profile Where Name='System Administrator'];
        system.assert(profiles.size()>0, 'Profile Not Found.');
        Id ProfileId=Profiles[0].Id;
        
        Group group1= getGroup('Test Group');
        insert group1;

        User U=getUser(profileId,//id ProfileId, 
                       'testemail1@testemail.com.pandora',//String UserName,
                       //'test function',//String function_group,
                       //'test cost center',//String Cost_Center,
                       //'test Location',//String WD_Location__c,
                       //'title',//String Title,
                       //True,//String Is_Manager,
                       'testemail1@testemail.com',//String Email,
                       'test', //String Alias
                       mapping1);
         
        insert U;

   }
   

   public static LMS_Public_Group_Mapping__c getLMSPublicGroupMapping(String Is_Manager,
                                                                      String function_group,
                                                                      String Cost_Center,
                                                                      String Location,
                                                                      String Manager,
                                                                      String Title,
                                                                      String group_name
                                                                      )
   {
     LMS_Public_Group_Mapping__c mapping= new LMS_Public_Group_Mapping__c();

     mapping.Is_Manager__c=Is_Manager;
     mapping.function_group__c=function_group;
     mapping.Cost_Center__c=Cost_Center;
     mapping.Location__c=Location;
     mapping.Manager__c=Manager;
     mapping.Title__c=Title;
     mapping.group_name__c=group_name;

     return mapping;
   }

   public static LMS_Public_Group_Mapping_criteria__c getLMS_Public_Group_Mapping_criteria(String field_Name,
                                                           //String criteria, 
                                                           //String Inclusion_Criteria,
                                                           Id LMS_Public_Group_Mapping
                                                           )
   {
    LMS_Public_Group_Mapping_criteria__c mappingCriteria= new LMS_Public_Group_Mapping_criteria__c();
        mappingCriteria.field_Name__c=field_Name;//'Title';
        mappingCriteria.exclusion_criteria__c='';//criteria;//='Director';
        mappingCriteria.Inclusion_criteria__c='';
        mappingCriteria.LMS_Public_Group_Mapping__c = LMS_Public_Group_Mapping;

        return  mappingCriteria;

   }

   public static User getUser(id ProfileId, 
                              String UserName,
                              //String function_group,
                              //String Cost_Center,
                              //String WD_Location,
                              //String Title,
                              //boolean Is_Manager,
                              String Email,
                              String Alias,
                              LMS_Public_Group_Mapping__c mapping)
   {
      User usr = new User(EmailEncodingKey='UTF-8', LastName='Testing', LanguageLocaleKey='en_US',LocaleSidKey='en_US',TimeZoneSidKey='America/Los_Angeles');
      usr.ProfileId=ProfileId;
      usr.Email='testemail@testemail.com';
      usr.Username=UserName;
      usr.Alias='test';


        usr.function_group__c=mapping.function_group__c;//'test function'; 
        usr.Cost_Center__c=mapping.Cost_Center__c;//'test cost center2';
        usr.WD_Location__c=mapping.Location__c;//'test Location';
        usr.Title=mapping.Title__c;//'title';
        usr.Is_Manager__c=boolean.valueOf(mapping.Is_Manager__c);  
        
        
        return usr;
         

   }

   public static group getGroup(String name)
   {
        Group g = new Group();
        g.Name=name;//'Test Group';
        g.Type='Regular';
        return g;
        
   }
    
}