/*
Name : LOGO_TestUpdateIOSignatureStatus
Author : ClearTask
Date : Pct 7, 2011
Usage : Test Class for trigger on Case with Record Type = Logo Permission Request 
        to update the IO Signature Status field with the 
        same value in the IO Signature Status field on the Case with 
        record type = IO Approval Details that is also on the same related 
        Opportunity.
*/
@isTest
private class LOGO_TestUpdateIOSignatureStatus{

   /* Test Variables */
    
    static Account testAccount1;
    static Account testAccount2;
    static Case testCase;
    Static Contact conObj;
    Static Opportunity testOpportunity;
    
    /* Before Test Actions */
    
    // Create two accounts and generate a case with a related 
    // opportunity and a record type matching the trigger filters
    //updated by VG 1/29/2013
    static {
        testAccount1 = UTIL_TestUtil.generateAccount();
        testAccount2 = UTIL_TestUtil.generateAccount();
        insert new Account[] { testAccount1, testAccount2 };
        testOpportunity = UTIL_TestUtil.generateOpportunity(testAccount1.id);
        testOpportunity.Name = 'Test Opp';
        testOpportunity.StageName = 'Qualified';
        testOpportunity.CloseDate = System.today();
        insert testOpportunity;
        
              //updated by VG 1/29/2013
        //Create Test Contact.
        
          //updated by VG 1/29/2013
          conObj = UTIL_TestUtil.generateContact(testAccount1.id);
          
            //Contact c = new Contact();// specify all the required fields
        conObj.firstname = 'MyFristName';
        conObj.lastname = 'MyLastName';
        conObj.Title = 'MyTitle';
        conObj.Email = 'myemail@mydomain.com';
        conObj.MailingCity = 'Fremont';
        conObj.MailingState = 'CA';
        conObj.MailingCountry = 'USA';
        conObj.MailingStreet = 'MyStreet';
       insert conObj;
       
       }




    static testMethod void testSignatureStatus(){
        
        /* variable for IO Approval Details record type */
        String IO_APPROVAL_CASE = 'IO Approval Details';
        
        /* variable for Logo Permission Request record type */
        String LOGO_PERMISSION_REQUEST = 'Logo Permission Request';
    
        /* function to get the ids of Case record types -- Start */
    
        Schema.DescribeSObjectResult c = Schema.SObjectType.Case;
        Map<String,Schema.RecordTypeInfo> rtMapByName = c.getRecordTypeInfosByName();
        
        Schema.RecordTypeInfo ioApprovalCaseRT = rtMapByName.get(IO_APPROVAL_CASE);
        String ioApprovalCaseRTId = ioApprovalCaseRT.getRecordTypeId();
        
        Schema.RecordTypeInfo logoPermissionRT = rtMapByName.get(LOGO_PERMISSION_REQUEST);
        String logoPermissionRTId = logoPermissionRT.getRecordTypeId();
        
        /* function to get the ids of Case record types -- End */
        
            //updated by VG 1/29/2013
        /* create opportunity */
       // Opportunity oppRecord = new Opportunity(
         //   Name = 'Test Opp',
        //    StageName = 'Qualified',
        //    CloseDate = System.today()
       // );
      //insert oppRecord;
        
        /* create case related to opportunity and record type IO Approval Details */
        Case caseRecord1 = new Case(
            Opportunity__c = testOpportunity.id,     //updated by VG 1/29/2013
            Account = testAccount1,
            RecordTypeId = ioApprovalCaseRTId,
            Billing_Type__c = 'Advertiser',
            IO_Signature_Status__c = 'Test'
        );
        insert caseRecord1;
        
        /* create case related to opportunity and record type Logo Permission */
        Case caseRecord2 = new Case(
            Opportunity__c = testOpportunity.id,     //updated by VG 1/29/2013
             Account = testAccount1,
             Billing_Type__c = 'Advertiser',
            RecordTypeId = logoPermissionRTId
        );
        insert caseRecord2;
        
        Case caseRecord = [select IO_Signature_Status__c from Case where id = :caseRecord2.id];
        //System.assertEquals(caseRecord1.IO_Signature_Status__c, caseRecord.IO_Signature_Status__c);
        
    }
    
}