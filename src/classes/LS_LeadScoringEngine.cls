public class  LS_LeadScoringEngine {
    public static Boolean evaluateLeadsFlag = FALSE;
    /*
    //If you are using code to insert Leads's multiple times synchronously in the same class, only the first
    //batch will be scored unless you set this variable to false in between inserts
    //This method exists primarily for my negative test cases to run properly
    public static void setEvaluateLeadsFlag(Boolean alreadyCalled){
        evaluateLeadsFlag = alreadyCalled;
    }
    
    public static boolean hasEvaluateLeadsExecuted(){
        return evaluateLeadsFlag;
    }
    
    
    public static List<Lead> evaluateLeads(List<Lead> triggerLeads){
    
        Integer i,j;
        Double scoreChange;//safer to define as a double in case anyone tries to change the field's # decimals
        Schema.DescribeFieldResult cmFieldDescribe;
        Map<Id,Double> leadOldScores=new Map<Id,Double>();
        Map<Id,Double> leadNewScores=new Map<Id,Double>();
        Map<Id,Double> leadScores = new Map<Id,Double>();
        String qrySOQL='';
        Integer LeadCount=0;        
        Lead_Score__c[] Rules=activeLeadScoringRuleDetails();
        //only do work if there are active rules
        if (Rules.size()>0){
            //These maps house the field list as well as field types of the fields of Lead
            Map<String, Schema.SObjectField> leadMap = Schema.SObjectType.Lead.fields.getMap();
            Map<String,DisplayType> leadTypeMap = new Map<String,DisplayType>(); //this will hold the field type for all the fields in the LeadScoringRule__c
            
            leadTypeMap=getRuleFieldInfo(leadMap, Rules);//check back here
            List<String> fieldNames=new List<String>();//could change to set to not require "contains key" check; check if sets back on dupes
            Lead_Score__c[] rulesCopy=new List<Lead_Score__c>();
            for (i=0;i<Rules.size();i++){
                if(leadTypeMap.containsKey(Rules[i].Field_Name__c)==True){
                    fieldNames.add(Rules[i].Field_Name__c);
                    //this effectively removes bad fieldname rules from the list 
                    rulesCopy.add(Rules[i]);
                }//if 
            }//for 2
            Rules=rulesCopy;
            For (i=0;i<triggerLeads.size();i++){
            //Evaluate criteria here for each lead
                triggerLeads[i].Lead_Score__c=0;
                triggerLeads[i].Score_Summary__c='';
                triggerLeads[i].Scoring_In_Progress__c = false;
                For(Lead_Score__c currentRule:Rules){
                    if (evaluateCriteria(currentRule,
                                         String.ValueOf(triggerLeads.get(i).get(currentRule.Field_Name__c)),
                                         leadTypeMap.get(currentRule.Field_Name__c))){
                         triggerLeads[i].Score_Summary__c += currentRule.Field_Name__c +'=' + String.ValueOf(triggerLeads.get(i).get(currentRule.Field_Name__c)) + ', Score=' + currentRule.Score__c + '<br/>';
                         triggerLeads[i].Lead_Score__c+=currentRule.Score__c;
                    }//if 4
                }//for 3
            }//for 2
            
            if(system.isBatch()) {
                try{
                    evaluateLeadsFlag = FALSE;
                    Database.update(triggerLeads,false); 
                    system.debug('In batch if');                    
                } catch (Exception e){
                    system.debug('Leads were not updated: '+e);
                }//catch
            } else {
                try{
                    evaluateLeadsFlag = TRUE;
                    Database.update(triggerLeads,false);  
                    system.debug('In future else'); 
                } catch (Exception e){
                    system.debug('Leads were not updated: '+e);
                }//catch
                
            }
            evaluateLeadsFlag = TRUE;
        }//if 1
        system.debug('*****' + triggerLeads);
        return triggerLeads;
    }
    
    public static Lead_Score__c[] activeLeadScoringRuleDetails(){
        //Once all the active rules criteria are saved, return them as a list to wherever they were called from        
        return [Select Value__c, Score__c, Field_Name__c, Data_Type__c, Operator__c, Start_Date__c, End_Date__c FROM Lead_Score__c WHERE IsActive__c=TRUE Order By Order__c];
    }
    
    public static Boolean evaluateCriteria(Lead_Score__c currentRule, String recordFieldValue, DisplayType ruleFieldType){
                  
        Boolean metCriteria=false;
        String ruleFieldValue = currentRule.Value__c; 
        String operator = currentRule.Operator__c; 
        
            if (operator=='equals'){//if 1
                if (recordFieldValue!=null&&(ruleFieldType==Schema.DisplayType.STRING || ruleFieldType==Schema.DisplayType.ComboBox ||ruleFieldType==Schema.DisplayType.Picklist ||ruleFieldType==Schema.DisplayType.MultiPicklist ||ruleFieldType==Schema.DisplayType.email||ruleFieldType==Schema.DisplayType.encryptedString||ruleFieldType==Schema.DisplayType.Phone||ruleFieldType==Schema.DisplayType.url)){
                        metCriteria=(ruleFieldValue.equalsIgnoreCase(recordFieldValue));
                        system.debug('******' + metCriteria);
                } else if (ruleFieldType==Schema.DisplayType.BOOLEAN){//if 2
                        metCriteria=(recordFieldValue.toLowerCase()==ruleFieldValue.toLowerCase());
                } else if (recordFieldValue!=null&&(ruleFieldType==Schema.DisplayType.Double||ruleFieldType==Schema.DisplayType.Currency||ruleFieldType==Schema.DisplayType.Percent ||ruleFieldType==Schema.DisplayType.Integer)){
                        metCriteria=(decimal.valueOf(recordFieldValue)==decimal.valueOf(ruleFieldValue));
                } else if (recordFieldValue!=null&&(ruleFieldType==Schema.DisplayType.Date||ruleFieldType==Schema.DisplayType.DateTime)){
                        metCriteria = (date.valueOf(recordFieldValue) >= currentRule.Start_Date__c && date.valueOf(recordFieldValue) <= currentRule.End_Date__c);
                        
                }         
            }else if (operator=='not equal to'){//if 1
                if (ruleFieldType==Schema.DisplayType.BOOLEAN){//if 2
                        metCriteria=!(ruleFieldValue.equalsIgnoreCase(recordFieldValue));
                } else if (recordFieldValue!=null&&(ruleFieldType==Schema.DisplayType.Double||ruleFieldType==Schema.DisplayType.Currency||ruleFieldType==Schema.DisplayType.Percent ||ruleFieldType==Schema.DisplayType.Integer)){
                        metCriteria=(decimal.valueOf(recordFieldValue)!=decimal.valueOf(ruleFieldValue));
                } else if (recordFieldValue!=null&&(ruleFieldType==Schema.DisplayType.STRING || ruleFieldType==Schema.DisplayType.ComboBox ||ruleFieldType==Schema.DisplayType.Picklist ||ruleFieldType==Schema.DisplayType.MultiPicklist ||ruleFieldType==Schema.DisplayType.email||ruleFieldType==Schema.DisplayType.encryptedString||ruleFieldType==Schema.DisplayType.Phone||ruleFieldType==Schema.DisplayType.url)){
                        metCriteria=(recordFieldValue.toLowerCase()!=ruleFieldValue.toLowerCase());
                } else if (recordFieldValue!=null&&(ruleFieldType==Schema.DisplayType.Date||ruleFieldType==Schema.DisplayType.DateTime)){
                        metCriteria = !(date.valueOf(recordFieldValue) > currentRule.Start_Date__c && date.valueOf(recordFieldValue) < currentRule.End_Date__c);
                }             
           } else if (operator=='greater than'){//if 1
                if (recordFieldValue!=null&&(ruleFieldType==Schema.DisplayType.STRING || ruleFieldType==Schema.DisplayType.ComboBox ||ruleFieldType==Schema.DisplayType.Picklist ||ruleFieldType==Schema.DisplayType.MultiPicklist ||ruleFieldType==Schema.DisplayType.email||ruleFieldType==Schema.DisplayType.encryptedString||ruleFieldType==Schema.DisplayType.Phone||ruleFieldType==Schema.DisplayType.url)){
                        metCriteria=(recordFieldValue.toLowerCase()>ruleFieldValue.toLowerCase());
                } else if (recordFieldValue!=null&&(ruleFieldType==Schema.DisplayType.Double||ruleFieldType==Schema.DisplayType.Currency||ruleFieldType==Schema.DisplayType.Percent ||ruleFieldType==Schema.DisplayType.Integer)){
                        metCriteria=(decimal.valueOf(recordFieldValue)>decimal.valueOf(ruleFieldValue));
                } else if (recordFieldValue!=null&&(ruleFieldType==Schema.DisplayType.Date||ruleFieldType==Schema.DisplayType.DateTime)){
                        metCriteria = date.valueOf(recordFieldValue) > currentRule.Start_Date__c;
                }   
           } else if (operator=='less than'){//if 1
                if (recordFieldValue!=null&&(ruleFieldType==Schema.DisplayType.STRING || ruleFieldType==Schema.DisplayType.ComboBox ||ruleFieldType==Schema.DisplayType.Picklist ||ruleFieldType==Schema.DisplayType.MultiPicklist ||ruleFieldType==Schema.DisplayType.email||ruleFieldType==Schema.DisplayType.encryptedString||ruleFieldType==Schema.DisplayType.Phone||ruleFieldType==Schema.DisplayType.url)){
                        metCriteria=(recordFieldValue.toLowerCase()<ruleFieldValue.toLowerCase());
                } else if (recordFieldValue!=null&&(ruleFieldType==Schema.DisplayType.Double||ruleFieldType==Schema.DisplayType.Currency||ruleFieldType==Schema.DisplayType.Percent ||ruleFieldType==Schema.DisplayType.Integer)){
                        metCriteria=(decimal.valueOf(recordFieldValue) < decimal.valueOf(ruleFieldValue));
                        
                } else if (recordFieldValue!=null&&(ruleFieldType==Schema.DisplayType.Date||ruleFieldType==Schema.DisplayType.DateTime)){
                        metCriteria = date.valueOf(recordFieldValue) < currentRule.Start_Date__c;
                } 
           } else if (operator=='greater or equal'){//if 1
                if (recordFieldValue!=null&&(ruleFieldType==Schema.DisplayType.STRING || ruleFieldType==Schema.DisplayType.ComboBox ||ruleFieldType==Schema.DisplayType.Picklist ||ruleFieldType==Schema.DisplayType.MultiPicklist ||ruleFieldType==Schema.DisplayType.email||ruleFieldType==Schema.DisplayType.encryptedString||ruleFieldType==Schema.DisplayType.Phone||ruleFieldType==Schema.DisplayType.url)){
                        metCriteria=(recordFieldValue.toLowerCase()>=ruleFieldValue.toLowerCase());
                } else if (recordFieldValue!=null&&(ruleFieldType==Schema.DisplayType.Double||ruleFieldType==Schema.DisplayType.Currency||ruleFieldType==Schema.DisplayType.Percent ||ruleFieldType==Schema.DisplayType.Integer)){
                        metCriteria=(decimal.valueOf(recordFieldValue)>=decimal.valueOf(ruleFieldValue));
                } else if (recordFieldValue!=null&&(ruleFieldType==Schema.DisplayType.Date||ruleFieldType==Schema.DisplayType.DateTime)){
                        metCriteria = date.valueOf(recordFieldValue) >= currentRule.Start_Date__c;
                }  
           }else if (operator=='less or equal'){//if 1
                if (recordFieldValue!=null&&(ruleFieldType==Schema.DisplayType.STRING || ruleFieldType==Schema.DisplayType.ComboBox ||ruleFieldType==Schema.DisplayType.Picklist ||ruleFieldType==Schema.DisplayType.MultiPicklist ||ruleFieldType==Schema.DisplayType.email||ruleFieldType==Schema.DisplayType.encryptedString||ruleFieldType==Schema.DisplayType.Phone||ruleFieldType==Schema.DisplayType.url)){
                        metCriteria=(recordFieldValue.toLowerCase()<=ruleFieldValue.toLowerCase());
                } else if (recordFieldValue!=null&&(ruleFieldType==Schema.DisplayType.Double||ruleFieldType==Schema.DisplayType.Currency||ruleFieldType==Schema.DisplayType.Percent ||ruleFieldType==Schema.DisplayType.Integer)){
                        metCriteria=(decimal.valueOf(recordFieldValue)<=decimal.valueOf(ruleFieldValue));
                } else if (recordFieldValue!=null&&(ruleFieldType==Schema.DisplayType.Date||ruleFieldType==Schema.DisplayType.DateTime)){
                         metCriteria = date.valueOf(recordFieldValue) <= currentRule.Start_Date__c;
                }  
             }else if (operator=='contains'){//if 1
                    if(recordFieldValue!=null){//if 3;  Checks for null values to avoid null pointer exception for blank lead
                        if(ruleFieldType==Schema.DisplayType.MultiPicklist) {
                            for(String str: ruleFieldValue.split(';')) {
                                metCriteria = recordFieldValue.toLowerCase().contains(str.toLowerCase());
                                break;
                            }
                        } 
                        metCriteria=recordFieldValue.toLowerCase().contains(ruleFieldValue.toLowerCase());
                    }//if 3    
             }else if (operator=='does not contain'){//if 1
                    if(recordFieldValue!=null){//if 3;  Checks for null values to avoid null pointer exception for blank lead
                            if(ruleFieldType==Schema.DisplayType.MultiPicklist) {
                                for(String str: ruleFieldValue.split(';')) {
                                    metCriteria = !recordFieldValue.toLowerCase().contains(str.toLowerCase());
                                    break;
                                }
                            } else {
                                metCriteria=!recordFieldValue.toLowerCase().contains(ruleFieldValue.toLowerCase());
                            }
                    }//if 3    
             }else if (operator=='starts with'){//if 1
                //took out the DisplayType check with the assumption that the controller code only allows string types to use Starts With
                if(recordFieldValue!=null){//if 3;  Checks for null values to avoid null pointer exception for blank lead
                    metCriteria=recordFieldValue.startsWith(ruleFieldValue);
                }//if 2 
           }//if 1        
        return metCriteria;
    }
    
    public static String getRuleFieldNames(List<String> fieldNames){
        String ruleFieldNameString='';
        Set<String> ruleFieldNames=new Set<String>();
        
        //Loop through the rules & get the field names possibly needed for criteria verification
        for (String fieldName:fieldNames){
            system.debug('Field added :'+fieldName);
            if (ruleFieldNames.contains(fieldName.toLowerCase())==False){//note that a set.Contains() doesn't match "Email" and "email"
                ruleFieldNames.add(fieldName.toLowerCase());
                ruleFieldNameString+=fieldName+',';
            }//if
        }//for
        return ruleFieldNameString;
    }//getRuleFieldNames;

    
    //this gets the field type for each LeadScoringRule__c.  The type is needed for comparison later    
    public static Map<String,DisplayType> getRuleFieldInfo(Map<String, Schema.SObjectField> objectMap, Lead_Score__c[] Rules){
        Set<String> RuleFieldNames=new Set<String>();
        Map<String,DisplayType> fieldTypeMap=new Map<String,DisplayType>();
        
        for (Lead_Score__c rule:Rules){
            if (RuleFieldNames.contains(rule.Field_Name__c)==FALSE){
                RuleFieldNames.add(rule.Field_Name__c);
                try{    //try to get the field type for this field for casting the object later.  
                        //This will fail if the rule.Field_Name__c doesn't match the API name for a field in CampaignMember
                    Schema.SObjectField fieldToken=objectMap.get(rule.Field_Name__c);
                    fieldTypeMap.put(rule.Field_Name__c,fieldToken.getDescribe().getType());
                } catch (Exception e){
//                    system.debug('The field name in this customers rule is likely invalid: '+ rule.Field_Name__c);
//                    system.debug('Error when trying to get the type of this field: '+ e);
                }//try    
            }//if
        }//for    
        
        return fieldTypeMap;
    }//getRuleFieldInfo
    
    */
}