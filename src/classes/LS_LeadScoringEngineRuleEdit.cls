public with sharing class LS_LeadScoringEngineRuleEdit {
   /* public Boolean picklistRendered { get; set;}
    public Boolean multiPicklistRendered { get; set;}
    public Boolean dateRendered { get; set;}
    public Boolean inputBoxRendered{ get; set; }
    public Boolean inputCheckBoxRendered{ get; set;}
    public SelectOption[] selectedValues { get; set; }
    public SelectOption[] allValues { get; set; }
    public Lead_Score__c objLeadScore {get;set;}
    public List<Lead_Score__c> listLeadScoreToRemove;
    public List<LeadScoreWrapper> listLeadScoreWrapper {get; set;}
    public Integer counter;
    public Boolean isEdit {get;set;}
    Map<String,String> fieldNameLabelMap;
    Map<String,String> fieldNameTypeMap;
    public Boolean rightDateRender {get;set;}
    public Boolean leftDateRender {get;set;}    
    
    public List<SelectOption> leadFieldNames {get;set;}
    public List<SelectOption> operatorValues {get;set;}
    public List<SelectOption> valuePicklistOptions {get;set;}
    
    public LS_LeadScoringEngineRuleEdit() {
        counter = 0;
        isEdit = false;
        rightDateRender = false;
        leftDateRender = rightDateRender;
        objLeadScore = new Lead_Score__c();
        objLeadScore.IsActive__c = true;
        listLeadScoreWrapper = new List<LeadScoreWrapper>();
        listLeadScoreToRemove = new List<Lead_Score__c>(); 
        valueRender();   
        getLSERFieldNames(); 
        for(Lead_Score__c objLS: [Select l.Value__c, 
                                      l.Start_Date__c, 
                                      l.Score__c, 
                                      l.OwnerId, l.Owner.Name,
                                      l.Order__c, 
                                      l.Operator__c, 
                                      l.Name, 
                                      l.IsActive__c, 
                                      l.Id, 
                                      l.Field_Name__c, 
                                      l.Field_Label__c, 
                                      l.End_Date__c, 
                                      l.Data_Type__c, 
                                      l.Comments_Description__c 
                                 From Lead_Score__c l 
                                 ORDER BY Order__c] ) {
        
            listLeadScoreWrapper.add(new LeadScoreWrapper(objLS, counter));
            counter ++;
        }
    }
  
    public PageReference saveRules()
    {
        PageReference pr;
        try{
            
            if(listLeadScoreToRemove != null && ! listLeadScoreToRemove.isEmpty()) {
                delete listLeadScoreToRemove;
            }
            if(! listLeadScoreWrapper.isEmpty() ) {
                List<Lead_Score__c> listLeadScoreToUpsert = new List<Lead_Score__c>();
                for(LeadScoreWrapper lsw: listLeadScoreWrapper) {
                    listLeadScoreToUpsert.add(lsw.leadScore);
                }
                upsert listLeadScoreToUpsert;
            }
            pr = new PageReference('/00Q/o');
            pr.setRedirect(true);
            return pr;    
        }catch(Exception e){
            system.debug('Exception message '+e);
            //ApexPages.Message myMsg = new ApexPages.Message(ApexPages.Severity.ERROR,e.getMessage());
            //ApexPages.addMessage(myMsg);
            ApexPages.addmessages(e);            
            return null;
        }//try

    }//saveRules
    
    
    public PageReference quickSaveRules() {
        PageReference pr;
        try{
            
            if(listLeadScoreToRemove != null && ! listLeadScoreToRemove.isEmpty()) {
                delete listLeadScoreToRemove;
            }
            if(! listLeadScoreWrapper.isEmpty() ) {
                List<Lead_Score__c> listLeadScoreToUpsert = new List<Lead_Score__c>();
                for(LeadScoreWrapper lsw: listLeadScoreWrapper) {
                    listLeadScoreToUpsert.add(lsw.leadScore);
                }
                upsert listLeadScoreToUpsert;
            }
            pr = new PageReference('/apex/LS_LeadScoringEngineRuleEdit');
            pr.setRedirect(true);
            return pr;    
        }catch(Exception e){
            system.debug('Exception message '+e);
            //ApexPages.Message myMsg = new ApexPages.Message(ApexPages.Severity.ERROR,e.getMessage());
            //ApexPages.addMessage(myMsg);
            ApexPages.addmessages(e);            
            return null;
        }//try

    }//saveRules
    
    public PageReference evaluateAllLeads() {
        LS_LeadScoringInitialize lsi = new LS_LeadScoringInitialize();
        lsi.autoRun();
        PageReference pr = new PageReference('/00Q/o');
        pr.setRedirect(true);
        return pr; 
        
    }
        
    private void getLSEROperator(){
       if(operatorValues == null) {
        operatorValues = new List<selectOption>();
       } else {
        operatorValues.clear();
       }
       Map<String, Schema.SObjectField> leadFieldMap=Schema.SObjectType.Lead.fields.getMap();
       DisplayType fieldType;
       Boolean typeSelected=False;
       String fieldName =  objLeadScore.Field_Name__c;//ApexPages.currentPage().getParameters().get('leadFieldName');
       Schema.SObjectField fieldToken=leadFieldMap.get(fieldName);
        try{
           fieldType=fieldToken.getDescribe().getType();
           typeSelected=true;
        }catch (Exception e){}//exception happens when changing type as the other picklist hasn't had a chance to refresh yet   
    
       //only add an operator if a valid field type is chosen.  Otherwise, leave it blank.
       if (typeSelected){//if 1
           if (fieldType!=Schema.DisplayType.anyType && fieldType!=Schema.DisplayType.base64 && fieldType!=Schema.DisplayType.EncryptedString && fieldType!=Schema.DisplayType.Id &&fieldType!=Schema.DisplayType.Reference&&fieldType!=Schema.DisplayType.TextArea && fieldType!=Schema.DisplayType.Time){//if 2
               //the above types are unsupported at this time 
               operatorValues.add(new selectOption('equals','equals')); 
               operatorValues.add(new selectOption('not equal to','not equal to')); 
               if(fieldType == Schema.DisplayType.MultiPicklist) {
                   operatorValues.add(new selectOption('contains','contains')); 
                   operatorValues.add(new selectOption('does not contain','does not contain')); 
               }
               if (fieldType!=Schema.DisplayType.Boolean&&fieldType!=Schema.DisplayType.Picklist&&fieldType!=Schema.DisplayType.MultiPicklist){//if 3
                   //boolean can't use the below, but the others can
                   operatorValues.add(new selectOption('greater than','greater than')); 
                   operatorValues.add(new selectOption('less than','less than')); 
                   operatorValues.add(new selectOption('greater or equal','greater or equal')); 
                   operatorValues.add(new selectOption('less or equal','less or equal')); 
                   if (fieldType==Schema.DisplayType.STRING || fieldType==Schema.DisplayType.ComboBox ||fieldType==Schema.DisplayType.Picklist ||fieldType==Schema.DisplayType.email||fieldType==Schema.DisplayType.encryptedString||fieldType==Schema.DisplayType.Phone||fieldType==Schema.DisplayType.url){//if 4
                       //these operatorValues are for string type fields
                       operatorValues.add(new selectOption('contains','contains')); 
                       operatorValues.add(new selectOption('does not contain','does not contain')); 
                       operatorValues.add(new selectOption('starts with','starts with')); 
                   }//if 4
              } 
          }//if 2          
       }//if 1
    }
    
    private void getLSERFieldNames(){
        leadFieldNames=new List<selectOption>();
        List<String> fieldLabels=new List<String>();//included to create a sorted field name list
        Map<String,String> fieldLabelNameMap=new Map<String,String>();
        Map<String, Schema.SObjectField> fieldMap=new Map<String, Schema.SObjectField>();
        fieldNameLabelMap = new Map<String,String>();
        fieldNameTypeMap = new Map<String,String>();
        fieldMap=Schema.SObjectType.Lead.fields.getMap();
        leadFieldNames.add(new selectOption('',''));
            
        for (String fName:fieldMap.keySet()){//for 1
            //Disallow unsupported field types
            if(fieldMap.get(fName).getDescribe().getType()!=Schema.DisplayType.anytype&&fieldMap.get(fName).getDescribe().getType()!=Schema.DisplayType.base64&&fieldMap.get(fName).getDescribe().getType()!=Schema.DisplayType.EncryptedString&&fieldMap.get(fName).getDescribe().getType()!=Schema.DisplayType.Id&&fieldMap.get(fName).getDescribe().getType()!=Schema.DisplayType.Reference&&fieldMap.get(fName).getDescribe().getType()!=Schema.DisplayType.TextArea&&fieldMap.get(fName).getDescribe().getType()!=Schema.DisplayType.Time){
                fieldLabels.add(fieldMap.get(fName).getDescribe().getLabel());                  
                fieldLabelNameMap.put(fieldMap.get(fName).getDescribe().getLabel(), fName);
                fieldNameLabelMap.put(fName, fieldMap.get(fName).getDescribe().getLabel());
                fieldNameTypeMap.put(fName, string.valueOf(fieldMap.get(fName).getDescribe().getType()));
            }//if 2
        }//for 1
        fieldLabels.sort();
        for (String fLabel:fieldLabels){//for 1
            //remove scoring fields
            if((fieldLabelNameMap.get(fLabel)!='Lead_Score__c')){//if 2
                leadFieldNames.add(new selectOption(fieldLabelNameMap.get(fLabel),fLabel));
            }//if 2
        }//for 1    
        if(objLeadScore.Value__c != '' && objLeadScore.Value__c != null) {
            objLeadScore.Value__c = null;
        }
    }//getLSERFieldNames
    
    private void getValuePicklistOptions(){
        if(valuePicklistOptions == null) {
            valuePicklistOptions = new List<SelectOption>();
        } else {
            valuePicklistOptions.clear();
        }
        Map<String, Schema.SObjectField> fieldMap=new Map<String, Schema.SObjectField>();
        String fieldName =  objLeadScore.Field_Name__c;//ApexPages.currentPage().getParameters().get('leadFieldName');
        if (fieldName !=null){
            system.debug('Field Name: '+fieldName);
            fieldMap=Schema.SObjectType.Lead.fields.getMap();
            try{//this will error when changing types as the field name is different than the type as the FieldNames picklsit hasn't been regenerated yet
                Schema.DisplayType fType=fieldMap.get(fieldName).getDescribe().getType();
                if(fType==Schema.DisplayType.Picklist){
                    for (Schema.PickListEntry pickVal: fieldMap.get(fieldName).getDescribe().getPicklistValues()){
                        // create a selectoption for each pickval
                        valuePicklistOptions.add(new SelectOption(pickVal.getValue(),pickVal.getLabel()));
                    }//for 1
                }else if(fType==Schema.DisplayType.MultiPicklist){//if 1
                    allValues = new List<SelectOption>();
                    selectedValues = new List<SelectOption>();
                    if(objLeadScore.Value__c != '' && objLeadScore.Value__c != null) {
                        Set<String> setSelValues = new Set<String> ();
                        setSelValues.addAll(objLeadScore.Value__c.split(';'));
                        for (Schema.PickListEntry pickVal: fieldMap.get(fieldName).getDescribe().getPicklistValues()){
                            if(!setSelValues.contains(pickVal.getValue())) {
                                allValues.add(new SelectOption(pickVal.getValue(),pickVal.getLabel()));
                            } else {
                                selectedValues.add(new SelectOption(pickVal.getValue(),pickVal.getLabel()));
                            }
                        }//for 1
                    } else {
                        for (Schema.PickListEntry pickVal: fieldMap.get(fieldName).getDescribe().getPicklistValues()){
                            allValues.add(new SelectOption(pickVal.getValue(),pickVal.getLabel()));
                        }//for
                    }
                }
            }catch(Exception e){
                system.debug('getValuePicklistOptions had an error'+e);
            }
        }//if 1    
    } //getValuePicklistOptions
    
    public void dateSelector() {
        if(objLeadScore.Operator__c == null || objLeadScore.Operator__c =='') {
            objLeadScore.Operator__c = 'equals';
        }
        String fieldOperator =  objLeadScore.Operator__c;
        if((fieldOperator != null && fieldOperator != '' ) && (fieldNameTypeMap.get(objLeadScore.Field_Name__c) == 'DATE' || fieldNameTypeMap.get(objLeadScore.Field_Name__c) == 'DATETIME')) {
            if(fieldOperator == 'greater than' || fieldOperator == 'less than' || fieldOperator == 'greater or equal' || fieldOperator == 'less or equal' ) {
                rightDateRender = false;
                leftDateRender = !rightDateRender ;
                if(objLeadScore.End_Date__c != null) { objLeadScore.End_Date__c = null; }
            } else if(fieldOperator == 'equals' || fieldOperator == 'not equal to'){
                rightDateRender = true;
                leftDateRender = rightDateRender;
            } else {
                rightDateRender = false;
                leftDateRender = rightDateRender;
                if(objLeadScore.Start_Date__c  != null){ objLeadScore.Start_Date__c = null; }
                if(objLeadScore.End_Date__c != null){ objLeadScore.End_Date__c = null; }
            }
        }
    }
    
    public void valueRender(){
        picklistRendered=False;
        inputCheckBoxRendered=False;
        multiPicklistRendered = False;
        dateRendered = False;
        Map<String, Schema.SObjectField> fieldMap=new Map<String, Schema.SObjectField>();
        String fieldName =  objLeadScore.Field_Name__c;//ApexPages.currentPage().getParameters().get('leadFieldName');
        if (fieldName !=null){//if 1
            fieldMap=Schema.SObjectType.Lead.fields.getMap();
            if(fieldMap.get(fieldName).getDescribe().getType()==Schema.DisplayType.Picklist){//if 1
                picklistRendered=True;
            }else if(fieldMap.get(fieldName).getDescribe().getType()==Schema.DisplayType.Boolean){//if 2
                inputCheckBoxRendered = True;
            }else if(fieldMap.get(fieldName).getDescribe().getType()==Schema.DisplayType.MultiPicklist){
                multiPicklistRendered = True;
            }else if(fieldMap.get(fieldName).getDescribe().getType()==Schema.DisplayType.Date || fieldMap.get(fieldName).getDescribe().getType()==Schema.DisplayType.DateTime) {
                dateRendered = True;
            }
        }//if 1
        inputboxRendered=!(picklistRendered || inputCheckBoxRendered || multiPicklistRendered || dateRendered);
        getLSEROperator();
        if(picklistRendered || multiPicklistRendered) {
            getValuePicklistOptions();
        } else if(dateRendered) {
            dateSelector();
        }
    }//valueRender   
    
    
    //Add Lead Score
    public void addLeadScore() {
        if(multiPicklistRendered) {
            Boolean first = true;
            String valueToSave = '';
            for ( SelectOption so : selectedValues ) {
                if (!first) {
                    valueToSave += ';';
                }
                valueToSave += so.getValue();
                first = false;
            }
            objLeadScore.Value__c = valueToSave;
        }
        objLeadScore.Field_Label__c = fieldNameLabelMap.get(objLeadScore.Field_Name__c);
        objLeadScore.Order__c = counter + 1;
        objLeadScore.Data_Type__c = fieldNameTypeMap.get(objLeadScore.Field_Name__c);
        listLeadScoreWrapper.add(new LeadScoreWrapper(objLeadScore, counter));
        counter ++;
        objLeadScore = new Lead_Score__c();
        objLeadScore.IsActive__c = true;
        valueRender();
    }
    
    public void deleteLeadScore() {
        String ls_index = ApexPages.currentPage().getParameters().get('ls_index');
        system.debug('*********ls_index'+ls_index);
        if(ls_index != null && ls_index != '') {
            if(listLeadScoreWrapper.get(integer.valueOf(ls_index)).leadScore.Id != null) {
                listLeadScoreToRemove.add(listLeadScoreWrapper.get(integer.valueOf(ls_index)).leadScore);
            }
            listLeadScoreWrapper.remove(integer.valueOf(ls_index));
            recalculateLeadIndexes();
        }
    }
    
    public void editLeadScore(){
        String ls_index = ApexPages.currentPage().getParameters().get('ls_index');
        system.debug('*********ls_index'+ls_index);
        if(ls_index != null && ls_index != '') {
            listLeadScoreWrapper.get(integer.valueOf(ls_index)).isEdit = true;
            isEdit = true;
            LeadScoreWrapper lw = listLeadScoreWrapper.get(integer.valueOf(ls_index));
            leftDateRender = lw.leftDate;
            rightDateRender  = lw.rightDate;
            objLeadScore = lw.leadScore;
            valueRender();
        }
    }
    
    public void saveLeadScore(){
        String ls_index = ApexPages.currentPage().getParameters().get('ls_index');
        system.debug('*********ls_index'+ls_index);
        if(ls_index != null && ls_index != '') {
            LeadScoreWrapper lw = listLeadScoreWrapper.get(integer.valueOf(ls_index));
            if(multiPicklistRendered) {
                Boolean first = true;
                String valueToSave = '';
                for ( SelectOption so : selectedValues ) {
                    if (!first) {
                        valueToSave += ';';
                    }
                    valueToSave += so.getValue();
                    first = false;
                }
                
                lw.leadScore.Value__c = valueToSave;
            }
            if(dateRendered) {
                lw.leftDate  = leftDateRender;
                lw.rightDate = rightDateRender;
            }
            lw.isEdit = false;
            isEdit = false;
            objLeadScore = new Lead_Score__c();
        }
    }
    
    public void recalculateLeadIndexes() {
        counter = 0;
        for(LeadScoreWrapper lsw : listLeadScoreWrapper) {
            lsw.index = counter;
            lsw.leadScore.Order__c = counter + 1;
            counter ++;
        }
    }
    
    public class LeadScoreWrapper {
        public Lead_Score__c leadScore {get;set;}
        public Integer index {get; set;}
        public Boolean isEdit {get;set;}
        public Boolean isDate {get;set;}
        public Boolean leftDate {get;set;}
        public Boolean rightDate {get;set;}
        
        public LeadScoreWrapper(Lead_Score__c ls, Integer idx) {
            this.leadScore = ls;
            this.isEdit = false;
            this.index = idx;

            if(ls.Data_Type__c != null && ls.Data_Type__c.containsIgnoreCase('date')) {
                isDate = true;
                if(ls.Operator__c == 'equals' || ls.Operator__c == 'not equal to') {
                    leftDate = true;
                    rightDate = true;
                } else {
                    leftDate = true;
                    rightDate = false;
                }
            } else {
                isDate = false;
            }
        }
    } 
    */
}