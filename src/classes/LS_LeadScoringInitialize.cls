public class LS_LeadScoringInitialize{
/*
    // Constructor - this only really matters if the autoRun function doesn't work right
    public LS_LeadScoringInitialize() {
    }
    // Code we will invoke on button click.
    public void autoRun() {
        autoRunLeads(null, false, new Set<Id>() );
    }
 
    @future 
    public static void evaluateLeadsAsync(Set<Id> leadIds) {
        autoRunLeads(null, true, leadIds);
    }
    
    
    public static void autoRunLeads(Integer qryLimit, Boolean isTrigger, Set<Id> leadIds) {
         //First invoke the lead initialization.  
        
        
        //get the field names from here next, and then do the same for the other query, check exceptions
        Lead_Score__c[] Rules = LS_LeadScoringEngine.activeLeadScoringRuleDetails();
        List<String> fieldNames = new List<String>();

        Map<String, Schema.SObjectField> leadMap=Schema.SObjectType.Lead.fields.getMap();
        Map<String,DisplayType> leadTypeMap = new Map<String,DisplayType>(); //this will hold the field type for all the fields in the Lead_Score__c

        //gets the field types for each rule, needed for formula's later
        leadTypeMap = LS_LeadScoringEngine.getRuleFieldInfo(leadMap, Rules);

        for (Integer i=0;i<Rules.size();i++){
            if(leadTypeMap.containsKey(Rules[i].Field_Name__c) == True){
                fieldNames.add(Rules[i].Field_Name__c);
            } else {
                Rules.remove(i);//remove the rule if that field name isn't a valid field on Lead
            }//if 1
        }//for 1
        String qrySOQL = LS_LeadScoringEngine.getRuleFieldNames(fieldNames);
        qrySOQL='SELECT ' + qrySOQL +'Id,Lead_Score__c FROM Lead ';
        
        if(isTrigger) {
            qrySOQL += 'WHERE Id IN :leadIds AND isConverted = FALSE';
        } else {
           qrySOQL += ' WHERE isConverted = FALSE ';
        }
        
        if(qryLimit != null && qryLimit > 0){
            qrySOQL = qrySOQL + ' LIMIT ' + qryLimit;
        }
        if(! isTrigger) {
            LS_LeadScoringBatch initLeads = new LS_LeadScoringBatch();
            initLeads.leadQuery = qrySOQL;
        
            Id leadProcessId;
            if(qryLimit != null && qryLimit <= 200) {
                leadProcessId = Database.executeBatch(initLeads, qryLimit);
            } else {
                leadProcessId = Database.executeBatch(initLeads);
            } 
        } else {
            LS_LeadScoringEngine.evaluateLeads(Database.query(qrySOQL));    
        }
    }//autoRunLeads
   */ 
}