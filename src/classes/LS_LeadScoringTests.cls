@isTest(seealldata=false)
private class LS_LeadScoringTests{
/*    //used to run the tests quicker by ignoring the batch tests with 90 leads & 80 rules
    static Boolean runBatchTests=true;
    

    
    //utility class to create dummy data to test
    public static List<Lead> createLeads(Long NumLeads, String LN, String Cmpy, Boolean OptedOutOfEmail, String LeadSrc, Boolean OptOutFax, Double AnnualRev) {
        List<Lead> leads = new List<Lead>{};
        Integer i;
        for (i=0;i<numLeads;i++){
            Lead l = new Lead(LastName=LN+i, Company=Cmpy, HasOptedOutOfEmail=OptedOutOfEmail, LeadSource=LeadSrc, HasOptedOutOfFax=OptOutFax, AnnualRevenue=AnnualRev);
            leads.add(l);
        }//for
        insert leads;

        return leads;
    }//createLeads


    public static Lead_Score__c createLSR(Boolean Active, Double LSRScore, String LSRFN, String LSROperator, String LSRValue, Integer order) {
        Lead_Score__c lsr;
        lsr=new Lead_Score__c(IsActive__c=Active, Score__c=LSRScore, Field_Name__c=LSRFN, Field_Label__c = LSRFN, Operator__c=LSROperator, Value__c=LSRValue, Order__c = order);
        
        return lsr;
    }//createLSR

    
    public static Lead_Score__c createLSR(Boolean Active, Double LSRScore, String LSRFN, String LSROperator, Date startDate, Date endDate, Integer order) {
        Lead_Score__c lsr;
        lsr=new Lead_Score__c(IsActive__c=Active, Score__c=LSRScore, Field_Name__c=LSRFN, Field_Label__c = LSRFN, Operator__c=LSROperator, Start_Date__c=startDate, End_Date__C = endDate,Order__c = order);
        
        return lsr;
    }//createLSRDate
    
    public static void cleanUpTestData() {
        List<Lead_Score__c> lsrs=[Select ID from Lead_Score__c LIMIT 500];
        delete lsrs;
    }//cleanUpTestData
       
    static testMethod void verifyLeadStringEquals(){
        Long numLeads=1;
        List<Lead> leads = new List<lead>{};
        Boolean Active=True;
        
        Double LSRScore=1;
        String LSRFieldName='Company';
        String LSROperator='eQuals';//testing capitalization as well
        String LSRValue='comPany';//testing capitalization as well
        List<Lead_Score__c> lsrs=new List<Lead_Score__c>();

        String LN='TestLead';
        String Company=LSRValue;
        Boolean HasOptedOutOfEmail=TRUE;
        String LeadSource='Web';
        Boolean HasOptedOutOfFax=FALSE;
        String CampaignName='';
        Double AnnualRev=1000;
        
        cleanUpTestData();
        
        lsrs.add(createLSR(Active, LSRScore, LSRFieldName, LSROperator, LSRValue, 1));    
        insert lsrs;
        
        Test.StartTest();
        leads=createLeads(numLeads,LN,Company,HasOptedOutOfEmail, LeadSource, HasOptedOutOfFax,AnnualRev);
        
        Company='asdf';
        List<Lead> leads2=createLeads(numLeads,LN,Company,HasOptedOutOfEmail, LeadSource, HasOptedOutOfFax,AnnualRev);
        Test.StopTest();

        Lead l=[Select Id, Company,Lead_Score__c FROM Lead WHERE Id=:leads[0].Id LIMIT 1];
        system.assertEquals(l.Lead_Score__c,LSRScore);

        l=[Select Id, Lead_Score__c FROM Lead WHERE Id=:leads2[0].Id LIMIT 1];
        system.assertNOTEquals(l.Lead_Score__c,LSRScore);
    }//verifyLeadStringEquals
    
    static testMethod void verifyLeadStringNotEqualTo(){
        Long numLeads=1;
        List<Lead> leads = new List<lead>();
        Boolean Active=True;
        
        Double LSRScore=1;
        
        String LSRFieldName='Company';
        String LSROperator='Not Equal To';
        String LSRValue='TestString';
        List<Lead_Score__c> lsrs=new List<Lead_Score__c>();

        String LN='TestLead';
        String Company='Company';
        Boolean HasOptedOutOfEmail=TRUE;
        String LeadSource='Web';
        Boolean HasOptedOutOfFax=FALSE;
        String CampaignName='';
        Double AnnualRev=1000;

        cleanUpTestData();
        
        lsrs.add(createLSR(Active, LSRScore, LSRFieldName, LSROperator, LSRValue, 1));    
        insert lsrs;
        
        Test.StartTest();
        leads=createLeads(numLeads,LN,Company,HasOptedOutOfEmail, LeadSource, HasOptedOutOfFax,AnnualRev);
        Company='TestString';
        List<Lead> leads2=createLeads(numLeads,LN,Company,HasOptedOutOfEmail, LeadSource, HasOptedOutOfFax,AnnualRev);
        Test.StopTest();

        Lead l=[Select Id, Company,Lead_Score__c FROM Lead WHERE Id=:leads[0].Id LIMIT 1];
        system.assertEquals(l.Lead_Score__c,LSRScore);
        l=[Select Id, Lead_Score__c FROM Lead WHERE Id=:leads2[0].Id LIMIT 1];
        system.assertNOTEquals(l.Lead_Score__c,LSRScore);
    }//verifyLeadStringNotEqualTo

    static testMethod void verifyLeadPicklistContains(){
        Long numLeads=1;
        List<Lead> leads = new List<lead>();
        Boolean Active=True;
        
        Double LSRScore=1;
        
        String LSRFieldName='LeadSource';
        String LSROperator='Contains';
        String LSRValue='we';//note testing case sensitivity of the LSR value here
        List<Lead_Score__c> lsrs=new List<Lead_Score__c>();
        
        String LN='TestLead';
        String Company='Company';
        Boolean HasOptedOutOfEmail=TRUE;
        String LeadSource='Web';
        Boolean HasOptedOutOfFax=FALSE;
        String CampaignName='';
        Double AnnualRev=1000;

        cleanUpTestData();
        
        lsrs.add(createLSR(Active, LSRScore, LSRFieldName, LSROperator, LSRValue, 1));    
        insert lsrs;
        
        Test.StartTest();
        leads=createLeads(numLeads,LN,Company,HasOptedOutOfEmail, LeadSource, HasOptedOutOfFax,AnnualRev);
        LeadSource='Referral';
        List<Lead> leads2=createLeads(numLeads,LN,Company,HasOptedOutOfEmail, LeadSource, HasOptedOutOfFax,AnnualRev);
        Test.StopTest();

        Lead l=[Select Id, Company,Lead_Score__c FROM Lead WHERE Id=:leads[0].Id LIMIT 1];
        system.assertEquals(l.Lead_Score__c,LSRScore);
        l=[Select Id, Lead_Score__c FROM Lead WHERE Id=:leads2[0].Id LIMIT 1];
        system.assertNOTEquals(l.Lead_Score__c,LSRScore);
    }//verifyLeadPicklistContains

    static testMethod void verifyLeadPicklistDoesNotContain(){
        Long numLeads=1;
        List<Lead> leads = new List<lead>();
        Boolean Active=True;
        
        Double LSRScore=1;
        String LSRFieldName='LeadSource';
        String LSROperator='Does Not Contain';
        String LSRValue='eferr';

        List<Lead_Score__c> lsrs=new List<Lead_Score__c>();
        
        String LN='TestLead';
        String Company='Company';
        Boolean HasOptedOutOfEmail=TRUE;
        String LeadSource='Web';
        Boolean HasOptedOutOfFax=FALSE;
        String CampaignName='';
        Double AnnualRev=1000;

        cleanUpTestData();
        
        lsrs.add(createLSR(Active, LSRScore, LSRFieldName, LSROperator, LSRValue, 1));    
        insert lsrs;
        
        Test.StartTest();
        leads=createLeads(numLeads,LN,Company,HasOptedOutOfEmail, LeadSource, HasOptedOutOfFax,AnnualRev);
        LeadSource='ReFerral';//note testing case sensitivity here as well
        List<Lead> leads2=createLeads(numLeads,LN,Company,HasOptedOutOfEmail, LeadSource, HasOptedOutOfFax,AnnualRev);
        Test.StopTest();


        Lead l=[Select Id, Company,Lead_Score__c FROM Lead WHERE Id=:leads[0].Id LIMIT 1];
        system.assertEquals(l.Lead_Score__c,LSRScore);
        l=[Select Id, LeadSource,Lead_Score__c FROM Lead WHERE Id=:leads2[0].Id LIMIT 1];
        system.debug('Lead Source: '+l.LeadSource +' should not contain '+LeadSource);
        system.assertNOTEquals(l.Lead_Score__c,LSRScore);
    }//verifyLeadPicklistDoesNotContain

    static testMethod void verifyLeadStringStartsWith(){
        Long numLeads=1;
        List<Lead> leads = new List<lead>();
        Boolean Active=True;
        
        Double LSRScore=1;
        String LSRFieldName='LastName';
        String LSROperator='Starts With';
        String LSRValue='Test';

        List<Lead_Score__c> lsrs=new List<Lead_Score__c>();
        
        String LN='TestLead';
        String Company='Company';
        Boolean HasOptedOutOfEmail=TRUE;
        String LeadSource='Web';
        Boolean HasOptedOutOfFax=FALSE;
        String CampaignName='';
        Double AnnualRev=1000;

        cleanUpTestData();
        
        lsrs.add(createLSR(Active, LSRScore, LSRFieldName, LSROperator, LSRValue, 1));    
        insert lsrs;
        
        Test.StartTest();
        leads=createLeads(numLeads,LN,Company,HasOptedOutOfEmail, LeadSource, HasOptedOutOfFax,AnnualRev);
        LN='LeadTest';
        List<Lead> leads2=createLeads(numLeads,LN,Company,HasOptedOutOfEmail, LeadSource, HasOptedOutOfFax,AnnualRev);
        Test.StopTest();

        Lead l=[Select Id, Company,Lead_Score__c FROM Lead WHERE Id=:leads[0].Id LIMIT 1];
        system.assertEquals(l.Lead_Score__c,LSRScore);
        l=[Select Id, Lead_Score__c FROM Lead WHERE Id=:leads2[0].Id LIMIT 1];
        system.assertNOTEquals(l.Lead_Score__c,LSRScore);
    }//verifyLeadStringStartsWith

  static testMethod void verifyLeadBooleanEquals(){
        Long numLeads=1;
        List<Lead> leads = new List<lead>();
        Boolean Active=True;
        
        Double LSRScore=1;
        String LSRFieldName='HasOptedOutOfEmail';
        String LSROperator='Equals';
        String LSRValue='True';

        List<Lead_Score__c> lsrs=new List<Lead_Score__c>();
        
        String LN='TestLead';
        String Company='Company';
        Boolean HasOptedOutOfEmail=TRUE;
        String LeadSource='Web';
        Boolean HasOptedOutOfFax=FALSE;
        String CampaignName='';
        Double AnnualRev=1000;

        cleanUpTestData();
        
        lsrs.add(createLSR(Active, LSRScore, LSRFieldName, LSROperator, LSRValue, 1));    
        insert lsrs;

        Test.StartTest();
        leads=createLeads(numLeads,LN,Company,HasOptedOutOfEmail, LeadSource, HasOptedOutOfFax,AnnualRev);
        HasOptedOutOfEmail = FALSE;
        List<Lead> leads2=createLeads(numLeads,LN,Company,HasOptedOutOfEmail, LeadSource, HasOptedOutOfFax,AnnualRev);
        Test.StopTest();

        Lead l=[Select Id, Company,Lead_Score__c FROM Lead WHERE Id=:leads[0].Id LIMIT 1];
        system.assertEquals(l.Lead_Score__c ,LSRScore);
        l=[Select Id, Lead_Score__c FROM Lead WHERE Id=:leads2[0].Id LIMIT 1];
        system.assertNOTEquals(l.Lead_Score__c,LSRScore);
    }//verifyLeadBooleanEquals
    
  static testMethod void verifyLeadBooleanNotEqualTo(){
       Long numLeads=1;
        List<Lead> leads = new List<lead>();
        Boolean Active=True;
        
        Double LSRScore=1;
        String LSRFieldName='HasOptedOutOfEmail';
        String LSROperator='Not Equal To';
        String LSRValue='True';

        List<Lead_Score__c> lsrs=new List<Lead_Score__c>();
        
        String LN='TestLead';
        String Company='Company';
        Boolean HasOptedOutOfEmail=FALSE;
        String LeadSource='Web';
        Boolean HasOptedOutOfFax=FALSE;
        String CampaignName='';
        Double AnnualRev=1000;
        String cmStatus='Sent';//this drives HasResponded=True

        cleanUpTestData();
        
        lsrs.add(createLSR(Active, LSRScore, LSRFieldName, LSROperator, LSRValue, 1));    
        insert lsrs;

        Test.StartTest();        
        leads=createLeads(numLeads,LN,Company,HasOptedOutOfEmail, LeadSource, HasOptedOutOfFax,AnnualRev);
        HasOptedOutOfEmail = TRUE;
        List<Lead> leads2=createLeads(numLeads,LN,Company,HasOptedOutOfEmail, LeadSource, HasOptedOutOfFax,AnnualRev);
        Test.StopTest();

        Lead l=[Select Id, Company,Lead_Score__c FROM Lead WHERE Id=:leads[0].Id LIMIT 1];
        system.assertEquals(l.Lead_Score__c ,LSRScore);
        l=[Select Id, Lead_Score__c FROM Lead WHERE Id=:leads2[0].Id LIMIT 1];
        system.assertNOTEquals(l.Lead_Score__c ,LSRScore);
    }//verifyLeadBooleanNotEqualTo

  static testMethod void verifyLeadDoubleEquals(){
        Long numLeads=1;
        List<Lead> leads = new List<lead>();
        Boolean Active=True;
        
        Double LSRScore=1;
        
        String LSRFieldName='AnnualRevenue';
        String LSROperator='Equals';
        String LSRValue='1000';

        List<Lead_Score__c> lsrs=new List<Lead_Score__c>();
        
        String LN='TestLead';
        String Company='Company';
        Boolean HasOptedOutOfEmail=TRUE;
        String LeadSource='Web';
        Boolean HasOptedOutOfFax=FALSE;
        String CampaignName='';
        Double AnnualRev=1000;

        cleanUpTestData();
        
        lsrs.add(createLSR(Active, LSRScore, LSRFieldName, LSROperator, LSRValue, 1));    
        insert lsrs;
        
        Test.StartTest();
        leads=createLeads(numLeads,LN,Company,HasOptedOutOfEmail, LeadSource, HasOptedOutOfFax,AnnualRev);
        AnnualRev=2000;
        List<Lead> leads2=createLeads(numLeads,LN,Company,HasOptedOutOfEmail, LeadSource, HasOptedOutOfFax,AnnualRev);
        Test.StopTest();

        Lead l=[Select Id, Company,Lead_Score__c FROM Lead WHERE Id=:leads[0].Id LIMIT 1];
        system.assertEquals(l.Lead_Score__c,LSRScore);
        l=[Select Id, Lead_Score__c FROM Lead WHERE Id=:leads2[0].Id LIMIT 1];
        system.assertNOTEquals(l.Lead_Score__c,LSRScore);
    }//verifyLeadDoubleEquals

  static testMethod void verifyLeadDoubleNotEqualTo(){
        Long numLeads=1;
        List<Lead> leads = new List<lead>();
        Boolean Active=True;
        
        Double LSRScore=1;
        
        String LSRFieldName='AnnualRevenue';
        String LSROperator='Not Equal To';
        String LSRValue='999';

        List<Lead_Score__c> lsrs=new List<Lead_Score__c>();
        
        String LN='TestLead';
        String Company='Company';
        Boolean HasOptedOutOfEmail=TRUE;
        String LeadSource='Web';
        Boolean HasOptedOutOfFax=FALSE;
        String CampaignName='';
        Double AnnualRev=1000;

        cleanUpTestData();
        
        lsrs.add(createLSR(Active, LSRScore, LSRFieldName, LSROperator, LSRValue, 1));    
        insert lsrs;
        
        Test.StartTest();
        leads=createLeads(numLeads,LN,Company,HasOptedOutOfEmail, LeadSource, HasOptedOutOfFax,AnnualRev);
        AnnualRev=999;
        List<Lead> leads2=createLeads(numLeads,LN,Company,HasOptedOutOfEmail, LeadSource, HasOptedOutOfFax,AnnualRev);
        Test.StopTest();

        Lead l=[Select Id, Company,Lead_Score__c FROM Lead WHERE Id=:leads[0].Id LIMIT 1];
        system.assertEquals(l.Lead_Score__c,LSRScore);
        l=[Select Id, Lead_Score__c FROM Lead WHERE Id=:leads2[0].Id LIMIT 1];
        system.assertNOTEquals(l.Lead_Score__c,LSRScore);
    }//verifyLeadDoubleNotEqualTo

  static testMethod void verifyLeadDoubleGreaterThan(){
        Long numLeads=1;
        List<Lead> leads = new List<lead>();
        Boolean Active=True;
        
        Double LSRScore=1;
        
        String LSRFieldName='AnnualRevenue';
        String LSROperator='Greater Than';
        String LSRValue='999';

        List<Lead_Score__c> lsrs=new List<Lead_Score__c>();
        
        String LN='TestLead';
        String Company='Company';
        Boolean HasOptedOutOfEmail=TRUE;
        String LeadSource='Web';
        Boolean HasOptedOutOfFax=FALSE;
        String CampaignName='';
        Double AnnualRev=1000;

        cleanUpTestData();
        
        lsrs.add(createLSR(Active, LSRScore, LSRFieldName, LSROperator, LSRValue, 1));    
        insert lsrs;
        
        Test.StartTest();
        leads=createLeads(numLeads,LN,Company,HasOptedOutOfEmail, LeadSource, HasOptedOutOfFax,AnnualRev);
        AnnualRev=998;
        List<Lead> leads2=createLeads(numLeads,LN,Company,HasOptedOutOfEmail, LeadSource, HasOptedOutOfFax,AnnualRev);
        Test.StopTest();

        Lead l=[Select Id, Company,Lead_Score__c FROM Lead WHERE Id=:leads[0].Id LIMIT 1];
        system.assertEquals(l.Lead_Score__c,LSRScore);
        l=[Select Id, Lead_Score__c FROM Lead WHERE Id=:leads2[0].Id LIMIT 1];
        system.assertNOTEquals(l.Lead_Score__c,LSRScore);
    }//verifyLeadDoubleGreaterThan

     static testMethod void verifyLeadDoubleGreaterorEqual(){
        Long numLeads=1;
        List<Lead> leads = new List<lead>();
        Boolean Active=True;
        
        Double LSRScore=1;
        
        String LSRFieldName='AnnualRevenue';
        String LSROperator='Greater or Equal';
        String LSRValue='1000';

        List<Lead_Score__c> lsrs=new List<Lead_Score__c>();
        
        String LN='TestLead';
        String Company='Company';
        Boolean HasOptedOutOfEmail=TRUE;
        String LeadSource='Web';
        Boolean HasOptedOutOfFax=FALSE;
        String CampaignName='';
        Double AnnualRev=1000;

        cleanUpTestData();
        
        lsrs.add(createLSR(Active, LSRScore, LSRFieldName, LSROperator, LSRValue, 1));    
        insert lsrs; 
        
        Test.StartTest();
        leads=createLeads(numLeads,LN,Company,HasOptedOutOfEmail, LeadSource, HasOptedOutOfFax,AnnualRev);
        AnnualRev=999;
        LS_LeadScoringEngine.setEvaluateLeadsFlag(FALSE);
        List<Lead> leads2=createLeads(numLeads,LN,Company,HasOptedOutOfEmail, LeadSource, HasOptedOutOfFax,AnnualRev);
        AnnualRev=1001;
        LS_LeadScoringEngine.setEvaluateLeadsFlag(FALSE);
        List<Lead> leads3=createLeads(numLeads,LN,Company,HasOptedOutOfEmail, LeadSource, HasOptedOutOfFax,AnnualRev);
        Test.StopTest();
        
        Lead l=[Select Id, Company,Lead_Score__c FROM Lead WHERE Id=:leads[0].Id LIMIT 1];
        system.assertEquals(LSRScore,l.Lead_Score__c);

        l=[Select Id, Lead_Score__c FROM Lead WHERE Id=:leads2[0].Id LIMIT 1];
        system.assertNOTEquals(LSRScore,l.Lead_Score__c);
        l=[Select Id, Lead_Score__c FROM Lead WHERE Id=:leads3[0].Id LIMIT 1];
        system.assertEquals(l.Lead_Score__c,LSRScore);
        
    }//verifyLeadDoubleGreaterorEqual

  static testMethod void verifyLeadDoubleLessOrEqual(){
        Long numLeads=1;
        List<Lead> leads = new List<lead>();
        Boolean Active=True;
        
        Double LSRScore=1;
        
        String LSRFieldName='AnnualRevenue';
        String LSROperator='Less Or Equal';
        String LSRValue='1000';

        List<Lead_Score__c> lsrs=new List<Lead_Score__c>();
        
        String LN='TestLead';
        String Company='Company';
        Boolean HasOptedOutOfEmail=TRUE;
        String LeadSource='Web';
        Boolean HasOptedOutOfFax=FALSE;
        String CampaignName='';
        Double AnnualRev=1000;

        cleanUpTestData();
        
        lsrs.add(createLSR(Active, LSRScore, LSRFieldName, LSROperator, LSRValue, 1));    
        insert lsrs;
        
        Test.StartTest();
        leads=createLeads(numLeads,LN,Company,HasOptedOutOfEmail, LeadSource, HasOptedOutOfFax,AnnualRev);
        AnnualRev=1001;
        List<Lead> leads2=createLeads(numLeads,LN,Company,HasOptedOutOfEmail, LeadSource, HasOptedOutOfFax,AnnualRev);
        AnnualRev=999;
        List<Lead> leads3=createLeads(numLeads,LN,Company,HasOptedOutOfEmail, LeadSource, HasOptedOutOfFax,AnnualRev);
        Test.StopTest();

        Lead l=[Select Id, Company,Lead_Score__c FROM Lead WHERE Id=:leads[0].Id LIMIT 1];
        system.assertEquals(l.Lead_Score__c,LSRScore);
        l=[Select Id, Lead_Score__c FROM Lead WHERE Id=:leads2[0].Id LIMIT 1];
        system.assertNOTEquals(l.Lead_Score__c,LSRScore);
        l=[Select Id, Lead_Score__c FROM Lead WHERE Id=:leads3[0].Id LIMIT 1];
        system.assertEquals(l.Lead_Score__c,LSRScore);
    }//verifyLeadDoubleLessOrEqual

  static testMethod void verifyLeadDoubleLessThan(){
        Long numLeads=1;
        List<Lead> leads = new List<lead>();
        Boolean Active=True;
        
        Double LSRScore=1;
        
        String LSRFieldName='AnnualRevenue';
        String LSROperator='Less Than';
        String LSRValue='1001';

        List<Lead_Score__c> lsrs=new List<Lead_Score__c>();
        
        String LN='TestLead';
        String Company='Company';
        Boolean HasOptedOutOfEmail=TRUE;
        String LeadSource='Web';
        Boolean HasOptedOutOfFax=FALSE;
        String CampaignName='';
        Double AnnualRev=1000;

        cleanUpTestData();
        
        lsrs.add(createLSR(Active, LSRScore, LSRFieldName, LSROperator, LSRValue, 1));    
        insert lsrs;
        
        Test.StartTest();
        leads=createLeads(numLeads,LN,Company,HasOptedOutOfEmail, LeadSource, HasOptedOutOfFax,AnnualRev);
        AnnualRev=1002;
        List<Lead> leads2=createLeads(numLeads,LN,Company,HasOptedOutOfEmail, LeadSource, HasOptedOutOfFax,AnnualRev);
        Test.StopTest();

        Lead l=[Select Id, Company,Lead_Score__c FROM Lead WHERE Id=:leads[0].Id LIMIT 1];
        system.assertEquals(l.Lead_Score__c,LSRScore);
        l=[Select Id, Lead_Score__c FROM Lead WHERE Id=:leads2[0].Id LIMIT 1];
        system.assertNOTEquals(l.Lead_Score__c,LSRScore);
    }//verifyLeadDoubleLessThan

  static testMethod void verifyLeadDateEquals(){
        Long numLeads=1;
        List<Lead> leads = new List<lead>();
        Boolean Active=True;
        
        Double LSRScore=1;
        
        String LSRFieldName='CreatedDate';
        String LSROperator='Equals';
        String LSRValue=String.valueOf(DateTime.now());

        List<Lead_Score__c> lsrs=new List<Lead_Score__c>();
        
        String LN='TestLead';
        String Company='Company';
        Boolean HasOptedOutOfEmail=TRUE;
        String LeadSource='Web';
        Boolean HasOptedOutOfFax=FALSE;
        String CampaignName='';
        Double AnnualRev=1000;

        cleanUpTestData();
        
        lsrs.add(createLSR(Active, LSRScore, LSRFieldName, LSROperator, system.today(),system.today().addDays(1), 1));    
        insert lsrs;
        
        Test.StartTest();
        leads=createLeads(numLeads,LN,Company,HasOptedOutOfEmail, LeadSource, HasOptedOutOfFax,AnnualRev);
        Test.StopTest();

        Lead l=[Select Id, Company,Lead_Score__c FROM Lead WHERE Id=:leads[0].Id LIMIT 1];
//        system.assertEquals(l.Lead_Score__c,LSRScore);
//Date criteria don't yet work
        //Negative test in next test method
    }//verifyLeadDateEquals

  static testMethod void verifyLeadDateEqualsNegativeTest(){
        Long numLeads=1;
        List<Lead> leads = new List<lead>();
        Boolean Active=True;
        
        Double LSRScore=1;
        
        String LSRFieldName='CreatedDate';
        String LSROperator='Equals';
        DateTime dateValue=DateTime.now();
        DateTime dateValue2=dateValue.addDays(3);
        String LSRValue=String.valueOf(dateValue2);

        List<Lead_Score__c> lsrs=new List<Lead_Score__c>();
        
        String LN='TestLead';
        String Company='Company';
        Boolean HasOptedOutOfEmail=TRUE;
        String LeadSource='Web';
        Boolean HasOptedOutOfFax=FALSE;
        String CampaignName='';
        Double AnnualRev=1000;

        cleanUpTestData();
        
        lsrs.add(createLSR(Active, LSRScore, LSRFieldName, LSROperator, system.today().addDays(3),system.today().addDays(5), 1));  
        insert lsrs;
        
        Test.StartTest();
        leads=createLeads(numLeads,LN,Company,HasOptedOutOfEmail, LeadSource, HasOptedOutOfFax,AnnualRev);
        Test.StopTest();

        Lead l=[Select Id, CreatedDate,Company,Lead_Score__c FROM Lead WHERE Id=:leads[0].Id LIMIT 1];
        system.debug('Created Date: '+l.CreatedDate+' ; LSRValue : '+LSRValue);
        system.assertNOTEquals(l.Lead_Score__c,LSRScore);
    }//verifyLeadDateEqualsNegativeTest

 static testMethod void verifyLeadDateNotEqualTo(){
        Long numLeads=1;
        List<Lead> leads = new List<lead>();
        Boolean Active=True;
        
        Double LSRScore=1;
        
        String LSRFieldName='CreatedDate';
        String LSROperator='Not Equal To';
        Date dateValue=Date.today();
        Date dateValue2=dateValue.addDays(2);
        String LSRValue=String.valueOf(dateValue2);

        List<Lead_Score__c> lsrs=new List<Lead_Score__c>();
        
        String LN='TestLead';
        String Company='Company';
        Boolean HasOptedOutOfEmail=TRUE;
        String LeadSource='Web';
        Boolean HasOptedOutOfFax=FALSE;
        String CampaignName='';
        Double AnnualRev=1000;

        cleanUpTestData();
        
        lsrs.add(createLSR(Active, LSRScore, LSRFieldName, LSROperator, system.today().addDays(3),system.today().addDays(5), 1));   
        insert lsrs;
        
        Test.StartTest();
        leads=createLeads(numLeads,LN,Company,HasOptedOutOfEmail, LeadSource, HasOptedOutOfFax,AnnualRev);
        Test.StopTest();

        Lead l=[Select Id, Company,Lead_Score__c FROM Lead WHERE Id=:leads[0].Id LIMIT 1];
//        system.assertEquals(l.Lead_Score__c,LSRScore);
//Date criteria don't yet work
        //Negative test in next test method
    }//verifyLeadDateNotEqualTo

  static testMethod void verifyLeadDateNotEqualToNegativeTest(){
        Long numLeads=1;
        List<Lead> leads = new List<lead>();
        Boolean Active=True;
        
        Double LSRScore=1;
        
        String LSRFieldName='CreatedDate';
        String LSROperator='Not Equal To';
        String LSRValue=String.valueOf(Date.today());

        List<Lead_Score__c> lsrs=new List<Lead_Score__c>();
        
        String LN='TestLead';
        String Company='Company';
        Boolean HasOptedOutOfEmail=TRUE;
        String LeadSource='Web';
        Boolean HasOptedOutOfFax=FALSE;
        String CampaignName='';
        Double AnnualRev=1000;

        cleanUpTestData();
        
        lsrs.add(createLSR(Active, LSRScore, LSRFieldName, LSROperator, system.today(),system.today().addDays(1), 1)); 
        insert lsrs;
        
        Test.StartTest();
        leads=createLeads(numLeads,LN,Company,HasOptedOutOfEmail, LeadSource, HasOptedOutOfFax,AnnualRev);
        Test.StopTest();

        Lead l=[Select Id, Company,Lead_Score__c FROM Lead WHERE Id=:leads[0].Id LIMIT 1];
        system.assertEquals(l.Lead_Score__c,LSRScore);
    }//verifyLeadDateNotEqualToNegativeTest

  static testMethod void verifyDupeRulesDoesntGack(){
        Long numLeads=1;
        List<Lead> leads = new List<lead>();
        Boolean Active=True;
        
        Double LSRScore=1;
        
        String LSRFieldName='Company';
        String LSROperator='Equals';
        String LSRValue='Company';

        List<Lead_Score__c> lsrs=new List<Lead_Score__c>();
        
        String LN='TestLead';
        String Company='Company';
        Boolean HasOptedOutOfEmail=TRUE;
        String LeadSource='Web';
        Boolean HasOptedOutOfFax=FALSE;
        String CampaignName='';
        Double AnnualRev=1000;

        cleanUpTestData();
        
        lsrs.add(createLSR(Active, LSRScore, LSRFieldName, LSROperator, LSRValue, 1));    
        lsrs.add(createLSR(Active, LSRScore, LSRFieldName, LSROperator, LSRValue, 2));    
        insert lsrs;
        
        Test.StartTest();
        leads=createLeads(numLeads,LN,Company,HasOptedOutOfEmail, LeadSource, HasOptedOutOfFax,AnnualRev);
        Test.StopTest();

        Lead l=[Select Id, Company,Lead_Score__c FROM Lead WHERE Id=:leads[0].Id LIMIT 1];
        system.assertEquals(l.Lead_Score__c,LSRScore*2);
    }//verifyDupeRulesDoesntGack


    public static testMethod void testLS_LeadScoringEngineRuleEdit() {
        
        //Use the PageReference Apex class to instantiate a page
        PageReference pageRef = Page.LS_LeadScoringEngineRuleEdit;
        
        //In this case, the Visualforce page named 'success' is the starting point of this test method. 
        Test.setCurrentPage(pageRef);
        Lead_Score__c lsr=new Lead_Score__c();
        lsr.Field_Name__c='Status';//this will run the checkbox check
        lsr.Score__c=5;
        lsr.Operator__c='Equals';
        lsr.Value__c='True';
        lsr.IsActive__c=True;
        insert lsr;
        LS_LeadScoringEngineRuleEdit scExt = new LS_LeadScoringEngineRuleEdit();
        scExt.objLeadScore.Field_Name__c ='Company';
        scExt.objLeadScore.Score__c=5;
        scExt.objLeadScore.Operator__c='Equals';
        scExt.objLeadScore.Value__c='True';
        scExt.objLeadScore.IsActive__c=True;
        scExt.valueRender();
        scExt.addLeadScore();
        scExt.objLeadScore.Field_Name__c ='Status';
        scExt.objLeadScore.Score__c=5;
        scExt.objLeadScore.Operator__c='Equals';
        scExt.objLeadScore.Value__c='Open';
        scExt.objLeadScore.IsActive__c=True;
        scExt.valueRender();
        scExt.addLeadScore();
        scExt.quickSaveRules();
        
        LS_LeadScoringEngineRuleEdit scExt2 = new LS_LeadScoringEngineRuleEdit();
        scExt2.objLeadScore.Field_Name__c='CreatedDate';//this will run the checkbox check
        scExt2.objLeadScore.Score__c=5;
        scExt2.objLeadScore.Operator__c='Equals';
        scExt2.objLeadScore.Start_Date__c=system.today();
        scExt2.objLeadScore.End_Date__c=system.today();
        scExt2.objLeadScore.IsActive__c=True;
        scExt2.valueRender();
        scExt2.dateSelector();
        scExt2.addLeadScore();
        scExt2.objLeadScore.Field_Name__c='Advertising_Interest__c';//this will run the checkbox check
        scExt2.objLeadScore.Score__c=5;
        scExt2.objLeadScore.Operator__c='Equals';
        scExt2.objLeadScore.Value__c = 'Online';
        scExt2.objLeadScore.IsActive__c=True;
        scExt2.valueRender();
        scExt2.addLeadScore();
        scExt2.quickSaveRules();//test no exception upon saving & creating a new record
                        //Also having active=false by default should ensure the limits trigger doesn't gack if active = false.
                        //Note due to Apex test deficiencies the limit can't be tested directly (limited to 100 record inserts in 1 method)
        ApexPages.currentPage().getParameters().put('ls_index','1');
        scExt2.editLeadScore();
        scExt2.saveLeadScore();
        scExt2.deleteLeadScore();
        scExt2.saveRules();
    }//testLS_LeadScoringEngineRuleEdit

  public static testMethod void testLS_LeadScoringInitialize () {//tests the batch apex method for campaign members
        if(runBatchTests==True){//if 1
            Long NumLeads=10;//test methods can't test more than ~100 records inserted, so no point in even trying as the app scales over 200 lead rules
            Long NumLeadRules=10;
            
            List<Lead_Score__c> lsrs=new List<Lead_Score__c>();
            cleanUpTestData();
            
            List<Lead> leads=createLeads(NumLeads, 'Lead', 'Company', False, 'Web', False,1000);
      
            for (Integer i=0;i<NumLeadRules;i++){
                lsrs.add(createLSR(TRUE, 1, 'Company', 'equals', 'Company',1));
            }//for 1    
    
            insert lsrs;
            
            Set<Id> leadId = new Set<Id>();
            for ( Lead l : leads )
            {
                leadId.add(l.Id);
            }
    
            
            Test.StartTest(); 
                LS_LeadScoringInitialize.autoRunLeads(10, false, leadId);
            Test.StopTest();
                
            Lead l=[Select Id, Lead_Score__c FROM Lead WHERE Id=:leads[0].Id LIMIT 1];
                
             System.AssertEquals(NumLeadRules,l.Lead_Score__c);
        }//if 1
   }//testLS_LeadScoringInitialize
   
   public static testMethod void testLS_LeadScoringInitialize2 () {//tests the batch apex method for campaign members
        if(runBatchTests==True){//if 1
            Long NumLeads=10;//test methods can't test more than ~100 records inserted, so no point in even trying as the app scales over 200 lead rules
            Long NumLeadRules=10;
            
            List<Lead_Score__c> lsrs=new List<Lead_Score__c>();
            cleanUpTestData();
            
            List<Lead> leads=createLeads(NumLeads, 'Lead', 'Company', False, 'Web', False,1000);
      
            for (Integer i=0;i<NumLeadRules;i++){
                lsrs.add(createLSR(TRUE, 1, 'Company', 'equals', 'Company',1));
            }//for 1    
    
            insert lsrs;
            
            Set<Id> leadId = new Set<Id>();
            for ( Lead l : leads )
            {
                leadId.add(l.Id);
            }
    
            
            Test.StartTest(); 
                LS_LeadScoringInitialize.evaluateLeadsAsync(leadId);
            Test.StopTest();
                
            Lead l=[Select Id, Lead_Score__c FROM Lead WHERE Id=:leads[0].Id LIMIT 1];
                
             System.AssertEquals(NumLeadRules,l.Lead_Score__c);
        }//if 1
   }//testLS_LeadScoringInitialize2

 */  
    
}//LeadScoringTests