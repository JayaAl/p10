global class LeadConversionBatch implements Database.Batchable<sObject> , Database.Stateful{  

    global List<LeadDuplicateMapCtrlr.leadConvertWrapper> LeadWrapperToConvert ;  // Start Method         
    global Map<Id,LeadDuplicateMapCtrlr.leadConvertWrapper> LeadWraperMap ;
    global Map<Id,String> successMap;
    global Map<Id,String> errorMap;
    global Integer total_Values_cnt;

    global Database.QueryLocator start(Database.BatchableContext BC){      
        
        system.debug(' ******* START METHOD ******** '+LeadWrapperToConvert);       
        Set<Id> leadIds = new Set<Id>();
        total_Values_cnt = 0;

        LeadWraperMap = new Map<Id,LeadDuplicateMapCtrlr.leadConvertWrapper>();
        if(successMap == null)
        successMap = new Map<Id,String>();
        
        if(errorMap == null)
        errorMap = new Map<Id,String>();
        
        for(LeadDuplicateMapCtrlr.leadConvertWrapper wrapperVar : LeadWrapperToConvert){        
            leadIds.add(wrapperVar.LeadId);
            LeadWraperMap.put(wrapperVar.LeadId,wrapperVar);        
        }
        
        system.debug(' ******* START METHOD END ******** ');
        system.debug('LeadIds'+leadIds);
        system.debug('LeadWraperMap ::'+LeadWraperMap);
        
        total_Values_cnt = leadIds.size();

        return Database.getQueryLocator('SELECT Id,Name FROM Lead WHERE Id in :leadIds ');         
    } 
            
    global void execute(Database.BatchableContext BC, list<sObject> leadsToProcess){
        system.debug('******** EXECUTE BEGINS *************');
        system.debug('Leads to Process ::: '+leadsToProcess);
        system.debug('Leads Map : '+LeadWraperMap);        
        
        LeadStatus convertStatus = [SELECT Id, MasterLabel FROM LeadStatus WHERE IsConverted=true LIMIT 1];
        List<Database.LeadConvert> convertLst = new List<Database.LeadConvert>();
        
        for(Lead LeadVar : (List<Lead>)leadsToProcess){    
            if(LeadWraperMap.containsKey(LeadVar.id)){
                LeadDuplicateMapCtrlr.leadConvertWrapper wrapperVar = LeadWraperMap.get(LeadVar.id);
                Database.LeadConvert lc = new Database.LeadConvert();
                lc.setLeadId(LeadVar.Id);
                lc.setAccountId(wrapperVar.AccountId);
                lc.setOwnerId(wrapperVar.userName);
                lc.setDoNotCreateOpportunity(true);            
                lc.setConvertedStatus(convertStatus.MasterLabel);
                lc.setSendNotificationEmail(true);
                convertLst.add(lc);
            }
        }

        String returnStr ;
        
        system.debug('Lead conversion List'+convertLst);
        
        List<Database.LeadConvertResult> lcr = Database.convertLead(convertLst,false);
        Boolean isError = false;
        String errorMsg = '';
        system.debug('Lead Conversion Result'+lcr);
        
        for(Database.LeadConvertResult res : lcr ){
            if(!res.isSuccess()){
                isError = true;
                errorMsg += ('\n'+res.getErrors());
                errorMap.put(res.getLeadId(),String.ValueOf(res.getErrors()));
            }else{          
                successMap.put(res.getLeadId(),'Created Contact : '+res.getContactId()+' for Account '+res.getAccountId() );
                system.debug('Result :::: '+res.isSuccess());      
            }              
        }
        system.debug('Success :'+successMap);
        system.debug('Error :'+errorMap);
        system.debug('******** EXECUTE ENDS *************');
        
    }
    
    global void finish(Database.BatchableContext BC){
      system.debug('******** FINISH BEGINS *************');      
      
      AsyncApexJob a = [SELECT Id, Status, NumberOfErrors, JobItemsProcessed,
          TotalJobItems, CreatedBy.Email
          FROM AsyncApexJob WHERE Id =
          :BC.getJobId()];
      
       String senderDispName = 'Salesforce Admin';
       String HTMLBodyStr = ''; 
       HTMLBodyStr += 'Hello, ';
       HTMLBodyStr += '<p> ';
       HTMLBodyStr += 'The Lead conversion batch job processed ';
       HTMLBodyStr += ' '+total_Values_cnt+' records in ' + a.TotalJobItems +' batches'; 
        HTMLBodyStr += '</p> ';
        if(successMap.keySet().size() != 0)
        HTMLBodyStr += 'SUCCESS ::: ['+successMap.keySet().size()+' Records ] <br/>';
        
        HTMLBodyStr += '<table> ';
            for(String idVar : successMap.keySet()){
                HTMLBodyStr +=  '<tr>';
                    HTMLBodyStr +=  '<td>';
                            HTMLBodyStr += idVar;
                    HTMLBodyStr +=  '</td>';
                    
                    HTMLBodyStr +=  '<td>';
                            HTMLBodyStr += (successMap.get(idVar));
                    HTMLBodyStr +=  '</td>';
                    
                HTMLBodyStr +=  '</tr>';
            }
        HTMLBodyStr += '</table> ';
        if(errorMap.keySet().size() != 0)
        HTMLBodyStr += '<br/> ERROR ::: ['+errorMap.keySet().size()+' Records ] <br/>';
        HTMLBodyStr += '<table> ';
            for(String idVar : errorMap.keySet()){
                HTMLBodyStr +=  '<tr>';
                    HTMLBodyStr +=  '<td>';
                            HTMLBodyStr += idVar;
                    HTMLBodyStr +=  '</td>';
                    
                    HTMLBodyStr +=  '<td>';
                            HTMLBodyStr += (errorMap.get(idVar));
                    HTMLBodyStr +=  '</td>';
                    
                HTMLBodyStr +=  '</tr>';
            }
        HTMLBodyStr += '</table> ';
                
        HTMLBodyStr += '<br/>';
        
        HTMLBodyStr += 'Thank you! <br/> ';

        EmailUtil.sendEmail( new List<Messaging.SingleEmailMessage> { EmailUtil.generateEmail(senderDispName,HTMLBodyStr,'Lead Conversion status ' + a.Status,new List<String>{'abidap@pandora.com',a.CreatedBy.Email},null)});
       system.debug('******** FINISH ENDS *************');
    }
    
}