global class LeadDuplicateMapCtrlr{
    
    public string nameFile{get;set;}
    public Blob contentFile{get;set;}
    String[] filelines = new String[]{};
    public List<leadConvertWrapper> wrapperLst {get;set;}
    public List<leadConvertWrapper> InvalidWrapperLst {get;set;}
    private static Map<String,User> userNameMap;    
    private static Map<Id,Account> acctIdMap;
    

    public void ReadFile(){
        try{
        nameFile=contentFile.toString();
        filelines = nameFile.split('\n');
        wrapperLst = new List<leadConvertWrapper>();
        InvalidWrapperLst = new List<leadConvertWrapper>();
        Set<String> userNameLst = new Set<String>();
        Set<String> acctNameLst = new Set<String>();
        Set<String> contactOwnerNameLst = new Set<String>();
        Map<String,Contact> contactMap = new Map<String,Contact>();

        for (Integer i=1;i<filelines.size();i++){
            String[] inputvalues = new String[]{};
            inputvalues = filelines[i].split(',');             
            acctNameLst.add(inputvalues[2].normalizeSpace().trim());
            contactOwnerNameLst.add(inputvalues[3].normalizeSpace().trim());
        }
        
        system.debug('userNameLst ::::'+userNameLst);
        
        
        system.debug('AcctNameLst ::::'+acctNameLst);
        Set<Id> ownerIdSet = new Set<Id>();
        
        if(acctIdMap == null){

            acctIdMap = new Map<Id,Account> ();
            for(Account userObj : [Select id,name,OwnerId from Account where Id IN : acctNameLst ]){
                
                acctIdMap.put(userObj.Id,userObj);
                ownerIdSet.add(userObj.OwnerId);
             }    
        }
        
        

        system.debug('acctIdMap ::::'+acctIdMap);
        
        system.debug('ownerIdSet ::::: '+ownerIdSet);
        if(userNameMap == null){

            userNameMap = new Map<String,User> ();
            for(User userObj : [Select id,name from User where Name IN : contactOwnerNameLst ]){
                userNameMap.put(userObj.name,userObj);
            }    
        }
        
        system.debug('userNameMap ::::'+userNameMap);

       Integer cnt = filelines.size() > 1000 ? 1000 : filelines.size();
     
        for (Integer i=1;i<=(cnt-1);i++)
        {
            system.debug('inside for loop') ;
            String[] inputvalues = new String[]{};
            inputvalues = filelines[i].split(',');            
            system.debug(' 0 => '+inputvalues[0]+'1 => '+inputvalues[1]+'2 =>'+inputvalues[2]);            
            
            if((inputvalues[1] != null && inputvalues[1] != '' ) && (inputvalues[2].normalizeSpace().trim() != null ) && userNameMap.containsKey((inputvalues[3].normalizeSpace().trim())) )
                wrapperLst.add(new leadConvertWrapper(inputvalues[0].normalizeSpace().trim(),(inputvalues[2].normalizeSpace().trim()),userNameMap.get((inputvalues[3].normalizeSpace().trim())).Id));    
            else    
                InvalidWrapperLst.add(new leadConvertWrapper(inputvalues[0].normalizeSpace().trim(),'',''));    
            
            
        }
         
        }catch(Exception ex){

            ApexPages.addMessage(new ApexPages.message(ApexPages.severity.ERROR,ex.getMessage()));      
        } 
        
          
    }

    
    public void invokeConversionBatchAction(){

        LeadConversionBatch batchInstance = new LeadConversionBatch();
        batchInstance.LeadWrapperToConvert = wrapperLst;
        Database.executeBatch(batchInstance,10);
        ApexPages.addMessage(new ApexPages.message(ApexPages.severity.INFO,'Lead Conversion batch in Progress , You will be notified with the result soon.'));  
    }
  
    

    global class leadConvertWrapper{
        global String LeadId {get;set;}
        global String AccountId {get;set;}
        global String userName {get;set;}
        global boolean isSelected {get;set;}

        global leadConvertWrapper(String LeadIdStr, String AccountIdStr , String userNameStr){
            LeadId = LeadIdStr;
            AccountId = AccountIdStr;
            userName = userNameStr;
            isSelected = false;
        }
    }
}