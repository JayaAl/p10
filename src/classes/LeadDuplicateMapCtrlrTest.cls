@isTest(seeAlldata=false)
public class LeadDuplicateMapCtrlrTest{
    static Lead leadObj;
    static Account acctObj;
    static Contact contactObj;
    public static void setupData(){
        
        leadObj = new Lead(lastName = 'dddddddccc', company = 'PSL');        
        insert leadObj;
        
        acctObj = Util_testUtil.generateAccount();        
        insert acctObj ;
        
       
        
        
        
    }
    
    public static testmethod void LeadDuplicateMapCtrlrTest(){
        
        setupData();
        Test.startTest();
        LeadDuplicateMapCtrlr ctrlr = new LeadDuplicateMapCtrlr();
        ctrlr.nameFile = 'Test File';
        String body = 'Lead/Contact ID,Company / Account,Account ID,Contact Owner\n'+leadObj.Id+','+acctObj.Name+','+acctObj.Id+',Tom Marshall';
        ctrlr.contentFile = Blob.valueOf(body);
        ctrlr.ReadFile();
        ctrlr.invokeConversionBatchAction();
        Test.stopTest();
        
    }

}