/**
* ─────────────────────────────────────────────────────────────────────────────────────────────────┐
* This class services lightning action  and services for Lead.
* ──────────────────────────────────────────────────────────────────────────────────────────────────
* @author         Jaya Alaparthi
* @maintainedBy   Jaya Alaparthi
* @version        1.0
* @created        2017-06-01
* @modified       
* @systemLayer    Service
* @see            ????
* @see            ????
* ──────────────────────────────────────────────────────────────────────────────────────────────────
* @changes
* vX.X            Jaya Alaparthi
* YYYY-MM-DD      Each change to this file should be documented by incrementing the version number,
*                 and adding a new entry to this @changes list. Note that there is a single blank
*                 line between each @changes entry.
* ─────────────────────────────────────────────────────────────────────────────────────────────────┘
*/
public with sharing class LeadLightningActions {
	
	//─────────────────────────────────────────────────────────────────────────┐
	// doConvert: convert lead
	// @param Id  lead Id
	// @return String account Id
    //─────────────────────────────────────────────────────────────────────────┘
	@AuraEnabled
	public static String doConvert(Id leadId, Boolean opptyCreate,
									Id accountId) {

		String respStr = saveLead(leadId);
		if(!respStr.startsWithIgnoreCase('Error')) {

			Database.LeadConvert leadConvert = new Database.LeadConvert();
            leadConvert.setLeadId(leadId);
			LeadStatus Leads= [SELECT Id, MasterLabel 
			 					FROM LeadStatus 
			 					WHERE IsConverted=true LIMIT 1];
            leadConvert.setConvertedStatus(Leads.MasterLabel);
            leadConvert.setDoNotCreateOpportunity(opptyCreate); 
            if(accountId != null || accountId != '') {
            	leadConvert.setAccountId(accountId);
            }
            try {
            	Database.LeadConvertResult leadConvertResult = Database.convertLead(leadConvert);
            	respStr = leadConvertResult.getAccountId();
            } catch (Exception e) {

            	respStr = e.getMessage();
            }
		}
		return respStr;
	}
		//─────────────────────────────────────────────────────────────────────────┐
	// doConvert: convert lead
	// @param Id  lead Id
	// @return String account Id
    //─────────────────────────────────────────────────────────────────────────┘
	@AuraEnabled
	public static Lead getLead(Id leadId) {

		Lead leadRec = new Lead();
		leadRec = [SELECT Id,Status,Company,OwnerId,Owner.Name,
						Cancel_Workflow__c
					FROM Lead
					WHERE Id =: leadId];

		return leadRec;
	}
	//─────────────────────────────────────────────────────────────────────────┐
	// saveLead: save lead with cancle wf = true
	// @param Id  lead Id
	// @return String status
    //─────────────────────────────────────────────────────────────────────────┘
    @AuraEnabled
    public static String saveLead(Id leadId) {

    	String respStr = '';
    	Lead leadRec = new Lead(Id =  leadId,
    							Cancel_Workflow__c = true);
    	try {
    		update leadRec;
    		respStr = leadRec.Id;
    	} catch(Exception e) {
    		respStr = 'Error:'+e.getMessage();
    	}
    	return respStr;
    }
    @AuraEnabled
	public static list<String> statusList() {

		List<String> leadStatusList = new List<String>();
		List<Schema.Picklistentry> picklistValues =  Schema.sObjectType.Lead.fields.Status.getPicklistValues();

		 for(Schema.Picklistentry entry : picklistValues) {

		 	leadStatusList.add(entry.getValue());
		 }
		 return leadStatusList;
	}
}