/**
* ─────────────────────────────────────────────────────────────────────────────────────────────────┐
* This class handles the logc for moving a lead to Sharktank for 60 days and 
* this cannot be handled by Process builder as process builder does not support bulk data load.
*
* 3 days. Its a replacement for the following WF rules
* 45 days:[based on Last Modified Date : chagne Lead Owner to Shark Tank; Days Bucket = 60;]
* (Lead: ConvertedEQUALSFalse) AND 
* (Lead: Pre Convert CheckEQUALSFalse) AND 
* (Lead: Lead StatusNOT EQUAL TOUnqualified) AND 
* (Current User: ProfileDOES NOT CONTAINRevana) AND 
* (Lead: Lead OwnerNOT EQUAL TOLeads - Deletion Queue,Cold - No Contact,Duplicate Leads,
* 	Unqualified Leads,Unassigned Queue,Out of Scope,Opt Out Queue,Self-Service) AND 
* (Lead: Do not routeEQUALSFalse) AND 
* (Lead: Lead Record TypeNOT EQUAL TOMarketing Leads) AND 
* (Lead: Sharktank ExclusionEQUALSFalse)
* 
* 
* Activity tracker: [based on Custom Tracker Date : change Lead Owner to Shark Tank; Days Bucket  =  3;]
* (Lead: Lead StatusEQUALSOpen) AND 
* (Lead: Pre Convert CheckEQUALSFalse) AND 
* (Current User: ProfileDOES NOT CONTAINRevana) AND 
* (Lead: Lead OwnerNOT EQUAL TOLeads - Deletion Queue,Cold - No Contact,Duplicate Leads,
*  	Unqualified Leads,Unassigned Queue,Out of Scope,Opt Out Queue,Self-Service) AND 
* (Lead: Do not routeEQUALSFalse) AND 
* (Lead: Lead Record TypeNOT EQUAL TOMarketing Leads,Self-Service)
* This class is referenced in LeadTriggerHandler for handling shark tank 3 days rule's 
* custom date field update
* and leadSharkTankbatchjob class for handling 3 days and 60 days update.
* Custom Settings:
* Lead To Sharktank
* ──────────────────────────────────────────────────────────────────────────────────────────────────
* @author         Jaya Alaparthi
* @maintainedBy   Jaya Alaparthi
* @version        1.0
* @created        2017-06-12
* @modified       
* @systemLayer    Service
* @see            ????
* @see            ????
* ──────────────────────────────────────────────────────────────────────────────────────────────────
* @changes
* vX.X            Jaya Alaparthi
* YYYY-MM-DD      Each change to this file should be documented by incrementing the version number,
*                 and adding a new entry to this @changes list. Note that there is a single blank
*                 line between each @changes entry.
* ─────────────────────────────────────────────────────────────────────────────────────────────────┘
*/
public with sharing class LeadSharkTankQueue {
	
	public static Boolean IS_Batch_Triggered = false;
	public static String OPEN_LEAD = 'Open';
	public static String UNQUALIFIED = 'Unqualified';
	public static String MARKETING_LEADS = 'Marketing Leads';
	public static String SELF_SERVICE_LEADS = 'Self-Service';
	public static String REVANA_PROFILE = '%Revana%';
	public static List<String> LEAD_QUEUE = new List<String>{'Leads - Deletion Queue',
															'Cold - No Contact',
	 					'Duplicate Leads','Unqualified Leads','Unassigned Queue',
						'Out of Scope','Opt Out Queue','Self-Service'};
	//─────────────────────────────────────────────────────────────────────────┐
	// customTrackerUpdateTriggered: update Custom Tracker date field on 
	//								Qualified leads. Is used only in trigger
	// @param leadList  list of lead records
	// @return leadQualifiedList : List of qualified leads for updating custom tracker
    //─────────────────────────────────────────────────────────────────────────┘
    public static void customTrackerUpdateTriggered(List<Lead> leadsToValidate) {

    	List<Lead> customTrackerUpdateList = new List<Lead>();
    	List<Lead> validatedLeads = new List<Lead>();
    	if(!leadsToValidate.isEmpty() && !IS_Batch_Triggered) {

    		validatedLeads = leadActivityTrackerValidation(leadsToValidate,System.Trigger.isExecuting);
    		for(Lead leadRec: validatedLeads) {

    			/*customTrackerUpdateList.add(new Lead(Id = leadRec.Id,
    										Custom_Tracker__c = Datetime.now()));
    			*/
    			System.debug('customTrackerUpdate:'+leadRec.Custom_Tracker__c);
    			leadRec.Custom_Tracker__c = Datetime.now();
    			System.debug('customTrackerUpdate:'+leadRec.Custom_Tracker__c);
    		}
    	}
    	//return customTrackerUpdateList;
    }
    //─────────────────────────────────────────────────────────────────────────┐
	// leadActivityToSharktank: validate if the give lead is qualified 
	// 									for this update.
	// @param leadsToValidate  list of lead records
	// @return 
    //─────────────────────────────────────────────────────────────────────────┘
    public static List<Lead> leadActivityToSharktank(List<Lead> leadsToValidate) {

    	List<Lead> validateActivityShark = new List<Lead>();
    	List<Lead> leadsToSharktank = new List<Lead>();

    	// get no of days for the lead to be in the bucket.
    	Lead_To_Sharktank__c openLead = Lead_To_Sharktank__c.getValues('OpenLeads');
    	// validate leads for Lead Activity tracker WF rules.
    	validateActivityShark = leadActivityTrackerValidation(leadsToValidate,false);

    	if(!validateActivityShark.isEmpty()) {

	    	for(Lead leadRec : validateActivityShark) {

	    		if(leadRec.Custom_Tracker__c  != null 
	    			&& getDaysDifference(leadRec.Custom_Tracker__c) >= openLead.No_Of_Days__c) {
	    			System.debug('leadRec:'+leadRec);
					leadsToSharktank.add(new Lead(Id = leadRec.Id,
									OwnerId = openLead.Owner__c,
									Days_Bucket__c = String.valueOf(openLead.No_Of_Days__c)));
				}
				System.debug('a4ter leadsToSharktank:'+leadsToSharktank);
	    	}
	    }
	    return leadsToSharktank;
    }
	//─────────────────────────────────────────────────────────────────────────┐
	// leadActivityTrackerValidation: validate if the give lead is qualified 
	// 									for this updation.
	// @param leadsToValidate  list of lead records
	// @param isTrigger
	// @return 
    //─────────────────────────────────────────────────────────────────────────┘
    public static List<Lead> leadActivityTrackerValidation(List<Lead> leadsToValidate,
    														Boolean isTrigger) {

    	List<Lead> listOfLeadsForShark = new list<Lead>();
    	List<Lead> leadsNotOwnedByLeadQueue = new List<Lead>();
    	Map<Id,Profile> profileIdMap = new Map<Id,Profile>();

    	
    	profileIdMap = getAllRevanaProfiles();
    	// If the instance is trigger then the current user profile should not 
    	// match to keys in profileIdMap
    	// If its batch no such validation is needed.
    	if((isTrigger  && (profileIdMap.isEmpty() || 
    						(!profileIdMap.isEmpty() && 
    						!profileIdMap.containsKey(UserInfo.getProfileId()))))
    		|| !isTrigger) {

    		// Lead Owner validation is only needed if the instance is from a trigger.
    		// In case of batch this needs to be done as a batch query.
    		if(isTrigger) {

	    		leadsNotOwnedByLeadQueue = leadsNotOwnedBy(leadsToValidate);
	    	} else {
	    		leadsNotOwnedByLeadQueue = leadsToValidate;
	    	}


	    	for(Lead leadRec : leadsNotOwnedByLeadQueue) {

	    		if(leadRec.Status != null 
	    			&& leadRec.Status.equalsIgnoreCase(OPEN_LEAD)
	    			&& !leadRec.Do_not_route__c
	    			&& !isValidLeadRecordType(leadRec,MARKETING_LEADS)
	    			&& !isValidLeadRecordType(leadRec,SELF_SERVICE_LEADS)) {

	    				listOfLeadsForShark.add(leadRec);
	    		}
	    	}
	    }
	    return listOfLeadsForShark;
    }
    
    //─────────────────────────────────────────────────────────────────────────┐
	// sharkTankSixtyDay: validate leads for 60 day rule [Lead_To_Sharktank__c]
	// @param leadList  list of lead records
	// @return 
    //─────────────────────────────────────────────────────────────────────────┘
    public static List<Lead> sharkTankSixtyDay(List<Lead> leadsToValidate) {

    	List<Lead> listOfLeadsForShark = new list<Lead>();
    	List<Lead> leadsNotOwnedByLeadQueue = new List<Lead>();
    	Map<Id,Profile> profileIdMap = new Map<Id,Profile>();

    	// get no of days for the lead to be in the bucket.
    	Lead_To_Sharktank__c nonOpenLead = Lead_To_Sharktank__c.getValues('NONOpenLeads');

    	profileIdMap = getAllRevanaProfiles();
    	if((!profileIdMap.isEmpty() && 
    		!profileIdMap.containsKey(UserInfo.getProfileId()))
    		|| profileIdMap.isEmpty()) {

	    	leadsNotOwnedByLeadQueue = leadsNotOwnedBy(leadsToValidate);

	    	for(Lead leadRec : leadsNotOwnedByLeadQueue) {
				// adding self services condition before couple of hours to deployment
	    		// so i am not making this as one method. 
	    		if(!leadRec.Status.equalsIgnoreCase(UNQUALIFIED)
	    			&& !leadRec.Do_not_route__c
	    			&& !leadRec.Sharktank_Exclusion__c
	    			&& !isValidLeadRecordType(leadRec,MARKETING_LEADS)
                  	&& !isValidLeadRecordType(leadRec,SELF_SERVICE_LEADS)) {

	    				if(getDaysDifference(leadRec.LastModifiedDate) >= nonOpenLead.No_Of_Days__c) {

	    					listOfLeadsForShark.add(new Lead(Id = leadRec.Id,
	    									OwnerId = nonOpenLead.Owner__c,
	    									Days_Bucket__c = String.valueOf(nonOpenLead.No_Of_Days__c)));
	    				}
	    		}
	    	}
	    }
	    return listOfLeadsForShark;
    }
    //─────────────────────────────────────────────────────────────────────────┐
	// isValidLeadRecordType: checks if the lead recordType is
	//					based on the parameter passed
	// @param 
	// @return 
    //─────────────────────────────────────────────────────────────────────────┘
    public static Map<Id,QueueSobject> getOwnerIds() {

    	Map<Id,QueueSobject> leadOwnerNameIdMap = new Map<Id,QueueSobject>();

    	for(QueueSobject queue : [SELECT Id,QueueId,Queue.Name,SobjectType 
    									FROM QueueSobject 
    									WHERE SobjectType = 'Lead'
    									AND Queue.Name IN: LEAD_QUEUE]) {

    		leadOwnerNameIdMap.put(queue.QueueId,queue);
    	}
    	return leadOwnerNameIdMap;
    }
    //─────────────────────────────────────────────────────────────────────────┐
	// leadsNotOwnedBy: checks if the lead owner is
	//					Leads - Deletion Queue,Cold - No Contact,
	// 					Duplicate Leads,Unqualified Leads,Unassigned Queue,
	//					Out of Scope,Opt Out Queue,Self-Service
	// @param 
	// @return 
    //─────────────────────────────────────────────────────────────────────────┘
    private static List<Lead> leadsNotOwnedBy(List<Lead> leadsToValidate) {

    	Boolean isOwnedBy = false;
    	set<Id> leadOwnerIds = new set<Id>();
    	Map<Id,QueueSobject> leadOwnerNameIdMap = new Map<Id,QueueSobject>();
    	List<Lead> leadsNotOwnedByLeadQueue = new List<Lead>();

    	for(Lead lead : leadsToValidate) {

    		leadOwnerIds.add(lead.OwnerId);
    	}
    	leadOwnerNameIdMap = getOwnerIds();
    	System.debug('leadOwnerNameIdMap:'+leadOwnerNameIdMap);
    	for(Lead lead : leadsToValidate) {
    		System.debug('lead.OwnerId:'+lead.OwnerId);
    		if((!leadOwnerNameIdMap.IsEmpty() 
    			&& !leadOwnerNameIdMap.containsKey(lead.OwnerId))
    		|| leadOwnerNameIdMap.isEmpty()) {

    			leadsNotOwnedByLeadQueue.add(lead);
    		}
    	}
    	System.debug('leadsNotOwnedByLeadQueue:'+leadsNotOwnedByLeadQueue);
    	return leadsNotOwnedByLeadQueue;
    } 
    //─────────────────────────────────────────────────────────────────────────┐
	// isValidLeadRecordType: checks if the lead recordType is
	//					based on the parameter passed
	// @param 
	// @return 
    //─────────────────────────────────────────────────────────────────────────┘
    private static boolean isValidLeadRecordType(Lead lead, String leadRecordType) {

    	Id leadRecordId = Schema.getGlobalDescribe().get('Lead').getDescribe()
                            .getRecordTypeInfosByName().get(leadRecordType)
                            .getRecordTypeId();
        if(leadRecordId == lead.RecordTypeId) {

        	return true;
        } else {

        	return false;
        }
    }
    //─────────────────────────────────────────────────────────────────────────┐
	// getAllRevanaProfiles: checks if the current lead is Revana
	// @param 
	// @return 
    //─────────────────────────────────────────────────────────────────────────┘
    private static Map<Id,Profile> getAllRevanaProfiles() {

    	Map<Id,Profile> profileIdMap = new Map<Id,Profile>();
		for(Profile p : [SELECT Id 
						FROM Profile
						WHERE Name Like : REVANA_PROFILE]) {
			profileIdMap.put(p.Id,p);
		}
		return profileIdMap;
    }
    
    //─────────────────────────────────────────────────────────────────────────┐
	// getDaysDifference: calculates difference between date recorded on lead 
	//						to current datetime
	// @param 
	// @return 
    //─────────────────────────────────────────────────────────────────────────┘
    private static Integer getDaysDifference(Datetime leadRecDate) {

    	Datetime nowDateTime = Datetime.now();
    	Integer daysDifference = 0;

		daysDifference = leadRecDate.Date().daysBetween(nowDateTime.Date());
		System.debug('days diff:'+daysDifference);
		return daysDifference;
    }
    //─────────────────────────────────────────────────────────────────────────┐
	// logError: calculates difference between date recorded on lead 
	//						to current datetime
	// @param 
	// @return 
    //─────────────────────────────────────────────────────────────────────────┘
    public static void logError(String errorStr, String jobId,
    								String method) {

    	Error_Log__c errorRec =  new Error_Log__c(Class__c = 'LeadSharttankBatch',
    											Error__c = errorStr,
    											Method__c = method,
    											Running_user__c = jobId);
    	insert errorRec;
    }

}