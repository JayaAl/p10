/**
* ─────────────────────────────────────────────────────────────────────────────────────────────────┐
* This class handles test cases for:
*			lead custom tracker updated [Lead activity WF in the past]
*			changing owner of the lead if either the lastmodified date 
*			or the cusotm tracker match the date today
* class covered: LeadSharkTankQueue.cls
*				
* ──────────────────────────────────────────────────────────────────────────────────────────────────
* @author         Jaya Alaparthi
* @maintainedBy   Jaya Alaparthi
* @version        1.0
* @created        2017-06-19
* @modified       
* @systemLayer    Service
* @see            ????
* @see            ????
* ──────────────────────────────────────────────────────────────────────────────────────────────────
* @changes
* vX.X            Jaya Alaparthi
* YYYY-MM-DD      Each change to this file should be documented by incrementing the version number,
*                 and adding a new entry to this @changes list. Note that there is a single blank
*                 line between each @changes entry.
* ─────────────────────────────────────────────────────────────────────────────────────────────────┘
*/
@isTest(SeeAllData=false)
private class LeadSharkTankQueueTest {
	
	@testSetup static void setUpLeadSharkData() {

		Id sharkQueueId;
		// create group
		Group sharkGroup = new Group(Name = 'TesSharkGrp',
									Type = 'Queue');
		insert sharkGroup;
		// create queue
		QueueSobject sharkQueue = new QueueSobject(QueueId = sharkGroup.Id,
									SobjectType = 'Lead');
		// get system admin user
		Profile profileAdmin = [SELECT Id FROM Profile
								WHERE Name='System Administrator' Limit 1];
		// user
		User adminUser = new User(LastName = 'sharkTestAdmin',
								Username = 'testCls@sharktankcls.com',
								Email = 'testCls@sharktankcls.com',
								Alias = 'testAlis',
								TimeZoneSidKey = 'America/Los_Angeles',
								LocaleSidKey = 'en_US', 
                            	EmailEncodingKey = 'ISO-8859-1', 
                            	ProfileId = profileAdmin.Id, 
                            	LanguageLocaleKey = 'en_US');

		Test.startTest();
			System.runAs(adminUser) {

				insert sharkQueue;
				sharkQueueId = sharkQueue.Id;
				// lead to Shark tank
				Lead_To_Sharktank__c leadShark3Days = new Lead_To_Sharktank__c(Name='OpenLeads',
												No_Of_Days__c = 3,
												Owner__c = sharkQueueId);
				insert leadShark3Days;
				Lead_To_Sharktank__c leadShark60Days = new Lead_To_Sharktank__c(Name='NONOpenLeads',
												No_Of_Days__c = 60,
												Owner__c = sharkQueueId);
				insert leadShark60Days;
			}

		Test.stopTest();
		
	}
	@isTest static void customTrackerUpdateTest() {
		// Implement test code
		Id insideSalesLeadId = Schema.SObjectType.Lead.getRecordTypeInfosByName().get('Leads - Inside Sales').getRecordTypeId();
		List<Lead> leadsList = new List<Lead>();
		for(Integer i=0; i<= 100;i++) {

			String leadName = 'testCustomLead'+i;
			String leadEmail = 'test'+i+'sh@email'+i+'.com';
			String companyName = 'testClsCompany'+i;
			Lead leadRec = new Lead(RecordTypeId = insideSalesLeadId,
								LastName = leadName,
								Phone = '1231231234',
								Email = leadEmail,
								Status = 'Open',
								Company = companyName);	
			leadsList.add(leadRec);
		}	
		Test.startTest();
		// get system admin user
		Profile profileAdmin = [SELECT Id FROM Profile
								WHERE Name='System Administrator' Limit 1];
		// user
		User adminUser = new User(LastName = 'sharkTestAdmin',
								Username = 'leadInserttest@sharktankcls.com',
								Email = 'leadInserttest@sharktankcls.com',
								Alias = 'testAlis',
								TimeZoneSidKey = 'America/Los_Angeles',
								LocaleSidKey = 'en_US', 
                            	EmailEncodingKey = 'ISO-8859-1', 
                            	ProfileId = profileAdmin.Id, 
                            	LanguageLocaleKey = 'en_US');
		System.runAs(adminUser)	{
			insert leadsList;
		}
		Test.stopTest();
		Date dtToday =  Date.today();
		for(Lead leadRec : [SELECT Id, Custom_Tracker__c 
							FROM Lead
							WHERE Status = 'Open']) {
			System.debug('leadRec:'+leadRec.Custom_Tracker__c);
			Date leadDt = Date.newInstance(leadRec.Custom_Tracker__c.year(),
										leadRec.Custom_Tracker__c.month(),
										leadRec.Custom_Tracker__c.day());
			System.assertEquals(dtToday,leadDt);
		}
	}
	
	@isTest static void is3daysLeadMovedtoShark() {
		
		List<Lead> leadsInserted = new List<Lead>();
		Set<Id> nonValidLeadOwnerIds = new Set<Id>();
		List<Lead> leadsInShark = new List<Lead>();

		Id insideSalesLeadId = Schema.SObjectType.Lead.getRecordTypeInfosByName().get('Leads - Inside Sales').getRecordTypeId();
		Lead_To_Sharktank__c lead3Days = [SELECT Id,
											Name, No_Of_Days__c,
											Owner__c 
											FROM Lead_To_Sharktank__c
											WHERE Name = 'OpenLeads'];

		lead3Days.No_Of_Days__c = 0;
		update lead3Days;

		List<Lead> leadsList = new List<Lead>();
		for(Integer i=0; i<= 100;i++) {

			String leadName = 'testCustom3Lead'+i;
			String leadEmail = 'test'+i+'sh3@email'+i+'.com';
			String companyName = 'testClsCompany3'+i;
			Lead leadRec = new Lead(RecordTypeId = insideSalesLeadId,
								LastName = leadName,
								Phone = '1231231333',
								Email = leadEmail,
								Status = 'Open',
								Company = companyName);	
			leadsList.add(leadRec);
		}	
		Test.startTest();
		// get system admin user
		Profile profileAdmin = [SELECT Id FROM Profile
								WHERE Name='System Administrator' Limit 1];
		// user
		User adminUser = new User(LastName = 'sharkTest3Admin',
								Username = 'lead3test@sharktankcls.com',
								Email = 'lead3test@sharktankcls.com',
								Alias = 't3Alis',
								TimeZoneSidKey = 'America/Los_Angeles',
								LocaleSidKey = 'en_US', 
                            	EmailEncodingKey = 'ISO-8859-1', 
                            	ProfileId = profileAdmin.Id, 
                            	LanguageLocaleKey = 'en_US');
		System.runAs(adminUser)	{

			insert leadsList;

			nonValidLeadOwnerIds = LeadSharkTankQueue.getOwnerIds().keySet();
			leadsInserted = [SELECT Id, 
								Custom_Tracker__c, 
								OwnerId,
								Do_not_route__c,
								Status, 
								RecordTypeId,
								LastModifiedById,
								LastModifiedDate
							FROM Lead 
							WHERE IsConverted = false
							AND OwnerId NOT IN : nonValidLeadOwnerIds];

			leadsInShark = LeadSharkTankQueue.leadActivityToSharktank(leadsInserted);
		}
		Test.stopTest();
		for(Lead leadRec : leadsInShark) {
			
			System.assertEquals(leadRec.OwnerId,lead3Days.Owner__c,'3 Day Sharktank assignement failed.');
			System.debug('leadRec Owner:'+leadRec.OwnerId
						+'lead3Days:'+lead3Days.Owner__c);
		}

	}

	@isTest static void is60daysLeadMovedtoShark() {
		
		List<Lead> leadsInserted = new List<Lead>();
		Set<Id> nonValidLeadOwnerIds = new Set<Id>();
		List<Lead> leadsInShark = new List<Lead>();

		Id insideSalesLeadId = Schema.SObjectType.Lead.getRecordTypeInfosByName().get('Leads - Inside Sales').getRecordTypeId();
		Lead_To_Sharktank__c lead60Days = [SELECT Id,
											Name, No_Of_Days__c,
											Owner__c 
											FROM Lead_To_Sharktank__c
											WHERE Name = 'NONOpenLeads'];

		lead60Days.No_Of_Days__c = 0;
		update lead60Days;

		List<Lead> leadsList = new List<Lead>();
		for(Integer i=0; i<= 100;i++) {

			String leadName = 'testCustom60Lead'+i;
			String leadEmail = 'test'+i+'sh60@email'+i+'.com';
			String companyName = 'testClsCompany60'+i;
			Lead leadRec = new Lead(RecordTypeId = insideSalesLeadId,
								LastName = leadName,
								Phone = '1231231360',
								Email = leadEmail,
								Status = 'Qualified',
								Company = companyName);	
			leadsList.add(leadRec);
		}	
		Test.startTest();
		// get system admin user
		Profile profileAdmin = [SELECT Id FROM Profile
								WHERE Name='System Administrator' Limit 1];
		// user
		User adminUser = new User(LastName = 'sharkTest60Admin',
								Username = 'lead60test@sharktankcls.com',
								Email = 'lead60test@sharktankcls.com',
								Alias = 't60lis',
								TimeZoneSidKey = 'America/Los_Angeles',
								LocaleSidKey = 'en_US', 
                            	EmailEncodingKey = 'ISO-8859-1', 
                            	ProfileId = profileAdmin.Id, 
                            	LanguageLocaleKey = 'en_US');
		System.runAs(adminUser)	{

			insert leadsList;

			nonValidLeadOwnerIds = LeadSharkTankQueue.getOwnerIds().keySet();
			leadsInserted = [SELECT Id, 
								Custom_Tracker__c,
                             	OwnerId,
                             	Do_not_route__c,
                             	Status, 
                             	RecordTypeId,
                             	LastModifiedById,
                             	LastModifiedDate,
                             	Days_Bucket__c,
                             	Sharktank_Exclusion__c
							FROM Lead 
							WHERE IsConverted = false
							AND OwnerId NOT IN : nonValidLeadOwnerIds];

			leadsInShark = LeadSharkTankQueue.sharkTankSixtyDay(leadsInserted);
		}
		Test.stopTest();
		Date dtToday =  Date.today();
		for(Lead leadRec : leadsInShark) {

			System.assertEquals(leadRec.OwnerId,lead60Days.Owner__c,'60 Day Sharktank assignement failed.');
			System.debug('leadRec Owner:'+leadRec.OwnerId
						+'lead60Days:'+lead60Days.Owner__c);
		}

	}
	
	@isTest static void logErrorTest() {

		String method = 'Test class LeadSharkTankQueueTest';
		String errorMsg = 'Test class error log';
		
		Test.startTest();
			LeadSharkTankQueue.logError(errorMsg+method,'ErrorLogTestId',method);
		Test.stopTest();
		Error_Log__c rec = [SELECT Id,Running_user__c,
									Method__c,
									Class__c,
									Error__c 
							FROM Error_Log__c
							WHERE Running_user__c = 'ErrorLogTestId'];
		System.assertEquals(rec.Class__c,'LeadSharttankBatch','Error in recording error by method logError in class LeadSharkTankQueue.');				

	}
}