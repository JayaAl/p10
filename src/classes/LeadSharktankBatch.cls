/**
* ─────────────────────────────────────────────────────────────────────────────────────────────────┐
* This batch class will update the owner of the Lead to Shark tank
* for every 60 days based on lastmodified if its an non unqualified 
* or for every 3 days 
* based on Custom tracker date if its an open lead
*
* 3 days. Its a replacement for the following WF rules
* 45 days:[based on Last Modified Date : chagne Lead Owner to Shark Tank; Days Bucket = 60;]
* (Lead: ConvertedEQUALSFalse) AND 
* (Lead: Pre Convert CheckEQUALSFalse) AND 
* (Lead: Lead StatusNOT EQUAL TOUnqualified) AND 
* (Current User: ProfileDOES NOT CONTAINRevana) AND 
* (Lead: Lead OwnerNOT EQUAL TOLeads - Deletion Queue,Cold - No Contact,Duplicate Leads,
* 	Unqualified Leads,Unassigned Queue,Out of Scope,Opt Out Queue,Self-Service) AND 
* (Lead: Do not routeEQUALSFalse) AND 
* (Lead: Lead Record TypeNOT EQUAL TOMarketing Leads) AND 
* (Lead: Sharktank ExclusionEQUALSFalse)
* 
* 
* Activity tracker: [based on Custom Tracker Date : change Lead Owner to Shark Tank; Days Bucket  =  3;]
* (Lead: Lead StatusEQUALSOpen) AND 
* (Lead: Pre Convert CheckEQUALSFalse) AND 
* (Current User: ProfileDOES NOT CONTAINRevana) AND 
* (Lead: Lead OwnerNOT EQUAL TOLeads - Deletion Queue,Cold - No Contact,Duplicate Leads,
*  	Unqualified Leads,Unassigned Queue,Out of Scope,Opt Out Queue,Self-Service) AND 
* (Lead: Do not routeEQUALSFalse) AND 
* (Lead: Lead Record TypeNOT EQUAL TOMarketing Leads,Self-Service)
* This class is referenced in LeadTriggerHandler for handling shark tank 3 days rule's 
* custom date field update
* and leadSharkTankbatchjob class for handling 3 days and 60 days update.
* Custom Settings:
* Lead To Sharktank
* ──────────────────────────────────────────────────────────────────────────────────────────────────
* @author         Jaya Alaparthi
* @maintainedBy   Jaya Alaparthi
* @version        1.0
* @created        2017-06-14
* @modified       
* @systemLayer    Service[Batch]
* @see            ????
* @see            ????
* ──────────────────────────────────────────────────────────────────────────────────────────────────
* @changes
* vX.X            Jaya Alaparthi
* YYYY-MM-DD      Each change to this file should be documented by incrementing the version number,
*                 and adding a new entry to this @changes list. Note that there is a single blank
*                 line between each @changes entry.
* ─────────────────────────────────────────────────────────────────────────────────────────────────┘
*/
global class LeadSharktankBatch  implements Database.Batchable<sObject> {
	
	global final String query;
	global final Set<Id> nonValidLeadOwnerIds;
	global LeadSharktankBatch () {
		
		Map<Id,QueueSobject> leadOwnerNameIdMap = new Map<Id,QueueSobject>();
		Set<String> leadOwnerIds = new Set<String>();
		LeadSharkTankQueue.LEAD_QUEUE.add('Shark Tank');
		system.debug('leadSharktankQueue:'+LeadSharkTankQueue.LEAD_QUEUE);
		nonValidLeadOwnerIds = LeadSharkTankQueue.getOwnerIds().keySet();
		
	}
	
	global Database.QueryLocator start(Database.BatchableContext BC) {
		System.debug('query:'+query);
		return Database.getQueryLocator([SELECT Id, 
											Custom_Tracker__c, 
											OwnerId,
											Do_not_route__c,
											Status, 
											RecordTypeId,
											LastModifiedById,
											LastModifiedDate,
											Days_Bucket__c,
											Sharktank_Exclusion__c
										FROM Lead 
										WHERE IsConverted = false
										AND OwnerId NOT IN : nonValidLeadOwnerIds]);
	}

   	global void execute(Database.BatchableContext BC, List<sObject> scope) {
		
		List<Lead> leadsToProcessBatch = new List<Lead>();
		Map<Id,Lead> sharkLeadsForUpdate = new Map<Id,Lead>();

		String jobId = BC.getJobId();
		leadsToProcessBatch = scope;
		List<Lead> lastModifiedDependentLeads = new List<Lead>();
		List<Lead> customTrackerDependentLeads = new List<Lead>();

		try {
			LeadSharkTankQueue.IS_Batch_Triggered = true;
			customTrackerDependentLeads = LeadSharkTankQueue.leadActivityToSharktank(leadsToProcessBatch);
			for(Lead leadRec : customTrackerDependentLeads) {

				sharkLeadsForUpdate.put(leadRec.Id,leadRec);
			}
			lastModifiedDependentLeads = LeadSharkTankQueue.sharkTankSixtyDay(leadsToProcessBatch);
			for(Lead leadRec : lastModifiedDependentLeads) {

				sharkLeadsForUpdate.put(leadRec.Id,leadRec);
			}
			
			if(!sharkLeadsForUpdate.isEmpty()) {

				list<Lead> leadsToUpdate = sharkLeadsForUpdate.values();
				update leadsToUpdate;
			}
		} catch(Exception e) {

			String method = e.getStackTraceString();
			String errorMsg = e.getMessage();
			

			LeadSharkTankQueue.logError(errorMsg+method,jobId,method);
		}
	}
	
	global void finish(Database.BatchableContext BC) {
		
	}
	
}