/**
* ─────────────────────────────────────────────────────────────────────────────────────────────────┐
* This class handles test cases for:
*			lead custom tracker updated [Lead activity WF in the past]
*			changing owner of the lead if either the lastmodified date 
*			or the cusotm tracker match the date today
* class covered: LeadSharktankBatch.cls
*				
* ──────────────────────────────────────────────────────────────────────────────────────────────────
* @author         Jaya Alaparthi
* @maintainedBy   Jaya Alaparthi
* @version        1.0
* @created        2017-06-19
* @modified       
* @systemLayer    Service
* @see            ????
* @see            ????
* ──────────────────────────────────────────────────────────────────────────────────────────────────
* @changes
* vX.X            Jaya Alaparthi
* YYYY-MM-DD      Each change to this file should be documented by incrementing the version number,
*                 and adding a new entry to this @changes list. Note that there is a single blank
*                 line between each @changes entry.
* ─────────────────────────────────────────────────────────────────────────────────────────────────┘
*/
@isTest(SeeAllData=false)
private class LeadSharktankBatchTest {
	
	@testSetup static void setUpLeadSharkData() {

		Id sharkQueueId;
		// create group
		Group sharkGroup = new Group(Name = 'TesSharkGrp',
									Type = 'Queue');
		insert sharkGroup;
		// create queue
		QueueSobject sharkQueue = new QueueSobject(QueueId = sharkGroup.Id,
									SobjectType = 'Lead');
		// get system admin user
		Profile profileAdmin = [SELECT Id FROM Profile
								WHERE Name='System Administrator' Limit 1];
		// user
		User adminUser = new User(LastName = 'sharkTestAdmin',
								Username = 'testCls@sharktankcls.com',
								Email = 'testCls@sharktankcls.com',
								Alias = 'testAlis',
								TimeZoneSidKey = 'America/Los_Angeles',
								LocaleSidKey = 'en_US', 
                            	EmailEncodingKey = 'ISO-8859-1', 
                            	ProfileId = profileAdmin.Id, 
                            	LanguageLocaleKey = 'en_US');

		Test.startTest();
			System.runAs(adminUser) {

				insert sharkQueue;
				sharkQueueId = sharkQueue.Id;
				QueueSobject testQueueRec = [SELECT QueueId FROM QueueSobject
											WHERE Id =: sharkQueueId];
				sharkQueueId = testQueueRec.QueueId;
				// lead to Shark tank
				Lead_To_Sharktank__c leadShark3Days = new Lead_To_Sharktank__c(Name='OpenLeads',
												No_Of_Days__c = 0,
												Owner__c = sharkQueueId);
				insert leadShark3Days;
				Lead_To_Sharktank__c leadShark60Days = new Lead_To_Sharktank__c(Name='NONOpenLeads',
												No_Of_Days__c = 0,
												Owner__c = sharkQueueId);
				insert leadShark60Days;
			}

		Test.stopTest();
		
	}
	@isTest static void testSharkBatchMethod() {
		
		// create leads
		List<Lead> leadsList = new List<Lead>();
		Id insideSalesLeadId = Schema.SObjectType.Lead.getRecordTypeInfosByName().get('Leads - Inside Sales').getRecordTypeId();
		
		Lead_To_Sharktank__c leadQueue = [SELECT Id,
											Name, No_Of_Days__c,
											Owner__c 
											FROM Lead_To_Sharktank__c
											WHERE Name = 'OpenLeads'];

		for(Integer i=0; i<= 50;i++) {

			String leadName = 'testCustomLead'+i;
			String leadEmail = 'test'+i+'sh@email'+i+'.com';
			String companyName = 'testClsCompany'+i;

			String nonOpenleadName = 'onOpenCustomLead'+i;
			String nonOpenleadEmail = 'nonOpentes'+i+'sh@email'+i+'.com';
			String nonOpencompanyName = 'nonOpentestCls'+i;
			Lead leadRec = new Lead(RecordTypeId = insideSalesLeadId,
								LastName = leadName,
								Phone = '1231231234',
								Email = leadEmail,
								Status = 'Open',
								Company = companyName);	
			Lead nonOpenLead = new Lead(RecordTypeId = insideSalesLeadId,
								LastName = nonOpenleadName,
								Phone = '1231231234',
								Email = nonOpenleadEmail,
								Status = 'Qualified',
								Company = nonOpencompanyName);	
			leadsList.add(leadRec);
			leadsList.add(nonOpenLead);
		}	
		Profile profileAdmin = [SELECT Id FROM Profile
								WHERE Name='System Administrator' Limit 1];
		// user
		User adminUser = new User(LastName = 'sharkTestBatch',
								Username = 'leadBatchtest@sharktankcls.com',
								Email = 'leadBatchtest@sharktankcls.com',
								Alias = 'tBatch',
								TimeZoneSidKey = 'America/Los_Angeles',
								LocaleSidKey = 'en_US', 
                            	EmailEncodingKey = 'ISO-8859-1', 
                            	ProfileId = profileAdmin.Id, 
                            	LanguageLocaleKey = 'en_US');
		
		Test.startTest();			
			System.runAs(adminUser)	{

				insert leadsList;
			}
			LeadSharktankBatch batchTest = new LeadSharktankBatch();
			Database.executeBatch(batchTest);		
		Test.stopTest();

		for(Lead processedLead : [SELECT Id, OwnerId 
									FROM Lead
									WHERE Phone='1231231234'
									AND Status ='Qualified']) {
			System.assertEquals(processedLead.OwnerId,leadQueue.Owner__c,'Error in assigning shark');
			System.debug('processedLead:'+processedLead.OwnerId
				+'leadQueue:'+leadQueue.Owner__c);
		}
	}
    @isTest static void testSharkBatchDiffRTMethod() {
		
		// create leads
		List<Lead> leadsList = new List<Lead>();
		Id marketingLeadId = Schema.SObjectType.Lead.getRecordTypeInfosByName().get('Marketing Leads').getRecordTypeId();
		
		Lead_To_Sharktank__c leadQueue = [SELECT Id,
											Name, No_Of_Days__c,
											Owner__c 
											FROM Lead_To_Sharktank__c
											WHERE Name = 'OpenLeads'];

		for(Integer i=0; i<= 10;i++) {

			String leadName = 'testCustomLead'+i;
			String leadEmail = 'test'+i+'sh@email'+i+'.com';
			String companyName = 'testClsCompany'+i;

			String nonOpenleadName = 'onOpenCustomLead'+i;
			String nonOpenleadEmail = 'nonOpentes'+i+'sh@email'+i+'.com';
			String nonOpencompanyName = 'nonOpentestCls'+i;
			Lead leadRec = new Lead(RecordTypeId = marketingLeadId,
								LastName = leadName,
								Phone = '1231231234',
								Email = leadEmail,
								Status = 'Open',
								Company = companyName);	
			Lead nonOpenLead = new Lead(RecordTypeId = marketingLeadId,
								LastName = nonOpenleadName,
								Phone = '1231231234',
								Email = nonOpenleadEmail,
								Status = 'Qualified',
								Company = nonOpencompanyName);	
			leadsList.add(leadRec);
			leadsList.add(nonOpenLead);
		}	
		Profile profileAdmin = [SELECT Id FROM Profile
								WHERE Name='System Administrator' Limit 1];
		// user
		User adminUser = new User(LastName = 'sharkTestBatch',
								Username = 'leadBatchtest@sharktankcls.com',
								Email = 'leadBatchtest@sharktankcls.com',
								Alias = 'tBatch',
								TimeZoneSidKey = 'America/Los_Angeles',
								LocaleSidKey = 'en_US', 
                            	EmailEncodingKey = 'ISO-8859-1', 
                            	ProfileId = profileAdmin.Id, 
                            	LanguageLocaleKey = 'en_US');
		
		Test.startTest();			
			System.runAs(adminUser)	{

				insert leadsList;
			}
			LeadSharktankBatch batchTest = new LeadSharktankBatch();
			Database.executeBatch(batchTest);		
		Test.stopTest();

		for(Lead processedLead : [SELECT Id, OwnerId 
									FROM Lead
									WHERE Phone='1231231234'
									AND Status ='Qualified']) {
			System.assertNotEquals(processedLead.OwnerId,leadQueue.Owner__c,'Marketing leads should not be moved to Shark Tank.');
			System.debug('processedLead:'+processedLead.OwnerId
				+'leadQueue:'+leadQueue.Owner__c);
		}
	}
	
}