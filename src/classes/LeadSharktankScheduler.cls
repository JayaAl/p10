/**
* ─────────────────────────────────────────────────────────────────────────────────────────────────┐
* LeadSharktankScheduler: schedules LeadSharkBatch.
* ──────────────────────────────────────────────────────────────────────────────────────────────────
* @author         Jaya Alaparthi
* @maintainedBy   Jaya Alaparthi
* @version        1.0
* @created        2017-06-15
* @modified       
* @systemLayer    Service
* @see            ????
* @see            ????
* ──────────────────────────────────────────────────────────────────────────────────────────────────
* @changes
* vX.X            Jaya Alaparthi
* YYYY-MM-DD      Each change to this file should be documented by incrementing the version number,
*                 and adding a new entry to this @changes list. Note that there is a single blank
*                 line between each @changes entry.
* ─────────────────────────────────────────────────────────────────────────────────────────────────┘
*/
global with sharing class LeadSharktankScheduler implements Schedulable {
	
	global void execute(SchedulableContext sc) {

		LeadSharktankBatch ref = new LeadSharktankBatch();
		Database.executebatch(ref);
	}
}