/**
* ─────────────────────────────────────────────────────────────────────────────────────────────────┐
* This class handles test cases for:
*			LeadSharktankScheduler
* class covered: LeadSharktankScheduler.cls
*				
* ──────────────────────────────────────────────────────────────────────────────────────────────────
* @author         Jaya Alaparthi
* @maintainedBy   Jaya Alaparthi
* @version        1.0
* @created        2017-06-19
* @modified       
* @systemLayer    Service
* @see            ????
* @see            ????
* ──────────────────────────────────────────────────────────────────────────────────────────────────
* @changes
* vX.X            Jaya Alaparthi
* YYYY-MM-DD      Each change to this file should be documented by incrementing the version number,
*                 and adding a new entry to this @changes list. Note that there is a single blank
*                 line between each @changes entry.
* ─────────────────────────────────────────────────────────────────────────────────────────────────┘
*/
@isTest(SeeAllData=false)
private class LeadSharktankSchedulerTest {
	
	@testSetup static void setUpLeadSharkData() {

		Id sharkQueueId;
		// create group
		Group sharkGroup = new Group(Name = 'TesSharkGrp',
									Type = 'Queue');
		insert sharkGroup;
		// create queue
		QueueSobject sharkQueue = new QueueSobject(QueueId = sharkGroup.Id,
									SobjectType = 'Lead');
		// get system admin user
		Profile profileAdmin = [SELECT Id FROM Profile
								WHERE Name='System Administrator' Limit 1];
		// user
		User adminUser = new User(LastName = 'sharkTestAdmin',
								Username = 'testCls@sharktankcls.com',
								Email = 'testCls@sharktankcls.com',
								Alias = 'testAlis',
								TimeZoneSidKey = 'America/Los_Angeles',
								LocaleSidKey = 'en_US', 
                            	EmailEncodingKey = 'ISO-8859-1', 
                            	ProfileId = profileAdmin.Id, 
                            	LanguageLocaleKey = 'en_US');

		Test.startTest();
			System.runAs(adminUser) {

				insert sharkQueue;
				sharkQueueId = sharkQueue.Id;
				QueueSobject testQueueRec = [SELECT QueueId FROM QueueSobject
											WHERE Id =: sharkQueueId];
				sharkQueueId = testQueueRec.QueueId;
				// lead to Shark tank
				Lead_To_Sharktank__c leadShark3Days = new Lead_To_Sharktank__c(Name='OpenLeads',
												No_Of_Days__c = 0,
												Owner__c = sharkQueueId);
				insert leadShark3Days;
				Lead_To_Sharktank__c leadShark60Days = new Lead_To_Sharktank__c(Name='NONOpenLeads',
												No_Of_Days__c = 0,
												Owner__c = sharkQueueId);
				insert leadShark60Days;
			}

		Test.stopTest();
		
	}
	
	@isTest static void schedulerTest() {
		
		LeadSharktankScheduler schedulerRef = new LeadSharktankScheduler();
		String sch = '0  00 1 * * ? *';
		Test.startTest();
		System.schedule('testLeadSharktankSchdeulerTest', sch, schedulerRef);
		Test.stopTest();
		list<AsyncApexJob> schRef = [SELECT ApexClassId, Id, JobItemsProcessed,
											 JobType, 
       										Status, NumberOfErrors, 
       										MethodName 
									FROM AsyncApexJob
									WHERE JobType IN ('BatchApexWorker','ScheduledApex')];
		System.assert(schRef.size() > 0);
	}
	
}