/*********************************************************************************************************************
*  Name                 : LeadTriggerHandler
*  Author               : Sridharan Subramanian
*  Last Modified Date   : 05/04/2016
*  Description          : Used to handle all Lead Trigger events
*                         1. Changed the logic to Assigns Lead Owner in Round Robin fashion after infer update
*                         2. Categorize and assign the leads to the SalesRep/Queue based on Infer Score & Ratings
*                         3. Added Logic to update lead MSA, Territory and Region from Zip_Code__c
*                            object to respective Lead fields  
*                          
************************************************************************************************************************
* ──────────────────────────────────────────────────────────────────────────────────────────────────
* @changes
* v1.2            Jaya Alaparthi
* 2017-06-13      Adding Lead Activity WorkFlow rule logic to Lead BeforeInsert, Beforeupdate trigger.
*                 This logic referes  LeadSharkTankQueue.cls
* ─────────────────────────────────────────────────────────────────────────────────────────────────┘
*/
 
 
public class LeadTriggerHandler {
 
    public static string marketingLeadsRecordType = 'Marketing Leads';
    public static string selfServiceLeadsRecordType = 'Self-Service';
    public static string salesLeadsRecordType = 'Leads - Inside Sales';
    public static string accountRecordTypeName = 'default';
    
    public Map<ID, Schema.RecordTypeInfo> rtMap;
    public Id insideSalesRecordTypeId{get;set;}  
    public id marketingRecordTypeId{get;set;}  
    public id selfServiceRecordTypeId{get;set;}
    //boolean assignmentRuleTrigger{get;set;}
    //public map<id,Lead> leadOwnerReps {get;set;}    
    public map<string,Map<id,Lead>> leadOwnerTypes {get;set;}
    //public static map<id,Map<datetime,decimal>> leadToAssignmentTime {get;set;}
    public Map<Id,Lead> leadsForDeletionQueue = new Map<Id,Lead>();
    public static Boolean assignAlreadyCalled=FALSE;
    //public static map<lead,account> leadToAccount {get;set;}
    public static date thisDay = system.today();
    public map<id,Lead> leadDUNSMatch {get;set;}
    
    
    
    public LeadTriggerHandler()
    {
        /*assignmentRuleTrigger = true;
        leadOwnerReps = new Map<id,lead>();
        leadOwnerTypes = new map<string,Map<id,Lead>>();
        */
        leadOwnerTypes = new map<string,Map<id,Lead>>();
        insideSalesRecordTypeId = LeadTriggerUtil.getSalesRecordTypeId();//Schema.SObjectType.Lead.getRecordTypeInfosByName().get(salesLeadsRecordType).getRecordTypeId();
        rtMap = Schema.SObjectType.Lead.getRecordTypeInfosById();
        marketingRecordTypeId = LeadTriggerUtil.getMarketingRecordTypeId();//Schema.SObjectType.Lead.getRecordTypeInfosByName().get(marketingLeadsRecordType).getRecordTypeId();
        selfServiceRecordTypeId = LeadTriggerUtil.getSelfServiceRecordTypeId();//Schema.SObjectType.Lead.getRecordTypeInfosByName().get(selfServiceLeadsRecordType).getRecordTypeId();
        system.debug(logginglevel.info,'SELFSERVICE' + selfServiceRecordTypeId);
        
    }
    public void OnBeforeInsert(List<Lead> newRecords){
        LeadTriggerHandlerHelper leadHelper = new LeadTriggerHandlerHelper();
        //Method that validates if the Sales Leads passes through Assignment Rules and if so we don't let the lead go through RR logic
        //If the Sales lead is validated to be eligible for RR logic then customRoutedLead__c is set to true
        leadHelper.CheckCustomRouting(newRecords);
        
        //ASSIGNMENT OF REGION TERRITORY AND MSA
        leadHelper.leadZipToState(newRecords, new Map<Id, Lead>()); 
        //─────────────────────────────────────────────────────────────────────────┐
        //v1.2 
        //─────────────────────────────────────────────────────────────────────────┘
        LeadSharkTankQueue.customTrackerUpdateTriggered(newRecords);
    }
    
    
    public void OnBeforeUpdate(List<Lead> oldRecords, 
      List<Lead> updatedRecords,  Map<Id, Lead> oldMap, 
      Map<Id, Lead> newMap){
        system.debug(logginglevel.info,'before update class called');
        map<id,Lead> leadOwnerToQueue = new Map<id,lead>();
        LeadTriggerHandlerHelper leadHelper = new LeadTriggerHandlerHelper();
        
        
        //FUNCTIONALITY OF THE BELOW METHOD
        //SETS THE DELETION AUTO NUMBER
        //Assigns the OWNER AS MGR QUEUE IF THE LEAD MATCHES THE CRITERIA
        //Creates a leadOwnerTypes Map for SalesRep Assignment which is done in leadRoundRobin method
        
        system.debug(logginglevel.info,'updatedRecords called' + updatedRecords);
        system.debug(logginglevel.info,'oldMap called'+ oldMap);
        leadOwnerTypes = leadHelper.LeadOwnerChange(updatedRecords,oldMap);
        
        //if(leadOwnerToQueue==null || leadOwnerToQueue.size()==0 || (leadOwnerReps!=null && leadOwnerReps.size()>0))
        if(leadOwnerTypes!=null && leadOwnerTypes.containsKey('Rep'))
        {
            system.debug(logginglevel.info,'this is REPS1' + leadOwnerTypes);
            system.debug(logginglevel.info,'this is REPS2' + leadOwnerTypes.get('Reps'));
            //ASSIGNMENT OF REGION TERRITORY AND MSA
            leadHelper.leadZipToState(leadOwnerTypes.get('Rep').values(), oldMap);
            
            //Uses the leadOwnerTypes MAP for Reps which was created in LeadOwnerChange() method and does RR for reps here
            //CODE CHANGED FOR DISTRIBUTION ENGINE
            //leadRoundRobin(leadOwnerTypes.get('Rep').values(), oldMap);
            leadHelper.assignToUnAssignedQueue(leadOwnerTypes.get('Rep').values());
            
        }
        //─────────────────────────────────────────────────────────────────────────┐
        //v1.2 
        //─────────────────────────────────────────────────────────────────────────┘
        LeadSharkTankQueue.customTrackerUpdateTriggered(updatedRecords);
    }
    
    
    
    public void OnAfterUpdate(List<Lead> oldRecords, 
      List<Lead> updatedRecords,  Map<Id, Lead> oldMap, 
      Map<Id, Lead> newMap){
        System.debug(logginglevel.info, 'OnAfterUpdate class called');
          
        //CONSTRUCT FOR MARKETING LEADS
        //==========================================================
        leadDUNSMatch = new Map<Id,lead>();
        Map<id,sObject> oldMapSobject = new Map<id,sObject>();
        oldMapSobject = oldMap;
        Map<id,sObject> marketingLeads = new Map<id,sObject>();
        List<Lead> salesUpdatedRecords = new List<Lead>();
        Map<Id, Lead> salesOldMap = new Map<Id, Lead>();
        Map<id,Lead> insideSalesLeads = new Map<id,lead>();
        LeadTriggerHandlerHelper leadHelper = new LeadTriggerHandlerHelper();
        
        //marketingRecordTypeId
        for(sObject l:updatedRecords)
        {
            if((l.get('RecordTypeId')==marketingRecordTypeId && l.get('Marketo_Contact_Us_Form__c') <> oldMapSobject.get(l.id).get('Marketo_Contact_Us_Form__c') &&  l.get('Marketo_Contact_Us_Form__c')==true))
            {
                insideSalesLeads.put(l.id,(lead)l);
            }
            else if(!string.isEmpty(LeadTriggerHandlerHelper.leadDUNSFieldName) && l.get('RecordTypeId')==marketingRecordTypeId && ((decimal)l.get(LeadTriggerHandlerHelper.mktoFieldName) <> (decimal)oldMapSobject.get(l.id).get(LeadTriggerHandlerHelper.mktoFieldName) &&  (decimal)l.get(LeadTriggerHandlerHelper.mktoFieldName)>=75))
            {
                marketingLeads.put(l.id,l);
            }
            else if(l.get('RecordTypeId')!=null && l.get('RecordTypeId')!=marketingRecordTypeId && l.get('RecordTypeId')!= selfServiceRecordTypeId)
            {
                assignAlreadyCalled = true;
                salesUpdatedRecords.Add((lead)l);
                salesOldMap.put(l.Id,oldMap.get(l.Id));
            }
            
            
            if(insideSalesLeads.size()>0)
                system.debug(logginglevel.info,'i AM HERE - insideSalesLeads - 2 ' + insideSalesLeads);
            system.debug(logginglevel.info,'i AM HERE - RECORDTYPEID - 2 ' + l.get('RecordTypeId'));
            system.debug(logginglevel.info,'i AM HERE - mktoFieldName - 2 ' + LeadTriggerHandlerHelper.mktoFieldName);
            system.debug(logginglevel.info,'i AM HERE - (decimal)oldMapSobject.get(l.id).get(LeadTriggerHandlerHelper.mktoFieldName) - 2 ' + (decimal)oldMapSobject.get(l.id).get(LeadTriggerHandlerHelper.mktoFieldName));
            system.debug(logginglevel.info,'i AM HERE - (decimal)l.get(LeadTriggerHandlerHelper.mktoFieldName) - 2 ' + (decimal)l.get(LeadTriggerHandlerHelper.mktoFieldName));
            system.debug(logginglevel.info,'i AM HERE - FALSE - 2 ' + (!string.isEmpty(LeadTriggerHandlerHelper.leadDUNSFieldName) && l.get('RecordTypeId')!=null && l.get('RecordTypeId')==marketingRecordTypeId && ((decimal)l.get(LeadTriggerHandlerHelper.mktoFieldName) <> (decimal)oldMapSobject.get(l.id).get(LeadTriggerHandlerHelper.mktoFieldName) &&  (decimal)l.get(LeadTriggerHandlerHelper.mktoFieldName)>=75)));
        
        }
        
        if(insideSalesLeads.size()>0)
            LeadTriggerHandlerHelper.convertToSalesLead(insideSalesLeads,LeadTriggerUtil.getSalesRecordTypeId());
        
        system.debug(logginglevel.info,'i AM HERE - marketingLeads - 2 ' + marketingLeads.values());
        system.debug(logginglevel.info,'i AM HERE - leadDUNSFieldName - 2 ' + LeadTriggerHandlerHelper.leadDUNSFieldName);
        
        if(marketingLeads.size()>0)
            leadHelper.matchDUNSandProcess(marketingLeads.values());
        
        
        list<lead> lstLeads =new List<Lead>();
        
        //CONSTRUCT FOR INSIDE SALES LEADS
        //==========================================================
        system.debug(logginglevel.info,'i AM HERE - salesUpdatedRecords' + salesUpdatedRecords);
        system.debug(logginglevel.info,'i AM HERE - salesOldMap' + salesOldMap);
        if(salesUpdatedRecords.size()>0 && salesOldMap.size()>0)
        {
            //The below method is used to populate the ASSIGNMENTGROUPS,GROUPMEMBERJUNCTION, AND GROUPMEMBER
            //OBJECTS with the last updated time on lead so that the roundrobin order is decided
            //leadOwnerUpdate(salesUpdatedRecords, salesOldMap);
            
            for(lead objlead:salesUpdatedRecords)
            {
                if(objLead.isConverted == false && (!leadDUNSMatch.containsKey(objlead.Id)))
                {
                    lstLeads.Add(objlead);
                }
            }
        }
        
        //The below code is used to populate the Pandora_B2B_Event__c IN ACCOUNT AND Pandora_Promotion__c IN CONTACT
        //ThIS IS CALLED ONLY WHEN THE LEAD IS CONVERTED
        
        system.debug(logginglevel.info,'i AM HERE - lstLeads' + lstLeads);
        if(lstLeads.size()>0)
            leadHelper.mapAccountContactOnConvert(lstLeads);
        
    }
    

        
}