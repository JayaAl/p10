/*******************************************************************************************************
*  Name                 : LeadTriggerHandlerTest
*  Author               : Sridharan Subramanian
*  Last Modified Date   : 05/04/2016
*  Description          : Test Class to manage the Code Coverage of LeadHandler Class and TriggerOnLead
********************************************************************************************************/

@isTest(SeeAllData=false)
public class LeadTriggerHandlerTest {
    
    
    @testsetup static void testDataSetup() {
    
        List<Leads_Settings__c> lstLeadSettings = new List<Leads_Settings__c>();
        
        Leads_Settings__c lc1 = new Leads_Settings__c();
        lc1.Name = 'Account Criteria Date Field';
        lc1.Value__c = 'LastActivityDate';
        lstLeadSettings.Add(lc1);
        
        Leads_Settings__c lc2 = new Leads_Settings__c();
        lc2.Name = 'Account DUNS Field Name';
        lc2.Value__c = 'D_U_N_S__c';
        lstLeadSettings.Add(lc2);
        
        Leads_Settings__c lc3 = new Leads_Settings__c();
        lc3.Name = 'Account Record Type Name';
        lc3.Value__c = 'default';
        lstLeadSettings.Add(lc3);
        
        Leads_Settings__c lc4 = new Leads_Settings__c();
        lc4.Name = 'Lead DUNS Field Name';
        lc4.Value__c = 'CompanyDunsNumber';
        lstLeadSettings.Add(lc4);
        
        Leads_Settings__c lc5 = new Leads_Settings__c();
        lc5.Name = 'Lead Process Start Date';
        lc5.Lead_Process_Date__c = system.Today();
        lstLeadSettings.Add(lc5);
        
        Leads_Settings__c lc6 = new Leads_Settings__c();
        lc6.Name = 'Lead SnapShot Field';
        lc6.Value__c = 'infer3__Score_Snapshot__c';
        lstLeadSettings.Add(lc6);
        
        Leads_Settings__c lc7 = new Leads_Settings__c();
        lc7.Name = 'Marketing Leads Record Type Name';
        lc7.Value__c = 'Marketing Leads';
        lstLeadSettings.Add(lc7);
        
        Leads_Settings__c lc8 = new Leads_Settings__c();
        lc8.Name = 'Marketo Score Field Name';
        lc8.Value__c = 'mkto71_Lead_Score__c';
        lstLeadSettings.Add(lc8);
        
        Leads_Settings__c lc9 = new Leads_Settings__c();
        lc9.Name = 'Sales Leads Record Type Name';
        lc9.Value__c = 'Leads - Inside Sales';
        lstLeadSettings.Add(lc9);
        
        insert lstLeadSettings;
        
        
        Custom_Constants__c cs = new Custom_Constants__c();
        cs.Name='Lead Routing - Infer3__Infer_Rating__c';
        cs.Value__c = 'A';
        
        Custom_Constants__c cs1 = new Custom_Constants__c();
        cs1.Name='Lead Routing - Infer3__Infer_Score__c';
        cs1.Value__c = '89';
        insert cs;
        insert cs1;
        
        
        Assignment_Group__c agTest = new Assignment_Group__c(Name = 'Test Group', Type__c = 'Lead');
        Group_Member__c gmTest = new Group_Member__c(Name=UserInfo.getName(), Active__c = 'True', User__c = UserInfo.getUserId());
        Zip_Code__c zcTest = new Zip_Code__c(Active__c = 'True', Name = '11111');
        insert agTest;
        insert gmTest;
        insert zcTest;
        Group_Member_Zip_Junction__c gmzjTest = new Group_Member_Zip_Junction__c(Assignment_Group__c = agTest.Id, Group_Member__c = gmTest.Id,Zip_Code__c = zcTest.Id);
        
        Lead_ZipState__c lz = new Lead_ZipState__c(Name='11111',State__c = 'Northwest');
        insert gmzjTest;
        insert lz;  
        //insert cs;
        //insert cs1; 
        
        Lead testISLead = new Lead (firstname = 'TODAYCREATED', LastName='TESTLEAD',Company='TestCompany', PostalCode = '94538', OwnerID=UserInfo.getUserId(),Phone='3332223232', email='testLeadIS@test.com',LeadSource='Web',infer3__Score_Index__c=1041,Pandora_Promotion__c='March 2015;June 2015;AAA',Pandora_B2B_Event__c='Advertising Week 2015;Trade Show;Seminar - Internal',Do_not_route__c=false); //tempOwnerID__c=testGroup2.id,  
        insert testISLead;
        system.debug('C1ID = ' + testISLead.id);
        
        Schema.DescribeSObjectResult cfrSchema = Schema.SObjectType.Lead; 
        Map<String,Schema.RecordTypeInfo> LeadRecordTypeInfo = cfrSchema.getRecordTypeInfosByName();
        Id mktRTId = LeadRecordTypeInfo.get('Marketing Leads').getRecordTypeId();
        
        /*Lead testMKTINGLead1 = new Lead (firstname = 'TODAYCREATEDCompany001', mkto71_Lead_Score__c=78, LastName='TESTLEADCompany001',Company='TestCompany001', PostalCode = '94538', OwnerID=UserInfo.getUserId(),Phone='7772223232', email='testLeadog@testcompanies.com',LeadSource='Web',infer3__Score_Index__c=1041,Pandora_Promotion__c='March 2015;June 2015;AAA',Pandora_B2B_Event__c='Advertising Week 2015;Trade Show;Seminar - Internal',Do_not_route__c=false,RecordTypeId=mktRTId); //tempOwnerID__c=testGroup2.id,  
        insert testMKTINGLead1;
        system.debug('C1ID = ' + testMKTINGLead1.id);
        */
        
        
    
    }
    
    /*public static  testMethod void UpdateInsideSalesLeadForRR()
    {
    
        
        //lead lead2 = [select id, LastName,Company,PostalCode,OwnerID,LeadSource, Phone,email, infer3__Score_Snapshot__c,Do_not_route__c,RecordTypeId,infer3__Score_Index__c from lead where recordtypeid!=:mktRTId];
        //system.debug('C2ID = ' + lead2.id);
        //Lead lead2 = new Lead (firstname = 'TODAYCREATED011', mkto71_Lead_Score__c=78, LastName='TESTLEAD011',Company='TestCompany', PostalCode = '94538', OwnerID=UserInfo.getUserId(),Phone='3332223232', email='testLead0101@test.com',LeadSource='Web',infer3__Score_Index__c=1041,Pandora_Promotion__c='March 2015;June 2015;AAA',Pandora_B2B_Event__c='Advertising Week 2015;Trade Show;Seminar - Internal',Do_not_route__c=false,RecordTypeId=mktRTId); //tempOwnerID__c=testGroup2.id,  
        Schema.DescribeSObjectResult cfrSchema = Schema.SObjectType.Lead; 
        Map<String,Schema.RecordTypeInfo> LeadRecordTypeInfo = cfrSchema.getRecordTypeInfosByName();
        Id salesRTID = LeadRecordTypeInfo.get('Leads - Inside Sales').getRecordTypeId();
        
        //Lead lead1 = new Lead (firstname = 'TODAYCREATED', LastName='TESTLEAD',Company='TestCompany', PostalCode = '94538', OwnerID=UserInfo.getUserId(),Phone='3332223232', email='testLeadIS@test.com',LeadSource='Web',infer3__Score_Index__c=1041,Pandora_Promotion__c='March 2015;June 2015;AAA',Pandora_B2B_Event__c='Advertising Week 2015;Trade Show;Seminar - Internal',Do_not_route__c=false); //tempOwnerID__c=testGroup2.id,  
        //insert lead1;
        
        
        Lead lead1 = new Lead (firstname = 'TODAYCREATED0101_01', LastName='TESTLEAD0110_01',Company='TestCompany_01', PostalCode = '94539', OwnerID=UserInfo.getUserId(),Phone='3332223232', email='testLeadIS010_01@testing.com',LeadSource='Web',infer3__Score_Index__c=1041,Pandora_Promotion__c='March 2015;June 2015;AAA',Pandora_B2B_Event__c='Advertising Week 2015;Trade Show;Seminar - Internal',Do_not_route__c=false,RecordTypeId=salesRTID); //tempOwnerID__c=testGroup2.id,  
        test.startTest();
        insert lead1;
        lead lead2 = [select id, LastName,Company,PostalCode,OwnerID,LeadSource, Phone,email, infer3__Score_Snapshot__c,Do_not_route__c,RecordTypeId,infer3__Score_Index__c from lead limit 1];
        system.debug('THIS IS LEAD2' + lead2);
        
        lead2.infer3__Score_Snapshot__c = 100;
        lead2.Do_not_route__c = false;
        lead2.PostalCode = '11111';
        system.debug('C2 = ' + lead2);
        update lead2;
        test.stoptest();
    }
    
    
    public static  testMethod void UpdateInsideSalesLeadForDeletionRR()
    {
    
        Schema.DescribeSObjectResult cfrSchema = Schema.SObjectType.Lead; 
        Map<String,Schema.RecordTypeInfo> LeadRecordTypeInfo = cfrSchema.getRecordTypeInfosByName();
        Id salesRTID = LeadRecordTypeInfo.get('Leads - Inside Sales').getRecordTypeId();
        //lead lead2 = [select id, LastName,Company,PostalCode,OwnerID,LeadSource, Phone,email, infer3__Score_Snapshot__c,Do_not_route__c,RecordTypeId,infer3__Score_Index__c from lead  where recordtypeid!=:mktRTId];
        //Lead lead2 = new Lead (firstname = 'Mkt_Lead1New',D_u_n_s__c = '123456',CompanyDunsNumber='123456', mkto71_Lead_Score__c=78, infer3__Score_Snapshot__c=80, LastName='Lead1Test1',Company='TestCompany', PostalCode = '11111', OwnerID=UserInfo.getUserId(),Phone='3332223232', email='testogmail2@gmail.com',LeadSource='Web',infer3__Score_Index__c=1041,Pandora_Promotion__c='March 2015;June 2015;AAA',Pandora_B2B_Event__c='Advertising Week 2015;Trade Show;Seminar - Internal',Do_not_route__c=false,RecordTypeId=mktRTId); //tempOwnerID__c=testGroup2.id,  
        
        Lead lead1 = new Lead (firstname = 'TODAYCREATED001', LastName='TESTLEAD110',Company='TestCompany', PostalCode = '94538', OwnerID=UserInfo.getUserId(),Phone='3332223232', email='testLeadIS010@test.com',LeadSource='Web',infer3__Score_Index__c=1041,Pandora_Promotion__c='March 2015;June 2015;AAA',Pandora_B2B_Event__c='Advertising Week 2015;Trade Show;Seminar - Internal',Do_not_route__c=false,RecordTypeId=salesRTID); //tempOwnerID__c=testGroup2.id,  
        //insert lead2;
        test.startTest();
            insert lead1;
            lead lead2 = [select id, LastName,Company,PostalCode,OwnerID,LeadSource, Phone,email, infer3__Score_Snapshot__c,Do_not_route__c,RecordTypeId,infer3__Score_Index__c from lead where id=:lead1.Id];
            
            
            system.debug('C2ID = ' + lead2.id);
            lead2.infer3__Score_Snapshot__c = 100;
            lead2.mkto71_Lead_Score__c=80;
            
            
            lead2.Do_not_route__c = false;
            lead2.email='testogmail2@gmail.com';
            lead2.PostalCode = '11111';
            
            
            system.debug('C2 = ' + lead2);
        
            update lead2;
           
        test.stoptest();
    }*/
    
    /*public static  testMethod void UpdateMarketingLeadToConvertToInsideSales()
    {
    
        Schema.DescribeSObjectResult cfrSchema = Schema.SObjectType.Lead; 
        Map<String,Schema.RecordTypeInfo> LeadRecordTypeInfo = cfrSchema.getRecordTypeInfosByName();
        Id mktRTId = LeadRecordTypeInfo.get('Marketing Leads').getRecordTypeId();
        //lead lead2 = [select id, LastName,Company,mkto71_Lead_Score__c,PostalCode,OwnerID,LeadSource, Phone,email, infer3__Score_Snapshot__c,Do_not_route__c,RecordTypeId,infer3__Score_Index__c from lead  where recordtypeid=:mktRTId];
        Lead lead2 = new Lead (firstname = 'Mkt_Lead1New1',D_u_n_s__c = '123456',CompanyDunsNumber='123456', mkto71_Lead_Score__c=78, infer3__Score_Snapshot__c=80, LastName='Lead1Test11',Company='TestCompany', PostalCode = '11111', OwnerID=UserInfo.getUserId(),Phone='3332223232', email='testogmail2@gmail.com',LeadSource='Web',infer3__Score_Index__c=1041,Pandora_Promotion__c='March 2015;June 2015;AAA',Pandora_B2B_Event__c='Advertising Week 2015;Trade Show;Seminar - Internal',Do_not_route__c=false,RecordTypeId=mktRTId); //tempOwnerID__c=testGroup2.id,  
        insert lead2;
        system.debug('C2ID = ' + lead2.id);
        lead2.infer3__Score_Snapshot__c = 100;
        lead2.Do_not_route__c = false;
        lead2.mkto71_Lead_Score__c = 80;
        //lead2.email='testogmail3@gmail.com';
        //lead2.PostalCode = '11111';
        system.debug('C2 = ' + lead2);
        test.startTest();
           update lead2;
           
        test.stoptest();
    }*/
    
    public static  testMethod void mapAccountContactOnConvertTest()
    {
        test.starttest();
        Schema.DescribeSObjectResult cfrSchema = Schema.SObjectType.Lead; 
        Map<String,Schema.RecordTypeInfo> LeadRecordTypeInfo = cfrSchema.getRecordTypeInfosByName();
        Id salesRecTypeId = LeadRecordTypeInfo.get('Leads - Inside Sales').getRecordTypeId();
        System.debug('##### mapAccountContactOnConvertTest ');
        
        Lead lead1 = new Lead (LastName='testLead002',Company='TestCompany', OwnerID=UserInfo.getUserId(),Phone='3332223232', email='testLead002@comp.com',LeadSource='Web',infer3__Score_Index__c=90,Pandora_Promotion__c='March 2015;June 2015;AAA',Pandora_B2B_Event__c='Advertising Week 2015;Trade Show;Seminar - Internal', Cancel_Workflow__c=true, Do_not_route__c=true,RecordTypeId=salesRecTypeId);
        
        insert lead1;
        //lead lead1 = [select id,lastName,company,ownerId,phone,email,LeadSource,Pandora_Promotion__c,Pandora_B2B_Event__c,RecordTypeId from lead where recordtypeid=:salesRecTypeId limit 1];
        System.debug('samplele '+[select Id, Owner.Alias,RecordType.Name From Lead Where id =:lead1.id]);
        
        
            Database.LeadConvert lc = new database.LeadConvert();
            lc.setLeadId(lead1.id);
            
            LeadStatus convertStatus = [Select Id, MasterLabel from LeadStatus where IsConverted=true limit 1];
            lc.setConvertedStatus(convertStatus.MasterLabel);
            Database.LeadConvertResult lcr = Database.convertLead(lc);
        test.stoptest();
        System.debug('samplele lcr' + lcr);
            system.assert(lcr.isSuccess()==true);
            system.assert(lcr.getContactId()!=null);
            system.assert(lcr.getAccountId()!=null);
    
    }
    
    
    static testMethod void leadCreationTest() {

        // This code runs as the system user
        User u1;

        try{
          u1 = [select Id, Name,email from User WHERE IsActive=True AND Profile.Name = 'System Administrator'  LIMIT 1];
          } 
        catch (QueryException qe){
        }

        


         test.starttest();
        
            //Run test
            //Assignment_Group__c agTest1 = new Assignment_Group__c(Name = 'Test Group', Type__c = 'Lead');
            //insert agTest1;
            
            //Group_Member__c gmTest2 = new Group_Member__c(Name=u1.Name, Active__c = 'True', User__c = u1.Id);
            //insert gmTest2;
            
            //Group_Member__c gmTest3 = new Group_Member__c(Name=UserInfo.getName(), Active__c = 'True', User__c = UserInfo.getUserId());
            //insert gmTest3;
            
            
            
            Zip_Code__c zcTest2 = new Zip_Code__c(Active__c = 'True', Name = '33333');
            insert zcTest2;
            
            Zip_Code__c zcTest3 = new Zip_Code__c(Active__c = 'True', Name = '44444');
            insert zcTest3;
            
            //Group_Member_Zip_Junction__c gmzjTest2 = new Group_Member_Zip_Junction__c(Assignment_Group__c = agTest1.Id, Group_Member__c = gmTest2.Id, Zip_Code__c = zcTest2.Id);
            //insert gmzjTest2;
            
            //Group_Member_Zip_Junction__c gmzjTest3 = new Group_Member_Zip_Junction__c(Assignment_Group__c = agTest1.Id, Group_Member__c = gmTest3.Id, Zip_Code__c = zcTest3.Id);
            //insert gmzjTest3;  
            
            Lead_ZipState__c lz2 = new Lead_ZipState__c(Name='33333',State__c = 'Northwest');
            insert lz2;  
            Lead_ZipState__c lz3 = new Lead_ZipState__c(Name='44444',State__c = 'Northwest');
            insert lz3;    
            
            Custom_Constants__c cs2 = new Custom_Constants__c();
            cs2.Name='Lead Routing - Infer3__Infer_Rating__c';
            cs2.Value__c = 'A';
            insert cs2;
            Custom_Constants__c cs3 = new Custom_Constants__c();
            cs3.Name='Lead Routing - Infer3__Infer_Score__c';
            cs3.Value__c = '89';
            insert cs3;  
            
                        
            /*Custom_Constants__c mgrQueueName1 = new Custom_Constants__c();
            mgrQueueName1.Name='Lead Routing - Manager Queue Name';
            mgrQueueName1.Value__c = 'Manager "A+"';
            insert mgrQueueName1; 
            
            Group deletionQueue1 = new Group(type='Queue',Name='Leads - Deletion Queue');
            insert deletionQueue1;
            
            
            QueuesObject q2 = new QueueSObject(QueueID = deletionQueue1.id, SobjectType = 'Lead');
            System.runAs(new User(Id = UserInfo.getUserId())) {   
            insert q2;
            }*/
                          
            List<Lead> leads=new List<lead>();
            Lead ld1 = new Lead (LastName='testLead0001',Company='TestCompany', PostalCode = zcTest2.Name, OwnerID=UserInfo.getUserId(),Phone='3332223232', email='testLead00011@test.com',LeadSource='Web',infer3__Score_Index__c=1041,Pandora_Promotion__c='March 2015;June 2015;AAA',Pandora_B2B_Event__c='Advertising Week 2015;Trade Show;Seminar - Internal'); //tempOwnerID__c=testGroup2.id,  
            leads.add(ld1);
            //insert c1;
            
            Lead ld2 = new Lead (LastName='testLead2',Company='TestCompany2', PostalCode = zcTest3.Name, OwnerID=UserInfo.getUserId(),Phone='3332223233', email='testLead2@test.com',LeadSource='Web'); //tempOwnerID__c=testGroup2.id,  
            leads.add(ld2);
            //insert c2;
            
            Lead l3 = new Lead (LastName='testLead3',Company='TestCompany3', PostalCode = zcTest3.Name, OwnerID=UserInfo.getUserId(),Phone='3332223234', email='testLead3@test.com',LeadSource='Web',How_would_you_describe_your_marketplace__c='Local'); //tempOwnerID__c=testGroup2.id,  
            leads.add(l3);
            
            Lead l4 = new Lead (LastName='testLead4',Company='TestCompany4', PostalCode = zcTest2.Name, OwnerID=UserInfo.getUserId(),Phone='3332223235', email='testLead4@test.com',LeadSource='Web',Do_not_route__c=true,infer3__Score_Index__c=1041,Pandora_Promotion__c='March 2015;June 2015;AAA',Pandora_B2B_Event__c='Advertising Week 2015;Trade Show;Seminar - Internal'); //tempOwnerID__c=testGroup2.id,  
            leads.add(l4);
            
            Lead l5 = new Lead (LastName='testLead5',Company='Hillcrest HealthCare System & Lovelace Health System', PostalCode = zcTest2.Name, OwnerID=UserInfo.getUserId(),Phone='3332223235', email='testLead55@test.com',LeadSource='Web',Do_not_route__c=false,infer3__Score_Index__c=1041,Pandora_Promotion__c='March 2015;June 2015;AAA',Pandora_B2B_Event__c='Advertising Week 2015;Trade Show;Seminar - Internal'); //tempOwnerID__c=testGroup2.id,  
            leads.add(l5);
            
            insert leads;
            
            list<lead> leadList=  new list<lead>();
            Map<id,lead> leadUpdateMap=  new Map<id,lead>();
            leadList=[select id,infer3__Score_Snapshot__c from lead];
            for(lead ld:leadList)
            {
                ld.infer3__Score_Snapshot__c = 89;
            
                //leads.Add(ld1);
            
                //ld2.infer3__Score_Snapshot__c = 89;
            
                leadUpdateMap.put(ld.id,ld);//.Add(ld2);
            }    
            update leadUpdateMap.values();
            
            
            
            cs3.Value__c = '92';
            update cs3;
            //c1.PostalCode = zcTest.Name;
            //update c1;
            
             
            //mgrQueueName1.Value__c='';
            //update mgrQueueName1;
            
        
            test.stoptest();
        
    }
    
    //NEW COMMENTED
    /* 
    static testMethod void deletionRRtest()
    {
    
            // This code runs as the system user
        User u1;

        try{
          u1 = [select Id, Name,email from User WHERE IsActive=True AND Profile.Name = 'System Administrator'  LIMIT 1];
        } catch (QueryException qe){
        }

    
    
            //Run test
            Assignment_Group__c agTest = new Assignment_Group__c(Name = 'Test Group', Type__c = 'Lead');
            insert agTest;
            
            Group_Member__c gmTest = new Group_Member__c(Name=u1.Name, Active__c = 'True', User__c = u1.Id);
            insert gmTest;
            
            Group_Member__c gmTest1 = new Group_Member__c(Name=UserInfo.getName(), Active__c = 'True', User__c = UserInfo.getUserId());
            insert gmTest1;
            
            Zip_Code__c zcTest = new Zip_Code__c(Active__c = 'True', Name = '54304');
            insert zcTest;
            
            Zip_Code__c zcTest1 = new Zip_Code__c(Active__c = 'True', Name = '22222');
            insert zcTest1;
            
            Group_Member_Zip_Junction__c gmzjTest = new Group_Member_Zip_Junction__c(Assignment_Group__c = agTest.Id, Group_Member__c = gmTest.Id,
                                                                                Zip_Code__c = zcTest.Id);
            insert gmzjTest;
            
            Group_Member_Zip_Junction__c gmzjTest1 = new Group_Member_Zip_Junction__c(Assignment_Group__c = agTest.Id, Group_Member__c = gmTest1.Id,
                                                                                Zip_Code__c = zcTest1.Id);
            insert gmzjTest1;  
            
            Lead_ZipState__c lz = new Lead_ZipState__c(Name='54304',State__c = 'Northwest');
            insert lz;  
            Lead_ZipState__c lz1 = new Lead_ZipState__c(Name='22222',State__c = 'Northwest');
            insert lz1;    
            
             
            
            Group deletionQueue = new Group(type='Queue',Name='Leads - Deletion Queue');
            insert deletionQueue;
            
            
            QueuesObject q1 = new QueueSObject(QueueID = deletionQueue.id, SobjectType = 'Lead');
            System.runAs(new User(Id = UserInfo.getUserId())) {   
                insert q1;
            }
            
            
            
    
        List<Lead> leads=new List<lead>();
        Lead lead1 = new Lead (LastName='testLeadDELRR1',Company='TestCompany', PostalCode = zcTest.Name, OwnerID=UserInfo.getUserId(),Phone='3332223232', email='testLeadDel01@comp.com',LeadSource='Web',infer3__Score_Index__c=45,Pandora_Promotion__c='March 2015;June 2015;AAA',Pandora_B2B_Event__c='Advertising Week 2015;Trade Show;Seminar - Internal');

        Lead lead2 = new Lead (LastName='testLeadDELRR2',Company='TestCompany', PostalCode = zcTest.Name, OwnerID=UserInfo.getUserId(),Phone='3332223232', email='testLeadDel02@gmail.com',LeadSource='Sample',infer3__Score_Index__c=45,Pandora_Promotion__c='March 2015;June 2015;AAA',Pandora_B2B_Event__c='Advertising Week 2015;Trade Show;Seminar - Internal'); 
        
            leads.add(lead1);
            leads.add(lead2);
            
        insert leads;
        
        lead1.PostalCode=zcTest1.Name;
        
        update leads;
        
        
        List<Lead> deletionQueueleads = new List<lead>();
            deletionQueueleads.add(lead1);
         Map<Id,Lead> deletionQueueleadsMap = new Map<Id,lead>();
            deletionQueueleadsMap.put(lead1.Id, lead1);
        set<Id> deletionQueueleadIds= new set<Id> ();
            deletionQueueleadIds.add(lead1.Id);
        
            LeadTriggerHandler handler = new LeadTriggerHandler();
            handler.leadsForDeletionQueue.put(lead1.Id, lead1);
            //handler.LeadOwnerChange(deletionQueueleads,deletionQueueleadsMap);
            LeadTriggerHandlerHelper.applyAssignmentRule(deletionQueueleadIds);
        
        
        
    
    }
    */
    
    
    /*public static testMethod void bumpCoverageTest()
     {
         test.startTest();
            leadTriggerHandler  handler = new leadTriggerHandler();
            //handler.bumpCodeCoverage_1();
         test.stoptest();
         
     }
    */
    
    
    
}