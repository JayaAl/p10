public class LeadTriggerUtil{

public static id insideSalesRecordTypeId{get;set;}
public static id marketingRecordTypeId{get;set;}
public static id selfServiceRecordTypeId{get;set;}
public static string ssLeadsRecordType = 'Self-Service';
public static string mLeadsRecordType = 'Marketing Leads';
public static string sLeadsRecordType = 'Leads - Inside Sales';
public static final string PUBLIC_STRING_CONST_DELQUEUE = 'Leads - Deletion Queue';
public static final string PUBLIC_STRING_CONST_OOAQUEUE = 'Out Of Assignment Queue';
public static final string PUBLIC_STRING_CONST_UNASSIGNEDQUEUE = 'Unassigned Queue';

public static id getSalesRecordTypeId()
{
    insideSalesRecordTypeId = Schema.SObjectType.Lead.getRecordTypeInfosByName().get(sLeadsRecordType).getRecordTypeId();
    return insideSalesRecordTypeId;
}

public static id getMarketingRecordTypeId()
{
    marketingRecordTypeId = Schema.SObjectType.Lead.getRecordTypeInfosByName().get(mLeadsRecordType).getRecordTypeId();
    return marketingRecordTypeId;
}
public static id getSelfServiceRecordTypeId()
{
    selfServiceRecordTypeId = Schema.SObjectType.Lead.getRecordTypeInfosByName().get(ssLeadsRecordType).getRecordTypeId();
    return selfServiceRecordTypeId;
}

public static List<group> getGroups(List<String> lstQueueNames)
{
    system.debug('THIS IS STHE QUEUE' + lstQueueNames);
    List<Group> lstGroups = new list<Group>();
    lstGroups = [SELECT Id,Type,RelatedId,DeveloperName,Name,OwnerId FROM Group where Type='Queue' and Name in: lstQueueNames];
    return lstGroups;
}

}