global class Lead_ScheduleUpdateOnCancelConvert implements Schedulable {

   
   global void execute(SchedulableContext ctx) {
      List<Lead> listLead = [SELECT Id, Cancel_Workflow__c from Lead where Cancel_Workflow__c = true AND IsConverted = false];
      for(Lead objLead: listLead) {
          objLead.Cancel_Workflow__c = false;
      }
      if(! listLead.isEmpty()) {
          update listLead;
      }
   }   
}