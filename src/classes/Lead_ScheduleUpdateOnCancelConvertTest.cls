@istest(seealldata=false)
public class Lead_ScheduleUpdateOnCancelConvertTest{

   private static testmethod void testScheduler() {
            Lead l = new Lead(LastName='Last Name Test', Company='Company', HasOptedOutOfEmail=false, LeadSource='Web', HasOptedOutOfFax=false, AnnualRevenue=1000, Cancel_Workflow__c = true);
            insert l;
            Test.startTest();
            // Schedule the test job
            
            String jobId = System.schedule('testBasicRestWorkflow',
            '0 0 0 1 9 ? 2022', 
             new Lead_ScheduleUpdateOnCancelConvert ());
            
            // Get the information from the CronTrigger API object
            CronTrigger ct = [SELECT Id, CronExpression, TimesTriggered, 
             NextFireTime
             FROM CronTrigger WHERE id = :jobId];
            
            // Verify the expressions are the same
            System.assertEquals('0 0 0 1 9 ? 2022', 
             ct.CronExpression);
            
            // Verify the job has not run
            System.assertEquals(0, ct.TimesTriggered);
            
            // Verify the next time the job will run
            System.assertEquals('2022-09-01 00:00:00', 
             String.valueOf(ct.NextFireTime));
            
            Test.stopTest();
    }
    
}