public with sharing class LightningWrapper {
	
	@AuraEnabled
	public String caseRecordType;
	public String getCaseRecordType(){
		return caseRecordType;
	}
	public void setCaseRecordType(String value){
		caseRecordType =  value;
	}
	@AuraEnabled
	public Case caseObj;
	@AuraEnabled
	public String adOpsName;
	/*global Case getCaseObj() {
		return caseObj;
	}
	global void setCaseObj(Case value) {

		caseObj = value;
	}*/
	public LightningWrapper() {
		
	}
	public LightningWrapper(Case caseRec, String caseRecType, String adOpsName) {
		
		this.caseRecordType = caseRecType;
		this.caseObj = caseRec;
		this.adOpsName = adOpsName;
	}
}