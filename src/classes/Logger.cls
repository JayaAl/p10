/*
   Author: Srikanth Nelli
   Ref: Apex code samples
   Description: Purpose of this class is to log the custom error messages which can be later reported or reviewed. Note that this will add an overhead on the overall transaction
and must be used for limited period of time to track down intermittent issues.

Note: Some implementations considerations
1) Future methods are not used to avoid the salesforce limitations
2) Custom label (Enable_Error_Log) is used to start/stop error logging
*/
public class Logger{
    public static void logMessage(String className, String methodName, String message, String objectName, String IdVal, String typeVal){
        /*
            This if block ensures that INSERT that we are doing shouldn't disturb the GOVERNOR LIMITS
        */
        
        String enableELog = Label.Enable_Error_Log;
        if (enableELog == 'YES'){
            if ((Limits.getDMLRows() < Limits.getLimitDMLRows()) && (Limits.getDMLStatements() < Limits.getLimitDMLStatements()))
            {              
                Error_Log__c newDebugMsg =
                    new Error_Log__c(
                        Class__c = className,
                        Method__c = methodName,
                        Error__c = message,
                        Object__c = objectName,
                        ID_List__c = IdVal,
                        Running_user__c = UserInfo.getUserId(),
                        Type__c = typeVal
                    );
    
                try{
                    INSERT newDebugMsg;
                }
                catch (Exception ex){
                    System.debug(
                        'Failed to INSERT the [Error Log] ELog record. ' +
                        'Error: ' + ex.getMessage()
                    );
                }
            }
            else {
                System.debug(
                    'Failed to INSERT the [Error Log] ELog record. ' +
                    'Error: The APEX RUNTIME GOVERNOR LIMITS has been exhausted.'
                );
            }//End of gov limit check
        }//End of enableLog check
    }

    public static void logMessages(List<Error_Log__c> eLogList){
        String enableELog = Label.Enable_Error_Log;
        if(enableELog == 'YES'){
            if ((Limits.getDMLRows() < Limits.getLimitDMLRows()) && (Limits.getDMLStatements() < Limits.getLimitDMLStatements()))
            {              
                try{
                    INSERT eLogList;
                }
                catch (Exception ex){
                    System.debug(
                        'Failed to INSERT the [Error Log] ELog record. ' +
                        'Error: ' + ex.getMessage()
                    );
                }
            }
            else {
                System.debug(
                    'Failed to INSERT the [Error Log] ELog record. ' +
                    'Error: The APEX RUNTIME GOVERNOR LIMITS has been exhausted.'
                );
            }
        }
    }
}