/**
* ─────────────────────────────────────────────────────────────────────────────────────────────────┐
* Controller for CustomLookup lightning componenet.
* ──────────────────────────────────────────────────────────────────────────────────────────────────
* @author         Jaya Alaparthi
* @maintainedBy   Jaya Alaparthi
* @version        1.0
* @created        2017-12-05
* @modified       
* @systemLayer    Helper
* @see            
* @see            ????
* ──────────────────────────────────────────────────────────────────────────────────────────────────
* @changes
* v1.1          Jaya Alaparthi
* YYYY-MM-DD    
* ──────────────────────────────────────────────────────────────────────────────────────────────────
* @changes
* v1.1          Jaya Alaparthi
* YYYY-MM-DD       
* ─────────────────────────────────────────────────────────────────────────────────────────────────┘
*/
public with sharing class LookupController {
	
	//───────────────────────────────────────────────────────────────────────────┐
	// param		type					
	// searchKey	String passing in user keyed search name.
	// lookupObj    SObject that we have query on. 
	//				This is passed from lightning componenet
	// ──────────────────────────────────────────────────────────────────────────
	// Description: getCurrentUserId returns current userId						
	//───────────────────────────────────────────────────────────────────────────┘
	@AuraEnabled
	public static list<User> getUsers(String searchKey, 
									String lookupObj) {

		searchKey = '%'+String.escapeSingleQuotes(searchKey)+'%';
		String searchQuery = 'SELECT Id,Name FROM User WHERE Name LIKE: searchKey';
		System.debug('searchQuery:'+searchQuery);
		list<User> usersList = Database.query(searchQuery);
		System.debug('usersList:'+usersList);
		return usersList;
	} 
	//───────────────────────────────────────────────────────────────────────────┐
	// param		type					
	// searchKey	String passing in user keyed search name.
	// lookupObj    SObject that we have query on. 
	//				This is passed from lightning componenet
	// ──────────────────────────────────────────────────────────────────────────
	// Description: getCurrentUserId returns current userId						
	//───────────────────────────────────────────────────────────────────────────┘
	@AuraEnabled
	public static User getCurrentUser() {

		return NonGuaranteedHelper.getCurrentUserId(); 
	}
}