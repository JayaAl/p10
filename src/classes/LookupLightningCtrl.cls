/**
* ─────────────────────────────────────────────────────────────────────────────────────────────────┐
* This class services lightning event for lookup.
* ──────────────────────────────────────────────────────────────────────────────────────────────────
* @author         Jaya Alaparthi
* @maintainedBy   Jaya Alaparthi
* @version        1.0
* @created        2017-06-02
* @modified       
* @systemLayer    Service
* @see            ????
* @see            ????
* ──────────────────────────────────────────────────────────────────────────────────────────────────
* @changes
* vX.X            Jaya Alaparthi
* YYYY-MM-DD      Each change to this file should be documented by incrementing the version number,
*                 and adding a new entry to this @changes list. Note that there is a single blank
*                 line between each @changes entry.
* ─────────────────────────────────────────────────────────────────────────────────────────────────┘
*/
public with sharing class LookupLightningCtrl {
	
	@AuraEnabled
	public static List<Account> getAccount(String searchKeyWord) {

		String searchKey = searchKeyWord + '%';
		List<Account> returnList = new List<Account>();
		List<Account> lstOfAccount = [SELECT id, Name FROM Account 
										WHERE Name LIKE: searchKey];
		for (Account acc: lstOfAccount) {

			returnList.add(acc);
		}
		return returnList;
	}
	@AuraEnabled
	public static List<User> getUser(String searchKeyWord) {

		String searchKey = searchKeyWord + '%';
		List<User> returnList = new List<User>();
		List<User> lstOfUser = [SELECT id, Name FROM User 
										WHERE Name LIKE: searchKey];
		for (User acc: lstOfUser) {

			returnList.add(acc);
		}
		return returnList;
	}
	
}