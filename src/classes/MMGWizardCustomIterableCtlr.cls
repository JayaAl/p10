global class  MMGWizardCustomIterableCtlr implements Iterator<list<MMGWizardWrapper>>
{
	list<MMGWizardWrapper> InnerList{get; set;}
	list<MMGWizardWrapper> ListRequested{get; set;}
	Integer count {get; set;} 
	public Integer pageSize {get; set;} 

	public MMGWizardCustomIterableCtlr(List<MMGWizardWrapper> listContWrapper) {
		InnerList = new list<MMGWizardWrapper>(); 
		ListRequested = new list<MMGWizardWrapper>();     
		InnerList = listContWrapper;
		pageSize = 10;
		count = 0;
	}
	// Verify if search result has more than 10 records.
	global boolean hasNext(){ 
		if(count >= InnerList.size()) {
			return false; 
			} else {
				return true; 
			}
		} 
       // Verify if the search result has more than 10 records
		global boolean hasPrevious(){ 
			if(count <= pageSize) {
				return false; 
				} else {
					return true; 
				}
			}   
             // Returns if the search has more than 10 values in the list
			global list<MMGWizardWrapper> next(){       
				ListRequested = new list<MMGWizardWrapper>(); 
				integer startNumber;
				integer size = InnerList.size();
				if(hasNext())
				{  
					if(size <= (count + pageSize)){
						startNumber = count;
						count = size;
					}
					else{
						count = (count + pageSize);
						startNumber = (count - pageSize);
					}
					for(integer start = startNumber; start < count; start++){
						if(start >= 0){
							ListRequested.add(InnerList[start]);
						}
					}
				} 
				return ListRequested;
			} 
            // Returns if the search has more than 10 values in the list
			global list<MMGWizardWrapper> previous(){      
				ListRequested = new list<MMGWizardWrapper>(); 
				integer size = InnerList.size(); 
				if(count == size){
					if(math.mod(size, pageSize) > 0)	{    
						count = size - math.mod(size, pageSize);
					}
					else{
						count = (size - pageSize);
					} 
				}
				else{
					count = (count - pageSize);
				}


				for(integer start = (count - pageSize); start < count; ++start){
					if(start >= 0){
						ListRequested.add(InnerList[start]);
					}
				} 
				return ListRequested;
			}  
		}