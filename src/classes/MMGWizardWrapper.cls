global class MMGWizardWrapper {
	 public Contact cont {get; set;}
     public Boolean isSelected {get; set;}
	
	public MMGWizardWrapper(Contact cont) {
		   this.cont = cont;
           this.isSelected = false;
	}
}