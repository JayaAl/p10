/**
* ─────────────────────────────────────────────────────────────────────────────────────────────────┐
* MMG_Wizard vf page controller.
* ──────────────────────────────────────────────────────────────────────────────────────────────────
* @author         
* @maintainedBy   
* @version        1.0
* @created        
* ──────────────────────────────────────────────────────────────────────────────────────────────────
* @changes
* v1.1            Jaya Alaparthi
* 2017-02-28      Bug Fixes: ESS-35933
*
* vX.X            Jaya Alaparthi
* YYYY-MM-DD      Each change to this file should be documented by incrementing the version number,
*                 and adding a new entry to this @changes list. Note that there is a single blank
*                 line between each @changes entry.
* ─────────────────────────────────────────────────────────────────────────────────────────────────┘
*/
public with sharing class MMG_WizardCtlr {
    public Account account;
    public Contact contact;
    public AccountContactRelation accContactRelation;
    public Account acc{get;set;}    
    public Boolean isContactWizEnabled{get;set;}
    public Boolean accountTypeWizard {get;set;}
    public Boolean accountWizard {get;set;}
    public Boolean contactWizard {get;set;}
    public Boolean relationshipWizard {get;set;}
    public Boolean contactTable {get;set;} 
    public Boolean accountTypeWizardView {get;set;}
    public Boolean accountWizardDetail {get;set;}
    public Boolean contactWizardDetail {get;set;}
    public Boolean isRecordSaved {get;set;}
    public Boolean searchBox {get;set;}
    public Boolean contactSearch { get; set;} 
    public Boolean accountSearch {get;set;}
    public Boolean hasNextVal {get;set;}   
    public Boolean hasPreviousVal {get;set;} 
    public Boolean countStepNo {get;set;} 
    public String searchstring { get; set;} 
    public String selectedAccountId{get;set;}
    public String selectedType {get;set;}    
    public String parentAccountName {get;set;}
    public List<MMGWizardWrapper> listContWrapper {get;set;}
    public List<MMGWizardWrapper> listSetController{get;set;}
    public MMGWizardCustomIterableCtlr iterateCont{get;set;}      
    public Integer pageSize = 10;
    public Integer searchResultSize {get;set;}
    // This method controls the wizard visibility 
    public void init(){
        contactWizardDetail = false;
        accountWizardDetail = false;
        accountTypeWizardView = false;
        contactTable = false;
        accountTypeWizard = false;
        accountWizard = false;
        contactWizard = false;
        relationshipWizard = false;
        searchBox = false;
    }
    
    public MMG_WizardCtlr() {
        searchstring = '';
        isContactWizEnabled = False;
        listContWrapper =  new List<MMGWizardWrapper>();
        listSetController = new List<MMGWizardWrapper>();
        accountType();
    }
    
    public void accountType(){
        init();
        account = Null;
        accountTypeWizard = true;
    }
    
    //Prepare for new account record.
    public Account getAccount() {
        if(account == null) account = new Account();
        account.RecordTypeId = getRecordTypeId('Account','Music Makers Group');
        return account;
    }
    
    // Prepare for new contact record.
    public Contact getContact() {
        if(contact == null) contact = new Contact();
        contact.RecordTypeId = getRecordTypeId('Contact','Music Makers Group');
        return contact;
    }
    // prepare new one or list ACR.
    public AccountContactRelation getAccountContactRelation() {
        if(accContactRelation == null) accContactRelation = new AccountContactRelation();
        return accContactRelation;
    }
    
    // Step 2 - Account Information wizard Visibility controled by this method
    public void accountStep1() {
        init();
        contact= Null;
        isContactWizEnabled = false;
        accountTypeWizardView = true;
        accountWizard = true;
    }
    // Step 3 - Contact wizard Visibility controled by this method
    public void contactStep2() {
        init();   
        if(!isAccountExists()){
            countStepNo =true;
            accountTypeWizardView = true;
            accountWizardDetail = true;
            contactWizard = true;
            getParentAccountName();
        }else {
            accountTypeWizardView = true;
            accountWizard = true;
            ApexPages.addmessage(new ApexPages.message(ApexPages.severity.ERROR,'This Account name is already exists. Please enter a different Account name.'));
            
        }
        
    }
    // Step 4 - Add Contact to Account relationship visibility controlled by this method.
    public void relationshipStep3() {
        init();
        isContactWizEnabled = True;
        countStepNo = false;
        accountTypeWizardView = true;
        accountWizardDetail = true;
        contactWizardDetail = true;
        acc = new  Account();
        relationshipWizard = true;
    }
    
    // Give input object and record type name to get the recordtypeid
    public static Id getRecordTypeId(String obj, String recType) {
        Id mmgRecordId = Schema.getGlobalDescribe().get(obj).getDescribe()
            .getRecordTypeInfosByName().get(recType)
            .getRecordTypeId();
        return mmgRecordId;
    }
    // This method creates Accounts, Contacts and Account Contact Relationship beased on the business requirements
    // Requirements : 
    // 1. Account Type : Artist, Venue,Label,Management_Company,Promotion_Company,Agency
    // 2. If the account type = Artist || Venue, create contact automattically with account information
    // such as Account Name TO Contact Name and Account Type TO Contact Role Type(values are defined dynamically)
    // 3. If the account type = Label,Management_Company,Promotion_Company,Agency - allow user to create a contact manually.
    // 4. Add account contact relationship if user has selected any checkbox from UI.
    public void save(){
        isRecordSaved = false;
        Datetime currentRcdCreatedDate ;
        account.RecordTypeId =getRecordTypeId('Account','Music Makers Group');
        try{
            List<Contact> selectedContacts = new List<Contact>();
            // Get the selected contact records from search recults via wrapper
            for(MMGWizardWrapper wrapperCon: listContWrapper) {
                if(wrapperCon.isSelected == true) {
                    selectedContacts.add(wrapperCon.cont);
                }
            }
            if(selectedContacts.size() > 0 && selectedContacts != Null){
                if((!isAccountExists()) && account.CreatedDate == currentRcdCreatedDate ){
                    insert account;
                    currentRcdCreatedDate = account.CreatedDate; 
                    insertContact();
                } 
                
                if(!isACRExists(selectedContacts)){
                    insertSelectedRecords(selectedContacts);
                } else{
                    ApexPages.addmessage(new ApexPages.message(ApexPages.severity.WARNING, 'The selected contact already has a relationship with this account'));
                    
                }    
            }else{
                ApexPages.addmessage(new ApexPages.message(ApexPages.severity.WARNING,'Please select atleast one record to create a relationship.'));
            }
            
        }    
        catch(Exception ex){
            ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, ex.getMessage() +ex.getLineNumber()));          
        }  
    }
    public void insertContact() {
        
        // v1.1 retrive MMG record type for contact
        Id contactRecordTypeId = getRecordTypeId('Contact','Music Makers Group');
        if(contact != null){
            // v1.1 contact.RecordTypeId = getRecordTypeId('Contact','Music Makers Group');
            contact.RecordTypeId = contactRecordTypeId;
            // Verify the entered contact is exists in the datbase
            List<Contact> existingCon = [SELECT Name 
                                         from Contact 
                                         WHERE Name =: contact.Name AND AccountId =:account.id];
            if(existingCon.size() == 0){
                contact.accountId = account.id;
                insert contact;
            }
            // Create contact automatically if the selected account type is 'Artist', 'Venue'
            // The above type accounts we call as a persons account, remaing MMG account type is called Business account.
            // We don't create contact manually for the business account
        }else if(contact == null & (account.Type == 'Artist' || account.Type == 'Venue')){
            String contRole = contactRoleType(account.Type);
            // v1.1 added contact record type as MMG
            contact cont = new contact(accountId = account.id,
                                        lastname = account.name,
                                        Role__c = contRole,
                                        RecordTypeId = contactRecordTypeId);
            insert cont;
        } 
        
        
        
    }
    // Insert if user selects any records.
    public void insertSelectedRecords(List<Contact> selectedContacts){
        List<AccountContactRelation> acrList = new List<AccountContactRelation>();
        for(Contact con: selectedContacts) {
            AccountContactRelation accContactRelation = new AccountContactRelation();
            accContactRelation.AccountId = account.id;
            accContactRelation.ContactId = con.id;
            acrList.add(accContactRelation);
        }
        insert acrList;
        isRecordSaved = true;
        ApexPages.addmessage(new ApexPages.message(ApexPages.severity.CONFIRM, ''+selectedType+' '+'relationship has added successfully!'));
        
    }
    
    // ACR - Account Contact Relationship       
    private Boolean isACRExists(List<Contact> selectedContacts){
        Set<Id> idList = new Set<Id>();
        Boolean isRelationshipExists = false;
        for(Contact cont: selectedContacts){
            idList.add(cont.id);
        }
        List<AccountContactRelation> contList = [SELECT Id, Contact.Name 
                                                 from AccountContactRelation where ContactId IN: idList AND AccountId =: account.id];
        
        if(contList.size() > 0 && contList != null){
            isRelationshipExists = true;
        }else {
            isRelationshipExists = false;
        }
        return isRelationshipExists;
    }
    public String contactRoleType(String accType){
        String role= '';
        if(accType == 'Artist'){
            role ='Artist';
        } else if(accType == 'Venue'){
            role ='Venue';
        }
        return role;
    }
    // This method take users to account detail view page.
    public PageReference done(){
        save();
        if(isRecordSaved == True){
            PageReference accountPage = new ApexPages.StandardController(Account).view();
            accountPage.setRedirect(true);
            return accountPage;
        } else{
            return null;
        }
        //else {
        //    ApexPages.addmessage(new ApexPages.message(ApexPages.severity.INFO,'Please select atleast one record to create a relationship.'));
        //    return null;
        //}
        
    }
    // Clear all the values upon Cancel button Click
    public void cancel() {
        init();
        account = Null;
        contact = Null;
        accountTypeWizard = true;
        clear();
        
    }
    
    public PageReference searchBoxView(){
        init();
        searchBox = true;
        return null;
    }
    //set contact role 
    public List<SelectOption> getContactRole(){
        List<SelectOption> role = new List<SelectOption>();
        if(account.Type == 'Label'){
            role.add(new SelectOption('Select','--Select--'));
            role.add(new SelectOption('Artist','Artist'));
            role.add(new SelectOption('Agent','Agent'));
            role.add(new SelectOption('Manager','Manager')); 
            role.add(new SelectOption('Promoter','Promoter'));
            role.add(new SelectOption('Other','Other'));
            role.add(new SelectOption('Venue','Venue'));
        } else if(account.Type == 'Artist'){
            role.add(new SelectOption('Select','--Select--'));
            role.add(new SelectOption('Agent','Agent'));
            role.add(new SelectOption('Label','Label'));
            role.add(new SelectOption('Manager','Manager')); 
            role.add(new SelectOption('Promoter','Promoter'));
            role.add(new SelectOption('Other','Other'));
            role.add(new SelectOption('Venue','Venue'));
        } else if(account.Type == 'Management Company') {
            role.add(new SelectOption('Select','--Select--'));
            role.add(new SelectOption('Artist','Artist'));
            role.add(new SelectOption('Agent','Agent'));
            role.add(new SelectOption('Label','Label'));
            // v1.1 adding Manager in case account Type is Management Company
            role.add(new SelectOption('Manager','Manager')); 
            role.add(new SelectOption('Promoter','Promoter'));
            role.add(new SelectOption('Other','Other'));
            role.add(new SelectOption('Venue','Venue'));
        } else if(account.Type == 'Promotion Company'){
            role.add(new SelectOption('Select','--Select--'));
            role.add(new SelectOption('Artist','Artist'));
            role.add(new SelectOption('Agent','Agent'));
            role.add(new SelectOption('Label','Label'));
            role.add(new SelectOption('Manager','Manager')); 
            role.add(new SelectOption('Other','Other'));
            role.add(new SelectOption('Promoter','Promoter'));
            role.add(new SelectOption('Venue','Venue'));
        } else if(account.Type == 'Venue'){
            role.add(new SelectOption('Select','--Select--'));
            role.add(new SelectOption('Artist','Artist'));
            role.add(new SelectOption('Agent','Agent'));
            role.add(new SelectOption('Label','Label'));
            role.add(new SelectOption('Manager','Manager')); 
            role.add(new SelectOption('Promoter','Promoter'));
            role.add(new SelectOption('Other','Other'));
        } else if(account.Type == 'Other'){
            role.add(new SelectOption('Select','--Select--'));
            role.add(new SelectOption('Artist','Artist'));
            role.add(new SelectOption('Agent','Agent'));
            role.add(new SelectOption('Label','Label'));
            role.add(new SelectOption('Manager','Manager')); 
            role.add(new SelectOption('Promoter','Promoter'));
            role.add(new SelectOption('Venue','Venue'));
        }else if(account.Type == 'Agency') {
            role.add(new SelectOption('Select','--Select--'));
            role.add(new SelectOption('Artist','Artist'));
            role.add(new SelectOption('Label','Label'));
            role.add(new SelectOption('Manager','Manager')); 
            role.add(new SelectOption('Promoter','Promoter'));
            role.add(new SelectOption('Other','Other'));
            role.add(new SelectOption('Venue','Venue'));
        }
        return role;
    }
    
    
    // this method is callled by Relationship Wizard Previous button
    // Control the Step 3 / 4 wizard based on what user clicks on the account type wizard buttons
    public void showContactWizard(){
        init(); 
        if(isContactWizEnabled){
            accountTypeWizardView = true;
            accountWizardDetail = true;
            contactWizard = true;
        }else {
            accountTypeWizardView = true;
            accountWizard = true;
        }
        
        clear();
    }
    
    public void clear(){
        searchstring ='';
        selectedType = '';
        listContWrapper = Null;
        listSetController = Null;
        iterateCont = Null;
        contactSearch = False;
        accountSearch = False;
    }
    // This method is called upon Add Relatioship Button click on the account wizard page
    public void addRelationship(){
        init();
        if(!isAccountExists()){
            countStepNo = true;
            searchstring = '';
            // Get the parent name
            getParentAccountName();
            accountTypeWizardView = true;
            accountWizardDetail = true;
            relationshipWizard = true;
        }else{
            accountTypeWizardView = true;
            accountWizard = true;
            ApexPages.addmessage(new ApexPages.message(ApexPages.severity.ERROR,'This Account name is already exists. Please enter a different Account name.'));
        }
        
    }
    // Verify the entered account is exists in the database
    
    public Boolean isAccountExists(){
        Boolean isAcctExists = false;
        List<Account> existingAcc =[SELECT Id,Name  
                                    from Account 
                                    WHERE Name =:account.Name];
        if(existingAcc.size() == 0 && existingAcc != Null){                          
            isAcctExists = false;
        }else {
            isAcctExists = true;
        }
        return isAcctExists;
    }
    public void getParentAccountName(){
        if(account.ParentId != Null){
            Account acct = [select name from Account  where id =: account.ParentId];
            parentAccountName = acct.Name;
        }else {
            parentAccountName = '';
        }
    }
    
    // This method called by upon user search on the Add Relatioship wizard step.
    // This method calls MMGWizardWrapper, MMGWizardCustomIterableCtlr class to add more than one contact and pagination.
    // Allow user to enter minmum 3 charters
    public void getContacts() {
        listContWrapper =  new List<MMGWizardWrapper>();
        listSetController = new List<MMGWizardWrapper>();
        string searchquery ='Select Name, role__C, Contact.Account.Name from Contact';
        try{
            if(selectedType !=null && searchstring.length() >= 3){
                if(contactSearch == True && accountSearch != True){
                    //Search by Contact Name
                    searchquery +=' where(role__C =:selectedType AND Name like \'%'+searchstring+'%\')Order By Name '; 
                }else if(contactSearch != True && accountSearch == True){
                    // Search by Account Name
                    searchquery +=' where(role__C =:selectedType AND Contact.Account.Name like \'%'+searchstring+'%\')Order By Name '; 
                }else if((contactSearch == True && accountSearch == True) ||
                         (contactSearch == False && accountSearch == False)){
                             // Search by Account and Contact Name
                             searchquery +=' where (role__C =:selectedType AND name like\'%'+searchstring+'%\') OR (role__C =:selectedType AND Contact.Account.Name like \'%'+searchstring+'%\')Order By Name '; 
                         }   
                if(searchquery != Null){
                    List<Contact> contactList = Database.query(searchquery);
                    // searchResultSize = contactList.size();
                    //Pass all the search result to wrapper class thorugh iteration for multi selection
                    for(Contact c:contactList) {
                        listContWrapper.add(new MMGWizardWrapper(c));
                    }
                    //Pass wrapper output to the below class to get records for pagination 
                    iterateCont = new MMGWizardCustomIterableCtlr(listContWrapper);
                    iterateCont.pageSize = pageSize;
                    // Control the initial view of the search result page block table.
                    // Page size set 10 records/page
                    // If the page doesn't have <10 then Pagination button wont dispaly
                    if(listContWrapper.size() >pageSize && listContWrapper != Null){
                        hasNextVal = True;
                        hasPreviousVal = True;
                    }else {
                        hasNextVal = False; 
                        hasPreviousVal = False;
                    }
                    next();       
                }
            } else{
                ApexPages.addmessage(new ApexPages.message(ApexPages.severity.INFO,'Please select a Contact Role and enter minimum 3 charaters in Name field to search.'));
            }
            searchstring = '';
        } catch(Exception ex){
            ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, ex.getMessage()));          
        }
    }
    // Get next 10 records if the search has more than 10 records.
    public void next() 
    {  
        List<MMGWizardWrapper> contWrap = iterateCont.next();
        if(contWrap != Null && contWrap.size() >0){
            listSetController = contWrap;
        }
    }
    // Get the previous 10 records if the pae has more than 10 records.
    public void previous() 
    {
        List<MMGWizardWrapper> contWrap = iterateCont.previous();
        if(contWrap != Null && contWrap.size() >0){
            listSetController = contWrap;
        }
        
    }
    
    
}