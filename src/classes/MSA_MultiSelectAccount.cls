public class MSA_MultiSelectAccount {
    // Selectoptions for picklist
    public List<SelectOption> accountOptions {get;set;}
    // User selection from UI
    public List<Id> selectedaccountIds {get;set;}
    public List<Account> accounts;
    public string soql;
    private Master_Service_Agreement__c objMSA; 
    // the current sort direction. defaults to asc
      public String sortDir {
        get  { if (sortDir == null) {  sortDir = 'asc'; } return sortDir;  }
        set;
      }
     
      // the current field to sort by. defaults to name
      public String sortField {
        get  { if (sortField == null) {sortField = 'name'; } return sortField;  }
        set;
      }

    //Default constructor
    public MSA_MultiSelectAccount() {
        this.objMSA= [Select Id,
                             /*WLI__c,
                             Payment__c,
                             Guaranteed__c,
                             Performance__c,
                             Flat_Fee_Fixed__c,
                             Reference_Text__c,
                             Editorial_Adjacency__c,
                             Competitive_Separation__c,
                             User_Generated_Content__c,*/
                             Makegood_Penalty__c,
                             Revenue_Split__c,
                             Reference_Text__c
                             //Choice_of_Law__c
                        from  Master_Service_Agreement__c where Id =: ApexPages.currentPage().getParameters().get('id')];
        accountOptions = new List<SelectOption>();
        accounts = new List<Account>();
        
    }

    // runs the actual query
    public void runQuery() {
     
      try {
        accounts = Database.query(soql + ' order by ' + sortField + ' ' + sortDir + ' limit 20');
        //ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.INFO, soql));
      } catch (Exception e) {
        ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, e.getMessage()));
        ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, soql));
      }
     
    }    
    
    //search for accounts
    public void searchAccounts() {
        String accountName = ApexPages.currentPage().getParameters().get('accountName');
        soql = 'Select Id, Name from Account where Name != null AND Id Not In(Select Account_Name__c From MSA_Junction__c where Master_Service_Agreement__c =\''+objMSA.Id+'\')';
        if(accountName != null && accountName != '') 
        soql += ' AND Name Like \'%'+String.escapeSingleQuotes(accountName)+'%\'';      
        runQuery();
        accountOptions.clear();
        for (Account a : accounts) {
            accountOptions.add(new SelectOption(a.Id, a.Name));
        }
    }
    

    public PageReference saveSelection() {
        if (selectedaccountIds != null && !selectedaccountIds.isEmpty()) {
            List<MSA_Junction__c> listMSAJunction = new List<MSA_Junction__c>();
            for(Id accId: selectedaccountIds) {
                MSA_Junction__c msaJ = new MSA_Junction__c();
                msaJ.Master_Service_Agreement__c =  objMSA.Id;
                msaJ.Account_Name__c = accId;
                /*msaJ.WLI__c = objMSA.WLI__c;
                msaJ.Payment__c = objMSA.Payment__c;
                if(objMSA.Guaranteed__c <> NULL)
                msaJ.Cancellation_CPM__c = objMSA.Guaranteed__c + ' Hours';
                if(objMSA.Performance__c <> NULL)
                msaJ.Cancellation_CPA__c = objMSA.Performance__c + ' Days';
                if(objMSA.Flat_Fee_Fixed__c == null)
                msaJ.Cancellation_Fixed__c = '0';
                msaJ.EA__c = objMSA.Editorial_Adjacency__c;
                msaJ.Competitive_Separation__c = objMSA.Competitive_Separation__c;
                msaJ.UGC__c = objMSA.User_Generated_Content__c;*/
                msaJ.Revenue__c = objMSA.Revenue_Split__c;
                msaJ.Reference__c = objMSA.Reference_Text__c;
                //msaJ.Choice_of_Law__c = objMSA.Choice_of_Law__c;
                
                
                
                
                listMSAJunction.add(msaJ);
            }
            insert listMSAJunction;
        }
        String newPageUrl = '/'+objMSA.Id;
        PageReference newPage = new PageReference(newPageUrl);
        newPage.setRedirect(true);
        return newPage;
    }
    
}