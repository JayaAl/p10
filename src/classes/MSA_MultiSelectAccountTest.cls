@isTest
public class MSA_MultiSelectAccountTest {
    public static testMethod void testMSA_MultiSelectAccount () {
        Master_Service_Agreement__c msa = new Master_Service_Agreement__c (Name = 'Test MSA');
        insert msa;
        
        Account acc = UTIL_TestUtil.createAccount();
        ApexPages.currentPage().getParameters().put('accountName', acc.Name);
        ApexPages.currentPage().getParameters().put('id', msa.Id);
        
        Test.startTest();
        MSA_MultiSelectAccount testMSA = new MSA_MultiSelectAccount();
        testMSA.searchAccounts();
        testMSA.selectedaccountIds = new  List<Id> {acc.Id};
        testMSA.saveSelection();
        
        Test.stopTest();
    
    }
}