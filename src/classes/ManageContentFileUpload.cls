/**
* ─────────────────────────────────────────────────────────────────────────────────────────────────┐
* Opportunity Related Products
* 
* This class is servr side controler for Manage content componenet.
* also refer to ManageContentFileUploadHelper.cls
* ──────────────────────────────────────────────────────────────────────────────────────────────────
* @author         Jaya Alaparthi
* @maintainedBy   Jaya Alaparthi
* @version        1.0
* @created        2017-03-06
* @modified       YYYY-MM-DD
* @systemLayer    Service
* @see            ????
* @see            ????
* ──────────────────────────────────────────────────────────────────────────────────────────────────
* @changes
* vX.X            Jaya Alaparthi
* YYYY-MM-DD      Explanation of the change.  Multiple lines can be used to explain the change, but
*                 each line should be indented till left aligned with the previous description text.
*
* vX.X            Jaya Alaparthi
* YYYY-MM-DD      Each change to this file should be documented by incrementing the version number,
*                 and adding a new entry to this @changes list. Note that there is a single blank
*                 line between each @changes entry.
* ─────────────────────────────────────────────────────────────────────────────────────────────────┘
*/
public with sharing class ManageContentFileUpload {

    //─────────────────────────────────────────────────────────────────────────┐
    // getExistingContent: Retrive related content for the current record.
    // @param String  selectedSearchFolder default to All 
    // @param String  lookupFieldName default to All 
    // @param Id  currentRecordId current record Id
    // @param Boolean  viewAll
    // @return list of ContentVersion
    //─────────────────────────────────────────────────────────────────────────┘
    @AuraEnabled
	public static List<ContentVersion> getExistingContent(String selectedSearchFolder,
														  String lookupFieldName,
														  Id currentRecordId,
														  Boolean viewAll) {
		List<ContentVersion> contentVersionList = 
				ManageContentFileUploadHelper.getExistingContent(selectedSearchFolder,
																lookupFieldName,
																currentRecordId,
																viewAll);
		return contentVersionList;
	}
	
    //─────────────────────────────────────────────────────────────────────────┐
    // folderOptions: List of floders avilable    
    // @return list of string
    //─────────────────────────────────────────────────────────────────────────┘
    @AuraEnabled
	public static List<String> folderOptions() {

		List<String> folderOptionsList = new List<String>();
		folderOptionsList = ManageContentFileUploadHelper.getFolderOptions();
		return folderOptionsList;
	}
	
    //─────────────────────────────────────────────────────────────────────────┐
    // getExistingContent: Retrive related content for the current record.
    // @param String  sObjectType; object the request is comming from.
    //                currently suporting only Opportunity and account.
    // @param Id  currentRecordId; current record Id. Supporting Account 
    //                          and Opportunity only
    // @return map of workspace with record Id
    //─────────────────────────────────────────────────────────────────────────┘
    @AuraEnabled
	public static Map<String,String> workspaceOptions(String sObjectType,
													Id currentRecordId) {

		Map<String,String> wsOptionsMap = new Map<String,String>();
		if(sObjectType == 'Opportunity') {
			wsOptionsMap = ManageContentFileUploadHelper.getWorkspaceList('Opportunity',currentRecordId);
		} else if(sObjectType == 'Account') {
			wsOptionsMap = ManageContentFileUploadHelper.getWorkspaceList('Account',currentRecordId);
		}
		return wsOptionsMap;
	}
	

    //─────────────────────────────────────────────────────────────────────────┐
    // saveTheFile: saving uploaded record.
    // @param Id  parentId; file parent.
    // @param String  fileName; name of the file.
    // @param String  base64Data; data type.
    // @param String  contentType;
    // @param String  selectedFolder;
    // @param String  selectedWorkspaceId; 
    // @param String  sObjType; object for which file is attached. Account/Opportunity
    // @return String 
    //─────────────────────────────────────────────────────────────────────────┘
    @AuraEnabled
    public static String saveTheFile(Id parentId, 
    							String fileName, 
    							String base64Data, 
    							String contentType,
    							String selectedFolder,
    							String selectedWorkspaceId,
    							String sObjType) { 

        System.debug('in saveTheFile');
    	String responseData = '';
    	Map<String,String> fieldMap = getFieldMapForFolder(selectedFolder,sObjType);

    	if(!fieldMap.isEmpty()) {
    		try {

		        ContentVersion contentVersion = createContentVersion(parentId,
		        													fileName,
		        													base64Data,
		        													contentType,
		        													sObjType);
		        insert contentVersion;
		        // create workspaceDoc
		        ContentWorkspaceDoc workspaceDoc = 
		        		ManageContentFileUploadHelper.createContentWorkspaceDoc(contentVersion,
		        																selectedWorkspaceId);
		        insert workspaceDoc;
		        contentVersion = initializeContentVersionFields(contentVersion,
		        												fieldMap,
		        												sObjType,
		    													parentId);
		        contentVersion.Folder__c = selectedFolder;
		        update contentVersion;

		        responseData = contentVersion.Id;

		    } catch(Exception e) {
                System.debug('exception:'+e.getMessage());
		    	responseData = e.getMessage();
		    }
	    } else {

	    	responseData = 'File cannot be uploaded to this folder '+selectedFolder;
	    }
        return responseData;
    }
    
    //─────────────────────────────────────────────────────────────────────────┐
    // createContentVersion: create ContentVersion Record.
    // @param Id  parentId; file parent.
    // @param String  fileName; name of the file.
    // @param String  base64Data; data type.
    // @param String  contentType;
    // @param String  sObjType; object for which file is attached. Account/Opportunity
    // @return String 
    //─────────────────────────────────────────────────────────────────────────┘
    private static ContentVersion createContentVersion(Id parentId, 
    													String fileName, 
    													String base64Data, 
    													String contentType,
    													String sObjType) {

    	// convert this to blob
    	base64Data = EncodingUtil.urlDecode(base64Data, 'UTF-8');
    	// get content version record type
    	String recordTypeName = Manage_Content_Record_Type_Name__c.getValues('RecordTypeName').Record_Type_Name__c;
    	String contentRecordType = Schema.SObjectType.ContentVersion.getRecordTypeInfosByName().get(recordTypeName.trim()).getRecordTypeId();

    	ContentVersion contentVersion = new ContentVersion();
        contentVersion.VersionData = Blob.valueOf(base64Data);
        //String escapedFileName = escapeFileName(this.fileName);      
        contentVersion.PathOnClient = fileName;
        contentVersion.Title = fileName;
        contentVersion.RecordTypeId = contentRecordType;

        if(sObjType == 'Opportunity') {
        	contentVersion.Opportunity__c = parentId;
        } else if(sObjType == 'Account') {
        	contentVersion.Account__c = parentId;
        }
        return contentVersion;
    }

    //─────────────────────────────────────────────────────────────────────────┐
    // initializeContentVersionFields: retrive field
    // @param ContentVersion  contentversion record create.
    // @param map<string,string> field mapping between the object and content version.
    // @param String  sObjType;
    // @param String  parentId; 
    // @return ContentVersion after attached to parent object. 
    //─────────────────────────────────────────────────────────────────────────┘
    private static ContentVersion initializeContentVersionFields(ContentVersion contentVersion, 
    												Map<String,String> fieldMap,
    												String sObjType,
    												String parentId){
        String fieldSet = '';
        sObject sObjContentVersion;
        for(String key : fieldMap.keySet()){
            fieldSet = fieldSet + key + ',';
        } 
        if(sObjType == 'Opportunity') {

        	sObjContentVersion = new Opportunity();
        } else if(sObjType == 'Account') {

        	sObjContentVersion = new Account();
        }
        //Opportunity oppty = new Opportunity();
        sObjContentVersion = Database.query('Select '
        						+fieldSet.substring(0,fieldSet.length()-1)
        						+' from '+sObjType
        						+' where Id = \''+parentId+ '\'');
        for(String key : fieldMap.keySet()){
            contentVersion.put(fieldMap.get(key),(String)sObjContentVersion.get(key));
        } 
        System.debug('contentVersion:'+contentVersion);
        return contentVersion;
    }
    //─────────────────────────────────────────────────────────────────────────┐
    // getFieldMapForFolder: populate field map
    // @param String  sObjType;
    // @param String  selectedFolder; 
    // @return map<string,string>
    //─────────────────────────────────────────────────────────────────────────┘
    private static Map<String,String> getFieldMapForFolder(String selectedFolder,
    														String sObjType) {

    	System.debug('selectedFolder:'+selectedFolder);
        Map<String,String> fieldMap = new Map<String,String>();
        sObject folderFieldMap;
        FolderFieldMap__c opptyFolderFieldMap = FolderFieldMap__c.getValues(selectedFolder);
        AccountFolderContent__c acctFolderFieldMap = AccountFolderContent__c.getValues(selectedFolder);
        // in case of Opportunity
        if(sObjType == 'Opportunity' && opptyFolderFieldMap != null) {

        	folderFieldMap = new FolderFieldMap__c();
        	folderFieldMap = opptyFolderFieldMap;
        } else if(sObjType == 'Account' && acctFolderFieldMap != null) {

        	folderFieldMap = new AccountFolderContent__c();
        	folderFieldMap = acctFolderFieldMap;
        }
        if(folderFieldMap != null) {
        	if(folderFieldMap.get('Field_Map1__c') != null) {

        		List<String> splitList = String.valueOf(folderFieldMap.get('Field_Map1__c')).split(',');
            	for(String split : splitList){
                	fieldMap.put(split.split('=')[0],split.split('=')[1]);
            	}
        	}
	        if(folderFieldMap.get('Field_Map2__c') != null) {
				
				List<String>  splitList = String.valueOf(folderFieldMap.get('Field_Map2__c')).split(',');
	            for(String split : splitList){
	                fieldMap.put(split.split('=')[0],split.split('=')[1]);
	            }
	        }
	    }
	    
        return fieldMap;
    }
    /*
    @AuraEnabled
    public static Id saveTheChunk(Id parentId, 
    							String fileName, 
    							String base64Data, 
    							String contentType, 
    							String fileId,
    							String selectedFolder,
    							String selectedWorkspaceId,
    							String sObjType) { 
        if (fileId == '') {
            fileId = saveTheFile(parentId, fileName, base64Data,
            					 contentType, selectedFolder,
            					 selectedWorkspaceId, sObjType);
        } else {
            appendToFile(fileId, base64Data);
        }
        
        return Id.valueOf(fileId);
    }
    
    private static void appendToFile(Id fileId, String base64Data) {
        base64Data = EncodingUtil.urlDecode(base64Data, 'UTF-8');
        
        Attachment a = [
            SELECT Id, Body
            FROM Attachment
            WHERE Id = :fileId
        ];
        
     	String existingBody = EncodingUtil.base64Encode(a.Body);
        a.Body = EncodingUtil.base64Decode(existingBody + base64Data); 
        
        update a;
    }*/

}