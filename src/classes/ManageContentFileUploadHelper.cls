/**
* ─────────────────────────────────────────────────────────────────────────────────────────────────┐
* Opportunity Related Products
* 
* This class is servr side controler for Related products on
*  opportunity in lightning mode.
* Refered in relatedProduct component.
* ──────────────────────────────────────────────────────────────────────────────────────────────────
* @author         Jaya Alaparthi
* @maintainedBy   Jaya Alaparthi
* @version        1.0
* @created        2017-03-06
* @modified       YYYY-MM-DD
* @systemLayer    Service
* @see            ????
* @see            ????
* ──────────────────────────────────────────────────────────────────────────────────────────────────
* @changes
* vX.X            Jaya Alaparthi
* YYYY-MM-DD      Explanation of the change.  Multiple lines can be used to explain the change, but
*                 each line should be indented till left aligned with the previous description text.
*
* vX.X            Jaya Alaparthi
* YYYY-MM-DD      Each change to this file should be documented by incrementing the version number,
*                 and adding a new entry to this @changes list. Note that there is a single blank
*                 line between each @changes entry.
* ─────────────────────────────────────────────────────────────────────────────────────────────────┘
*/
public with sharing class ManageContentFileUploadHelper {
	
	public ManageContentFileUploadHelper() {
	}

    //─────────────────────────────────────────────────────────────────────────┐
    // getExistingContent: Retrive related content for the current record.
    // @param String  selectedSearchFolder default to All 
    // @param String  lookupFieldName default to All 
    // @param Id  currentRecordId current record Id
    // @param Boolean  viewAll
    // @return list of ContentVersion
    //─────────────────────────────────────────────────────────────────────────┘
    public Static Map<String,String> getWorkspaceList(String workSpaceFor,
													Id recordId) {  
    	
    	Map<String,String> workSpaceIdMap = new Map<String,String>();
    	// create dropdown list for workspace field.
    	
    	if(workSpaceFor == 'Opportunity') {

    		for (Workspaces_Opportunity__c workspace : 
    				Workspaces_Opportunity__c.getAll().values()) {

	        	workSpaceIdMap.put(workspace.Name,workspace.Workspace_ID__c);
        	}
    	} else if(workSpaceFor == 'Account') {

    		// get account recordType
    		String accountRecordTypeId = getRecordTypeId('Account',recordId);

    		for (Workspace_Account__c workspace : 
    				Workspace_Account__c.getAll().values()) {

    			if ((workspace.RecordTypeNames__c != null 
    					&& workspace.RecordTypeNames__c.Contains(accountRecordTypeId))
    				||(String.isBlank(workspace.RecordTypeNames__c))
    				||(workspace.ShowForAll__c)) {

    				workSpaceIdMap.put(workspace.Name,workspace.Workspace_ID__c);
    			}	
		        
	        }
    	}
        
       
        return workSpaceIdMap;
    }
    //─────────────────────────────────────────────────────────────────────────┐
    // getExistingContent: Retrive related content for the current record.
    // @param String  selectedSearchFolder default to All 
    // @param String  lookupFieldName default to All 
    // @param Id  currentRecordId current record Id
    // @param Boolean  viewAll
    // @return list of ContentVersion
    //─────────────────────────────────────────────────────────────────────────┘
    public Static List<String> getFolderOptions() {

    	List<String> folderOptionList = new List<String>();
		List<Schema.Picklistentry> picklistValues =  Schema.sObjectType.ContentVersion.fields.Folder__c.getPicklistValues();

		 for(Schema.Picklistentry entry : picklistValues) {

		 	folderOptionList.add(entry.getValue());
		 }
		 return folderOptionList;
    }

    //─────────────────────────────────────────────────────────────────────────┐
    // getExistingContent: Retrive related content for the current record.
    // @param String  selectedSearchFolder default to All 
    // @param String  lookupFieldName default to All 
    // @param Id  currentRecordId current record Id
    // @param Boolean  viewAll
    // @return list of ContentVersion
    //─────────────────────────────────────────────────────────────────────────┘
    public static ContentWorkspaceDoc createContentWorkspaceDoc(ContentVersion contentVersion,
    															Id selectedWorkspaceId) {

    	contentVersion = [SELECT ContentDocumentId
    					  FROM ContentVersion
    					  WHERE Id =: contentVersion.Id];

    	ContentWorkspaceDoc workspaceDoc = new ContentWorkspaceDoc();
    	workspaceDoc.ContentWorkspaceId = selectedWorkspaceId; 
        workspaceDoc.ContentDocumentId = contentVersion.ContentDocumentId; 

        return workspaceDoc;
    }

    //─────────────────────────────────────────────────────────────────────────┐
    // getExistingContent: Retrive related content for the current record.
    // @param String  selectedSearchFolder default to All 
    // @param String  lookupFieldName default to All 
    // @param Id  currentRecordId current record Id
    // @param Boolean  viewAll
    // @return list of ContentVersion
    //─────────────────────────────────────────────────────────────────────────┘
    public static List<ContentVersion> getExistingContent(String selectedSearchFolder,
    												String lookupFieldName,
    												Id currentRecordId,
    												Boolean viewAll) {

    	List<ContentVersion> contentVersionList = new List<ContentVersion>();
    	String queryString = 'SELECT ContentDocumentId,'
    						+' PathOnClient,Title,Folder__c,'
    						+' ContentModifiedDate, CreatedDate '
    						+' FROM ContentVersion '
    						+' WHERE '+lookupFieldName+' = \''+currentRecordId+'\''
    						+' AND IsLatest = true';
        if(!selectedSearchFolder.equalsIgnoreCase('All')) {  

            queryString += ' AND Folder__c = \''+ selectedSearchFolder + '\'';
        }
        if(!viewAll) {

        	queryString += ' Limit 2 ';
        }
        System.debug('queryString:'+queryString);
        for(ContentVersion content: Database.query(queryString)) {
        	
            contentVersionList.add(content);
        } 
        return contentVersionList;
    }

    //─────────────────────────────────────────────────────────────────────────┐
    // getRecordTypeId: retrive account record Type Id.
    // @param String  selectedSearchFolder default to All 
    // @param String  lookupFieldName default to All 
    // @param Id  currentRecordId current record Id
    // @param Boolean  viewAll
    // @return list of ContentVersion
    //─────────────────────────────────────────────────────────────────────────┘
    private static String getRecordTypeId(String obj,Id sObjId) {

    	Account account = new Account();
        String acctRecordTypeId;
        if(obj.equalsIgnoreCase('Account')) {

        	account = [SELECT Id, RecordTypeId
        				FROM Account
        				WHERE Id =: sObjId];
            acctRecordTypeId = account.RecordTypeId;
        	System.debug('account recordType:'+account.RecordTypeId);
        }
        return acctRecordTypeId;
    }
}