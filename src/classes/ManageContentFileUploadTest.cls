/**
* ─────────────────────────────────────────────────────────────────────────────────────────────────┐
* Opportunity Related Products Test
* 
* Test class for:
* RelatedListDisplay.cls
* ProductOpportunityRelatedUtil.cls
* ──────────────────────────────────────────────────────────────────────────────────────────────────
* @author         Jaya Alaparthi
* @maintainedBy   Jaya Alaparthi
* @version        1.0
* @created        2017-05-09
* @modified       YYYY-MM-DD
* @systemLayer    Test
* ──────────────────────────────────────────────────────────────────────────────────────────────────
* @changes
* vX.X            Jaya Alaparthi
* YYYY-MM-DD      Explanation of the change.  Multiple lines can be used to explain the change, but
*                 each line should be indented till left aligned with the previous description text.
* ─────────────────────────────────────────────────────────────────────────────────────────────────┘
*/
@isTest(SeeAllData=true)

private class ManageContentFileUploadTest {
	
	/*@testSetUp static void setupData() {

		// create Account
		Account account =  new Account();
		account = UTIL_TestUtil.generateAccount();
		account.Name = 'ManageContentAcctTest';
		insert account;
		// create Opportunity
		Id opptyRecordTypeId = Schema.SObjectType.Opportunity.getRecordTypeInfosByName().get('Opportunity - Inside/Local Full').getRecordTypeId();
        
		System.debug('opptyRecordTypeId:'+opptyRecordTypeId);
		Opportunity opportunity = new Opportunity();
		opportunity = UTIL_TestUtil.generateOpportunity(account.Id);
		opportunity.Name = 'ManageContentOpptyTestClass';
		opportunity.CurrencyIsoCode = 'USD';
		opportunity.RecordTypeId = opptyRecordTypeId;
		insert opportunity;
	
	}*/

	@isTest
	static void getWorkspace() {


		Map<String,String> opptyWorkspaceOptions = new Map<String,String>();
		Map<String,String> acctWorkspaceOptions = new Map<String,String>();
		List<String> listOptions = new List<String>();
		Test.startTest();
		// create Account
		Account account =  new Account();
		account = UTIL_TestUtil.generateAccount();
		account.Name = 'ManageContentAcctTest';
		insert account;
		// create Opportunity
		Id opptyRecordTypeId = Schema.SObjectType.Opportunity.getRecordTypeInfosByName().get('Opportunity - Inside/Local Full').getRecordTypeId();
        
		System.debug('opptyRecordTypeId:'+opptyRecordTypeId);
		Opportunity opportunity = new Opportunity();
		opportunity = UTIL_TestUtil.generateOpportunity(account.Id);
		opportunity.Name = 'ManageContentOpptyTestClass';
		opportunity.CurrencyIsoCode = 'USD';
		opportunity.RecordTypeId = opptyRecordTypeId;
		insert opportunity;
	
		/*Account account  = [SELECT Id FROM Account
							WHERE  Name = 'ManageContentAcctTest'
							LIMIT 1];
		Opportunity oppty = [SELECT Id FROM Opportunity 
							WHERE Name = 'ManageContentOpptyTestClass' 
							LIMIT 1];*/
		opptyWorkspaceOptions = ManageContentFileUpload.workspaceOptions('Opportunity',opportunity.Id);
		for(Workspaces_Opportunity__c opptyWS : Workspaces_Opportunity__c.getAll().values()) {

			System.assert(opptyWorkspaceOptions.containsKey(opptyWS.Name),
				'workspaces options for Oppty retrived in test class doesnot match workspace retrived in class.');
		}
		String opptyWSId = opptyWorkspaceOptions.get('Campaign Documentation');
		acctWorkspaceOptions = ManageContentFileUpload.workspaceOptions('Account',account.Id);
        System.debug('WorkspaceOptions for Account:'+acctWorkspaceOptions);
		System.assert(acctWorkspaceOptions.size() >= 1,'workspace options for account are note retrived.');
		listOptions = ManageContentFileUpload.folderOptions();
		System.assert(listOptions.size() >0,'Folder Options are empty.');


		ManageContentFileUpload.saveTheFile(opportunity.Id,
									'TestManageContFileName',
									'File Content Data',
									'txt',
									'Agreements - Other',
									opptyWSId,
									'Opportunity');
		List<ContentVersion> allContentForOppty = ManageContentFileUpload.getExistingContent('Agreements - Other',
																	'Opportunity__c',
																	opportunity.Id,
																	true);

		System.assert(allContentForOppty.size() == 1,'Content retrived for the oppty is not matching the content on the oppty');
		Test.stopTest();
	}
}