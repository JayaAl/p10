public with sharing class ManageOpportunityScriptsController {
    /*
     * Depends heavily on methods found in ScriptUtils.cls
    */

    public Script__c opportunityPlaceholder{get;set;} // Placeholder allowing users to select an Opportunity via lookup
    public static string preferredInvoicingMethod = 'Mail w/ Notarization';
    public static Opportunity oppty{get;set;} // Opportunity record queried to allow us to display Oppty info on the page
    public String paramId; // internally used to pass the Id from the URL between functions
    private String scriptId; // internally used to indicate a specific Script for the scriptForAttachments var below
    public List<ScriptUtils.ScriptItem> listScriptItem{get;set;} // List of Script wrapper objects for display
    public integer listScriptItemSize{ // returns size of collection, used in the display to determine if there are any Script wrappers to display
        get{
            return listScriptItem.isEmpty() ? 0 : listScriptItem.size();
        }
        set;
    }
    public static ScriptUtils.ScriptItem scriptForAttachments{get;set;} // Single Script wrapper used when adding Attachments
    public List<Script__c> bulkScripts{get;set;}
    
    // Booleans used to control page contents
    public boolean opportunityEdit{get;set;}
    public boolean addEditScript{get;set;}
    public Boolean showAttachmentForm{get;set;}

    public ManageOpportunityScriptsController(){
        // get URL parameters
        defaultParams();
        // create opportunityPlaceholder record
        setupOpportunityPlaceholder();
        // Load initial list of Scripts
        loadScripts();
    }

    private void defaultParams(){
        opportunityEdit = true;
        showAttachmentForm = false;
        addEditScript = false;
        // Opportunity Id
        if(ApexPages.currentPage().getParameters().get('id') != null && ApexPages.currentPage().getParameters().get('id') != ''){
            paramId = ApexPages.currentPage().getParameters().get('id');
            opportunityEdit = false;
        }
        // Script Id
        if(ApexPages.currentPage().getParameters().get('scriptId') != null && ApexPages.currentPage().getParameters().get('scriptId') != ''){
            scriptId = ApexPages.currentPage().getParameters().get('scriptId');
            showAttachmentForm = true;
        }
        queryOpportunity();
        bulkScripts = ScriptUtils.listEmptyScripts(paramId);
    }
    
    public void saveBulkScripts(){
        // system.assert(false,'bulkScripts:'+bulkScripts);
        try{
            List<Script__c> toInsert = new List<Script__c>();
            for(Script__c s:bulkScripts){
                if(s.Script_Name__c != null && s.Script_Name__c != ''){
                    toInsert.add(s);
                }
            }
            insert toInsert;
            
            if(toInsert.size()>0 && paramId != null && paramId != '')
            {
                 Opportunity o = new Opportunity(Id = paramId, Preferred_Invoicing_Method__c = preferredInvoicingMethod);
                 update o;
            }
            
            bulkScripts = ScriptUtils.listEmptyScripts(paramId);
            loadScripts();
        } catch (exception e) {
            myAddMessage(ApexPages.Severity.FATAL,e.getMessage());
        }
    }

    private void setupOpportunityPlaceholder(){
        opportunityPlaceholder = new Script__c(
            Opportunity__c = paramId
        );
    }
    
    // Load current Scripts
    public void loadScripts(){
        addEditScript = false;
        listScriptItem = new List<ScriptUtils.ScriptItem>();
        Set<Id> setScriptIds = new Set<Id>();
        if(paramId == null || paramId == '' ){
            myAddMessage(ApexPages.Severity.FATAL,'No/Invalid Opportunity selected, cannot query Scripts.');
            return;
        }
        try {
            listScriptItem = ScriptUtils.querySingleOpptyScripts(paramId);
            if(scriptId!=null && scriptId!=''){
                Integer listItem = findListReference(listScriptItem, scriptId);
                scriptForAttachments = listScriptItem.get(listItem);
                
            }
        } catch (exception e) {
            myAddMessage(ApexPages.Severity.FATAL,e.getMessage());
        }
    }

/*
    Buttons
*/

    // Change Opportunities
    public void editSelectedOpportunity(){
        opportunityEdit = true;
    }
    public void changeOpportunity(){
        opportunityEdit = false;
        paramId = opportunityPlaceholder.Opportunity__c;
        loadScripts();
        queryOpportunity();
    }

    // Add a Script to the beginning of the list of Scripts
    public void addScript(){
        Script__c newScript = new Script__c(Opportunity__c = paramId, Script_Name__c = 'New Script');
        List<ScriptUtils.ScriptItem> newList = new List<ScriptUtils.ScriptItem>();
        ScriptUtils.ScriptItem si = ScriptUtils.scriptItemFromScript(newScript);
        si.recordId = 'NEW'+generateRandomString(); // Sets ID of any newly created Scripts to a random String with a 'NEW' prefix so they can be identified later
        si.boolEdit = true;
        addEditScript = true;
        newList.add(si);
        newList.addAll(listScriptItem);
        listScriptItem = newList;
    }

    // Delete a single Attachment
    public void deleteSingleAttachment(){
        Id attachmentToDelete = System.currentPageReference().getParameters().get('attachmentToDelete');
        try{
            delete new List<Attachment>{new Attachment(Id=attachmentToDelete)};
        } catch (exception e) {
            myAddMessage(ApexPages.Severity.FATAL,'Error deleting Attachment');
            myAddMessage(ApexPages.Severity.FATAL,e.getMessage());
        }
        loadScripts();
    }

    // Delete a Script
    public void deleteSingleScript(){
        Id scriptToDelete = System.currentPageReference().getParameters().get('scriptToDelete');
        try{
            delete new List<Script__c>{new Script__c(Id=scriptToDelete)};
        } catch (exception e) {
            myAddMessage(ApexPages.Severity.FATAL,'Error deleting Script');
            myAddMessage(ApexPages.Severity.FATAL,e.getMessage());
        }
        loadScripts();
    }

    // Clone a Script
    

    // Edit a Script
    public void editSingleScript(){
        String scriptToEdit = System.currentPageReference().getParameters().get('scriptToEdit');
        Integer listItem = findListReference(listScriptItem, scriptToEdit);
        ScriptUtils.ScriptItem si = listScriptItem.get(listItem);
        si.boolEdit = true;
        addEditScript = true;
    }

    public void cancelEdit(){
        addEditScript = false;
        opportunityEdit = false;
        loadScripts();
    }

    // Save changes to a Script
    public void updateSingleScript(){
        String scriptToSave = System.currentPageReference().getParameters().get('scriptToSave');
        Integer listItem = findListReference(listScriptItem, scriptToSave);
        ScriptUtils.ScriptItem si = listScriptItem.get(listItem);
        try{
            upsert new List<Script__C>{si.script};
            // Update Opportunity.Preferred_Invoicing_Method__c to 'Mail w/ Notarization'
            Opportunity o = new Opportunity(Id=si.script.Opportunity__c, Preferred_Invoicing_Method__c='Mail w/ Notarization');
            update o;
        } catch (exception e) {
            myAddMessage(ApexPages.Severity.FATAL,'Error updating Script');
            myAddMessage(ApexPages.Severity.FATAL,e.getMessage());
        }
        loadScripts();
    }

    // get List reference integer for a specific Script
    public Integer findListReference(List<ScriptUtils.ScriptItem> theList, String scriptId){
        Integer i=0;
        for(ScriptUtils.ScriptItem si:theList){
            if(si.recordId == scriptId){
                return i;
            }
            i++;
        }
        return null;
    }


    public void showAttachmentFormButton(){
        String recordId = System.currentPageReference().getParameters().get('recId');
        Integer listItem = findListReference(listScriptItem, recordId);
        scriptForAttachments = listScriptItem.get(listItem);
        showAttachmentForm = true;
    }

    // add an attachment
    public transient String fileName {get;set;}
    public transient Blob fileBody {get;set;}
    public pagereference UploadFile(){
        String recId = System.currentPageReference().getParameters().get('recId');
        pagereference pr = Apexpages.currentPage();
        pr.getParameters().put('id', paramId);
        pr.getParameters().put('ScriptId', recId);
        
        if(recId==null || recId==''){
            myAddMessage(ApexPages.Severity.FATAL, 'Unable to add Attachment, please refresh the page to continue.');
            return pr;
        } else if (fileBody==null || fileName==null || fileName==''){
            myAddMessage(ApexPages.Severity.FATAL, 'No attachment selected, please select an attachment to continue.');
            return pr;
        }
        try{
            if(fileBody != null && fileName != null){
                Attachment myAttachment  = new Attachment();
                myAttachment.Body = fileBody;
                myAttachment.Name = fileName;
                myAttachment.ParentId = recId;
                insert myAttachment;
            }
        } catch (exception e) {
            myAddMessage(ApexPages.Severity.FATAL,'Error inserting Attachment');
            myAddMessage(ApexPages.Severity.FATAL,e.getMessage());
            return pr;
        }
        
        
        fileBody = null;
        fileName = null;
        ScriptId = recId;
        loadScripts();
        return pr;
    }

/* 
    Helper functions 
*/

    // Gather Opportunity information
    private void queryOpportunity(){
        try{
            if(paramId!=null&&paramId!=''){
                oppty = [Select Id, Preferred_Invoicing_Method__c from Opportunity where Id = :paramId limit 1];
            } else {
                oppty = new Opportunity();
            }
        } catch (exception e){
            myAddMessage(ApexPages.SEVERITY.ERROR,'Invalid Opportunity Id provided');
        }
    }

    // Add a pagemessage
    private void myAddMessage(ApexPages.Severity severity, String message){
        // valid Severities: CONFIRM, ERROR, FATAL, INFO, WARNING
        ApexPages.Message myMsg = new ApexPages.Message(severity, message);
        ApexPages.addMessage(myMsg);
    }

    // function to create a random ID for newly created Scripts
    private static Set<String> priorRandoms; // set of prior randomly generated placeholder Ids so as to ensure that we always assign a new random placeholder
    public static String generateRandomString(){
        if(priorRandoms == null)
            priorRandoms = new Set<String>();

        Integer length = 12;
        String characters = 'abcdefghijklmnopqrstuvwxyz1234567890';
        String returnString = '';
        while(returnString.length() < length){
            Integer charpos = Math.round( Math.random() * (characters.length()-1) );
            returnString += characters.substring( charpos , charpos+1 );
        }
        if(priorRandoms.contains(returnString)) {
            return generateRandomString();
        } else {
            priorRandoms.add(returnString);
            return returnString;
        }
    }
}