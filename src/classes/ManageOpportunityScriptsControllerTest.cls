@isTest
public class ManageOpportunityScriptsControllerTest {

/* Prep testing record data */    
    private static ScriptUtilsTest testUtil;
    private static void prepTests(){
        ScriptUtilsTest.prepTests();
    }

/* Perform individual tests */
    private static testmethod void testEmptyConstructor(){
        test.startTest();
        ManageOpportunityScriptsController con = new ManageOpportunityScriptsController();

        // test values defaulted in the constructor
        system.assertEquals(true, con.opportunityEdit);
        system.assertEquals(false, con.showAttachmentForm);
        system.assertEquals(false, con.addEditScript);
        system.assertEquals(null, con.paramId);
        // page should contain a message
        
        test.stopTest();
    }
    
    private static testmethod void testConstructorWithEmptyOppty(){
        prepTests();
        test.startTest();
        Test.setCurrentPage(Page.ManageOpportunityScripts); // set current pagereference
        ApexPages.currentPage().getParameters().put('id', ScriptUtilsTest.oppNoScripts.Id); // add Oppty Id to the pagereference parameters
        ManageOpportunityScriptsController con = new ManageOpportunityScriptsController();
        test.stopTest();
        
        // test values defaulted in the constructor
        system.assertEquals(false, con.opportunityEdit);
        system.assertEquals(false, con.showAttachmentForm);
        system.assertEquals(false, con.addEditScript);
        system.assertEquals(ScriptUtilsTest.oppNoScripts.Id, con.paramId);
        // listScriptItem should be an empty list
        system.assertEquals(true, con.listScriptItem.isEmpty());

    }
    
    private static testmethod void testConstructorWithOpptyAndScripts(){
        prepTests();
        test.startTest();
        Test.setCurrentPage(Page.ManageOpportunityScripts); // set current pagereference
        ApexPages.currentPage().getParameters().put('id', ScriptUtilsTest.oppWithScripts.Id); // add Oppty Id to the pagereference parameters
        ManageOpportunityScriptsController con = new ManageOpportunityScriptsController();
        test.stopTest();
        
        // test values defaulted in the constructor
        system.assertEquals(false, con.opportunityEdit);
        system.assertEquals(false, con.showAttachmentForm);
        system.assertEquals(false, con.addEditScript);
        system.assertEquals(ScriptUtilsTest.oppWithScripts.Id, con.paramId);
        // listScriptItem should be a list of 10 items
        system.assertEquals(false, con.listScriptItem.isEmpty());
        system.assertEquals(10, con.listScriptItem.size());
        system.assertEquals(10, con.listScriptItemsize);
    }
    
    private static testmethod void testSingleScript(){
        prepTests();
        test.startTest();
        Test.setCurrentPage(Page.ManageOpportunityScripts); // set current pagereference
        ApexPages.currentPage().getParameters().put('scriptId', ScriptUtilsTest.listTestScripts[0].Id); // add Oppty Id to the pagereference parameters
        ManageOpportunityScriptsController con = new ManageOpportunityScriptsController();
        test.stopTest();
        
        // test values defaulted in the constructor
        system.assertEquals(true, con.opportunityEdit);
        system.assertEquals(true, con.showAttachmentForm);
    }
    
    private static testmethod void testSaveBulkScripts(){
        prepTests();
        test.startTest();
        Test.setCurrentPage(Page.ManageOpportunityScripts); // set current pagereference
        ApexPages.currentPage().getParameters().put('id', ScriptUtilsTest.oppWithScripts.Id); // add Oppty Id to the pagereference parameters
        ManageOpportunityScriptsController con = new ManageOpportunityScriptsController();
        for(integer i=0;i<6;i++){ // Update 6 of the bulkScripts records, adding a value to Script_Name__c 
            con.bulkScripts[i].Script_Name__c = UTIL_TestUtil.generateRandomString(16);
        }
        con.saveBulkScripts();
        test.stopTest();
        
        // confirm that the Opportunity now has 16 Scripts associated to it
        List<Script__c> newList = [Select Id from Script__c where Opportunity__c = :ScriptUtilsTest.oppWithScripts.Id];
        system.assertEquals(16, newList.size());
        // confirm that the controller has a "fresh" set of bulkScripts
        system.assertEquals(10, con.bulkScripts.size());
        for(Script__c s:con.bulkScripts){
            system.assertEquals('', s.Script_Name__c);
        }
    }
    
    private static testmethod void testEditSelectedOpportunity(){
        prepTests();
        
        Test.setCurrentPage(Page.ManageOpportunityScripts); // set current pagereference
        ApexPages.currentPage().getParameters().put('id', ScriptUtilsTest.oppWithScripts.Id); // add Oppty Id to the pagereference parameters
        ManageOpportunityScriptsController con = new ManageOpportunityScriptsController();
        // test values defaulted in the constructor
        system.assertEquals(false, con.opportunityEdit); // Confirm that before calling editSelectedOpportunity the opportunityEdit flag is FALSE
        system.assertEquals(10, con.listScriptItem.size()); // Confirm that this Oppty has 10 Scripts for display
        test.startTest();
        con.editSelectedOpportunity(); // push the editSelectedOpportunity button
        system.assertEquals(true, con.opportunityEdit); // Confirm that after calling editSelectedOpportunity the opportunityEdit flag is TRUE
        con.opportunityPlaceholder.Opportunity__c = ScriptUtilsTest.oppNoScripts.Id; // Update the selected Oppty
        con.changeOpportunity(); // commit the changed Opportunity
        test.stopTest();
        system.assertEquals(false, con.opportunityEdit); // Confirm that opportunityEdit has been reverted to FALSE
        system.assertEquals(0, con.listScriptItem.size()); // Confirm that we are not populating any Scripts for display as this Oppty has none
    }
    
    private static testmethod void testAddScript(){
        prepTests();
        
        Test.setCurrentPage(Page.ManageOpportunityScripts); // set current pagereference
        ApexPages.currentPage().getParameters().put('id', ScriptUtilsTest.oppWithScripts.Id); // add Oppty Id to the pagereference parameters
        ManageOpportunityScriptsController con = new ManageOpportunityScriptsController();
        // test values defaulted in the constructor
        system.assertEquals(10, con.listScriptItem.size()); // Confirm that this Oppty has 10 Scripts for display
        
        test.startTest();
        con.addScript();
        test.stopTest();
        
        system.assertEquals(11, con.listScriptItem.size()); // Confirm that listScriptItem now shows 11 items
        List<Script__c> testList = [Select Id from Script__c where Opportunity__c = :ScriptUtilsTest.oppWithScripts.Id];
        system.assertEquals(10, testList.size()); // Confirm that Actual Scripts still equal 10 items
    }
    
    private static testmethod void testDeleteSingleScript(){
        prepTests();
        
        Test.setCurrentPage(Page.ManageOpportunityScripts); // set current pagereference
        ApexPages.currentPage().getParameters().put('id', ScriptUtilsTest.oppWithScripts.Id); // add Oppty Id to the pagereference parameters
        ManageOpportunityScriptsController con = new ManageOpportunityScriptsController();
        system.assertEquals(10, con.listScriptItem.size()); // Confirm that this Oppty has 10 Scripts
        
        test.startTest();
        String scriptToDelete = con.listScriptItem[0].recordId;
        System.currentPageReference().getParameters().put('scriptToDelete',scriptToDelete);
        con.deleteSingleScript();
        test.stopTest();
        system.assertEquals(9, con.listScriptItem.size()); // Confirm that listScriptItem now shows 9 items
        List<Script__c> testList = [Select Id from Script__c where Opportunity__c = :ScriptUtilsTest.oppWithScripts.Id];
        system.assertEquals(9, testList.size()); // Confirm that Actual Scripts still equal 9 items
    }
    
    private static testmethod void testUploadFile(){
        prepTests();
        
        Test.setCurrentPage(Page.ManageOpportunityScripts); // set current pagereference
        ApexPages.currentPage().getParameters().put('id', ScriptUtilsTest.oppWithScripts.Id); // add Oppty Id to the pagereference parameters
        ManageOpportunityScriptsController con = new ManageOpportunityScriptsController();
        
        Attachment testAtt = UTIL_TestUtil.generateAttachment(null);
        
        test.startTest();
        String scriptToDelete = con.listScriptItem[0].recordId;
        System.currentPageReference().getParameters().put('recId',scriptToDelete);
        con.fileName = testAtt.Name;
        con.fileBody = testAtt.Body;
        con.UploadFile();
        test.stopTest();
        
        // Confirm that one Attachment has been saved to SF
        List<Attachment> theList = [Select Id from Attachment];
        system.assertEquals(1, theList.size());
        
    }
    
    /*
deleteSingleAttachment(){
cancelEdit(){
updateSingleScript(){
showAttachmentFormButton(){
generateRandomString()
*/
    

}