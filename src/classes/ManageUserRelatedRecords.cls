public with sharing class ManageUserRelatedRecords {
    
	public ManageUserRelatedRecords() {}
    

    public static void manageUserRelatedRecords(List<User> t_New, Map<ID, User> t_oldMap){
        List<User> changedUsers = new List<User>();

        for(User newU:t_New  ){
            if(newU.isActive && newU.UserType=='Standard'){ // Only active standard users
                if(t_oldMap == null || t_oldMap.isEmpty()){ // if Insert

                    changedUsers.add(newU);
                } else {
                    // else if any details updated
                    
                        changedUsers.add(newU);
                }
            }
        }
        // process each in turn
        processApplication_Users(changedUsers);
        processERP_Users(changedUsers);
        processContact_Users(changedUsers);
    }

//================================================================================
    
	public static void processApplication_Users(List<User> t_New){
		try {
			List<Application__c> toUpsert = new List<Application__c>(); // records to eventually upsert
			Map<Id,User> newMap = new Map<Id,User>(); // Map of updated records, used later to populate the found ERP_User__c records
		
			// Feed all New/Updated Users into a Map
			for(User u:t_New){
				newMap.put(u.Id, u);
			}
			// Find all ERP_User__c records associated to the updated User records
			for(Application__c app :queryApplicationsByUserIds(newMap.keySet())){
				toUpsert.add(updateApplicationFromUser(app, newMap.get(app.User__c))); // Add an updated version of the ERP_User__c record into the List for later Upsert
				newMap.remove(app.User__c); // remove the User from the Map so that we can create new ERP_User records for the remaining entries afterward
			}
             
			// Create any new records necessary, either because we could not find an existing ERP_User__c record or if the User is new
			toUpsert.addAll(createApplicationFromUser(newMap.values()));
			
			// Perform the Upsert if there are any records 
			if(!toUpsert.isEmpty()){
				upsert toUpsert;
			}
		} catch (Exception ex){ // Capture any errors
            System.debug('Error in Application user creation'+ex.getMessage());
        }
	}


/* Application__c helper functions */
	public static List<Application__c> queryApplicationsByUserIds(Set<Id> setIds){
		return [SELECT Id, 
                OwnerId, 
                IsDeleted,
                Name, 
                User__c
				FROM Application__c 
				WHERE User__c IN:setIds
		];
	}
	public static Application__c updateApplicationFromUser(Application__c e, User u){
		Application__c toReturn = new Application__c(  OwnerId = u.Id,Id = e.Id,Name = u.Name );
			// Manager, Email address, and User Id automatically pulled down via Formula to the Application__c
		
		return toReturn;
	}
	public static List<Application__c> createApplicationFromUser(List<User> listUsers){
		// gather the 'DefaultOn' fields for Applications from the Field Set
		List<Schema.FieldSetMember> DefaultOnFields = SObjectType.Application__c.FieldSets.DefaultOn.getFields();
		List<Application__c> toReturn = new List<Application__c>();
		for(User u:listUsers){
			Application__c newApp = new Application__c(
				Name = u.Name,
				OwnerId = u.Id,
				User__c = u.Id
				// Manager, Email address, and User Id automatically pulled down via Formula to the Application__c
			);

			for(Schema.FieldSetMember f : DefaultOnFields) {
			    newApp.put(f.getFieldPath(),true);
			}

			toReturn.add( newApp );
		}
		return toReturn;
	}

//================================================================================
   
	public static void processERP_Users(List<User> t_New){
		try {
			List<ERP_User__c> toUpsert = new List<ERP_User__c>(); // records to eventually upsert
			Map<Id,User> newMap = new Map<Id,User>(); // Map of updated records, used later to populate the found ERP_User__c records
			
			// Feed all New/Updated Users into a Map
			for(User u:t_New){
				newMap.put(u.Id, u);
			}

			// Find all ERP_User__c records associated to the updated User records
			for(ERP_User__c erpU :queryERPUsersByUserIds(newMap.keySet())){
				toUpsert.add(updateERPUserFromUser(erpU, newMap.get(erpU.User__c))); // Add an updated version of the ERP_User__c record into the List for later Upsert
				newMap.remove(erpU.User__c); // remove the User from the Map so that we can create new ERP_User records for the remaining entries afterward
			}

			// Create any new records necessary, either because we could not find an existing ERP_User__c record or if the User is new
			toUpsert.addAll(createERPFromUser(newMap.values()));
			
			// Perform the Upsert if there are any records 
			
			if(!toUpsert.isEmpty()){
                upsert toUpsert;
			}
		} catch (Exception ex){ // Capture any errors
           System.debug('Error in ERP user creation'+ex.getMessage());
		}
	}


/* ERP_User__c helper functions */
	public static List<ERP_User__c> queryERPUsersByUserIds(Set<Id> setIds){
		return [SELECT Id, OwnerId,
                IsDeleted, Name, 
                Manager__c, 
                Contact__c,
                User__c, 
                Email_Override__c
				FROM ERP_User__c 
				WHERE User__c IN:setIds
		];
	}
	public static ERP_User__c updateERPUserFromUser(ERP_User__c e, User u){
		ERP_User__c toReturn = new ERP_User__c(
			OwnerId = u.Id,Id = e.Id,	Name = u.Name,Manager__c = u.ManagerId
			// Email address and User Id automatically pulled down via Formula to the ERP_User__c
		);
		return toReturn;
	}
	public static List<ERP_User__c> createERPFromUser(List<User> listUsers){
		List<ERP_User__c> toReturn = new List<ERP_User__c>();
		for(User u:listUsers){
			toReturn.add(
				new ERP_User__c(Name = u.FirstName
                                +' '+ u.LastName,
                                OwnerId = u.Id,	
                                User__c = u.Id,
                                Manager__c = u.ManagerId
					// Email address and User Id automatically pulled down via Formula to the ERP_User__c
				)
			);
		}
		return toReturn;
	}
    
//================================================================================
	public static void processContact_Users(List<User> t_New){
		try {
			List<Contact> toUpsert = new List<Contact>(); // records to eventually upsert
			Map<String,User> newMap = new Map<String,User>(); // Map of updated records, used later to populate the found Contact records
			String accName = 'Pandora Media, Inc.';
                Account acc = [Select Id 
                               from Account
                               where Name =:accName 
                               limit 1];
                String parentId = acc.id;
			// Feed all New/Updated Users into a Map
			for(User u:t_New){
				newMap.put(u.Username, u); // keying the Contacts to Users via the USERNAME of the User
			}
				for(Contact con :queryContactsByUsernames(newMap.keySet(),parentId)){
					
					toUpsert.add(updateContactFromUser(con, newMap.get(con.Email),parentId)); // Add an updated version of the Contact record into the List for later Upsert
					newMap.remove(con.Email); // remove the User from the Map so that we can create new Contact records for the remaining entries afterward
				}
			
			
			// Create any new records necessary, either because we could not find an existing Contact record or if the User is new
			toUpsert.addAll(createContactFromUser(newMap.values(),parentId));
			// Perform the Upsert if there are any records 
			if(!toUpsert.isEmpty()){
				upsert toUpsert;
			}
		} catch (Exception ex){ // Capture any errors
             System.debug('Error in contact user update'+ex.getMessage());
		}
	}

	public static List<Contact> queryContactsByUsernames(Set<String> setUsernames, String parentId){
		List<Contact> conList = new List<Contact>();
		for(Contact con : [SELECT Id,
                           OwnerId, IsDeleted, 
                           Name, FirstName, 
                           LastName, Email
							FROM Contact 
                           WHERE AccountId =: parentId 
                           AND Email IN:setUsernames 
                           ORDER BY CreatedDate
		])
        {

			conList.add(con);
		}
		return conList;
	}
	public static Contact updateContactFromUser(Contact e, User u,String parentId){
	Contact toReturn = new Contact(OwnerId = u.Id,Id = e.Id, AccountId = parentId,LastName = u.LastName,FirstName = u.FirstName,Email = u.Username);
		return toReturn;
	}
	public static List<Contact> createContactFromUser(List<User> listUsers,String parentId)
	{
		List<Contact> toReturn = new List<Contact>();
		for(User u:listUsers){
			toReturn.add(new Contact(AccountId =  parentId,
                                     LastName = u.LastName,
                                     FirstName = u.FirstName,
                                     OwnerId = u.Id, 
                                     Email = u.Username
				)
			);
		}
		return toReturn;
	}
}