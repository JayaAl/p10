@isTest
public class ManageUserRelatedRecordsTest {
    
    public static integer testCount = 202;
    public static List<User> listUserNoRelated;
    public static List<User> listUserWithRelated;
    public static Account testAccount;
    
    @testSetup
     private static  void insertAccount(){
         testAccount = UTIL_testUtil.generateAccount();
         List<Account> pandoraAcc = [SELECT Name FROM Account WHERE Name = 'Pandora Media, Inc.'];
         If(pandoraAcc.size() > 0){
          delete pandoraAcc;
         }
         Account acc = new Account(Name = 'Pandora Media, Inc.');
       
        insert acc;
    }
     @testSetup
    private static void prepTests(){
       
        // set up users with no related records
        listUserNoRelated = new List<User>();
        for(integer i=0;i<testCount;i++){
            listUserNoRelated.add(UTIL_testUtil.generateUser()); //
        }
        insert listUserNoRelated;
        clearRelated(); // clear any ERP_User__c, Application__c, or Contact records that may have been created
        
        // Set up users with related records
        listUserWithRelated = new List<User>();
        for(integer i=0;i<testCount;i++){
            listUserWithRelated.add(UTIL_testUtil.generateUser()); //
        }
        insert listUserWithRelated;
        system.debug('listUserWithRelated :::'+listUserWithRelated);
        // ERP_User__c, Application__c and Contact records should be completed by User Trigger
    }
    private static void clearRelated(){ // clear any ERP_User__c, Application__c, or Contact records that may have been created
        List<sObject> toDelete = new List<Sobject>();
        List<sObject> tempL = new List<Sobject>();
        tempL = [select Id from ERP_User__c];
        toDelete.addAll(tempL);
        tempL = [select Id from Application__c];
        toDelete.addAll(tempL);
        tempL = [select Id from Contact];
        toDelete.addAll(tempL);
        delete toDelete; 
    }

//------------------------------------------------------------------------------------------------
  public static testmethod void test_InsertUsers(){
    prepTests(); // listUserWithRelated is inserted, should result in related records created for all updated users
        List<sObject> tempL = new List<Sobject>();
        tempL = [select Id from ERP_User__c];
        system.debug('Size'+tempL.size()); // Count ERP Users
        tempL = [select Id from Application__c];
        system.debug('Size'+tempL.size()); // Count Applications
        tempL = [select Id from Contact];
        system.debug('Size'+tempL.size()); // Count Contacts
    }
//------------------------------------------------------------------------------------------------
  public static testmethod void test_UpdateUsers(){
   prepTests(); // listUserWithRelated is inserted, should result in related records created for all updated users
        for(User u:listUserWithRelated){
            u.LastName = u.LastName+'_UPDATED'; // Change the LastName for all users
        }
        update listUserWithRelated;
        List<sObject> tempL = new List<Sobject>(); // Confirm that no new records were created
        tempL = [select Id from ERP_User__c];
        system.debug('Size'+tempL.size()); // Count ERP Users
        tempL = [select Id from Application__c];
        system.debug('Size'+tempL.size());// Count Applications
        tempL = [select Id from Contact];
        system.debug('Size'+tempL.size()); // Count Contacts
    }
}