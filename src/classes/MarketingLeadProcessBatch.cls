global class MarketingLeadProcessBatch implements database.batchable<Sobject>, database.stateful {
   public list<sObject> lstLeads = new List<sObject>();
   
   class MarketingLeadProcessBatchException extends Exception {}
   
   global Database.QueryLocator start(database.BatchableContext bc) {
       string recordTypeName = 'Marketing Leads';
       string currencyType ='AUD';
       Id opid = '0064000000jVXbb';//'0064000000fjmcLAAQ';
      //List<sobject> lineItems = new List<sObject>();
      
      string query = 'SELECT Id,Name,infer3__Infer_Rating__c,email,lastName,phone,ownerId,CompanyDunsNumber,D_U_N_S__c,mkto71_Lead_Score__c,Do_not_route__c,PostalCode,customRoutedLead__c,AutoNumberForDeletionQueueRR__c,createdDate,RecordtypeId  ';
             query += 'FROM lead ';
             query += 'WHERE  ';
             query += 'CompanyDunsNumber != null ';
             query += ' AND recordType.Name =\''+ String.escapeSingleQuotes(recordTypeName)+'\'';
             query += ' AND isConverted=false';
             //AND createddate = Today
      
      system.debug('Query?' + query);
      
      return database.getQueryLocator(query);

   }
   
   global void execute(database.BatchableContext bc,List<Sobject> scope) {
      system.debug('SCOPE SIZE' + scope.size() + 'SIZE ' + scope);
      LeadTriggerHandlerHelper handlerHelper = new LeadTriggerHandlerHelper();
      handlerHelper.matchDUNSandProcess(scope);
    }

   global void finish(database.BatchableContext bc) {
      
      
        
   }
}