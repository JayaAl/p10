global class MarketingLeadsWithDUNSBatch implements database.batchable<Sobject>, database.stateful {
   public list<sObject> lstLeads = new List<sObject>();
   
   class MarketingLeadsWithDUNSException extends Exception {}
   
   global Database.QueryLocator start(database.BatchableContext bc) {
       string recordTypeName = 'Marketing Leads';
       string currencyType ='AUD';
       Id opid = '0064000000jVXbb';//'0064000000fjmcLAAQ';
       Leads_Settings__c leadProcessStartFromDate = Leads_Settings__c.getValues('Lead Process Start Date');
       datetime leadProcessDate = leadProcessStartFromDate.Lead_Process_Date__c;
       
       //Leads_Settings__c leadProcessStartFromDate = Leads_Settings__c.getValues('Lead Processed Until');
       //datetime leadProcessedUntil;// = leadProcessStartFromDate.Lead_Process_Start_Date__c;
       
       system.debug('leadProcessDate?' + leadProcessDate); 
       //String name = 'SRITODAY';
       //String val='\'%' + String.escapeSingleQuotes(name) + '%\'';

      //List<sobject> lineItems = new List<sObject>();
      string query = 'SELECT Id,Name,mkto71_Lead_Score__c,infer3__Infer_Rating__c,';
             query += 'email,firstname,lastName,phone,ownerId,CompanyDunsNumber,LeadSource,D_U_N_S__c,Do_not_route__c,';
             query += 'PostalCode,customRoutedLead__c,AutoNumberForDeletionQueueRR__c,createdDate,RecordtypeId,infer3__Score_Snapshot__c   ';
             query += 'FROM lead ';
             query += 'WHERE  ';
             query += ' CompanyDunsNumber != null ';
             query += ' AND recordType.Name =\''+ String.escapeSingleQuotes(recordTypeName)+'\'';
             query += ' AND isConverted=false';
             query += ' and infer3__Score_Snapshot__c > 0';
             query += ' AND isConverted=false ';
             if(leadProcessDate!=null)
                query += ' AND createddate >=: leadProcessDate' ;
             else
                query += ' and Createddate=TODAY';
             
             query += ' order by createddate asc';
             
      
      
      return database.getQueryLocator(query);

   }
   
   global void execute(database.BatchableContext bc,List<Sobject> scope) {
      system.debug('SCOPE SIZE--' + scope.size() + '--SIZE --' + scope);
      /*for(lead l: (list<lead>)scope)
      {
          system.debug('Lead is: ?' + l);
          
          
      }*/
      
       
      
      LeadTriggerHandlerHelper handlerHelper = new LeadTriggerHandlerHelper();
      handlerHelper.matchDUNSandProcess(scope);
    }

   global void finish(database.BatchableContext bc) {
      
      
        
   }
}