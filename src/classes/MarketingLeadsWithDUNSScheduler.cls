public class MarketingLeadsWithDUNSScheduler implements Schedulable {
    
    public void execute(SchedulableContext sc) {
        MarketingLeadsWithDUNSBatch mlpBatch = new MarketingLeadsWithDUNSBatch();
        Database.executeBatch(mlpBatch,100); 
    } 
}