global class MarketingLeadsWithOutDUNSBatch implements database.batchable<Sobject>, database.stateful {
   public list<sObject> lstLeads = new List<sObject>();
   
   class MarketingLeadsWithOutDUNSException extends Exception {}
   
   global Database.QueryLocator start(database.BatchableContext bc) {
       string recordTypeName = 'Marketing Leads';
       string currencyType ='AUD';
       //Id leadId = '00Q22000001PgPq';//'0064000000fjmcLAAQ';
      
      Leads_Settings__c leadProcessStartFromDate = Leads_Settings__c.getValues('Lead Process Start Date');
      datetime leadProcessDate = leadProcessStartFromDate.Lead_Process_Date__c;// = system.Today();
       
      
        
      string query = 'SELECT Id,Name,mkto71_Lead_Score__c,infer3__Infer_Rating__c,';
             query += 'email,firstname,lastName,phone,ownerId,CompanyDunsNumber,LeadSource,D_U_N_S__c,Do_not_route__c,';
             query += 'PostalCode,customRoutedLead__c,AutoNumberForDeletionQueueRR__c,createdDate,RecordtypeId,infer3__Score_Snapshot__c   ';
             query += ' FROM lead ';
             query += ' WHERE  ';
             query += ' CompanyDunsNumber = null'; 
             query += ' and infer3__Score_Snapshot__c > 0';
             query += ' and mkto71_Lead_Score__c>=75 ';
             query += ' AND recordType.Name =\''+ String.escapeSingleQuotes(recordTypeName)+'\'';
             query += ' AND isConverted=false ';
             //query += ' AND CreatedDate=TODAY ';
             
             if(leadProcessDate!=null)
                query += ' AND CreatedDate >=: leadProcessDate' ;
             
             if(Test.isRunningTest()==false)
                query += ' and CreatedDate< YESTERDAY';
             
             query += ' order by CreatedDate asc';
      
            system.debug('Query?' + query);
      
      return database.getQueryLocator(query);

   }
   
   global void execute(database.BatchableContext bc,List<Sobject> scope) {
      system.debug('SCOPE SIZE' + scope.size() + 'SIZE ' + scope);
      LeadTriggerHandlerHelper handlerHelper = new LeadTriggerHandlerHelper();
      handlerHelper.formMapForNullDunsToSalesLeads(scope);
    }

   global void finish(database.BatchableContext bc) {
      
      
        
   }
}