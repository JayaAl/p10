public class MarketingLeadsWithOutDUNSScheduler implements Schedulable {
    
    public void execute(SchedulableContext sc) {
        MarketingLeadsWithOutDUNSBatch mlWODBatch = new MarketingLeadsWithOutDUNSBatch();
        Database.executeBatch(mlWODBatch,100);
    }
}