/**
 * See https://github.com/financialforcedev/fflib-apex-common for more info
 *
 * Install library via
 *   https://githubsfdeploy.herokuapp.com/app/githubdeploy/financialforcedev/fflib-apex-common
 */

/**
 * Encapsulates all service layer logic for a given function or module in the application
 * 
 * For more guidelines and details see 
 *   https://developer.salesforce.com/page/Apex_Enterprise_Patterns_-_Service_Layer
 *
 **/
public class MergeAccountService implements Queueable
{	
	public Map<Id,List<Contact>> acctIdToContIdMap;
	public static Map<Id,List<Contact>> acctIdToContIdStaticMap;
	public void execute(QueueableContext qc) {
        
        try{
           
            List<Contact> contactSet = new List<COntact>();
            system.debug('acctIdToContIdMap ===>'+acctIdToContIdMap);
            Set<Id> contIdSet = new Set<Id>();
            for(List<Contact> contLst : acctIdToContIdMap.values() ){
            	for(Contact cont : contLst)
            		contIdSet.add(cont.id);
            }

            if(!contIdSet.isEmpty()){
                Map<Id,Contact> contactMap = new Map<Id,Contact>([Select Id,Name,ERP_Contact_Sync_ID__c from Contact where id IN: contIdSet ]);
        		for(Id acctIdVar : acctIdToContIdMap.keySet()){

        			for(Contact contactVar : acctIdToContIdMap.get(acctIdVar)){
        				 Contact newContactObj = contactMap.get(contactVar.id);
        				 String contact_sync_str = (String.valueOf(acctIdVar)).substring(0,15)+'-'+String.valueOf(contactVar.LastModifiedDate).right(9);
        				 newContactObj.ERP_Contact_Sync_ID__c = contact_sync_str.deleteWhitespace(); 
        				 System.debug('contactVar ==>' +newContactObj);
        				 contactSet.add(newContactObj);
        			}
        		}
            }

            if(!contactSet.isEmpty())
            	update contactSet;

    	}catch(Exception e){

            Logger.logMessages(new List<Error_Log__c> {new Error_Log__c(Class__c = 'MergeAccountService',
                        Method__c = 'execute',
                        Error__c = e.getMessage()+e.getStackTraceString(),
                        Object__c = 'Account',
                        ID_List__c = null,
                        Running_user__c = UserInfo.getUserId(),
                        Type__c = 'Error'
                    )});   
    	}
    }

}