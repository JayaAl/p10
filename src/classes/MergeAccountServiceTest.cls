@isTest
public class MergeAccountServiceTest{
    
     @isTest static void constructorTest() {
       
       string TEST_STRING = 'test Account'   ;
       
       Account acc = new Account(
            name = TEST_STRING                      
        );
        insert acc;

         Account acc2 = new Account(
            name = TEST_STRING+'2'                      
        );

        insert acc2;
        
        Test.starttest();        
         Database.merge(acc, new List<Account>{acc,acc2}, false);
        Test.stopTest();
     }

}