global class MyCaseTemplateChooser implements Support.EmailTemplateSelector {
    // Empty constructor 
    global MyCaseTemplateChooser() {    }

    // The main interface method 
    global ID getDefaultEmailTemplateId(ID caseId) {
        // Select the case we're interested in, choosing any fields that are relevant to our decision
        Case c = [SELECT Status, Reason, Id, RecordType.DeveloperName, Language__c  FROM Case 
                      WHERE Id=:caseId];

        EmailTemplate et = null;

        if (c.RecordType.DeveloperName != 'AMP' && c.RecordType.DeveloperName != 'Artist_Support' && c.RecordType.DeveloperName != 'Next_Big_Sound' && c.RecordType.DeveloperName != 'User_Support' ){

            } else if (c.RecordType.DeveloperName == 'AMP'){
                et = getTemplateIdHelper('AMP_General_Email');
            } else if (c.RecordType.DeveloperName == 'Artist_Support'){
                et = getTemplateIdHelper('Artist_Support_General_Email');
            } else if (c.RecordType.DeveloperName == 'Next_Big_Sound'){
                et = getTemplateIdHelper('NBS_General_Email');
            } else if (c.RecordType.DeveloperName == 'User_Support' && c.Language__c == 'es'){
                et = getTemplateIdHelper('User_Support_General_Email_Spanish');
            }else if (c.RecordType.DeveloperName == 'User_Support'){
                et = getTemplateIdHelper('User_Support_General_Email');
            }
        
        // Return the ID of the template selected
        if(et != Null){
            return et.id;}
        else {
            return null;
        }
    }
    
    private EmailTemplate getTemplateIdHelper(String templateApiName) {
        EmailTemplate templateId = null;
        try {
            templateId = [select id, name from EmailTemplate 
                          where developername = : templateApiName];
        } catch (Exception e) {
            system.debug('Unble to locate EmailTemplate using name: ' + 
                templateApiName + ' refer to Setup | Communications Templates ' 
                    + templateApiName);
        }
        return templateId;
    }
}