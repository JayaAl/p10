@isTest
private class MyCaseTemplateChooserTest {

    static testMethod void testChooseTemplate() {
    
        MyCaseTemplateChooser chooser = new MyCaseTemplateChooser();

        RecordType rt = [SELECT Id FROM RecordType WHERE SobjectType = 'Case' AND DeveloperName = 'AMP'];
        
        // Create a simulated case to test with
        Case c = new Case();
        c.Subject = 'Test';
        c.RecordTypeId = rt.Id;
        Database.insert(c);
            
        // Make sure the proper template is chosen for this subject
        Id actualTemplateId = chooser.getDefaultEmailTemplateId(c.Id);
        EmailTemplate expectedTemplate = 
          [SELECT id FROM EmailTemplate WHERE DeveloperName = 'AMP_General_Email'];
        Id expectedTemplateId = expectedTemplate.Id;
        System.assertEquals(actualTemplateId, expectedTemplateId);
  
        rt = [SELECT Id FROM RecordType WHERE SobjectType = 'Case' AND DeveloperName = 'Artist_Support'];
        
        // Change the case properties to match a different template
        c.RecordTypeId = rt.Id;
        Database.update(c);
  
        // Make sure the correct template is chosen in this case
       actualTemplateId = chooser.getDefaultEmailTemplateId(c.Id);
        expectedTemplate = 
          [SELECT id FROM EmailTemplate WHERE DeveloperName = 'Artist_Support_General_Email'];
        expectedTemplateId = expectedTemplate.Id;
        System.assertEquals(actualTemplateId, expectedTemplateId);
        
        rt = [SELECT Id FROM RecordType WHERE SobjectType = 'Case' AND DeveloperName = 'Next_Big_Sound'];
        
        // Change the case properties to match a different template
        c.RecordTypeId = rt.Id;
        Database.update(c);
  
        // Make sure the correct template is chosen in this case
       actualTemplateId = chooser.getDefaultEmailTemplateId(c.Id);
        expectedTemplate = 
          [SELECT id FROM EmailTemplate WHERE DeveloperName = 'NBS_General_Email'];
        expectedTemplateId = expectedTemplate.Id;
        System.assertEquals(actualTemplateId, expectedTemplateId);
        
        rt = [SELECT Id FROM RecordType WHERE SobjectType = 'Case' AND DeveloperName = 'User_Support'];
        
        // Change the case properties to match a different template
        c.RecordTypeId = rt.Id;
        Database.update(c);
  
        // Make sure the correct template is chosen in this case
       actualTemplateId = chooser.getDefaultEmailTemplateId(c.Id);
        expectedTemplate = 
          [SELECT id FROM EmailTemplate WHERE DeveloperName = 'User_Support_General_Email'];
        expectedTemplateId = expectedTemplate.Id;
        System.assertEquals(actualTemplateId, expectedTemplateId);
        
        rt = [SELECT Id FROM RecordType WHERE SobjectType = 'Case' AND DeveloperName = 'User_Support'];
        
        // Change the case properties to match a different template
        c.RecordTypeId = rt.Id;
        c.Language__c = 'es';
        Database.update(c);
  
        // Make sure the correct template is chosen in this case
       actualTemplateId = chooser.getDefaultEmailTemplateId(c.Id);
        expectedTemplate = 
          [SELECT id FROM EmailTemplate WHERE DeveloperName = 'User_Support_General_Email_Spanish'];
        expectedTemplateId = expectedTemplate.Id;
        System.assertEquals(actualTemplateId, expectedTemplateId);
    
    }
}