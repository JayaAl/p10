public with sharing class NewBillingCase {
    
    public Case caseRecord{get;set;}
    public String caseCommentText{get;set;}
    public String caseOwner{get;set;}
    public CaseComment caseCommentRec;
    public Id opportunityId;

    public void resetData() {
        
        caseRecord = new Case();
        caseCommentRec = new CaseComment();
        caseCommentText = '';
        caseOwner = UserInfo.getName();        
    }
    public NewBillingCase(){

        resetData();
    }
    public NewBillingCase(ApexPages.StandardController stdController) {

        resetData();
        Opportunity opptyREC = (Opportunity)stdController.getRecord();
        opportunityId = opptyREC.Id;
        prePopulate(opptyREC.Id);
    }
    public PageReference prePopulate(Id opptyId) {

        Date todaysDt = Date.today().addDays(2);
        

        if(NewBillingCaseUtil.isParamOpptyId(opptyId)) {

            try {

                Opportunity oppty = new Opportunity();
                oppty = NewBillingCaseUtil.getOpportunity(opptyId);
                caseRecord.RecordTypeId = NewBillingCaseUtil.getBillingcaseRecordType();
                caseRecord.AccountId = oppty.AccountId;
                caseRecord.Opportunity__c = oppty.Id;
                caseRecord.OwnerId = UserInfo.getUserId();
                caseRecord.Campaign_Start_Date2__c = Date.today();
                caseRecord.Campaign_End_Date2__c = Date.today();
                caseRecord.Revenue_Forecasted__c = 0.0;
                caseRecord.Type = '';
                caseRecord.Priority = 'Medium';
                caseRecord.Due_Date__c = todaysDt;
                //caseRecord.Comments = '';
                caseRecord.Status = 'New';
                caseCommentRec.CommentBody = caseCommentText;

                OpportunityContactRole primaryOpptyRole = new OpportunityContactRole();
                primaryOpptyRole = NewBillingCaseUtil.getOpportunityPrimaryContact(opptyId);
                if(primaryOpptyRole.Id != null) {

                    caseRecord.ContactId = primaryOpptyRole.ContactId;
                }
                Ad_Operations__c adOpsRec = new Ad_Operations__c();
                adOpsRec = NewBillingCaseUtil.getOptyadOperations(opptyId);
                if(adOpsRec.Id != null) {

                    caseRecord.Ad_Ops__c = adOpsRec.Id;
                    caseRecord.DFP_Order_ID__c = adOpsRec.DFP_Order_ID__c;
                    caseRecord.Billing_Ad_Server__c = adOpsRec.Billing_Ad_Server__c;
                }
            } catch(Exception e) {

                caseRecord.DFP_Order_ID__c = 'blabla';
                caseRecord.Billing_Ad_Server__c = 'blablabla address';
            }

        }
        
        return null;
    }

    public PageReference save() {

        Id createdCaseId = createCase();
        PageReference pref = new PageReference('/'+createdCaseId);
        return pref;
    }
    public PageReference saveAndNew() {

        Id createdCaseId = createCase();
        PageReference pref = new PageReference('/apex/New_Billing_Case?id='+opportunityId);
        resetData();
        prePopulate(opportunityId);
        return pref;
    }
    public Id createCase(){

        insert caseRecord;
        caseCommentRec.ParentId = caseRecord.Id;
        insert caseCommentRec;
        return caseRecord.Id;
    }
}