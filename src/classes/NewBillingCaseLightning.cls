public with sharing class NewBillingCaseLightning {
	
	public NewBillingCaseLightning() {}

	@AuraEnabled
	public static list<LightningWrapper> getPrepopulateData(Id opptyId) {

		
		list<LightningWrapper> billCaseDetails = new list<LightningWrapper>();
		NewBillingCase ref = new NewBillingCase();
		ref.prePopulate(opptyId);
		LightningWrapper wrapperRef = new LightningWrapper();
		wrapperRef.caseObj = ref.caseRecord;
		billCaseDetails.add(wrapperRef);
		return billCaseDetails;
	}
}