/*
*/
public with sharing class NewBillingCaseUtil {

	public static Id getBillingcaseRecordType() {

		return Schema.SObjectType.Case.getRecordTypeInfosByName().get('Billing Case').getRecordTypeId();
 	}
	public static Opportunity getOpportunity(Id opptyId) {
		
		// validate opptyId
		// retrive opportunity	
		Opportunity oppty = [SELECT Id, AccountId
                                FROM Opportunity
                                WHERE Id =: opptyId];
        return oppty;
	}
	public static  Ad_Operations__c getOptyadOperations(Id opptyId) {
		
		// validate opptyId
		// retrive adOperations
		Ad_Operations__c adOpsRec = new Ad_Operations__c();
		if(isParamOpptyId(opptyId)) {
			adOpsRec = [SELECT Id, Name,
                               DFP_Order_ID__c, 
                               Billing_Ad_Server__c
                        FROM Ad_Operations__c
                        WHERE Opportunity__c = :opptyId
                        ORDER BY CreatedDate Desc LIMIT 1];
        }
		return adOpsRec;
	}
	public static  OpportunityContactRole getOpportunityPrimaryContact(Id opptyId) {

		OpportunityContactRole primaryOpptyRole = new OpportunityContactRole();
		if(isParamOpptyId(opptyId)) {

			primaryOpptyRole = [SELECT ContactId,
                                        Id,
                                        IsPrimary,
                                        OpportunityId,
                                        Role 
                                FROM OpportunityContactRole 
                                WHERE OpportunityId = :opptyId
                                AND IsPrimary = true];	
		}
		
        return primaryOpptyRole;
	}
	public static Boolean isParamOpptyId(String opptyId) {

		return opptyId.startsWith('006');
	}
}