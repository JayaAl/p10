/***************************************************
   Name: NewUserRegistrationController
   Usage: This Class Changes Picklist value of field Action
   Author – ''
   Date – 07/29/2017   
   Revision History
******************************************************/

public class NewUserRegistrationController {

    /* public Boolean isAdvertiser{get;set;}
  public Boolean isAgency{get;set;}
  public string agencyAdvRadio {get;set;}*/
    public String customerEmail {
        get;
        set;
    }

    private static final string initVector = 'eudbvGn2XQ1lUyJY';

    public NewUserRegistrationController() {
        /* isAdvertiser = false;
   isAgency = false;*/
    }


    public Boolean isValid() {
        Boolean validationPass = true;

        if (string.isblank(customerEmail)) {
            return false;
        }
        return validationPass;
    }

    public PageReference Next() {
        Pagereference p;
        if (isValid()) {
            String acctName;
            List < Contact > conts = new List < Contact > ();
            Set < Id > acctIdSet = new Set < Id > ();
            List < Account > accts = new List < Account > ();
            String emailRegex = '([a-zA-Z0-9_\\-\\.]+)@((\\[a-z]{1,3}\\.[a-z]{1,3}\\.[a-z]{1,3}\\.)|(([a-zA-Z0-9\\-]+\\.)+))([a-zA-Z]{2,4}|[0-9]{1,3})';
            Pattern MyPattern = Pattern.compile(emailRegex);
            Matcher MyMatcher = MyPattern.matcher(customerEmail);

            if (!MyMatcher.matches()) {
                ApexPages.addmessage(new ApexPages.message(ApexPages.severity.WARNING, 'Please enter Valid Email Address'));
            } else {
                system.debug('customer email----' + customerEmail);
                acctName = customerEmail.split('@').get(1);
                if (Restricted_Domains__c.getInstance(acctName) != NULL) {
                    ApexPages.addmessage(new ApexPages.message(ApexPages.severity.WARNING, acctName + ' is not a valid email domain.'));
                    return NULL;
                } else {
                    acctName = '%' + acctName;
                }

                List < user > udupe = [select email from User where email = :customerEmail];
                system.debug('udupe ::::-->' + udupe);
                if (!udupe.isEmpty()) {
                    // ' A user already exists with this email.'
                    ApexPages.addmessage(new ApexPages.message(ApexPages.severity.WARNING, 'You indicated you are a new customer, but an account already exists with the e-mail ' + customerEmail));
                    return NULL;
                }
                conts = [select email, AccountId, account.name from Contact where email LIKE: acctName And AccountId != null]; //email,accountId from Contact where Account.Type = 'Advertiser' and Account.Name =: acctName];  
                /*
    if(isAdvertiser){
        conts = [select accountId from Contact where Account.Type = 'Advertiser' and Account.Name =: acctName];
    }else{
        conts = [select accountId from Contact where Account.Type = 'Ad Agency' and Account.Name =: acctName];              
    }*/
                system.debug('acctName---' + acctName);
                system.debug('conts---' + conts);
                if (conts.size() > 0) {
                    for (Contact c: conts) {
                        /*if(c.Email.equalsIgnoreCase(customerEmail)){
          ApexPages.addmessage(new ApexPages.message(ApexPages.severity.WARNING, ' A user already exists with this email.'));                              
          return NULL; 
      }*/
                        acctIdSet.add(c.AccountId);
                    }
                    String acctQuery = 'SELECT ';
                    for (Schema.FieldSetMember f: SObjectType.Account.FieldSets.NewUserRegistration.getFields()) {
                        acctQuery += f.getFieldPath() + ',';
                    }
                    acctQuery = acctQuery.removeEnd(',');
                    acctQuery += ' FROM Account WHERE Id IN: acctIdSet';
                   // accts = database.query(acctQuery);

                    System.debug('ACCOUNTS' + accts);

                    OrgWideEmailAddress[] owea = [select Id from OrgWideEmailAddress where Address = 'donotreply-vana@pandora.com'];

                    Messaging.SingleEmailMessage message = new Messaging.SingleEmailMessage();
                    // Set recipients to two contact IDs.
                    // Replace IDs with valid record IDs in your org.
                    message.toAddresses = new String[] {
                        customerEmail
                    };
                    message.subject = 'Pandora Account Registration : Possible Accounts found.';
                    //message.setSenderDisplayName('DoNotReply@Pandora.com');
                    /*
     EmailTemplate templateId = [Select id from EmailTemplate where name = 'SelfRegEmail'];
     message.setTargetObjectId(cts[0].Id);
     message.setWhatId(cts[0].Id);
     message.setTemplateID(templateId.Id);
     message.setSaveAsActivity(false);   
     */

                    //TO-DO: make the base URL dynamic; update image path for Production                    
                    string htmlBody = '<img src="https://pandora--pandora10--c.cs30.content.force.com/servlet/servlet.ImageServer?id=015n0000000E7ld&oid=00Dn00000000aug&lastMod=1502341987000" width="200"/> <br/><br/>';
                    htmlBody += 'Please register by selecting one of the Accounts below : <br/><br/>';

                    //Table Header
                    htmlBody += '<table cellpadding="3" style="border: thin solid #FFFFFF; font-size: 16px; font-family:\'trebuchet MS\';" cellpadding="2"><tr>';
                    htmlBody += '<th scope="col" style="background-color: #b9c9fe; color: #039; font-weight: bold;">' + 'Link' + '</th>';
                    for (Schema.FieldSetMember f: SObjectType.Account.FieldSets.NewUserRegistration.getFields()) {
                        if (f.getFieldPath() != 'Id') {

                            htmlBody += '<th scope="col" style="background-color: #b9c9fe; color: #039; font-weight: bold;">' + (f.getlabel()).replace('Location', '') + '</th>';
                        }
                    }
                    htmlBody += '</tr>';
                    Blob key = blob.valueOf(initVector);
                    Blob emaildata = Blob.valueOf(customerEmail);
                    Blob encryptedemail = Crypto.encryptWithManagedIV('AES128', key, emaildata);
                    string encryptedemailId = EncodingUtil.base64Encode(encryptedemail);

                    //end table header                 
                    for (Account a: database.query(acctQuery)) {

                        Blob data = Blob.valueOf(a.Id);
                        Blob encrypted = Crypto.encryptWithManagedIV('AES128', key, data);
                        string encryptedId = EncodingUtil.base64Encode(encrypted);

                        //table body
                        //table row
                        htmlBody += '<tr>';
                        htmlBody += '<td style="background-color: #e8edff; color: #669;text-align: right" scope="row"><a href="https://pandora10-pandoraforbrands.cs30.force.com/ads/CommunitiesSelfReg?acctId=' + EncodingUtil.urlEncode(encryptedId, 'UTF-8') + '&nEmail=' + EncodingUtil.urlEncode(encryptedemailId, 'UTF-8') + '">Register with this Account</a></td>';
                        for (Schema.FieldSetMember f: SObjectType.Account.FieldSets.NewUserRegistration.getFields()) {

                            if (f.getFieldPath() != 'Id') {

                                htmlBody += '<td style="background-color: #e8edff; color: #669;text-align: right" scope="row">';
                                if (a.get(f.getFieldPath()) == null) {
                                    htmlBody += 'None';
                                } else {
                                    htmlBody += a.get(f.getFieldPath());
                                }

                                htmlBody += '</td>';
                            }

                        }
                        //end table row
                        htmlBody += '</tr>';

                    }
                    //end table
                    htmlBody += '</table>';
                    htmlBody += '<br/>';
                    htmlBody += 'If you want to create a new Account Click <a href="https://pandora10-pandoraforbrands.cs30.force.com/ads/CommunitiesSelfReg?nEmail=' + EncodingUtil.urlEncode(encryptedemailId, 'UTF-8') + '">Here</a>';
                    message.setOrgWideEmailAddressId(owea.get(0).Id);
                    message.setHtmlBody(htmlBody);
                    Messaging.SingleEmailMessage[] messages = new List < Messaging.SingleEmailMessage > {
                        message
                    };
                    Messaging.SendEmailResult[] results = Messaging.sendEmail(messages);

                    ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.INFO, 'We’ve found some accounts related to your user!  Please check your email for additional instructions.'));
                } else {

                    //Scenario 2
                    p = new PageReference('/apex/CommunitiesSelfReg');
                    p.setRedirect(true);
                    p.getParameters().put('accountName', acctName);
                    p.getParameters().put('cEmail', customerEmail);
                }

            }
        } else {
            ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.WARNING, 'Please enter an email.'));
            return null;
        }
        return p;

    }

    public pagereference cancel() {
        return Network.communitiesLanding();
    }

}