/**
* ─────────────────────────────────────────────────────────────────────────────────────────────────┐
* This class is  helper for Non guaranteed tab.
* ──────────────────────────────────────────────────────────────────────────────────────────────────
* @author         Jaya Alaparthi
* @maintainedBy   Jaya Alaparthi
* @version        1.0
* @created        2017-11-15
* @modified       
* @systemLayer    Helper
* @see            NonGuaranteedUtil.cls
* @see            ????
* ──────────────────────────────────────────────────────────────────────────────────────────────────
* @changes
* v1.1          Jaya Alaparthi
* YYYY-MM-DD    
* ──────────────────────────────────────────────────────────────────────────────────────────────────
* @changes
* v1.1          Jaya Alaparthi
* YYYY-MM-DD       
* ─────────────────────────────────────────────────────────────────────────────────────────────────┘
*/
public with sharing class NonGuaranteedHelper {
	
	//───────────────────────────────────────────────────────────────────────────┐
	// param		type					
	// 
	// ──────────────────────────────────────────────────────────────────────────
	// Description: getCurrentUserId returns current userId						
	//───────────────────────────────────────────────────────────────────────────┘
	@AuraEnabled
	public static User getCurrentUserId() {

		Id currentUserId = UserInfo.getUserId();
		User userObj = [SELECT Id,Email,Name FROM User WHERE Id =: currentUserId];
		return userObj;
	}
	//───────────────────────────────────────────────────────────────────────────┐
	// param		type					
	// 
	// ──────────────────────────────────────────────────────────────────────────
	// Description: getNonguaranteedList 
	//				retrive all realted Account forecasts 
	//				for the give details
	//			list<NonGuaranteedWrapper>							
	//───────────────────────────────────────────────────────────────────────────┘
	@AuraEnabled
	public static void getData(String acctName,String acctStatus,
											String acctOwnerId) {
		System.debug('acctName:'+acctName+'acctStatus:'+acctStatus+'acctOwnerId:'+acctOwnerId);
	}
	//───────────────────────────────────────────────────────────────────────────┐
	// param		type					
	// 
	// ──────────────────────────────────────────────────────────────────────────
	// Description: getNonguaranteedList 
	//				retrive all realted Account forecasts 
	//				for the give details
	//			list<NonGuaranteedWrapper>							
	//───────────────────────────────────────────────────────────────────────────┘
	@AuraEnabled
	public static list<NonGuaranteedWrapper> getNonguaranteedList(String acctName,
																String acctStatus,
																String acctOwnerId) {

		System.debug('acctName:'+acctName+'acctStatus:'+acctStatus+'acctOwnerId:'+acctOwnerId);
		list<Opportunity> opptyList = new list<Opportunity>();
		list<Integer> quartersList = new list<Integer>();
		list<NonGuaranteedWrapper> nonGuaranteedWrapperList = 
												new list<NonGuaranteedWrapper>();

		if(!String.isBlank(acctOwnerId)) {
			// retrive current quarter and previous quarter
			quartersList = getCurrentandPrevQuarter();
			list<Integer> yearsList = getCurrentandPrevYears();
			// retrive all the opportunities owned by the current acctOwner
			opptyList = NonGuaranteedUtil.getRelatedOpportunities(acctName,
																acctStatus,
																acctOwnerId,
																quartersList[0],
																quartersList[1]);
			// format a map with adv|agency => wrapper record
			// retrive all the Account forecast details for every adv}agency combination
			nonGuaranteedWrapperList = formatOpportunities(opptyList,UserInfo.getUserId(),quartersList);
		}
		return nonGuaranteedWrapperList;
	}
	//───────────────────────────────────────────────────────────────────────────┐
	// param		type					
	// 
	// ──────────────────────────────────────────────────────────────────────────
	// Description: format Opportunities	
	//			list<NonGuaranteedWrapper>							
	//───────────────────────────────────────────────────────────────────────────┘
	private static list<NonGuaranteedWrapper> formatOpportunities(list<Opportunity> opptyList,
																	String acctOwnerId,
																	list<Integer> quartersList) {

		map<String,NonGuaranteedWrapper> nonGuaranteedData = 
											new map<String,NonGuaranteedWrapper>();
		list<NonGuaranteedWrapper> nonGuaranteedWrapperList = new list<NonGuaranteedWrapper>();
		map<String,Account_Forecast_Details__c> acctForecastDetails = 
								new map<String,Account_Forecast_Details__c>();

		set<String> advertiserAgencyList = new set<String>();

		// get acctOwner employeId
		//acctOwnerId
		String employeeId = NonGuaranteedUtil.getEmployeeId(acctOwnerId);
		if(!opptyList.isEmpty()) {

			// get current and previous years
			list<Integer> yearsList = getCurrentandPrevYears();
			Integer currentQuarter = NonGuaranteedUtil.getQuarter(Date.today().month());
			// retrive account forecast details
			for(Opportunity oppty: opptyList) {

				String mapId = getMapKey(oppty.AccountId,oppty.Agency__c);
				mapId = oppty.Owner.EmployeeNumber+'-'+mapId;
				// append agent EmployeeNumber. 
				// but confirm if the agent would be the one on Opportunity owner
				// get both current year and previous year append only if curent quarter is 1
				if(yearsList[0] != yearsList[1] && currentQuarter == 1) {
					advertiserAgencyList.add(mapId+'-'+yearsList[0]);
					advertiserAgencyList.add(mapId+'-'+yearsList[1]);
				} else {
					advertiserAgencyList.add(mapId+'-'+yearsList[0]);
				}
				System.debug('b4 retriving acctforecastetails advertiserAgencyList:'+advertiserAgencyList);
			}
			if(!advertiserAgencyList.isEmpty()) {
				// retrive accountforecastdetails
				acctForecastDetails = NonGuaranteedUtil.getAccountForecastDetails(
																advertiserAgencyList);

			}
			// create wrapper list
			for(Opportunity oppty: opptyList) {

				NonGuaranteedWrapper wrapperObj =  new NonGuaranteedWrapper();
				String mapKey = '';
				Double booked = 0;
				Double pipelined = 0;

				if(oppty.StageName == 'Closed Won' && oppty.Amount != null) {

					booked += oppty.Amount != null ? oppty.Amount : 0;
				} else {

					pipelined += oppty.Amount != null ? oppty.Amount : 0;
				}
				// prepare map key
				//mapKey = getMapKey(oppty.Account.Name,oppty.Agency__r.Name);
				// prepare forecastdetails mapid
				String mapId = getMapKey(oppty.AccountId,oppty.Agency__c);
				//mapId.replace('|','-');
				mapId = oppty.Owner.EmployeeNumber+'-'+mapId;
				System.debug('mapId:'+mapId);
				System.debug('nonGuaranteedData:'+nonGuaranteedData);
				if(!nonGuaranteedData.isEmpty() && String.isBlank(mapId)
					&& nonGuaranteedData.containsKey(mapId)) {

					wrapperObj = nonGuaranteedData.get(mapId);
				} else {

					System.debug('oppty.Account.Name:'+oppty.Account.Name);
					wrapperObj.advertiser = oppty.Account.Name;
					wrapperObj.agency = oppty.Agency__r.Name;
					wrapperObj.currentQuarterLow =	0;
					wrapperObj.currentQuarterMid = 0;
					wrapperObj.currentQuarterHigh = 0;
					wrapperObj.lastQuarterLow = 0;
					wrapperObj.lastQuarterMid = 0;
					wrapperObj.lastQuarterHigh = 0;
					wrapperObj.currentQuarterBooked = booked;
					wrapperObj.currentQuarterPipeline = pipelined;

					System.debug('wrapperObj:'+wrapperObj);
				}
				System.debug('wrapperObj:'+wrapperObj);
				if(oppty.StageName == 'Closed Won' && oppty.Amount != null) {

					
					wrapperObj.currentQuarterBooked = 
									wrapperObj.currentQuarterBooked != null ?
									(wrapperObj.currentQuarterBooked + pipelined) : booked;
				} 
				if(oppty.StageName != 'Closed Won' && oppty.StageName != 'Closed Lost') {

					System.debug('wrapperObj:'+wrapperObj);
					wrapperObj.currentQuarterPipeline = 
									wrapperObj.currentQuarterPipeline != null ?
									(wrapperObj.currentQuarterPipeline + pipelined) : pipelined;
					
				}

				// adding forecast details to wrapper obj
				String currentForecastKey,prevForecastKey;
				
				String tempKey = getMapKey(oppty.Agency__c,oppty.AccountId);
				tempKey.replace('|','-');
				// the above replace is not working
				System.debug('tempKey:'+tempKey);
				currentForecastKey = employeeId+'-'+tempKey+'-'+yearsList[0];
				prevForecastKey = employeeId+'-'+tempKey+'-'+yearsList[1];

				// adding forecast details
				System.debug('acctForecastDetails:'+acctForecastDetails+'mapId:'+mapId
						+'\n currentForecastKey:'+currentForecastKey+'\n prevForecastKey:'+prevForecastKey);
				if(!acctForecastDetails.isEmpty() 
						&& (acctForecastDetails.containsKey(currentForecastKey)
							|| acctForecastDetails.containsKey(prevForecastKey))) {

					
					if(acctForecastDetails.containsKey(currentForecastKey)) {

						Account_Forecast_Details__c currentForecastDetails = 
										acctForecastDetails.get(currentForecastKey);	
						wrapperObj.currentAcctforecast = currentForecastDetails;
					}

					if(acctForecastDetails.containsKey(prevForecastKey)) {

						Account_Forecast_Details__c prevForecastDetails = 
										acctForecastDetails.get(prevForecastKey);	
						wrapperObj.prevAcctforecast = prevForecastDetails;
					}
					// update forecast details on wrapper members from wrapper forecast member
					wrapperObj = loadForecastData(wrapperObj,quartersList);
					System.debug('wrapperObj:'+wrapperObj);
				}
				nonGuaranteedData.put(mapId,wrapperObj);	
				//nonGuaranteedWrapperList.add(wrapperObj);				
			}// end of for loop
			
		}
		nonGuaranteedWrapperList = nonGuaranteedData.values();
		System.debug('nonGuaranteedWrapperList:'+nonGuaranteedWrapperList);
		return nonGuaranteedWrapperList;
	}
	//───────────────────────────────────────────────────────────────────────────┐
	// param		type					
	// 
	// ──────────────────────────────────────────────────────────────────────────
	// Description: getCurrentandPrevYears
	//										
	//───────────────────────────────────────────────────────────────────────────┘
	private static list<Integer> getCurrentandPrevYears(){

		list<Integer> currPreYearsList = new list<Integer>();

		Integer currYear = Date.today().Year();
		Integer prevYear = Date.today().addYears(-1).Year();
		currPreYearsList.add(currYear);
		currPreYearsList.add(prevYear);

		return currPreYearsList;
	}
	//───────────────────────────────────────────────────────────────────────────┐
	// param		type					
	// 
	// ──────────────────────────────────────────────────────────────────────────
	// Description: getCurrentandPrevQuarter current and previous quarters
	//										
	//───────────────────────────────────────────────────────────────────────────┘
	private static list<Integer> getCurrentandPrevQuarter() {

		list<Integer> quarter = new list<Integer>();
		Integer prevQuarter = 0;
		Date currentDate = Date.today();
		Integer currentQuarter = NonGuaranteedUtil.getQuarter(currentDate.month());

		if(currentQuarter > 1 && currentQuarter <= 4) {

			prevQuarter = currentQuarter -1;
		} 
		if(currentQuarter == 1) {

			prevQuarter = 4;
		}
		quarter.add(currentQuarter);
		quarter.add(prevQuarter);

		return quarter;
	}
	//───────────────────────────────────────────────────────────────────────────┐
	// param		type					
	// 
	// ──────────────────────────────────────────────────────────────────────────
	// Description:  getMapKey generate map key using advertiser and agency names
	//										
	//───────────────────────────────────────────────────────────────────────────┘
	private static String getMapKey(String advertiserName, String agencyName) {

		String mapKey = '';

		if(!String.isBlank(advertiserName)) {

			mapKey = advertiserName;
		} 
		if(!String.isBlank(agencyName)) {

			mapKey += '-'+agencyName;
		}
		return mapKey;
	}
	//───────────────────────────────────────────────────────────────────────────┐
	// param		type					
	// 
	// ──────────────────────────────────────────────────────────────────────────
	// Description: loadForecastData 
	//				update account forecast details on to current and previous 
	//				members of the wrapper object
	//				based on current and previous quarters
	//───────────────────────────────────────────────────────────────────────────┘
	private static NonGuaranteedWrapper loadForecastData(NonGuaranteedWrapper wrapperObj,
														list<Integer> quartersList) {

		// current quarter is always the first value in the quartersList
		Integer currentQuarter = quartersList[0];
		
		System.debug('In loadForecastData:'+quartersList);
		if(currentQuarter == 1) {

			System.debug('wrapperObj.currentAcctforecast.Rep_Forecast_Q1_Low__c:'+wrapperObj.currentAcctforecast.Rep_Forecast_Q1_Low__c);
			wrapperObj.currentQuarterLow = wrapperObj.currentAcctforecast.Rep_Forecast_Q1_Low__c;
			wrapperObj.currentQuarterMid = wrapperObj.currentAcctforecast.Rep_Forecast_Q1_Mid__c;
			wrapperObj.currentQuarterHigh = wrapperObj.currentAcctforecast.Rep_Forecast_Q1__c;

			wrapperObj.lastQuarterLow = wrapperObj.prevAcctforecast.Rep_Forecast_Q4_Low__c;
			wrapperObj.lastQuarterMid = wrapperObj.prevAcctforecast.Rep_Forecast_Q4_Mid__c;
			wrapperObj.lastQuarterHigh = wrapperObj.prevAcctforecast.Rep_Forecast_Q4__c;

		} else if(currentQuarter == 2) {

			wrapperObj.currentQuarterLow = wrapperObj.currentAcctforecast.Rep_Forecast_Q2_Low__c;
			wrapperObj.currentQuarterMid = wrapperObj.currentAcctforecast.Rep_Forecast_Q2_Mid__c;
			wrapperObj.currentQuarterHigh = wrapperObj.currentAcctforecast.Rep_Forecast_Q2__c;

			wrapperObj.lastQuarterLow = wrapperObj.prevAcctforecast.Rep_Forecast_Q1_Low__c;
			wrapperObj.lastQuarterMid = wrapperObj.prevAcctforecast.Rep_Forecast_Q1_Mid__c;
			wrapperObj.lastQuarterHigh = wrapperObj.prevAcctforecast.Rep_Forecast_Q1__c;

		} else if(currentQuarter == 3) {

			wrapperObj.currentQuarterLow = wrapperObj.currentAcctforecast.Rep_Forecast_Q3_Low__c;
			wrapperObj.currentQuarterMid = wrapperObj.currentAcctforecast.Rep_Forecast_Q3_Mid__c;
			wrapperObj.currentQuarterHigh = wrapperObj.currentAcctforecast.Rep_Forecast_Q3__c;

			wrapperObj.lastQuarterLow = wrapperObj.prevAcctforecast.Rep_Forecast_Q2_Low__c;
			wrapperObj.lastQuarterMid = wrapperObj.prevAcctforecast.Rep_Forecast_Q2_Mid__c;
			wrapperObj.lastQuarterHigh = wrapperObj.prevAcctforecast.Rep_Forecast_Q2__c;

		} else if(currentQuarter == 4) {

			wrapperObj.currentQuarterLow = wrapperObj.currentAcctforecast.Rep_Forecast_Q4_Low__c;
			wrapperObj.currentQuarterMid = wrapperObj.currentAcctforecast.Rep_Forecast_Q4_Mid__c;
			wrapperObj.currentQuarterHigh = wrapperObj.currentAcctforecast.Rep_Forecast_Q4__c;

			wrapperObj.lastQuarterLow = wrapperObj.prevAcctforecast.Rep_Forecast_Q3_Low__c;
			wrapperObj.lastQuarterMid = wrapperObj.prevAcctforecast.Rep_Forecast_Q3_Mid__c;
			wrapperObj.lastQuarterHigh = wrapperObj.prevAcctforecast.Rep_Forecast_Q3__c;

		}
		return wrapperObj;
	}

	//───────────────────────────────────────────────────────────────────────────┐
	// param		type					
	// 
	// ──────────────────────────────────────────────────────────────────────────
	// Description: 	
	//										
	//───────────────────────────────────────────────────────────────────────────┘
}