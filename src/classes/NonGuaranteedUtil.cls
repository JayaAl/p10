/**
* ─────────────────────────────────────────────────────────────────────────────────────────────────┐
* This class is  util for Non guaranteed tab.
* ──────────────────────────────────────────────────────────────────────────────────────────────────
* @author         Jaya Alaparthi
* @maintainedBy   Jaya Alaparthi
* @version        1.0
* @created        2017-11-15
* @modified       
* @systemLayer    Util
* @see            ????
* @see            ????
* ──────────────────────────────────────────────────────────────────────────────────────────────────
* @changes
* v1.1          Jaya Alaparthi
* YYYY-MM-DD    
* ──────────────────────────────────────────────────────────────────────────────────────────────────
* @changes
* v1.1          Jaya Alaparthi
* YYYY-MM-DD       
* ─────────────────────────────────────────────────────────────────────────────────────────────────┘
*/
public with sharing class NonGuaranteedUtil {

	//───────────────────────────────────────────────────────────────────────────┐
	// param		type					
	// 
	// ──────────────────────────────────────────────────────────────────────────
	// Description: getOpptyRecordType	
	//										
	//───────────────────────────────────────────────────────────────────────────┘
	public static map<String,Id> getOpptyRecordType() {

		list<String> opptyRecordType = new list<String>{'Opportunity_Performance',
												'Opportunity_Programmatic'};
		map<String,Id> opptyRecordTypeMap = new map<String,Id>();
		for(RecordType objRecordType : [SELECT DeveloperName,Id,
        									IsActive,SobjectType 
			        					FROM RecordType 
			        					WHERE SobjectType = 'Opportunity' 
			        					AND IsActive = true 
			        					AND DeveloperName IN : opptyRecordType]) {
			opptyRecordTypeMap.put(objRecordType.DeveloperName,objRecordType.Id);
		}
		return opptyRecordTypeMap;
	}
	//───────────────────────────────────────────────────────────────────────────┐
	// param		type					
	// 
	// ──────────────────────────────────────────────────────────────────────────
	// Description: getOpportunities	
	//										
	//───────────────────────────────────────────────────────────────────────────┘
	/*public static void getOpportunities() {

		for(AggregateResult ar : [
                SELECT SUM(Amount) amt, 
                		FISCAL_QUARTER(ContractStartDate) contractStartDate,
                        Account.Name advertiser,
                        Agency__r.Name agencyName,
                        Owner.Name salesRep, 
                        StageName stageName
                FROM Opportunity 
                WHERE OwnerId =:acctOwnerId
                AND FISCAL_YEAR(ContractStartDate) = quarterStart
                And StageName != 'Closed Lost'
                GROUP BY StageName,Account.Name,Agency__r.Name, 
                            FISCAL_QUARTER(Date__c)
                order by Fiscal_Quarter(Date__c)]) {
	}*/
	//───────────────────────────────────────────────────────────────────────────┐
	// param		type					
	// 
	// ──────────────────────────────────────────────────────────────────────────
	// Description: getRelatedOpportunities	
	//										
	//───────────────────────────────────────────────────────────────────────────┘
	public static list<Opportunity> getRelatedOpportunities(String acctName,
												String acctStatus,
												String acctOwnerId,
												Integer currentQuarter,
												Integer perviousQuarter) {
		
		list<Opportunity> opptyList = new list<Opportunity>();
		Integer currentYear,previousYear = 0;
		String dateCriteria = '';
		System.debug('getRelatedOpportunities:acctOwnerId::'+acctOwnerId
					+' currentQuarter:'+currentQuarter
					+'perviousQuarter:'+perviousQuarter);
		// get oppty record types
		list<Id> recordTypeIdsList = NonGuaranteedUtil.getOpptyRecordType().values();
		if(!recordTypeIdsList.isEmpty()) {

			if(currentQuarter == 1) {

				currentYear = Date.today().year();
				previousYear = Date.today().addYears(-1).year();

				dateCriteria = 
						+' AND ((FISCAL_QUARTER(ContractStartDate__c) =: currentQuarter'
						+'		AND CALENDAR_YEAR(ContractStartDate__c) =: currentYear'
						+'      AND CALENDAR_YEAR(ContractEndDate__c) =: currentYear) OR '
						
						+' (FISCAL_QUARTER(ContractStartDate__c) =: perviousQuarter '
						+'	AND CALENDAR_YEAR(ContractStartDate__c) =: previousYear '
						+'  AND FISCAL_QUARTER(ContractEndDate__c) =: currentQuarter'
						+'  AND CALENDAR_YEAR(ContractEndDate__c) =: currentYear) OR '
						
						+' (FISCAL_QUARTER(ContractEndDate__c) =: currentQuarter '
						+'	AND CALENDAR_YEAR(ContractEndDate__c) =: currentYear) '
						+' ) ';
			} else {

				currentYear = Date.today().year();
				previousYear = 0;
				dateCriteria = 
						+' AND ((FISCAL_QUARTER(ContractStartDate__c) =: currentQuarter'
						+'		AND CALENDAR_YEAR(ContractStartDate__c) =: currentYear'
						+'      AND CALENDAR_YEAR(ContractEndDate__c) =: currentYear) OR '
						
						+' (FISCAL_QUARTER(ContractStartDate__c) =: perviousQuarter '
						+'	AND CALENDAR_YEAR(ContractStartDate__c) =: currentYear '
						+'  AND FISCAL_QUARTER(ContractEndDate__c) =: currentQuarter'
						+'  AND CALENDAR_YEAR(ContractEndDate__c) =: currentYear) OR '
						
						+' (FISCAL_QUARTER(ContractEndDate__c) =: currentQuarter '
						+'	AND CALENDAR_YEAR(ContractEndDate__c) =: currentYear) '
						+' ) ';
			}
			String opptyQuery = 'SELECT Id, AccountId,Account.Name,'
								+' Agency__c,StageName,Agency__r.Name,'
								+' ContractStartDate__c,Primary_Contact__c,'
								+' Account.Owner.Name,Account.OwnerId,'
								+' Account.Status__c,ContractEndDate__c, '
								+' Owner.EmployeeNumber, Amount  '
								+' FROM Opportunity'
								+' WHERE StageName != '+'\'Closed Lost\''
								+' AND Owner.EmployeeNumber != null '
								+' AND RecordTypeId IN: recordTypeIdsList ';

			opptyQuery += dateCriteria;
			if(!String.isBlank(acctName)) {

				opptyQuery += ' AND Account.Name =: acctName ';
			}
			if(!String.isBlank(acctStatus)) {

				opptyQuery += ' AND Account.Status__c =: acctStatus ';
			}
			if(!String.isBlank(acctOwnerId)) {

				opptyQuery += ' AND Account.OwnerId =: acctOwnerId ';
			}

			/*opptyQuery += ' ( FISCAL_QUARTER(ContractStartDate__c) =: currentQuarter OR '
						+' FISCAL_QUARTER(ContractStartDate__c) =: perviousQuarter OR '
						+' FISCAL_QUARTER(ContractEndDate__c) =: currentQuarter OR '
						+' FISCAL_QUARTER(ContractEndDate__c) =: perviousQuarter OR)';*/
			System.debug('opptyQuery:'+opptyQuery);
			opptyList = Database.query(opptyQuery);
		}
		system.debug('Retrived opportunities:'+opptyList);
		return opptyList;
	}//───────────────────────────────────────────────────────────────────────────┐
	// param		type					
	// 
	// ──────────────────────────────────────────────────────────────────────────
	// Description: getQuarter takes month as an input and gives quarter the 
	//							month falls under
	//										
	//───────────────────────────────────────────────────────────────────────────┘
	public static Integer getQuarter(Integer monthInt) {

		Integer quarter = 0;
		if(monthInt != null && monthInt != 0 && monthInt <= 12) {

			if(monthInt >=1 && monthInt <=3) {

				quarter = 1;
			}
			if(monthInt >=4 && monthInt <=6) {

				quarter = 2;
			}
			if(monthInt >=7 && monthInt <=9) {

				quarter = 3;
			}
			if(monthInt >=10 && monthInt <=12) {

				quarter = 4;
			}
		} 
		return quarter;
	}
	
	//───────────────────────────────────────────────────────────────────────────┐
	// param		type					
	// 
	// ──────────────────────────────────────────────────────────────────────────
	// Description: getAccountForecastDetails with low,mid,high,avertiser,agency
	//										
	//───────────────────────────────────────────────────────────────────────────┘
	public static map<String,Account_Forecast_Details__c> getAccountForecastDetails(
												set<String> advertiserAgencyList) {

		map<String,Account_Forecast_Details__c> retrivedForecastDetails = 
						new map<String,Account_Forecast_Details__c>();
		
		System.debug('advertiserAgencyList:'+advertiserAgencyList);
		if(!advertiserAgencyList.isEmpty()) {

			String externalKey = '%';
			String acctForecastDetailsQuery = 'SELECT Id, Rep_Forecast_Q1_Low__c,'
											+'Rep_Forecast_Q1_Mid__c,Rep_Forecast_Q1__c,'
											+'Rep_Forecast_Q2_Low__c,Rep_Forecast_Q2_Mid__c,'
											+'Rep_Forecast_Q2__c,Rep_Forecast_Q3_Low__c,'
											+'Rep_Forecast_Q3_Mid__c,Rep_Forecast_Q3__c,'
											+'Rep_Forecast_Q4_Low__c,Rep_Forecast_Q4_Mid__c,'
											+'Rep_Forecast_Q4__c,Year__c,External_ID_Year__c'
											+' FROM Account_Forecast_Details__c'
											+' WHERE External_ID_Year__c =: advertiserAgencyList';
			
											//+' AND Account_Forecast__r.External_ID__c LIKE 'appl%'';
			for(Account_Forecast_Details__c forecastDetail: Database.query(acctForecastDetailsQuery)) {

				retrivedForecastDetails.put(forecastDetail.External_ID_Year__c,forecastDetail);

			}

		}
		system.debug('Retrived retrivedForecastDetails:'+retrivedForecastDetails);
		return retrivedForecastDetails;
	}
	//───────────────────────────────────────────────────────────────────────────┐
	// param			type					
	// accountOwnerId	String
	// ──────────────────────────────────────────────────────────────────────────
	// Description: getEmployeeId based on agentId[accountOwnerId]
	//										
	//───────────────────────────────────────────────────────────────────────────┘
	public static String getEmployeeId(String acctOwnerId) {

		User userRecord = new User();
		userRecord = [SELECT Id,EmployeeNumber 
						FROM User 
						WHERE Id =: acctOwnerId];
		return userRecord.EmployeeNumber;
	}
	//───────────────────────────────────────────────────────────────────────────┐
	// param		type					
	// 
	// ──────────────────────────────────────────────────────────────────────────
	// Description: getOpportunityDetails	get all required fields from 
	//										Opportunity for provided OpptyIds
	//───────────────────────────────────────────────────────────────────────────┘
}