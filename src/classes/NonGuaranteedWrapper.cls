/**
* ─────────────────────────────────────────────────────────────────────────────────────────────────┐
* This class is  helper for Non guaranteed tab.
* ──────────────────────────────────────────────────────────────────────────────────────────────────
* @author         Jaya Alaparthi
* @maintainedBy   Jaya Alaparthi
* @version        1.0
* @created        2017-11-15
* @modified       
* @systemLayer    Helper
* @see            NonGuaranteedUtil.cls
* @see            ????
* ──────────────────────────────────────────────────────────────────────────────────────────────────
* @changes
* v1.1          Jaya Alaparthi
* YYYY-MM-DD    
* ──────────────────────────────────────────────────────────────────────────────────────────────────
* @changes
* v1.1          Jaya Alaparthi
* YYYY-MM-DD       
* ─────────────────────────────────────────────────────────────────────────────────────────────────┘
*/
public with sharing class NonGuaranteedWrapper {
	
	//───────────────────────────────────────────────────────────────────────────┐
	// param		type					
	// 
	// ──────────────────────────────────────────────────────────────────────────
	// Description: getNonguaranteedList	
	//										
	//───────────────────────────────────────────────────────────────────────────┘
	@AuraEnabled
	public String advertiser {get;set;}
	@AuraEnabled
	public String agency {get;set;}
	@AuraEnabled
	public Double currentQuarterLow {get;set;}
	@AuraEnabled
	public Double currentQuarterMid {get;set;}
	@AuraEnabled
	public Double currentQuarterHigh {get;set;}
	@AuraEnabled
	public Double lastQuarterLow {get;set;}
	@AuraEnabled
	public Double lastQuarterMid {get;set;}
	@AuraEnabled
	public Double lastQuarterHigh {get;set;}
	@AuraEnabled
	public Double currentQuarterBooked {get;set;}
	@AuraEnabled
	public Double currentQuarterPipeline {get;set;}
	@AuraEnabled
	public Account_Forecast_Details__c currentAcctforecast {get;set;}
	@AuraEnabled
	public Account_Forecast_Details__c prevAcctforecast {get;set;}

	public NonGuaranteedWrapper(){}
	public NonGuaranteedWrapper(String advertiser, String agency, Double currentQuarterLow,
								Double currentQuarterMid, Double currentQuarterHigh,
								Double lastQuarterLow, Double lastQuarterMid,
								Double lastQuarterHigh, Double currentQuarterBooked,
								Double currentQuarterPipeline) { 
								/*Account_Forecast_Details__c currentAcctforecast,
								Account_Forecast_Details__c prevAcctforecast) {*/

		advertiser= advertiser;
		agency = agency;
		currentQuarterLow = currentQuarterLow;
		currentQuarterMid = currentQuarterMid;
		currentQuarterHigh = currentQuarterHigh;
		lastQuarterLow = lastQuarterLow;
		lastQuarterMid = lastQuarterMid;
		lastQuarterHigh = lastQuarterHigh;
		currentQuarterBooked = currentQuarterBooked;
		currentQuarterPipeline = currentQuarterPipeline;
		currentAcctforecast = currentAcctforecast;
		prevAcctforecast = prevAcctforecast;
	}
}