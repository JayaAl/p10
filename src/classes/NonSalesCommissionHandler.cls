public with sharing class NonSalesCommissionHandler {
    @future
    public static void EnableRecordAcessAtFuture(set<Id> nonSalesCommissionRecordIds)//(list<Non_Sales_Commissions__c> newList)//
    {
       list<Non_Sales_Commissions__c> newList = [select id,name,OwnerId,Date__c,Employee__c,Manager_Level_01__c,Manager_Level_02__c,Manager_Level_03__c,Manager_Level_04__c from Non_Sales_Commissions__c where id in:nonSalesCommissionRecordIds];
        
       // Create a new list of sharing objects for Job
        List<Non_Sales_Commissions__Share> NonSalesCommissionRecordShrs  = new List<Non_Sales_Commissions__Share>();
        system.debug(logginglevel.info,'INSIDE HERE');
        // Declare variables for recruiting and hiring manager sharing
        
        //Non_Sales_Commissions__Share hmShr;

        for(Non_Sales_Commissions__c job : newlist){
        system.debug(logginglevel.info,'INSIDE HERE job' + job);
            //SHARE WITH MANAGER 1
            //=================================================================================
            // Instantiate the sharing objects 
            if(job.Manager_Level_01__c!=null)
            {
                system.debug(logginglevel.info,'INSIDE HERE job.Manager_Level_01__c' + job.Manager_Level_01__c);
                Non_Sales_Commissions__Share RvpShr1 = new Non_Sales_Commissions__Share();  
                
                // Set the ID of record being shared
                RvpShr1.ParentId = job.Id;
                
                // Set the ID of user or group being granted access
                RvpShr1.UserOrGroupId = job.Manager_Level_01__c;
                
                // Set the access level
                RvpShr1.AccessLevel = 'Read';
                
                // Set the Apex sharing reason for hiring manager and recruiter
                RvpShr1.RowCause = Schema.Non_Sales_Commissions__Share.RowCause.Manager1__c;
                
                
                
                // Add objects to list for insert
                NonSalesCommissionRecordShrs.add(RVPShr1);
                
                system.debug(logginglevel.info,'INSIDE HERE .NonSalesCommissionRecordShrs' + NonSalesCommissionRecordShrs);
                
            }
            //=================================================================================
            
            //SHARE WITH MANAGER 2
            //=================================================================================
            // Instantiate the sharing objects 
            if(job.Manager_Level_02__c!=null)
            {
                system.debug(logginglevel.info,'INSIDE HERE job.Manager_Level_02__c' + job.Manager_Level_02__c);
                Non_Sales_Commissions__Share RvpShr2 = new Non_Sales_Commissions__Share();  
                
                // Set the ID of record being shared
                RvpShr2.ParentId = job.Id;
                
                // Set the ID of user or group being granted access
                RvpShr2.UserOrGroupId = job.Manager_Level_02__c;
                
                // Set the access level
                RvpShr2.AccessLevel = 'Read';
                
                // Set the Apex sharing reason for hiring manager and recruiter
                RvpShr2.RowCause = Schema.Non_Sales_Commissions__Share.RowCause.Manager2__c;
                
                // Add objects to list for insert
                NonSalesCommissionRecordShrs.add(RVPShr2);
                system.debug(logginglevel.info,'INSIDE HERE .NonSalesCommissionRecordShrs' + NonSalesCommissionRecordShrs);
                
            }
            //=================================================================================
            
            //SHARE WITH MANAGER 3
            //=================================================================================
            // Instantiate the sharing objects 
            if(job.Manager_Level_03__c!=null)
            {
                system.debug(logginglevel.info,'INSIDE HERE job.Manager_Level_03__c' + job.Manager_Level_03__c);
                Non_Sales_Commissions__Share RvpShr3 = new Non_Sales_Commissions__Share();  
                
                // Set the ID of record being shared
                RvpShr3.ParentId = job.Id;
                
                // Set the ID of user or group being granted access
                RvpShr3.UserOrGroupId = job.Manager_Level_03__c;
                
                // Set the access level
                RvpShr3.AccessLevel = 'Read';
                
                // Set the Apex sharing reason for hiring manager and recruiter
                RvpShr3.RowCause = Schema.Non_Sales_Commissions__Share.RowCause.Manager3__c;
                
                // Add objects to list for insert
                NonSalesCommissionRecordShrs.add(RVPShr3);
                system.debug(logginglevel.info,'INSIDE HERE .NonSalesCommissionRecordShrs' + NonSalesCommissionRecordShrs);
            }
            //=================================================================================
            
            //SHARE WITH MANAGER 4
            //=================================================================================
            // Instantiate the sharing objects 
            if(job.Manager_Level_04__c!=null)
            {
                system.debug(logginglevel.info,'INSIDE HERE job.Manager_Level_04__c' + job.Manager_Level_04__c);
                Non_Sales_Commissions__Share RvpShr4 = new Non_Sales_Commissions__Share();  
                
                // Set the ID of record being shared
                RvpShr4.ParentId = job.Id;
                
                // Set the ID of user or group being granted access
                RvpShr4.UserOrGroupId = job.Manager_Level_04__c;
                
                // Set the access level
                RvpShr4.AccessLevel = 'Read';
                
                // Set the Apex sharing reason for hiring manager and recruiter
                RvpShr4.RowCause = Schema.Non_Sales_Commissions__Share.RowCause.Manager4__c;
                
                // Add objects to list for insert
                NonSalesCommissionRecordShrs.add(RVPShr4);
                system.debug(logginglevel.info,'INSIDE HERE .NonSalesCommissionRecordShrs' + NonSalesCommissionRecordShrs);
            }
        
        }
        // Insert sharing records and capture save result 
        // The false parameter allows for partial processing if multiple records are passed 
        // into the operation 
        if(NonSalesCommissionRecordShrs.size()>0){
            Database.SaveResult[] lsr = Database.insert(NonSalesCommissionRecordShrs,false); 
            
            // Create counter
            Integer i=0;
            
            // Process the save results
            for(Database.SaveResult sr : lsr){
                system.debug(logginglevel.info,'INSIDE HERE sr.isSuccess()' + sr.isSuccess());
                if(!sr.isSuccess()){
                    // Get the first save result error
                    Database.Error err = sr.getErrors()[0];
                    
                    system.debug(logginglevel.info,'INSIDE HERE sr.getErrors()[0]' + sr.getErrors()[0]);
                    
                    // Check if the error is related to a trivial access level
                    // Access levels equal or more permissive than the object's default 
                    // access level are not allowed. 
                    // These sharing records are not required and thus an insert exception is 
                    // acceptable. 
                    if(!(err.getStatusCode() == StatusCode.FIELD_FILTER_VALIDATION_EXCEPTION  
                                                   &&  err.getMessage().contains('AccessLevel'))){
                        // Throw an error when the error is not related to trivial access level.
                        new Map<Id,Non_Sales_Commissions__c>(newList)
                        .get(NonSalesCommissionRecordShrs[i].ParentId).
                          addError(
                           'Unable to grant sharing access due to following exception: '
                           + err.getMessage());
                           
                           system.debug(logginglevel.info,'INSIDE HERE err.getMessage()' + err.getMessage());
                    
                    }
                }
                i++;
            }
        }
        
    
    
    
   }


    /*
    public static void EnableRecordAcessTo(list<Non_Sales_Commissions__c> newList)
    {
       
      // Create a new list of sharing objects for Job
        List<Non_Sales_Commissions__Share> NonSalesCommissionRecordShrs  = new List<Non_Sales_Commissions__Share>();
        system.debug(logginglevel.info,'INSIDE HERE');
        // Declare variables for recruiting and hiring manager sharing
        Non_Sales_Commissions__Share RvpShr1;
        Non_Sales_Commissions__Share RvpShr2;
        Non_Sales_Commissions__Share RvpShr3;
        Non_Sales_Commissions__Share RvpShr4;
        //Non_Sales_Commissions__Share hmShr;

        for(Non_Sales_Commissions__c job : newlist){
        system.debug(logginglevel.info,'INSIDE HERE job' + job);
        
        
        
            //SHARE WITH MANAGER 1
            //=================================================================================
            // Instantiate the sharing objects 
            if(job.Manager_Level_01__c!=null)
            {
                system.debug(logginglevel.info,'INSIDE HERE job.Manager_Level_01__c' + job.Manager_Level_01__c);
                RvpShr1 = new Non_Sales_Commissions__Share();  
                
                // Set the ID of record being shared
                RvpShr1.ParentId = job.Id;
                
                // Set the ID of user or group being granted access
                RvpShr1.UserOrGroupId = job.Manager_Level_01__c;
                
                // Set the access level
                RvpShr1.AccessLevel = 'Read';
                
                // Set the Apex sharing reason for hiring manager and recruiter
                RvpShr1.RowCause = Schema.Non_Sales_Commissions__Share.RowCause.Manager1__c;
                
                // Add objects to list for insert
                NonSalesCommissionRecordShrs.add(RVPShr1);
                
                system.debug(logginglevel.info,'INSIDE HERE .NonSalesCommissionRecordShrs' + NonSalesCommissionRecordShrs);
                
            }
            //=================================================================================
            
            //SHARE WITH MANAGER 2
            //=================================================================================
            // Instantiate the sharing objects 
            if(job.Manager_Level_02__c!=null)
            {
                system.debug(logginglevel.info,'INSIDE HERE job.Manager_Level_02__c' + job.Manager_Level_02__c);
                RvpShr2 = new Non_Sales_Commissions__Share();  
                
                // Set the ID of record being shared
                RvpShr2.ParentId = job.Id;
                
                // Set the ID of user or group being granted access
                RvpShr2.UserOrGroupId = job.Manager_Level_02__c;
                
                // Set the access level
                RvpShr2.AccessLevel = 'Read';
                
                // Set the Apex sharing reason for hiring manager and recruiter
                RvpShr2.RowCause = Schema.Non_Sales_Commissions__Share.RowCause.Manager2__c;
                
                // Add objects to list for insert
                NonSalesCommissionRecordShrs.add(RVPShr2);
                system.debug(logginglevel.info,'INSIDE HERE .NonSalesCommissionRecordShrs' + NonSalesCommissionRecordShrs);
                
            }
            //=================================================================================
            
            //SHARE WITH MANAGER 3
            //=================================================================================
            // Instantiate the sharing objects 
            if(job.Manager_Level_03__c!=null)
            {
                system.debug(logginglevel.info,'INSIDE HERE job.Manager_Level_03__c' + job.Manager_Level_03__c);
                RvpShr3 = new Non_Sales_Commissions__Share();  
                
                // Set the ID of record being shared
                RvpShr3.ParentId = job.Id;
                
                // Set the ID of user or group being granted access
                RvpShr3.UserOrGroupId = job.Manager_Level_03__c;
                
                // Set the access level
                RvpShr3.AccessLevel = 'Read';
                
                // Set the Apex sharing reason for hiring manager and recruiter
                RvpShr3.RowCause = Schema.Non_Sales_Commissions__Share.RowCause.Manager3__c;
                
                // Add objects to list for insert
                NonSalesCommissionRecordShrs.add(RVPShr3);
                system.debug(logginglevel.info,'INSIDE HERE .NonSalesCommissionRecordShrs' + NonSalesCommissionRecordShrs);
            }
            //=================================================================================
            
            //SHARE WITH MANAGER 4
            //=================================================================================
            // Instantiate the sharing objects 
            if(job.Manager_Level_04__c!=null)
            {
                system.debug(logginglevel.info,'INSIDE HERE job.Manager_Level_04__c' + job.Manager_Level_04__c);
                RvpShr4 = new Non_Sales_Commissions__Share();  
                
                // Set the ID of record being shared
                RvpShr4.ParentId = job.Id;
                
                // Set the ID of user or group being granted access
                RvpShr4.UserOrGroupId = job.Manager_Level_04__c;
                
                // Set the access level
                RvpShr4.AccessLevel = 'Read';
                
                // Set the Apex sharing reason for hiring manager and recruiter
                RvpShr4.RowCause = Schema.Non_Sales_Commissions__Share.RowCause.Manager4__c;
                
                // Add objects to list for insert
                NonSalesCommissionRecordShrs.add(RVPShr4);
                system.debug(logginglevel.info,'INSIDE HERE .NonSalesCommissionRecordShrs' + NonSalesCommissionRecordShrs);
            }
        // Insert sharing records and capture save result 
        // The false parameter allows for partial processing if multiple records are passed 
        // into the operation 
        Database.SaveResult[] lsr = Database.insert(NonSalesCommissionRecordShrs,false); 
        
        // Create counter
        Integer i=0;
        
        // Process the save results
        for(Database.SaveResult sr : lsr){
            system.debug(logginglevel.info,'INSIDE HERE sr.isSuccess()' + sr.isSuccess());
            if(!sr.isSuccess()){
                // Get the first save result error
                Database.Error err = sr.getErrors()[0];
                
                system.debug(logginglevel.info,'INSIDE HERE sr.getErrors()[0]' + sr.getErrors()[0]);
                
                // Check if the error is related to a trivial access level
                // Access levels equal or more permissive than the object's default 
                // access level are not allowed. 
                // These sharing records are not required and thus an insert exception is 
                // acceptable. 
                if(!(err.getStatusCode() == StatusCode.FIELD_FILTER_VALIDATION_EXCEPTION  
                                               &&  err.getMessage().contains('AccessLevel'))){
                    // Throw an error when the error is not related to trivial access level.
                    new Map<Id,Non_Sales_Commissions__c>(newList)
                    .get(NonSalesCommissionRecordShrs[i].ParentId).
                      addError(
                       'Unable to grant sharing access due to following exception: '
                       + err.getMessage());
                       
                       system.debug(logginglevel.info,'INSIDE HERE err.getMessage()' + err.getMessage());
                
                }
            }
            i++;
        }
    }
   }





    public static void EnableRecordAcessTo( Id ManagerId, list<Non_Sales_Commissions__c> newList)
    {
       
      // Create a new list of sharing objects for Job
        List<Non_Sales_Commissions__Share> NonSalesCommissionRecordShrs  = new List<Non_Sales_Commissions__Share>();
        
        // Declare variables for recruiting and hiring manager sharing
        Non_Sales_Commissions__Share RvpShr;
        //Non_Sales_Commissions__Share hmShr;

        for(Non_Sales_Commissions__c job : newlist){
            // Instantiate the sharing objects 
            RvpShr = new Non_Sales_Commissions__Share();  
            //hmShr = new Non_Sales_Commissions__Share();
            
            // Set the ID of record being shared
            RvpShr.ParentId = job.Id;
            //hmShr.ParentId = job.Id;
            
            // Set the ID of user or group being granted access
            RvpShr.UserOrGroupId = ManagerId;
            //hmShr.UserOrGroupId = job.Hiring_Manager__c;
            
            // Set the access level
            RvpShr.AccessLevel = 'edit';
            //hmShr.AccessLevel = 'read';
            
            // Set the Apex sharing reason for hiring manager and recruiter
            RvpShr.RowCause = Schema.Non_Sales_Commissions__Share.RowCause.Manager__c; 
           // hmShr.RowCause = Schema.Non_Sales_Commissions__Share.RowCause.Hiring_Manager__c;
            
            // Add objects to list for insert
            NonSalesCommissionRecordShrs.add(RVPShr);
            //jobShrs.add(hmShr);
            // Insert sharing records and capture save result 
        // The false parameter allows for partial processing if multiple records are passed 
        // into the operation 
        Database.SaveResult[] lsr = Database.insert(NonSalesCommissionRecordShrs,false); 
        
        // Create counter
        Integer i=0;
        
        // Process the save results
        for(Database.SaveResult sr : lsr){
            if(!sr.isSuccess()){
                // Get the first save result error
                Database.Error err = sr.getErrors()[0];
                
                // Check if the error is related to a trivial access level
                // Access levels equal or more permissive than the object's default 
                // access level are not allowed. 
                // These sharing records are not required and thus an insert exception is 
                // acceptable. 
                if(!(err.getStatusCode() == StatusCode.FIELD_FILTER_VALIDATION_EXCEPTION  
                                               &&  err.getMessage().contains('AccessLevel'))){
                    // Throw an error when the error is not related to trivial access level.
                    new Map<Id,Non_Sales_Commissions__c>(newList)
                    .get(NonSalesCommissionRecordShrs[i].ParentId).
                      addError(
                       'Unable to grant sharing access due to following exception: '
                       + err.getMessage());
                }
            }
            i++;
        }
    }
   }*/
   
    public NonSalesCommissionHandler() {
        
    }
}