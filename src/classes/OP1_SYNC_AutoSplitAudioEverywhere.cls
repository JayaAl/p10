/*
Developer: Ralph Callaway <ralph@callawaycloudconsulting.com>
Description:
    Ensure that Audio Everywhere packages created in Operative 
    are correctly forecasted in Salesforce.
Configuration:
    Custom labels with category O1_SYNC are used to control
    - identification of operative sync user 
    - identification of web/mobile/audio everywhere line itesm
    - split of audio everywhere between web/mobile products
Assumptions:
    1) We can use the creation of a revenue schedule as the triggering
       mechanism for splitting the product 
Risks:
    1) If the method Operative One uses to sync back to Salesforce changes
    the assumptions made by this class could be violated
    2) If any of the specified products are not revenue enabled this code
    will break
    3) If any of the products are inactive the code will break
    4) If any of the products aren't included in the oppty pricebook the
    code will break
How Operative Sync Occurs:
    For orders using the media plan as the source for the revenue schedule
    the sync occurs in the following steps:
    1) All line items are deleted
    2) For each forecast category in Operative
        a) A new line item is created
        b) Revenue schedule is created
    3) Opportunity is updated
How Split Works:
    We monitor before update on opportunity line items for:
    1) updates by sync user
    2) for audio everywhere line items
    3) that has just created a revenue schedule for the line item
    When this occurs we:
    1) create two clones of the audio everywhere product on for web and mobile products
    2) change the pricebook entry of the clones to the mobile/web product
    3) mark the new clones as audio everywhere splits (so we can display them differently in add products ui)
    4) clone the revenue schedule for the audio everywhere product and split it for web and mobile
    line items
    5) delete the audio everywhere line items
*/
public class OP1_SYNC_AutoSplitAudioEverywhere {


//VG - 04/02/2013 - Code Deprecated as Operative One is currently not in use.

/****

     // Constants, Properties, and Variables //
    
    // Configuration Constants (left non-final so test methods can alter override the custom label values)  
    private static String AUDIO_EVERYWHERE_PRODUCT_ID = Label.OP1_SYNC_AudioEverywhereProductId;
    private static Id MOBILE_PRODUCT_ID = Label.OP1_SYNC_MobileProductId;
    private static Decimal MOBILE_SPLIT_PERCENTAGE = Decimal.valueOf(Label.OP1_SYNC_MobileSplitPercentage) / 100; // label of "80" implies 80%
    private static Id SYNC_USER_ID = Label.OP1_SYNC_OperativeSyncUser;
    private static Id WEB_PRODUCT_ID = Label.OP1_SYNC_WebProductId;
    private static Decimal WEB_SPLIT_PERCENTAGE = Decimal.valueOf(Label.OP1_SYNC_WebSplitPercentage) / 100; // label of "80" implies 80%
    
    // Trigger variables
    private List<OpportunityLineItem> newList;
    private Map<Id, OpportunityLineItem> oldMap;
    
    //  Memoized Properties //
    
    private static Set<Id> audioEverywherePbeIds {
        get {
            return audioEverywherePBMap.keySet();
        }
    }
    
    private static Map<Id, Id> audioEverywherePBMap {
        get {
            if(audioEverywherePBMap == null) {
                initMaps();
            }
            return audioEverywherePBMap;
        }   
        set;
    }
    
    private static Map<String, Schema.SObjectField> lineItemFieldMap {
        get {
            if(lineItemFieldMap == null) {
                lineItemFieldMap = Schema.SObjectType.OpportunityLineItem.fields.getMap();
            }
            return lineItemFieldmap;
        }
        set;
    }
    
    private static Map<Id, Id> mobilePBEMap {
        get {
            if(mobilePBEMap == null) {
                initMaps();
            }
            return mobilePBEMap;
        }   
        set;
    }
    
    private static Map<String, Schema.SObjectField> scheduleFieldMap {
        get {
            if(scheduleFieldMap == null) {
                scheduleFieldMap = Schema.SObjectType.OpportunityLineItemSchedule.fields.getMap();
            }
            return scheduleFieldMap;
        }
        set;
    }
    
    private static Map<Id, Id> webPBEMap {
        get {
            if(webPBEMap == null) {
                initMaps();
            }
            return webPBEMap;
        }   
        set;
    }
    
    //  Constructor  // 
    
    public OP1_SYNC_AutoSplitAudioEverywhere(List<OpportunityLineItem> newList, Map<Id, OpportunityLineItem> oldMap) {
        this.newList = newList;
        this.oldMap = oldMap;
    }
    
     //  Action Functions  //   
    
    // NB: By design this should be called from a befort update trigger only
    public void doSplit() {
        
        // Only run when line items created/updated by operative sync user
        if(UserInfo.getUserId() == SYNC_USER_ID) {
            
            // identify line items that need to be split and mark them as audio everyhere parents
            List<OpportunityLineItem> audioEverywhereLineItems = new List<OpportunityLineItem>();
            for(OpportunityLineItem newLineItem : newList) {
                OpportunityLineItem oldLineItem = oldMap.get(newLineItem.id);
                // is this an audio everywhere line item?
                if(splitNeeded(newLineItem, oldLineItem)) {
                    audioEverywhereLineItems.add(newLineItem);
                }
            }
            
            // bail if we don't have an splits to create
            if(!audioEverywhereLineItems.isEmpty()) {
            
                // create split line items
                List<OpportunityLineItem> splitLineItems = new List<OpportunityLineItem>();
                Map<Id, OpportunityLineItem> mobileSplitMap = new Map<Id, OpportunityLineItem>();
                Map<Id, OpportunityLineItem> webSplitMap = new Map<Id, OpportunityLineItem>();
                for(OpportunityLineItem lineItem : audioEverywhereLineItems) {
                    // determine pricebook id
                    Id pricebookId = audioEverywherePBMap.get(lineItem.pricebookEntryId);
                
                    // bail if I don't have an active mobile and web product
                    if(mobilePBEMap.containsKey(pricebookId) && webPBEMap.containsKey(pricebookId)) {
                        // create web and mobile splits
                        OpportunityLineItem mobileSplit = splitLineItem(lineItem, mobilePBEMap.get(pricebookId), MOBILE_SPLIT_PERCENTAGE);
                        OpportunityLineItem webSplit = splitLineItem(lineItem, webPBEMap.get(pricebookId), WEB_SPLIT_PERCENTAGE);
                        
                        // map them back to audio everywhere parent
                        mobileSplitMap.put(lineItem.id, mobileSplit);
                        webSplitMap.put(lineItem.id, webSplit);
                        
                        // gather them for insert
                        splitLineItems.add(mobileSplit);
                        splitLineItems.add(webSplit);
                    }
                }
                insert splitLineItems;
                
                // query revenue schedule for audio everywhere line items
                Map<Id, List<OpportunityLineItemSchedule>> scheduleMap = getScheduleMap(audioEverywhereLineItems);
                
                // create split revenue schedules and clear audio everywhere schedules
                List<OpportunityLineItemSchedule> splitSchedules = new List<OpportunityLineItemSchedule>();
                for(Id audioEverywhereLineItemId : scheduleMap.keySet()) {
                    for(OpportunityLineItemSchedule schedule : scheduleMap.get(audioEverywhereLineItemId)) {
                        splitSchedules.add(splitSchedule(
                              schedule
                            , mobileSplitMap.get(audioEverywhereLineItemId).id // id of mobile line item
                            , MOBILE_SPLIT_PERCENTAGE
                        ));
                        splitSchedules.add(splitSchedule(
                              schedule
                            , webSplitMap.get(audioEverywhereLineItemId).id // id of mobile line item
                            , WEB_SPLIT_PERCENTAGE
                        ));
                    }   
                }
                insert splitSchedules;
                
                // Delete audio everywhere line item asynchronously.  Can't delete it synchronously since
                // it's in the process of being updated
                asyncAudioEverywhereDelete(scheduleMap.keySet());
            }
        }
    }
    
    @future
    public static void asyncAudioEverywhereDelete(Set<Id> lineItemIds) {
        delete [select id from OpportunityLineItem where id in :lineItemIds];
    } 
    
     // Support Methods // 
    
    private static Map<Id, List<OpportunityLineItemSchedule>> getScheduleMap(List<OpportunityLineItem> lineItems) {
        Map<Id, List<OpportunityLineItemSchedule>> scheduleMap = new Map<Id, List<OpportunityLineItemSchedule>>();
        for(OpportunityLineItemSchedule schedule : [
            select
                  currencyIsoCode
                , description
                , opportunityLineItemId
                , quantity
                , revenue
                , scheduleDate
                , type
            from OpportunityLineItemSchedule
            where opportunityLineItemId in :lineItems
        ]) {
            if(!scheduleMap.containsKey(schedule.opportunityLineItemId))
                scheduleMap.put(schedule.opportunityLineItemId, new List<OpportunityLineItemSchedule>());
            scheduleMap.get(schedule.opportunityLineItemId).add(schedule);
        }
        return scheduleMap;
    }
    
    private static void initMaps() {
        // Query pricebook entries data
        // 1. Gather mobile pricebook entry ids by pricebook
        // 2. Gather web pricebook entry ids by pricebook
        // 3. Gather pricebooks for audio everywhere ids by pb entry id 
        mobilePBEMap = new Map<Id, Id>();
        webPBEMap = new Map<Id, Id>();
        audioEverywherePBMap = new Map<Id, Id>();
        for(PricebookEntry pbe : [
            select pricebook2Id, product2Id
            from PricebookEntry
            where isActive = true 
            and (
                product2Id = :MOBILE_PRODUCT_ID
             or product2Id = :WEB_PRODUCT_ID
             or product2Id = :AUDIO_EVERYWHERE_PRODUCT_ID
            )
        ]) {
            if(pbe.product2Id == MOBILE_PRODUCT_ID) {
                mobilePBEMap.put(pbe.pricebook2Id, pbe.id);
            } else if(pbe.product2Id == WEB_PRODUCT_ID) {
                webPBEMap.put(pbe.pricebook2Id, pbe.id);
            } else if(pbe.product2Id == AUDIO_EVERYWHERE_PRODUCT_ID) {
                audioEverywherePBMap.put(pbe.id, pbe.pricebook2Id);
            }
        }
    }
    
    // Switches up the pricebook entry, marks a "Split" product, and splits the price
    // Work around usual OpportunityLineItem funkiness by cloning manually.  Can't user input.clone(false, true)
    // because sfdc runtime treats it as a committed sObject and complains that either pricebookEntryId cannot be
    // update, or that you can' set both unitPrice and totalPrice (even if you null out one of them)
    private static OpportunityLineItem splitLineItem(OpportunityLineItem input, Id newPBEId, Decimal splitPercentage) {
        OpportunityLineItem output = new OpportunityLineItem();
        for(String fieldName : lineItemFieldMap.keySet()) {
            Schema.SObjectField fieldToken = lineItemFieldMap.get(fieldName);
            Schema.DescribeFieldResult fieldDescribe = fieldToken.getDescribe();
            if(
                !fieldName.equalsIgnoreCase('totalPrice')
             && !fieldName.equalsIgnoreCase('unitPrice')
             && !fieldName.equalsIgnoreCase('pricebookEntryId')
             && fieldDescribe.isCreateable()
             && input.get(fieldName) != null
            ) {
                output.put(fieldName, input.get(fieldName));
            }
        }
        output.Audio_Everywhere_Split_Product__c = true;
        output.unitPrice = input.totalPrice * splitPercentage / input.quantity;
        output.pricebookEntryId = newPBEId;
        return output;
    }

    // returns true when line item is audio everywhere line item, hasn't yet been marked as a parent (meaning
    // we haven't split it yet), and it just had a revenue schedule added
    private static Boolean splitNeeded(OpportunityLineItem newLineItem, OpportunityLineItem oldLineItem) {
        return audioEverywherePbeIds.contains(newLineItem.pricebookEntryId)
            && newLineItem.hasRevenueSchedule
            && !oldLineItem.hasRevenueSchedule;
    }
    
    private static OpportunityLineItemSchedule splitSchedule(OpportunityLineItemSchedule input, Id newLineItemId, Decimal splitPercentage) {
        OpportunityLineItemSchedule output = new OpportunityLineItemSchedule();
        for(String fieldName : scheduleFieldMap.keySet()) {
            Schema.SObjectField fieldToken = scheduleFieldMap.get(fieldName);
            Schema.DescribeFieldResult fieldDescribe = fieldToken.getDescribe();
            if(
                !fieldName.equalsIgnoreCase('opportunityLineItemId')
             && fieldDescribe.isCreateable()
             && input.get(fieldName) != null
            ) {
                output.put(fieldName, input.get(fieldName));
            }
        }
        output.opportunityLineItemId = newLineItemId;
        output.Revenue = input.revenue * splitPercentage;
        return output;
    }

     //  Test Hooks  // 
    
    public static void setAudioEverywhereProductId(Id productId) {
        system.assert(Test.isRunningTest(), 'setAudioEverywhereProductId can only be called from test methods');
        AUDIO_EVERYWHERE_PRODUCT_ID = productId;
    }
    
    public static void setMobileProductId(Id productId) {
        system.assert(Test.isRunningTest(), 'setMobileProductId() can only be called from test methods');
        MOBILE_PRODUCT_ID = productId;
    }
    
    public static void setMobileSplitPercentage(Decimal percentage) {
        system.assert(Test.isRunningTest(), 'setMobileSplitPercentage() can only be called from test methods');
        MOBILE_SPLIT_PERCENTAGE = percentage;
    }
    
    public static void setSyncUserId(Id userId) {
        system.assert(Test.isRunningTest(), 'setSyncUserId() can only be called from test methods');
        SYNC_USER_ID = userId;
    }

    public static void setWebProductId(Id productId) {
        system.assert(Test.isRunningTest(), 'setWebProductId() can only be called from test methods');
        WEB_PRODUCT_ID = productId;
    }
    
    public static void setWebSplitPercentage(Decimal percentage) {
        system.assert(Test.isRunningTest(), 'setWebSplitPercentage() can only be called from test methods');
        WEB_SPLIT_PERCENTAGE = percentage;
    }
    
  ****/  
    
}