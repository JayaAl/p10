/*
Developer: Ralph Callaway <ralph@callawaycloudconsulting.com>
Description:
    Test methods for OP1_SYNC_AutoSplitAudioEverywhere.cls
*/

//VG - 04/02/2013 - Code Deprecated as Operative One is currently not in use.

@isTest(seeAllData=true) // required for product creation
private class OP1_SYNC_AutoSplitAudioEverywhere_Test {

  
 /****
  
  
    // Test Variables //
    
        
    static PricebookEntry audioEverywherePBE;
    static PricebookEntry mobilePBE;
    static PricebookEntry webPBE;
    static Opportunity testOpportunity;
    static User syncUser;
    static OpportunityLineItem audioEverywhereLineItem;
    static Decimal mobileSplit = 0.8;
    static Decimal webSplit = 0.2;
    
   // Before Test Actions //
    
    static {
        setupTest();
    }
    
   // Test Methods //
    
    @isTest
    private static void testAudioEverywhereSplit() {
        system.debug('==>Opportunity: ' + testOpportunity);
        
        // validate initial conditions
        Integer lineItemCnt = [select count() from OpportunityLineItem where opportunityId = :testOpportunity.id];
        system.assertEquals(1, lineItemCnt);
        
        // add revenue schedule to audio everywhere line item as sync user;
        Test.startTest();
        OpportunityLineItemSchedule audioEverywhereSchedule = new OpportunityLineItemSchedule(
              opportunityLineItemId = audioEverywhereLineItem.id
            , revenue = audioEverywhereLineItem.totalPrice
            , scheduleDate = audioEverywhereLineItem.serviceDate
            , type = 'Revenue'
        );
        system.runAs(syncUser) {
            insert audioEverywhereSchedule;
        }
        Test.stopTest();
        
        // validate split results
        Integer audioEverywhereExistenceCheck = [select count() from OpportunityLineItem where id = :audioEverywhereLineItem.id];
        system.assertEquals(0, audioEverywhereExistenceCheck);
        OpportunityLineItem mobileLineItem;
        OpportunityLineItem webLineItem;
        List<OpportunityLineItem> lineItems = [
            select 
                  Audio_Everywhere_Split_Product__c
                , end_date__c
                , pricebookEntryId
                , quantity
                , serviceDate
                , totalPrice
                , (select revenue, scheduleDate, type from OpportunityLineItemSchedules)
            from OpportunityLineItem
            where opportunityid = :testOpportunity.id
        ];
        for(OpportunityLineItem lineItem : lineItems) {
            if(lineItem.pricebookEntryId == mobilePBE.id) {
                mobileLineItem = lineItem;
            } else if(lineItem.pricebookEntryId == webPBE.id) {
                webLineItem = lineItem;
            }
        }
        system.assertNotEquals(null, mobileLineItem);
        system.assertEquals(audioEverywhereLineItem.serviceDate, mobileLineItem.serviceDate);
        system.assertEquals(audioEverywhereLineItem.end_date__c, mobileLineItem.end_date__c);
        system.assertEquals(audioEverywhereLineItem.totalPrice * mobileSplit, mobileLineItem.totalPrice);
        system.assertEquals(audioEverywhereLineItem.quantity, mobileLineItem.quantity);
        system.assertEquals(1, mobileLineItem.opportunityLineItemSchedules.size());
        OpportunityLineItemSchedule mobileSchedule = mobileLineItem.opportunityLineItemSchedules[0];
        system.assertEquals(audioEverywhereSchedule.revenue * mobileSplit, mobileSchedule.revenue);
        system.assertEquals(audioEverywhereSchedule.scheduleDate, mobileSchedule.scheduleDate);
        system.assertEquals(audioEverywhereSchedule.type, mobileSchedule.type);
        system.assertNotEquals(null, webLineItem);
        system.assertEquals(audioEverywhereLineItem.serviceDate, webLineItem.serviceDate);
        system.assertEquals(audioEverywhereLineItem.end_date__c, webLineItem.end_date__c);
        system.assertEquals(audioEverywhereLineItem.totalPrice * webSplit, webLineItem.totalPrice);
        system.assertEquals(audioEverywhereLineItem.quantity, webLineItem.quantity);
        system.assertEquals(1, webLineItem.opportunityLineItemSchedules.size());
        OpportunityLineItemSchedule webSchedule = webLineItem.opportunityLineItemSchedules[0];
        system.assertEquals(audioEverywhereSchedule.revenue * webSplit, webSchedule.revenue);
        system.assertEquals(audioEverywhereSchedule.scheduleDate, webSchedule.scheduleDate);
        system.assertEquals(audioEverywhereSchedule.type, webSchedule.type);
    }
    
    @isTest 
    private static void testAudioEverywhereNotSplitForOtherUsers() {
        // validate initial conditions
        Integer lineItemCnt = [select count() from OpportunityLineItem where opportunityId = :testOpportunity.id];
        system.assertEquals(1, lineItemCnt);
        
        // add revenue schedule to audio everywhere line item not as sync user
        Test.startTest();
        OpportunityLineItemSchedule audioEverywhereSchedule = new OpportunityLineItemSchedule(
              opportunityLineItemId = audioEverywhereLineItem.id
            , revenue = audioEverywhereLineItem.totalPrice
            , scheduleDate = audioEverywhereLineItem.serviceDate
            , type = 'Revenue'
        );
        insert audioEverywhereSchedule;
        Test.stopTest();
        
        // validate line items not split
        lineItemCnt = [select count() from OpportunityLineItem where opportunityId = :testOpportunity.id];
        system.assertEquals(1, lineItemCnt);
    }

    // Test Helpers //
    
    private static void setupTest() {
        // setup pricebook data for mobile, web, and audio everywhere
        Pricebook2 pricebook = UTIL_TestUtil.createPricebook();
        Product2 mobileProduct = UTIL_TestUtil.generateProduct();
        mobileProduct.canUseRevenueSchedule = true;
        Product2 webProduct = mobileProduct.clone(false, true);
        Product2 audioEverywhereProduct = mobileProduct.clone(false, true);
        insert new Product2[] { mobileProduct, webProduct, audioEverywhereProduct };
        for(PricebookEntry pbe : UTIL_TestUtil.createPricebookEntries(
              new Id[] { mobileProduct.id, webProduct.id, audioEverywhereProduct.id }
            , pricebook.id
        )) {
            if(pbe.product2Id == mobileProduct.id) {
                mobilePBE = pbe;
            } else if(pbe.product2Id == webProduct.id) {
                webPBE = pbe;
            } else if(pbe.product2Id == audioEverywhereProduct.id) {
                audioEverywherePBE = pbe;
            }
        }
        
        // create sync user
        syncUser = UTIL_TestUtil.createUser();
        
        // set config values for split
        OP1_SYNC_AutoSplitAudioEverywhere.setAudioEverywhereProductId(audioEverywhereProduct.id);
        OP1_SYNC_AutoSplitAudioEverywhere.setMobileProductId(mobileProduct.id);
        OP1_SYNC_AutoSplitAudioEverywhere.setWebProductId(webProduct.id);
        OP1_SYNC_AutoSplitAudioEverywhere.setMobileSplitPercentage(mobileSplit);
        OP1_SYNC_AutoSplitAudioEverywhere.setWebSplitPercentage(webSplit);
        OP1_SYNC_AutoSplitAudioEverywhere.setSyncUserId(syncUser.id);
        
        // create opportunity and audio everywhere line item
        Account testAccount = UTIL_TestUtil.createAccount();
        //testOpportunity = UTIL_TestUtil.generateOpportunity(testAccount.id);
        
        //updated by VG 12/21/2012
         Contact conObj = UTIL_TestUtil.generateContact(testAccount .id);
        insert conObj;
        
        //updated by VG 12/21/2012
         testOpportunity = UTIL_TestUtil.generateOpportunity(testAccount.id, conObj.id);
         
        
        testOpportunity.pricebook2id = pricebook.id;
        insert testOpportunity;
        audioEverywhereLineItem = UTIL_TestUtil.createOpportunityLineItem(
              testOpportunity.id
            , audioEverywherePBE.id
        );
    }

*****/

}