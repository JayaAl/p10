@isTest//adding test context so that it does not hamper code coverage
global class OPT_BATCH_SALES_INVOICE_EMAIL_Class /*implements  Database.Batchable<sObject>, Database.Stateful*/ {

   /* global final string query;
    global String templateId;
    global Set<Id> ownerIds;
    global OPT_BATCH_SALES_INVOICE_EMAIL_Class(String q)
    {
        query = q;
    }
    
    global Database.QueryLocator start(Database.BatchableContext BC){
        templateId = [select Id from emailtemplate where Name = 'OPT_VF_TEMPLATE_SalesInvoice' limit 1].Id;
        ownerIds = new Set<Id>();
        return Database.getQueryLocator(query);
    }
    
    global void execute(Database.BatchableContext BC, List<sObject> scope){
        List<Messaging.SingleEmailMessage> mails = new List<Messaging.SingleEmailMessage>();
        List<c2g__codaInvoice__c> invoicesToUpdate = new List<c2g__codaInvoice__c>();
        for(Sobject s : scope)  
        {
            
            c2g__codaInvoice__c si = (c2g__codaInvoice__c)s; 
            if(si.c2g__Opportunity__r.Owner.Id <> null && !ownerIds.contains(si.c2g__Opportunity__r.Owner.Id))
            {
                if(si.Times_Notified__c == null)
                si.Times_Notified__c = 0;
                else
                si.Times_Notified__c++;
                
                invoicesToUpdate.add(si);
                //Sending Mail  
                Messaging.SingleEmailMessage mail = new Messaging.SingleEmailMessage() ; 
                mail.templateId = templateId;
                //Set To Address as Owner Id
                mail.setTargetObjectId(si.c2g__Opportunity__r.Owner.Id);
                mail.setSaveAsActivity(false);
                mail.setWhatId(si.Id);
                mail.setOrgWideEmailAddressId('0D24000000000u9');
                mail.setCcAddresses(new List<String>{'commissions@pandora.com'});
                //Sending the email  
                mails.add(mail); 
                
                //Add current Owner Id to set
                ownerIds.add(si.c2g__Opportunity__r.Owner.Id);
            }
        }
        if(ownerIds.size()>0)
        {
            Messaging.sendEmail(mails); 
            update invoicesToUpdate;
        }
    }
    
    
    global void finish(Database.BatchableContext BC){
        // Get the ID of the AsyncApexJob representing this batch job  
        // from Database.BatchableContext.    
        // Query the AsyncApexJob object to retrieve the current job's information.  
        
        AsyncApexJob a = [Select Id, Status, NumberOfErrors, JobItemsProcessed,
        TotalJobItems 
        from AsyncApexJob where Id =:BC.getJobId()];
        User objUser = [Select Email from User where Name = 'David Rogers' limit 1];
        // Send an email to the Apex job's submitter notifying of job completion.  
        Messaging.SingleEmailMessage mail = new Messaging.SingleEmailMessage();
        String[] toAddresses = new String[] {objUser.Email};
        mail.setToAddresses(toAddresses);
        mail.setSubject('Apex Sharing Recalculation ' + a.Status);
        mail.setPlainTextBody('The batch Apex job processed ' + a.TotalJobItems +
        ' batches with '+ a.NumberOfErrors + ' failures.');
        
        Messaging.sendEmail(new Messaging.SingleEmailMessage[] { mail });
    }
    @isTest
    private static void testBatch() {
    // initial setup
    FF_BLNG_TestUtil_InvoiceRollups util = new FF_BLNG_TestUtil_InvoiceRollups();
    util.setup();
        
    // avoid mixed dml from adding running user to test company
    // owner group
    system.runAs(new User(id = UserInfo.getUserId())) {
    // create test data
    Account testAccount = FF_BLNG_TestUtil.createAccount();
    Opportunity testOppty = FF_BLNG_TestUtil.createOpportunity(testAccount.id);
    
    // validate initial conditions
    testOppty = [
        select invoiced_to_date__c
        from Opportunity
        where id = :testOppty.id
    ];
    Double value = testOppty.invoiced_to_date__c == null ? 0 : testOppty.invoiced_to_date__c;
    system.assertEquals(0, value);
    
    // create a line item
    c2g__codaInvoice__c invoice = util.createSalesInvoice(testAccount.id, testOppty.id);
    Test.startTest();
    String q= 'SELECT Billing_Contact__r.Name, '+
                     'Advertiser__r.Name, '+
                     'Name, Id, c2g__Opportunity__r.Owner.Id, '+
                     'Billing_Terms__c, Times_Notified__c, '+
                     'Operative_Order_ID__c,'+
                     'Billing_Contact__r.Id,'+
                     'c2g__Opportunity__r.Name,'+
                     'c2g__OutstandingValue__c '+
              'FROM c2g__codaInvoice__c where c2g__Opportunity__c <> null And (c2g__PaymentStatus__c = \'Unpaid\' OR c2g__PaymentStatus__c= \'Part Paid\') AND CreatedDate = Today Limit 10';

    Database.executeBatch(new OPT_BATCH_SALES_INVOICE_EMAIL_Class(q));
    batchJobs controller = new batchJobs();
    controller.getBatchJobs();
    Test.stopTest();
    }
    
    }
    */    
}