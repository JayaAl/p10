@isTest(seeAllData = true)
public class OPT_CloneOpportunityExtTest {
    static testMethod void runPositiveTestCases1() {
        Account testAccount = UTIL_TestUtil.createAccount();
        Opportunity testOppInside = UTIL_TestUtil.generateOpportunity(testAccount.Id);
        testOppInside.RecordTypeId = Custom_Constants.OPT_SimpleEntryInside;
        testOppInside.ContractStartDate__c = Date.today();
        testOppInside.ContractEndDate__c = Date.today()+2;
        testOppInside.Amount = 1000;
         user adsUser = [Select Id from user limit 1];
        testOppInside.Account_Development_Specialist__c = adsUser.id;
        insert testOppInside;
        
        Test.startTest();
        OPT_CloneOpportunityExt testExt = new OPT_CloneOpportunityExt(new ApexPages.StandardController(testOppInside));
        testExt.objOpp.Name = 'TestingName';
        testExt.objOpp.CloseDate = Date.today();
        testExt.objOpp.StageName = 'Prospecting';
        testExt.save();
        Test.stopTest();
    }
    static testMethod void runPositiveTestCases2() {
        Account acct1 = new Account(name='test Account One1'); 
        insert acct1; 
        //Create Opportunity on Account 
        Opportunity Oppty1 = new Opportunity(AccountId=acct1.Id,name='test Oppty One1',Confirm_direct_relationship__c=true,Industry_Sub_Category__c='Political Campaign Marketing',Type='Advertiser'); 
       user adsUser = [Select Id from user limit 1];
        Oppty1.StageName = 'Qualified'; Oppty1.CloseDate = Date.today(); 
        Oppty1.Account_Development_Specialist__c = adsUser.id;
          Schema.DescribeSObjectResult R;
         Map<String,Schema.RecordTypeInfo> rtMapById;
         R = Opportunity.SObjectType.getDescribe();
        rtMapById = R.getRecordTypeInfosByName();
        Oppty1.RecordTypeId=rtMapById.get('Opportunity - Performance').getRecordtypeId();
        Oppty1.Amount = 200;
        insert Oppty1;
        
        // Create Products 
        Product2 testprod1 = new Product2 (name='test product one1'); 
        testprod1.productcode = 'test pd code1one'; 
        testprod1.CanUseRevenueSchedule = True; 
        testprod1.NumberOfRevenueInstallments = 12; 
        testprod1.RevenueInstallmentPeriod = 'Monthly'; 
        testprod1.RevenueScheduleType = 'Repeat'; 
        insert testprod1; 
        Product2 testprod2 = new Product2 (name='test product two2'); 
        testprod2.productcode = 'test pd code2two'; 
        testprod2.CanUseRevenueSchedule = True; 
        testprod2.NumberOfRevenueInstallments = 12;
        testprod2.RevenueInstallmentPeriod = 'Monthly'; 
        testprod2.RevenueScheduleType = 'Repeat'; 
        insert testprod2; 
        // Ger Pricebook 
        Pricebook2 testpb = [select id from Pricebook2 where IsStandard = true];
        // Add to pricebook 
        PricebookEntry testpbe1 = new PricebookEntry (); 
        testpbe1.pricebook2id = testpb.id; 
        testpbe1.product2id = testprod1.id; 
        testpbe1.IsActive = True; 
        testpbe1.UnitPrice = 250; 
        testpbe1.UseStandardPrice = false; 
        insert testpbe1; 
        PricebookEntry testpbe2 = new PricebookEntry (); 
        testpbe2.pricebook2id = testpb.id; 
        testpbe2.product2id = testprod2.id; 
        testpbe2.IsActive = True; 
        testpbe2.UnitPrice = 250; 
        testpbe2.UseStandardPrice = false; 
        insert testpbe2; 
        
        integer todo = 2;
        List<OpportunityLineItem> bulkoli = new List<OpportunityLineItem>();
        for(integer bi=0; bi<todo; bi++) {
        bulkoli.add( new OpportunityLineItem(Quantity = 1,Duration__c=12, UnitPrice = 1, PriceBookEntryId = testpbe2.id,Impression_s__c=10, OpportunityId = Oppty1.id, End_Date__c=Date.today().addDays(10),ServiceDate = Date.today().addDays(5)) ); 
        } 
        insert bulkoli;
        Test.startTest();
        System.currentPageReference().getParameters().put('cProds','1');
        OPT_CloneOpportunityExt testExt = new OPT_CloneOpportunityExt(new ApexPages.StandardController(Oppty1));
        testExt.objOpp.Name = 'TestingName';
        testExt.objOpp.CloseDate = Date.today();
        testExt.objOpp.StageName = 'Prospecting';
        testExt.save();
        
        testExt.bumpUpCodeCoverage();
        
        
        Test.stopTest();
    }
  
}