/*
Developer: Lakshman (sfdcace@gmail.com)
Description:
    Utility for OPT Clone project.
*/
public class OPT_CloneUtil {
    public static String fetchAllCreatableFields(String objectName, String objectId) {
        String query = 'SELECT';
        Map<String, Schema.SObjectField> objectFields = Schema.getGlobalDescribe().get(objectName).getDescribe().fields.getMap();
     
        // Grab the fields from the describe method and append them to the queryString one by one.
        for (Schema.SObjectField f : objectFields.values()) { 
            Schema.DescribeFieldResult d = f.getDescribe(); 
            if (d.isUpdateable() || (objectName == 'OpportunityLineItem' && d.isCreateable() && !d.isDefaultedOnCreate())) {
                query += ' ' + d.getName() + ',';
            }
        }
        
        // Manually add related object's fields that are needed.
        //query += 'Account.Name,'; // modify as needed
         
        // Strip off the last comma if it exists.
        if (query.subString(query.Length()-1,query.Length()) == ','){
            query = query.subString(0,query.Length()-1);
        }
         
        // Add FROM statement
        query += ' FROM ' + objectName;
         
        // Add on a WHERE/ORDER/LIMIT statement as needed
        if(objectName.equalsIgnoreCase('OpportunityLineItem')) {
            query += ' WHERE OpportunityId = \'' + objectId + '\''; // modify as needed
        }else {
            query += ' WHERE Id = \'' + objectId + '\''; // modify as needed
        }
        return query;
    }
    public static void populateFieldsToBeCloned(sObject sObj, List<Schema.FieldSetMember> listFSM) {
        Set<String> setOfallowedFields = new Set<String>();
        for(Schema.FieldSetMember f : listFSM) {
            setOfallowedFields.add(f.getFieldPath().toLowerCase());
        }
        Map<String, Schema.SObjectField> objectFields = ((Id)sObj.get('id')).getSObjectType().getDescribe().fields.getMap();
        // Grab the fields from the describe method and append them to the queryString one by one.
        for (Schema.SObjectField f : objectFields.values()) { 
            Schema.DescribeFieldResult d = f.getDescribe(); 
            if (d.isUpdateable()) {
                if(! setOfallowedFields.contains(d.getName().toLowerCase())) {
                    if(d.getType() == Schema.DisplayType.Boolean) {
                        sObj.put(d.getName(), false);
                    } else {
                        if(d.getName().equals('CurrencyIsoCode') ) continue; // ESS-31385 Ignore 'CurrencyIsoCode' field as including this was throwing an error
                        sObj.put(d.getName(), null);
                    }
                }
            }
        }
    }
}