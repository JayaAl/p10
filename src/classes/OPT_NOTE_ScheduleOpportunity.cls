global class OPT_NOTE_ScheduleOpportunity implements schedulable {
    global void execute(SchedulableContext sc) {
        /* updated query 2015-04-27 to include only those users still active in the notifications -- Casey Grooms */
        String query = 'Select Id,Name, CloseDate,OwnerId, Owner.FirstName,Owner.Email, ContractStartDate__c,StageName From Opportunity where Owner.isActive = TRUE AND (CloseDate <= TOMORROW OR ContractStartDate__c <= TOMORROW) AND (StageName <> \'Closed Lost\' AND StageName <> \'Closed Won\' AND RecordType.DeveloperName <> \'Opportunity_Biz_Dev\')';
        OPT_NOTE_SendEmailToOwner batchApex = new OPT_NOTE_SendEmailToOwner(query);
        ID batchprocessid = Database.executeBatch(batchApex,10);
    }
}