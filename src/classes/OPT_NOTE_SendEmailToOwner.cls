global class OPT_NOTE_SendEmailToOwner implements  Database.Batchable<sObject>, Database.Stateful {
global final string query;
global OPT_NOTE_SendEmailToOwner(String q)
{
   query = q;
}

global Database.QueryLocator start(Database.BatchableContext BC){
  return Database.getQueryLocator(query);
}

global void execute(Database.BatchableContext BC, List<sObject> scope){
    for(Sobject s : scope)  
    {
        Opportunity o = (Opportunity)s; 
        String body = '';
        //Sending Mail  
        Messaging.SingleEmailMessage mail = new Messaging.SingleEmailMessage() ; 
        
        String[] toAddresses = new String[] {o.Owner.Email} ;
        mail.setToAddresses(toAddresses) ;
        mail.setSenderDisplayName('Steven Turacek');
        mail.setReplyTo('sturacek@pandora.com');
        mail.setSubject(o.Name+' Close Date Or Start Date In The Past'); 
        body = '<img src="http://c.cs12.content.force.com/servlet/servlet.ImageServer?id=015V00000004p4T&oid=00DV00000004wNA&lastMod=1324353012000"/>'+
        '<p>Hi ' + o.Owner.FirstName + ', </p>'+
        '<p>This email is to notify you that this opportunity has a start date or close date in the past. </p>'+
        '<p>To keep a clean and accurate pipeline please go into this opportunity and update to reflect correct date estimates.</p>'+
        '<p>Opportunity Link: <br/><strong><a href="'+ URL.getSalesforceBaseUrl().toExternalForm() + '/' + o.id+'" target="_blank">'+o.Name+'</a></strong><br/><br/>'+
        'Opportunity Stage: <br/><strong>'+o.StageName+'</strong><br/><br/>'+
        'Current Close Date: <br/><strong>'+o.CloseDate.format()+'</strong><br/><br/>'+
        'Current Start Date: <br/><strong>'+((o.ContractStartDate__c != null) ? o.ContractStartDate__c.format():'')+'</strong></p>'+        
         'Thank you!';
         System.debug('@@@@@@'+body);
         
         mail.setHtmlBody(body);
        //Sending the email  
        Messaging.sendEmail(new Messaging.SingleEmailMessage[] { mail });
    }
}


global void finish(Database.BatchableContext BC){
  // Get the ID of the AsyncApexJob representing this batch job  
  // from Database.BatchableContext.    
  // Query the AsyncApexJob object to retrieve the current job's information.  

AsyncApexJob a = [Select Id, Status, NumberOfErrors, JobItemsProcessed,
   TotalJobItems, CreatedBy.Email
   from AsyncApexJob where Id =:BC.getJobId()];

  // Send an email to the Apex job's submitter notifying of job completion.  
  Messaging.SingleEmailMessage mail = new Messaging.SingleEmailMessage();
  String[] toAddresses = new String[] {a.CreatedBy.Email};
  mail.setToAddresses(toAddresses);
  mail.setSubject('Apex Sharing Recalculation ' + a.Status);
  mail.setPlainTextBody('The batch Apex job processed ' + a.TotalJobItems +
    ' batches with '+ a.NumberOfErrors + ' failures.');

  Messaging.sendEmail(new Messaging.SingleEmailMessage[] { mail });
}

}