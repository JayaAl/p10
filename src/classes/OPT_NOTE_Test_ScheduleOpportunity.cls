@isTest
private class OPT_NOTE_Test_ScheduleOpportunity {
    static testMethod void TestOpportunityScheduler()
    {   
    test.starttest();
    try{
    Account accT = new Account(Name='Test',Type='Advertiser');
    insert accT;  
    Profile p = [Select Id from Profile where name = 'System Administrator'];
    User user1 = new User ();
       
    user1.LastName = 'Tester';
    user1.ProfileId = p.Id;
    user1.TimeZoneSidKey = 'America/Chicago';
    user1.LocaleSidKey = 'en_US';
    user1.EmailEncodingKey = 'ISO-8859-1';
    user1.LanguageLocaleKey = 'en_US';
    user1.alias = 'tst';
    user1.Email = 'tester@pandora.com';
    user1.Username = 'test123@pandora.com';
    insert user1;
                              
    List<Opportunity> opp = new List<Opportunity>();
    opp = Database.query('Select OwnerId From Opportunity where (CloseDate <= TOMORROW OR ContractStartDate__c <= TOMORROW ) AND (StageName <> \'Closed Lost\' AND StageName <> \'Closed Won\') For update');
    if(opp.size()>0)
    {
    delete opp;
    opp = new List<Opportunity>();
    }
    RecordType r = [Select Id from RecordType where DeveloperName <> 'Opportunity_Biz_Dev' AND SobjectType='Opportunity' limit 1];
    for(Integer i=0;i<10;i++)
    {
        Opportunity o = new Opportunity(Confirm_direct_relationship__c=true,Industry_Sub_Category__c='Political Campaign Marketing',Type='Advertiser');
        o.Name='Test'+string.valueOf(i);
        o.Name = 'Test Opp';
        o.Amount = 100;
        o.OwnerId = user1.Id;
        o.AccountId = acct.Id;
        o.Probability = 10;
        o.RecordTypeId = r.Id;
        o.StageName = 'Prospecting';
        if(i<5)
        {
        o.ContractStartDate__c = Date.today()+1;
        o.CloseDate = Date.today()+2;
        }
        else
        {
        o.CloseDate = Date.today()+1;
        o.ContractStartDate__c = Date.today();
        }
        opp.add(o);                                  
    }                                  
    insert opp;
    System.debug('@@@@'+opp);                              
    DateTime currTime = DateTime.now();
        Integer min = currTime.minute();
        Integer hour = currTime.hour();
        String sch;
        
    if(min <= 58)
            sch = '0 '+ (min + 1) + ' ' + hour + ' * * ? '+ currTime.year();
    else          
        sch = '0 0 '+ (hour + 1) + ' * * ? '+ currTime.year();
                                      
    OPT_NOTE_ScheduleOpportunity obj = new OPT_NOTE_ScheduleOpportunity();
    
    String jobId = system.schedule('test', sch, obj);        
    CronTrigger ct = [SELECT id, CronExpression, TimesTriggered, NextFireTime FROM CronTrigger where id = :jobId];        
    System.assertEquals(sch, ct.CronExpression); 
    String query = 'Select Id, Name, Owner.Email, CloseDate, Owner.FirstName, ContractStartDate__c,StageName From Opportunity where (CloseDate <= TOMORROW OR ContractStartDate__c <= TOMORROW) AND (StageName <> \'Closed Lost\' AND StageName <> \'Closed Won\' AND RecordType.DeveloperName <> \'Opportunity_Biz_Dev\')';
    System.debug('$$$$$$'+Database.query(query));
    OPT_NOTE_SendEmailToOwner batchApex = new OPT_NOTE_SendEmailToOwner(query);
    ID batchprocessid = Database.executeBatch(batchApex,10); 
    }
    
    catch (Exception e)
    {
    	
    }
    test.stopTest();
    }
}