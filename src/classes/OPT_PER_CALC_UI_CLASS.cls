public class OPT_PER_CALC_UI_CLASS
{
public List<PerformanceCalculator__c> listPC{get;set;}
public String PCidx{get;set;}
public OPT_PER_CALC_UI_CLASS(ApexPages.StandardController controller)
{
    
    listPC = new List<PerformanceCalculator__c>();
    init();
}
public void init()
{
    listPC = [Select p.Opportunity__c, 
                     p.LastModifiedById, 
                     p.ImpressionsNeeded__c, 
                     p.Id, 
                     p.Performance_Type__c,
                     p.Conv_Rate_from_imps__c, 
                     p.Conv_Rate_from_clicks__c, 
                     p.ClicksNeeded__c, 
                     p.CalculatorType__c, 
                     p.CTR__c, 
                     p.CPM__c, 
                     p.CPC__c, 
                     p.CPA__c, 
                     p.Budget__c, 
                     p.Actions__c 
                From PerformanceCalculator__c p
               where Opportunity__c = :ApexPages.currentPage().getParameters().get('id')];
}

public void DeletePC()
{
    delete listPC[integer.valueOf(PCidx)];
    listPC.remove(integer.valueOf(PCidx));
}
static testMethod void testPcUi()
{
      System.test.starttest();
     //create Account
        Account acc = new   Account(Name ='test Account1',Type='Advertiser');
        insert(acc);
        
           Contact conObj = UTIL_TestUtil.generateContact(acc.id);
        insert conObj;
        
    Opportunity Oppty1 = new Opportunity(name='test Oppty One1',AccountId=acc.id,Confirm_direct_relationship__c=true,Industry_Sub_Category__c='Political Campaign Marketing',Type='Advertiser', Primary_Billing_Contact__c = conObj.id); 
    Oppty1.StageName = 'Test';
    Oppty1.CloseDate = Date.today(); 
    insert Oppty1; 
    ApexPages.currentPage().getParameters().put('id',Oppty1.Id);
    PerformanceCalculator__c objPC = new PerformanceCalculator__c(Opportunity__c = Oppty1.Id);
    insert objPC;
    ApexPages.StandardController sc = new ApexPages.StandardController(Oppty1);
    OPT_PER_CALC_UI_CLASS pc = new OPT_PER_CALC_UI_CLASS(sc);
    pc.PCidx = '0';
    pc.DeletePC();
     System.test.stoptest();
}
}