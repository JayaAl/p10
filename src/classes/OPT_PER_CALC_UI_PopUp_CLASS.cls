public class OPT_PER_CALC_UI_PopUp_CLASS
{
public PerformanceCalculator__c objPC {get; set;}
public Boolean showCalculations{get;set;}
public Opportunity objOPP{get;set;}
public OPT_PER_CALC_UI_PopUp_CLASS()
{
    objPC = new PerformanceCalculator__c();
    objOPP = [Select Id,Name from Opportunity where Id= :ApexPages.currentPage().getParameters().get('oppId')];
    showCalculations = false;
}
public void Calculate()
{
    showCalculations = true;
    if(objPC.CalculatorType__c == 'Performance Calculator A - CPC Campaigns')
    {
        objPC.ClicksNeeded__c = (objPC.Budget__c/objPC.CPC__c);
        objPC.ImpressionsNeeded__c = (objPC.ClicksNeeded__c/(objPC.CTR__c/100));
        objPC.CPM__c = (objPC.Budget__c/(objPC.ImpressionsNeeded__c/1000));
    }
    else if(objPC.CalculatorType__c == 'Performance Calculator B - CPA Campaigns')
    {
        objPC.Actions__c = (objPC.Budget__c/objPC.CPA__c);
        objPC.ClicksNeeded__c = (objPC.Actions__c/(objPC.Conv_Rate_from_clicks__c/100));
        objPC.CPC__c = (objPC.Budget__c/objPC.ClicksNeeded__c);
        objPC.ImpressionsNeeded__c = (objPC.ClicksNeeded__c/(objPC.CTR__c/100));
        objPC.Conv_Rate_from_imps__c = (objPC.Actions__c*100).divide(objPC.ImpressionsNeeded__c, 2, System.RoundingMode.UP);
        objPC.CPM__c = (objPC.Budget__c/(objPC.ImpressionsNeeded__c/1000));
    }
    else if(objPC.CalculatorType__c == 'Performance Calculator C - CPC Campaigns')
    {
        objPC.ImpressionsNeeded__c = (objPC.Budget__c*1000).divide(objPC.CPM__c, 0, System.RoundingMode.UP);
        objPC.ClicksNeeded__c = objPC.Budget__c.divide(objPC.CPC__c, 0, System.RoundingMode.UP);
        objPC.CTR__c = (objPC.ClicksNeeded__c*100).divide(objPC.ImpressionsNeeded__c, 0, System.RoundingMode.UP);
    }
    else if(objPC.CalculatorType__c == 'Performance Calculator D - CPC Campaigns')
    {
        objPC.ImpressionsNeeded__c = objPC.Budget__c.divide(objPC.CPM__c, 0, System.RoundingMode.UP)*1000;
        objPC.ClicksNeeded__c = (objPC.ImpressionsNeeded__c*(objPC.CTR__c/100));
        objPC.CPC__c = objPC.Budget__c.divide(objPC.ClicksNeeded__c, 2, System.RoundingMode.UP);
    }
}
public void Save()
{
    Calculate();
    objPC.Opportunity__c = objOPP.Id;
    insert objPC;
    /*String newPageUrl = '/'+objOPP.Id;
    PageReference newPage = new PageReference(newPageUrl);
    newPage.setRedirect(true);
    return newPage;*/
    
}

public void resetVariables()
{
    String temp = '';
    temp = objPC.CalculatorType__c;
    objPC = new PerformanceCalculator__c();
    objPC.CalculatorType__c = temp;
    showCalculations = false;
}
static testMethod void testPcUi()
{
   System.test.starttest();
     //create Account
        Account acc = new   Account(Name ='test Account1',Type='Advertiser');
        insert(acc);
        
         // updated by VG 12/26/2012
          Contact conObj = UTIL_TestUtil.generateContact(acc.id);
        insert conObj;
    
        
    Opportunity Oppty1 = new Opportunity(name='test Oppty One1',AccountId=acc.id,Primary_Billing_Contact__c =conObj.Id, Confirm_direct_relationship__c=true,Industry_Sub_Category__c='Political Campaign Marketing',Type='Advertiser'); 
    Oppty1.StageName = 'Test';
    Oppty1.CloseDate = Date.today(); 
    insert Oppty1; 
    ApexPages.currentPage().getParameters().put('oppId',Oppty1.Id);
    PerformanceCalculator__c objPCTest = new PerformanceCalculator__c
                                                                 (
                                                                  Opportunity__c = Oppty1.Id,
                                                                  CalculatorType__c = 'Performance Calculator A - CPC Campaigns',
                                                                  Conv_Rate_from_imps__c = 10.0,
                                                                  Conv_Rate_from_clicks__c = 9.0,
                                                                  ClicksNeeded__c = 6.0,
                                                                  CTR__c = 6.0,
                                                                  CPM__c = 1.10,
                                                                  CPC__c = 2.5,
                                                                  CPA__c = 6.5,
                                                                  Budget__c = 5000,
                                                                  Actions__c = 95
                                                                 );
    insert objPCTest;
    OPT_PER_CALC_UI_PopUp_CLASS  pc = new OPT_PER_CALC_UI_PopUp_CLASS();
    pc.resetVariables();
    pc.Save();
    pc.objPC = objPCTest;
    pc.Calculate();
    pc.objPC.CalculatorType__c = 'Performance Calculator B - CPA Campaigns';
    pc.Calculate();
    pc.objPC.CalculatorType__c = 'Performance Calculator C - CPC Campaigns';
    pc.Calculate();
    pc.objPC.CalculatorType__c = 'Performance Calculator D - CPC Campaigns';
    pc.Calculate();
    System.test.stoptest();
}
}