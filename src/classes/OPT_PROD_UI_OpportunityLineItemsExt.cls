public class OPT_PROD_UI_OpportunityLineItemsExt {
    public List<Selectoption> listAllProd{get;set;}
    public List<PricebookEntry> listPE;
    public String selProduct{get;set;}
    public List<OpportunityLineItem> objOLI{get;set;}
    public String focusedLi{get;set;}
    public Integer oliIdx{get;set;}
    public List<OLIWrapper> listOLIWrapper{get;set;}
    public Boolean showSaveAllButton{get;set;}
    public Integer countAdded{get;set;}
    public Opportunity objOpp{get;set;}
    public String errorMessage{get;set;}
    public String pageId{get;set;}
    public String pageURL {set;}
    public Boolean reloadNeeded {get; set;}
    public Boolean disableButton {get;set;}
    Datetime currentDate = system.now();// Start date

    /* Lakshman, 11/7/2015: Added pagination functionality to avoid view state error: ESS-22182 */

    //Pagination variables
    Integer queryLimit;
    Integer offset;

    // for active and deactive page naviagator
    public Boolean firstOff{set;get;}    // previous part
    public Boolean lastOff{set;get;}     // next part

    public String limits{set;get;}
    public Integer pageNumber{set;get;}

    public Integer listSize {get;set;}    // for total OpportunityLineItem size
    public String getPageURL() {
        String newPageUrl = '/'+ApexPages.currentPage().getParameters().get('id')+'#'+pageId;
        return newPageUrl;
    }
    public OPT_PROD_UI_OpportunityLineItemsExt(ApexPages.StandardController stdCont){
        firstOff = false;
        queryLimit = 10;
        offset = 0;
        limits = '10';
        pageNumber = 1;
        aggregateResult res = [
            SELECT COUNT(id) cnt
            FROM OpportunityLineItem
            WHERE OpportunityId = :System.currentPageReference().getParameters().get('id')
        ];

        // fill size of all OpportunityLineItem
        listSize = Integer.valueOf(res.get('cnt'));

        // initialy check page more then 1 or not
        if(listSize > queryLimit) lastOff = true;
        else lastOff = false;
        init();
    }

/* Helper to determine whether the Oppty should use updated Taxonomy layouts */
    public Set<String> setTaxonomyRTs{
        get{
            if(setTaxonomyRTs == null||setTaxonomyRTs.isEmpty()){
                setTaxonomyRTs = new Set<String>();
                for(TaxonomyRecordTypes__c t:[Select Name from TaxonomyRecordTypes__c]){
                    setTaxonomyRTs.add(t.Name);
                }
            }
            return setTaxonomyRTs;
        }
        set;
    }
    public Boolean isTaxonomyRT{
        get{
            return setTaxonomyRTs.contains(opportunity.RecordType.DeveloperName);
        }
        set;
    }
/* Helper to generate the PriceBookEntry to relate all new Taxonomy products to by default */
    private String defaultTaxonomyPBEName = 'Audio Everywhere';
    private PriceBookEntry taxonomyPBE{
        get{
            if(taxonomyPBE==null){
                if(listPE == null){
                    getListProd();
                }
                for(PricebookEntry pbe:listPE){
                    if(pbe.Name == defaultTaxonomyPBEName){
                        taxonomyPBE = pbe;
                        break;
                    }
                }
            }
            return taxonomyPBE;
        }
        set;
    }


    //This is init method called to reset the view
    public void init() {
        objOLI = new List<OpportunityLineItem>();
        listOLIWrapper = new List<OLIWrapper>();
        oliIdx = 0;
        reloadNeeded = false;
        disableButton = false;
        showSaveAllButton = false;
        countAdded = 0;
        pageId = [SELECT Id FROM ApexPage WHERE Name = 'OPT_PROD_UI_OpportunityLineItems'].Id;
        pageId = pageId.substring(0,15);
        objOpp = [
            Select CloseDate, CreatedDate,  RecordType.Name,  Pricebook2Id,  Amount, AccountId,  CurrencyIsoCode, Account.Name, Account.BillingStreet, Account.BillingState, Account.BillingPostalCode, Account.BillingCountry,  Account.BillingCity, FinancialForce_Invoice_Number__r.Name
            FROM Opportunity
            WHERE Id =:System.currentPageReference().getParameters().get('id')];
        getWrapperRecords();
        getListProd();
    }

    public void getWrapperRecords() {
        objOLI = [
           Select Date_Adjustment_Made__c, Audio_Everywhere_Split_Product__c, CreatedDate, HasSchedule, Duration__c, Discount__c,
                eCPM__c, End_Date__c, Impression_s__c, Offering_Type__c, Medium__c, Platform__c, Sub_Platform__c, Banner__c, Banner_Type__c,
                Size__c, Takeover__c, Guaranteed__c, Cost_Type__c, Performance_Type__c, PricebookEntry.Name, PricebookEntryId, ServiceDate,
                Quantity, CurrencyIsoCode, Total_Amount__c, UnitPrice
            From OpportunityLineItem o
            where OpportunityId =: System.currentPageReference().getParameters().get('id')
            Order By CreatedDate Desc  LIMIT :queryLimit OFFSET :offset
        ];//Lakshman: Changed Order By CreatedDate Desc from Asc to view latest added records first
        listOLIWrapper.clear();
        for (OpportunityLineItem item : objOLI){
            OLIWrapper wrapper = new OLIWrapper(oliIdx++);
            wrapper.isNew = false;
            wrapper.item = item;
            listOLIWrapper.add(wrapper);
        }
    }

    // navigate on next page
    public void next(){
        offset += queryLimit;
        if(offset+queryLimit >= listSize) lastOff = false;
        firstOff = true;
        pageNumber++;
        getWrapperRecords();
    }

    // navigate on previous page
    public void previous(){
        if(offset-queryLimit <= 0){
            offset = 0;
            firstOff = false;
        } else {
            offset -= queryLimit;
        }
        lastOff = true;
        pageNumber--;
        getWrapperRecords();
    }

    // switch on first page
    public void first(){
        offset = 0;
        firstOff = false;
        lastOff = true;
        pageNumber = 1;
        getWrapperRecords();
    }

    // switch on last page
    public void last(){
        // set page number of and offset
        if(Math.Mod(listSize,queryLimit) == 0){
            offset = listSize-queryLimit;
            pageNumber = listSize/queryLimit;
        } else {
            offset = (listSize/queryLimit)*queryLimit;
            pageNumber = (listSize/queryLimit)+1;
        }

        lastOff = false;
        firstOff = true;
        getWrapperRecords();
    }

    // for record limits
    public List<SelectOption> getItems(){
        List<SelectOption> options = new List<SelectOption>();
        options.add(new SelectOption('10','10'));
        options.add(new SelectOption('20','20'));
        options.add(new SelectOption('50','50'));
        options.add(new SelectOption('100','100'));
        return options;
    }

    // change query limit
    public void changeLimit(){
        // set query limit
        queryLimit = Integer.valueOf(limits);

        offset = 0;
        firstOff = false;

        // initialy check page more then 1 or not
        if(listSize > queryLimit) lastOff = true;
        else lastOff = false;

        // set page number
        pageNumber = 1;
        getWrapperRecords();
    }

    // for show current record numbers
    public String getRecordInfo(){
        integer lastLimit;
        if(offset+queryLimit > listSize) lastLimit = listSize;
        else lastLimit = offset+queryLimit;
        return (offset+1) + ' - ' + lastLimit + ' of '+listSize;
    }

    // return total page number
    public Integer getTotalPage(){
        if(Math.Mod(listSize,queryLimit) == 0) return listSize/queryLimit;
        else return (listSize/queryLimit)+1;
    }

    public List<Integer> previousSkipPageNumbers {
        get{
            List<Integer> returnValues = new List<Integer>();
            for(Integer i = 3; i > 0; i--){
                if(pageNumber-i <= 0){
                    continue;
                }
                returnValues.add(pageNumber-i);
            }
            return returnValues;
        }
    }

    public List<Integer> nextSkipPageNumbers {
        get{
            List<Integer> returnValues = new List<Integer>();
            for(Integer i = 1; i <= 3; i++){
                if(pageNumber+i > getTotalPage()){
                    break;
                }
                returnValues.add(pageNumber+i);
            }
            return returnValues;
        }
    }

    // for direct page switching
    public void pageNavigation(){

        /* if user enter more then number ot total page number than
           set the value last page number in PageNumber. */
        if(Math.Mod(listSize,queryLimit) == 0 && pageNumber > listSize/queryLimit)
            pageNumber = listSize/queryLimit;
        else if(pageNumber > (listSize/queryLimit)+1)
            pageNumber = (listSize/queryLimit)+1;

        // set offset according to pageNumber
        if((pageNumber-1)*queryLimit < 0) offset = 0;
        else offset = (pageNumber-1)*queryLimit;

        /* if pageNumber is 1 than deactive previous navigator
           else if pageNumber is o tha set the value of pageNumber is 1
           else if pageNumber is more than 1 active next navigator
        */
        if(pageNumber == 1) firstOff = false;
        else if(pageNumber == 0) pageNumber = 1;
        else if(pageNumber > 1) firstOff = true;

        // user enter last number of pagenumber than deactive next navigator
        if(Math.Mod(listSize,queryLimit) == 0){
            if(pageNumber == listSize/queryLimit) lastOff = false;
            else lastOff = true;
        } else {
            if(pageNumber == (listSize/queryLimit)+1) lastOff = false;
            else lastOff = true;
        }
        getWrapperRecords();
    }

    //Called to get list of all products
    public void getListProd(){
        listAllProd = new List<Selectoption>();
        listPE = new List<PricebookEntry>();
        if(objOpp.RecordType.Name == Label.Oppty_Gift_Code_record_type_label) {  // Modified by Lakshman on 2014-04-23 to fix multiple products issue for Pandora One Subs opps
            listPE = [
                SELECT p.Id, p.Name, UnitPrice
                FROM PricebookEntry p
                WHERE IsActive=true
                AND Name Like 'Pandora One %'
                AND Pricebook2.Name = 'Standard Price Book'
                AND CurrencyIsoCode =: objOpp.CurrencyIsoCode
                ORDER BY Name desc
            ];
        } else {
            listPE = [
                SELECT p.Id, p.Name, UnitPrice
                FROM PricebookEntry p
                WHERE IsActive=true
                AND ( NOT Name LIKE 'Pandora One %' )  // changed
                AND Pricebook2.Name = 'Standard Price Book'
                AND CurrencyIsoCode =: objOpp.CurrencyIsoCode
                AND Product2.Auto_Schedule__c = true // Added
                ORDER BY Name
            ];
        }
        for(PricebookEntry objPE: listPE){
            listAllProd.add(new SelectOption(objPE.Id,objPE.Name));
        }
    }

    //This method will be invoked to edit the seleted OLI
    public void editLineItem(){
        focusedLi = System.currentPageReference().getParameters().get('selectedID');
    }

//This method will cancel the add or save operation
    public void cancelLineItem()
    {
        for(Integer i = 0; i < listOLIWrapper.size(); i++)
        {
            if (listOLIWrapper[i].oliId == System.currentPageReference().getParameters().get('selectedID'))
            {
                if(listOLIWrapper[i].isNew == true)
                {
                    listOLIWrapper.remove(i);
                    countAdded--;
                    if(countAdded<2)
                    showSaveAllButton = false;
                }
                focusedLi = '';
                break;
            }
        }
    }

    //this method will deleted existing OLI
    public void deleteLineItem()
    {
        for(Integer i = 0; i < listOLIWrapper.size(); i++)
        {
            if (listOLIWrapper[i].oliId == System.currentPageReference().getParameters().get('selectedID'))
            {
                if (listOLIWrapper[i].isNew == false)
                {
                delete (listOLIWrapper[i].item);
                }
                focusedLi = '';
                listOLIWrapper.remove(i);
                reloadNeeded = true;
                break;
            }
        }
    }
    
    public pageReference viewLineItem(){
        pageReference newPageRef = new PageReference('/'+System.currentPageReference().getParameters().get('selectedID'));
        return newPageRef;
    }

    //This method will save OLI
    public void saveLineItem(){
        try{
            for(OLIWrapper oliW:listOLIWrapper){
                if (oliW.oliId == System.currentPageReference().getParameters().get('selectedID')){
                    if(validateOLIWrapper(oliW)){
                        break; // End if the OLI does not meet all criteria
                    }

                    if(oliW.isNew == false){
                        System.debug(oliW.item.UnitPrice+'@@@@@'+oliW.item.ServiceDate);
                        oliW.item.Duration__c = oliW.item.ServiceDate.daysBetween(oliW.item.End_Date__c)+1;

                        if(objOpp.RecordType.Name == Label.Oppty_Gift_Code_record_type_label) {
                            if(oliW.item.Discount__c <> NULL){
                                oliW.item.UnitPrice = oliW.item.UnitPrice - (oliW.item.UnitPrice * oliW.item.Discount__c.divide(100,2));
                            }
                        } else {
                            if(objOpp.RecordType.Name == 'Opportunity - Performance') {
                                if(oliW.item.Performance_Type__c != 'Monthly Adjustment'){
                                    oliW.item.eCPM__c = (oliW.item.UnitPrice/oliW.item.Impression_s__c)*1000;
                                } else if(oliW.item.Impression_s__c != 0){
                                    oliW.item.eCPM__c = (oliW.item.UnitPrice/oliW.item.Impression_s__c)*1000;
                                } else {
                                    oliW.item.eCPM__c = 0;
                                }
                            } else if(objOpp.RecordType.Name == 'Opportunity - Programmatic') {
                                if(oliW.item.Performance_Type__c != 'Monthly Adjustment'){
                                    oliW.item.eCPM__c = (oliW.item.UnitPrice/oliW.item.Impression_s__c)*1000;
                                } else if(oliW.item.Impression_s__c != 0){
                                    oliW.item.eCPM__c = (oliW.item.UnitPrice/oliW.item.Impression_s__c)*1000;
                                } else {
                                    oliW.item.eCPM__c = 0;
                                }
                            }
                            OPT_PROD_STATIC_HELPER.runForProds = true;
                            oliW.item.Quantity = 1.0;
                        }
                        OpportunityLineItem temp = oliW.item.clone(false);
                        temp.OpportunityId = System.currentPageReference().getParameters().get('id');
                        delete oliW.item;
                        oliW.item = temp;
                        insert oliW.item;
                        reloadNeeded = true;
                        break;
                    } else {
                        oliW.item.Duration__c = oliW.item.ServiceDate.daysBetween(oliW.item.End_Date__c)+1;
                        if(objOpp.RecordType.Name == Label.Oppty_Gift_Code_record_type_label) {
                            OPT_PROD_STATIC_HELPER.runForProds = false;
                            if(oliW.item.Discount__c <> NULL){
                                oliW.item.UnitPrice = oliW.item.UnitPrice - (oliW.item.UnitPrice * oliW.item.Discount__c.divide(100,2));
                            }
                        } else {
                            if(objOpp.RecordType.Name == 'Opportunity - Performance') {
                                if(oliW.item.Performance_Type__c != 'Monthly Adjustment'){
                                    oliW.item.eCPM__c = (oliW.item.UnitPrice/oliW.item.Impression_s__c)*1000;
                                } else if(oliW.item.Impression_s__c != 0){
                                    oliW.item.eCPM__c =(oliW.item.UnitPrice/oliW.item.Impression_s__c)*1000;
                                } else{
                                    oliW.item.eCPM__c = 0;
                                }
                            } else if(objOpp.RecordType.Name == 'Opportunity - Programmatic') {
                                if(oliW.item.Performance_Type__c != 'Monthly Adjustment'){
                                    oliW.item.eCPM__c = (oliW.item.UnitPrice/oliW.item.Impression_s__c)*1000;
                                } else if(oliW.item.Impression_s__c != 0){
                                    oliW.item.eCPM__c =(oliW.item.UnitPrice/oliW.item.Impression_s__c)*1000;
                                } else {
                                    oliW.item.eCPM__c = 0;
                                }
                            }
                            OPT_PROD_STATIC_HELPER.runForProds = true;
                        }
                        insert oliW.item;
                        oliW.isNew = false;
                        reloadNeeded = true;
                        break;
                    }
                    focusedLi = '';
                }
            }

        }catch(Exception ex){
            if(ex.getMessage().contains('Owner role for Opportunity must match Agency')){
                errorMessage = 'Owner role for Opportunity must match Agency or Advertiser before you may continue to save!';
            } else {
                errorMessage = parseValidationMessage(ex.getMessage());
            }
            system.debug('Exception Caught@@@'+ex.getMessage());
        }

    }

    private Boolean validateOLIWrapper(OLIWrapper oliW){
        Boolean hasErr = false;

        if( currentDate.Date().daysBetween(oliW.item.ServiceDate) > 3650 ) {
            oliW.item.ServiceDate.addError('Start date cannot be over 10 years into the future');
            hasErr = true;
        }

        if( currentDate.Date().daysBetween(oliW.item.End_Date__c) > 3650 ) {
            oliW.item.End_Date__c.addError('End date cannot be over 10 years into the future');
            hasErr = true;
        }

        if( oliW.item.End_Date__c < oliW.item.ServiceDate ) {
            oliW.item.End_Date__c.addError('End must be after start'+ ' - OppId: ' + objOpp.id);
            hasErr = true;
        }

        if( // Start Date must be after the Close Date
            oliW.item.ServiceDate < objOpp.CloseDate && oliW.item.CreatedDate > objOpp.CreatedDate && String.valueOf(objOpp.RecordTypeId).substring(0,15) != Label.OpportunityRT_Performance
        ){
            oliW.item.ServiceDate.addError('Start Date must be after the Close Date' + ' - OppId: ' + objOpp.id);
            hasErr = true;
        }

        if( // Impressions required
            objOpp.RecordType.Name == 'Opportunity - Performance' && ( oliW.item.Impression_s__c == null || ( oliW.item.Impression_s__c == 0 && oliW.item.Performance_Type__c != 'Monthly Adjustment' ) )
        ){
            oliW.item.Impression_s__c.addError('Please enter correct Impression'+ ' - OppId: ' + objOpp.id);
            hasErr = true;
        }

        if( // Adjustment Date cannot be blank if Monthly Adjustment is selected
            objOpp.RecordType.Name == 'Opportunity - Performance' && oliW.item.Performance_Type__c == 'Monthly Adjustment' && oliW.item.Date_Adjustment_Made__c == null
        ){
            oliW.item.Date_Adjustment_Made__c.addError('Adjustment Date cannot be blank if Monthly Adjustment is selected'+ ' - OppId: ' + objOpp.id);
            hasErr = true;
        }

        if( // Adjustment Date can only be added if Monthly Adjustment is selected
            objOpp.RecordType.Name == 'Opportunity - Performance' && oliW.item.Performance_Type__c != 'Monthly Adjustment' && oliW.item.Date_Adjustment_Made__c != null
        ){
            oliW.item.Date_Adjustment_Made__c.addError('Adjustment Date can only be added if Monthly Adjustment is selected'+ ' - OppId: ' + objOpp.id);
            hasErr = true;
        }
        return hasErr;
    }

    //called to save all the OLI
    public void saveAllLineItem() {
        try{
            List<OpportunityLineItem> massOLI = new List<OpportunityLineItem>();
            for(OLIWrapper oliW:listOLIWrapper){
                if(oliW.isNew == true) {
                    //nsk: 10/21 - ISS-11221
                    //if( oliW.item.ServiceDate < objOpp.CloseDate && datetime.now() > objOpp.CreatedDate )
                    if(validateOLIWrapper(oliW)){
                        break;
                    } else {
                        oliW.item.Duration__c = oliW.item.ServiceDate.daysBetween(oliW.item.End_Date__c)+1;
                    }

                    if(objOpp.RecordType.Name == Label.Oppty_Gift_Code_record_type_label) {
                        OPT_PROD_STATIC_HELPER.runForProds = false;
                        if(oliW.item.Discount__c <> NULL)
                        oliW.item.UnitPrice = oliW.item.UnitPrice - (oliW.item.UnitPrice * oliW.item.Discount__c.divide(100,2));

                        /*insert oliW.item;
                        reloadNeeded = true;*/
                    } else {
                        if(objOpp.RecordType.Name == 'Opportunity - Performance') {
                            if(oliW.item.Performance_Type__c != 'Monthly Adjustment')
                            oliW.item.eCPM__c = (oliW.item.UnitPrice/oliW.item.Impression_s__c)*1000;
                            else if(oliW.item.Impression_s__c != 0)
                            oliW.item.eCPM__c = (oliW.item.UnitPrice/oliW.item.Impression_s__c)*1000;
                            else
                            oliW.item.eCPM__c = 0;
                        } else if(objOpp.RecordType.Name == 'Opportunity - Programmatic') {
                            if(oliW.item.Performance_Type__c != 'Monthly Adjustment')
                            oliW.item.eCPM__c = (oliW.item.UnitPrice/oliW.item.Impression_s__c)*1000;
                            else if(oliW.item.Impression_s__c != 0)
                            oliW.item.eCPM__c = (oliW.item.UnitPrice/oliW.item.Impression_s__c)*1000;
                            else
                            oliW.item.eCPM__c = 0;
                        }
                        OPT_PROD_STATIC_HELPER.runForProds = true;
                        /*insert oliW.item;
                        oliW.isNew = false;
                        reloadNeeded = true;*/
                    }
                    //insert oliW.item;
                    massOLI.add(oliW.item);
                    oliW.isNew = false;
                    //oliW.item.PricebookEntryId = oliW.priceBookId;
            }
        }
        if(massOLI.size() > 0) {
            insert massOLI;
            focusedLi = '';
            reloadNeeded = true;
        }
    } catch(Exception ex) {
        if(ex.getMessage().contains('Owner role for Opportunity must match Agency')){
            errorMessage = 'Owner role for Opportunity must match Agency or Advertiser before you may continue to save!';
        } else {
            errorMessage = parseValidationMessage(ex.getMessage());
        }
        system.debug('Exception Caught@@@'+ex.getMessage());
        }
    }

    public String parseValidationMessage(String ex) {
        if(ex != null && ex.contains('FIELD_CUSTOM_VALIDATION_EXCEPTION')) {
            String retString = ex.substring(ex.indexOf('FIELD_CUSTOM_VALIDATION_EXCEPTION'),ex.indexOf(': ['));
            return retString;
        }
        return ex;
    }



    //Called to add OLI
    public void AddProduct(){
        OLIWrapper ow = new OLIWrapper(oliIdx++);
        ow.item.OpportunityId = System.currentPageReference().getParameters().get('id');
        ow.item.Quantity = 1.0;
        if(listPE.size() > 0) {
            ow.item.UnitPrice = listPE[0].UnitPrice;
        }

        // For Taxonomy related products select the approproiate PriceBookEntry since we hide that field on the layout
        if(isTaxonomyRT){
            ow.item.PricebookEntryId = taxonomyPBE.Id;
        }

        countAdded++;
        if(countAdded>1){
            showSaveAllButton = true;
        }
        List<OLIWrapper> tempList = new List<OLIWrapper>();
        tempList.add(ow);
        if(listOLIWrapper.size() > 0){
            tempList.addAll(listOLIWrapper);
        }
        listOLIWrapper = tempList;
    }

    //Method to fetch Unit Price for selected Product
    public void fetchUnitPriceForProd(){
        String priceBookId = System.currentPageReference().getParameters().get('pbId');
        String selectOliId = System.currentPageReference().getParameters().get('selectedID');
        system.debug(selectOliId+'@@@@@'+priceBookId);
        for(OLIWrapper oliW:listOLIWrapper){
            if (oliW.oliId == System.currentPageReference().getParameters().get('selectedID')){
                for(PricebookEntry objPE: listPE){
                    if(objPE.Id == priceBookId){
                        oliW.item.UnitPrice = objPE.UnitPrice;
                        //oliW.item.PricebookEntryId = objPE.Id;
                    }
                }
            }
        }
    }
    //Everything is here in the wrapper
    public class OLIWrapper
    {
        public OpportunityLineItem item { get; set;}
        public String oliId{get;set;}
        public Boolean isNew{get;set;}
        public Boolean selected {get; set;}
        //public String priceBookId{get;set;}

        public OLIWrapper(Integer oliId){
            item = new OpportunityLineItem();
            this.oliId = ''+ oliId;
            isNew = true;
            selected= false;
        }
    }
    //End of Wrapper
    public pageReference CloneProductLines(){
        System.debug('listOLIWrapper=============='+listOLIWrapper);
        List<OLIWrapper> tempList = new List<OLIWrapper>();
        for(OLIWrapper oliW:listOLIWrapper){
            if(oliW.selected){
                OLIWrapper ow = new OLIWrapper(oliIdx++);
                ow.item = oliW.item.Clone(false, true, false, false);
                ow.item.OpportunityId = System.currentPageReference().getParameters().get('id');
                
                // the following are nulled out on clone per ESS-26904
                ow.item.Platform__c = null;
                ow.item.Sub_Platform__c = null;
                ow.item.Banner__c = null;
                ow.item.Banner_Type__c = null;
                ow.item.Size__c = null;
                ow.item.Cost_Type__c = null;
                ow.item.Guaranteed__c = false; // Needs to be FALSE, checkboxes cannot be NULL
                // system.assert(false);
                countAdded++;
                if(countAdded>1)
                showSaveAllButton = true;
                tempList.add(ow);
            }
        }
        if(listOLIWrapper.size() > 0)
        tempList.addAll(listOLIWrapper);
        listOLIWrapper = tempList;
      return null;
    }

    private Opportunity opportunity{
        get{
            if(opportunity == null){
                opportunity = new Opportunity();
                if(mapURLParams.containsKey('Id')){
                    opportunity = OppFromId(mapURLParams.get('Id'));
                }
            }
            return opportunity;
        }
        set;
    }

    private Opportunity OppFromId(Id theId){
        Opportunity o = null;
        if(theId!=null){
            o = [
                SELECT Name,Amount,OwnerId,AccountId,Probability, CurrencyIsoCode, Splits_Locked__c, ContractStartDate__c, RecordType.DeveloperName
                FROM Opportunity o
                WHERE id = :theId
            ];
        }
        return o;
    }

    private Map<String, String> mapURLParams{
        get{
            return ApexPages.currentPage().getParameters();
        }
        set;
    }
}