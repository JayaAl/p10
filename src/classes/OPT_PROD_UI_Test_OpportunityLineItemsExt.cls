@isTest
public class OPT_PROD_UI_Test_OpportunityLineItemsExt{
    
/* TODO: Add system asserts to the following to determine if the methods behave as expected, rather than just assuming so if they do not error on running them */
    static testMethod void test_getPageURL(){
        test_DataPrep();
        ext.selProduct ='testProd';
        ext.errorMessage='errorMess';
        ext.pageURL ='test';
        ext.focusedLi ='test';
        ext.getPageURL();
    }
    
    static testMethod void test_editLineItem(){
        test_DataPrep();
        ext.editLineItem();
    }
    
    static testMethod void test_saveLineItem(){
       test_DataPrep();
        ext.saveLineItem();
      /*  ext.AddProduct();
        ext.saveAllLineItem();
        ext.fetchUnitPriceForProd();
        ext.CloneProductLines();
        List<OPT_PROD_UI_OpportunityLineItemsExt.OLIWrapper> lstOwrap = new List<OPT_PROD_UI_OpportunityLineItemsExt.OLIWrapper>();
        OPT_PROD_UI_OpportunityLineItemsExt.OLIWrapper owrap = new OPT_PROD_UI_OpportunityLineItemsExt.OLIWrapper(2);
        owrap.oliId=''+2;
        owrap.isNew = true;
        owrap.selected= false;
        owrap.item.Quantity =1;
        owrap.item.UnitPrice=1;
        lstOwrap.add(owrap);
        ext.editLineItem();
        ext.CloneProductLines();
        ext.cancelLineItem();
        ext.deleteLineItem();
        ext.saveLineItem();
        ext.AddProduct();
        ext.saveAllLineItem();
        ext.fetchUnitPriceForProd();
        System.currentPageReference().getParameters().put('id',Oppty1.Id);
        OPT_PROD_UI_OpportunityLineItemsExt.OLIWrapper owrap1 = new OPT_PROD_UI_OpportunityLineItemsExt.OLIWrapper(2);
        owrap1.oliId=''+2;
        owrap1.isNew = true;
        owrap1.selected= true;
        owrap.item.Quantity =1;
        owrap.item.UnitPrice=1;
        lstOwrap.add(owrap1);
        ext.CloneProductLines();
        ext.cancelLineItem();
        ext.deleteLineItem();
        
        //now we have data, lets test pagination
        Test.startTest();
            ext = new OPT_PROD_UI_OpportunityLineItemsExt(testController);
            ext.getItems();
            ext.next();
            ext.previous();
            ext.first();
            ext.last();
            ext.limits = '10';
            ext.changeLimit();
            ext.getRecordInfo();
            List<Integer> tempLst = ext.previousSkipPageNumbers;
            tempLst = ext.nextSkipPageNumbers;
            ext.pageNumber = 1;
            ext.pageNavigation();
            
        Test.stopTest();*/
    }
    
    /*
static testMethod void test_DataPrep1(){
prepareTestRecords();
Test.setCurrentPage(Page.OPT_PROD_UI_OpportunityLineItems);
Test.setCurrentPage(Page.OPT_PROD_UI_OpportunityLineItems);
System.currentPageReference().getParameters().put('id',Oppty1.Id);
ApexPages.StandardController testController = new ApexPages.Standardcontroller(Oppty1);
OPT_PROD_UI_OpportunityLineItemsExt ext = new OPT_PROD_UI_OpportunityLineItemsExt(testController);
ext.selProduct ='testProd';
ext.errorMessage='errorMess';
ext.pageURL ='test';
ext.getPageURL();
ext.editLineItem();
ext.saveLineItem();
ext.AddProduct();
ext.saveAllLineItem();
ext.fetchUnitPriceForProd();
ext.CloneProductLines();
List<OPT_PROD_UI_OpportunityLineItemsExt.OLIWrapper> lstOwrap = new List<OPT_PROD_UI_OpportunityLineItemsExt.OLIWrapper>();
OPT_PROD_UI_OpportunityLineItemsExt.OLIWrapper owrap = new OPT_PROD_UI_OpportunityLineItemsExt.OLIWrapper(2);
owrap.isNew = true;
owrap.selected= true;
owrap.item.Quantity =1;
owrap.item.UnitPrice=1;
lstOwrap.add(owrap);
System.currentPageReference().getParameters().put('selectedID','2');
ext.editLineItem();
ext.CloneProductLines();
ext.cancelLineItem();
ext.deleteLineItem();
ext.saveLineItem();
ext.AddProduct();
ext.saveAllLineItem();
ext.fetchUnitPriceForProd();
System.currentPageReference().getParameters().put('id',Oppty1.Id);
ext.CloneProductLines();
ext.cancelLineItem();
ext.deleteLineItem();
}
*/
    
    
// Test data preparation
    // Common objects for testing
    private static Product2 prod1;
    private static Product2 prod2;
    private static Pricebook2 pb1;
    private static PricebookEntry pbe1;
    private static PricebookEntry pbe2;
    private static List<OpportunityLineItem> bulkoli;
    private static Account acct1;
    private static Opportunity Oppty1;
    private static ApexPages.StandardController testController;
    private static OPT_PROD_UI_OpportunityLineItemsExt ext;
    
    private static void test_DataPrep(){
        prepareTestRecords();
        Test.setCurrentPage(Page.OPT_PROD_UI_OpportunityLineItems);
        System.currentPageReference().getParameters().put('id',Oppty1.Id);
        testController = new ApexPages.Standardcontroller(Oppty1);
        ext = new OPT_PROD_UI_OpportunityLineItemsExt(testController);
    }
    
    private static void prepareTestRecords(){
        // Create Products
        prod1 = testProd('test prod one1');
        prod2 = testProd('test prod two2');
        insert new List<Product2>{prod1,prod2};  
            
            // Get Pricebook 
            pb1 = testPB();
        
        // Create PriceBookEntries 
        pbe1 = testPBE(pb1.id, prod1.id);
        pbe2 = testPBE(pb1.id, prod2.id); 
        
        insert new List<PricebookEntry>{pbe1,pbe2};
            
            bulkoli = new List<OpportunityLineItem>();
        
        acct1 = UTIL_TestUtil.createAccount();
        
        Oppty1 = testOppty(Label.Oppty_Gift_Code_record_type_label);
        insert Oppty1; 
        
        for(integer bi=0; bi<6; bi++) {
            bulkoli.add(
                new OpportunityLineItem(
                    Quantity = 1,
                    Duration__c=12,
                    UnitPrice = 1,
                    PriceBookEntryId = pbe2.id,
                    Impression_s__c=10,
                    OpportunityId = Oppty1.id,
                    End_Date__c=Date.today().addDays(11),
                    ServiceDate = Date.today().addDays(11)
                )
            ); 
        }
        insert bulkoli;
    }
    
    
    
    // Custom object creators to set values for these tests
    private static Opportunity testOppty(String rtName){
        Opportunity testOpp = UTIL_TestUtil.generateOpportunity(acct1.Id);
        // new Opportunity(AccountId=acct1.Id,name='test Oppty One1',Confirm_direct_relationship__c=true,
        testOpp.Industry_Sub_Category__c='Political Campaign Marketing';
        testOpp.Type='Advertiser'; 
        testOpp.StageName = 'Qualified';
        testOpp.CloseDate = Date.today(); 
        Schema.DescribeSObjectResult R;
        Map<String,Schema.RecordTypeInfo> rtMapById;
        R = Opportunity.SObjectType.getDescribe();
        rtMapById = R.getRecordTypeInfosByName();
        testOpp.RecordTypeId=rtMapById.get(rtName).getRecordtypeId();
        return testOpp;
    }
    
    private static Product2 testProd(String prodName){ 
        // not using UTIL_TestUtil.createProduct() as we need to set many specific values
        Product2 testprod = new Product2 (
            name=prodName,
            productcode = prodName,
            CanUseRevenueSchedule = True,
            NumberOfRevenueInstallments = 12,
            RevenueInstallmentPeriod = 'Monthly',
            RevenueScheduleType = 'Repeat'
        );
        return testprod;
    }
    private static Pricebook2 testPB(){
        Pricebook2 pb = [select id from Pricebook2 where IsStandard = true];
        return pb;
    }
    private static PricebookEntry testPBE(Id pbId, Id prodId){
        PricebookEntry pbe = new PricebookEntry (
            pricebook2id = pbId,
            product2id = prodId,
            IsActive = True,
            UnitPrice = 250,
            UseStandardPrice = false
        );
        return pbe;
    }
    private static String taxonomyRTName(){
        TaxonomyRecordTypes__c t = [Select Name from TaxonomyRecordTypes__c limit 1];
        return t.Name;
    }
    
}