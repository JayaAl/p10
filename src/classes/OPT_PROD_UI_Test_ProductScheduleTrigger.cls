/** * This class tests the trigger named ProductScheduleTrigger. */ 
@isTest private class OPT_PROD_UI_Test_ProductScheduleTrigger { 
static testMethod void myUnitTest() {
//Data Prep 
//Create Account, Opportunity, Product, etc. 

Account acct1 = new Account(name='test Account One1',Type='Advertiser'); 
insert acct1; 

     // updated by VG 12/26/2012
          Contact conObj = UTIL_TestUtil.generateContact(acct1.id);
        insert conObj;

//Create Opportunity on Account 
Opportunity Oppty1 = new Opportunity(name='test Oppty One1',AccountId=acct1.id,Primary_Billing_Contact__c =conObj.Id, Confirm_direct_relationship__c=true,Industry_Sub_Category__c='Political Campaign Marketing',Type='Advertiser'); 
Oppty1.StageName = 'Test'; Oppty1.CloseDate = Date.today(); 
insert Oppty1; 
// Create Products 
Product2 testprod1 = new Product2 (name='test product one1'); 
testprod1.productcode = 'test pd code1one'; 
testprod1.CanUseRevenueSchedule = True; 
testprod1.NumberOfRevenueInstallments = 12; 
testprod1.RevenueInstallmentPeriod = 'Monthly';
testprod1.Auto_Schedule__c = False; 
testprod1.RevenueScheduleType = 'Repeat'; 
insert testprod1; 
Product2 testprod2 = new Product2 (name='test product two2'); 
testprod2.productcode = 'test pd code2two'; 
testprod2.CanUseRevenueSchedule = True; 
testprod2.NumberOfRevenueInstallments = 12;
testprod2.RevenueInstallmentPeriod = 'Monthly'; 
testprod2.RevenueScheduleType = 'Repeat';
testprod2.Auto_Schedule__c = True; 
insert testprod2; 
// Ger Pricebook 
Pricebook2 testpb = [select id from Pricebook2 where IsStandard = true];
// Add to pricebook 
PricebookEntry testpbe1 = new PricebookEntry (); 
testpbe1.pricebook2id = testpb.id; 
testpbe1.product2id = testprod1.id; 
testpbe1.IsActive = True; 
testpbe1.UnitPrice = 250; 
testpbe1.UseStandardPrice = false; 
insert testpbe1; 
PricebookEntry testpbe2 = new PricebookEntry (); 
testpbe2.pricebook2id = testpb.id; 
testpbe2.product2id = testprod2.id; 
testpbe2.IsActive = True; 
testpbe2.UnitPrice = 250; 
testpbe2.UseStandardPrice = false; 
insert testpbe2; 
//And now you want execute the startTest method to set the context 
//of the following apex methods as separate from the previous data 
//preparation or DML statements. 
// Adding multiple line items in bulk which should call the trigger. 
// Auto Schedule is true so it should build the schedule. 
// This trigger does not work in large bulk amounts as it adds multiple rows. 
test.starttest(); 
List<OpportunityLineItem> bulkoli = new List<OpportunityLineItem>(); 
integer todo = 3; 
for(integer bi=0; bi<todo; bi++) {
bulkoli.add( new OpportunityLineItem(Quantity = 1,Duration__c=12, UnitPrice = 1, PriceBookEntryId = testpbe2.id, OpportunityId = Oppty1.id, End_Date__c = Date.today().addDays(10),ServiceDate = NULL) ); 
} 
OPT_PROD_STATIC_HELPER.runForProds = true;
insert bulkoli; 
System.assertEquals(12*todo, [select count() from OpportunityLineItemSchedule where OpportunityLineItemId in :bulkoli]); 
List<OpportunityLineItem> bulkoli2 = new List<OpportunityLineItem>(); 
integer todo2 = 3; 
for(integer bii=0; bii<todo2; bii++) { 
bulkoli2.add( new OpportunityLineItem(Quantity = 1, Duration__c=12, UnitPrice = 1, PriceBookEntryId = testpbe2.id, OpportunityId = Oppty1.id, End_Date__c = Date.today().addDays(10), ServiceDate = Date.today().addDays(5)) ); 
} 
OPT_PROD_STATIC_HELPER.runForProds = true;
insert bulkoli2; 
System.assertEquals(12*todo, [select count() from OpportunityLineItemSchedule where OpportunityLineItemId in :bulkoli]); 
test.stoptest(); 
} 
}