@isTest
private class OPT_PRTNR_Test_PartnerRoleOPPS
{
    static testMethod void myUnitTest() 
    {
        Profile p = [Select Id from Profile where Name = 'System Administrator' limit 1];
        UserRole r = [Select Id, Name from UserRole where Name ='Administrators' limit 1];
        UserRole r1 = [Select Id, name from UserRole where Name !='Administrators' AND PortalType = 'None' limit 1];
        User u1 = new User( UserRoleId=r.id,
                            ProfileId = p.Id,
                            FirstName='Test',
                            LastName='Pandora',
                            Username='tester123@pandora.com' + System.now().getTime(),
                            alias = 'pTester', 
                            email='pandoraTester@testorg.com', 
                            emailencodingkey='UTF-8', 
                            languagelocalekey='en_US', 
                            localesidkey='en_US', 
                            timezonesidkey = 'America/Los_Angeles'
                           );
        insert u1;
        User u2 = new User( UserRoleId=r1.Id,
                            ProfileId = p.Id,
                            FirstName='Test',
                            LastName='Pandora',
                            Username='tester1234@pandora.com' + System.now().getTime(),
                            alias = 'pTester', 
                            email='pandoraTester@testorg.com', 
                            emailencodingkey='UTF-8', 
                            languagelocalekey='en_US', 
                            localesidkey='en_US', 
                            timezonesidkey = 'America/Los_Angeles'
                           );
        System.debug('role = ' + r1.Name);
        insert u2;
                
        System.runAs(u1)
        {
          Account acct1 = new Account(name='test Account One1', OwnerId=u1.Id, Type='Ad Agency'); 
          insert acct1; 
          Account acct2 = new Account(name='test Account Two2', OwnerId=u1.Id,Type='Advertiser'); 
          insert acct2;
          
         // Contact con = new Contact(lastname='Test Contact one1',AccountId = acct1.Id);
         // insert con;
          
           // updated by VG 12/26/2012
          Contact con = UTIL_TestUtil.generateContact(acct1.id);
    
        
        //Contact c = new Contact();// specify all the required fields
        con.firstname = 'MyFristName';
        con.lastname = 'MyLastName';
        con.Title = 'MyTitle';
        con.Email = 'myemail@mydomain.com';
        con.MailingCity = 'Fremont';
        con.MailingState = 'CA';
        con.MailingCountry = 'USA';
        con.MailingStreet = 'MyStreet';
       insert con;
          
          
          
          //Create Opportunity on Account 
          Opportunity Oppty1 = new Opportunity(OwnerId=u1.Id,AccountID=acct2.Id,name='test Oppty One1',Agency__c=acct1.Id, Primary_Contact__c =con.Id,Primary_Billing_Contact__c =con.Id, Industry_Sub_Category__c='Political Campaign Marketing',Type='Advertiser'); 
          Oppty1.StageName = 'Test'; 
          Oppty1.CloseDate = Date.today()+1;
          //Oppty1.AccountId = acct2.Id; 
          insert Oppty1; 
          Partner op = new Partner (AccountToId = Oppty1.Agency__c,
                                        IsPrimary = true,
                                        Role = Oppty1.Agency__r.Type,
                                        OpportunityId = Oppty1.Id);
          insert op;                                      
          OpportunityContactRole oc = new OpportunityContactRole (ContactId = Oppty1.Primary_Contact__c,
                                                                          IsPrimary = true,
                                                                          Role = Oppty1.Primary_Contact__r.Role__c,
                                                                          OpportunityId = Oppty1.Id);
          insert oc;                                        
          /*Oppty1.Name = 'Test 123';
          Oppty1.OwnerId=u2.Id;
          update Oppty1;*/
       }
    }
}