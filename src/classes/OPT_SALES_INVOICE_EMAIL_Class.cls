public class OPT_SALES_INVOICE_EMAIL_Class{

/* Commenting all code in this class as the functionality is deprecated, and replaced by Conga functionality
 * TODO: Delete this stand-alone class, no separate test class as this was created prior to the separation being enforced
 * Date: 2015-04-02
 * Author: Casey Grooms cgrooms@pandora.com
 * 
    public String OpportunityOwnerId{get;set;}
    public String numberOfDays{get;set;}
    public String error{get;set;}
    public String query{get;set;}
    public List<c2g__codaInvoice__c> listSI{get;set;}
    public Id batchprocessid{get;set;}
    public Boolean showInvoiceBlock{get;set;}
    public Boolean showBatchBlock{get;set;}
    public String displayLimit{get;set;}
    public List<c2g__codaInvoice__c> listSalesInvoice{get;set;}
    public OPT_SALES_INVOICE_EMAIL_Class()
    {
        listSI = new List<c2g__codaInvoice__c>();
        numberOfDays = '60';
        showInvoiceBlock = true;
        showBatchBlock = false;
    }
    
    public void viewBatchJobs()
    {
        showBatchBlock = true;
    }
    
    public List<GroupedQuery> getGroupedResult()
    {
        List<GroupedQuery> queryResults = new List<GroupedQuery>();
        listSalesInvoice = new List<c2g__codaInvoice__c>();
        listSalesInvoice = [SELECT   Billing_Contact__r.Name, 
                                     Name,
                                     Id,
                                     c2g__Account__r.Name,
                                     Advertiser__r.Name,
                                     c2g__Opportunity__c,
                                     Aged_Category_120__c,
                                     Billing_Terms__c,
                                     Operative_Order_ID__c,
                                     c2g__Opportunity__r.Name,
                                     c2g__Opportunity__r.Owner.Name, 
                                     Times_Notified__c,
                                     c2g__InvoiceTotal__c,
                                     c2g__OutstandingValue__c
                              FROM c2g__codaInvoice__c where c2g__Opportunity__r.Owner.Id = :OpportunityOwnerId ORDER by Aged_Category_120__c NULLS last];
                                
         Decimal countTotal = 0;
         Integer totalRecords = 0;
         Integer i=0;
         
         for(c2g__codaInvoice__c s :listSalesInvoice)
         {
             if(i!=0)
             {

                 if(s.Aged_Category_120__c == listSalesInvoice[i-1].Aged_Category_120__c)
                 {
                     if(s.c2g__OutstandingValue__c <> null)
                     countTotal += s.c2g__OutstandingValue__c;
                     totalRecords++;
                     
                     
                 }
                 if(s.Aged_Category_120__c != listSalesInvoice[i-1].Aged_Category_120__c)
                 {
                     GroupedQuery myObject = new GroupedQuery();
                     myObject.Count = totalRecords;
                     myObject.GroupBy1 = listSalesInvoice[i-1].Aged_Category_120__c;
                     myObject.Sum = countTotal;
                     queryResults.Add(myObject);
                     totalRecords = 1;
                     countTotal = 0;
                     if(s.c2g__OutstandingValue__c <> null)
                     countTotal += s.c2g__OutstandingValue__c;
                     
                 }
                 if(listSalesInvoice.size() == i+1)
                 {
                     GroupedQuery myObject = new GroupedQuery();
                     myObject.Count = totalRecords;
                     myObject.GroupBy1 = listSalesInvoice[i].Aged_Category_120__c;
                     myObject.Sum = countTotal;
                     queryResults.Add(myObject);
                     totalRecords = 1;
                     countTotal = 0;
                     if(s.c2g__OutstandingValue__c <> null)
                     countTotal += s.c2g__OutstandingValue__c;
                 }
                 
             }
             else
             {
                 if(s.c2g__OutstandingValue__c <> null)
                 countTotal += s.c2g__OutstandingValue__c;
                 totalRecords = 1;
                 if(listSalesInvoice.size()==1)
                 {
                     GroupedQuery myObject = new GroupedQuery();
                     myObject.Count = totalRecords;
                     myObject.GroupBy1 = listSalesInvoice[i].Aged_Category_120__c;
                     myObject.Sum = countTotal;
                     queryResults.Add(myObject);
                 }
             }
             i++;
         }
        return queryResults;
    }
    
    
    public List<SelectOption> getDays{
    get {
      if (getDays == null) {
    
        List<SelectOption> options = new List<SelectOption>();
        options.add(new SelectOption('60','60'));
        options.add(new SelectOption('90','90'));
        options.add(new SelectOption('120','120'));
        return options;
      }
      return getDays;          
    }
    set;
    }
    
    public void runJob()
    {
        List<AsyncApexJob> openJobs = new List<AsyncApexJob>();
        openJobs = [select Id from AsyncApexJob where Status = 'Processing' OR Status = 'Queued']; 
        if(openJobs.size() < 5){
               OPT_SCH_SALES_INVOICE_EMAIL_Class schApex = new OPT_SCH_SALES_INVOICE_EMAIL_Class();
               schApex.numberOfDays = integer.valueOf(numberOfDays);
               Integer dayOfSchedule = (datetime.newInstance(system.now().year(), system.now().month(), system.now().day(), 0, 0, 1) > system.now()) ? system.now().day() : (math.min(system.now().day()+1,31));
               String cronStr = '1 0 0 '+ dayOfSchedule + ' * ? *';//seconds, minutes, hours, dayofMonth, month, dayofWeek, year(optional)
               if(numberOfDays == '60')
               System.schedule('InvoiceEmailJob60', cronStr, schApex);
               else if(numberOfDays == '90')
               System.schedule('InvoiceEmailJob90', cronStr, schApex);
               else if(numberOfDays == '120')
               System.schedule('InvoiceEmailJob120', cronStr, schApex);
        }
        else{
            error = 'Only five batch jobs can run at a time.';
        }              
        
    }
    
    public void runQuery()
    {
        
        Integer siCount = Database.countQuery('Select count() FROM c2g__codaInvoice__c where c2g__Opportunity__c <> null And (c2g__PaymentStatus__c = \'Unpaid\' OR c2g__PaymentStatus__c= \'Part Paid\') AND CreatedDate < LAST_N_DAYS:'+integer.valueOf(numberOfDays));
        if(siCount > 1000)
        displayLimit = 'There are more than 1000 Sales Invoices, showing only 1000';
        else
        displayLimit = '';
        query = 'SELECT Billing_Contact__r.Name, '+
                     'Advertiser__r.Name, '+
                     'Name, '+
                     'Billing_Terms__c,'+
                     'Operative_Order_ID__c,'+
                     'c2g__Opportunity__r.Name,'+
                    'c2g__Opportunity__r.Owner.Name, '+
                    'Times_Notified__c, '+
                     'c2g__OutstandingValue__c '+
              'FROM c2g__codaInvoice__c where c2g__Opportunity__c <> null And (c2g__PaymentStatus__c = \'Unpaid\' OR c2g__PaymentStatus__c= \'Part Paid\') AND CreatedDate < LAST_N_DAYS:'+integer.valueOf(numberOfDays)+' Limit 1000';
        listSI = Database.query(query);
        if(listSI.size()>0)
        showInvoiceBlock = true;
        else
        showInvoiceBlock = false; 
        showBatchBlock = false;             
    }
    public class GroupedQuery
    {
        public string GroupBy1     {get;set;}
        public integer Count {get;set;}
        public Decimal Sum {get;set;}
    }
    static testMethod void runTestSalesInvoice() {
        test.startTest();
        OPT_SALES_INVOICE_EMAIL_Class objCntrl = new OPT_SALES_INVOICE_EMAIL_Class();
        objCntrl.getGroupedResult();
        objCntrl.viewBatchJobs();
        List<SelectOption> daysPick = objCntrl.getDays;
        objCntrl.runQuery();
        objCntrl.runJob();
        test.stopTest();
        
    }
    */
}