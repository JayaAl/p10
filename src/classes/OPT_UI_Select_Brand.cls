public class OPT_UI_Select_Brand{
public List<Selectoption> listAllAccBrands{get;set;}
public String selectedBrand{get;set;}
public Opportunity objOpp{get;set;}
public Boolean reloadNeeded {get; set;}
public Boolean someError {get;set;}
public OPT_UI_Select_Brand(ApexPages.StandardController stdCont)
{
    init();
}
public void init()
{
    objOpp = [Select AccountId,Brand__c, Brand__r.Name from Opportunity where Id = :System.currentPageReference().getParameters().get('id')];        
    reloadNeeded = false;
    someError = false;
}
public String getPageURL() {
    String newPageUrl = '/'+ApexPages.currentPage().getParameters().get('id'); 
    return newPageUrl;
}
public List<Selectoption> getBrandList()
{
    listAllAccBrands = new List<Selectoption>(); 
    List<Brand__c> listB = new List<Brand__c>();
    listB = [Select Id, Name From Brand__c  where Account_Name__c=:objOpp.AccountId];
    for(Brand__c objB: listB)
    {
        listAllAccBrands.add(new SelectOption(objB.Id,objB.Name));
    }
    return listAllAccBrands; 
}
public void saveBrand()
{
    objOpp.Brand__c = selectedBrand;
    try{
    update objOpp;
    init();
    //reloadNeeded = true;
    }catch(Exception ex){
    reloadNeeded = false;
    someError = true;
    objOpp.Brand__c = null;
    ApexPages.addMessage(new ApexPages.message(ApexPages.severity.ERROR,'Please review the record before selecting Brand<br/>'));
    }
    
}

public void deleteBrand()
{
    objOpp.Brand__c = null;
    try{
    update objOpp;
    reloadNeeded = true;
    }catch(Exception ex){
    someError = true;
    reloadNeeded = false;
    ApexPages.addMessage(new ApexPages.message(ApexPages.severity.ERROR,'Please review the record before deleting Brand<br/>'));
    }
}

static testMethod void runTestBrand() {
    Account acct1 = new Account(name='test Account One1'); 
    test.startTest();
    insert acct1;
    
     // updated by VG 12/26/2012
          Contact conObj = UTIL_TestUtil.generateContact(acct1.id);
        insert conObj;
    
    Brand__c b = new Brand__c(Name='Test', Account_Name__c=acct1.Id);
    insert b; 
    //Create Opportunity on Account 
    Opportunity Oppty1 = new Opportunity(name='test Oppty One1', accountId = acct1.Id, Primary_Billing_Contact__c =conObj.Id); 
    Oppty1.StageName = 'Test'; Oppty1.CloseDate = Date.today(); 
    Oppty1.Confirm_direct_relationship__c = true;
    insert Oppty1;
    ApexPages.currentPage().getParameters().put('id',Oppty1.Id);
    ApexPages.StandardController   testController   = new ApexPages.Standardcontroller(Oppty1);
    OPT_UI_Select_Brand ctrl = new  OPT_UI_Select_Brand(testController);
    ctrl.getBrandList();
    ctrl.saveBrand();
    ctrl.deleteBrand();
    ctrl.getPageURL();
    test.stopTest();
    
    
}
}