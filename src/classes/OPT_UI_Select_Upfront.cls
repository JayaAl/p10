public class OPT_UI_Select_Upfront{
public List<Selectoption> listAllAccUpfronts{get;set;}
public String selectedUpfront{get;set;}
public Opportunity objOpp{get;set;}
public Boolean reloadNeeded {get; set;}
public Boolean someError {get;set;}
public String agencyId;
public String accountId;
public OPT_UI_Select_Upfront(ApexPages.StandardController stdCont)
{
    init();
}
public void init()
{
    objOpp = [Select Owner.Email, AccountId,Account.Name, Agency__c, Agency__r.Name, Upfront__c, Upfront__r.Name, Upfront__r.Current_Tier__c, Id, Name from Opportunity where Id = :System.currentPageReference().getParameters().get('id')];
    accountId = objOpp.AccountId <> null ? objOpp.AccountId : null;   
    agencyId = objOpp.Agency__c <> null ? objOpp.Agency__c : null;     
    if(objOpp.Upfront__c <> null){
    	selectedUpfront = objOpp.Upfront__c;
    } else {
        selectedUpfront = '--None--';
    }
    reloadNeeded = false;
    someError = false;
}
public String getPageURL() {
    String newPageUrl = '/'+ApexPages.currentPage().getParameters().get('id'); 
    return newPageUrl;
}
    
    /* Updated 12-3-2014: Casey Grooms
	* Updated to include default option in the list of '--None--'
	*/
    public List<Selectoption> getUpfrontList(){
        // add a boolean to track whether there are any Upfronts found, and pre-populate the default SelectOption
        boolean boolUpfrontsFound = false;
        listAllAccUpfronts = new List<Selectoption>(); 
        listAllAccUpfronts.add(new SelectOption('--None--','--None--'));
        // Query Upfront_Junction__c records and populate the list
        for(Upfront_Junction__c objB: [
            SELECT Upfront__c, Upfront__r.Name, Upfront__r.Amount_Spent__c 
            FROM Upfront_Junction__c  
            WHERE (Account__c=:agencyId OR Account__c=:accountId) AND Upfront__r.Expiration_Date__c >= TODAY
        ]){
            listAllAccUpfronts.add(new SelectOption(objB.Upfront__c,objB.Upfront__r.Name));
            boolUpfrontsFound = true;
        }
        // If Upfronts were found return the list, otherwise return an empty list
        if(boolUpfrontsFound){
            return listAllAccUpfronts; 
        }
        return new List<Selectoption>();
    }
    public void saveUpfront(){
        if(selectedUpfront != NULL && selectedUpfront != '' && selectedUpfront != '--None--'){
            objOpp.Upfront__c = selectedUpfront;
            try{
                update objOpp;
                updateUpfrontAmtSpent();
                init();
                sendEmailToOwner();
                //reloadNeeded = true;
            } catch(Exception ex) {
                reloadNeeded = false;
                someError = true;
                objOpp.Upfront__c = null;
                ApexPages.addMessage(new ApexPages.message(ApexPages.severity.ERROR,'Please review the record before selecting Upfront <br/> '+ex.getMessage()));
            }
        } else {
            ApexPages.addMessage(new ApexPages.message(ApexPages.severity.ERROR,'No Upfront Selected. <br /> Please make a selecton before updating the Opportunity.'));
        }
    }
public void sendEmailToOwner()
{
    String subject = 'Upfront associated to new opportunity';
    String[] toaddress = new String[]{};
    toaddress.add(objOpp.Owner.Email);
    String body = 'The Upfront [<a href="' + URL.getSalesforceBaseUrl().toExternalForm() + '/' + objOpp.Upfront__c +'">' + objOpp.Upfront__r.Name+ '</a>]'+ 
                  ' has been associated to a new opportunity [<a href="' + URL.getSalesforceBaseUrl().toExternalForm() + '/' + objOpp.Id +'">'+objOpp.Name+ '</a>]'+
                  '<br/> Account Name:[<a href="' + URL.getSalesforceBaseUrl().toExternalForm() + '/'+ (accountId <> null ? objOpp.AccountId : objOpp.Agency__c) +'">'+ (accountId <> null ? objOpp.Account.Name : objOpp.Agency__r.Name)+ '</a>]';
    Messaging.SingleEmailMessage mail = new Messaging.SingleEmailMessage();
    mail.setToAddresses(toaddress);
    mail.setsubject(subject);
    mail.sethtmlbody(body);
    Messaging.sendEmail(new Messaging.SingleEmailMessage[] { mail });
}
public void deleteUpfront()
{
    objOpp.Upfront__c = null;
    try{
    update objOpp;
    updateUpfrontAmtSpent();
    reloadNeeded = true;
    }catch(Exception ex){
    someError = true;
    reloadNeeded = false;
    ApexPages.addMessage(new ApexPages.message(ApexPages.severity.ERROR,'Please review the record before deleting Upfront<br/>'+ex.getMessage()));
    }
}
public void updateUpfrontAmtSpent()
{
    /*Modified by VG: 1/9/2013-> Added Closed Won conditioin in the query*/
    AggregateResult[] groupedResults = [Select SUM(Amount) amt From Opportunity where Upfront__c=:selectedUpfront  AND StageName = 'Closed Won'];
    Decimal sumAmount = (Decimal)groupedResults[0].get('amt');
    Upfront__c objUp = [Select Amount_Spent__c from Upfront__c where Id =: selectedUpfront];
    objUp.Amount_Spent__c = sumAmount;
    update objUp;
}
static testMethod void runTestUpfront() {
    Account acct1 = new Account(name='test Account One1'); 
    insert acct1;
    
          // updated by VG 12/26/2012
          Contact conObj = UTIL_TestUtil.generateContact(acct1.id);
        insert conObj;
    
    Upfront__c b = new Upfront__c(Name='Test', Expiration_Date__c = Date.today() + 1);
    insert b; 
    Upfront_Junction__c objUJ = new Upfront_Junction__c(Account__c = acct1.Id, Upfront__c = b.Id );
    insert objUJ;
    //Create Opportunity on Account 
    Opportunity Oppty1 = new Opportunity(name='test Oppty One1', accountID = acct1.Id, Primary_Billing_Contact__c =conObj.Id); 
    Oppty1.StageName = 'Prospecting'; Oppty1.CloseDate = Date.today(); 
    Oppty1.Confirm_direct_relationship__c = true;
    insert Oppty1;
    
    test.startTest();
        ApexPages.currentPage().getParameters().put('id',Oppty1.Id);
        ApexPages.StandardController   testController   = new ApexPages.Standardcontroller(Oppty1);
        OPT_UI_Select_Upfront ctrl = new  OPT_UI_Select_Upfront(testController);
        ctrl.getUpfrontList();
        ctrl.objOpp.StageName = 'Closed Won';//Added by Lakshman to cover Upfront calulation changes
        ctrl.objOpp.Bill_on_Broadcast_Calendar2__c = 'Yes';
        ctrl.objOpp.Lead_Campaign_Manager__c = UserInfo.getUserId();
        ctrl.selectedUpfront = b.Id;
        ctrl.saveUpfront();
        ctrl.deleteUpfront();
        ctrl.getPageURL();
    test.stopTest();
}
}