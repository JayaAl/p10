/**
 * @name: OPT_Upfront_UpdateCaseFromOpp
 * @desc: Used to copy Upfront value from Opportunity to Case
 * @author: Lakshman(sfdcace@gmail.com)
 */
public class OPT_Upfront_UpdateCaseFromOpp{
    List<String> listOfAllowedRecTypes = new List<String>{'COGS_Approval_Create','COGS_Approval_Details'};//Constant list of allowed Record Types
    
    /**
     * @desc: Method - updateCaseUpfrontFromOppAfterInsert contains logic to copy opportunity Upfront value to Case Upfront
     * @param: Cases array of Case records
     * @param: newCaseMap Case map with new values
     */
    public void updateCaseUpfrontFromOppAfterInsert(Case[] cases){
        if(!cases.isEmpty()){
            Set<Id> oppIds = new Set<Id>();
            Set<Id> rtIds = new Set<Id>();
            for(RecordType rt : [Select Id from RecordType where DeveloperName =: listOfAllowedRecTypes]){
                rtIds.add(rt.Id);
            }
            Map<Id,Id> mapOfOppToUpfront = new Map<id,Id>();
            
            for(Case objCase: cases){
                if(objCase.Opportunity__c != null && rtIds.contains(objCase.RecordTypeId)){
                    oppIds.add(objCase.Opportunity__c);
                }
            }
            if(!oppIds.isEmpty()){
                for(Opportunity objOpp : [Select Upfront__c, Id from Opportunity where Id =: oppIds]){
                    if(objOpp.Upfront__c != null){
                        mapOfOppToUpfront.put(objOpp.Id, objOpp.Upfront__c);
                    }
                }
                if(!mapOfOppToUpfront.isEmpty()){
                    for(Case objCase : cases){
                        if(objCase.Opportunity__c != null && objCase.Opportunity__r.Upfront__c != null){
                            if(mapOfOppToUpfront.containsKey(objCase.Opportunity__c)){
                                objCase.Upfront__c = mapOfOppToUpfront.get(objCase.Opportunity__c);
                            }
                        }
                    }
                }
            }
        }
    }
    
    
    /**
     * @desc: Method - updateCaseUpfrontFromOppAfterInsert contains logic to copy opportunity Upfront value to Case Upfront
     * @param: opportunities array of Opportunity records
     * @param: oldOpportunityMap Opportunity map with old values
     * @param: newOpportunityMap Opportunity map with new values     
     */
    public void updateCaseUpfrontFromOppBeforeUpdate(Opportunity[] opportunities, Map<Id,Opportunity> oldOpportunityMap, Map<Id,Opportunity> newOpportunityMap){
        if(!newOpportunityMap.isEmpty()){
            Map<Id, Id> oppToUpfront = new Map<Id, Id>();
            List<Case> listCase = new List<Case>();
            for(Opportunity oppNew : newOpportunityMap.values()){
                if(oppNew.Upfront__c != null && oldOpportunityMap.get(oppNew.Id).Upfront__c != newOpportunityMap.get(oppNew.Id).Upfront__c){
                    oppToUpfront.put(oppNew.Id, oppNew.Upfront__c);
                }
            }
            if(! oppToUpfront.isEmpty()){
                Set<Id> rtIds = new Set<Id>();
                for(RecordType rt : [Select Id from RecordType where DeveloperName =: listOfAllowedRecTypes]){
                    rtIds.add(rt.Id);
                }
                for(Case objCase : [Select Id, Upfront__c,Opportunity__c, RecordTypeId from Case where Opportunity__c In : oppToUpfront.keySet()]){
                    if(rtIds.contains(objCase.RecordTypeId) && objCase.Upfront__c != oppToUpfront.get(objCase.Opportunity__c)){
                        objCase.Upfront__c = oppToUpfront.get(objCase.Opportunity__c);
                        listCase.add(objCase);
                    }
                }
                if(!listCase.isEmpty()){
                    Database.SaveResult[] results = Database.update(listCase, false);
                    for(Database.SaveResult sr: results){
                       if(!sr.isSuccess()){
                          system.debug('Errors if any=>'+sr.getErrors()[0]);
                       }
                    }
                }
            }
        }
    }
}