/**
* ─────────────────────────────────────────────────────────────────────────────────────────────────┐
* This class is controller extenssion for vf page OpportuityClone.
* ──────────────────────────────────────────────────────────────────────────────────────────────────
* @author         Jaya Alaparthi
* @maintainedBy   Jaya Alaparthi
* @version        1.0
* @created        2017-04-13
* @modified       
* @systemLayer    Service
* @see            ????
* @see            ????
* ──────────────────────────────────────────────────────────────────────────────────────────────────
* @changes
* v1.1            Jaya Alaparthi
* 2018-01-30      Adding functionality to create Data licensing ESS-43633
* ──────────────────────────────────────────────────────────────────────────────────────────────────
* @changes
* v1.2            Jaya Alaparthi
* 2017-06-06      
* ─────────────────────────────────────────────────────────────────────────────────────────────────┘
*/
public with sharing class OpportunityCloneExt {
        
    public Opportunity objOpp {get;set;}
    private List<OpportunityLineItem> listOppProducts; 
    private Boolean isWithProducts;
    public String cloneProduct;
    public Id parentOpptyId;
    public map<Integer,PageGeneratorWrapper> pgGenWrapperDisplay {get;set;}
    // use this to display data from api
    public list<PageGeneratorWrapper> pageGenWrapperList {get;set;} 
    

    public OpportunityCloneExt(ApexPages.StandardController stdCont) {

        ApexPages.Message extCtrlMsg;
        Id opptyRecordtypeId;
        String pageLayoutId;
        String fieldsetName;
        Opportunity_Clone_VF_Page_Map__c cloneVfRef;

        isWithProducts = false;
        objOpp = new Opportunity();
        //v1.1
        // retrive parameters
        opptyRecordtypeId = ApexPages.currentPage().getParameters().get('rid');
        if(String.isBlank(opptyRecordtypeId) && stdCont.getId() != null) {

            objOpp = (Opportunity)Database.query(
                        OpptyPageGenUtil.fetchAllCreatableFields('Opportunity',
                                                            stdCont.getId()));
            opptyRecordtypeId = objOpp.RecordTypeId;
            system.debug('======'+objOpp);
            parentOpptyId = stdCont.getId();
        }
        //should products be cloned
        String cloneProdsParam = ApexPages.currentPage().getParameters().get('cProds');
        cloneProduct =  String.isBlank(cloneProduct) ? cloneProdsParam : cloneProduct;

        System.debug('cloneProduct:'+cloneProduct+'opptyRecordtypeId:'+opptyRecordtypeId);
        // get fieldsets
        try {

            cloneVfRef = Opportunity_Clone_VF_Page_Map__c.getValues(opptyRecordtypeId);
            pageLayoutId = cloneVfRef.Opportunity_LayoutId__c;

            if(cloneProduct != null && cloneProduct != '' && cloneProduct == '1') {

                isWithProducts = true;
                fieldsetName = cloneVfRef.WithProductsFieldSet__c;    
            } else {

                isWithProducts = false;
                fieldsetName = cloneVfRef.WithoutProductsFieldSet__c;    
            }
            System.debug('fieldsetName:'+fieldsetName);
            if(String.isBlank(fieldsetName)) {

                // post the error msg to vf page
                extCtrlMsg = new ApexPages.Message(ApexPages.Severity.ERROR,
                            'Required Configuration for generating clone page is missing.');
                ApexPages.addMessage(extCtrlMsg);
            }
            else {

                Schema.DescribeSObjectResult DescribeSObjectResultObj = Schema.getGlobalDescribe().get('Opportunity').getDescribe();
                Schema.FieldSet fieldSetObj = DescribeSObjectResultObj.FieldSets.getMap().get(fieldsetName);

                System.debug('pageLayoutId:'+pageLayoutId+'**********fieldSetObj.getFields():'+fieldSetObj.getFields());
                // populate using api
                pageGenWrapperList = OpportunityLayoutAPIHelper.metadataToDisplay(pageLayoutId,
                                                                                fieldSetObj.getFields());
                // clone PLI
                if(isWithProducts) {

                    listOppProducts = (List<OpportunityLineItem>)Database.query(
                                        OpptyPageGenUtil.fetchAllCreatableFields('OpportunityLineItem',
                                                                                stdCont.getId()));
                }
                // clone Oppty based on recrdtype
                //v1.1 no clone needed in this case
                if(ApexPages.currentPage().getParameters().get('rid') == null) { 

                    OpptyPageGenUtil.populateFieldsToBeCloned(objOpp,
                                                        fieldSetObj.getFields());
                }
                
                System.debug('cloned fields are populated');
                if(isWithProducts) {

                    objOpp.RecordTypeId = opptyRecordtypeId;
                    // this flag avoids creating some default products.
                    objOpp.Do_not_add_default_products__c = true;
                    //objOpp.Account_Development_Specialist__c = null;
                } else {

                    objOpp.RecordTypeId = opptyRecordtypeId;
                    //objOpp.Account_Development_Specialist__c = null;
                }
             
                objOpp.OwnerId = UserInfo.getUserId();
            }
            
        } catch(Exception e) {

            if(cloneVfRef ==  null) {

                extCtrlMsg = new ApexPages.Message(ApexPages.Severity.ERROR,
                        'Required Configuration for generating clone page is missing.');
            
            } else {
                extCtrlMsg = new ApexPages.Message(ApexPages.Severity.ERROR,
                                                    'Error:'+e.getMessage());
            }
            ApexPages.addMessage(extCtrlMsg);
        }
    }


    public PageReference save() {
        String retURL = '';
        objOpp.Id = null;

        try {
            insert objOpp;
        }catch (system.Dmlexception e) {
            System.debug('DML Error in CloneExt class : '+e.getMessage());
            ApexPages.Message insertErr = new ApexPages.Message(ApexPages.Severity.Error,'Error:'+e.getMessage());
            //ApexPages.addMessages(e);
            ApexPages.addMessage(insertErr);
            return null;
        }

        retURL = '/'+objOpp.Id;
        if(isWithProducts) {
            //Adding new workflow in place when dealing with "Clone with Products"
            retURL = '/apex/CloneWithProducts?id='+objOpp.Id+'&parentOppId='+parentOpptyId;
          
        }
        return new PageReference(retURL);    
     }
     
}