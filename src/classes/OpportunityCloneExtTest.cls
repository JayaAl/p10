@isTest(SeeAllData = true)
private class OpportunityCloneExtTest {
	
	
	static testmethod void test_method_two() {


		RecordType acctRT = [SELECT Id
   							FROM RecordType
   							WHERE SobjectType = 'Account'
   							AND DeveloperName = 'default'];
   		Account acct = new Account(RecordTypeId = acctRT.Id,
   							Name='testClsActOptyClonePage',
   							Type='Advertiser');
   		insert acct;
   		// retrive recordtypeId
   		RecordType recordType = [SELECT DeveloperName,Id,IsActive,Name 
   								FROM RecordType 
   								WHERE SobjectType = 'Opportunity' 
   								AND DeveloperName = 'Opportunity_Inside_Sales'
   								AND IsActive = true];

   		Opportunity oppty = new Opportunity(accountId = acct.Id,
   							RecordTypeId = recordType.Id,
   							Name = 'testClsOpptyClonePage',
   							StageName = 'ClosedWon',
   							Industry_Category__c = 'B2B',
   							Budget_Source__c = 'TV',
   							Lead_Campaign_Manager__c = UserInfo.getUserId(),
   							Initiative_Location__c = 'US',
   							CloseDate = Date.today());
   		insert oppty;

		// Implement test code
		// run OpportunityLayoutAPIHelper with Mockup
		Test.startTest();
		//Opportunity oppty = new Opportunity(id='0064000000SoMf2AAF');
		Test.setMock(HttpCalloutMock.class,
						new OpportunityCloneExtMockTest());
		
		
		PageReference pref = Page.OpportunityClone;
		Test.setCurrentPage(pref);
		pref.getParameters().put('cProds','1');
		ApexPages.StandardController std = new ApexPages.StandardController(oppty);
		OpportunityCloneExt testExt = new OpportunityCloneExt(std);
		//testExt.cloneProduct = '1';
		//Test.setCurrentPageReference(new PageReference('Page.OpportunityClone')); 
		//System.currentPageReference().getParameters().put('cProds','1');
		testExt.save();
		pref.getParameters().put('cProds','');
		ApexPages.StandardController std1 = new ApexPages.StandardController(oppty);
		OpportunityCloneExt testExt1 = new OpportunityCloneExt(std1);

		Test.stopTest();
		//OpportunityLayoutAPIHelper helperRef = new OpportunityLayoutAPIHelper();

	}
	
}