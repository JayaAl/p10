public with sharing class OpportunityCloneExt_V1 {

	ApexPages.StandardController stdctrl;
	public Schema.FieldSet fieldset;
	Public list<string> fieldAPIs{get;set;}
	Public Opportunity Opp{get;set;}

	
	/*
       The fieldsets are retreived by the record type.
       The fields set names should match the record type developer names (APIs)
	*/
	public OpportunityCloneExt_V1(ApexPages.StandardController apsc){
     
     Id OppId=apsc.getId();
     Opp= [select id,name,RecordType.name from opportunity where Id=:OppId];    
     //fieldset= getOpportunityFieldSets('OPT_CloneProgrammatic');//Opp.RecordType.name
     List<Schema.FieldSetMember> fieldsetMemList= getOpportunityFieldsForRecordType('OPT_CloneInsideSales');
     fieldAPIs= getFieldAPIList(fieldsetMemList);
     apsc.addFields(fieldAPIs);
     //Opp= getOpportunity(fieldAPIs);
	}
	
	public pageReference save() 
	{
		//Code for Clone.
        
		return null;
	}

	public pageReference Cancel()
	{

		return null;
	}

	public List<Schema.FieldSetMember>  getOpportunityFieldsForRecordType(String name)
	{
		Schema.DescribeSObjectResult d =  Opportunity.sObjectType.getDescribe();
        Map<String, Schema.FieldSet> FsMap = d.fieldSets.getMap();

        system.debug(FsMap.get(name));

        return FsMap.get(name).getFields();


	}

	public Opportunity getOpportunity(List<string> fieldAPIList)
	{
		String query = 'SELECT ';
        string str= string.join(fieldAPIList,',');
        str=' '+str+' ';
        query += 'AccountId,Id, Name FROM Opportunity LIMIT 1';
        return Database.query(query);

	}

	public list<string> getFieldAPIList(List<Schema.FieldSetMember> fieldsetMemList)
	{
     list<string> strlist= new list<string>();
     for(Schema.FieldSetMember f : fieldsetMemList) {
            strlist.add(f.getFieldPath()); 
        }

       return  strlist;

	}
}
/*
Opportunity RecordTypes and API names.
Name	      DeveloperName
Opportunity - Audio	Opportunity_Audio
Opportunity - Biz Dev	Opportunity_Biz_Dev
Opportunity - Channel Sales - Reseller	Opportunity_Channel_Sales
Opportunity - Inside/Local Full	Opportunity_Inside_Sales
Opportunity - Inside/Local Quick Create	Opportunity_simple_entry_Inside_Sales
Opportunity - Non-Revenue	Opportunity_Non_Revenue
Opportunity - Outside Sales	Opportunity_Premium
Opportunity - Pandora One Subs	Opportunity_Pandora_One_Subs
Opportunity - Performance	Opportunity_Performance
Opportunity - Programmatic	Opportunity_Programmatic
Opportunity - Simple Entry - Outside Sales	Opportunity_simple_entry_Outside_Sales


*/