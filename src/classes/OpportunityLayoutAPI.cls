/**
* @name				: OpportunityLayoutAPI
* @desc				: Parser for Opportunity Layout JSON
* @author			: Jaya Alaparthi
* @date[YYYY-MM-DD]	: 2017-08-02
* ─────────────────────────────────────────────────────────────────────────────────────────────────┐
* @modifiedBy     Jaya Alaparthi <JAlaparthi@pandora.com>
* @version        
* @modified       
* @changes        
* ─────────────────────────────────────────────────────────────────────────────────────────────────
* 
* ─────────────────────────────────────────────────────────────────────────────────────────────────┘
*/
public with sharing class OpportunityLayoutAPI {

		
	public static void consumeObject(JSONParser parser) {
		Integer depth = 0;
		do {
			JSONToken curr = parser.getCurrentToken();
			if (curr == JSONToken.START_OBJECT || 
				curr == JSONToken.START_ARRAY) {
				depth++;
			} else if (curr == JSONToken.END_OBJECT ||
				curr == JSONToken.END_ARRAY) {
				depth--;
			}
		} while (depth > 0 && parser.nextToken() != null);
	}
	public class LayoutSections {
		public Boolean customLabel {get;set;} 
		public Boolean detailHeading {get;set;} 
		public Boolean editHeading {get;set;} 
		public String label {get;set;} 
		public List<LayoutColumns> layoutColumns {get;set;} 
		public String style {get;set;} 

		public LayoutSections(JSONParser parser) {
			while (parser.nextToken() != JSONToken.END_OBJECT) {
				if (parser.getCurrentToken() == JSONToken.FIELD_NAME) {
					String text = parser.getText();
					if (parser.nextToken() != JSONToken.VALUE_NULL) {
						if (text == 'customLabel') {
							customLabel = parser.getBooleanValue();
						} else if (text == 'detailHeading') {
							detailHeading = parser.getBooleanValue();
						} else if (text == 'editHeading') {
							editHeading = parser.getBooleanValue();
						} else if (text == 'label') {
							label = parser.getText();
						} else if (text == 'layoutColumns') {
							layoutColumns = new List<LayoutColumns>();
							while (parser.nextToken() != JSONToken.END_ARRAY) {
								layoutColumns.add(new LayoutColumns(parser));
							}
						} else if (text == 'style') {
							style = parser.getText();
						} else {
							System.debug(LoggingLevel.WARN, 'LayoutSections consuming unrecognized property: '+text);
							consumeObject(parser);
						}
					}
				}
			}
		}
	}

	public class PageSections {
		public List<LayoutSections> layoutSections;

		public PageSections(JSONParser parser) {

			while (parser.nextToken() != JSONToken.END_OBJECT) {
				if (parser.getCurrentToken() == JSONToken.FIELD_NAME) {
					String text = parser.getText();
					if (parser.nextToken() != JSONToken.VALUE_NULL) {
						if (text == 'layoutSections') {
							layoutSections = new List<LayoutSections>();
							while (parser.nextToken() != JSONToken.END_ARRAY) {
								layoutSections.add(new LayoutSections(parser));
							}
						} else {
							System.debug(LoggingLevel.WARN, 'JSON2Apex consuming unrecognized property: '+text);
							consumeObject(parser);
						}
					}
				}
			}
		}
	}

	public class LayoutColumns {
		public List<LayoutItems> layoutItems;
		public String reserved;
		public LayoutColumns(JSONParser parser) {
			while (parser.nextToken() != JSONToken.END_OBJECT) {
				if (parser.getCurrentToken() == JSONToken.FIELD_NAME) {
					String text = parser.getText();
					if (parser.nextToken() != JSONToken.VALUE_NULL) {
						if (text == 'layoutItems') {
							layoutItems = new List<LayoutItems>();
							while (parser.nextToken() != JSONToken.END_ARRAY) {
								layoutItems.add(new LayoutItems(parser));
							}
						} else if (text == 'reserved') {
							reserved = parser.getText();
						} else {
							System.debug(LoggingLevel.WARN, 'LayoutColumns consuming unrecognized property: '+text);
							consumeObject(parser);
						}
					}
				}
			}
		}
	}

	

	public class LayoutItems {
		public String analyticsCloudComponent;
		public String behavior;
		public String canvas;
		public String component;
		public String customLink;
		public String emptySpace;
		public String field;
		public String height;
		public String page;
		public String reportChartComponent;
		public String scontrol;
		public Boolean showLabel;
		public Boolean showScrollbars;
		public String width;
		public LayoutItems(JSONParser parser) {
			while (parser.nextToken() != JSONToken.END_OBJECT) {
				if (parser.getCurrentToken() == JSONToken.FIELD_NAME) {
					String text = parser.getText();
					if (parser.nextToken() != JSONToken.VALUE_NULL) {
						if (text == 'analyticsCloudComponent') {
							analyticsCloudComponent = parser.getText();
						} else if (text == 'behavior') {
							behavior = parser.getText();
						} else if (text == 'canvas') {
							canvas = parser.getText();
						} else if (text == 'component') {
							component = parser.getText();
						} else if (text == 'customLink') {
							customLink = parser.getText();
						} else if (text == 'emptySpace') {
							emptySpace = parser.getText();
						} else if (text == 'field') {
							field = parser.getText();
						} else if (text == 'height') {
							height = parser.getText();
						} else if (text == 'page') {
							page = parser.getText();
						} else if (text == 'reportChartComponent') {
							reportChartComponent = parser.getText();
						} else if (text == 'scontrol') {
							scontrol = parser.getText();
						} else if (text == 'showLabel') {
							showLabel = parser.getBooleanValue();
						} else if (text == 'showScrollbars') {
							showScrollbars = parser.getBooleanValue();
						} else if (text == 'width') {
							width = parser.getText();
						} else {
							System.debug(LoggingLevel.WARN, 'LayoutItems consuming unrecognized property: '+text);
							consumeObject(parser);
						}
					}
				}
			}
		}
	}

	
	public static OpportunityLayoutAPI.PageSections parse(String json) {

		OpportunityLayoutAPI.PageSections ref;
		ref = (OpportunityLayoutAPI.PageSections) System.JSON.deserialize(json, PageSections.class);
		System.debug('OpportunityLayoutAPI:'+ref);
		return ref;
	}

}