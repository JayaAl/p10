/**
* ─────────────────────────────────────────────────────────────────────────────────────────────────┐
* Helper class for OpportuityCloneExt.cls
* ──────────────────────────────────────────────────────────────────────────────────────────────────
* @author         Jaya Alaparthi
* @maintainedBy   Jaya Alaparthi
* @version        1.0
* @created        2017-08-02
* @modified       
* @systemLayer    Service
* @see            ????
* @see            ????
* ──────────────────────────────────────────────────────────────────────────────────────────────────
* @changes
* v1.1            Jaya Alaparthi
* [YYYY-MM-DD]     Change Made
* ──────────────────────────────────────────────────────────────────────────────────────────────────
* @changes
* v1.2            Jaya Alaparthi
* [YYYY-MM-DD]    Changes Made
* ─────────────────────────────────────────────────────────────────────────────────────────────────┘
*/
public with sharing class OpportunityLayoutAPIHelper {
	


	public static list<PageGeneratorWrapper> metadataToDisplay(Id layoutId,
																List<Schema.FieldSetMember> listFSM) {

		list<PageGeneratorWrapper> pgList = new list<PageGeneratorWrapper>();
		String baseUrl = URL.getSalesforceBaseUrl().toExternalForm()+ '/services/data/v39.0/tooling/';
		Http http = new HTTP();
		HttpRequest req = new HttpRequest();
		req.setEndpoint(baseUrl+'query?q=Select+Id,MetaData,fullName+From+Layout+WHERE+Id=\''+layoutId+'\'');
		req.setMethod('GET');
		
		System.debug('sessionId:'+UserInfo.getSessionId());
    	req.setHeader('Authorization', 'OAuth ' + UserInfo.getSessionId());
    	req.setHeader('Content-Type', 'application/json');
    	
		System.debug('req:'+req);
		HttpResponse resp = http.send(req);
		System.debug('resp:'+resp);
		System.debug('resp:'+resp.getBody());
		String responseBody = resp.getBody();
		System.debug('responseBody:'+responseBody);
		//String tempResp = responseBody.replace('"', '');
		//System.debug('tempResp:'+tempResp);
		System.debug('Index:'+responseBody.indexOf('layoutSections'));
		System.debug('Index:'+responseBody.indexOf('miniLayout'));
		String subStr = responseBody.substring(responseBody.indexOf('layoutSections'), 
						responseBody.indexOf('miniLayout')-2);

		//List<OpportunityLayoutAPI.LayoutSections> parsedResp = OpportunityLayoutAPI.customParse(resp.getBody());
		// at home   
		String respBodySubText = '{"'+subStr+'}';
		System.debug('respBodySubText:'+respBodySubText);
		
		OpportunityLayoutAPI.PageSections parsedResp = OpportunityLayoutAPI.parse(respBodySubText);
		System.debug('parsedResp:'+parsedResp);
		pgList = convertParsedToWrapper(parsedResp,listFSM);
		System.debug('pgList:'+pgList);
		return pgList;
	}
	private static list<PageGeneratorWrapper> convertParsedToWrapper(OpportunityLayoutAPI.PageSections parsedResp,
																	List<Schema.FieldSetMember> listFSM){
	/*private static list<PageGeneratorWrapper> convertParsedToWrapper(List<OpportunityLayoutAPI.LayoutSections> parsedResp,
																	List<Schema.FieldSetMember> listFSM) {*/

		list<PageGeneratorWrapper> pageGenWrapper = new list<PageGeneratorWrapper>();

		System.debug('parsedResp:'+parsedResp+'listFSM:'+listFSM);
		for(OpportunityLayoutAPI.LayoutSections layoutSec: 
											parsedResp.layoutSections) {
		//for(OpportunityLayoutAPI.LayoutSections layoutSec: parsedResp) {
			
			System.debug('layoutSection label:'+layoutSec.label);

			list<PageGeneratorWrapper.LayoutColumDetails> colTemp1 = new list<PageGeneratorWrapper.LayoutColumDetails>();
			//list<PageGeneratorWrapper.LayoutColumDetails> colTemp2 = new list<PageGeneratorWrapper.LayoutColumDetails>();
			list<PageGeneratorWrapper.LayoutColumDetails> col = new list<PageGeneratorWrapper.LayoutColumDetails>();
			PageGeneratorWrapper tempWrapper = new PageGeneratorWrapper();
			Set<String> setOfallowedFields = new Set<String>();

			// get field apis from fieldsets
			setOfallowedFields = OPT_CloneUtil.fieldAPIsFromFieldSets(listFSM);
			tempWrapper.sectionHeader = layoutSec.label;
			System.debug('setOfallowedFields:'+setOfallowedFields);
			if(layoutSec.layoutColumns.size() >1) {

				// create two seperate list of columns 
				System.debug('layoutSec.layoutColumns[0].layoutItems:'+layoutSec.layoutColumns[0].layoutItems
						+'layoutSec.layoutColumns[1].layoutItems:'+layoutSec.layoutColumns[1].layoutItems);

			 
				//passing both layout column at the sames time to merge them.
				colTemp1 = getColumDetails(layoutSec.layoutColumns[0].layoutItems,
										layoutSec.layoutColumns[1].layoutItems,
										setOfallowedFields);
				
				// only one layout column at a time.
				//colTemp1 = getColumDetails(layoutSec.layoutColumns[0].layoutItems,
				//							setOfallowedFields);
				// this is for both 
				tempWrapper.layoutColumn1 = colTemp1;
				// for merged columns
				if(colTemp1.size() >= 1) {
					pageGenWrapper.add(tempWrapper);
				}
			}
			
			// only one layout column at a time.
			/*if(layoutSec.layoutColumns.size() >1){
				
				colTemp2 = getColumDetails(layoutSec.layoutColumns[1].layoutItems,
											setOfallowedFields);
				tempWrapper.layoutColumn2 = colTemp2;
			}
			if(colTemp1.size() >= 1 || colTemp2.size() >= 1) {
				pageGenWrapper.add(tempWrapper);
			}*/

			// end of one column at a time
			
			System.debug('layoutSection Columns:'+tempWrapper.layoutColumn1);
		}
		return pageGenWrapper;
		
	}
// thi is for lightning purpose it takes only one layout column a a time and converts it
// to a wrapper format.
	/*private static list<PageGeneratorWrapper.LayoutColumDetails> getColumDetails(list<OpportunityLayoutAPI.LayoutItems> layoutItemsCol,
																				Set<String> setOfallowedFields){
		
		list<PageGeneratorWrapper.LayoutColumDetails> columnDetails = new list<PageGeneratorWrapper.LayoutColumDetails>();
		System.debug('colm Details:'+layoutItemsCol);
		for(OpportunityLayoutAPI.LayoutItems item: layoutItemsCol) {

			PageGeneratorWrapper.LayoutColumDetails temp = new PageGeneratorWrapper.LayoutColumDetails();
			//if(setOfallowedFields.contains(item.field)) {
				System.debug('item:'+item);
				temp.fieldAPI = item.field;
				temp.fieldBehavior = item.behavior;
				columnDetails.add(temp);
			//}
		}
		System.debug('columnDetails----'+columnDetails);
		return columnDetails;
	}*/

// this method takes two layout columns at a time to merge the columns in wrapper format.
	private static list<PageGeneratorWrapper.LayoutColumDetails> getColumDetails(list<OpportunityLayoutAPI.LayoutItems> layoutItemsCol1,
																				list<OpportunityLayoutAPI.LayoutItems> layoutItemsCol2,
																				Set<String> setOfallowedFields) {


		list<PageGeneratorWrapper.LayoutColumDetails> tempMergeCol = new list<PageGeneratorWrapper.LayoutColumDetails>();
		// get Opportunity field Labels with field api as key map
		Map<String,String> opptyFieldAPILabelMap = new Map<String,String>();

		opptyFieldAPILabelMap = getFieldAPILabelMap('Opportunity');
		Integer maxColumnSize = layoutItemsCol1.size() > layoutItemsCol2.size()
								? (layoutItemsCol1.size() >0 
									? layoutItemsCol1.size()
									: 0) 
								: (layoutItemsCol2.size() >0 
									? layoutItemsCol2.size()
									: 0);
		for(Integer i = 0; i< maxColumnSize ;i++) {

			// get column1 item
			if(layoutItemsCol1.size() > i && layoutItemsCol1.get(i) != null) {

				PageGeneratorWrapper.LayoutColumDetails temp;
				OpportunityLayoutAPI.LayoutItems item = layoutItemsCol1.get(i);
				temp = getFieldAPIInWrapper(item,opptyFieldAPILabelMap);
				if(temp != null) {
					tempMergeCol.add(temp);
				}
			}
			// get column2 item
			if(layoutItemsCol2.size() > i && layoutItemsCol2.get(i) != null) {

				PageGeneratorWrapper.LayoutColumDetails temp;
				OpportunityLayoutAPI.LayoutItems item = layoutItemsCol2.get(i);
				temp = getFieldAPIInWrapper(item,opptyFieldAPILabelMap);
				if(temp != null) {
					tempMergeCol.add(temp);
				}
			}
		}
		return tempMergeCol;
	}


	private static PageGeneratorWrapper.LayoutColumDetails getFieldAPIInWrapper(
							OpportunityLayoutAPI.LayoutItems item, Map<String,String> opptyFieldAPILabelMap) {

		PageGeneratorWrapper.LayoutColumDetails temp ;
		// get opportnity field labels

		String fieldAPI = item.field != null ? item.field : '';
		System.debug('fieldAPI#########:'+fieldAPI.toLowerCase());
		String fieldLabel = opptyFieldAPILabelMap.containsKey(fieldAPI.toLowerCase()) 
							? opptyFieldAPILabelMap.get(fieldAPI.toLowerCase())
							: '';
		if(!String.isBlank(fieldAPI) 
			&& (item.behavior == OpptyPageGenUtil.BEHAVIOR_EDIT
				|| item.behavior == OpptyPageGenUtil.BEHAVIOR_REQUIRED
				|| item.behavior == OpptyPageGenUtil.BEHAVIOR_READONLY)) {

			temp = new PageGeneratorWrapper.LayoutColumDetails();
			temp.fieldAPI = item.field;
			temp.fieldBehavior = item.behavior;
			temp.fieldLabel = fieldLabel;
		} else {
			temp = new PageGeneratorWrapper.LayoutColumDetails();
			 temp.fieldAPI = '';
			 temp.fieldBehavior = 'blank';
			 temp.fieldLabel = '';
		}
		return temp;
	}	

	private static Map<String,String> getFieldAPILabelMap(String sObj) {

		Map<String,String> fieldAPILabelMap = new Map<String,String>();
		Map<String, Schema.SObjectType> schemaMap = Schema.getGlobalDescribe();
        Schema.SObjectType ObjectSchema = schemaMap.get(sObj);
        Map<String, Schema.SObjectField> fieldMap = ObjectSchema.getDescribe().fields.getMap();

       // List<SelectOption> fieldNames = new List<SelectOption>();
        for (String fieldName: fieldMap.keySet()) {

	        String fieldLabel = fieldMap.get(fieldName).getDescribe().getLabel();
	        fieldLabel = fieldLabel.endsWith('ID') ? fieldLabel.replace('ID','Name') : fieldLabel;
	        fieldLabel = fieldLabel.equalsIgnoreCase('name') ? 'Opportunity Name' : fieldLabel;
	        fieldAPILabelMap.put(fieldName,fieldLabel);
	        System.debug('fieldName:'+fieldName);
        }
        return fieldAPILabelMap;
	}
}