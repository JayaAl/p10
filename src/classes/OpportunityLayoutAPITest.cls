/**
* @name             : OpportunityLayoutAPI
* @desc             : Parser for Opportunity Layout JSON
* @author           : Jaya Alaparthi
* @date[YYYY-MM-DD] : 2017-08-02
* ─────────────────────────────────────────────────────────────────────────────────────────────────┐
* @modifiedBy     Jaya Alaparthi <JAlaparthi@pandora.com>
* @version        
* @modified       
* @changes        
* ─────────────────────────────────────────────────────────────────────────────────────────────────
* 
* ─────────────────────────────────────────────────────────────────────────────────────────────────┘
*/
@isTest
public class OpportunityLayoutAPITest {
    
    // This test method should give 100% coverage
    static testMethod void testParserWithProperData() {

        String json = '{\"layoutSections\" : [ { \"customLabel\" : true, \"detailHeading\" : false, \"editHeading\" : true, \"label\" : \"Opportunity Information\", '+
        '\"layoutColumns\" : [ { \"layoutItems\" : [ '+
        '{ \"analyticsCloudComponent\" : null, '+
        '\"behavior\" : \"Required\", '+
        '\"canvas\" : null, '+
        '\"component\" : null, '+
        '\"customLink\" : null, '+
        '\"emptySpace\" : null, '+
        '\"field\" : \"Name\", '+
        '\"height\" : null, '+
        '\"page\" : null, '+
        '\"reportChartComponent\" : null, '+
        '\"scontrol\" : null, '+
        '\"showLabel\" : null, '+
        '\"showScrollbars\" : null, '+
        '\"width\" : null }, { \"analyticsCloudComponent\" : null, \"behavior\" : \"Required\", \"canvas\" : null, \"component\" : null, \"customLink\" : null, \"emptySpace\" : null, \"field\" : \"AccountId\", \"height\" : null, \"page\" : null, \"reportChartComponent\" : null, \"scontrol\" : null, \"showLabel\" : null, \"showScrollbars\" : null, \"width\" : null }, { \"analyticsCloudComponent\" : null, \"behavior\" : \"Edit\", \"canvas\" : null, \"component\" : null, \"customLink\" : null, \"emptySpace\" : null, \"field\" : \"Agency__c\", \"height\" : null, \"page\" : null, \"reportChartComponent\" : null, \"scontrol\" : null, \"showLabel\" : null, \"showScrollbars\" : null, \"width\" : null }, { \"analyticsCloudComponent\" : null, \"behavior\" : \"Edit\", \"canvas\" : null, \"component\" : null, \"customLink\" : null, \"emptySpace\" : null, \"field\" : \"Account_Development_Specialist__c\", \"height\" : null, \"page\" : null, \"reportChartComponent\" : null, \"scontrol\" : null, \"showLabel\" : null, \"showScrollbars\" : null, \"width\" : null }, { \"analyticsCloudComponent\" : null, \"behavior\" : \"Edit\", \"canvas\" : null, \"component\" : null, \"customLink\" : null, \"emptySpace\" : null, \"field\" : \"Confirm_direct_relationship__c\", \"height\" : null, \"page\" : null, \"reportChartComponent\" : null, \"scontrol\" : null, \"showLabel\" : null, \"showScrollbars\" : null, \"width\" : null }, { \"analyticsCloudComponent\" : null, \"behavior\" : \"Readonly\", \"canvas\" : null, \"component\" : null, \"customLink\" : null, \"emptySpace\" : null, \"field\" : \"Probability\", \"height\" : null, \"page\" : null, \"reportChartComponent\" : null, \"scontrol\" : null, \"showLabel\" : null, \"showScrollbars\" : null, \"width\" : null }, { \"analyticsCloudComponent\" : null, \"behavior\" : \"Required\", \"canvas\" : null, \"component\" : null, \"customLink\" : null, \"emptySpace\" : null, \"field\" : \"Industry_Category__c\", \"height\" : null, \"page\" : null, \"reportChartComponent\" : null, \"scontrol\" : null, \"showLabel\" : null, \"showScrollbars\" : null, \"width\" : null }, { \"analyticsCloudComponent\" : null, \"behavior\" : \"Required\", \"canvas\" : null, \"component\" : null, \"customLink\" : null, \"emptySpace\" : null, \"field\" : \"Industry_Sub_Category__c\", \"height\" : null, \"page\" : null, \"reportChartComponent\" : null, \"scontrol\" : null, \"showLabel\" : null, \"showScrollbars\" : null, \"width\" : null }, { \"analyticsCloudComponent\" : null, \"behavior\" : \"Edit\", \"canvas\" : null, \"component\" : null, \"customLink\" : null, \"emptySpace\" : null, \"field\" : \"ATG_Campaign_Status__c\", \"height\" : null, \"page\" : null, \"reportChartComponent\" : null, \"scontrol\" : null, \"showLabel\" : null, \"showScrollbars\" : null, \"width\" : null }, { \"analyticsCloudComponent\" : null, \"behavior\" : \"Edit\", \"canvas\" : null, \"component\" : null, \"customLink\" : null, \"emptySpace\" : null, \"field\" : \"ATG_Detailed_Status__c\", \"height\" : null, \"page\" : null, \"reportChartComponent\" : null, \"scontrol\" : null, \"showLabel\" : null, \"showScrollbars\" : null, \"width\" : null } ], \"reserved\" : null }, { \"layoutItems\" : [ { \"analyticsCloudComponent\" : null, \"behavior\" : \"Required\", \"canvas\" : null, \"component\" : null, \"customLink\" : null, \"emptySpace\" : null, \"field\" : \"Amount\", \"height\" : null, \"page\" : null, \"reportChartComponent\" : null, \"scontrol\" : null, \"showLabel\" : null, \"showScrollbars\" : null, \"width\" : null }, { \"analyticsCloudComponent\" : null, \"behavior\" : \"Required\", \"canvas\" : null, \"component\" : null, \"customLink\" : null, \"emptySpace\" : null, \"field\" : \"ContractStartDate__c\", \"height\" : null, \"page\" : null, \"reportChartComponent\" : null, \"scontrol\" : null, \"showLabel\" : null, \"showScrollbars\" : null, \"width\" : null }, { \"analyticsCloudComponent\" : null, \"behavior\" : \"Required\", \"canvas\" : null, \"component\" : null, \"customLink\" : null, \"emptySpace\" : null, \"field\" : \"ContractEndDate__c\", \"height\" : null, \"page\" : null, \"reportChartComponent\" : null, \"scontrol\" : null, \"showLabel\" : null, \"showScrollbars\" : null, \"width\" : null }, { \"analyticsCloudComponent\" : null, \"behavior\" : \"Required\", \"canvas\" : null, \"component\" : null, \"customLink\" : null, \"emptySpace\" : null, \"field\" : \"CloseDate\", \"height\" : null, \"page\" : null, \"reportChartComponent\" : null, \"scontrol\" : null, \"showLabel\" : null, \"showScrollbars\" : null, \"width\" : null }, { \"analyticsCloudComponent\" : null, \"behavior\" : \"Required\", \"canvas\" : null, \"component\" : null, \"customLink\" : null, \"emptySpace\" : null, \"field\" : \"StageName\", \"height\" : null, \"page\" : null, \"reportChartComponent\" : null, \"scontrol\" : null, \"showLabel\" : null, \"showScrollbars\" : null, \"width\" : null }, { \"analyticsCloudComponent\" : null, \"behavior\" : \"Required\", \"canvas\" : null, \"component\" : null, \"customLink\" : null, \"emptySpace\" : null, \"field\" : \"Sales_Forecast__c\", \"height\" : null, \"page\" : null, \"reportChartComponent\" : null, \"scontrol\" : null, \"showLabel\" : null, \"showScrollbars\" : null, \"width\" : null }, { \"analyticsCloudComponent\" : null, \"behavior\" : \"Required\", \"canvas\" : null, \"component\" : null, \"customLink\" : null, \"emptySpace\" : null, \"field\" : \"Budget_Source__c\", \"height\" : null, \"page\" : null, \"reportChartComponent\" : null, \"scontrol\" : null, \"showLabel\" : null, \"showScrollbars\" : null, \"width\" : null }, { \"analyticsCloudComponent\" : null, \"behavior\" : \"Required\", \"canvas\" : null, \"component\" : null, \"customLink\" : null, \"emptySpace\" : null, \"field\" : \"Radio_Type__c\", \"height\" : null, \"page\" : null, \"reportChartComponent\" : null, \"scontrol\" : null, \"showLabel\" : null, \"showScrollbars\" : null, \"width\" : null } ], \"reserved\" : null } ], \"style\" : \"TwoColumnsTopToBottom\" }, { \"customLabel\" : true, \"detailHeading\" : false, \"editHeading\" : false, \"label\" : \"Order Management System\", \"layoutColumns\" : [ { \"layoutItems\" : [ ], \"reserved\" : null }, { \"layoutItems\" : [ ], \"reserved\" : null }, { \"layoutItems\" : [ ], \"reserved\" : null } ], \"style\" : \"CustomLinks\" } ]}';
        OpportunityLayoutAPI.PageSections properIP = OpportunityLayoutAPI.parse(json);
        System.assert(properIP != null);

         json = ' { \"analyticsCloudComponent\": null,'+
                '\"behavior\" :\"Required\",\"canvas\" : null,'+
                '\"component\" : null,\"customLink\" : null,\"emptySpace\" : null,\"field\" : \"Name\",'+
                '\"height\" : null, \"page\" : null,\"reportChartComponent\" : null,\"scontrol\" : null,'+
                '\"showLabel\" : null,\"showScrollbars\" : null,\"width\" : null}';

        OpportunityLayoutAPI.LayoutItems layoutItems = new OpportunityLayoutAPI.LayoutItems(System.JSON.createParser(json));
        System.assert(layoutItems != null);
        System.assert(layoutItems.analyticsCloudComponent == null);
        System.assert(layoutItems.behavior == 'Required');
        System.assert(layoutItems.canvas == null);
        System.assert(layoutItems.component == null);
        System.assert(layoutItems.customLink == null);
        System.assert(layoutItems.emptySpace == null);
        System.assert(layoutItems.field == 'Name');
        System.assert(layoutItems.height == null);
        System.assert(layoutItems.page == null);
        System.assert(layoutItems.reportChartComponent == null);
        System.assert(layoutItems.scontrol == null);
        System.assert(layoutItems.showLabel == null);
        System.assert(layoutItems.showScrollbars == null);
        System.assert(layoutItems.width == null);

        json = '{\"layoutSections\" : [ { \"customLabel\" : true, \"detailHeading\" : false, \"editHeading\" : true, \"label\" : \"Opportunity Information\", '+
        '\"layoutColumns\" : [ { \"layoutItems\" : [ '+
        '{ \"analyticsCloudComponent\" : null, '+
        '\"behavior\" : \"Required\", '+
        '\"canvas\" : null, '+
        '\"component\" : null, '+
        '\"customLink\" : null, '+
        '\"emptySpace\" : null, '+
        '\"field\" : \"Name\", '+
        '\"height\" : null, '+
        '\"page\" : null, '+
        '\"reportChartComponent\" : null, '+
        '\"scontrol\" : null, '+
        '\"showLabel\" : null, '+
        '\"showScrollbars\" : null, '+
        '\"width\" : null }, { \"analyticsCloudComponent\" : null, \"behavior\" : \"Required\", \"canvas\" : null, \"component\" : null, \"customLink\" : null, \"emptySpace\" : null, \"field\" : \"AccountId\", \"height\" : null, \"page\" : null, \"reportChartComponent\" : null, \"scontrol\" : null, \"showLabel\" : null, \"showScrollbars\" : null, \"width\" : null }, { \"analyticsCloudComponent\" : null, \"behavior\" : \"Edit\", \"canvas\" : null, \"component\" : null, \"customLink\" : null, \"emptySpace\" : null, \"field\" : \"Agency__c\", \"height\" : null, \"page\" : null, \"reportChartComponent\" : null, \"scontrol\" : null, \"showLabel\" : null, \"showScrollbars\" : null, \"width\" : null }, { \"analyticsCloudComponent\" : null, \"behavior\" : \"Edit\", \"canvas\" : null, \"component\" : null, \"customLink\" : null, \"emptySpace\" : null, \"field\" : \"Account_Development_Specialist__c\", \"height\" : null, \"page\" : null, \"reportChartComponent\" : null, \"scontrol\" : null, \"showLabel\" : null, \"showScrollbars\" : null, \"width\" : null }, { \"analyticsCloudComponent\" : null, \"behavior\" : \"Edit\", \"canvas\" : null, \"component\" : null, \"customLink\" : null, \"emptySpace\" : null, \"field\" : \"Confirm_direct_relationship__c\", \"height\" : null, \"page\" : null, \"reportChartComponent\" : null, \"scontrol\" : null, \"showLabel\" : null, \"showScrollbars\" : null, \"width\" : null }, { \"analyticsCloudComponent\" : null, \"behavior\" : \"Readonly\", \"canvas\" : null, \"component\" : null, \"customLink\" : null, \"emptySpace\" : null, \"field\" : \"Probability\", \"height\" : null, \"page\" : null, \"reportChartComponent\" : null, \"scontrol\" : null, \"showLabel\" : null, \"showScrollbars\" : null, \"width\" : null }, { \"analyticsCloudComponent\" : null, \"behavior\" : \"Required\", \"canvas\" : null, \"component\" : null, \"customLink\" : null, \"emptySpace\" : null, \"field\" : \"Industry_Category__c\", \"height\" : null, \"page\" : null, \"reportChartComponent\" : null, \"scontrol\" : null, \"showLabel\" : null, \"showScrollbars\" : null, \"width\" : null }, { \"analyticsCloudComponent\" : null, \"behavior\" : \"Required\", \"canvas\" : null, \"component\" : null, \"customLink\" : null, \"emptySpace\" : null, \"field\" : \"Industry_Sub_Category__c\", \"height\" : null, \"page\" : null, \"reportChartComponent\" : null, \"scontrol\" : null, \"showLabel\" : null, \"showScrollbars\" : null, \"width\" : null }, { \"analyticsCloudComponent\" : null, \"behavior\" : \"Edit\", \"canvas\" : null, \"component\" : null, \"customLink\" : null, \"emptySpace\" : null, \"field\" : \"ATG_Campaign_Status__c\", \"height\" : null, \"page\" : null, \"reportChartComponent\" : null, \"scontrol\" : null, \"showLabel\" : null, \"showScrollbars\" : null, \"width\" : null }, { \"analyticsCloudComponent\" : null, \"behavior\" : \"Edit\", \"canvas\" : null, \"component\" : null, \"customLink\" : null, \"emptySpace\" : null, \"field\" : \"ATG_Detailed_Status__c\", \"height\" : null, \"page\" : null, \"reportChartComponent\" : null, \"scontrol\" : null, \"showLabel\" : null, \"showScrollbars\" : null, \"width\" : null } ], \"reserved\" : null }, { \"layoutItems\" : [ { \"analyticsCloudComponent\" : null, \"behavior\" : \"Required\", \"canvas\" : null, \"component\" : null, \"customLink\" : null, \"emptySpace\" : null, \"field\" : \"Amount\", \"height\" : null, \"page\" : null, \"reportChartComponent\" : null, \"scontrol\" : null, \"showLabel\" : null, \"showScrollbars\" : null, \"width\" : null }, { \"analyticsCloudComponent\" : null, \"behavior\" : \"Required\", \"canvas\" : null, \"component\" : null, \"customLink\" : null, \"emptySpace\" : null, \"field\" : \"ContractStartDate__c\", \"height\" : null, \"page\" : null, \"reportChartComponent\" : null, \"scontrol\" : null, \"showLabel\" : null, \"showScrollbars\" : null, \"width\" : null }, { \"analyticsCloudComponent\" : null, \"behavior\" : \"Required\", \"canvas\" : null, \"component\" : null, \"customLink\" : null, \"emptySpace\" : null, \"field\" : \"ContractEndDate__c\", \"height\" : null, \"page\" : null, \"reportChartComponent\" : null, \"scontrol\" : null, \"showLabel\" : null, \"showScrollbars\" : null, \"width\" : null }, { \"analyticsCloudComponent\" : null, \"behavior\" : \"Required\", \"canvas\" : null, \"component\" : null, \"customLink\" : null, \"emptySpace\" : null, \"field\" : \"CloseDate\", \"height\" : null, \"page\" : null, \"reportChartComponent\" : null, \"scontrol\" : null, \"showLabel\" : null, \"showScrollbars\" : null, \"width\" : null }, { \"analyticsCloudComponent\" : null, \"behavior\" : \"Required\", \"canvas\" : null, \"component\" : null, \"customLink\" : null, \"emptySpace\" : null, \"field\" : \"StageName\", \"height\" : null, \"page\" : null, \"reportChartComponent\" : null, \"scontrol\" : null, \"showLabel\" : null, \"showScrollbars\" : null, \"width\" : null }, { \"analyticsCloudComponent\" : null, \"behavior\" : \"Required\", \"canvas\" : null, \"component\" : null, \"customLink\" : null, \"emptySpace\" : null, \"field\" : \"Sales_Forecast__c\", \"height\" : null, \"page\" : null, \"reportChartComponent\" : null, \"scontrol\" : null, \"showLabel\" : null, \"showScrollbars\" : null, \"width\" : null }, { \"analyticsCloudComponent\" : null, \"behavior\" : \"Required\", \"canvas\" : null, \"component\" : null, \"customLink\" : null, \"emptySpace\" : null, \"field\" : \"Budget_Source__c\", \"height\" : null, \"page\" : null, \"reportChartComponent\" : null, \"scontrol\" : null, \"showLabel\" : null, \"showScrollbars\" : null, \"width\" : null }, { \"analyticsCloudComponent\" : null, \"behavior\" : \"Required\", \"canvas\" : null, \"component\" : null, \"customLink\" : null, \"emptySpace\" : null, \"field\" : \"Radio_Type__c\", \"height\" : null, \"page\" : null, \"reportChartComponent\" : null, \"scontrol\" : null, \"showLabel\" : null, \"showScrollbars\" : null, \"width\" : null } ], \"reserved\" : null } ], \"style\" : \"TwoColumnsTopToBottom\" }, { \"customLabel\" : true, \"detailHeading\" : false, \"editHeading\" : false, \"label\" : \"Order Management System\", \"layoutColumns\" : [ { \"layoutItems\" : [ ], \"reserved\" : null }, { \"layoutItems\" : [ ], \"reserved\" : null }, { \"layoutItems\" : [ ], \"reserved\" : null } ], \"style\" : \"CustomLinks\" } ]}';
        // layout sections
        
        OpportunityLayoutAPI.PageSections objLayoutPgSections = new OpportunityLayoutAPI.PageSections(System.JSON.createParser(json));
        System.assert(objLayoutPgSections != null);
        /*System.assert(objLayoutSections.customLabel);
        System.assert(!objLayoutSections.detailHeading);
        System.assert(objLayoutSections.editHeading);
        System.assert(objLayoutSections.label == 'Opportunity Information');
        System.assert(objLayoutSections.layoutColumns != null);
        System.assert(objLayoutSections.style == 'TwoColumnsTopToBottom');*/

    }
    static testMethod void testParserWithDummyData() {
        String json = '{\"layoutSections\" : [ { \"customLabel\" : true, \"detailHeading\" : false, \"editHeading\" : true, \"label\" : \"Opportunity Information\", '+
        '\"layoutColumns\" : [ { \"layoutItems\" : [ '+
        '{ \"analyticsCloudComponent\" : null, '+
        '\"behavior\" : \"Required\", '+
        '\"canvas\" : null, '+
        '\"component\" : null, '+
        '\"customLink\" : null, '+
        '\"emptySpace\" : null, '+
        '\"field\" : \"Name\", '+
        '\"height\" : null, '+
        '\"page\" : null, '+
        '\"reportChartComponent\" : null, '+
        '\"scontrol\" : null, '+
        '\"showLabel\" : null, '+
        '\"showScrollbars\" : null, '+
        '\"width\" : null }, { \"analyticsCloudComponent\" : null, \"behavior\" : \"Required\", \"canvas\" : null, \"component\" : null, \"customLink\" : null, \"emptySpace\" : null, \"field\" : \"AccountId\", \"height\" : null, \"page\" : null, \"reportChartComponent\" : null, \"scontrol\" : null, \"showLabel\" : null, \"showScrollbars\" : null, \"width\" : null }, { \"analyticsCloudComponent\" : null, \"behavior\" : \"Edit\", \"canvas\" : null, \"component\" : null, \"customLink\" : null, \"emptySpace\" : null, \"field\" : \"Agency__c\", \"height\" : null, \"page\" : null, \"reportChartComponent\" : null, \"scontrol\" : null, \"showLabel\" : null, \"showScrollbars\" : null, \"width\" : null }, { \"analyticsCloudComponent\" : null, \"behavior\" : \"Edit\", \"canvas\" : null, \"component\" : null, \"customLink\" : null, \"emptySpace\" : null, \"field\" : \"Account_Development_Specialist__c\", \"height\" : null, \"page\" : null, \"reportChartComponent\" : null, \"scontrol\" : null, \"showLabel\" : null, \"showScrollbars\" : null, \"width\" : null }, { \"analyticsCloudComponent\" : null, \"behavior\" : \"Edit\", \"canvas\" : null, \"component\" : null, \"customLink\" : null, \"emptySpace\" : null, \"field\" : \"Confirm_direct_relationship__c\", \"height\" : null, \"page\" : null, \"reportChartComponent\" : null, \"scontrol\" : null, \"showLabel\" : null, \"showScrollbars\" : null, \"width\" : null }, { \"analyticsCloudComponent\" : null, \"behavior\" : \"Readonly\", \"canvas\" : null, \"component\" : null, \"customLink\" : null, \"emptySpace\" : null, \"field\" : \"Probability\", \"height\" : null, \"page\" : null, \"reportChartComponent\" : null, \"scontrol\" : null, \"showLabel\" : null, \"showScrollbars\" : null, \"width\" : null }, { \"analyticsCloudComponent\" : null, \"behavior\" : \"Required\", \"canvas\" : null, \"component\" : null, \"customLink\" : null, \"emptySpace\" : null, \"field\" : \"Industry_Category__c\", \"height\" : null, \"page\" : null, \"reportChartComponent\" : null, \"scontrol\" : null, \"showLabel\" : null, \"showScrollbars\" : null, \"width\" : null }, { \"analyticsCloudComponent\" : null, \"behavior\" : \"Required\", \"canvas\" : null, \"component\" : null, \"customLink\" : null, \"emptySpace\" : null, \"field\" : \"Industry_Sub_Category__c\", \"height\" : null, \"page\" : null, \"reportChartComponent\" : null, \"scontrol\" : null, \"showLabel\" : null, \"showScrollbars\" : null, \"width\" : null }, { \"analyticsCloudComponent\" : null, \"behavior\" : \"Edit\", \"canvas\" : null, \"component\" : null, \"customLink\" : null, \"emptySpace\" : null, \"field\" : \"ATG_Campaign_Status__c\", \"height\" : null, \"page\" : null, \"reportChartComponent\" : null, \"scontrol\" : null, \"showLabel\" : null, \"showScrollbars\" : null, \"width\" : null }, { \"analyticsCloudComponent\" : null, \"behavior\" : \"Edit\", \"canvas\" : null, \"component\" : null, \"customLink\" : null, \"emptySpace\" : null, \"field\" : \"ATG_Detailed_Status__c\", \"height\" : null, \"page\" : null, \"reportChartComponent\" : null, \"scontrol\" : null, \"showLabel\" : null, \"showScrollbars\" : null, \"width\" : null } ], \"reserved\" : null }, { \"layoutItems\" : [ { \"analyticsCloudComponent\" : null, \"behavior\" : \"Required\", \"canvas\" : null, \"component\" : null, \"customLink\" : null, \"emptySpace\" : null, \"field\" : \"Amount\", \"height\" : null, \"page\" : null, \"reportChartComponent\" : null, \"scontrol\" : null, \"showLabel\" : null, \"showScrollbars\" : null, \"width\" : null }, { \"analyticsCloudComponent\" : null, \"behavior\" : \"Required\", \"canvas\" : null, \"component\" : null, \"customLink\" : null, \"emptySpace\" : null, \"field\" : \"ContractStartDate__c\", \"height\" : null, \"page\" : null, \"reportChartComponent\" : null, \"scontrol\" : null, \"showLabel\" : null, \"showScrollbars\" : null, \"width\" : null }, { \"analyticsCloudComponent\" : null, \"behavior\" : \"Required\", \"canvas\" : null, \"component\" : null, \"customLink\" : null, \"emptySpace\" : null, \"field\" : \"ContractEndDate__c\", \"height\" : null, \"page\" : null, \"reportChartComponent\" : null, \"scontrol\" : null, \"showLabel\" : null, \"showScrollbars\" : null, \"width\" : null }, { \"analyticsCloudComponent\" : null, \"behavior\" : \"Required\", \"canvas\" : null, \"component\" : null, \"customLink\" : null, \"emptySpace\" : null, \"field\" : \"CloseDate\", \"height\" : null, \"page\" : null, \"reportChartComponent\" : null, \"scontrol\" : null, \"showLabel\" : null, \"showScrollbars\" : null, \"width\" : null }, { \"analyticsCloudComponent\" : null, \"behavior\" : \"Required\", \"canvas\" : null, \"component\" : null, \"customLink\" : null, \"emptySpace\" : null, \"field\" : \"StageName\", \"height\" : null, \"page\" : null, \"reportChartComponent\" : null, \"scontrol\" : null, \"showLabel\" : null, \"showScrollbars\" : null, \"width\" : null }, { \"analyticsCloudComponent\" : null, \"behavior\" : \"Required\", \"canvas\" : null, \"component\" : null, \"customLink\" : null, \"emptySpace\" : null, \"field\" : \"Sales_Forecast__c\", \"height\" : null, \"page\" : null, \"reportChartComponent\" : null, \"scontrol\" : null, \"showLabel\" : null, \"showScrollbars\" : null, \"width\" : null }, { \"analyticsCloudComponent\" : null, \"behavior\" : \"Required\", \"canvas\" : null, \"component\" : null, \"customLink\" : null, \"emptySpace\" : null, \"field\" : \"Budget_Source__c\", \"height\" : null, \"page\" : null, \"reportChartComponent\" : null, \"scontrol\" : null, \"showLabel\" : null, \"showScrollbars\" : null, \"width\" : null }, { \"analyticsCloudComponent\" : null, \"behavior\" : \"Required\", \"canvas\" : null, \"component\" : null, \"customLink\" : null, \"emptySpace\" : null, \"field\" : \"Radio_Type__c\", \"height\" : null, \"page\" : null, \"reportChartComponent\" : null, \"scontrol\" : null, \"showLabel\" : null, \"showScrollbars\" : null, \"width\" : null } ], \"reserved\" : null } ], \"style\" : \"TwoColumnsTopToBottom\" }, { \"customLabel\" : true, \"detailHeading\" : false, \"editHeading\" : false, \"label\" : \"Order Management System\", \"layoutColumns\" : [ { \"layoutItems\" : [ ], \"reserved\" : null }, { \"layoutItems\" : [ ], \"reserved\" : null }, { \"layoutItems\" : [ ], \"reserved\" : null } ], \"style\" : \"CustomLinks\" } ]}';
        OpportunityLayoutAPI.PageSections properIP = OpportunityLayoutAPI.parse(json);
        System.assert(properIP != null);

        json = '{\"TestAMissingObject\": { \"TestAMissingArray\": [ { \"TestAMissingProperty\": \"Some Value\" } ] } }';
        OpportunityLayoutAPI.LayoutItems objLayoutItems = new OpportunityLayoutAPI.LayoutItems(System.JSON.createParser(json));
        System.assert(objLayoutItems != null);

        json = '{\"TestAMissingObject\": { \"TestAMissingArray\": [ { \"TestAMissingProperty\": \"Some Value\" } ] } }';
        OpportunityLayoutAPI.LayoutSections objLayoutSections = new OpportunityLayoutAPI.LayoutSections(System.JSON.createParser(json));
        System.assert(objLayoutSections != null);
        System.assert(objLayoutSections.customLabel == null);
        System.assert(objLayoutSections.detailHeading == null);
        System.assert(objLayoutSections.editHeading == null);
        System.assert(objLayoutSections.label == null);
        System.assert(objLayoutSections.layoutColumns == null);
        System.assert(objLayoutSections.style == null);

        json = '{\"TestAMissingObject\": { \"TestAMissingArray\": [ { \"TestAMissingProperty\": \"Some Value\" } ] } }';
        OpportunityLayoutAPI.PageSections objPageSections = new OpportunityLayoutAPI.PageSections(System.JSON.createParser(json));
        System.assert(objPageSections != null);
        System.assert(objPageSections.layoutSections == null);

        json = '{\"TestAMissingObject\": { \"TestAMissingArray\": [ { \"TestAMissingProperty\": \"Some Value\" } ] } }';
        OpportunityLayoutAPI.LayoutColumns objLayoutColumns = new OpportunityLayoutAPI.LayoutColumns(System.JSON.createParser(json));
        System.assert(objLayoutColumns!= null);
        System.assert(objLayoutColumns.layoutItems == null);
        System.assert(objLayoutColumns.reserved == null);

        json = '{\"TestAMissingObject\": { \"TestAMissingArray\": [ { \"TestAMissingProperty\": \"Some Value\" } ] } }';
        OpportunityLayoutAPI.LayoutItems layoutItems = new OpportunityLayoutAPI.LayoutItems(System.JSON.createParser(json));
        System.assert(layoutItems != null);
        System.assert(layoutItems.analyticsCloudComponent == null);
        System.assert(layoutItems.behavior == null);
        System.assert(layoutItems.canvas == null);
        System.assert(layoutItems.component == null);
        System.assert(layoutItems.customLink == null);
        System.assert(layoutItems.emptySpace == null);
        System.assert(layoutItems.field == null);
        System.assert(layoutItems.height == null);
        System.assert(layoutItems.page == null);
        System.assert(layoutItems.reportChartComponent == null);
        System.assert(layoutItems.scontrol == null);
        System.assert(layoutItems.showLabel == null);
        System.assert(layoutItems.showScrollbars == null);
        System.assert(layoutItems.width == null);
    }

}