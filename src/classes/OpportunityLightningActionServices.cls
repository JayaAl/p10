/**
* ─────────────────────────────────────────────────────────────────────────────────────────────────┐
* This class services lightning action buttons on Opportunity.
* ──────────────────────────────────────────────────────────────────────────────────────────────────
* @author         Jaya Alaparthi
* @maintainedBy   Jaya Alaparthi
* @version        1.0
* @created        2017-04-13
* @modified       
* @systemLayer    Service
* @see            ????
* @see            ????
* ──────────────────────────────────────────────────────────────────────────────────────────────────
* @changes
* v1.1            Jaya Alaparthi
* 2017-05-25      Adding Sbx and Prod Jira urls to Custom settings.
* ──────────────────────────────────────────────────────────────────────────────────────────────────
* @changes
* v1.2            Jaya Alaparthi
* 2017-06-06      making sbx determination as seperate method to use for 
*				Create sponsorship.
* ──────────────────────────────────────────────────────────────────────────────────────────────────
* @changes
* v1.3            Jaya Alaparthi
* 2017-10-26      JIRA Profiles are being added to custom settings
* ESS-41928
* ─────────────────────────────────────────────────────────────────────────────────────────────────┘
*/
public with sharing class OpportunityLightningActionServices {

	/*public static Map<String,String> JIRA_VALID_PROFILES = new Map<String,String>{
								'00e40000000rYpQAAU'=>'Sales - Client Services User',
								'00e40000000rYppAAE'=>'Sales - Account Manager',
								'00e40000000rYpuAAE'=>'Sales - Trafficker',
								'00e40000000rYqnAAE'=>'Sales - Sales Planner User',
								'00e40000000rYzpAAE'=>'Sales - Client Services - Performance',
								'00e40000000rZ4LAAU'=>'Sales - Power Client Services User',
								'00e40000000rbDJAAY'=>'Sales - Sales Planner/bulk subs User'
							};*/
	public static Map<String,String> JIRA_VALID_PROFILES = new Map<String,String>();


	//─────────────────────────────────────────────────────────────────────────┐
	// retirive JIRA VALID PROFILES: 
	// @return Map<String,String> 
    //─────────────────────────────────────────────────────────────────────────┘
    private static void retriveJIRAVALIDProfiles() {

    	for(Jira_Valid_Profiles__c profile: Jira_Valid_Profiles__c.getAll().values()) {
    		JIRA_VALID_PROFILES.put(profile.Name,profile.ProfileName__c);
    	}	
    	
    }
	//─────────────────────────────────────────────────────────────────────────┐
	// validateForJIRACreation: validate Jira creation based on user Profile.
	// @param opptyId  opportunity Id
	// @return String 
    //─────────────────────────────────────────────────────────────────────────┘
	@AuraEnabled
	public static String validateForJIRACreation(Id opptyId) {

		String responseStr;
		Id userProfileId;

		if(!String.isBlank(opptyId) 
				&& String.valueOf(opptyId).startsWith('006')) {

			userProfileId =  UserInfo.getProfileId();
			retriveJIRAVALIDProfiles();

			if(userProfileId != null && !JIRA_VALID_PROFILES.isEmpty() 
					&& JIRA_VALID_PROFILES.containsKey(userProfileId)) {

				System.debug('valid for Jira profile');
				//responseStr = System.Label.JIRA_Ticket_Creation;
				responseStr = getJIRAUrl();
				System.debug('responseStr:'+responseStr);
				responseStr.replace('OPPTYID',opptyId);
				responseStr.replace(OPPTYID, opptyId);
			} else {

				responseStr = 'You do not have the required permissions to open a Jira Task.';
			}
		}

		return responseStr;
	}
	//─────────────────────────────────────────────────────────────────────────┐
	// retriveOrgId: to retrive Org Id of the user.
	// @return String 
    //─────────────────────────────────────────────────────────────────────────┘
	@AuraEnabled
	public static Boolean determineCurrentInstance() {

		return isSBX();
	}
	//─────────────────────────────────────────────────────────────────────────┐
	// determineClonePage: determine clone vf page based on opportunity 
	//						record type
	// @param opptyId  opportunity Id
	// @return String 
    //─────────────────────────────────────────────────────────────────────────┘
	@AuraEnabled
	public static String determineClonePage(Id opptyId) {

		//String opptyRecordTypeId = getOpptyRecordType(opptyId);
		String opptyRecordTypeId = getOpportunityRecord(opptyId).RecordTypeId;
		System.debug('opportunityREcordTypeId:'+opptyRecordTypeId);
		String opptyClonePage;
		Map<String,String> opptyVFMap = new Map<String,String>();

		opptyVFMap = getRecordTypeToVFMap();

		if(!opptyVFMap.isEmpty()) {

			if(!String.isBlank(opptyRecordTypeId)) {

				System.debug('opptyRecordTypeId:'+opptyRecordTypeId);
				if(opptyVFMap.containsKey(opptyRecordTypeId)) {

					opptyClonePage = opptyVFMap.get(opptyRecordTypeId);
				} else {

					opptyClonePage = '';
				}
				opptyClonePage +=  '?id='+opptyId;
			}
		} else {

			opptyClonePage = opptyId;
		}
		// hard coded values are moved to cusotm settings.
		/*if(!String.isBlank(opptyRecordTypeId)) {

			System.debug('opptyRecordTypeId:'+opptyRecordTypeId);
			if(opptyRecordTypeId == '012400000009ebLAAQ') { 

				opptyClonePage = 'OPT_CloneSimpleEntryInside?id='+opptyId; 
			} else if(opptyRecordTypeId == '012400000009gTKAAY') { 

				opptyClonePage = 'OPT_CloneSimpleEntryOutside?id='+opptyId; 
			} else if(opptyRecordTypeId == '012400000009gTJAAY') { 

				opptyClonePage = 'OPT_ClonePerformance?id='+opptyId; 
			}else if(opptyRecordTypeId == '01240000000UeEgAAK') { 

				opptyClonePage = 'OPT_CloneProgrammatic?id='+opptyId; 
			} else {

				opptyClonePage = opptyId;
			}

		}*/

		System.debug('opptyClonePage:'+opptyClonePage);
		return opptyClonePage;
	}
	//─────────────────────────────────────────────────────────────────────────┐
	// getOpportunityRecord: get opportunity record.
	//						
	// @param opptyId  opportunity Id
	// @return Opportunity
    //─────────────────────────────────────────────────────────────────────────┘
	@AuraEnabled
	public static Opportunity getOpportunityRecord(Id opptyId) {

		Opportunity oppty = [SELECT Id,Name,TTR_User_Data__c,
									Account.Name,Opp_Order_ID__c,
									Agency__c,TTR_URL__c,
									RecordTypeId 
							FROM Opportunity
							WHERE Id =: opptyId];
		return oppty;
	}
	//─────────────────────────────────────────────────────────────────────────┐
	// buildCongaURL: generate conga url
	//						
	// @param opptyId  opportunity Id
	// @param serverURL String
	// @return string conga url 
    //─────────────────────────────────────────────────────────────────────────┘
	@AuraEnabled
	public static String buildCongaURL(Id opptyId, String serverURL) { 

		String tempGrp = 'Advertiser';
		Opportunity oppty = new Opportunity();
		oppty = getOpportunityRecord(opptyId);
		String congaUrl;

		if(oppty.Id != null 
			&& ((oppty.TTR_URL__c != null && oppty.TTR_URL__c != '')
				 || (oppty.TTR_User_Data__c != null && oppty.TTR_User_Data__c != ''))) {

			System.debug('oppty:'+oppty);
			System.debug('TTR_URL__c:'+oppty.TTR_URL__c);
			if(oppty.Agency__c != null) {

				tempGrp += 'Agency';
			}

			congaUrl = 'https://www.appextremes.com/apps/Conga/Composer.aspx?'+ 
								'sessionId=' + UserInfo.getSessionId() + 
								'&serverUrl=' + serverURL + 
								'&id=' + oppty.Id + 
								'&ReportId=00O40000003ITv2' + 
								'&ContentVisible=1' + 
								'&LGAttachOption=1' + 
								'&ContentWorkspaceId=05840000000Cdp4' + 
								'&ContentCustomFieldName=Opportunity__c' + 
								'&ContentCustomFieldValue=' + oppty.Id + 
								'&ContentCustomFieldName1=Folder__c' + 
								'&ContentCustomFieldValue1=Insertion+Orders' + 
								'&DefaultPDF=1&AttachmentParentId=' + oppty.Id + 
								'&FP0=1' + 
								'&TemplateGroup=' + tempGrp + 
								'&DefaultLocal=0&OFN={Template.Label}+-+' 
								+ oppty.Account.Name + '+' + oppty.Opp_Order_ID__c + '+' + Date.today() + 
								'&DS1=1&DS2=1&DS3=1&DS5=1&DS6=1&DS9=1';
		} else {

			congaUrl = 'Enter values for TTR Url and/or TTR User Data before attempting to generate a TTR Addendum.';
		}
		System.debug('congaUrl:'+congaUrl);
		return congaUrl;
		
	}
	
	//─────────────────────────────────────────────────────────────────────────┐
	// getRecordTypeToVFMap: retrive record type and cloe VF page 
	//						for Opportunity
	// @return map sting or string
    //─────────────────────────────────────────────────────────────────────────┘
	private static map<String,String> getRecordTypeToVFMap() {

		Map<String,String> recordTypeToVFMap = new Map<String,String>();
		for(Opportunity_Clone_VF_Page_Map__c opptyVF : Opportunity_Clone_VF_Page_Map__c.getall().values()) {

			recordTypeToVFMap.put(opptyVF.Name,opptyVF.VF_page_Name__c);
		}
		return recordTypeToVFMap;
	}
	// v1.1
	//─────────────────────────────────────────────────────────────────────────┐
	// getJIRAUrl: determine JIRA Url based on the Org.
	// @return sting or string
    //─────────────────────────────────────────────────────────────────────────┘
	private static String getJIRAUrl() {

		String jiraUrl;
		Boolean isSBXFlag;
		isSBXFlag = isSBX();
		System.debug('isSBXFlag:'+isSBXFlag);
		if(isSBXFlag) {
			jiraUrl = System.Label.JIRA_SBX_Ticket_OpptyCreate;
		} else {
			jiraUrl = System.Label.JIRA_Ticket_Creation;
		}
		return jiraUrl;
	}
	// v1.1
	//─────────────────────────────────────────────────────────────────────────┐
	// isSBX: determine if org is sbx or prod.
	// @return sting or string
    //─────────────────────────────────────────────────────────────────────────┘
	private static Boolean isSBX() {

		Organization isSBX = [SELECT IsSandbox
								FROM Organization
								LIMIT 1];
		if(isSBX.IsSandbox) {

			return true;
		} else {

			return false;
		}
	}
	/*private map<String,String> getRecordTypeNameIdMap(list<String> recordTypeNames) {

		map<String,String> recordTypeIdMap = new map<String,String>();

		return recordTypeIdMap;
	}*/
}