/**
* ─────────────────────────────────────────────────────────────────────────────────────────────────┐
* Opportunity Related lightning actions Test
* 
* Test class for:
* OpportunityLightningActionServices.cls
* ──────────────────────────────────────────────────────────────────────────────────────────────────
* @author         Jaya Alaparthi
* @maintainedBy   Jaya Alaparthi
* @version        1.0
* @created        2017-05-15
* @modified       YYYY-MM-DD
* @systemLayer    Test
* ──────────────────────────────────────────────────────────────────────────────────────────────────
* @changes
* vX.X            Jaya Alaparthi
* YYYY-MM-DD      Explanation of the change.  Multiple lines can be used to explain the change, but
*                 each line should be indented till left aligned with the previous description text.
* ─────────────────────────────────────────────────────────────────────────────────────────────────┘
*/
@isTest
private class  OpportunityLightningActionServicesTest {
	
	@testSetUp static void testDataSetup() {

		// create Account
		Account account =  new Account();
		account = UTIL_TestUtil.generateAccount();
		account.Name = 'opptyLightActSErvicesTEST';
		insert account;

		Id opptyRecordTypeId = Schema.SObjectType.Opportunity.getRecordTypeInfosByName().get('Opportunity - Inside/Local Full').getRecordTypeId();
        
		System.debug('opptyRecordTypeId:'+opptyRecordTypeId);
		Opportunity opportunity = new Opportunity();
		opportunity = UTIL_TestUtil.generateOpportunity(account.Id);
		opportunity.Name = 'OpptyLighServicesTest';
		opportunity.CurrencyIsoCode = 'USD';
		opportunity.RecordTypeId = opptyRecordTypeId;
		opportunity.TTR_User_Data__c = 'Email Address';
		insert opportunity;
	}
	@isTest
    static void jiraValidationTest() {

		Profile p = [SELECT Id FROM Profile
					WHERE Id = '00e40000000rYppAAE'];
		User u = new User(Alias = 'standt', Email='opptyLigTEst@testorg.com', 
    					EmailEncodingKey='UTF-8', 
    					LastName='Testing', 
    					LanguageLocaleKey='en_US', 
    					LocaleSidKey='en_US', 
    					ProfileId = p.Id, 
    					TimeZoneSidKey='America/Los_Angeles', 
    					UserName='opptyLigTEst@testorg.com');
		Test.startTest();
    	System.runAs(u) {
        	
        	// create test data in cusotm settings
			Jira_Valid_Profiles__c jiraRec = new Jira_Valid_Profiles__c(Name=p.Id,
																ProfileName__c = 'Sales - Account Manager');
			insert jiraRec;

        	Account account =  new Account();
			account = UTIL_TestUtil.generateAccount();
			account.Name = 'TESTJIRA';
			account.website = 'TESTJIRA.com';
			insert account;

	        Id opptyRecordTypeId = Schema.SObjectType.Opportunity.getRecordTypeInfosByName().get('Opportunity - Inside/Local Full').getRecordTypeId();
        
			System.debug('opptyRecordTypeId:'+opptyRecordTypeId);
			Opportunity opportunity = new Opportunity();
			opportunity = UTIL_TestUtil.generateOpportunity(account.Id);
			opportunity.Name = 'OpptyLighServicesTestJIRA';
			opportunity.CurrencyIsoCode = 'USD';
			opportunity.RecordTypeId = opptyRecordTypeId;
			opportunity.TTR_User_Data__c = 'Email Address';
			insert opportunity;
    		
			Opportunity oppty = OpportunityLightningActionServices.getOpportunityRecord(opportunity.Id);
			System.assert(oppty.Name == 'OpptyLighServicesTestJIRA','Opportunity created in test class and '
								+'the opportunity returned does not match.');
			OpportunityLightningActionServices.determineCurrentInstance();
			String jiraUrl = OpportunityLightningActionServices.validateForJIRACreation(opportunity.Id);
			System.debug('jiraUrl:'+jiraUrl);
			System.assert(jiraUrl.contains('OPPTYID'),'validateForJIRACreation failed.');
		}
		Test.stopTest();
   }

   @isTest
   static void opptycloneVFPage() {

   		// create data for oppty clone vf page
   		Opportunity_Clone_VF_Page_Map__c rec = new Opportunity_Clone_VF_Page_Map__c(Name='012400000009ebLAAQ',
   										VF_page_Name__c = '	OPT_CloneSimpleEntryInside');
   		insert rec;

   		Test.startTest();
   		Opportunity opportunity = [SELECT Id,Name
    									FROM Opportunity 
    									WHERE Name = 'OpptyLighServicesTest' 
    									LIMIT 1];
   		OpportunityLightningActionServices.determineClonePage(opportunity.Id);
   		Test.stopTest();
   }

   @isTest
   static void opptyTTR() {

   	Test.startTest();
   		Opportunity opportunity = [SELECT Id,Name
    									FROM Opportunity 
    									WHERE Name = 'OpptyLighServicesTest' 
    									LIMIT 1];
    	String url = new ApexPages.StandardController(opportunity).view().getHeaders().get('Host');
    	OpportunityLightningActionServices.buildCongaURL(opportunity.Id,url);
    Test.stopTest();
   }
}