public with sharing class OpportunityPageExt {
    
    public String copyURL {get;set;}
    public OpportunityPageExt(ApexPages.StandardController controller) {
        
        String currentURL = URL.getCurrentRequestUrl().toExternalForm();
        System.debug('currentURL:'+currentURL);
        currentURL = currentURL.substring(0,currentURL.indexOf('salesforce.com')+14);
        System.debug('currentURL:'+currentURL);
        copyURL  = currentURL;
    }
    

}