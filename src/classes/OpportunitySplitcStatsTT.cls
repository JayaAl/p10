@isTest
private class OpportunitySplitcStatsTT {

   static testMethod void testTrigger() {
      try {
          Opportunity_Split__c o = new Opportunity_Split__c();
          insert o;

          System.assertNotEquals(null, o);
      }
      catch(Exception e) {
          List<Opportunity_Split__c> l = [SELECT Id from Opportunity_Split__c LIMIT 1];
          update l;
          System.assertNotEquals(null, l);
      }
   }
}