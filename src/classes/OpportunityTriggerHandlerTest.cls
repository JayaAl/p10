/**
 * @name: OpportunityTriggerHandlerTest 
 * @desc: Used to test OpportunityTriggerHandler
 * @author: Lakshman(sfdcace@gmail.com)
 */
@isTest
public class OpportunityTriggerHandlerTest {

    public static void setupData(){

    }

    public static testMethod void testOnForsevaOpttyUpdate(){

        OpportunitySplit__c split = new OpportunitySplit__c();
        split.Name = 'Split Setting';
        split.Create_Splits__c = true;
        insert split;

         String TEST_STRING = 'aaaa';

        Account acc = new Account(
            name = TEST_STRING                      
        );
        insert acc;

         Account acc2 = new Account(
            name = TEST_STRING+'2'                      
        );

        insert acc2;

        Contact cont = new Contact(lastName = TEST_STRING, accountId = acc.id , MailingStreet = 'MyStreet1', MailingCountry = 'USA', MailingState = 'CA', MailingCity = 'Fremont', Email = 'myemail1@mydomain.com', Title = 'MyTitle1', firstname = 'MyFristName1');
        insert cont;

        // Insert custom setting Auto_Assign_Biller__c   
        insert UTIL_TestUtil.createAutoAssignCS(UserInfo.getUserId(),UserInfo.getUserName(),1);

         Product2 prod = new Product2(Name = 'Pandora One - 6 Month', 
                                   Family = 'Hardware');
        insert prod;
                
        Id pricebookId = Test.getStandardPricebookId();
          
        PricebookEntry standardPrice = new PricebookEntry(
              Pricebook2Id = pricebookId, Product2Id = prod.Id,
              UnitPrice = 10000, IsActive = true, CurrencyIsoCode = 'USD');

        insert standardPrice;

         List<Opportunity> oppLst = new List<Opportunity>();
        Opportunity oppty;
        for(Integer ind = 0; ind < 5 ; ind++){

                oppty = new Opportunity(
                  accountId = acc.id
                , bill_on_broadcast_calendar2__c = TEST_STRING
                , closeDate = Date.Today()
                , confirm_direct_relationship__c = false
                , name = TEST_STRING+String.ValueOf(ind)
                , stageName = 'Closed Won'
                , Primary_Billing_Contact__c = cont.id  // This is to address the validation rule where Primary Billing Contact is now a required field for all opportunities 
                , Lead_Campaign_Manager__c = UserInfo.getUserId()
                , Agency__c = acc2.id 
                , probability = 95 
                , Sales_Planner_NEW__c = UserInfo.getUserId()
                , Primary_Contact__c = cont.id
            );
            oppty.ContractStartDate__c = Date.today();
            oppty.ContractEndDate__c = Date.today()+50;

            oppLst.add(oppty);
        }
        
        insert oppLst;

        Upfront__c upfrontVar = new Upfront__c();
        upfrontVar.Amount_Spent__c = 500;
        upfrontVar.Name = 'Upfront-1';
        insert upfrontVar;

        Test.startTest();       
            
            for(Opportunity opptyVar : oppLst){
                opptyVar.CloseDate = Date.today()+50;
                opptyVar.Amount = 6000;
                opptyVar.StageName = 'Prospected';
                opptyVar.Upfront__c = upfrontVar.Id;
                opptyVar.probability = 45;
                oppty.ContractStartDate__c = Date.today()-30;
                oppty.ContractEndDate__c = Date.today()+50;
                oppty.Probability = 100;
            }

            update oppLst;

        Test.stopTest();


    }

  


    public static testMethod void testSetUPSplitdata(){
        
        Id runningUserId = Userinfo.getUserId();

        UserRole r = new UserRole(name = 'TEST ROLE');
        Database.insert(r);

        User userVar = new User(alias = 'hasrole', email='userwithrole@roletest1.com', userroleid = r.id,
                                    emailencodingkey='UTF-8', lastname='Testing', languagelocalekey='en_US', 
                                    localesidkey='en_US', profileid = UserInfo.getProfileId(), 
                                    timezonesidkey='America/Los_Angeles', username='userwithrole@testorg.com');
        insert userVar;
         List<Opportunity> oppLst = new List<Opportunity>();

        System.runAs(userVar){

         OpportunitySplit__c split = new OpportunitySplit__c();
        split.Name = 'Split Setting';
        split.Create_Splits__c = true;
        insert split;

         String TEST_STRING = 'aaaa';

        Account acc = new Account(
            name = TEST_STRING                      
        );
        insert acc;

         Account acc2 = new Account(
            name = TEST_STRING+'2'                      
        );

        insert acc2;

        Contact cont = new Contact(lastName = TEST_STRING, accountId = acc.id , MailingStreet = 'MyStreet1', MailingCountry = 'USA', MailingState = 'CA', MailingCity = 'Fremont', Email = 'myemail1@mydomain.com', Title = 'MyTitle1', firstname = 'MyFristName1');
        insert cont;

        // Insert custom setting Auto_Assign_Biller__c   
        insert UTIL_TestUtil.createAutoAssignCS(UserInfo.getUserId(),UserInfo.getUserName(),1);

         Product2 prod = new Product2(Name = 'Pandora One - 6 Month', 
                                   Family = 'Hardware');
        insert prod;
                
        Id pricebookId = Test.getStandardPricebookId();
          
        PricebookEntry standardPrice = new PricebookEntry(
              Pricebook2Id = pricebookId, Product2Id = prod.Id,
              UnitPrice = 10000, IsActive = true, CurrencyIsoCode = 'USD');

        insert standardPrice;

        
        Opportunity oppty;
        for(Integer ind = 0; ind < 5 ; ind++){

                oppty = new Opportunity(
                  accountId = acc.id
                , bill_on_broadcast_calendar2__c = TEST_STRING
                , closeDate = Date.Today()
                , confirm_direct_relationship__c = false
                , name = TEST_STRING+String.ValueOf(ind)
                , stageName = 'Prospected'
                , Primary_Billing_Contact__c = cont.id  // This is to address the validation rule where Primary Billing Contact is now a required field for all opportunities 
                , Lead_Campaign_Manager__c = UserInfo.getUserId()
                , Agency__c = acc2.id 
                , probability = 95 
                , Sales_Planner_NEW__c = UserInfo.getUserId()
                , Primary_Contact__c = cont.id
            );
            oppty.ContractStartDate__c = Date.today();
            oppty.ContractEndDate__c = Date.today()+50;

            oppLst.add(oppty);
        }
        
        Test.startTest(); 
            insert oppLst;
            Upfront__c upfrontVar = new Upfront__c();
            upfrontVar.Amount_Spent__c = 500;
            upfrontVar.Name = 'Upfront-1';
            insert upfrontVar;

            for(Opportunity opptyVar : oppLst){
                opptyVar.CloseDate = Date.today()+50;
                opptyVar.Amount = 6000;
                opptyVar.StageName = OpportunityTriggerUtil.CLOSED_WON_STAGE;
                opptyVar.Upfront__c = upfrontVar.Id;
                opptyVar.OwnerId = runningUserId;
            }

            update oppLst;
        Test.stopTest();      
            
            

        

        
        }



    }

    public static testMethod void testcheckAndInitiateOpportunityFields(){

        String TEST_STRING = 'aaaa';

        Account acc = new Account(
            name = TEST_STRING                      
        );
        insert acc;

         Account acc2 = new Account(
            name = TEST_STRING+'2'                      
        );

        insert acc2;

        Contact cont = new Contact(lastName = TEST_STRING, accountId = acc.id , MailingStreet = 'MyStreet1', MailingCountry = 'USA', MailingState = 'CA', MailingCity = 'Fremont', Email = 'myemail1@mydomain.com', Title = 'MyTitle1', firstname = 'MyFristName1');
        insert cont;

        // Insert custom setting Auto_Assign_Biller__c   
        insert UTIL_TestUtil.createAutoAssignCS(UserInfo.getUserId(),UserInfo.getUserName(),1);

         Product2 prod = new Product2(Name = 'Pandora One - 6 Month', 
                                   Family = 'Hardware');
        insert prod;
                
        Id pricebookId = Test.getStandardPricebookId();
          
        PricebookEntry standardPrice = new PricebookEntry(
              Pricebook2Id = pricebookId, Product2Id = prod.Id,
              UnitPrice = 10000, IsActive = true, CurrencyIsoCode = 'USD');

        insert standardPrice;

         List<Opportunity> oppLst = new List<Opportunity>();
        Opportunity oppty;
        for(Integer ind = 0; ind < 5 ; ind++){

                oppty = new Opportunity(
                  accountId = acc.id
                , bill_on_broadcast_calendar2__c = TEST_STRING
                , closeDate = Date.Today()
                , confirm_direct_relationship__c = false
                , name = TEST_STRING+String.ValueOf(ind)
                , stageName = 'Closed Won'
                , Primary_Contact__c = cont.id
                , Primary_Billing_Contact__c = cont.id  // This is to address the validation rule where Primary Billing Contact is now a required field for all opportunities 
                , Lead_Campaign_Manager__c = UserInfo.getUserId()
                , Agency__c = acc2.id 
                , probability = 95 
                , Sales_Planner_NEW__c = UserInfo.getUserId()
            );
            oppty.ContractStartDate__c = Date.today();
            oppty.ContractEndDate__c = Date.today()+50;

            oppLst.add(oppty);
        }
        
        Test.startTest();       
        insert oppLst;
        Test.stopTest();

    } 

    public static testMethod void testOpptyBforeInsert() {
        String TEST_STRING = 'aaaa';

        Account acc = new Account(
            name = TEST_STRING                      
        );

        insert acc;

         Account acc2 = new Account(
            name = TEST_STRING+'2'                      
        );

        insert acc2;
        Contact cont = new Contact(lastName = TEST_STRING, accountId = acc.id , MailingStreet = 'MyStreet1', MailingCountry = 'USA', MailingState = 'CA', MailingCity = 'Fremont', Email = 'myemail1@mydomain.com', Title = 'MyTitle1', firstname = 'MyFristName1');
        insert cont;

        // Insert custom setting Auto_Assign_Biller__c   
        insert UTIL_TestUtil.createAutoAssignCS(UserInfo.getUserId(),UserInfo.getUserName(),1);

         Product2 prod = new Product2(Name = 'Pandora One - 6 Month', 
                                   Family = 'Hardware');
      insert prod;
            
      Id pricebookId = Test.getStandardPricebookId();
      
      PricebookEntry standardPrice = new PricebookEntry(
          Pricebook2Id = pricebookId, Product2Id = prod.Id,
          UnitPrice = 10000, IsActive = true, CurrencyIsoCode = 'USD');

      insert standardPrice;
      
      List<Opportunity> oppLst = new List<Opportunity>();

        Opportunity oppty;
        for(Integer ind = 0; ind < 5 ; ind++){

                oppty = new Opportunity(
                  accountId = acc.id
                , bill_on_broadcast_calendar2__c = TEST_STRING
                , closeDate = Date.Today()
                , confirm_direct_relationship__c = false
                , name = TEST_STRING+String.ValueOf(ind)
                , stageName = 'Prospected'
                , Primary_Billing_Contact__c = cont.id  // This is to address the validation rule where Primary Billing Contact is now a required field for all opportunities 
                , Lead_Campaign_Manager__c = UserInfo.getUserId()
                , Agency__c = acc2.id 
                , Preferred_Invoicing_Method__c = OpportunityTriggerUtil.INVOICING_METHOD_MAIL_WO_NOT
                , Primary_Contact__c = cont.id
                , RecordTypeId = Custom_Constants.OPT_SimpleEntryOutside
                
                
            );
            oppty.ContractStartDate__c = Date.today();
            oppty.ContractEndDate__c = Date.today()+50;

            oppLst.add(oppty);
        }
        
        Test.startTest();       
        insert oppLst;
        Test.stopTest();
        for(Opportunity opptyvar : [Select id,name,Billing_Coordinator__c,Sales_Planner_NEW__c,X1st_Salesperson_Split__c from Opportunity where id in: oppLst]){
            // Covers Auto_Assign_Biller_Opportunity Trigger 
        
      //      system.assertNotEquals(opptyvar.Billing_Coordinator__c ,null); 
        
            
            system.assertEquals(opptyvar.X1st_Salesperson_Split__c ,100);
            system.assertNotEquals(opptyvar.Sales_Planner_NEW__c , null);
            
        }

        
        for(Integer ind = 0; ind < 3 ; ind++){
            oppLst[ind].closeDate = Date.Today()+5;
            oppLst[ind].stageName = 'Closed Won';
        }

       // update oppLst;


        List<OpportunityLineItem> oliLst = new List<OpportunityLineItem>();
        for(Opportunity oppVar : oppLst){

            OpportunityLineItem oli;
            for(Integer cnt = 0;cnt < 2;cnt++){
                oli = new OpportunityLineItem(OpportunityId = oppVar.Id, PricebookEntryId = standardPrice.Id, Quantity = 1, 
                    ServiceDate = System.Today(), End_Date__c = System.Today().addDays(100), UnitPrice = 100);
                oliLst.add(oli);
            }
    
        }
        
        system.debug('oliLst :::-->'+oliLst);
        
        insert oliLst;
      //  Test.stopTest();
        system.assert(oppLst[0].X1st_Salesperson__c != UserInfo.getUserId());
        system.assertEquals([Select id,name from Opportunity].size(),5);
       // system.assertEquals([Select id from OpportunityLineItem].size(),50*100);
        system.debug('Error : ==>'+[Select id,name,Class__c,Error__c,ID_List__c,Method__c,Object__c,Running_user__c,Trigger_ID__c from Error_Log__c]);
       // system.assertEquals([Select id,name from Error_Log__c].size(),0);

        //delete [Select id,name from Opportunity];
    }
 
    public static testMethod void testUtilMethods(){
        
        String TEST_STRING = 'aaaa';

        Account acc = new Account(
            name = TEST_STRING                      
        );

        insert acc;

         Account acc2 = new Account(
            name = TEST_STRING+'2'                      
        );

        insert acc2;
        Contact cont = new Contact(lastName = TEST_STRING, accountId = acc.id , MailingStreet = 'MyStreet1', MailingCountry = 'USA', MailingState = 'CA', MailingCity = 'Fremont', Email = 'myemail1@mydomain.com', Title = 'MyTitle1', firstname = 'MyFristName1');
        insert cont;

        // Insert custom setting Auto_Assign_Biller__c   
        insert UTIL_TestUtil.createAutoAssignCS(UserInfo.getUserId(),UserInfo.getUserName(),1);

         Product2 prod = new Product2(Name = 'Pandora One - 6 Month', 
                                   Family = 'Hardware');
      insert prod;
            
      Id pricebookId = Test.getStandardPricebookId();
      
      PricebookEntry standardPrice = new PricebookEntry(
          Pricebook2Id = pricebookId, Product2Id = prod.Id,
          UnitPrice = 10000, IsActive = true, CurrencyIsoCode = 'USD');

      insert standardPrice;
      
      List<Opportunity> oppLst = new List<Opportunity>();

        Opportunity oppty;
        for(Integer ind = 0; ind < 5 ; ind++){

                oppty = new Opportunity(
                  accountId = acc.id
                , bill_on_broadcast_calendar2__c = TEST_STRING
                , closeDate = Date.Today()
                , confirm_direct_relationship__c = false
                , name = TEST_STRING+String.ValueOf(ind)
                , stageName = 'Prospected'
                , Primary_Billing_Contact__c = cont.id  // This is to address the validation rule where Primary Billing Contact is now a required field for all opportunities 
                , Lead_Campaign_Manager__c = UserInfo.getUserId()
                , Agency__c = acc2.id 
                , Preferred_Invoicing_Method__c = OpportunityTriggerUtil.INVOICING_METHOD_MAIL_WO_NOT
                , Primary_Contact__c = cont.id
                , RecordTypeId = Custom_Constants.OPT_SimpleEntryOutside
                
                
            );
            oppty.ContractStartDate__c = Date.today();
            oppty.ContractEndDate__c = Date.today()+50;

            oppLst.add(oppty);
        }
        
        Test.startTest();       
        insert oppLst;
        Test.stopTest();
        

        List<OpportunityLineItem> oliLst = new List<OpportunityLineItem>();
        for(Opportunity oppVar : oppLst){

            OpportunityLineItem oli;
            for(Integer cnt = 0;cnt < 2;cnt++){
                oli = new OpportunityLineItem(OpportunityId = oppVar.Id, PricebookEntryId = standardPrice.Id, Quantity = 1, 
                    ServiceDate = System.Today(), End_Date__c = System.Today().addDays(100), UnitPrice = 100);
                oliLst.add(oli);
            }
    
        }

        Partner partnerVar = new Partner();
        partnerVar.AccountToId = acc2.id;
        partnerVar.Role = 'Ad Agency';
        partnerVar.IsPrimary = true;
        partnerVar.OpportunityId = oppLst[0].id;

        insert partnerVar;

        OpportunityContactRole ocrVar = new OpportunityContactRole();
        ocrVar.OpportunityId = oppLst[0].id;
        ocrVar.ContactId = cont.Id;
        ocrVar.IsPrimary = true;
        ocrVar.Role = 'Billing Contact';
        insert ocrVar;

        OpportunityTriggerUtil.addUpdateRecords(new List<Partner>{partnerVar},new List<Partner>{partnerVar}, new List<OpportunityContactRole>{ocrVar},new List<OpportunityContactRole>{ocrVar},new List<OpportunityContactRole>{ocrVar},new List<OpportunityContactRole>{ocrVar} );
        Set<id> iodetailsSet = new Set<id>();
        for(IO_Detail__c ioVar : [Select id,name from IO_Detail__c]){
            iodetailsSet.add(ioVar.id);
        }

        Case caseObj = new Case();
        caseObj.AccountId = acc.Id;
        caseObj.Opportunity__c = oppLst[0].Id;
        caseObj.Type = 'CSR';
        insert caseObj;


        OpportunityTriggerUtil.closeIODetailOnOpportunityClose(iodetailsSet);
        OpportunityTriggerUtil.insertOLIRecords(oliLst);
        OpportunityTriggerUtil.sendEmailTemplate(new List<String>{'aasss@wweee.com'},oppLst[0].id,oppLst[1].id,oppLst[0].id);
        OpportunityTriggerUtil.initiateAccountCV(new List<Id>{acc.Id,acc2.Id});
        OpportunityTriggerUtil.getCasesFromOppty(new Set<Id>{oppLst[0].Id},new Set<Id>{acc.Id,acc2.Id});
        OpportunityTriggerUtil.getUpdatedCaseOnOpportunityAccountChange(new Map<Id,Id>{oppLst[0].Id=>acc.Id},new Map<Id,Id>{oppLst[0].Id=>acc2.Id});
        OpportunityTriggerUtil.upsertAccounts(new List<Account>{acc});
        OpportunityTriggerUtil.getAcctGroupByUpfront(new Map<Id,Opportunity>{oppLst[0].Id=>oppLst[0]});
        OpportunityTriggerUtil.getAcctsAndrelatedoppties(new Set<Id>{acc.id});
        Upfront__c upfrontVar = new Upfront__c();
        upfrontVar.Amount_Spent__c = 500;
        upfrontVar.Name = 'Upfront-1';
        insert upfrontVar;

        OpportunityTriggerUtil.updateUpfrontRecords(JSON.serialize(new List<Upfront__c>{upfrontVar}));
        
        caseObj = new Case();
        caseObj.Type = 'CSR';
        insert caseObj;

        OpportunityTriggerUtil.updateCaseRecords(new List<CAse>{caseObj});
        OpportunityTriggerUtil.getOpptyAndCase(new Set<Id>{oppLst[0].Id});
        OpportunityTriggerUtil.getAutoAssignMap();
        OpportunityTriggerUtil.generateSplitsApproval(String.ValueOf([Select id,Name from Opportunity_Split__c limit 1].Id), system.now(), date.today(),oppLst[0].Id, 'Abhishek');
        OpportunityTriggerUtil.getCurrentUser();
    }
}