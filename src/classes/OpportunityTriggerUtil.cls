public without sharing class OpportunityTriggerUtil {
    
    public static final Set<String> RT_CASE_NAMES = new Set<String> {
          'Legal_IO_Approval_Case_Simple_Entry' // IO Approval Case
        , 'Legal_IO_Approval_Request' // IO Approval Details
    };
    public static final String INVOICING_METHOD_MAIL_WO_NOT = 'Mail w/ Notarization';
    public static final String VENDOR_CONST = 'Vendor';
    public static final String REASON_LOST_STR = 'No Activity - 60days';
    public static final String CONTEXT_TYPE_INSERT = 'insert';
    public static final String CONTEXT_TYPE_UPDATE = 'update';
    public static final String OFFERING_TYPE_STANDARD = 'Standard';
    public static final String MEDIUM_AUDIO = 'Audio';
    public static final String STAUS_APPROVED = 'Approved';
    public static final String BILLING_CONTACT = 'Billing Contact';
    public static final String CLOSED_WON_STAGE = 'Closed Won';
    public static List<Account> accountLst;
    public static List<Account> accountToUpdateAfterUpdate = new list<Account>();
    public static Map<String, Auto_Assign_Biller__c> mapBillers;
    public static User loggedInUser;

    @future
    public static void updateAccountsAfterUpdate(){
    
        if(!accountToUpdateAfterUpdate.isEmpty())
            update accountToUpdateAfterUpdate;
    }
    
    public static List<Account> getAllAccounts(List<Opportunity> newOpptyLst){
        
        if(accountLst == null){

            Set<String> acctRecordIds = new Set<String>();
        
            for(Opportunity objOpp : newOpptyLst){
                if(objOpp.AccountId != null)
                acctRecordIds.add(String.ValueOf(objOpp.AccountId));
            }

            if(acctRecordIds != null && !acctRecordIds.isEmpty())
            accountLst = (List<Account>) OpportunityTriggerUtil.queryDataByField('Account',new List<String>{'Credit_Verification2__c', 'RecordType.Name','Id', 'phone', 'forseva1__Credit_Review_Date__c', 'forseva1__Verification_Date__c', 'CV_approval_status__c', 'CV_approval_date__c', 'Credit_Verification2__r.Status__c', 'Credit_Verification2__r.Internal_Approval_Status__c', 'Credit_Status__c', 'credit_expiry_notification__c'},'Id',acctRecordIds);                    
        }    

        return accountLst;        
    }

    public static List<Aggregateresult> getAggregateOpptylesstan100(List<id> oppIdLst,List<id> acctIdLst){

       return [select AccountId, sum(Amount) from Opportunity
               where  Id not in : oppIdLst
               and    AccountId in :acctIdLst
               and    Probability >= 75
               and    Probability < 100
               group by AccountId];

    }

    public static List<Aggregateresult> getAggregateOpptyEquals100(List<id> oppIdLst,List<id> acctIdLst){
        return [select AccountId, sum(Amount), sum(Billed_To_Date__c)
                             from   Opportunity
                             where  Id not in : oppIdLst
                             and    AccountId in :acctIdLst
                             and    Probability = 100
                             group by AccountId];
    }

    public static void addUpdateRecords(List<Partner> listPartnersToDelete, List<Partner> listPartnersToInsert, List<opportunityContactRole> ocListtoUpdate,List<OpportunityContactRole> ocListtoAdd, List<OpportunityContactRole> billingContactOCListtoUpdate, List<OpportunityContactRole> billingContactOCListtoAdd){

        if(! listPartnersToDelete.isEmpty()){
            try{
                  system.debug('listPartnersToDelete Size =  ' + listPartnersToDelete.size())   ;
                Database.delete(listPartnersToDelete, false);
            } catch(Exception ex){
                system.debug('Ex =>' + ex.getMessage() + 'listPartnersToDelete Size =  ' + listPartnersToDelete.size()) ;
                throw ex;
            }
        }
        
        if(! listPartnersToInsert.isEmpty()){
            
            system.debug('listPartnersToInsert Size =  ' + listPartnersToInsert.size()) ;
            Database.insert(listPartnersToInsert, false);
            
        }
        if(! ocListtoUpdate.isEmpty()){
            try{
                system.debug('OC LIST Size =  ' + ocListtoUpdate.size())    ;
                Database.update(ocListtoUpdate, false);
            } catch(Exception ex){
                system.debug('Ex =>' + ex.getMessage() + 'OC LIST Size =  ' + ocListtoUpdate.size())    ;
                throw ex;
            }
        }
        if(! ocListtoAdd.isEmpty()){
            try{
                system.debug('ocListtoAdd Size =  ' + ocListtoAdd.size())   ;
                Database.insert(ocListtoAdd, false);
            } catch(Exception ex){
                system.debug('Ex =>' + ex.getMessage() + 'ocListtoAdd Size =  ' + ocListtoAdd.size())   ;
                throw ex;
            }
        }
        if(! billingContactOCListtoUpdate.isEmpty()){
            try{
                 system.debug('billingContactOCListtoUpdate Size =  ' + billingContactOCListtoUpdate.size())    ;
                Database.update(billingContactOCListtoUpdate, false);
            } catch(Exception ex){
                system.debug('Ex =>' + ex.getMessage() + 'billingContactOCListtoUpdate Size =  ' + billingContactOCListtoUpdate.size()) ;
                throw ex;
            }
        }
        if(! billingContactOCListtoAdd.isEmpty()){
            try{
                  system.debug('billingContactOCListtoAdd Size =  ' + billingContactOCListtoAdd.size()) ;
                Database.insert(billingContactOCListtoAdd, false);
            } catch(Exception ex){
                system.debug('Ex =>' + ex.getMessage() + 'billingContactOCListtoAdd Size =  ' + billingContactOCListtoAdd.size())   ;
                throw ex;
            }
        }

    }

    @future
    public static void closeIODetailOnOpportunityClose(Set<Id> closedOppIds){        
                 
        // query all IO_Detail__c records assocaited to the Closed Lost Opportunties, and update the IO_Detail__c status to "Canceled"
        List<IO_Detail__c> listToUpdate = new List<IO_Detail__c>();
        if(!closedOppIds.isEmpty()){
            for(IO_Detail__c iod:[Select Id, Status__c 
                                    From IO_Detail__c 
                                    Where Opportunity__c IN :closedOppIds 
                                    And Status__c != 'Canceled']){
                iod.Status__c = 'Canceled';
                listToUpdate.add(iod);
            }
        }
        
               
        if(!listToUpdate.isEmpty()){
            try{
                update listToUpdate;
            } catch (DMLException e) {
                // TODO Add exception reporting
                throw e;
            }
        }
    }

    /*
    *@Description : This method is used to create/ cofigure appropriate Line items and insert the Lin items
    *@Modified By : ABhishek
    *             : Moved DML to Util  
    */

    public static void autoCreateProductsHelper(Map<Id, List<OpportunityLineItem>> mapOfOpportunityProducts,Map<id,Opportunity> newOpptyMap){
        OpportunityTriggerUtil util = new OpportunityTriggerUtil();
        if(! mapOfOpportunityProducts.isEmpty()) {

            List<PricebookEntry> listOfPE = OpportunityTriggerUtil.getPriceBookEntryRecords();            
            List<OpportunityLineItem> listOfOpportunityLineItemInsert = new List<OpportunityLineItem>();
            
            for(Id oppId: mapOfOpportunityProducts.keySet()) {
                Opportunity opp = (Opportunity)newOpptyMap.get(oppId);
                Integer idx = 0;            
                for(OpportunityLineItem oli: mapOfOpportunityProducts.get(oppId)) {
                    for(PricebookEntry pe: listOfPE) {
                        if(opp.CurrencyIsoCode == pe.CurrencyIsoCode ) {
                            oli.PricebookEntryId = pe.Id;
                            if(util.isTaxonomyRT(opp)){
                                oli.UnitPrice = opp.Amount;
                                break;
                            } else {
                                if(pe.Name == 'Web' && idx == 0) {
                                    oli.UnitPrice = .2 * opp.Amount;
                                    break;
                                } else if(pe.Name == 'Mobile') {
                                    oli.UnitPrice = .8 * opp.Amount;
                                    break;
                                }
                            }
                        }
                    }
                    idx ++;
                    listOfOpportunityLineItemInsert.add(oli);
                }            
            }

            OpportunityTriggerUtil.insertOLIRecords(listOfOpportunityLineItemInsert);
        }

    }

    /*
    *@Author      : ABhishek
    *@dEscription : Moved from Helper to Util class , generates default splits for Oppty   
    */

    public static Opportunity_Split__c generateDefaultSplit(Opportunity opportunity){

        Opportunity_Split__c oSplit = new Opportunity_Split__c();
        oSplit.Split__c = 100;
        oSplit.Salesperson__c = opportunity.OwnerId;
        oSplit.Opportunity__c = opportunity.Id;
        oSplit.Opportunity_Owner__c = true;
        oSplit.CurrencyIsoCode = opportunity.CurrencyIsoCode;

        return oSplit;
    }

    public static void insertOLIRecords(List<OpportunityLineItem> listOfOpportunityLineItemInsert){
        
        try{
            
            if(! listOfOpportunityLineItemInsert.isEmpty()) {
                OPT_PROD_STATIC_HELPER.runForProds = true;
                insert listOfOpportunityLineItemInsert;
            }
        
        }catch(DMLException ex){
            throw ex;
        }
    }

    public static List<PriceBookEntry> getPriceBookEntryRecords(){

            return [SELECT p.Id, p.Name, p.UnitPrice, p.CurrencyIsoCode 
                FROM PricebookEntry p 
                WHERE IsActive=true 
                AND Name IN ('Web', 'Mobile') 
                AND Pricebook2.Name = 'Standard Price Book' 
                Order By Name desc];
    }

    public static Messaging.SingleEmailMessage sendEmailTemplate(List<String> toMail, Id templateId, Id targetTemplateId, Id ec){
        Messaging.SingleEmailMessage email = new Messaging.SingleEmailMessage();
        email.setToAddresses(toMail);
        email.setTemplateId(templateid);
        email.setTargetObjectId(targetTemplateId);
        email.setWhatId(ec);  
        email.setSaveAsActivity(false);  
        return email;
    } 

    private static List<TaxonomyRecordTypes__c> taxonomyRCLst;
    private static List<RecordType> taxRCtypeLst;

    /* Helper to determine whether the Oppty should use updated Taxonomy layouts */
    public Set<String> setTaxonomyRTs{
        get{
            if(setTaxonomyRTs == null||setTaxonomyRTs.isEmpty()){
                setTaxonomyRTs = new Set<String>();
                
                if(taxonomyRCLst == null)
                    taxonomyRCLst = [Select Name from TaxonomyRecordTypes__c];
                for(TaxonomyRecordTypes__c t:taxonomyRCLst){
                    setTaxonomyRTs.add(t.Name);
                }
                
                if(taxRCtypeLst == null)
                    taxRCtypeLst = [Select Id from RecordType where DeveloperName IN :setTaxonomyRTs];
                
                for(RecordType r:taxRCtypeLst){
                    setTaxonomyRTs.add(r.Id);
                }
            }
            return setTaxonomyRTs;
        }
        set;
    }
    public Boolean isTaxonomyRT(Opportunity objOpp){
        return setTaxonomyRTs.contains(objOpp.RecordTypeId);
    }
    /*
    Method: To invoke the CV process
    */
    @future(callout=true)
    public static void initiateAccountCV(List<Id> acctIDs){
     String acctId;
     String returnVal;
     Map<String,String> resultMap = new Map<String,String>();
     for (Integer i = 0; i < acctIDs.size(); i++) {
        acctID = acctIDs.get(i);
        try{
            returnVal = creditVerificationInitiation.ApplyForCredit(acctId, '');
            system.debug('Result of this CV: '+returnVal);
            resultMap.put(acctID, returnVal);
        }
        catch(Exception e){
            system.debug('Error occurred in CV process: '+e);
        }
        system.debug('Final result of CV process: '+resultMap);        
     }
    }
     /// Note: this method checkAndInitiateAccountCV method should be moved to 
    /// TriggerOnIOApproval related handler and helper class

    /*
      Method: Used from TriggerOnIOApproval.trigger
        Desc: Check for CV on the Account 
        If it does not exist then submit for Credit Check and create CV 
        If a CV does exist then do nothing
        Note: The reason this was seperated from the above similar method was to avoid a extra soql on Account and also to remove the dependency for any future enhancements.
    */
   /* public static void checkAndInitiateAccountCV(List<Opportunity> opportunities, 
                                            Map<Id,Opportunity> oldOpportunityMap, 
                                            Map<Id,Opportunity> newOpportunityMap)  {
         String acctId;
         List<Id> acctIds = new List<Id>();
         
         if(UserInfo.getProfileId().substring(0,15)!=Custom_Constants.IntegrationProfileId)
         {
             for(Opportunity objOpp : opportunities){
                 
                system.debug(' objOpp.Account.Credit_Verification2__c / Opp record = '+objOpp.Account.Credit_Verification2__c+' / '+objOpp);
                
                if(objOpp.probability >= 90 && objOpp.Account.Credit_Verification2__c == null && objOpp.Account.RecordType.Name != 'Vendor'){
                       system.debug('objOpp.Account.Credit_Verification2__c = '+objOpp.Account.Credit_Verification2__c);
                       acctId = objOpp.AccountId;
                       acctIds.add(acctId);
                    }
             }
             if(acctIds.size() > 0){
                 if(acctIds.size() < 10){
                     OpportunityTriggerUtil.initiateAccountCV(acctIds);
                     system.debug('CV request initiated');
                 }
                 else
                     system.debug('Cannot process CV for this set due to a salesforce limitation. Please process them individually (via the Check Credit button) OR reduce the batch size to a number less than or equal to 10.');
             }
             else
                     system.debug('No accts were eligible for a CV!');
        }
    }*/

    public static List<Case> getCasesFromOppty(Set<Id> accountOpptyIdSet,Set<Id> agencyOpptyIdSet){

        return [SELECT Id,AccountId,Ad_Agency__c,Opportunity__c  
                                FROM Case  
                                WHERE (Opportunity__c IN : accountOpptyIdSet
                                        OR Opportunity__c IN : agencyOpptyIdSet) 
                                        AND recordType.developerName IN : 
                                            OpportunityTriggerUtil.RT_CASE_NAMES ];

                                            

    }

    public static List<Case> getUpdatedCaseOnOpportunityAccountChange( Map<Id, Id> mapOppIdToAccountId, Map<Id, Id> mapOppIdToAgencyId){

        if(!mapOppIdToAccountId.isEmpty() ||  !mapOppIdToAgencyId.isEmpty()) {
            List<Case> caseListToUpdate = new List<Case> ();
            for(Case caseRec: (List<Case>) OpportunityTriggerUtil.getCasesFromOppty(mapOppIdToAccountId.keySet(), mapOppIdToAgencyId.keySet())){
                Boolean updateCase = false;
                if(mapOppIdToAccountId.containsKey(caseRec.Opportunity__c)) {
                    caseRec.AccountId = mapOppIdToAccountId.get(caseRec.Opportunity__c);
                    updateCase = true;
                }
                
                if(mapOppIdToAgencyId.containsKey(caseRec.Opportunity__c)) {
                    caseRec.Ad_Agency__c = mapOppIdToAgencyId.get(caseRec.Opportunity__c);
                    updateCase = true;
                }
                
                if(updateCase) {
                    caseListToUpdate.add(caseRec);
                }
            }
            
            return caseListToUpdate;
        }

        return null;
    }  

   // @future(callout=true)
    public static void insertSplitRecords(String opptySplitLstJSON){
        
        try{

            List<Opportunity_Split__c> opportunitySplitList = (List<Opportunity_Split__c>) JSON.deserialize(opptySplitLstJSON, List<Opportunity_Split__c>.class);
            if(!opportunitySplitList.isEmpty()){
                insert opportunitySplitList;
            }
        }catch(Exception ex){
            throw ex;
        }        
    }

    public static void upsertAccounts(List<Account> accList){
        try{
            if (!accList.isEmpty()) {
                upsert accList;
            } 
        }catch(Exception ex){
            throw ex;
        }  
    }

    public static List<Account> getAcctsToUpdate(Map<Id, Opportunity> mapOfOpp,Map<Id, Id> mapOfAccountIdToAgency){

        try{

            return [SELECT Id,Last_Opportunity_Created_Date__c 
                        FROM Account
                        WHERE (Id IN :mapOfOpp.keySet() 
                        OR Id IN :mapOfAccountIdToAgency.values()) 
                        AND RecordType.Name != 'Vendor'];

        }catch(QueryException ex){
            throw ex;
        }        
    }

    public static List<Aggregateresult> getAggregatedAccts(Map<Id, Opportunity> mapOfOpp){
        try{

            return [SELECT AccountId accId ,Max(CreatedDate) 
                       FROM Opportunity 
                       WHERE AccountId = :mapOfOpp.keySet() 
                       AND Account.RecordType.Name != 'Vendor' 
                       GROUP BY AccountId];
    
        }catch(QueryException ex){
            throw ex;
        }
        
    }

    public static List<Aggregateresult> getAcctGroupByUpfront(Map<Id, Opportunity> mapOfWonOppForUpfront){
        try{

            return [SELECT SUM(Amount) amt, Upfront__c upId 
                            FROM Opportunity 
                            WHERE Upfront__c=: mapOfWonOppForUpfront.keySet() 
                            AND StageName = 'Closed Won' 
                            Group By Upfront__c]; 

        }catch(Exception ex){
            throw ex;
        }
    } 

    public static List<Account> getAcctsAndrelatedoppties(Set<id> AcctIdSet){

        try{

            return [SELECT Id, Last_Close_Date__c, Last_Closed_Won_Date__c, 
                                    Last_Close_Won_Opportunity_Name__c,  
                                    (SELECT Id, Name, CloseDate, 
                                            StageName 
                                    FROM Opportunities1__r 
                                    WHERE CloseDate != null 
                                    ORDER BY CloseDate desc), 
                                    (SELECT Id, Name,CloseDate, 
                                            StageName 
                                    FROM Opportunities 
                                    WHERE CloseDate != null 
                                    ORDER BY CloseDate desc) 
                            FROM Account
                            WHERE Id In: AcctIdSet];
        
        }catch(EXception ex){
            throw ex;
        }
    }

    @future
    public static void updateUpfrontRecords(String jsonString){
        try{

            List<Upfront__c> listofUpfrontToUpdate = new List<Upfront__c>();
            listofUpfrontToUpdate =  (List<Upfront__c>) JSON.deserialize(jsonString, List<Upfront__c>.class);

            if(! listofUpfrontToUpdate.isEmpty()) {
                update listofUpfrontToUpdate;    
            }

        }catch(DMLException ex){
            throw ex;
        }
        
    }

    public static void updateCaseRecords(List<Case> caseUpdateList){

        if(caseUpdateList != null && caseUpdateList.size()>0){
           try{
               update caseUpdateList; 
           }catch(Exception e){
               throw e;  
           }
        } 
    }

    public static List<opportunity> getOpptyAndCase(Set<Id> oppIds){

       try{ 
       
            return [Select (Select Id,Record_Type__c, Opportunity_Owner__c From Cases__r where Record_Type__c =: CAMP_Util.RT_CC OR Record_Type__c =: COGS_Util.RT_CAC) 
                    , o.Owner.Name, o.OwnerId 
                    From Opportunity o 
                    where id in :oppIds];

        }catch(EXception ex){
            throw ex;
        }

    }

    public static List<SObject> queryDataByField(String sobjectTypeStr, List<String> fieldsToQueryLst,String fieldToCheck, Set<String> idLst ){
    
        String SOQL_Str = 'SELECT ';

        // parse the Fields list
        for(String fieldStr : fieldsToQueryLst){
            SOQL_Str += (fieldStr+',');
        }

        SOQL_Str = SOQL_Str.substring(0,SOQL_Str.length()-1); // to remove beginning and trailing , 

        SOQL_Str += ' FROM '+sobjectTypeStr;

        system.debug('idLst --->'+idLst);
        if(idLst != null && idLst.size() > 0){
            SOQL_Str += ' where '+fieldToCheck+' IN (';

            String idLstStr = '';
            for(String idVar : idLst){
                system.debug(idVar);
                if(idVar != null)
                idLstStr += ('\'' + idVar + '\',');
            }

            if(idLstStr.length() > 0)
            SOQL_Str += (idLstStr).substring(0, (idLstStr).length()-1);
            SOQL_Str += ' )';
        }

        Integer queryRowLimit = Limits.getLimitQueryRows() - Limits.getQueryRows();
        SOQL_Str += ' Limit '+queryRowLimit;
        system.debug('SOQL :: '+SOQL_Str);

        if(Limits.getLimitQueries() > Limits.getQueries())
            return Database.query(SOQL_Str);        
        else{

            Logger.logMessages(new List<Error_Log__c> {new Error_Log__c(Class__c = 'OpportunityTriggerUtil',
                        Method__c = 'queryDataByField',
                        Error__c = 'Max SOQL REached',
                        Object__c = 'Opportunity',
                        ID_List__c = null,
                        Running_user__c = UserInfo.getUserId(),
                        Type__c = 'Error'
                    )});
    
            return null;
        }
      
      
    }

    public static Splits_Approval__c generateSplitsApproval(String splitId, DateTime startDate, Date firstDayOfMonth, Id opptyId, String salesPersonName){

        Splits_Approval__c approvalVar = new Splits_Approval__c();
        approvalVar.Split__c = splitId;
        approvalVar.Start_Date__c = startDate;
        approvalVar.Effective_Month__c = firstDayOfMonth ;
        System.debug('====$$$$$$$'+approvalVar.Effective_Month__c+'====='+firstDayOfMonth);
        approvalVar.End_Date__c =approvalVar.Effective_Month__c;                  
        approvalVar.Opportunity__c = opptyId ;
        approvalVar.Name__c = salesPersonName ;

        return approvalVar;
    }

    public static Map<String, Auto_Assign_Biller__c> getAutoAssignMap(){
        if(mapBillers == null){
            return Auto_Assign_Biller__c.getAll();
        }   

        return mapBillers;  
    }

    public static User getCurrentUser(){
        
        if(loggedInUser == null){
            List<User> userLst = OpportunityTriggerUtil.queryDataByField('User',new List<String>{'Id','Territory__c','Name','Email'},'Id',new Set<String>{UserInfo.getUserid()});
            loggedInUser = (userLst != null)? userLst[0]: null;
        }
        
        return loggedInUser;        
    }
}