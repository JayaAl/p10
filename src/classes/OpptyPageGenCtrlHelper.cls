/**
* ─────────────────────────────────────────────────────────────────────────────────────────────────┐
* This class services lightning action buttons on Opportunity.
* ──────────────────────────────────────────────────────────────────────────────────────────────────
* @author         Jaya Alaparthi
* @maintainedBy   Jaya Alaparthi
* @version        1.0
* @created        2017-04-13
* @modified       
* @systemLayer    Service
* @see            ????
* @see            ????
* ──────────────────────────────────────────────────────────────────────────────────────────────────
* @changes
* v1.1            Jaya Alaparthi
* 2017-05-25      Adding Sbx and Prod Jira urls to Custom settings.
* ──────────────────────────────────────────────────────────────────────────────────────────────────
* @changes
* v1.2            Jaya Alaparthi
* 2017-06-06      making sbx determination as seperate method to use for 
*				Create sponsorship.
* ─────────────────────────────────────────────────────────────────────────────────────────────────┘
*/
public with sharing class OpptyPageGenCtrlHelper {

	public OpptyPageGenCtrlHelper() {
		
	}

	public map<Integer,PageGeneratorWrapper> cloneOpptyPageFields(String sObj, 
																String recordType) {

		map<Integer,PageGeneratorWrapper> displayMap = new map<Integer,PageGeneratorWrapper>();
		list<PageGeneratorDetails__c> pgDetails = new list<PageGeneratorDetails__c>();
		
		pgDetails = OpptyPageGenUtil.retrivefieldsToDisplay(sObj,recordType);
		displayMap = generatorPgWrapperData(pgDetails);
		return displayMap;
	}

	private map<Integer,PageGeneratorWrapper> generatorPgWrapperData(list<PageGeneratorDetails__c> pgGenDetailsList) {

		map<Integer,PageGeneratorWrapper> displayWrapperCollection = new map<Integer,PageGeneratorWrapper>();
		map<String,list<PageGeneratorDetails__c>> sectionHeaderToPGDetails = new map<String,list<PageGeneratorDetails__c>>();
		map<String,list<String>> sectionHeaderToFeildAPIsMap = new map<String,list<String>>();
		map<String,list<PageGeneratorDetails__c>> sectionHeaderSortedDetailsMap = new map<String,list<PageGeneratorDetails__c>>();
		
		if(!pgGenDetailsList.isEmpty()) {

			for(PageGeneratorDetails__c pgDetail : pgGenDetailsList) {

				//list<PageGeneratorDetailWrapper> fieldAPIDetails = new list<PageGeneratorDetailWrapper>();
				list<PageGeneratorDetails__c> fieldAPIDetails =  new list<PageGeneratorDetails__c>();
				// group fields per section header.
				if(sectionHeaderToPGDetails.containsKey(pgDetail.Section_Header__c)) {

					fieldAPIDetails = sectionHeaderToPGDetails.get(pgDetail.Section_Header__c);
				}

				//fieldAPIDetails.add(new PageGeneratorDetailWrapper(pgDetail));
				fieldAPIDetails.add(pgDetail);
				sectionHeaderToPGDetails.put(pgDetail.Section_Header__c,fieldAPIDetails);				
			}
			// sort the list of pgdetails for every secession
			for(String headerStr : sectionHeaderToPGDetails.keySet()) {

				//list<PageGeneratorDetailWrapper> preSortPgDetails = new list<PageGeneratorDetailWrapper>();
				list<PageGeneratorDetails__c> preSortPgDetails = new list<PageGeneratorDetails__c>();
				list<String> sortedFieldAPIsList = new list<String>();
				preSortPgDetails = sectionHeaderToPGDetails.get(headerStr);
				System.debug('before sort preSortPgDetails:'+preSortPgDetails);
				preSortPgDetails.sort();
				System.debug('sfter sort preSortPgDetails:'+preSortPgDetails);
				// put the sorted list back in the sectionHeaderTpPGDetailsMap
				//sectionHeaderToPGDetails.put(headerStr,preSortPgDetails);
				// comment this to hold entire pgenreatordetail inste of string list.
				/*sortedFieldAPIsList = getFieldAPIs(preSortPgDetails);
				System.debug('sortedFieldAPIsList:'+sortedFieldAPIsList);
				sectionHeaderToFeildAPIsMap.put(headerStr,sortedFieldAPIsList);
				System.debug('sectionHeaderToFeildAPIsMap:'+sectionHeaderToFeildAPIsMap);
				*/
				System.debug('sorted list of details:'+preSortPgDetails);
				sectionHeaderSortedDetailsMap.put(headerStr,preSortPgDetails);
				System.debug('sectionHeaderSortedDetailsMap:'+sectionHeaderSortedDetailsMap);
			}

			// Iterate teh sorted order of the preSortPgDetails list and 
			//create date to populate PageGeneratorWrapper
			for(PageGeneratorDetails__c pgDetail : pgGenDetailsList) {

				//displayWrapperCollection
				
				Integer sectionHeaderOrder = Integer.valueOf(pgDetail.Order_of_Section__c);
				String sectionHeaderTitle = pgDetail.Section_Header__c;

				if(!displayWrapperCollection.containsKey(sectionHeaderOrder)) {

					list<String> fieldAPIList = new list<String>();

					/*if(sectionHeaderToFeildAPIsMap.containsKey(sectionHeaderTitle)) {

						fieldAPIList = sectionHeaderToFeildAPIsMap.get(sectionHeaderTitle);
						System.debug('fieldAPIList:'+fieldAPIList);	
						// define PageGeneratorWrapper obj
						PageGeneratorWrapper temp = new PageGeneratorWrapper(sectionHeaderTitle,
														fieldAPIList,sectionHeaderOrder);
						displayWrapperCollection.put(sectionHeaderOrder,temp);
					}*/
					if(sectionHeaderSortedDetailsMap.containsKey(sectionHeaderTitle)) {

						list<PageGeneratorDetails__c> pgdetailsList = new list<PageGeneratorDetails__c>();
						pgdetailsList = sectionHeaderSortedDetailsMap.get(sectionHeaderTitle);
						System.debug('pgdetailsList:'+pgdetailsList);	
						// define PageGeneratorWrapper obj
						PageGeneratorWrapper temp = new PageGeneratorWrapper(sectionHeaderTitle,
														pgdetailsList,sectionHeaderOrder);
						displayWrapperCollection.put(sectionHeaderOrder,temp);
					}
				}
			}
		}
		return displayWrapperCollection;
	}

	//─────────────────────────────────────────────────────────────────────────┐
	// getFieldAPIs: retrive field apis in sorted order from the wrapper 
	//					object's PageGeneratorDetails Object.
	// @param PageGeneratorDetailWrapper  wrappeed PageGeneratorDetail Object.
	// @return list<String> sortedFieldAPIsList
    //─────────────────────────────────────────────────────────────────────────┘
	private list<String> getFieldAPIs(list<PageGeneratorDetails__c> pgDetailWrapper) {

		list<String> sortedFieldAPIsList = new list<String>();

		for(PageGeneratorDetails__c pgDetail : pgDetailWrapper) {

			sortedFieldAPIsList.add(pgDetail.Field_API__c);
		}
		return sortedFieldAPIsList;
	}

}