/**
 * @name: OpportunityTriggerHandler
 * @desc: Used to handle all trigger operation for Opportunity object
 *        Contains method specifc to each context used in the trigger   
 * @author: Abhishek
 * ──────────────────────────────────────────────────────────────────────────────────────────────────
* @changes
* v1.1            Jaya Alaparthi
* 2017-10-05      
* ──────────────────────────────────────────────────────────────────────────────────────────────────
*    
* ─────────────────────────────────────────────────────────────────────────────────────────────────┘
*/
 public with sharing class OpptyTriggerHandler {

    public Boolean isExecuting;
    public Integer size; 
    

    public OpptyTriggerHandler(Boolean isExecuting, Integer size){
        this.isExecuting = isExecuting;
        this.size = size;       
    }

    public void onBeforeInsert(List<Opportunity> newOpptyLst){
        OpportunityTriggerHandlerHelper helperRef = new OpportunityTriggerHandlerHelper();   
        
        //Test Case 1 : This method populates the Opportunity fields with some default values and : Passed 
        // Test Case 2 : Initiate Credit Validation if it does not exist              
        helperRef.checkAndInitiateOpportunityFields(newOpptyLst, null, null);//Added By Sri 

    } 

    public void onAfterInsert(List<Opportunity> newOpptyLst,Map<Id,Opportunity> newMap){
        //if(OpportunityTriggerHandlerHelper.runOnce()){        
            
            // v1.1
            // calling AutoEmailSend
            //AutoEmailSendHelper.createEmailActivites(newOpptyLst,new map<Id,Opportunity>(),true);
            OpportunityTriggerHandlerHelper helperRef = new OpportunityTriggerHandlerHelper();
            // Test Case 1 : Populate the Last CreatedDate of Opportunity on Account : Passed
            helperRef.updateLastCreatedDateOnAcct(newopptylst,null);
            //OpportunityTriggerUtil.updateAccountsAfterUpdate();
            
            // Test Case 2 : Check Credit Approval History and then send email and update Account if necessary
            // Test Case 3 : IO Approval records should be canceled on closing Opportunity
            // Test Case 4 : Generate & Insert Default Split records - Passed 
            // Test Case 5 : Generate & INsert Opportunity Line item records
            helperRef.notifyAccountCreditApprovalExpiry(OpportunityTriggerUtil.CONTEXT_TYPE_INSERT, newOpptyLst, null, newMap);
            // helperRef.closeIODetailOnOpportunityClose(newMap); // Moved to notifyAccountCreditApprovalExpiry for single fr loop execution            
            
            // Test Case 6 : Create Partner and OPportunityContactRole records based on the conditions : Passed - OCR
            helperRef.opptyPartnerRoleOperation(newOpptyLst,null);          
        //}
        
    }

    public void onBeforeUpdate( List<Opportunity> newOpptyLst, Map<Id,Opportunity> oldMap, Map<Id,Opportunity> newMap ){
        
            OpportunityTriggerHandlerHelper helperRef = new OpportunityTriggerHandlerHelper();
            
            //Test Case 1 : This method populates the Opportunity fields with some default values and 
            // Test Case 2 : Initiate Credit Validation if it does not exist     
            if(!System.isBatch() && !System.isFuture())
                helperRef.checkAndInitiateOpportunityFields(newOpptyLst, oldMap, newMap);//Added By Sri 

          // Check :   OpportunityTriggerUtil.checkAndInitiateAccountCV(newOpptyLst, null, null);

            // ForsevaOpportunityUpdate
            // Test Case 3 : Send email notifications from ForeSeva
            // Test Case 4 : Update Accounts
            helperRef.onForsevaOpttyUpdate(newOpptyLst, oldMap, newMap);
            
        
    }   


    public void onAfterUpdate( List<Opportunity> newOpptyLst, Map<Id,Opportunity> oldMap, Map<Id,Opportunity> newMap ){

        //if(OpportunityTriggerHandlerHelper.runOnce()){
            
            OpportunityTriggerHandlerHelper helperRef = new OpportunityTriggerHandlerHelper();

            if( UserInfo.getUserId() != Label.ADXSyncUser ){
                
                // v1.1
                // calling AutoEmailSend
                System.debug('in after update');
               // AutoEmailSendHelper.createEmailActivites(newOpptyLst,oldMap,false);
                // Test Case 2 : Create Split with appropriate Sales Person populated
                // test Case 3 : Evaluate Split types and delete existing and create new Split detail records
                system.debug('before setUpSplitData '+JSON.serialize(newOpptyLst));
                if(!system.isFuture() && !system.isBatch())
                    OpportunityTriggerHandlerHelper.setUpSplitData(JSON.serialize(newOpptyLst));
                else {
                    OpportunityTriggerHandlerHelper.manageSplitDetails(newOpptyLst);    
                }
                system.debug('after setUpSplitData ');
                // Test Case 4 : UPdate Splits based on Oppty changes
                SPLT_OpportunitySplit.updateSplitsOnOpptyChange(oldMap,newMap);
                // Test Case 5 : Send email notifications on Account Credit Approval expiry and Oppty probabiity changes and above 90
                // Test Case 6 :  IO Approval records should be canceled on closing Opportunity
                helperRef.notifyAccountCreditApprovalExpiry(OpportunityTriggerUtil.CONTEXT_TYPE_UPDATE, newOpptyLst, oldMap, newMap);
                // Test Case 7 : update Last close Date and Last Closed Oppty field on Account , once Oppty is closed   
                system.debug('b4 updateAccountOnOppCloscheckAndInitiateOpportunityFieldsed');
                
                helperRef.updateAccountOnOppClosed(newOpptyLst,oldMap,newMap);
                system.debug('after updateAccountOnOppClosed');
                // Test Case 1 : Populate the Last CreatedDate of Opportunity on Account, if Agency is updated
                helperRef.updateLastCreatedDateOnAcct(newOpptyLst,oldMap);
    
                // Update Accounts
                //OpportunityTriggerUtil.updateAccountsAfterUpdate();
                // Test Case 8 : Create Partner and OPportunityContactRole records based on the conditions
                helperRef.opptyPartnerRoleOperation(newOpptyLst,oldMap);
                
                // Test Case 8 : Update AccountId and Agency on Case whenever Oppty is changed
                //****************** Commented the code as this part is no more used and can be removed ***********************//
                //helperRef.updateOwnerOnCase(newOpptyLst,oldMap,newMap);
                //****************** Commented the code as this part is no more used and can be removed ***********************//

                OpportunityTriggerHandlerHelper.updateOpptySplitsonUpdate(newOpptyLst,oldMap,newMap);
                // UPdate Split record if Oppty Owner is changed
                //SPLT_OpportunitySplit.checkOpportunityOwner(oldMap,newMap); // merged the logic to updateSplitsOnOpptyChange
                // Removed the Common Query 
                // UPdate Split record if Oppty Currency is changed
                //SPLT_OpportunitySplit.checkOpportunityCurrencyISO(oldMap,newMap); // merged the logic to updateSplitsOnOpptyChange
                
                //UPdate Case with updated Account after change of Account in Oppty
                //helperRef.updateCaseOnOpportunityAccountChange(newOpptyLst,oldMap,newMap); // merge with updateOwnerOnCase so as to have one dml
                
            }
        //}     
    }

    public void onAfterDelete(Map<Id,Opportunity> oldOpportunityMap) {

        OpportunityTriggerHandlerHelper helperRef = new OpportunityTriggerHandlerHelper();
        // Test Case 1 : Populate the Last CreatedDate of Opportunity on Account, if Oppty is deleted
        helperRef.updateLastCreatedDateOnAcct(oldOpportunityMap.values(),null);
        //OpportunityTriggerUtil.updateAccountsAfterUpdate();
    }

    public void onAfterUnDelete(List<Opportunity> newopptylst){

        OpportunityTriggerHandlerHelper helperRef = new OpportunityTriggerHandlerHelper();
        // Test Case 1 : Populate the Last CreatedDate of Opportunity on Account, if Oppty is Undeleted
        helperRef.updateLastCreatedDateOnAcct(newopptylst,null);
        //OpportunityTriggerUtil.updateAccountsAfterUpdate();
    }  

}