/**
* ─────────────────────────────────────────────────────────────────────────────────────────────────┐
* This class services lightning action buttons on Opportunity.
* ──────────────────────────────────────────────────────────────────────────────────────────────────
* @author         Jaya Alaparthi
* @maintainedBy   Jaya Alaparthi
* @version        1.0
* @created        2017-04-13
* @modified       
* @systemLayer    Service
* @see            ????
* @see            ????
* ──────────────────────────────────────────────────────────────────────────────────────────────────
* @changes
* v1.1            Jaya Alaparthi
* 2017-05-25      Adding Sbx and Prod Jira urls to Custom settings.
* ──────────────────────────────────────────────────────────────────────────────────────────────────
* @changes
* v1.2            Jaya Alaparthi
* 2017-06-06      making sbx determination as seperate method to use for 
*				Create sponsorship.
* ─────────────────────────────────────────────────────────────────────────────────────────────────┘
*/
public with sharing class PageGeneratorWrapper {

	public String sectionHeader {get;set;}
	public list<LayoutColumDetails> layoutColumn1 {get;set;}
	/*public list<LayoutColumDetails> layoutColumn2 {get;set;}
	public Integer sectionOrder {get;set;}
 	public PageGeneratorWrapper(String sectionHeader, 
 								list<PageGeneratorDetails__c> fields, 
 								Integer sectionOrder ) {
		
		this.sectionHeader = sectionHeader;
		this.fieldAPIs = fields;
		this.sectionOrder = sectionOrder;
	}*/
	public PageGeneratorWrapper(){}
	public class LayoutColumDetails {

		public LayoutColumDetails(){}
		public String fieldAPI {get;set;}
		public String fieldBehavior {get;set;}
		public String fieldLabel {get;set;}
	}
	// change PageGeneratorDetail__c should be replaced by
	// the two LayoutColumns. i.e LayoutColumn1 and LayoutColumn2 are two member
	// varibles of the inner class. This layoutColumn1 should contain Field API and 
	// behavior field values. LayoutColumns should be a inner class.
}