/**
 * @name:   PandoraLightningUtils
 * @desc:	Utils class for retriving data for lightning custom buttons or custom links.
 * 			This is part of the converting to lightning process.
* ─────────────────────────────────────────────────────────────────────────────────────────────────┐
* @createdBy     Jaya Alaparthi <JAlaparthi@pandora.com>
* @version       1.0
* @createdDate   2016-12-06
* ─────────────────────────────────────────────────────────────────────────────────────────────────
* @Changes
* ─────────────────────────────────────────────────────────────────────────────────────────────────┘
*/
public with sharing class PandoraLightningUtils {

    @AuraEnabled
    public static String getPSessionID() {
        
        String sessionId = '';
        sessionId = UserInfo.getSessionId();
        return sessionId;
    }
    @AuraEnabled
    public static String getPOrgId() {
        
        String orgId = '';
        orgId = UserInfo.getOrganizationId();
        return orgId;
    }
    @AuraEnabled
    public static String opportunityToCase(Id opptyId){
        
        list<LightningWrapper> caseList = new list<LightningWrapper>();
        //old return typelist<LightningWrapper>
        LightningWrapper wrapperCase = new LightningWrapper();
        Case caseObj = new Case();
        // get case billing record typeId
        Id caseRtId =  Schema.Sobjecttype.Case.getRecordTypeInfosByName().get('Billing Case').getRecordTypeId();
        caseObj.RecordTypeId = caseRtId;
        caseObj.Subject = '';
        // get ad Ops castel information
        Ad_Operations__c adOpsRec = new Ad_Operations__c();
        
        try {
            adOpsRec = [SELECT Id, Name,
                           DFP_Order_ID__c, 
                           Billing_Ad_Server__c
                    FROM Ad_Operations__c
                    WHERE Opportunity__c = :opptyId
                    ORDER BY CreatedDate Desc LIMIT 1];
        } catch(Exception e) {

            caseObj.DFP_Order_ID__c = 'blabla';
            caseObj.Billing_Ad_Server__c = 'blablabla address';
            System.debug('Caught exception:'+e.getMessage());
        }
        if(adOpsRec.Id != null) {

            caseObj.Ad_Ops__c = adOpsRec.Id;
            caseObj.DFP_Order_ID__c = adOpsRec.DFP_Order_ID__c;
            caseObj.Billing_Ad_Server__c = adOpsRec.Billing_Ad_Server__c;
            System.debug('record returned:'+adOpsRec);
        }
        wrapperCase = new LightningWrapper(caseObj,'Billing Case',adOpsRec.Name);
        caseList.add(wrapperCase);
        return JSON.serializePretty(wrapperCase);
    }
}