@istest
global with sharing class Pandora_AccessTokenCalloutMock implements HttpCalloutMock{
	

	global HTTPResponse respond(HTTPRequest req) {

		HttpResponse res = new HttpResponse();
        res.setHeader('Content-Type', 'application/json');
        res.setBody('{ "access_token": "ya29.CjGFA4-MO3HUpJ-NWP7jI_jd236fq_7pVQYVnUw0EMnF-g4Yec0R41W3yMdH1XuIi14W","token_type": "Bearer","expires_in": "3600"}');
        res.setStatusCode(200);
        return res; 

	}
}