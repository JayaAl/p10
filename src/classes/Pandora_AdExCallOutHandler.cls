/*
   string accesstoken='';
   Pandora_AdExCallOutHandler.doCallOut(accesstoken);

*/

/*
   Handles the actual call out. The Rest call out URLs are coded in this class.
*/

public with sharing class Pandora_AdExCallOutHandler {
    
    public static ADXJson doCallOut(string AccessToken,String EndPontURL)
    {
     // googleauthclass oauth = new googleauthclass();
        // oauth.showtoken();
        // system.debug ( oauth.bodyprint );
        //String REPORT_BASE_URL = 'https://www.googleapis.com/adexchangeseller/v1/reports?';
        String REPORT_BASE_URL ='https://www.googleapis.com/adexchangeseller/v1.1/reports?';
        String START_DATE = 'startDate=2016-09-01&';
        String END_DATE = 'endDate=2016-09-01&';
        /*String ENDPOINT_URL_REPORT = REPORT_BASE_URL + START_DATE + END_DATE +
                                    'dimension=DATE&dimension=AD_CLIENT_ID&metric=AD_REQUESTS&metric=Ad_Impressions&metric=CLICKS&metric=EARNINGS&fields=headers%2Crows%2CtotalMatchedRows%2Ctotals%2Cwarnings';
        
        */  
        /*String ENDPOINT_URL_REPORT='https://www.googleapis.com/adexchangeseller/v1.1/reports?'+
        'startDate=2016-09-02&endDate=2016-09-02'+
        '&dimension=DATE&dimension=ADVERTISER_NAME&dimension=AD_TAG_NAME&dimension=AD_TAG_CODE&dimension=DEAL_ID&dimension=AD_CLIENT_ID'+
        '&metric=AD_REQUESTS&metric=AD_IMPRESSIONS&metric=CLICKS&metric=EARNINGS'+
        '&fields=headers%2Crows%2CtotalMatchedRows%2Ctotals%2Cwarnings';*/
 
         /*String ENDPOINT_URL_REPORT='https://www.googleapis.com/adexchangeseller/v1.1/reports?'+
        'startDate=2016-09-02&endDate=2016-09-02'+
        '&dimension=ADVERTISER_NAME&dimension=AD_TAG_NAME&dimension=DEAL_ID'+
        '&metric=AD_IMPRESSIONS&metric=EARNINGS'+
         */

         //09/06 Adding few more dimensions and metrics to the report.

         /*String ENDPOINT_URL_REPORT='https://www.googleapis.com/adexchangeseller/v1/reports?'+
        'startDate=2016-09-02&endDate=2016-09-02'+
        '&dimension=ADVERTISER_NAME&dimension=AD_TAG_NAME&dimension=DEAL_ID&dimension=BRANDING_TYPE_NAME&dimension=TRANSACTION_TYPE_NAME&dimension=DATE'+
        '&metric=AD_IMPRESSIONS&metric=EARNINGS&metric=CLICKS'+
        '&startIndex=0&maxResults=2000'+
        '&fields=headers%2Crows%2CtotalMatchedRows%2Ctotals%2Cwarnings';*/

         //09/09 working.

         /*String ENDPOINT_URL_REPORT='https://www.googleapis.com/adexchangeseller/v1.1/reports?'+
        'startDate=2016-09-02&endDate=2016-09-02'+
        '&dimension=ADVERTISER_NAME&dimension=AD_TAG_NAME&dimension=DEAL_ID&dimension=BRANDING_TYPE_NAME&dimension=TRANSACTION_TYPE_NAME&dimension=DATE'+
        '&metric=AD_IMPRESSIONS&metric=EARNINGS&metric=CLICKS'+
        '&startIndex='+StartIndex+'&maxResults='+MaxResults+
        '&fields=headers%2Crows%2CtotalMatchedRows%2Ctotals%2Cwarnings';*/

        /*String ENDPOINT_URL_REPORT='https://www.googleapis.com/adexchangeseller/v1.1/reports?'+
        'startDate=2016-09-18&endDate=2016-09-18'+
        '&dimension=ADVERTISER_NAME&dimension=AD_TAG_NAME&dimension=DEAL_ID&dimension=BRANDING_TYPE_NAME&dimension=TRANSACTION_TYPE_NAME&dimension=DATE'+
        '&metric=AD_IMPRESSIONS&metric=EARNINGS&metric=CLICKS'+
        //'&startIndex='+StartIndex+'&maxResults='+MaxResults+
        //'&filter=TRANSACTION_TYPE_NAME%3D%3DPreferred+deal%2CTRANSACTION_TYPE_NAME%3D%3DPrivate+auction'+
        '&filter=TRANSACTION_TYPE_NAME%3D%3DPreferred+deal'+
        '&fields=headers%2Crows%2CtotalMatchedRows%2Ctotals%2Cwarnings';*/

        String ENDPOINT_URL_REPORT=EndPontURL;
        //getEndPointURL_Preferred_PrivateAuctionDeals();


        /*
          &filter=TRANSACTION_TYPE_NAME%3D%3DPreferred+deal%2CTRANSACTION_TYPE_NAME%3D%3DPrivate+auction
        */

        PageReference pref= new PageReference(ENDPOINT_URL_REPORT);
        HttpRequest req = new HttpRequest();
        req.setEndpoint(ENDPOINT_URL_REPORT);
        req.setHeader('Authorization','Bearer ' + accessToken);
        req.setMethod('GET');
        
        req.setTimeOut(120000);
        
        Http h= new Http();
        
        HttpResponse res = h.send(req);
        ADXJson adx = (ADXJson) JSON.deserialize(res.getBody(), ADXJson.class);
        system.debug(logginglevel.info,'HEADERS' +  adx.headers);
        system.debug(logginglevel.info,'ROWS' +  adx.rows );
        system.debug('-->' + adx.totalMatchedRows);
        return adx;
        

    }

    public static Pandora_RefreshTokenResponse  getNewAccessToken()
    {
        
        String finalURL='https://www.googleapis.com/oauth2/v4/token?client_secret=o4Tk-CtoGA15r1BFmZrj9uFw&grant_type=refresh_token&refresh_token=1%2FHLndW9MUFO8Q4Ut3zhM5p5nnJe7UUkdVLws2EVos-yk'+
                 '&client_id=723789058145-miooatskvlrihp0i8r620uvc2v0gu8k2.apps.googleusercontent.com';
        

        system.debug('Final URL'+finalURL);
        
        HTTP H= new HTTP();
        HttpRequest req= new HttpRequest();
        req.setHeader('Content-Type', 'application/x-www-form-urlencoded');
        req.setEndpoint(finalURL);
        req.setMethod('POST');
        req.setBody('');

        HttpResponse Res= H.send(req);

        system.debug('Response Body:'+Res.getBody());

        Pandora_RefreshTokenResponse refreshTokenResponse=(Pandora_RefreshTokenResponse) JSON.deserialize(Res.getBody(), Pandora_RefreshTokenResponse.class);
        return refreshTokenResponse;
      

    }
    
    public Pandora_AdExCallOutHandler() {
        
    }
    
   

    public static string getEndPointURL_Preferred_PrivateAuctionDeals()
    { 
        //Date dt= date.today().addDays(-1);
        Date dt= Pandora_RevenueLoadBatchHelper.getDate();
        system.assert(dt!=null, 'Date Is Required. Check settings'); 

        /*String ENDPOINT_URL_REPORT='https://www.googleapis.com/adexchangeseller/v1.1/reports?'+
        'startDate='+getDateForReportURL(dt)+'&endDate='+getDateForReportURL(dt)+
        '&dimension=ADVERTISER_NAME&dimension=AD_TAG_NAME&dimension=DEAL_ID&dimension=BRANDING_TYPE_NAME&dimension=TRANSACTION_TYPE_NAME&dimension=DATE'+
        '&metric=AD_IMPRESSIONS&metric=EARNINGS&metric=CLICKS'+
        '&filter=TRANSACTION_TYPE_NAME%3D%3DPreferred+deal%2CTRANSACTION_TYPE_NAME%3D%3DPrivate+auction'+
        '&fields=headers%2Crows%2CtotalMatchedRows%2Ctotals%2Cwarnings';*/

        // replacing adtags with dfpadunit

         String ENDPOINT_URL_REPORT='https://www.googleapis.com/adexchangeseller/v1.1/reports?'+
        'startDate='+getDateForReportURL(dt)+'&endDate='+getDateForReportURL(dt)+
        '&dimension=ADVERTISER_NAME&dimension=DFP_AD_UNIT_ID&dimension=DEAL_ID&dimension=BRANDING_TYPE_NAME&dimension=TRANSACTION_TYPE_NAME&dimension=AD_UNIT_SIZE_NAME&dimension=DATE'+
        '&metric=AD_IMPRESSIONS&metric=EARNINGS&metric=CLICKS'+
        '&filter=TRANSACTION_TYPE_NAME%3D%3DPreferred+deal%2CTRANSACTION_TYPE_NAME%3D%3DPrivate+auction'+
        '&fields=headers%2Crows%2CtotalMatchedRows%2Ctotals%2Cwarnings';

        return ENDPOINT_URL_REPORT;


    }

    public static String getEndPointURL_OpenAcutionDeals()
    {
         //Date dt= date.today().addDays(-1);
         Date dt= Pandora_RevenueLoadBatchHelper.getDate();
         system.assert(dt!=null, 'Date Is Required. Check settings'); 


        
        /*String ENDPOINT_URL_REPORT='https://www.googleapis.com/adexchangeseller/v1.1/reports?'+
        'startDate='+getDateForReportURL(dt)+'&endDate='+getDateForReportURL(dt)+
        '&dimension=AD_TAG_NAME&dimension=DEAL_ID&dimension=BRANDING_TYPE_NAME&dimension=TRANSACTION_TYPE_NAME&dimension=DATE'+
        '&metric=AD_IMPRESSIONS&metric=EARNINGS&metric=CLICKS'+
        '&filter=TRANSACTION_TYPE_NAME%3D%3DOpen+auction%2CTRANSACTION_TYPE_NAME%3D%3DFirst+look'+
        '&startIndex=0&maxResults=3000'+
        
        '&fields=headers%2Crows%2CtotalMatchedRows%2Ctotals%2Cwarnings';*/

        //replacing adtags with dfp ad unit.

        String ENDPOINT_URL_REPORT='https://www.googleapis.com/adexchangeseller/v1.1/reports?'+
        'startDate='+getDateForReportURL(dt)+'&endDate='+getDateForReportURL(dt)+
        '&dimension=DFP_AD_UNIT_ID&dimension=DEAL_ID&dimension=BRANDING_TYPE_NAME&dimension=TRANSACTION_TYPE_NAME&dimension=AD_UNIT_SIZE_NAME&dimension=DATE'+
        '&metric=AD_IMPRESSIONS&metric=EARNINGS&metric=CLICKS'+
        '&filter=TRANSACTION_TYPE_NAME%3D%3DOpen+auction%2CTRANSACTION_TYPE_NAME%3D%3DFirst+look'+
        '&startIndex=0&maxResults=3000'+
        
        '&fields=headers%2Crows%2CtotalMatchedRows%2Ctotals%2Cwarnings';


      
        return ENDPOINT_URL_REPORT;
        
    }

    public static string getDateForReportURL(Date Dt)
    {
        //2016-09-01 date in this format.
        //Pandora_AdExCallOutHandler.getDateForReportURL(date.today().addDays(-1));

        //Date Dt= date.today().addDays(-1);
        Integer Day=dt.Day();
        Integer Month=Dt.month();
        Integer Year=Dt.Year();

        string monthstr= Month<10?'0'+Month:''+Month;
        string daystr= Day<10?'0'+Day:''+Day;

        string str=''+Year+'-'+monthstr+'-'+daystr;

        return str;
    }

    
}

/*
  //BRANDING_TYPE_NAME,TRANSACTION_TYPE_NAME
  //CLICKS
*/

/*
GET https://www.googleapis.com/adexchangeseller/v1/reports?startDate=2016-09-18&endDate=2016-09-18
&dimension=ADVERTISER_NAME&dimension=AD_TAG_NAME&dimension=DEAL_ID
&dimension=TRANSACTION_TYPE_NAME
&filter=TRANSACTION_TYPE_NAME%3D%3DPreferred+deal%2CTRANSACTION_TYPE_NAME%3D%3DPrivate+auction
&metric=Earnings&key={YOUR_API_KEY}
*/