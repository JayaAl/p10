@isTest
global with sharing class Pandora_AdExCallOutMock implements HttpCalloutMock {

	global HTTPResponse respond(HTTPRequest req) {
        // Create a fake response.
        // Set response values, and 
        // return response.
        //Pandora_testAdExIntegration.createAdXDeals();

        /*String ADVERTISER_NAME,
                   String DFP_AD_UNIT_ID,
                   String DEAL_ID, 
                   String BRANDING_TYPE_NAME,
                   String TRANSACTION_TYPE_NAME,
                   String AD_UNIT_SIZE_NAME
                   String reportDate,
                   String AD_IMPRESSIONS, 
                   String EARNINGS, 
                   String CLICKS, */

        List<ADXJSON.Headers> headerList= new List<ADXJSON.Headers>();
        
       
        //ADVERTISER_NAME
        ADXJSON.Headers adv_name= new ADXJSON.Headers();
        adv_name.name='adv_name';
        adv_name.type_Z='Dimension';
        headerList.add(adv_name);

        //DFP_AD_UNIT_ID

        ADXJSON.Headers DFP_AD_UNIT_ID= new ADXJSON.Headers();
        DFP_AD_UNIT_ID.name='adv_name';
        DFP_AD_UNIT_ID.type_Z='Dimension';
        headerList.add(DFP_AD_UNIT_ID);

        //DEAL_ID
        ADXJSON.Headers DEAL_ID= new ADXJSON.Headers();
        DEAL_ID.name='adv_name';
        DEAL_ID.type_Z='Dimension';
        headerList.add(DEAL_ID);


        //BRANDING_TYPE_NAME
        ADXJSON.Headers BRANDING_TYPE_NAME= new ADXJSON.Headers();
        BRANDING_TYPE_NAME.name='adv_name';
        BRANDING_TYPE_NAME.type_Z='Dimension';
        headerList.add(BRANDING_TYPE_NAME);


        //TRANSACTION_TYPE_NAME
        ADXJSON.Headers TRANSACTION_TYPE_NAME= new ADXJSON.Headers();
        TRANSACTION_TYPE_NAME.name='adv_name';
        TRANSACTION_TYPE_NAME.type_Z='Dimension';
        headerList.add(TRANSACTION_TYPE_NAME);

        //AD_UNIT_SIZE_NAME
        ADXJSON.Headers AD_UNIT_SIZE_NAME= new ADXJSON.Headers();
        AD_UNIT_SIZE_NAME.name='adv_name';
        AD_UNIT_SIZE_NAME.type_Z='Dimension';
        headerList.add(AD_UNIT_SIZE_NAME);

         //Date
        ADXJSON.Headers ddate= new ADXJSON.Headers();
        ddate.name='Date';
        ddate.type_Z='Dimension';
        headerList.add(ddate);


        //AD_IMPRESSIONS
        ADXJSON.Headers impressions= new ADXJSON.Headers();
        impressions.name='impressions';
        impressions.type_Z='Metric';
        headerList.add(impressions);
        
        //earnings
        ADXJSON.Headers earnings= new ADXJSON.Headers();
        earnings.name='earnings';
        earnings.type_Z='Metric';
        headerList.add(earnings);

        //CLICKS
        ADXJSON.Headers CLICKS= new ADXJSON.Headers();
        CLICKS.name='earnings';
        CLICKS.type_Z='Metric';
        headerList.add(CLICKS);


        



        
        ADXJSON adXJsonResponseObj= new ADXJSON();
        adXJsonResponseObj.headers=headerList;

        adXJsonResponseObj.totalMatchedRows='10';
        adXJsonResponseObj.totals=new List<String>{'100','1000','100'};
        adXJsonResponseObj.rows= new List<List<String>>{new List<String>{'Allstate',
                                                                         '115723456',
                                                                         '217059211824081026',
                                                                         'Branded',
                                                                         'Preferred deal',
                                                                         '300x250',
                                                                         '2016-07-15',
                                                                         '11',
                                                                         '0.05',
                                                                         '0'},
                               						 new List<String>{'Allstate',
   '115727536',
   '217059211824081026',
   'Branded',
   'Preferred deal',
   '300x250',
   '2016-07-15',
   '461',
   '1.97',
   '0'},
                               						 new List<String>{'Allstate',
   '115727656',
   '217059211824081026',
   'Branded',
   'Preferred deal',
   '300x250',
   '2016-07-15',
   '19115',
   '81.66',
   '8'},
                               						 new List<String>{  'Allstate',
   '115728736',
   '217059211824081026',
   'Branded',
   'Preferred deal',
   '300x250',
   '2016-07-15',
   '76',
   '0.32',
   '0'
}
                               						
                               						};
        String adXJsonResponse= JSON.serialize(adXJsonResponseObj);          

        // Create a fake response
        HttpResponse res = new HttpResponse();
        res.setHeader('Content-Type', 'application/json');
        res.setBody(adXJsonResponse);
        res.setStatusCode(200);
        return res;             						
        

        
    }
	

	/* Pandora_AdExCallOutMock() {
		
	}*/
}