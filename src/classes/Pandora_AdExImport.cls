/*
  Maximum timeout for all callouts (HTTP requests or Web services calls) in a transaction  120 seconds
  Total heap size4    12 MB
  Maximum CPU time on the Salesforce servers5  60,000 milliseconds
*/

/*
  This batch is invoked from a schduler class.
  Agreed up on scheduling at 1 AM every morning.
  Imports the Preferred deals/Private auction deals and invokes itself 
  to subsequently import open acution/firstlook deals

*/

global class Pandora_AdExImport implements Database.Batchable<pandora_processAdExRespone.rowInfo>, Database.AllowsCallouts,Database.stateful 
{
     
    String query;
    String accessToken;
    String EndPointURL;
    String BatchType;
    Integer batchcount=0;
    
    global Integer TotalResult;
    list<pandora_processAdExRespone.rowInfo> adExResponseRowList= new list<pandora_processAdExRespone.rowInfo>();
    ADXJSON Adx;
    
    global Pandora_AdExImport(String aToken, String reportEndPointURL, string type) {
      
       If(string.isEmpty(type))
       system.assert(false,'Type of batch is required.');
       BatchType=type;
       EndPointURL=reportEndPointURL;

       system.debug(''+EndPointURL);


       AccessToken=aToken;
       /*// get it from custom settings
        if(!string.isEmpty(AccessToken))
        {
          this.accessToken=accessToken;
          return;
        }

        
        system.debug('Acess Token:'+accessToken);
        system.assert(false,'assert failed.');
        */
       
    } 
    
    global list<pandora_processAdExRespone.rowInfo> start(Database.BatchableContext BC) {
         
        //system.debug('Start: Access Token'+accessToken+' Start Index:'+ StartIndex + 'MaxResults: '+ MaxResults);
        //system.assert(AccessToken!=null, 'Access token is required.');
        //String EndPointURL=Pandora_AdExCallOutHandler.getEndPointURL_Preferred_PrivateAuctionDeals();
        //system.assert(EndPointURL!=null,'End Point URL is not found');
        
        date dt=Pandora_RevenueLoadBatchHelper.getDate();
        system.assert(dt!=null,'Date is required.');


        
        /*if(batchType=='Preferred deals'){
        for(List<AdEx_Response_temp__c> adExList:[select id from AdEx_Response_temp__c 
                                                   where date__c=:dt]){
             
             delete adExList;

        }
       }*/

       if(string.IsEmpty(AccessToken)){
          Pandora_RefreshTokenResponse refresheTokenResponse= Pandora_AdExCallOutHandler.getNewAccessToken();
          accessToken=refresheTokenResponse.access_token;


       }

        Adx = Pandora_AdExCallOutHandler.doCallOut(accesstoken, EndPointURL);

        List<ADXJSON.headers> Headers= Adx.headers;
        TotalResult= string.isEmpty(Adx.totalMatchedRows)?0:Integer.valueOf(Adx.totalMatchedRows);
        adExResponseRowList=pandora_processAdExRespone.processAdExResponse(Adx);
        system.assert(adExResponseRowList.size()!=0,batchType+' Response does not contain Rows');
        return adExResponseRowList;
        } 



    global void execute(Database.BatchableContext BC, List<pandora_processAdExRespone.rowInfo> scope) {

      System.debug('Scope Size:'+scope.size());
      system.debug('');
      List<AdEx_Response_temp__c> templist= new List<AdEx_Response_temp__c>();
      for(pandora_processAdExRespone.rowInfo rowInfoObj:scope )
      {
           system.debug('rowInfoObj:'+rowInfoObj);
           AdEx_Response_temp__c tempObj= new AdEx_Response_temp__c(Advertizer_Name__c=rowInfoObj.ADVERTISER_NAME,
                                                                    Deal_Id__c=rowInfoObj.DEAL_ID,
                                                                    Earnings__c=Decimal.valueOf(!string.isEmpty(rowInfoObj.EARNINGS)?rowInfoObj.EARNINGS:'0.0'),
                                                                    //Tags__c=rowInfoObj.AD_TAG_NAME,
                                                                    DFP_Ad_Unit_Id__c=rowInfoObj.DFP_AD_UNIT_ID,
                                                                    Ad_Unit_Size_Name__c=rowInfoObj.AD_UNIT_SIZE_NAME,
                                                                    Ad_Impressions__c=rowInfoObj.AD_IMPRESSIONS,
                                                                    Date__c= pandora_processAdExRespone.setStringToDateFormat(rowInfoObj.reportDATE),

                                                                    Transcation_Type__c= rowInfoObj.TRANSACTION_TYPE_NAME,
                                                                    Branding_Type__c= rowInfoObj.BRANDING_TYPE_NAME
                                                                    );
           templist.add(tempObj);

           
       

      } 
      system.debug('temp list size' + templist.size());
      insert templist;  
    
    }
    
    global void finish(Database.BatchableContext BC) {
        
        system.debug('End:'+'Accesstoken'+AccessToken);
        
        String EndPointURL=Pandora_AdExCallOutHandler.getEndPointURL_OpenAcutionDeals();
        
        //Chain the batch for the next API request.
        system.assert(batchType!=null, 'BatchType is Empty');
         
        // invoke the same batch with batch type Open auction to import the Open auction deals/firstlook deals
        if(batchType=='Preferred deals'){
          Pandora_AdExImport padExp= new Pandora_AdExImport(accesstoken,EndPointURL, 'Open auction');
          database.executeBatch(padExp, 1000);
        }  
        
        //Open Auction invokes the revenue load
        if(batchType=='Open auction')
        {
           List<Id> Deallist= null;
           
           Pandora_revenueLoadBatch revLoad= new Pandora_revenueLoadBatch(Deallist);
          Database.ExecuteBatch(revLoad,20);
          
        }
        
    }
    
}

/*
String EndPointURL=Pandora_AdExCallOutHandler.getEndPointURL_Preferred_PrivateAuctionDeals();
Pandora_AdExImport padExp= new Pandora_AdExImport(null,EndPointURL,'Preferred deals');
       database.executeBatch(padExp, 2000);   
*/