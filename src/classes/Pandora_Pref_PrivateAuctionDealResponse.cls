public with sharing class Pandora_Pref_PrivateAuctionDealResponse {
	
	    public string reportDATE;
		public string ADVERTISER_NAME;
		public string AD_TAG_NAME;
		public string DEAL_ID;
		public string BRANDING_TYPE_NAME;
		public string TRANSACTION_TYPE_NAME;
		public string AD_IMPRESSIONS;
		public string EARNINGS;
		public string CLICKS;
}