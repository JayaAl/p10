public with sharing class Pandora_RefreshTokenResponse {
	 public String access_token;
     public String token_type;
     public String expires_in;

     public Pandora_RefreshTokenResponse()
     {
     	
     }
}