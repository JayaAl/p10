/* Pandora_RevenueLoadBatchHelper FROM P1
   This class is being used from Pandora_revenueLoadBatch to load the revenue.
   Method: processAdExResponseItems
   
*/

public with sharing class Pandora_RevenueLoadBatchHelper {
    
      private static string TransactionType_OpenAuction='Open auction';
    private static string TransactionType_PrivateAuction='Private auction';
    private static string TransactionType_PreferredDeals='Preferred deal';
    private static string TransactionType_FirstLook='First look';

    Map<Id,set<String>> AdExAccountMappings=new Map<Id,set<String>>();
    
    /*
       The deal_Id__c field on the opportunity. One Opportunity per deal Id.
    */

    public static Map<String,Opportunity> getOpportunitiesByDealId(List<String> DealIdList)
    {
        /*
          per nick, the dealIds in the deal id field is unique
          
        */
      
        Map<String,Opportunity> oppByDealId= new Map<String,Opportunity>();
        
        for(Opportunity Opp: [select id,name,Account.name,Adx_Deal_ID_Formatted__c,Always_On_Aggregate__c,Deal_ID__c from Opportunity 
                              where Deal_ID__c In:DealIdList 
                             ]
                             )
        {
        String DealId=Opp.Deal_ID__c;
            
            if(oppByDealId.containsKey(DealId))
            {
                
                //system.assert(false,'Duplicate DealId'+Opp.name);

                /*
                  the batch fails for every duplicate deal Id found. A validation is requried at the opportunity create/update.
                */
            }
            else 
            oppByDealId.put(DealId,Opp);
            

        }
        
        return oppByDealId;
    }

    // we have multiple opportunities by alternate deal Id.But advertizers might be different.
    public static Map<String,List<Opportunity>> getOpportunitesByAltDealId(List<String> DealIdList)
    {
        Map<String,List<Opportunity>> oppListByAlDealId= new Map<String,List<Opportunity>>();

        
        for(Opportunity Opp: [select id,name,Account.name,Adx_Deal_ID_Formatted__c,Alternative_Deal_ID__c,Always_On_Aggregate__c from Opportunity 
                              where Alternative_Deal_ID__c In:DealIdList 
                             ]
                             )
        {
            if(oppListByAlDealId.containsKey(Opp.Alternative_Deal_ID__c))
            {
                // alteranate deal Id can have multiple opportunities associated to the same Id
                list<Opportunity> OpportunityList=oppListByAlDealId.get(Opp.Alternative_Deal_ID__c);
                OpportunityList.add(Opp);
                oppListByAlDealId.put(Opp.Alternative_Deal_ID__c,OpportunityList);

            }
            else 
            oppListByAlDealId.put(Opp.Alternative_Deal_ID__c,new List<Opportunity>{Opp});
      }
        
        return oppListByAlDealId;
    }
    
    


    // Query for the account mappings per business logic.
    // may be optimized
    public static Map<String,set<String>> getAdExAccountMappings(List<AdEx_Response_temp__c> AdExResponseItems)
    {
      Map<String,set<String>> AccountMapping= new Map<String,Set<String>>();
       List<String> AdvertiserNames=getAdvertiserNames(AdExResponseItems);

       system.debug('AdvertiserNames'+AdvertiserNames);
      //Mapping table
      for(List<AdEx_Account_Mapping__c> AdExAccountlist:[Select Id,Name,Account__r.name 
                                                         from AdEx_Account_Mapping__c 
                                                         //where Name In:advertisernames
                                                         ]
                                                         )
      {

        for(AdEx_Account_Mapping__c AdExAccountMap: AdExAccountlist){
          //system.debug('In for loop');
         if(AccountMapping.containsKey(AdExAccountMap.Account__r.name))
         AccountMapping.get(AdExAccountMap.Account__r.name).add(AdExAccountMap.name);
         else 
         AccountMapping.put(AdExAccountMap.Account__r.name,new set<string>{AdExAccountMap.name});

        }
      }

      return AccountMapping;
      

    }
    
    

    // Get the advertizer names from deal Id.
    // Not in use after the query in the account mappings is updated.
    public static List<string> getAdvertiserNames(List<AdEx_Response_temp__c> AdExResponseItems)
    {
      List<String> advertisernames = new List<String>();
      for(AdEx_Response_temp__c adexresponseItem:AdExResponseItems)
      {
        //if(adexresponseItem.Transcation_Type__c==TransactionType_OpenAuction)
        advertisernames.add(adexresponseItem.Advertizer_Name__c);
      }


      return advertisernames;
    }

    // get the deals Ids from ad exchange log table(new)
    public static List<string> getDealIds(List<AdEx_Response_temp__c> AdExResponseItems)
    {
      List<String> DealIds = new List<String>();
      for(AdEx_Response_temp__c adexresponseItem:AdExResponseItems)
      {
        //if(adexresponseItem.Transcation_Type__c==TransactionType_OpenAuction)
        if(adexresponseItem.Deal_Id__c!=null)
        DealIds.add(adexresponseItem.Deal_Id__c);
      }


      return DealIds;
    }
    
    
    // for loading revenue for the open auction opportunity responses.
    // get the opportunity Id from settings.
    public static Id getOpenAuction_BrandedOppId()
    {
      GoogleAdExSettings__c setting= getGoogleAdExSetting();
      
      Id OpportunityId=setting.Open_Auction_Branded__c;
      //Id OpportunityId='006220000028FaR'; 

      return OpportunityId;

    }
  
  // get the anonymous opportunity Id from settings.
    public static Id getOpenAuction_AnonymousOppId()
    {
      //get the opportunity from custom settings.

      //Currently hardcoding it.
      GoogleAdExSettings__c setting= getGoogleAdExSetting();
      
      Id OpportunityId=setting.Open_Auction_Anonymous__c;
      //Id OpportunityId='00622000002GNuv'; 

      return OpportunityId;

    }
  
  // getFirstlook Opportunity from settings
    public static Id getFirstLook_OpportunityId()
    {
      //get the opportunity from custom settings.
      GoogleAdExSettings__c setting= getGoogleAdExSetting();
      Id OpportunityId=setting.Firstlook_Opportunity_Id__c;
      //Id OpportunityId='00622000002GHDz'; 

      return OpportunityId;

    }
    

    // instantiate Opportunity line item to be created for loading revenue per business logic.
  // refer to wiki page for logic
    public static OpportunityLineItem getOpportunityLineItem(Id OpportunityId, AdEx_Response_temp__c adExResponseItem, Adx_Product_Mapping__c setting)
    {
      system.assert(setting!=null,'Custom Setting for Ad Unit Id not found.');
      
      if(OpportunityId==null)
         Opportunity opp= new Opportunity();
         
      OpportunityLineItem oli = new OpportunityLineItem(OpportunityId = OpportunityId,
                                                        Quantity = 1
                                                        //PricebookEntryId = pbEntry.Id,
                                                        //TotalPrice = quantity * pbEntry.UnitPrice
                                                        );
      
      list<Pricebook2> stdpbs=[select id,name,IsStandard  from Pricebook2 where isActive=true limit 1];
       Id StdPriceBookId;
      if(!test.isRunningTest())
        StdPriceBookId=stdpbs[0].Id;
      else
      {
        StdPriceBookId=Test.getStandardPricebookId();
      }

      system.assert(StdPriceBookId!=null,'Standard pricebook Id is null');

      List<PriceBookEntry> priceBookEntryList = [SELECT Id, Product2Id, Product2.Id, unitprice
                                            FROM PriceBookEntry 
                                            WHERE Product2.Name =:setting.SFDC_Products__c and CurrencyIsoCode='USD'
                                            AND PriceBook2.Id=:StdPriceBookId LIMIT 1
                                            ];
      Id PriceBookEntryId;
      if(!test.isRunningTest())
      {
         PriceBookEntryId=priceBookEntryList[0].id;
      }
      else{

        PriceBookEntry pbe=[select id,name from pricebookentry where CurrencyIsoCode='USD' limit 1];

        PriceBookEntryId=pbe.id;
      }
       
      system.assert(PriceBookEntryId!=null,'Price book entry not found: '+setting.SFDC_Products__c+' for pricebook Id'+StdPriceBookId);    

            oli.servicedate = adExResponseItem.date__c;
            oli.end_date__c = adExResponseItem.date__c;
            string impressions=adExResponseItem.Ad_impressions__c!=null?adExResponseItem.Ad_impressions__c:'0';
            oli.Impression_s__c=Decimal.valueOf(impressions);
            oli.Quantity= 1;
            oli.Duration__c = 1;
            oli.unitprice = adExResponseItem.earnings__c;  
          
            oli.Medium__c = setting.Medium__c;
            oli.Offering_Type__c = setting.Offering_Type__c;
            //oli.Performance_Type__c = setting.Performance_Type__c;
            oli.Platform__c = setting.Platform__c;
            oli.Size__c = setting.Size__c;
            oli.Sub_Platform__c = setting.Sub_Platform__c;  

            oli.Banner__c=setting.Banner__c;

            if(oli.unitprice !=null && oli.Impression_s__c !=null){    
                        oli.eCPM__c = (oli.unitprice/oli.Impression_s__c) * 1000;
                    }
      oli.PricebookEntryId=PriceBookEntryId;
      
      return OLI; 
      }
      
      
    
    
    // The deals from logs table are processed in this method. This method is called from batch 
    //Pandora_revenueLoadBatch
    //May be optimized. Wrote the logic for processing 20 deals at a time. 
    // two unnested for loops in succession. first one processes deals with private auction/preferred deal transaction types
    // second for loop processes open auction/firstlook deals.

    public static void processAdExResponseItems(List<AdEx_Response_temp__c> AdExResponseItems)
    {
        
        Map<String,set<String>> AdExAccountMappings= getAdExAccountMappings(AdExResponseItems);
        system.debug('@accountmap'+AdExAccountMappings.keyset());
        list<string> advertiserNames= getAdvertiserNames(AdExResponseItems);
        List<String> DealIdList=getDealIds(AdExResponseItems);
        //map<String,BatchProcDeals__c> productandRevenueSettings= getRevenueSettingsForAdxTags();
        Map<String,List<Adx_Product_Mapping__c>> AdxProductMapping=getAdxProductMapping();
        system.assert(advertiserNames.size()>0 && DealIdList.size()>0, 'Advertiser Names and Deal Ids are required');
        
        /*Map<Id,Account> Advertisers= new Map<Id,Account>([Select Id,Name from Account where Name In:advertiserNames]);
        Map<Id,Account> AdvertiserMapping= new Map<Id,Account>([Select Id,Name from Account where Id In (select Account__c
                                                                                                     from AdEx_Account_Mapping__c
                                                                                                     where name in :advertiserNames) ]);*/
        
        Map<String,Opportunity> OpportunityMapByDealIds= getOpportunitiesByDealId(DealIdList);
        system.debug('DealId List'+DealIdList);
        Map<String,List<Opportunity>> AltDealIdByOpportunityList= getOpportunitesByAltDealId(DealIdList);
        
        //system.assert(OpportunityMapWithAdvertisers.size()>0, 'No Opportunites found with matching advertiser names');

       // List<string> advertiserAliases= getAliases( AdExAccountMappings.values());
        
       
        for(AdEx_Response_temp__c Item: AdExResponseItems)
        {
          boolean accountMatched = false;
          if(!(Item.Transcation_Type__c==TransactionType_PreferredDeals 
             || Item.Transcation_Type__c==TransactionType_PrivateAuction))
             Continue;




          if(item.DFP_Ad_Unit_Id__c!=null && !AdxProductMapping.containsKey(item.DFP_Ad_Unit_Id__c)){
             Item = updateAdExResponseItem(Item,false,null);
             continue;

          }

         // BatchProcDeals__c product_ReveneuSetting=productandRevenueSettings.get(Item.Tags__c);
          List<Adx_Product_Mapping__c> ProdMappinglist= AdxProductMapping.get(Item.DFP_Ad_Unit_Id__c);
          system.assert(ProdMappinglist.size()>0,'Product Mapping Does Not exist for '+Item.DFP_Ad_Unit_Id__c);
          Adx_Product_Mapping__c AdxProdMapping= getAdxProductMappingByAdUnitSize(ProdMappinglist,Item.Ad_Unit_Size_Intercept__c);
          
          if(AdxProdMapping==null)
          {
            Item = updateAdExResponseItem(Item,false,null);
             continue;
          }

          if(OpportunityMapByDealIds.containsKey(Item.Deal_Id__c))
            {
              // laod the revenue to the opportunity
              Opportunity Opp= OpportunityMapByDealIds.get(Item.Deal_Id__c);
              OpportunityLineItem opli=getOpportunityLineItem(Opp.Id,Item,AdxProdMapping); 
              insert opli;

              update Opp;

              Item = updateAdExResponseItem(Item,true,Opp.Id);


              //system.assert(false,'case 1:');
            }
            else 
            { 
                 
                 system.debug('IL1 else' + Item);
                 system.debug('IL1 AltDealIdByOpportunityList' + AltDealIdByOpportunityList);
                 system.debug('IL1 AltDealIdByOpportunityList.containsKey(Item.Deal_Id__c)' + AltDealIdByOpportunityList.containsKey(Item.Deal_Id__c));

                 if(!AltDealIdByOpportunityList.containsKey(Item.Deal_Id__c)){
                     system.debug('IL1 AltDealIdByOpportunityList.containsKey(Item.Deal_Id__c)');
                     Item = updateAdExResponseItem(Item,false,null);
                     continue;
                 }
                 
                    
                 List<Opportunity> OpportunityListForDealId= AltDealIdByOpportunityList.get(Item.Deal_Id__c);
                 /*Code Added By Sri*/
                 
                 system.debug('IL1 OpportunityListForDealId SIZE' + OpportunityListForDealId.size());
                 for(Opportunity opp: OpportunityListForDealId){
                 
                     system.debug('IL1 Account name'+opp.account.name);
                     set<String> aliases=AdExAccountMappings.get(opp.Account.name);
                     system.debug('IL1 @@@@aliases: before'+aliases);
 
                   if(aliases==null)
                     aliases=new set<String>();

                   //uppercase aliases
                   aliases=getAliases(aliases);


                   system.debug('IL1 @@@@aliases: after'+aliases+'Adv. Name:'+Item.Advertizer_Name__c.toUpperCase());


                   //system.assert(false, 'aliases not'+aliases[0]);
                   

                   /*if(Item.Advertizer_Name__c==null) 
                      Item.Advertizer_Name__c=' '; */
                      
                      
                      
                      
                   system.debug('IL1 aliases.contains(Item.Advertizer_Name__c.toUpperCase()'+ aliases.contains(Item.Advertizer_Name__c.toUpperCase()));


                   if(aliases.contains(Item.Advertizer_Name__c.toUpperCase())
                      //|| aliases.contains(Item.Advertizer_Name__c.toLowerCase())
                      ){

                    OpportunityLineItem opli=getOpportunityLineItem(Opp.Id,Item,AdxProdMapping); 
                    system.debug('IL1 inside condition opli'+opli);
                    system.debug('IL1 inside condition Item'+Item);
                    
                    
                    insert opli;
                    system.debug('IL1 inside condition opli '+opli);
                    system.debug('IL1 inside condition opli Id'+opli.id);
                    

                    update Opp;
                    system.debug('IL1 inside condition opli Id'+Opp.id);
                    Item = updateAdExResponseItem(Item,true,Opp.Id);
                    
                    system.debug('IL1 inside condition Item '+Item);
                    accountMatched = true;
                    system.debug('IL1 inside condition accountMatched'+accountMatched);
                    break; 

                    //system.assert(false,'failing here.');

                   }
                 }
                 
                 if(accountMatched==false){
                     system.debug('IL1 accountMatched condition'+accountMatched);
                     for(Opportunity opp: OpportunityListForDealId){
                       
                         if(Opp.Always_On_Aggregate__c) 
                         {
                            
                            OpportunityLineItem opli=getOpportunityLineItem(Opp.Id,Item,AdxProdMapping); 
                            system.debug('IL1 opli'+opli);
                            system.debug('IL1 Item'+Item);
                            insert opli;
        
                            update Opp;
                            Item = updateAdExResponseItem(Item,true,Opp.Id);
                            break; 
                        
                         }
                         else{
                          system.debug('IL1 INSIDE ELSE Item'+Item);
                          Item = updateAdExResponseItem(Item,false,null);
    
                         }    
                     }
                 }             
               }
                
            }
            
            for(AdEx_Response_temp__c Item: AdExResponseItems)
            {
             
              if(Item.DFP_Ad_Unit_Id__c!=null && !AdxProductMapping.containsKey(Item.DFP_Ad_Unit_Id__c))
              {
               //update AdxResponseItems.
               Item = updateAdExResponseItem(Item,false,null);
               continue;
              }

              List<Adx_Product_Mapping__c> ProdMappinglist= AdxProductMapping.get(Item.DFP_Ad_Unit_Id__c);
               system.assert(ProdMappinglist.size()>0,'Product Mapping Does Not exist for '+Item.DFP_Ad_Unit_Id__c);
               Adx_Product_Mapping__c AdxProdMapping= getAdxProductMappingByAdUnitSize(ProdMappinglist,Item.Ad_Unit_Size_Intercept__c);
          
              if(AdxProdMapping==null)
              {
               Item = updateAdExResponseItem(Item,false,null);
               continue;
              }

             if(!(Item.Transcation_Type__c==TransactionType_PreferredDeals 
             || Item.Transcation_Type__c==TransactionType_PrivateAuction))
             {

               
               Id OpenAuction_Branded_OppId= getOpenAuction_BrandedOppId(); 
               Id OpenAuction_AnonymouseOppId=getOpenAuction_AnonymousOppId();

               Id firstLookOppId=getFirstLook_OpportunityId();
               if('Open auction'.equalsIgnoreCase(Item.Transcation_Type__c))
               {
                 Opportunity Opp= new Opportunity();
                 opp.Id=OpenAuction_Branded_OppId;
                 Id OpportunityId= ('Branded'.EqualsIgnoreCase(Item.Branding_Type__c))?OpenAuction_Branded_OppId:OpenAuction_AnonymouseOppId;
                 
                 OpportunityLineItem Opli=getOpportunityLineItem(OpportunityId,Item,AdxProdMapping);
                 insert Opli;
                 
                 update opp;

                 Item = updateAdExResponseItem(Item,true,OpportunityId);

               }
               else if('First look'.equalsIgnoreCase(Item.Transcation_Type__c)){
                 Opportunity Opp= new Opportunity();
                 opp.Id=firstLookOppId;

                 OpportunityLineItem Opli=getOpportunityLineItem(firstLookOppId,Item,AdxProdMapping);
                 insert Opli;

                 update opp; 

                 Item = updateAdExResponseItem(Item,true,Opp.Id);

               }

             }

            }
           system.debug('IL1 AdExResponseItems '+AdExResponseItems);
           try{
               update AdExResponseItems; 
           }
           catch(exception ex)
           {
               system.debug('IL1 AdExResponseItems Exception '+ex);
           }
         return;

        }


  
    // Utility Method to get the Opportunity By Account Name
   /* public static map<String,Opportunity> getOpportunityByAccountName(List<Opportunity> opplist)
    {
        map<String,Opportunity> oppaccMap= new Map<String,Opportunity>();
      for(Opportunity opp: opplist)
      {
        oppaccMap.put(opp.account.name, opp);
      }
      return oppaccMap; 

    }*/

    
    // accont mapping for matching deal advertizer name.
    private static set<String> getAliases(set<String> AccountAliasesList){

        /*set<String> AliasList= new set<String>();
        for(set<String> AccountAliases: AccountAliasesList)
        {
          AliasList.addALL(AccountAliases);
        }*/

      set<String> upperCaseAlias= new set<String>();
      for(string str:AccountAliasesList)
      {
         if(Str!=null)
         upperCaseAlias.add(str.toUpperCase());

      }

        return upperCaseAlias;
    }

    // update the deal and mark if the revenue loaded to an opportunity.
    // link the opportunity.
    public static AdEx_Response_temp__c updateAdExResponseItem(AdEx_Response_temp__c Item, boolean flag, Id OppId)
    {
      Item.Line_created__c=flag;
      Item.Opportunity__c= OppId;
      //update item;
      return Item;

    }

   
    // get the deal Ids
    /*public static Map<Id,Opportunity> getDealIdsFromResponseItems(List<AdEx_Response_temp__c> AdExResponseItems)
    {
      //List<Opportunity> OppotunityList= new List<Opportunity>();
      Map<Id,Opportunity> opportunityMap= new Map<Id,Opportunity>();
      set<String> DealIdList= new set<String>();
      for(AdEx_Response_temp__c Item: AdExResponseItems)
        {
         //OppotunityList.add(Item.);
         DealIdList.add(Item.Deal_Id__c);
         
        }
     
     for(Opportunity Opp: [select id,name,Adx_Deal_ID_Formatted__c 
                           from Opportunity 
                           where Deal_Id__c In:DealIdList])
      {
        opportunityMap.put(Opp.Deal_Id__c,Opp);
      }

        return opportunityMap;
      
    }*/

    /*public static map<String,BatchProcDeals__c> getRevenueSettingsForAdxTags()
    { 
      Map<String,BatchProcDeals__c> mapofSetting = BatchProcDeals__c.getAll();
      Map<String,BatchProcDeals__c> mapofAdxTagandSetting = new map<String, BatchProcDeals__c>();
        for(String key : mapofSetting.keyset()){
            BatchProcDeals__c mappingRecord =  mapofSetting.get(key);
            //if(mappingRecord.SFDC_Products__c <> null){
             //   setofSfdcProducts.add(mappingRecord.SFDC_Products__c);
            //}
            mapofAdxTagandSetting.put(mappingRecord.AdX_Tags__c,mappingRecord);
        }

      return mapofAdxTagandSetting;
    }*/

    // get the product mapping by DFP AD Unit Id.

    public static Map<String,List<Adx_Product_Mapping__c>> getAdxProductMapping()
    {
        Map<String,Adx_Product_Mapping__c> mapofSetting = Adx_Product_Mapping__c.getAll();
        Map<String,List<Adx_Product_Mapping__c>> mapofAdxTagandSetting = new Map<String,List<Adx_Product_Mapping__c>>();
        for(string str:mapofSetting.keyset())
        {
          Adx_Product_Mapping__c mappingRecord =  mapofSetting.get(str);
          if(!mapofAdxTagandSetting.containsKey(mappingRecord.DFP_Ad_Unit_Id__c))
             mapofAdxTagandSetting.put(mappingRecord.DFP_Ad_Unit_Id__c,new list<Adx_Product_Mapping__c>{mappingRecord});
             else{
             List<Adx_Product_Mapping__c> maplist= mapofAdxTagandSetting.get(mappingRecord.DFP_Ad_Unit_Id__c);
             maplist.add(mappingRecord);
             mapofAdxTagandSetting.put(mappingRecord.DFP_Ad_Unit_Id__c,maplist);

             }

        }
      

        return mapofAdxTagandSetting;
    }
    // send an email to emails specified in settings after the revenue loading is complete.
    public static void SendNotificationEmailOnCompletion(List<string> emaillist)
    {
            
           GoogleAdExSettings__c setting= getGoogleAdExSetting();
            string emailcsv= setting.Emails_CSV__c!=null?setting.Emails_CSV__c:'';
            emaillist.addALL(emailcsv.split(','));
            date dt= Pandora_RevenueLoadBatchHelper.getDate();
            Messaging.SingleEmailMessage mail = new Messaging.SingleEmailMessage();
            String[] toAddresses = emaillist;
                mail.setToAddresses(toAddresses);
            mail.setSubject(' Adx Deals Loaded for date '+dt.format() +' Links to reports enclosed');
            //breakrec = breakrec.replace('null' , '') ;
            String finalStr = '' ;
            finalStr = '<html><head>'+
                       '</head><body>'+
                       'Link to Adx Log Report:'+setting.Adx_Log_Report_URL__c+'</br>'+
                       'Link to Opportunity Report:'+setting.Opportunity_Report_URL__c+'</br>'+
                       //'<table style="width:100%"><td> Deal Id </td> <td> Start Date </td><td> End Date </td> <td> AdxTag </td> <td> Ad Impressions </td> <td> Estimated Revenue </td> ' + breakrec +'</table>';
                       '</body></html>';
            mail.setHtmlBody(finalstr);
            Messaging.sendEmail(new Messaging.SingleEmailMessage[] { mail });
      return;
    } 

    // Get the settings.
    public static GoogleAdExSettings__c getGoogleAdExSetting()
    {
      Map<String,GoogleAdExSettings__c> settings= GoogleAdExSettings__c.getALL();
      GoogleAdExSettings__c setting;
      for(GoogleAdExSettings__c s:settings.values())
      { 
        setting=s;
      }
      if(setting==null)
      setting= new GoogleAdExSettings__c(name='test');

      return setting;

    }

    
    //   usage: Pandora_RevenueLoadBatchHelper.getDate();
    //   Get the date from settings to import the deal
    //   load the revenu for the imported deals for the date 
    //   date mentioned through settings. Always set to -1 when batch is scheduled.
    
    
    public static date getDate()
    {
     GoogleAdExSettings__c setting = getGoogleAdExSetting();
     Integer i= Integer.valueOf(setting.Import_Today_N_days__c!=null?setting.Import_Today_N_days__c:1.0);
     date dt= date.today().addDays(i);

     return dt;

      
    }


    
    public static Adx_Product_Mapping__c getAdxProductMappingByAdUnitSize(List<Adx_Product_Mapping__c> mappinglist, string Ad_Unit_Size_Name)
    {
      map<string,Adx_Product_Mapping__c> ProductMappingByAdUnitSize= new Map<String,Adx_Product_Mapping__c>();
      for(Adx_Product_Mapping__c mappingRecord:mappinglist){

        ProductMappingByAdUnitSize.put(mappingRecord.Ad_Unit_Size_Name__c, mappingRecord);
      }

      return ProductMappingByAdUnitSize.get(Ad_Unit_Size_Name);

    }

    /*
      usage : Pandora_RevenueLoadBatchHelper.updateCustomSetting();
    
    public static void updateCustomSetting() 
    {
      for(Adx_Product_Mapping__c adxmap: Adx_Product_Mapping__c.getAll().values())
      {
        //if(adxmap.Ad_Unit_Size_Name__c=='(anything but 300x600)')
           //adxmap.Ad_Unit_Size_Name__c='Default';
           adxMap.SFDC_Products__c='Mobile'; 
           
           update adxmap; 
      }  

    }
    */ 

    /*
     Pandora_RevenueLoadBatchHelper.deleteAdResponsesforDate(date.today().addDays(-1));
       
    */
    public static void deleteAdResponsesforDate(Date dt)
    {
       //Pandora_RevenueLoadBatchHelper.deleteAdResponsesforDate(date.today().addDays(-1));
       delete [select id from AdEx_Response_temp__c where date__c=:Dt];

    }



    
    public Pandora_RevenueLoadBatchHelper() {
    //TransactionType.OPENAUCTION='Open Auction';
    /* SELECT id, name, Adx_Deal_ID_Formatted__c FROM Opportunity 
    WHERE (Adx_Deal_ID_Formatted__c IN ('217059211824071514','217059211824075137') AND AccountId IN ())*/
        
    }
}