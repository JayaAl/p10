/*
  Venkata R Sabbella
  How to Use this:
  
  Pandora_revenueLoadBatch revLoad= new Pandora_revenueLoadBatch();
  Database.ExecuteBatch(revLoad,20);

*/
global class Pandora_revenueLoadBatch implements Database.Batchable<sObject> {  
	
	String query;
	Date dt;
	List<Id> dealList= new List<Id>();
	
	global Pandora_revenueLoadBatch(List<Id> DealIds) {

		//date dt=date.today().addDays(-1);
		dt= Pandora_RevenueLoadBatchHelper.getDate();
		system.assert(dt!=null,'Date is Required. Check Settings');
        
       if(DealIds!=null)
        {
        	dealList=dealIds;
        	query='select id,name,Deal_Id__c, Advertizer_Name__c, Line_Created__c, '+
		      'Ad_Impressions__c,Earnings__c, Branding_Type__c, Transcation_Type__c, Date__c,DFP_Ad_Unit_Id__c,Ad_Unit_Size_Intercept__c,Opportunity__c '+  
		      'from AdEx_Response_temp__c where Id In:dealList';
		      //' where Chosen_For_Testing__c =true ';

		      return;
        }

		query='select id,name,Deal_Id__c, Advertizer_Name__c, Line_Created__c, '+
		      'Ad_Impressions__c,Earnings__c, Branding_Type__c, Transcation_Type__c, Date__c,DFP_Ad_Unit_Id__c,Ad_Unit_Size_Intercept__c,Opportunity__c '+  
		      'from AdEx_Response_temp__c where date__c=:dt';
		      //' where Chosen_For_Testing__c =true ';
		
	}
	
	global Database.QueryLocator start(Database.BatchableContext BC) {
		return Database.getQueryLocator(query); 
	}

   	global void execute(Database.BatchableContext BC, List<sObject> scope) {
   	  List<AdEx_Response_temp__c> AdExResponseItems =(List<AdEx_Response_temp__c>)scope;
	  Pandora_RevenueLoadBatchHelper.processAdExResponseItems( AdExResponseItems);
	  //system.assert(false,'test Assert');
    
	}
	 
	global void finish(Database.BatchableContext BC) {
		
		AsyncApexJob aaj = [Select Id, Status, CreatedBy.Email from AsyncApexJob where Id = :BC.getJobId()];  
        String[] toAddresses = new String[] {aaj.CreatedBy.Email};
        Pandora_RevenueLoadBatchHelper.SendNotificationEmailOnCompletion(toAddresses); 
	}
	
}