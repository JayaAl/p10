@isTest(seealldata=false)
public with sharing class Pandora_testAdExIntegration {
	
	public Pandora_testAdExIntegration() {
		
	}

	@testsetup static void createTestData()
	{
            Account acc = UTIL_TestUtil.createAccount();
            Opportunity opp = UTIL_TestUtil.createOpportunity(acc.Id);
            list<Opportunity> OpportunityList= new List<Opportunity>();
               
                // private auction with deal_Id__c
                OpportunityList.add(getAdxOpportunity(opp, '217059211824078028',null,false));
                OpportunityList.add(getAdxOpportunity(opp, '217059211824082897',null,false));
                 //preferred deal with dealId__c
                OpportunityList.add(getAdxOpportunity(opp, '217059211824081026',null,false));
                OpportunityList.add(getAdxOpportunity(opp, '217059211824081026',null,false));
                

                //private auction/preferred deal with alternate deal Id
                OpportunityList.add(getAdxOpportunity(opp, null,'217059211824073971',false));
                OpportunityList.add(getAdxOpportunity(opp, null,'217059211824073971',false));
                OpportunityList.add(getAdxOpportunity(opp, null,'217059211824073971',false));
                
                //always on
                OpportunityList.add(getAdxOpportunity(opp, null,'217059211824073971',true));
                OpportunityList.add(getAdxOpportunity(opp, null,'217059211824073971',true));
                
                

                

                

                //open auction- branded/anonymous // firstlook
                OpportunityList.add(getAdxOpportunity(opp, 'Open auction',null,false)); 
                

            insert OpportunityList;

            List<Opportunity> OpportunityListQueried=[select id,name from opportunity];
            system.assert(OpportunityListQueried.size()>0,'Opportunity data not found');

            createproductAndPriceBookEntries();
            Id standardPricebookId=test.getStandardPriceBookId();

            List<PriceBookEntry> priceBookEntryList = [SELECT Id, Product2Id, Product2.Id, unitprice
                                            FROM PriceBookEntry 
                                            WHERE Product2.Name In ('Mobile')
                                            AND PriceBook2.Id=:standardPricebookId LIMIT 1
                                            ];

           //system.assert(priceBookEntryList.size()>0,'Price book entry not found: for mobile for standard pricebook ');     



            createCustomSettings();
            system.assert(Adx_Product_Mapping__c.getAll().size()>0,'Product mapping settings required');
            system.assert(GoogleAdExSettings__c.getAll().size()>0,'Product mapping settings required');


            createAdXDeals();
            list<AdEx_Response_temp__c> dealList=[select id,name,Advertizer_Name__c from AdEx_Response_temp__c];
            system.assert(dealList.size()>0,'populate adx deals data.');

            createaccountMappings(dealList,acc.id);
            list<AdEx_Account_Mapping__c> acctmappings= [select id,name from AdEx_Account_Mapping__c];

            system.assert(acctmappings.size()>0,'Please upload the account mappings'); 

            

            
	} 


	private static testMethod  void testAdeXCallout()
	{
     
     // Set mock callout class 

        Test.setMock(HttpCalloutMock.class, new Pandora_AdExCallOutMock());



        String EndPointURL=Pandora_AdExCallOutHandler.getEndPointURL_Preferred_PrivateAuctionDeals();
        
        ADXJSON jsonresp=Pandora_AdExCallOutHandler.doCallOut(null,  EndPointURL);


        
        

	}

	

	private static testMethod void TestAdExImportBatch()
	{
      
       Test.setMock(HttpCalloutMock.class, new Pandora_AccessTokenCalloutMock());
       String EndPointURL=Pandora_AdExCallOutHandler.getEndPointURL_Preferred_PrivateAuctionDeals();
       Pandora_AdExImport padExp= new Pandora_AdExImport(null,EndPointURL,'Preferred deals');
 
       Test.setMock(HttpCalloutMock.class, new Pandora_AdExCallOutMock());
       database.executeBatch(padExp, 2000);       
    
	}

	private static testMethod void testRevenueLoadBatch()
	{
	  test.startTest();
       createproductAndPriceBookEntries();
	  test.stopTest();
      List<Id> Deallist= new List<Id>{'a2c22000000EG48'};
      Deallist=null;
      Pandora_revenueLoadBatch revLoad= new Pandora_revenueLoadBatch(Deallist);
      Database.ExecuteBatch(revLoad,20);


	}

	private static testMethod void testRevenueLoadBatch_alwaysOn()
	{
	  test.startTest();
       createproductAndPriceBookEntries();
       delete [select id from AdEx_Account_Mapping__c]; 

	  test.stopTest();
      List<Id> Deallist= null;
      
      Pandora_revenueLoadBatch revLoad= new Pandora_revenueLoadBatch(Deallist);
      Database.ExecuteBatch(revLoad,20);


	}

    private static testMethod void testScheduler()
    {
      Test.setMock(HttpCalloutMock.class, new Pandora_AccessTokenCalloutMock());
       Test.setMock(HttpCalloutMock.class, new Pandora_AdExCallOutMock());
       
      AdxImportSchduler scheduler= new AdxImportSchduler();
      string sch='0 0 3 * * ?';// run at 3 am
      system.schedule('One Time Pro', sch, scheduler);



    }

    private static testMethod void testMiscelleneous()
    {
    	//Pandora_AdExCallOutHandler.getEndPointURL_OpenAcutionDeals();
    	//Pandora_AdExCallOutHandler.getNewAccessToken();
    	Pandora_RevenueLoadBatchHelper helper= new Pandora_RevenueLoadBatchHelper(); 
    	date dt=date.today();
    	Pandora_RevenueLoadBatchHelper.deleteAdResponsesforDate(dt);

    	List<Id> Deallist= new List<Id>{[select id,name from AdEx_Response_temp__c limit 1].Id};
        //Deallist=null;
        Pandora_revenueLoadBatch revLoad= new Pandora_revenueLoadBatch(Deallist);
        Database.ExecuteBatch(revLoad,20);


        Pandora_AdExCallOutHandler handler= new Pandora_AdExCallOutHandler();

        Pandora_RefreshTokenResponse response= new Pandora_RefreshTokenResponse();

        ADXJSON AdExResponseObj=getAdxJsonresponse();
        pandora_processAdExRespone.processAdExResponse( AdExResponseObj);

    }
	

	private static AdEx_Account_Mapping__c getAdExAccountMapping(Id AccountId, string mname)
	{
      AdEx_Account_Mapping__c acctmapping=new AdEx_Account_Mapping__c(Account__c=AccountId,
      																  name= mname

      																 );

       
      return acctmapping;

	}

	private static void deleteAdExAccountMapping(Id recordId)
	{
     delete [select id from AdEx_Account_Mapping__c where id=:recordId];

	}

	
	/*private static void createTestAdxDeals()
	{
	  AdEx_Response_temp__c adExTemp;	
      adExTemp=getDeal('test',
      	       ''+1.0,
      	       'CSC Holdings',
      	       'Branded',
      	       'Private auction',
      	       date.today(),
      	       '115727656',
      	       '300x600'

      	       ); 

	}*/



	static Opportunity getAdxOpportunity(Opportunity Opp, String DealId, string Alternative_Deal_ID, boolean Always_On_Aggregate)
	{
      Opportunity adxOpp=opp.clone(false,false,false,false);
      adxOpp.id=null;
      //adxOpp.Adx_Deal_ID_Formatted__c
      adxOpp.Deal_ID__c=DealId; 
      adxOpp.Alternative_Deal_ID__c=Alternative_Deal_ID;
      adxOpp.Always_On_Aggregate__c=Always_On_Aggregate;

      return adxOpp;

	} 

	

   

    public static Adx_Product_Mapping__c getAdXProductMapping( string mname,
    	                                       String  Ad_Unit_Size_Name,
      										   String Banner,
      										   String Medium,
      										   String DFP_Ad_Unit_Id,
      										   String Offering_Type,
      								           String Platform,
      									       String Size,
      										   String Sub_Platform,
      										   String SFDC_Products
      										   )
    {

    	Adx_Product_Mapping__c prodMapping= new Adx_Product_Mapping__c(name=mname,
    		                                                         Ad_Unit_Size_Name__c=Ad_Unit_Size_Name,
      																 Banner__c=Banner,
      																 Medium__c=Medium,
      																 DFP_Ad_Unit_Id__c=DFP_Ad_Unit_Id,
      																 Offering_Type__c=Offering_Type,
      																 Platform__c=Platform,
      																 Size__c=Size,
      																 Sub_Platform__c=Sub_Platform,
      																 SFDC_Products__c=SFDC_Products
      																 
      																 
             														 );
    	return prodMapping;
    }

    public static void createCustomSettings()
    {
     createAdxMapping();
     createGoogleAdExchangeSettings();

    }
 

    public static void createAdxMapping()
    {
      List<Adx_Product_Mapping__c> adxMapping = new List<Adx_Product_Mapping__c>
      {

         	 getAdXProductMapping(	'1',	'300x600',	'Performance',	'Display',	'115727656',	'Standard',	'Web',	'300x600',	'n/a',	'Mobile'),
	         getAdXProductMapping(	'10',	'Default',	'Performance',	'Display',	'115723456',	'Standard',	'Web',	'300x250',	'n/a',	'Mobile'),
	         getAdXProductMapping(	'11',	'Default',	'Performance',	'Display',	'115729696',	'Standard',	'Web',	'300x250',	'n/a',	'Mobile'),
	         getAdXProductMapping(	'12',	'Default',	'Performance',	'Display',	'115728736',	'Standard',	'Web',	'300x250',	'n/a',	'Mobile'),
	         getAdXProductMapping(	'13',	'Default',	'Performance',	'Display',	'115728856',	'Standard',	'Web',	'300x250',	'n/a',	'Mobile'),
	         getAdXProductMapping(	'14',	'Default',	'Performance',	'Display',	'115729096',	'Standard',	'Web',	'300x250',	'n/a',	'Mobile'),
	         getAdXProductMapping(	'15',	'Default',	'Performance',	'Display',	'115727536',	'Standard',	'Web',	'300x250',	'n/a',	'Mobile'),
	         getAdXProductMapping(	'16',	'Default',	'Performance',	'Display',	'115728376',	'Standard',	'Web',	'300x250',	'n/a',	'Mobile'),
	         getAdXProductMapping(	'17',	'Default',	'Performance',	'Display',	'115710976',	'Standard',	'Mobile',	'300x250',	'iPhone',	'Mobile'),
	         getAdXProductMapping(	'18',	'Default',	'Performance',	'Display',	'115703776',	'Standard',	'Mobile',	'300x250',	'Android',	'Mobile'),
	         getAdXProductMapping(	'19',	'Default',	'Performance',	'Display',	'115617136',	'Standard',	'Tablet',	'300x250',	'Android',	'Mobile'),
	         getAdXProductMapping(	'2',	'300x600',	'Performance',	'Display',	'115723456',	'Standard',	'Web',	'300x600',	'n/a',	'Mobile'),
	         getAdXProductMapping(	'20',	'Default',	'Performance',	'Display',	'115672936',	'Standard',	'Tablet',	'300x250',	'iPad',	'Mobile'),
	         getAdXProductMapping(	'21',	'Default',	'Performance',	'Display',	'115729216',	'Standard',	'Web',	'300x250',	'n/a',	'Mobile'),
	         getAdXProductMapping(	'22',	'Default',	'Performance',	'Display',	'115729336',	'Standard',	'Web',	'300x250',	'n/a',	'Mobile'),
	         getAdXProductMapping(	'23',	'Default',	'Performance',	'Display',	'115729456',	'Standard',	'Web',	'300x250',	'n/a',	'Mobile'),
	         getAdXProductMapping(	'24',	'Default',	'Performance',	'Display',	'115729576',	'Standard',	'Web',	'300x250',	'n/a',	'Mobile'),
	         getAdXProductMapping(	'25',	'Default',	'Performance',	'Display',	'115729816',	'Standard',	'Web',	'300x250',	'n/a',	'Mobile'),
	         getAdXProductMapping(	'3',	'300x600',	'Performance',	'Display',	'115729696',	'Standard',	'Web',	'300x600',	'n/a',	'Mobile'),
	         getAdXProductMapping(	'4',	'300x600',	'Performance',	'Display',	'115728736',	'Standard',	'Web',	'300x600',	'n/a',	'Mobile'),
	         getAdXProductMapping(	'5',	'300x600',	'Performance',	'Display',	'115728856',	'Standard',	'Web',	'300x600',	'n/a',	'Mobile'),
	         getAdXProductMapping(	'6',	'300x600',	'Performance',	'Display',	'115729096',	'Standard',	'Web',	'300x600',	'n/a',	'Mobile'),
	         getAdXProductMapping(	'7',	'300x600',	'Performance',	'Display',	'115727536',	'Standard',	'Web',	'300x600',	'n/a',	'Mobile'),
	         getAdXProductMapping(	'8',	'300x600',	'Performance',	'Display',	'115728376',	'Standard',	'Web',	'300x600',	'n/a',	'Mobile'),
	         getAdXProductMapping(	'9',	'Default',	'Performance',	'Display',	'115727656',	'Standard',	'Web',	'300x250',	'n/a',	'Mobile')
       };
       insert adxMapping;
    }

    public static void createGoogleAdExchangeSettings()
    {
     GoogleAdExSettings__c  adXsettings;
     Id OpportunityId= [select id,name from opportunity where Deal_Id__c='Open auction' limit 1].Id;
     system.assert(OpportunityId!=null,'Open Auction opportunity is not found.');
     

      adXsettings=getGoogleAdExSetting(

      	'Test',	
      	'https://pandora--pandora10.cs27.my.salesforce.com/00O22000000K0Da',	
      	'723789058145-miooatskvlrihp0i8r620uvc2v0gu8k2.apps.googleusercontent.com',	
      	'o4Tk-CtoGA15r1BFmZrj9uFw',	
      	'vsabbella@pandora.com',	
      	OpportunityId,	
      	-1.0,
      	OpportunityId,
      	OpportunityId,	 
      	'https://pandora--pandora10.cs27.my.salesforce.com/00O22000000Jyhg',	
      	'https://pandora--pandora10--c.cs27.visual.force.com',	
        '1%2FHLndW9MUFO8Q4Ut3zhM5p5nnJe7UUkdVLws2EVos-yk'	
        );

      insert adXsettings;

    }

    public static GoogleAdExSettings__c getGoogleAdExSetting(
    	        String sname,
    	        String Adx_Log_Report_URL,
				String Client_Id,
				String Client_Secret,
				String Emails_CSV,
				Id Firstlook_Opportunity_Id,
				Decimal Import_Today_N_days,
				Id Open_Auction_Anonymous,
				Id Open_Auction_Branded	,
				String Opportunity_Report_URL,
				String Redirect_URL,
				String Refresh_Token
				)
    {
    	GoogleAdExSettings__c adXsettings= new GoogleAdExSettings__c(

                name=sname,
     		   	Adx_Log_Report_URL__c	=Adx_Log_Report_URL,	
				Client_Id__c	=Client_Id,	
				Client_Secret__c	=Client_Secret,	
				Emails_CSV__c	=Emails_CSV,	
				Firstlook_Opportunity_Id__c	=Firstlook_Opportunity_Id,	
				Import_Today_N_days__c	=Import_Today_N_days,	
				Open_Auction_Anonymous__c	=Open_Auction_Anonymous,	
				Open_Auction_Branded__c	=Open_Auction_Branded,	
				Opportunity_Report_URL__c	=Opportunity_Report_URL,	
				Redirect_URL__c	=Redirect_URL,	
				Refresh_Token__c	=Refresh_Token
				//Token__c	=Token

				);	

    	return adXsettings;

    }

    private static AdEx_Response_temp__c getDeal(String dealname,
    	                                         String Advertizer_Name,
    	                                         String DFP_Ad_Unit_Id,
    	                                         String Branding_Type,
    	                                         String Ad_Unit_Size_Name,
    	                                         String Deal_Id,
    	                                         String Transcation_Type,
		                                         Date adExDate
		                                                        //String Ad_Unit_Size_Intercept,
		                                                        //String Opportunity
		                                                        //String Ad_Impressions,
		                                                        //decimal Earnings, 
		                                                        //boolean Line_Created, 
		                                                        
		                                                        ){ 

                         
		      

	  AdEx_Response_temp__c adExTemp= new AdEx_Response_temp__c(name=dealname,
	  	                                                        Advertizer_Name__c=Advertizer_Name, 
                                                                Deal_Id__c=Deal_Id,
                                                                Line_Created__c=false, 
		                                                        Ad_Impressions__c='100',
		                                                        Earnings__c=100, 
		                                                        Branding_Type__c=Branding_Type, 
		                                                        Transcation_Type__c=Transcation_Type, 
		                                                        Date__c=adExDate,
		                                                        DFP_Ad_Unit_Id__c=DFP_Ad_Unit_Id,
		                                                        Ad_Unit_Size_Name__c=Ad_Unit_Size_Name,
		                                                        //Ad_Unit_Size_Intercept__c=Ad_Unit_Size_Intercept,
		                                                        Opportunity__c=null
		                                                        );	      

      return adExTemp;
	} 

    public static void createAdXDeals()
    {
         
           // signature
		   //String dealname,String Advertizer_Name,String DFP_Ad_Unit_Id,String Branding_Type,String Ad_Unit_Size_Name,String Deal_Id,String Transcation_Type,
		                                                       // Date adExDate);



		   date  dt= Pandora_RevenueLoadBatchHelper.getDate();

		   // private auction
		   list<AdEx_Response_temp__c> privateAuctionDeals= new List<AdEx_Response_temp__c>{
		   getdeal(	'a2c22000000EFmi',	'BMW Group',	'115727656',	'Branded',	'300x250',	'217059211824078028',	'Private auction',	dt),
           getdeal(	'a2c22000000EFmm',	'Bask Digi',	'115727536',	'Branded',	'300x250',	'217059211824082897',	'Private auction',	dt),
           getdeal(	'a2c22000000EFmp',	'Berkshire',	'115703776',	'Branded',	'300x250',	'217059211824073971',	'Private auction',	dt)
           //getdeal(	'a2c22000000EFn5',	'Brandman ',	'115723456',	'Branded',	'300x250',	'217059211824078145',	'Private auction',	dt),
           //getdeal(	'a2c22000000EFl0',	'AARP',	        '115617136',	'Branded',	'300x250',	'217059211824082900',	'Private auction',	dt),
           //getdeal(	'a2c22000000EFl7',	'AT&T',	        '115723456',	'Branded',	'300x250',	'217059211824072631',	'Private auction',	dt),
           //getdeal(	'a2c22000000EFnc',	'Cars.com',	    '115727536',	'Branded',	'300x250',	'217059211824081671',	'Private auction',	dt),
           //getdeal(	'a2c22000000EFnh',	'Centro',	    '115723456',	'Branded',	'300x250',	'217059211824082897',	'Private auction',	dt),
           //getdeal(	'a2c22000000EFno',	'Charles S',	'115727536',	'Branded',	'300x250',	'217059211824077634',	'Private auction',	dt)
           }; 

           //Preferred Deals
		   list<AdEx_Response_temp__c> preferredDeals= new List<AdEx_Response_temp__c>{
           getDeal(	'a2c22000000EFlT',	'Allstate',	'115723456',	'Branded',	'300x250',	'217059211824081026',	'Preferred deal',	dt),
           getDeal(	'a2c22000000EFlU',	'Allstate',	'115727536',	'Branded',	'300x250',	'217059211824081026',	'Preferred deal',	dt)
           //getDeal(	'a2c22000000EFlV',	'Allstate',	'115727656',	'Branded',	'300x250',	'217059211824081026',	'Preferred deal',	dt),
           //getDeal(	'a2c22000000EFlW',	'Allstate',	'115728736',	'Branded',	'300x250',	'217059211824081026',	'Preferred deal',	dt),
           //getDeal(	'a2c22000000EFlX',	'Allstate',	'115728856',	'Branded',	'300x250',	'217059211824081026',	'Preferred deal',	dt),
           //getDeal(	'a2c22000000EFlY',	'Allstate',	'115729096',	'Branded',	'300x250',	'217059211824081026',	'Preferred deal',	dt)
		   };   

		   //Open Auctiondeals
		   list<AdEx_Response_temp__c> OpenAuctinDeals= new List<AdEx_Response_temp__c>{
            getDeal(	'a2c22000000EG3u',	'',	'0',	        'Anonymous',	'300x250',	'Open auction',	'Open auction',	dt),
            getDeal(	'a2c22000000EG3v',	'',	'0',	        'Branded',	'(unknown)',	'Open auction',	'Open auction',	dt),
            getDeal(	'a2c22000000EG3w',	'',	'115617136',	'Anonymous',	'300x250',	'Open auction',	'Open auction',	dt)
            //getDeal(	'a2c22000000EG3x',	'',	'115617136',	'Branded',	'300x250',	'Open auction',	'Open auction',	dt),
            //getDeal(	'a2c22000000EG3y',	'',	'115672936',	'Anonymous',	'300x250',	'Open auction',	'Open auction',	dt)
		   };                                                 
		   
		   //firstlook Deals
		   list<AdEx_Response_temp__c> firstLookDeals= new List<AdEx_Response_temp__c>{
            getdeal(	'Branded',	'3M',	'115727656',	'Branded',	'300x700',	'n/a',	'First Look',	dt)

		   };  

		   List<AdEx_Response_temp__c> adxDeals= new List<AdEx_Response_temp__c>();
		   adxDeals.addALL(privateAuctionDeals);
		   adxDeals.addALL(preferredDeals);
		   adxDeals.addALL(OpenAuctinDeals);
		   adxDeals.addALL(firstLookDeals);

		   insert adxDeals;



		                                                 
		                                                      

     
     
    }

    private static void createAccountMappings(List<AdEx_Response_temp__c> adxdeals, Id AccountId)
	{
      List<AdEx_Account_Mapping__c> acctmappings= new list<AdEx_Account_Mapping__c>();
      for(AdEx_Response_temp__c deal: adxdeals)
      {
        acctmappings.add(new AdEx_Account_Mapping__c(Account__c=AccountId,
        											 name=deal.Advertizer_Name__c) );
      }

      insert acctmappings;

	}


    public static void createproductAndPriceBookEntries()
    {
       /*Product2 prod = new Product2(Name = 'Web',Auto_Schedule__c=true, CanUseRevenueSchedule=true,
            Family = 'Hardware');
        insert prod;*/
        
        Product2 prod1 = new Product2(Name = 'Mobile',
            Family = 'Software',IsActive=true);
        insert prod1;
        
        // Get standard price book ID.
        // This is available irrespective of the state of SeeAllData.
        Id standard = Test.getStandardPricebookId();
     
        PricebookEntry standardPrice = new PricebookEntry(
            Pricebook2Id = standard, Product2Id = prod1.Id,
            UnitPrice = 10000, IsActive = true);
        insert standardPrice;

        system.assert(standardPrice.id!=null,'Pricebook entry not created.');

        /*PricebookEntry standardPrice1 = new PricebookEntry(
            Pricebook2Id = standard, Product2Id = prod1.Id,
            UnitPrice = 12000, IsActive = true);
        insert standardPrice1;*/
        
        
       /* // Create a custom price book
        Pricebook2 customPB = new Pricebook2(Name='Custom Pricebook', isActive=true);
        insert customPB;
        
        // 2. Insert a price book entry with a custom price.
        PricebookEntry customPrice = new PricebookEntry(
            Pricebook2Id = standard, Product2Id = prod1.Id,
            UnitPrice = 12000, IsActive = true);
        insert customPrice;*/

    }

    public static ADXJSON getAdxJsonresponse(){

    	List<ADXJSON.Headers> headerList= new List<ADXJSON.Headers>();
        
       
        /*//ADVERTISER_NAME
        ADXJSON.Headers adv_name= new ADXJSON.Headers();
        adv_name.name='adv_name';
        adv_name.type_Z='Dimension';
        headerList.add(adv_name);*/

        //DFP_AD_UNIT_ID

        ADXJSON.Headers DFP_AD_UNIT_ID= new ADXJSON.Headers();
        DFP_AD_UNIT_ID.name='adv_name';
        DFP_AD_UNIT_ID.type_Z='Dimension';
        headerList.add(DFP_AD_UNIT_ID);

        //DEAL_ID
        ADXJSON.Headers DEAL_ID= new ADXJSON.Headers();
        DEAL_ID.name='adv_name';
        DEAL_ID.type_Z='Dimension';
        headerList.add(DEAL_ID);


        //BRANDING_TYPE_NAME
        ADXJSON.Headers BRANDING_TYPE_NAME= new ADXJSON.Headers();
        BRANDING_TYPE_NAME.name='adv_name';
        BRANDING_TYPE_NAME.type_Z='Dimension';
        headerList.add(BRANDING_TYPE_NAME);


        //TRANSACTION_TYPE_NAME
        ADXJSON.Headers TRANSACTION_TYPE_NAME= new ADXJSON.Headers();
        TRANSACTION_TYPE_NAME.name='adv_name';
        TRANSACTION_TYPE_NAME.type_Z='Dimension';
        headerList.add(TRANSACTION_TYPE_NAME);

        //AD_UNIT_SIZE_NAME
        ADXJSON.Headers AD_UNIT_SIZE_NAME= new ADXJSON.Headers();
        AD_UNIT_SIZE_NAME.name='adv_name';
        AD_UNIT_SIZE_NAME.type_Z='Dimension';
        headerList.add(AD_UNIT_SIZE_NAME);

         //Date
        ADXJSON.Headers ddate= new ADXJSON.Headers();
        ddate.name='Date';
        ddate.type_Z='Dimension';
        headerList.add(ddate);


        //AD_IMPRESSIONS
        ADXJSON.Headers impressions= new ADXJSON.Headers();
        impressions.name='impressions';
        impressions.type_Z='Metric';
        headerList.add(impressions);
        
        //earnings
        ADXJSON.Headers earnings= new ADXJSON.Headers();
        earnings.name='earnings';
        earnings.type_Z='Metric';
        headerList.add(earnings);

        //CLICKS
        ADXJSON.Headers CLICKS= new ADXJSON.Headers();
        CLICKS.name='earnings';
        CLICKS.type_Z='Metric';
        headerList.add(CLICKS);


        



        
        ADXJSON adXJsonResponseObj= new ADXJSON();
        adXJsonResponseObj.headers=headerList;

        adXJsonResponseObj.totalMatchedRows='10';
        adXJsonResponseObj.totals=new List<String>{'100','1000','100'};
        adXJsonResponseObj.rows= new List<List<String>>{new List<String>{'Allstate',
                                                                         '115723456',
                                                                         '217059211824081026',
                                                                         'Branded',
                                                                         'Preferred deal',
                                                                         '300x250',
                                                                         '2016-07-15',
                                                                         '11',
                                                                         '0.05',
                                                                         '0'},
                               						 new List<String>{'Allstate',
   '115727536',
   '217059211824081026',
   'Branded',
   'Preferred deal',
   '300x250',
   '2016-07-15',
   '461',
   '1.97',
   '0'},
                               						 new List<String>{'Allstate',
   '115727656',
   '217059211824081026',
   'Branded',
   'Preferred deal',
   '300x250',
   '2016-07-15',
   '19115',
   '81.66',
   '8'},
                               						 new List<String>{  'Allstate',
   '115728736',
   '217059211824081026',
   'Branded',
   'Preferred deal',
   '300x250',
   '2016-07-15',
   '76',
   '0.32',
   '0'
}
                               						
                               						};
        String adXJsonResponse= JSON.serialize(adXJsonResponseObj);          

                    						
        
     return adXJsonResponseObj;
        
    
    }
    

    
    


}