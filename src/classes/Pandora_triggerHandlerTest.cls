/*
Venkata R Sabbela
09/14/16.
*/

@isTest
public with sharing class Pandora_triggerHandlerTest {
    
    @testSetup
    public static  void insertLMSGroupping()
    {
        List<LMS_Public_Group_Mapping__c> LMSList = generateGroupMapping(1);
        if(LMSList.size() > 0){
          Test.startTest();
            insert LMSList;
            Test.stopTest();
        } 
        List<LMS_Public_Group_Mapping_criteria__c> groupMapList = generateGroupMappingCriteria(1, LMSList[0].id);
        if(groupMapList.size() > 0){
         Insert groupMapList;
            system.debug('groupMapList RR:'+groupMapList);
            List<LMS_Public_Group_Mapping__c> All_LMS_Groups= [select id,Name, ManagerId__c,(select id,field_Name__c,inclusion_criteria__c,exclusion_criteria__c from LMS_Public_Group_Mapping_criteria__r), Is_Manager__c, function_group__c,Group_Name__c,Location__c,Cost_Center__c,Title__c,Exclusion_Criteria__C,Inclusion_Criteria__c  from LMS_Public_Group_Mapping__c limit 49999];
             system.debug('All_LMS_Groups MMM'+All_LMS_Groups.size());
            
        } 
    }
    @testSetup
    public static  void insertLMSGroupping2(){        
        LMS_Public_Group_Mapping__c mapping= new LMS_Public_Group_Mapping__c();
        mapping.Is_Manager__c='Al';
        mapping.function_group__c='test function group';
        mapping.Cost_Center__c ='3020';
        mapping.Location__c ='test Location';
        mapping.Manager__c ='All';
        mapping.Title__c ='VP';
        mapping.group_name__c='Test Group 2';
        Test.startTest();
        insert mapping;
        Test.stopTest();
        
    }
    @testSetup
    public static  void insertGroup()
    {
        List<Group> groupList  = generateGroup(1);
        if(groupList.size() > 0){
          Test.startTest();
            Insert groupList;
            Test.stopTest();
            system.debug('groupList SSS:'+groupList);
        } 
    }

    @testSetup
    private static  void insertOrUpdateSingleUser()    { 
        Account testAccount = UTIL_testUtil.generateAccount();
        testAccount.Name = 'Pandora Media, Inc.';
        system.debug('testAccount'+testAccount);
        insert testAccount;
        List<User> users = generateUsers(1);
        Test.startTest();
        insert users;
        Test.stopTest();
        List<user> user = [Select id, Cost_Center__c from User where id =: users[0].id];
        system.assert(user.size()>0, 'User record does not exist in the database');
        //Update a record
        if(users.size() >0 && users != null){ 
            users[0].Cost_Center__c = '3230';
            update users[0];
        }
        system.assertEquals('3230',users[0].Cost_Center__c);
        List<LMS_Public_Group_Mapping__c> All_LMS_Groups3= [select id,Name, ManagerId__c,(select id,field_Name__c,inclusion_criteria__c,exclusion_criteria__c from LMS_Public_Group_Mapping_criteria__r), Is_Manager__c, function_group__c,Group_Name__c,Location__c,Cost_Center__c,Title__c,Exclusion_Criteria__C,Inclusion_Criteria__c  from LMS_Public_Group_Mapping__c limit 49999];
        //system.debug('All_LMS_Groups MMM'+All_LMS_Groups.Name);
        system.debug('All_LMS_Groups OOO'+All_LMS_Groups3.size());
    }
    
    
    private static  void bulkTestforUserTrigger()
    {
      Account testAccount = UTIL_testUtil.generateAccount();
        testAccount.Name = 'Pandora Media, Inc.';
        system.debug('testAccount'+testAccount);
        insert testAccount;
        List<Profile> profiles = [Select Id from Profile Where Name='System Administrator'];
        system.assert(profiles.size()>0, 'Profile Does Not Exist');    
        Id ProfileId= profiles[0].Id;
        List<User> userList= new List<User>();
        for(integer i=0;i<300;i++)
        {
            user u=getUser(ProfileId,'testemail@testemail.com.pandora'+i,'test'+i);
            userList.add(u);
        }
        
        Test.startTest();
        insert userList;
        Test.stopTest();
        
        List<User> qUserList=[select id,name from user where createdDate=Today]; 
        system.assert(qUserList.size()>0, 'No users returned from the query');
        List<User> updateUserList= new List<User>();
         for(User usr: qUserList)
        {
            usr.LastName = usr.LastName+'_Updated';
        }
        update qUserList;
        
    }
    public static user getUser(Id ProfileId,String userName, String alias)
    {
        User u = new User(EmailEncodingKey='UTF-8', LastName='Testing', LanguageLocaleKey='en_US',LocaleSidKey='en_US',TimeZoneSidKey='America/Los_Angeles', ProfileId=ProfileId,
                          Email='testemail@testemail.com', Username=userName  );
        
        U.function_group__c='test function';
        u.Cost_Center__c='test cost center2';
        u.WD_Location__c='test Location';
        u.Title='title';
        u.Is_Manager__c=True;
        u.Alias=alias;
        return u; 
    }
    
    
    private static testMethod void testHandlerUser(){        
        TriggerHandler_User handler= new TriggerHandler_User(false,0); 
        LMS_Group_Assignment_Handler.isRun=true;
        try
        {
            boolean istgrContext=handler.IsTriggerContext;
            boolean isvfpcontext=handler.IsVisualforcePageContext;
            boolean iswebserviceContext=handler.IsWebServiceContext;
        }
        catch(Exception Ex)
        {
            system.debug('-->'+Ex.getMessage());
        }
    }
    
    /*=========================================================================================== */
    public static List<User> generateUsers(Integer totalCount)
    {
        Profile profile = [Select Id from Profile Where Name='Standard User' Limit 1];
        List<user> userList = new List<user>();
        for(Integer i=1; i<= totalCount; i++){
            User insertUser = new User(EmailEncodingKey='UTF-8', LastName='TestUser LN'+i,  LanguageLocaleKey='en_US', LocaleSidKey='en_US',TimeZoneSidKey='America/Los_Angeles');
            insertUser.function_group__c='test function';
            insertUser.Cost_Center__c='3020';
            insertUser.WD_Location__c='test Location';
            insertUser.Title='VP';
            insertUser.Is_Manager__c= boolean.valueOf(True);
            insertUser.ProfileId=profile.id;
            insertUser.Email='testemaillmstest@testemail.com'+i;
            insertUser.Username='testemaillmstest@testemail.com.pandora'+i;
            insertUser.Alias='test'+i;
            userList.add(insertUser);
        }
        return userList;
    }
    public static  List<LMS_Public_Group_Mapping_criteria__c> generateGroupMappingCriteria(Integer numGroupMap, String groupMapId){
        List<LMS_Public_Group_Mapping_criteria__c> groupMapList = new List<LMS_Public_Group_Mapping_criteria__c>();
        for(Integer i = 0; i < numGroupMap; i++){
            LMS_Public_Group_Mapping_criteria__c groupMap = new LMS_Public_Group_Mapping_criteria__c();
            groupMap.field_Name__c='Title';
            groupMap.exclusion_criteria__c='Director';
            groupMap.Inclusion_Criteria__c = 'VP';
            groupMap.LMS_Public_Group_Mapping__c = groupMapId;
            groupMapList.add(groupMap);
        }
        return groupMapList;   
    } 
    
    public static  List<Group> generateGroup(Integer numGroup){
        List<Group> groupList = new List<Group>();
        for(Integer i = 0; i < numGroup; i++){
            Group grou = new Group();
            grou.Name= 'Test Group';
            grou.Type='Regular';
            groupList.add(grou);
        }
        return groupList;   
    } 
    
    public static  List<LMS_Public_Group_Mapping__c> generateGroupMapping(Integer numLMSGroup){
        List<LMS_Public_Group_Mapping__c> LMSGroups= new List<LMS_Public_Group_Mapping__c>();
        for(Integer i = 0; i < numLMSGroup; i++){
            LMS_Public_Group_Mapping__c mapping= new LMS_Public_Group_Mapping__c();
            mapping.Is_Manager__c='All';
            mapping.function_group__c='test function';
            mapping.Cost_Center__c ='3020';
            mapping.Location__c ='test Location';
            mapping.Manager__c ='All';
            mapping.Title__c ='VP';
            mapping.group_name__c='Test Group';
            LMSGroups.add(mapping);
        }
        return LMSGroups;
    }
    
    public static testmethod void lmsPublicgroupMappingTest()
    {
        LMS_Public_Group_Mapping__c mapping= new LMS_Public_Group_Mapping__c();
        mapping.Is_Manager__c='All';
        mapping.function_group__c='test function';
        mapping.Cost_Center__c ='test cost center';
        mapping.Location__c ='test Location';
        mapping.Manager__c ='All';
        mapping.Title__c ='title';
        mapping.group_name__c='Test Group';
        insert mapping;
        
        LMS_Public_Group_Mapping_criteria__c lc = new LMS_Public_Group_Mapping_criteria__c();
        lc.field_Name__c='Title';
        lc.exclusion_criteria__c='Director';
        lc.LMS_Public_Group_Mapping__c = mapping.id;
        insert lc;
        
        LMS_Public_Group_Mapping__c mapping2= new LMS_Public_Group_Mapping__c();
        mapping2.Is_Manager__c='All';
        mapping2.function_group__c='test function';
        mapping2.Cost_Center__c ='test cost center';
        mapping2.Location__c ='test Location';
        mapping2.Manager__c ='All';
        mapping2.Title__c ='title';
        mapping2.group_name__c='Test Group';
        insert mapping2;
        
        LMS_Public_Group_Mapping_criteria__c lc1 = new LMS_Public_Group_Mapping_criteria__c();
        lc1.field_Name__c='Title';
        lc1.Inclusion_Criteria__c='Director';
        lc1.LMS_Public_Group_Mapping__c = mapping2.id;
        insert lc1;
        
        Group testGroup = new Group();
        testGroup.Name='Test Group';
        testGroup.Type='Regular';
        insert testGroup;
        
        Group testGroup2 = new Group();
        testGroup2.Name='Test Group2';
        testGroup2.Type='Regular';
        insert testGroup2;
        List<Profile> profiles = [Select Id from Profile Where Name='System Administrator'];
        User insertTest1 = new User(EmailEncodingKey='UTF-8', LastName='Testing', LanguageLocaleKey='en_US',LocaleSidKey='en_US',TimeZoneSidKey='America/Los_Angeles');
        insertTest1.function_group__c='test function';
        insertTest1.Cost_Center__c='test cost center2';
        insertTest1.WD_Location__c='test Location';
        insertTest1.Title='title';
        insertTest1.Is_Manager__c=True;
        insertTest1.ProfileId=profiles[0].id;
        insertTest1.Email='testemail1@testemail.com';
        insertTest1.Username='testemail1@testemail.com.pandora';
        insertTest1.Alias='test';
        Test.startTest();
        insert  insertTest1;
        Test.stopTest();
        
        system.runAs(insertTest1)
        {
            User updUser = [select id,Title from User where id=:insertTest1.Id];
            updUser.Title = 'Director';
            update updUser;
        }        
    }
    
    
    
    
    private static TestMethod void TestLMSGroupAssignmentHandlerExCriteria() 
    {                                                              
        LMS_Public_Group_Mapping__c mapping1 =getLMSPublicGroupMapping('All',//String Is_Manager,
                                                                       'test function',//String function_group
                                                                       'test cost center',// String Cost_Center
                                                                       'test Location',//String Location
                                                                       'All',//String Manager
                                                                       'title',//String Title
                                                                       'Test Group'//String group_name
                                                                       );
         
         Test.startTest();                                                            
        insert mapping1;
        Test.stopTest();
        LMS_Public_Group_Mapping_criteria__c mappingCriteria1;        
        mappingCriteria1= getLMS_Public_Group_Mapping_criteria('Title',//fieldname
                                                               mapping1.id );
        mappingCriteria1.exclusion_criteria__c='Director';
        insert mappingCriteria1;
        
        List<Profile> profiles = [Select Id from Profile Where Name='System Administrator'];
        system.assert(profiles.size()>0, 'Profile Not Found.');
        Id ProfileId=Profiles[0].Id;        
        Group group1= getGroup('Test Group');
        insert group1;
        
        User U=getUser(profileId,//id ProfileId, 
                       'testemail1@testemail.com.pandora',//String UserName,
                       'testemail1@testemail.com',//String Email,
                       'testur', //String Alias
                       mapping1);
        insert U;
        
    }
    
    private static testMethod void TestLMSGroupAssignmentHandlerExCriteria1()
    {
        LMS_Public_Group_Mapping__c mapping1 =getLMSPublicGroupMapping('All','test function', 'test cost center',// String Cost_Center
                                                                       'test Location', 'All', 'title',//String Title
                                                                          'Test Group' );
         Test.startTest();                                                            
        insert mapping1;   
        Test.stopTest();     
        LMS_Public_Group_Mapping_criteria__c mappingCriteria1;        
        mappingCriteria1= getLMS_Public_Group_Mapping_criteria('Cost Center',//fieldname
                                                               mapping1.id );
        mappingCriteria1.exclusion_criteria__c='Director';
        insert mappingCriteria1;
        
        List<Profile> profiles = [Select Id from Profile Where Name='System Administrator'];
        system.assert(profiles.size()>0, 'Profile Not Found.');
        Id ProfileId=Profiles[0].Id;        
        Group group1= getGroup('Test Group');
        insert group1;
        
        User U=getUser(profileId,//id ProfileId, 
                       'testemail2@testemail.com.pandora',//String UserName,
                       'testemail2@testemail.com',//String Email,
                       'testue', //String Alias
                       mapping1);
        insert U;
    }
    
    private static testMethod void TestLMSGroupAssignmentHandlerInCriteria()
    {
        LMS_Public_Group_Mapping__c mapping1 =getLMSPublicGroupMapping('All',//String Is_Manager,
                                                                       'test function',//String function_group
                                                                       'test cost center',// String Cost_Center
                                                                       'test Location',//String Location
                                                                       'All',//String Manager
                                                                       'title',//String Title
                                                                       'Test Group'//String group_name
                                                                      );
        Test.startTest();                                                           
        insert mapping1;   
        Test.stopTest();  
        
        LMS_Public_Group_Mapping_criteria__c mappingCriteria1;        
        mappingCriteria1= getLMS_Public_Group_Mapping_criteria('Title',//fieldname
                                                               mapping1.id
                                                              );
        mappingCriteria1.Inclusion_Criteria__c='Director';
        insert mappingCriteria1;        
        List<Profile> profiles = [Select Id from Profile Where Name='Standard User'];
        system.assert(profiles.size()>0, 'Profile Not Found.');
        Id ProfileId=Profiles[0].Id;
        
        Group group1= getGroup('Test Group');
        insert group1;
        
        User U=getUser(profileId,//id ProfileId, 
                       'testemail3@testemail1.com.pandora',//String UserName,
                       'testemail3@testemail1.com',//String Email,
                       'testo', //String Alias
                       mapping1);
        
        insert U;
        
    }
    
    
    public static LMS_Public_Group_Mapping__c getLMSPublicGroupMapping(String Is_Manager,  String function_group,  String Cost_Center,  String Location,
                                                                       String Manager, String Title, String group_name)
    {
        LMS_Public_Group_Mapping__c mapping= new LMS_Public_Group_Mapping__c();        
        mapping.Is_Manager__c=Is_Manager;
        mapping.function_group__c=function_group;
        mapping.Cost_Center__c=Cost_Center;
        mapping.Location__c=Location;
        mapping.Manager__c=Manager;
        mapping.Title__c=Title;
        mapping.group_name__c=group_name;        
        return mapping;
    }
    
    public static LMS_Public_Group_Mapping_criteria__c getLMS_Public_Group_Mapping_criteria(String field_Name, Id LMS_Public_Group_Mapping)
    {
        LMS_Public_Group_Mapping_criteria__c mappingCriteria= new LMS_Public_Group_Mapping_criteria__c();
        mappingCriteria.field_Name__c=field_Name;//'Title';
        mappingCriteria.exclusion_criteria__c='';//criteria;//='Director';
        mappingCriteria.Inclusion_criteria__c='';
        mappingCriteria.LMS_Public_Group_Mapping__c = LMS_Public_Group_Mapping;
        return  mappingCriteria;
        
    }
    
    public static User getUser(id ProfileId, 
                               String UserName,
                               String Email,
                               String Alias,
                               LMS_Public_Group_Mapping__c mapping)
    {
        User usr = new User(EmailEncodingKey='UTF-8', LastName='Testing', LanguageLocaleKey='en_US',LocaleSidKey='en_US',TimeZoneSidKey='America/Los_Angeles');
        usr.ProfileId=ProfileId;
        usr.Email='testemail@testemail.com';
        usr.Username=UserName;
        usr.Alias=Alias;  
        usr.function_group__c=mapping.function_group__c;//'test function'; 
        usr.Cost_Center__c=mapping.Cost_Center__c;//'test cost center2';
        usr.WD_Location__c=mapping.Location__c;//'test Location';
        usr.Title=mapping.Title__c;//'title';
        usr.Is_Manager__c=boolean.valueOf(mapping.Is_Manager__c);  
        return usr; 
    }
    
    public static group getGroup(String name)
    {
        Group g = new Group();
        g.Name=name;//'Test Group';
        g.Type='Regular';
        return g;
        
    }
    
}