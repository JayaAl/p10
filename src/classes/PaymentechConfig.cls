public with sharing class PaymentechConfig {
	private String user;
	private String pass;
	private String merchantId;
	private String bin;
	private String endPoint;
	private String urn;
	private String wsdl;
	private static PaymentechConfig config;
	
	private PaymentechConfig()
	{
		PaymentechGateway_Info__c PG = PaymentechGateway_Info__c.getOrgDefaults();
		user = PG.Username__c;
		System.debug(user);
		pass = EncodingUtil.base64Decode(PG.Password__c).toString();
		System.debug(pass);
		merchantId = PG.MerchantId__c;
		bin = PG.Bin__c;
		System.debug(bin);
		endPoint = PG.Endpoint__c;
		urn = PG.URN__c;
		wsdl = PG.WSDL__c;
	}
	
	public static PaymentechConfig getConfig()
	{
		if(config == null)
			config = new PaymentechConfig();
		return config;
	}
	
	public String getEndPoint()
	{
		return endPoint;
	}
	
	public String getUrn()
	{
		return urn;
	}
	
	public String getWsdl()
	{
		return wsdl;
	}
	
	public String getUsername()
	{
		return user;
	}
	
	public String getPassword()
	{
		return pass;
	}
	
	public String getMerchantId()
	{
		return merchantId;
	}
	
	public String getBin()
	{
		return bin;
	}
}