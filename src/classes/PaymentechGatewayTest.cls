@isTest(SeeAllData=true)
public class PaymentechGatewayTest {
	static testmethod void testProfiles() {
		PaymentechGateway.BaseElementsType bt  = new PaymentechGateway.BaseElementsType(); 
        bt.industryType = 'test';
        bt.cardBrand = 'test';
        bt.ccAccountNum = 'test';
        bt.encryptedPan = 'test';
        bt.encryptedPanMethod = 'test';
        bt.encryptedPanKey = 'test';
        bt.encryptedPanPublicKeyFingerPrint = 'test';
        bt.encryptedPanHash = 'test';
        bt.ccExp = 'test';
        bt.ccCardVerifyPresenceInd = 'test';
        bt.ccCardVerifyNum = 'test';
        bt.ecpCheckRT = 'test';
        bt.ecpCheckDDA = 'test';
        bt.ecpBankAcctType = 'test';
        bt.ecpAuthMethod = 'test';
        bt.ecpDelvMethod = 'test';
        bt.avsZip = 'test';
        bt.avsAddress1 = 'test';
        bt.avsAddress2 = 'test';
        bt.avsCity = 'test';
        bt.avsState = 'test';
        bt.avsName = 'test';
        bt.avsCountryCode = 'test';
        bt.avsPhone = 'test';
        bt.avsDestName = 'test';
        bt.avsDestAddress1 = 'test';
        bt.avsDestAddress2 = 'test';
        bt.avsDestCity = 'test';
        bt.avsDestState = 'test';
        bt.avsDestZip = 'test';
        bt.avsDestCountryCode = 'test';
        bt.avsDestPhoneNum = 'test';
        bt.useCustomerRefNum = 'test';
        bt.authenticationECIInd = 'test';
        bt.verifyByVisaCAVV = 'test';
        bt.verifyByVisaXID = 'test';
        bt.orderID = 'test';
        bt.amount = 'test';
        bt.comments = 'test';
        bt.mcSecureCodeAAV = 'test';
        bt.retryTrace = 'test';
        bt.customerEmail = 'test';
        bt.euddBankSortCode = 'test';
        bt.euddCountryCode = 'test';
        bt.euddRibCode = 'test';
        bt.bmlCustomerIP = 'test';
        bt.bmlCustomerEmail = 'test';
        bt.bmlShippingCost = 'test';
        bt.bmlTNCVersion = 'test';
        bt.bmlCustomerRegistrationDate = 'test';
        bt.bmlCustomerTypeFlag = 'test';
        bt.bmlItemCategory = 'test';
        bt.bmlPreapprovalInvitationNum = 'test';
        bt.bmlMerchantPromotionalCode = 'test';
        bt.bmlCustomerBirthDate = 'test';
        bt.bmlCustomerSSN = 'test';
        bt.bmlCustomerAnnualIncome = 'test';
        bt.bmlCustomerResidenceStatus = 'test';
        bt.bmlCustomerCheckingAccount = 'test';
        bt.bmlCustomerSavingsAccount = 'test';
        bt.bmlProductDeliveryType = 'test';
        bt.debitBillerReferenceNumber = 'test';
        bt.useStoredAAVInd = 'test';
        bt.ecpCheckSerialNumber = 'test';
        bt.ecpTerminalCity = 'test';
        bt.ecpTerminalState = 'test';
        bt.ecpImageReferenceNumber = 'test';
        bt.customerAni = 'test';
        bt.avsPhoneType = 'test';
        bt.avsDestPhoneType = 'test';
        bt.customerIpAddress = 'test';
        bt.emailAddressSubtype = 'test';
        bt.customerBrowserName = 'test';
        bt.shippingMethod = 'test';
        
        PaymentechGateway.NewOrderResponseElement ele  = new PaymentechGateway.NewOrderResponseElement();
        ele.version = 'test';
        ele.industryType = 'test';
        ele.transType = 'test';
        ele.bin = 'test';
        ele.merchantID = 'test';
        ele.terminalID = 'test';
        ele.cardBrand = 'test';
        ele.orderID = 'test';
        ele.txRefNum = 'test';
        ele.txRefIdx = 'test';
        ele.respDateTime = 'test';
        ele.procStatus = 'test';
        ele.approvalStatus = 'test';
        ele.respCode = 'test';
        ele.avsRespCode = 'test';
        ele.cvvRespCode = 'test';
        ele.authorizationCode = 'test';
        ele.mcRecurringAdvCode = 'test';
        ele.visaVbVRespCode = 'test';
        ele.procStatusMessage = 'test';
        ele.respCodeMessage = 'test';
        ele.hostRespCode = 'test';
        ele.hostAVSRespCode = 'test';
        ele.hostCVVRespCode = 'test';
        ele.retryTrace = 'test';
        ele.retryAttempCount = 'test';
        ele.lastRetryDate = 'test';
        ele.customerRefNum = 'test';
        ele.customerName = 'test';
        ele.profileProcStatus = 'test';
        ele.profileProcStatusMsg = 'test';
        ele.giftCardInd = 'test';
        ele.remainingBalance = 'test';
        ele.requestAmount = 'test';
        ele.redeemedAmount = 'test';
        ele.ccAccountNum = 'test';
        ele.debitBillerReferenceNumber = 'test';
        ele.mbMicroPaymentDaysLeft = 'test';
        ele.mbMicroPaymentDollarsLeft = 'test';
        ele.mbStatus = 'test';
        ele.debitPinSurchargeAmount = 'test';
        ele.debitPinTraceNumber = 'test';
        ele.debitPinNetworkID = 'test';
        ele.partialAuthOccurred = 'test';
        ele.countryFraudFilterStatus = 'test';
        ele.isoCountryCode = 'test';
        ele.fraudScoreProcStatus = 'test';
        ele.fraudScoreProcMsg = 'test';
        
        PaymentechGateway.PC3LineItem item  = new PaymentechGateway.PC3LineItem();
        item.pCard3DtlIndex = 'test';
        item.pCard3DtlDesc = 'test';
        item.pCard3DtlProdCd = 'test';
        item.pCard3DtlQty = 'test';
        item.pCard3DtlUOM = 'test';
        item.pCard3DtlTaxAmt = 'test';
        item.pCard3DtlTaxRate = 'test';
        item.pCard3Dtllinetot = 'test';
        item.pCard3DtlDisc = 'test';
        item.pCard3DtlCommCd = 'test';
        item.pCard3DtlUnitCost = 'test';
        item.pCard3DtlGrossNet = 'test';
        item.pCard3DtlTaxType = 'test';
        item.pCard3DtlDiscInd = 'test';
        item.pCard3DtlDebitInd = 'test';
        item.pCard3DtlDiscountRate = 'test';
        
        PaymentechGateway.SoftMerchantDescriptorsType descr  = new PaymentechGateway.SoftMerchantDescriptorsType();
        descr.smdDBA = 'test';
        descr.smdMerchantID = 'test';
        descr.smdContactInfo = 'test';
        descr.smdStreet = 'test';
        descr.smdCity = 'test';
        descr.smdRegion = 'test';
        descr.smdPostalCode = 'test';
        descr.smdCountryCode = 'test';
        descr.smdMCC = 'test';
        
        PaymentechGateway.NewOrderRequestElement eleR  = new PaymentechGateway.NewOrderRequestElement();
        eleR.orbitalConnectionUsername = 'test';
        eleR.orbitalConnectionPassword = 'test';
        eleR.version = 'test';
        eleR.industryType = 'test';
        eleR.transType = 'test';
        eleR.bin = 'test';
        eleR.merchantID = 'test';
        eleR.terminalID = 'test';
        eleR.cardBrand = 'test';
        eleR.ccAccountNum = 'test';
        eleR.encryptedPan = 'test';
        eleR.encryptedPanMethod = 'test';
        eleR.encryptedPanKey = 'test';
        eleR.encryptedPanPublicKeyFingerPrint = 'test';
        eleR.encryptedPanHash = 'test';
        eleR.ccExp = 'test';
        eleR.ccCardVerifyPresenceInd = 'test';
        eleR.ccCardVerifyNum = 'test';
        eleR.switchSoloIssueNum = 'test';
        eleR.switchSoloCardStartDate = 'test';
        eleR.ecpCheckRT = 'test';
        eleR.ecpCheckDDA = 'test';
        eleR.ecpBankAcctType = 'test';
        eleR.ecpAuthMethod = 'test';
        eleR.ecpDelvMethod = 'test';
        eleR.avsZip = 'test';
        eleR.avsAddress1 = 'test';
        eleR.avsAddress2 = 'test';
        eleR.avsCity = 'test';
        eleR.avsState = 'test';
        eleR.avsName = 'test';
        eleR.avsCountryCode = 'test';
        eleR.avsPhone = 'test';
        eleR.useCustomerRefNum = 'test';
        eleR.addProfileFromOrder = 'test';
        eleR.customerRefNum = 'test';
        eleR.profileOrderOverideInd = 'test';
        eleR.authenticationECIInd = 'test';
        eleR.verifyByVisaCAVV = 'test';
        eleR.verifyByVisaXID = 'test';
        eleR.priorAuthCd = 'test';
        eleR.orderID = 'test';
        eleR.amount = 'test';
        eleR.comments = 'test';
        eleR.shippingRef = 'test';
        eleR.taxInd = 'test';
        eleR.taxAmount = 'test';
        eleR.amexTranAdvAddn1 = 'test';
        eleR.amexTranAdvAddn2 = 'test';
        eleR.amexTranAdvAddn3 = 'test';
        eleR.amexTranAdvAddn4 = 'test';
        eleR.mcSecureCodeAAV = 'test';
        eleR.softDescMercName = 'test';
        eleR.softDescProdDesc = 'test';
        eleR.softDescMercCity = 'test';
        eleR.softDescMercPhone = 'test';
        eleR.softDescMercURL = 'test';
        eleR.softDescMercEmail = 'test';
        eleR.recurringInd = 'test';
        eleR.txRefNum = 'test';
        eleR.retryTrace = 'test';
        eleR.pCardOrderID = 'test';
        eleR.pCardDestZip = 'test';
        eleR.pCardDestName = 'test';
        eleR.pCardDestAddress = 'test';
        eleR.pCardDestAddress2 = 'test';
        eleR.pCardDestCity = 'test';
        eleR.pCardDestStateCd = 'test';
        eleR.pCard3FreightAmt = 'test';
        eleR.pCard3DutyAmt = 'test';
        eleR.pCard3DestCountryCd = 'test';
        eleR.pCard3ShipFromZip = 'test';
        eleR.pCard3DiscAmt = 'test';
        eleR.pCard3VATtaxAmt = 'test';
        eleR.pCard3VATtaxRate = 'test';
        eleR.pCard3AltTaxInd = 'test';
        eleR.pCard3AltTaxAmt = 'test';
        eleR.pCard3LineItemCount = 'test';
        eleR.magStripeTrack1 = 'test';
        eleR.magStripeTrack2 = 'test';
        eleR.retailTransInfo = 'test';
        eleR.customerName = 'test';
        eleR.customerEmail = 'test';
        eleR.customerPhone = 'test';
        eleR.cardPresentInd = 'test';
        eleR.euddBankSortCode = 'test';
        eleR.euddCountryCode = 'test';
        eleR.euddRibCode = 'test';
        eleR.bmlCustomerIP = 'test';
        eleR.bmlCustomerEmail = 'test';
        eleR.bmlShippingCost = 'test';
        eleR.bmlTNCVersion = 'test';
        eleR.bmlCustomerRegistrationDate = 'test';
        eleR.bmlCustomerTypeFlag = 'test';
        eleR.bmlItemCategory = 'test';
        eleR.bmlPreapprovalInvitationNum = 'test';
        eleR.bmlMerchantPromotionalCode = 'test';
        eleR.bmlCustomerBirthDate = 'test';
        eleR.bmlCustomerSSN = 'test';
        eleR.bmlCustomerAnnualIncome = 'test';
        eleR.bmlCustomerResidenceStatus = 'test';
        eleR.bmlCustomerCheckingAccount = 'test';
        eleR.bmlCustomerSavingsAccount = 'test';
        eleR.bmlProductDeliveryType = 'test';
        eleR.avsDestName = 'test';
        eleR.avsDestAddress1 = 'test';
        eleR.avsDestAddress2 = 'test';
        eleR.avsDestCity = 'test';
        eleR.avsDestState = 'test';
        eleR.avsDestZip = 'test';
        eleR.avsDestCountryCode = 'test';
        eleR.avsDestPhoneNum = 'test';
        eleR.debitBillerReferenceNumber = 'test';
        eleR.mbType = 'test';
        eleR.mbOrderIdGenerationMethod = 'test';
        eleR.mbRecurringStartDate = 'test';
        eleR.mbRecurringEndDate = 'test';
        eleR.mbRecurringNoEndDateFlag = 'test';
        eleR.mbRecurringMaxBillings = 'test';
        eleR.mbRecurringFrequency = 'test';
        eleR.mbMicroPaymentMaxDollarValue = 'test';
        eleR.mbMicroPaymentMaxBillingDays = 'test';
        eleR.mbMicroPaymentMaxTransactions = 'test';
        eleR.mbDeferredBillDate = 'test';
        eleR.debitPinNumber = 'test';
        eleR.debitPinSecurityControl = 'test';
        eleR.debitPinCashBack = 'test';
        eleR.partialAuthInd = 'test';
        eleR.accountUpdaterEligibility = 'test';
        eleR.useStoredAAVInd = 'test';
        eleR.ecpActionCode = 'test';
        eleR.ecpCheckSerialNumber = 'test';
        eleR.ecpTerminalCity = 'test';
        eleR.ecpTerminalState = 'test';
        eleR.ecpImageReferenceNumber = 'test';
        eleR.customerAni = 'test';
        eleR.avsPhoneType = 'test';
        eleR.avsDestPhoneType = 'test';
        eleR.customerIpAddress = 'test';
        eleR.emailAddressSubtype = 'test';
        eleR.customerBrowserName = 'test';
        eleR.shippingMethod = 'test';
        eleR.latitudeLongitude = 'test';
        eleR.politicalTimeZone = 'test';
        eleR.vendorID = 'test';
        eleR.softwareID = 'test';
        eleR.mobileDeviceType = 'test';
        eleR.deviceID = 'test';
        eleR.localDateTime = 'test';
        eleR.readerSerialNumber = 'test';
        eleR.keySerialNumber = 'test';
        eleR.encryptedMagStripeTrack2 = 'test';
        eleR.encryptionInd = 'test';
        
        PaymentechGateway.ProfileResponseElement resp  = new PaymentechGateway.ProfileResponseElement();
        resp.version = 'test';
        resp.bin = 'test';
        resp.merchantID = 'test';
        resp.customerName = 'test';
        resp.customerRefNum = 'test';
        resp.profileAction = 'test';
        resp.procStatus = 'test';
        resp.procStatusMessage = 'test';
        resp.customerAddress1 = 'test';
        resp.customerAddress2 = 'test';
        resp.customerCity = 'test';
        resp.customerState = 'test';
        resp.customerZIP = 'test';
        resp.customerEmail = 'test';
        resp.customerPhone = 'test';
        resp.customerCountryCode = 'test';
        resp.profileOrderOverideInd = 'test';
        resp.orderDefaultDescription = 'test';
        resp.orderDefaultAmount = 'test';
        resp.customerAccountType = 'test';
        resp.ccAccountNum = 'test';
        resp.ccExp = 'test';
        resp.ecpCheckDDA = 'test';
        resp.ecpBankAcctType = 'test';
        resp.ecpCheckRT = 'test';
        resp.ecpDelvMethod = 'test';
        resp.switchSoloCardStartDate = 'test';
        resp.switchSoloIssueNum = 'test';
        resp.mbType = 'test';
        resp.mbOrderIdGenerationMethod = 'test';
        resp.mbRecurringStartDate = 'test';
        resp.mbRecurringEndDate = 'test';
        resp.mbRecurringNoEndDateFlag = 'test';
        resp.mbRecurringMaxBillings = 'test';
        resp.mbRecurringFrequency = 'test';
        resp.mbMicroPaymentMaxDollarValue = 'test';
        resp.mbMicroPaymentMaxBillingDays = 'test';
        resp.mbMicroPaymentMaxTransactions = 'test';
        resp.mbDeferredBillDate = 'test';
        resp.mbMicroPaymentDaysLeft = 'test';
        resp.mbMicroPaymentDollarsLeft = 'test';
        resp.mbStatus = 'test';
        resp.mcSecureCodeAAV = 'test';
        resp.softDescMercName = 'test';
        resp.softDescProdDesc = 'test';
        resp.softDescMercCity = 'test';
        resp.softDescMercPhone = 'test';
        resp.softDescMercURL = 'test';
        resp.softDescMercEmail = 'test';
        resp.euddBankSortCode = 'test';
        resp.euddCountryCode = 'test';
        resp.euddRibCode = 'test';
        resp.status = 'test';
        resp.debitBillerReferenceNumber = 'test';
        resp.accountUpdaterEligibility = 'test';
        
        PaymentechGateway.ProfileFetchElement fetch  = new PaymentechGateway.ProfileFetchElement();
        fetch.orbitalConnectionUsername = 'test';
        fetch.orbitalConnectionPassword = 'test';
        fetch.version = 'test';
        fetch.bin = 'test';
        fetch.merchantID = 'test';
        fetch.customerName = 'test';
        fetch.customerRefNum = 'test';
        fetch.ccAccountNum = 'test';
        
        PaymentechGateway.ProfileChangeElement change  = new PaymentechGateway.ProfileChangeElement();
        change.orbitalConnectionUsername = 'test';
        change.orbitalConnectionPassword = 'test';
        change.version = 'test';
        change.bin = 'test';
        change.merchantID = 'test';
        change.customerName = 'test';
        change.customerRefNum = 'test';
        change.customerAddress1 = 'test';
        change.customerAddress2 = 'test';
        change.customerCity = 'test';
        change.customerState = 'test';
        change.customerZIP = 'test';
        change.customerEmail = 'test';
        change.customerPhone = 'test';
        change.customerCountryCode = 'test';
        change.customerProfileOrderOverideInd = 'test';
        change.orderDefaultDescription = 'test';
        change.orderDefaultAmount = 'test';
        change.customerAccountType = 'test';
        change.ccAccountNum = 'test';
        change.encryptedPan = 'test';
        change.encryptedPanMethod = 'test';
        change.encryptedPanKey = 'test';
        change.encryptedPanPublicKeyFingerPrint = 'test';
        change.encryptedPanHash = 'test';
        change.ccExp = 'test';
        change.ecpCheckDDA = 'test';
        change.ecpBankAcctType = 'test';
        change.ecpCheckRT = 'test';
        change.ecpDelvMethod = 'test';
        change.switchSoloCardStartDate = 'test';
        change.switchSoloIssueNum = 'test';
        change.mbType = 'test';
        change.mbOrderIdGenerationMethod = 'test';
        change.mbRecurringStartDate = 'test';
        change.mbRecurringEndDate = 'test';
        change.mbRecurringNoEndDateFlag = 'test';
        change.mbRecurringMaxBillings = 'test';
        change.mbRecurringFrequency = 'test';
        change.mbDeferredBillDate = 'test';
        change.mbMicroPaymentMaxDollarValue = 'test';
        change.mbMicroPaymentMaxBillingDays = 'test';
        change.mbMicroPaymentMaxTransactions = 'test';
        change.mbCancelDate = 'test';
        change.mbRestoreDate = 'test';
        change.mbRemoveFlag = 'test';
        change.mcSecureCodeAAV = 'test';
        change.softDescMercName = 'test';
        change.softDescProdDesc = 'test';
        change.softDescMercCity = 'test';
        change.softDescMercPhone = 'test';
        change.softDescMercURL = 'test';
        change.softDescMercEmail = 'test';
        change.euddBankSortCode = 'test';
        change.euddCountryCode = 'test';
        change.euddRibCode = 'test';
        change.status = 'test';
        change.debitBillerReferenceNumber = 'test';
        change.accountUpdaterEligibility = 'test';
        
        PaymentechGateway.FraudAnalysisResponseType analR  = new PaymentechGateway.FraudAnalysisResponseType();
        analR.fraudScoreIndicator = 'test';
        analR.fraudStatusCode = 'test';
        analR.riskInquiryTransactionID = 'test';
        analR.autoDecisionResponse = 'test';
        analR.riskScore = 'test';
        analR.kaptchaMatchFlag = 'test';
        analR.worstCountry = 'test';
        analR.customerRegion = 'test';
        analR.paymentBrand = 'test';
        analR.fourteenDayVelocity = 'test';
        analR.sixHourVelocity = 'test';
        analR.customerNetwork = 'test';
        analR.numberOfDevices = 'test';
        analR.numberOfCards = 'test';
        analR.numberOfEmails = 'test';
        analR.deviceLayers = 'test';
        analR.deviceFingerprint = 'test';
        analR.customerTimeZone = 'test';
        analR.customerLocalDateTime = 'test';
        analR.deviceRegion = 'test';
        analR.deviceCountry = 'test';
        analR.proxyStatus = 'test';
        analR.javascriptStatus = 'test';
        analR.flashStatus = 'test';
        analR.cookiesStatus = 'test';
        analR.browserCountry = 'test';
        analR.browserLanguage = 'test';
        analR.mobileDeviceIndicator = 'test';
        analR.mobileDeviceType = 'test';
        analR.mobileWirelessIndicator = 'test';
        analR.voiceDevice = 'test';
        analR.pcRemoteIndicator = 'test';
        analR.rulesDataLength = 'test';
        analR.rulesData = 'test';
        
        PaymentechGateway.ProfileDeleteElement delE  = new PaymentechGateway.ProfileDeleteElement();
        delE.orbitalConnectionUsername = 'test';
        delE.orbitalConnectionPassword = 'test';
        delE.version = 'test';
        delE.bin = 'test';
        delE.merchantID = 'test';
        delE.customerName = 'test';
        delE.customerRefNum = 'test';
        
        PaymentechGateway.FraudAnalysisType analT  = new PaymentechGateway.FraudAnalysisType();
        analT.fraudScoreIndicator = 'test';
        analT.rulesTrigger = 'test';
        analT.safetechMerchantID = 'test';
        analT.kaptchaSessionID = 'test';
        analT.websiteShortName = 'test';
        analT.cashValueOfFencibleItems = 'test';
        analT.customerDOB = 'test';
        analT.customerGender = 'test';
        analT.customerDriverLicense = 'test';
        analT.customerID = 'test';
        analT.customerIDCreationTime = 'test';
        analT.kttVersionNumber = 'test';
        analT.kttDataLength = 'test';
        analT.kttDataString = 'test';
        
        PaymentechGateway.ProfileAddElement addE  = new PaymentechGateway.ProfileAddElement();
        addE.orbitalConnectionUsername = 'test';
        addE.orbitalConnectionPassword = 'test';
        addE.version = 'test';
        addE.bin = 'test';
        addE.merchantID = 'test';
        addE.customerName = 'test';
        addE.customerRefNum = 'test';
        addE.customerAddress1 = 'test';
        addE.customerAddress2 = 'test';
        addE.customerCity = 'test';
        addE.customerState = 'test';
        addE.customerZIP = 'test';
        addE.customerEmail = 'test';
        addE.customerPhone = 'test';
        addE.customerCountryCode = 'test';
        addE.customerProfileOrderOverideInd = 'test';
        addE.customerProfileFromOrderInd = 'test';
        addE.orderDefaultDescription = 'test';
        addE.orderDefaultAmount = 'test';
        addE.customerAccountType = 'test';
        addE.ccAccountNum = 'test';
        addE.encryptedPan = 'test';
        addE.encryptedPanMethod = 'test';
        addE.encryptedPanKey = 'test';
        addE.encryptedPanPublicKeyFingerPrint = 'test';
        addE.encryptedPanHash = 'test';
        addE.ccExp = 'test';
        addE.ecpCheckDDA = 'test';
        addE.ecpBankAcctType = 'test';
        addE.ecpCheckRT = 'test';
        addE.ecpDelvMethod = 'test';
        addE.switchSoloCardStartDate = 'test';
        addE.switchSoloIssueNum = 'test';
        addE.mbType = 'test';
        addE.mbOrderIdGenerationMethod = 'test';
        addE.mbRecurringStartDate = 'test';
        addE.mbRecurringEndDate = 'test';
        addE.mbRecurringNoEndDateFlag = 'test';
        addE.mbRecurringMaxBillings = 'test';
        addE.mbRecurringFrequency = 'test';
        addE.mbMicroPaymentMaxDollarValue = 'test';
        addE.mbMicroPaymentMaxBillingDays = 'test';
        addE.mbMicroPaymentMaxTransactions = 'test';
        addE.mbDeferredBillDate = 'test';
        addE.mcSecureCodeAAV = 'test';
        addE.softDescMercName = 'test';
        addE.softDescProdDesc = 'test';
        addE.softDescMercCity = 'test';
        addE.softDescMercPhone = 'test';
        addE.softDescMercURL = 'test';
        addE.softDescMercEmail = 'test';
        addE.euddBankSortCode = 'test';
        addE.euddCountryCode = 'test';
        addE.euddRibCode = 'test';
        addE.status = 'test';
        addE.debitBillerReferenceNumber = 'test';
        addE.accountUpdaterEligibility = 'test';
        
        
	}
	static testmethod void testResponse() {
		CommerceService serv = new PaymentechMockGateway();
		CreditCardDetails details = new CreditCardDetails('Mr Mocked', 08, 2014, '94517', 'Ca', '123');
		CommerceProfileResponse resp = serv.createCustomerProfile(details);
		System.assert(resp.isSuccesful());
		System.assertEquals(resp.getExtVaultId(), 'MOCKED');
		
		resp = serv.updateCustomerProfile('', details);
		System.assert(resp.isSuccesful());
		
		resp = serv.fetchCustomerProfile('');
		System.assert(resp.isSuccesful());
		
		System.assert(resp.getCreditCardDetails() != null);
		
		CommerceTransactionResponse cResp = serv.makeSettlement(details, '', '', '');
		System.assert(cResp == null);	
	}
	
	static testmethod void testUser() {
		PaymentechGateway_Info__c PG = PaymentechGateway_Info__c.getOrgDefaults();
		String user = PG.Username__c;
		System.assertEquals(user, PaymentechConfig.getConfig().getUsername()); 
		System.debug(user);
		System.debug(PaymentechConfig.getConfig().getUsername());
		
		String pass = EncodingUtil.base64Decode(PG.Password__c).toString();
		System.assertEquals(pass, PaymentechConfig.getConfig().getPassword());
		System.debug(pass);
		System.debug(PaymentechConfig.getConfig().getPassword()); 
		
		String merchantId = PG.MerchantId__c;
		System.assertEquals(merchantId, PaymentechConfig.getConfig().getMerchantId()); 
		System.debug(merchantId);
		System.debug(PaymentechConfig.getConfig().getMerchantId());
		
		String bin = PG.Bin__c;
		System.assertEquals(bin, PaymentechConfig.getConfig().getBin()); 
		System.debug(bin);
		System.debug(PaymentechConfig.getConfig().getBin());
		
		String endPoint = PG.Endpoint__c;
		System.assertEquals(endPoint, PaymentechConfig.getConfig().getEndPoint()); 
		System.debug(endPoint);
		System.debug(PaymentechConfig.getConfig().getEndPoint());
		
		String urn = PG.URN__c;
		System.assertEquals(urn, PaymentechConfig.getConfig().getUrn()); 
		System.debug(urn);
		System.debug(PaymentechConfig.getConfig().getUrn());
		
		String wsdl = PG.WSDL__c;
		System.assertEquals(wsdl, PaymentechConfig.getConfig().getWsdl()); 
		System.debug(wsdl);
		System.debug(PaymentechConfig.getConfig().getWsdl());
		
	}
	static testmethod void testResponse2() {
		CommerceService serv = new PaymentechMockGateway();
		CreditCardDetails details = new CreditCardDetails('Mr Mocked', 08, 2014, '94517', 'Ca', '123');
		CommerceProfileResponse resp = serv.createCustomerProfile(details);
		System.assert(resp.isSuccesful());
		System.assertEquals(resp.getExtVaultId(), 'MOCKED');
		
		resp = serv.updateCustomerProfile('', details);
		System.assert(resp.isSuccesful());
		
		resp = serv.fetchCustomerProfile('');
		System.assert(resp.isSuccesful());
		
		System.assert(resp.getCreditCardDetails() != null);
		
		CommerceTransactionResponse cResp = serv.makeSettlement(details, '', '', '');
		System.assert(cResp == null);	
	}
	static testmethod void testProfiles2() {
    	PaymentechNetGateway gate = new PaymentechNetGateway();
    	CommerceProfileResponse resp = gate.fetchCustomerProfile('1234567');
    	CreditCardDetails details = new CreditCardDetails('Dan', 08, 2014, '94517', 'Ca', '123');
		details.setCreditCardNumber('5111005111051128');
    	resp = gate.createCustomerProfile(details);
    	resp = gate.updateCustomerProfile('1234567', details);
    	CommerceTransactionResponse settResp = gate.makeSettlement(details, '20000', 'ABCDEFG', '1234567');
    	
        PaymentechNetGateway.PaymentechGatewayWebService ws = new PaymentechNetGateway.PaymentechGatewayWebService(PaymentechConfig.getConfig());
        PaymentechGateway.ProfileChangeElement change = new PaymentechGateway.ProfileChangeElement();
        PaymentechGateway.ProfileResponseElement output = ws.ProfileChange(change); 
    
        System.assertEquals(output.profileAction, 'Update');
        
        PaymentechGateway.ProfileDeleteElement profileDeleteRequest = new PaymentechGateway.ProfileDeleteElement();
        output = ws.ProfileDelete(profileDeleteRequest);
        System.assertEquals(output.profileAction, 'Delete');
        
        PaymentechGateway.ProfileFetchElement profileFetchRequest = new PaymentechGateway.ProfileFetchElement();
        output = ws.ProfileFetch(profileFetchRequest);
        System.assertEquals(output.profileAction, 'Fetch'); 
        
        ws = new PaymentechNetGateway.PaymentechGatewayWebService(PaymentechConfig.getConfig());
        PaymentechGateway.NewOrderRequestElement newOrderRequest = new PaymentechGateway.NewOrderRequestElement();
        PaymentechGateway.NewOrderResponseElement outputEle = ws.NewOrder(newOrderRequest); 
    
        System.assertEquals(outputEle.customerName, 'Pandora');
        
        ws = new PaymentechNetGateway.PaymentechGatewayWebService(PaymentechConfig.getConfig());
        PaymentechGateway.ProfileAddElement profileAddRequest = new PaymentechGateway.ProfileAddElement();
        output = ws.ProfileAdd(profileAddRequest); 
    
        System.assertEquals(output.customerName, 'Pandora');
    }
    static testmethod void testResponse4() {
		String errorMessage = 'Error Message';
		PaymentechProfileResponse errorResp = new PaymentechProfileResponse(errorMessage, false);
		System.assert(!errorResp.isSuccesful());
		System.assertEquals(errorMessage, errorResp.getErrorMessage());
		
		PaymentechProfileResponse simpleGoodResp = new PaymentechProfileResponse(true);
		System.assert(simpleGoodResp.isSuccesful());
		
		String creditCardNumber = '4552534341130849';
		CreditCardDetails det = new CreditCardDetails('Dan', 08, 2014, '94517', 'Ca', '123');
		det.setCreditCardNumber(creditCardNumber);
		PaymentechProfileResponse profileGoodResp = new PaymentechProfileResponse(det, true);
		System.assert(profileGoodResp.isSuccesful());
		System.assertEquals(det, profileGoodResp.getCreditCardDetails());
		
		String vaultId = '12345';
		PaymentechProfileResponse profileFetchGoodResp = new PaymentechProfileResponse(true);
		profileFetchGoodResp.setExtVaultId(vaultId);
		System.assert(profileFetchGoodResp.isSuccesful());
		System.assertEquals(profileFetchGoodResp.getExtVaultId(), vaultId);
		
		profileFetchGoodResp.setIsSuccesful(false);
		profileFetchGoodResp.setCreditCardDetails(det);
		profileFetchGoodResp.setErrorMessage(errorMessage);
		
		System.assert(!profileFetchGoodResp.isSuccesful());
		System.assertEquals(profileFetchGoodResp.getCreditCardDetails(), det);
		System.assertEquals(profileFetchGoodResp.getErrorMessage(), errorMessage);
		
	}
	static testmethod void testTransactionReponse3() {
		String errorMessage = 'Error Calling Out';
		CommerceTransactionResponse errorResponse = PaymentechTransactionResponse.getFailedResponse(errorMessage);
		
		System.assertEquals('-1: ' + errorMessage, errorResponse.getErrorMessage());
		System.assert(!errorResponse.getApproved());
		
		PaymentechGateway.NewOrderResponseElement orderResponse = new PaymentechGateway.NewOrderResponseElement();
		orderResponse.procStatus = '0';
		orderResponse.approvalStatus = '1';
		orderResponse.respCodeMessage = 'SuccesfulOrder';
		orderResponse.respCode = '20';
		orderResponse.avsRespCode = '30';
		orderResponse.cvvRespCode = '123';
		orderResponse.txRefNum = 'ABCDEF12345';
		orderResponse.orderID = '12345';
		CommerceTransactionResponse succResponse = PaymentechTransactionResponse.convertResponse(orderResponse);
		System.assert(succResponse.getApproved());
		System.assertEquals(succResponse.getTxRefNumber(), 'ABCDEF12345');
		System.assertEquals(succResponse.getAddressVerification(), '30');
		System.assertEquals(succResponse.getApprovalCode(), '20');
		System.assertEquals(succResponse.getOrderNumber(), '12345');
		
		orderResponse.procStatus = '2';
		succResponse = PaymentechTransactionResponse.convertResponse(orderResponse);
		System.assert(!succResponse.getApproved());
		
		succResponse = PaymentechTransactionResponse.convertResponse(null);
		System.assert(!succResponse.getApproved());
	}
	static testmethod void testCommerceInterface2()
	{
		CommerceAddress add = new CommerceAddress('Daniel', '17345 Bishopsgate Dr', '', 'Pflugerville', 'Tx', '78660');
		CommerceTaxService serv = CommerceServiceFactory.getCommerceTaxService();
		CommerceTaxResponse resp = serv.getSalesTaxAmount(10000, add, false, '1000');
		if(resp.isSuccesful())
			System.debug(resp.getTax());
			
		serv = new SabrixTaxService();
		resp = serv.getSalesTaxAmount(10000, add, false, '1000');
		if(resp.isSuccesful())
		{
			resp.setIsSuccesful(true);
			resp.setErrorMessage('none');
			add.getName();
			add.getState();
			System.debug(resp.getTax());
		}else{
			System.debug(resp.getErrorMessage());
			System.assert(resp.isSuccesful());
		}
		add = new CommerceAddress();
		add.setName('daniel');
		add.setAddress1('17345 Bishopsgate Dr');
		add.setAddress2('');
		add.setCity('Pflugerville');
		add.setState('Tx');
		add.setZipcode('');
		resp = serv.getSalesTaxAmount(10000, add, false, '1000');
		if(resp.isSuccesful())
		{
			resp.setIsSuccesful(true);
			resp.setErrorMessage('none');
			add.getName();
			add.getState();
			System.debug(resp.getTax());
		}else{
			System.assert(!resp.isSuccesful());
			System.debug(resp.getErrorMessage());
		}
		
	}
	static testmethod void testDetails()
	{
		CreditCardDetails det = new CreditCardDetails('Dan', 08, 2012, '78660', 'Ca', '123');
		det = new CreditCardDetails('Dan', 08, 2012, '78660', '1001', 'humm', 'Clay', 'Ca', '123');
		det = new CreditCardDetails('Dan', 08, 2012, '78660', '1001', 'humm', 'Clay', 'Ca', '123', 'US');
		det.setCreditCardNumber('111');
		det.setCity('clay');
		det.getCreditCardNumber();
		det.getCountryCode();
		det.getState();
		det.getCity();
		det.getAddress1();
		det.getAddress2();
		det.getSecurityCode();
		det.getZipCode();
		det.getExpirationYear();
		det.getExpirationMonth();
		det.getCustomerName();
		det.setSecurityCode('112');
		det.cleanZipCode('78660');
		det.cleanZipCode('');
		det.cleanZipCode('7866');
		det.getConcatenatedExpiryDate();
	}
	static testmethod void testTaxElements()
	{
		SabrixTaxService.ChargeDetailCollection col = new SabrixTaxService.ChargeDetailCollection();
		col.charges = null;
		SabrixTaxService.ChargeDetail det2= new SabrixTaxService.ChargeDetail();
		det2.ChargeType = 'df';
		det2.ChargeTaxAmount = 1.1;
		det2.ChargeTaxRate = 1.2;
		SabrixTaxService.TaxSummary sum = new SabrixTaxService.TaxSummary();
		sum.EffectiveTaxRate = 1.1;
		sum.CalculatedTaxAmount = 1.1;
		sum.AccrualTaxAmount = 1.1;
		sum.PartnerTaxAmount = 1.1;
		sum.TaxableBasis = 1.1;
		sum.ExemptAmount = 1.1;
		
		
		
		SabrixTaxService.DocumentCollection 		a1=new SabrixTaxService.DocumentCollection();
		SabrixTaxService.TaxLineCollection 		a2=new SabrixTaxService.TaxLineCollection();
		SabrixTaxService.Registration 			a3=new SabrixTaxService.Registration();
		SabrixTaxService.ReferenceDocument 		a4=new SabrixTaxService.ReferenceDocument(); 
		SabrixTaxService.ZoneTaxCollection 		a5=new SabrixTaxService.ZoneTaxCollection();
		SabrixTaxService.Registrations 			a6=new SabrixTaxService.Registrations ();
		SabrixTaxService.UserAttributes_element 	a7=new SabrixTaxService.UserAttributes_element ();
		SabrixTaxService.TaxRequest_element 		a8=new SabrixTaxService.TaxRequest_element();
		SabrixTaxService.Dates_element 			a9=new SabrixTaxService.Dates_element ();
		SabrixTaxService.Document 				a11=new SabrixTaxService.Document ();
		SabrixTaxService.Quantity 				a12=new SabrixTaxService.Quantity ();
		SabrixTaxService.ChargeDetailCollection 	a13=new SabrixTaxService.ChargeDetailCollection(); 
		SabrixTaxService.TaxDetail 				a14=new SabrixTaxService.TaxDetail();
		SabrixTaxService.SellerRegistrations 		a15=new SabrixTaxService.SellerRegistrations();
		SabrixTaxService.Amounts_element 			a16=new SabrixTaxService.Amounts_element ();
		SabrixTaxService.TaxabilityInfo_element 	a17=new SabrixTaxService.TaxabilityInfo_element();
		SabrixTaxService.Charge 				a18=new SabrixTaxService.Charge ();
		SabrixTaxService.TaxSummary 				a19=new SabrixTaxService.TaxSummary(); 
		SabrixTaxService.TaxDocument 			a21=new SabrixTaxService.TaxDocument();
		SabrixTaxService.TaxDocumentCollection 	a22=new SabrixTaxService.TaxDocumentCollection();

		
		SabrixTaxService.AddressCollection 		a25=new SabrixTaxService.AddressCollection();
		SabrixTaxService.ChargeCollection 		a26=new SabrixTaxService.ChargeCollection();
		SabrixTaxService.Status					a27=new SabrixTaxService.Status();
		SabrixTaxService.TaxDetailCollection 		a28=new SabrixTaxService.TaxDetailCollection();
		SabrixTaxService.HostRequestInfo 			a29=new SabrixTaxService.HostRequestInfo();
		SabrixTaxService.TaxResponse_element 		a31=new SabrixTaxService.TaxResponse_element(); 
		SabrixTaxService.Attributes_element 		a32=new SabrixTaxService.Attributes_element ();
		SabrixTaxService.ZoneTaxSummary 			a33=new SabrixTaxService.ZoneTaxSummary();
		SabrixTaxService.LineCollection 			a34=new SabrixTaxService.LineCollection();
		SabrixTaxService.Address 				a35=new SabrixTaxService.Address ();
		SabrixTaxService.TaxLine 				a36=new SabrixTaxService.TaxLine ();
		SabrixTaxService.UserAttributeContainer 	a37=new SabrixTaxService.UserAttributeContainer();
		SabrixTaxService.Line 					a38=new SabrixTaxService.Line();
		SabrixTaxService.BuyerRegistrations 		a39=new SabrixTaxService.BuyerRegistrations ();
		SabrixTaxService.UseTaxInformation_element 	a40=new SabrixTaxService.UseTaxInformation_element();
	}
}