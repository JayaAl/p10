public with sharing class PaymentechMockGateway implements CommerceService{
	/**
	 * Charges a credit card with the amount specified
	 * @param details details of the credit card
	 * @param amountInCents amount, in pennies, to be charged
	 *   request sent to the commerce system.
	 * @param invoiceNumber - an optionally provided invoice number
	 * @param customerReferenceNumber id of credit card to be charged
	 * @return
	 */
	public CommerceTransactionResponse makeSettlement(CreditCardDetails details, String amountInCents, String invoiceNumber, String customerReferenceNumber)
	{
		return null;
	}

	/**
     * Create an external entry of the credit card data within a third party vault of the payment processor.
     * @param creditCardNumber     
     * @param details
     * @return the external vault id of the card(customerReferenceNumber)
     */
    public CommerceProfileResponse createCustomerProfile(CreditCardDetails details)
    {
    	PaymentechProfileResponse response = new PaymentechProfileResponse(true);
    	response.setExtVaultId('MOCKED');
    	return response;
    }

    /**
     * Update an external credit card profile with new credit card details for the given credit card
     * @param details
     * @return the external vault id of the card
     */
    public CommerceProfileResponse updateCustomerProfile(String customerReferenceNumber, CreditCardDetails details)
    {
    	PaymentechProfileResponse response = new PaymentechProfileResponse(true);
    	return response;
    }
    
    public CommerceProfileResponse fetchCustomerProfile(String customerReferenceNumber)
    {
    	PaymentechProfileResponse response = new PaymentechProfileResponse(true);
    	CreditCardDetails details = new CreditCardDetails('Mr Mocked', 08, 2014, '94517', 'Ca', '123');
    	response.setCreditCardDetails(details);
    	return response;
    }

}