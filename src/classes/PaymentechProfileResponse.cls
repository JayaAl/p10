public with sharing class PaymentechProfileResponse extends CommerceProfileResponse{
	public PaymentechProfileResponse(String errorMessage, boolean succesful)
	{
		super(errorMessage, succesful);
	}
	
	public PaymentechProfileResponse(boolean succesful)
	{
		super(succesful);
	}
	
	public PaymentechProfileResponse(CreditCardDetails creditCardDetails, boolean succesful)
	{
		super(creditCardDetails, succesful);
	}
}