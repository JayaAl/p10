/**
* Copyright 2014 Equifax.  All rights reserved.
*/

global class PendingApprovalAmountBatchJob implements Database.Batchable<SObject>, Schedulable, Database.Stateful { 

    // Global
    
    global PendingApprovalAmountBatchJob() {}
    
    global void execute(SchedulableContext sc) {
        Database.executeBatch(this, 100);
    }
    
    global Database.QueryLocator start(Database.BatchableContext bc) {
        Database.QueryLocator ql;
        try {
            ql = Database.getQueryLocator(getQuery());
        }
        catch (Exception e) {
            System.debug('Exception in PendingApprovalAmountBatchJob.start() : '+ e);
            ForsevaUtilities.decodeExceptionAndSendEmail(e, 'Exception in PendingApprovalAmountBatchJob.start()', '', 'Forseva Administrators');
            
            throw e;
        }
        return ql;
    }
    
    global void execute(Database.BatchableContext bc, List<SObject> scope) {
        batchNum++;
        //log += '\nbatchNum = ' + batchNum + ', m_currentAcc = ' + m_currentAcc + '\n';
        calculatePendingApprovalAmount(scope);
    }
    
    global void finish(Database.BatchableContext bc) {
        try {
            if (m_currentAcc != null) {
                update m_currentAcc;
            }
        }
        catch (Exception e) {
            System.debug(' Exception in PendingApprovalAmountBatchJob.finish() :'+e);
            ForsevaUtilities.decodeExceptionAndSendEmail(e, 'Exception in PendingApprovalAmountBatchJob.finish()', 'Account Id = ' + m_currentAcc.Id, 'Forseva Administrators');
        }
        AsyncApexJob a = [select Id, Status, NumberOfErrors, JobItemsProcessed, TotalJobItems, CreatedBy.Email
                          from  AsyncApexJob 
                          where Id = :bc.getJobId()];
        String subjectText = 'PendingApprovalAmountBatchJob completed';
        String totalJobItems = '' + a.TotalJobItems;
        String numberOfErrors = '' + a.NumberOfErrors;
        String msgText = 'The batch Apex job processed ' + totalJobItems + ' batches with ' + numberOfErrors + ' failures';
        String[] toAddresses = new String[] {a.CreatedBy.Email};
        //ForsevaUtilities.sendEmailNotification('PendingApprovalAmountBatchJob LOG', log, toAddresses);
        ForsevaUtilities.sendEmailNotification(subjectText, msgText, toAddresses);
        //Database.executeBatch(new PendingApprovalAmountResetBatchJob(), 200); 
    }
    
    // Public

    public Account m_currentAcc = null;
    
    public String getQuery() {
        Datetime startOfMonth = Date.today().toStartOfMonth(); // This is actually the last day of the previous month...
        String firstDayOfMonth = startOfMonth.format('yyyy-MM-dd');
        Date nextMonth = Date.today().addMonths(1);
        Integer daysInMonth = Date.daysInMonth(nextMonth.year(), nextMonth.month());
        Datetime endOfNextMonth = Date.newInstance(nextMonth.year(), nextMonth.month(), daysInMonth + 1);
        String lastDayOfNextMonth = endOfNextMonth.format('yyyy-MM-dd');
        String query = 'select   Id, AccountId, Probability, Amount, Billed_To_Date__c, ContractStartDate__c, ContractEndDate__c, Account.Credit_Approved_Amount__c ' +
                       'from     Opportunity ' +
                       'where    Probability >= 90 and Probability <= 100 ' + 
                       'and      ContractStartDate__c <= ' + lastDayOfNextMonth + ' ' +
                       'and      ContractEndDate__c > ' + firstDayOfMonth + ' ' +
                       'order by AccountId';
        if (System.Test.isRunningTest()) {
            query += ' limit 1';
        }
        return query;
    }
    // Private
    
    private Integer batchNum = 0;
    private String log = 'Trace: \n';
    
    @TestVisible
    private void calculatePendingApprovalAmount(List<SObject> scope) {
        Map<Id, Account> accMap = new Map<Id, Account>();
        Map<Id, Id> accIds = new Map<Id, Id>();
        Double amountNotBilled, availableCredit, averageAmountPerMonth, billedToDate, pendingApproval, openBalance;
        Integer monthsRemaining;
        for (SObject sobj : scope) {
            accIds.put((Id)sobj.get('AccountId'), (Id)sobj.get('AccountId'));
        }
         
/*        List<AggregateResult> salesInvoices = [select   Advertiser__c, sum(c2g__OutstandingValue__c)
                                               from     c2g__codaInvoice__c 
                                               where    Advertiser__c in :accIds.values() 
                                               and      c2g__OutstandingValue__c != 0
                                               group by Advertiser__c];
        
        List<AggregateResult> creditNotes = [select   c2g__Account__c, sum(c2g__OutstandingValue__c)
                                             from     c2g__codaCreditNote__c 
                                             where    c2g__Account__c in :accIds.values() 
                                             and      c2g__OutstandingValue__c != 0
                                             group by c2g__Account__c];

        List<AggregateResult> cjEntries = [select   c2g__Account__c, sum(c2g__DocumentOutstandingTotal__c)
                                           from     c2g__codaTransaction__c 
                                           where    c2g__Account__c in :accIds.values()  
                                           and      c2g__TransactionType__c in ('Journal', 'Cash') 
                                           and      c2g__DocumentOutstandingTotal__c != 0 
                                           group by c2g__Account__c];
*/
        
/*
        Map<Id, Double> totalARMap = new Map<Id, Double>(); 
        List<forseva1__ARSummary__c> arsList = [Select forseva1__Account__r.id, forseva1__Total_AR_Balance__c, forseva1__Total_AR_Current__c, forseva1__Total_AR_Past_Due__c 
                            from forseva1__ARSummary__c 
                            where forseva1__Account__r.id in :accIds.values()];  

        for(forseva1__ARSummary__c ars : arsList){
            totalARMap.put(ars.forseva1__Account__r.id, ars.forseva1__Total_AR_Balance__c);
        }                                           

*/

        List<AggregateResult> openBalances = [select forseva1__account__c, sum(forseva1__Amount_Owed__c)  
                                              from forseva1__FInvoice__c 
                                              where forseva1__Payment_Status__c = 'Open' 
                                              and forseva1__account__c in :accIds.values() 
                                              and forseva1__Amount_Owed__c != 0
                                              and advertiser__c != null
                                              group by forseva1__account__c ];
        
        Account previousAcc = null;
        Opportunity opp;
        if (m_currentAcc != null) {
            previousAcc = m_currentAcc;
        }
        for (SObject sobj : scope) {
            try {
                opp = (Opportunity)sobj;
                //log += 'oppId = ' + opp.Id + ', opp.AccountId = ' + opp.AccountId + ' and pp.Account.Credit_Approved_Amount__c = ' + opp.Account.Credit_Approved_Amount__c + '\n';
                opp.Amount = opp.Amount != null ? opp.Amount : 0;
                billedToDate = opp.Billed_To_Date__c != null ? opp.Billed_To_Date__c : 0;
                if (billedToDate < opp.Amount) {
                    amountNotBilled = opp.Amount - billedToDate;
                    //log += '   opp.amount = ' + opp.amount + ', billedToDate = ' + billedToDate + ' and amountNotBilled = ' + amountNotBilled + '\n';
                    if (opp.ContractEndDate__c != null) {
                        monthsRemaining = (opp.ContractEndDate__c.year() - System.today().year())*12 + opp.ContractEndDate__c.month() - System.today().month() + 1;
                    }
                    else {
                        monthsRemaining = 1;
                    }
    
                    log += '   monthsRemaining = ' + monthsRemaining + '\n';
                    if (previousAcc != null && previousAcc.Id != opp.AccountId) {
                        accMap.put(previousAcc.Id, previousAcc);
                        m_currentAcc = null;
                    }
                    averageAmountPerMonth = amountNotBilled / monthsRemaining;
                    pendingApproval = monthsRemaining > 2 ? averageAmountPerMonth * 2 : averageAmountPerMonth * monthsRemaining;
                    if (m_currentAcc == null) {
//                        openBalance = getOpenBalanceForAccount(opp.AccountId, salesInvoices, creditNotes, cjEntries);
//                        openBalance = totalARMap.get(opp.AccountId) != null ? (Double)totalARMap.get(opp.AccountId) : 0;
                        openBalance = getOpenBalanceForAccount(opp.AccountId, openBalances);
                        availableCredit = opp.Account.Credit_Approved_Amount__c != null ? opp.Account.Credit_Approved_Amount__c - openBalance - pendingApproval : 0 - openBalance - pendingApproval;
                        //log += 'm_currentAcc.pendingApproval = ' + pendingApproval + ' and Available Credit = ' + availableCredit + '\n';
                        m_currentAcc = new Account(Id = opp.AccountId, Open_Balance__c = openBalance, Pending_Approval__c = pendingApproval, Available_Credit__c = availableCredit, Pending_Approval_Date__c = System.today());
                    }
                    else {
                        m_currentAcc.Pending_Approval__c += pendingApproval;
                        m_currentAcc.Available_Credit__c = opp.Account.Credit_Approved_Amount__c != null ? opp.Account.Credit_Approved_Amount__c - m_currentAcc.Open_Balance__c - m_currentAcc.Pending_Approval__c : 0 - m_currentAcc.Open_Balance__c - m_currentAcc.Pending_Approval__c;
                        //log += 'm_currentAcc.Pending_Approval__c = ' + m_currentAcc.Pending_Approval__c + ' and m_currentAcc.Available_Credit__c = ' + m_currentAcc.Available_Credit__c + '\n';
                    }
                    previousAcc = m_currentAcc;
                }
                else {
                    if (previousAcc != null && previousAcc.Id != opp.AccountId) {
                        accMap.put(previousAcc.Id, previousAcc);
                        m_currentAcc = null;
                    }
                }
            }
            catch (Exception e) {
                System.debug('Error in PendingApprovalAmountBatchJob : Error processing opportunities for account ' + opp.AccountId + ' and Opp.id = ' + opp.Id+ e );
                ForsevaUtilities.decodeExceptionAndSendEmail(e, 'Error in PendingApprovalAmountBatchJob', 'Error processing opportunities for account ' + opp.AccountId + ' and Opp.id = ' + opp.Id, 'Forseva Administrators');
            }
        }
        //log += 'Before leaving calculatePendingApprovalAmount(): m_currentAcc = ' + m_currentAcc + '\n';
        //log += 'Before leaving calculatePendingApprovalAmount(): accMap.values().size() = ' + accMap.values().size() + '\n\n';
        try {
            update accMap.values();
        }
        catch (Exception e) {
            System.debug('Error in PendingApprovalAmountBatchJob Error updating accounts '+e);
            ForsevaUtilities.decodeExceptionAndSendEmail(e, 'Error in PendingApprovalAmountBatchJob', 'Error updating accounts\n', 'Forseva Administrators');
        }
    }
    
    private Double getOpenBalanceForAccount(Id acctId, List<AggregateResult> openBalances){
        Double openBal = 0;
        for(AggregateResult ar: openBalances){
            if(acctId == (Id)ar.get('forseva1__account__c')){
                openBal = (Double)ar.get('expr0');
                break;
            }
        }
        
        System.debug(' Account ID = '+acctId + ' Open Balance = '+openBal);
        return openBal;
    }
    
/*    private Double getOpenBalanceForAccount(Id accId, List<AggregateResult> salesInvoices, List<AggregateResult> creditNotes, List<AggregateResult> cjEntries) {
        Double openBalance = 0, salesInvOpenBal = 0, creditNotesOpenBal = 0, cjEntriesOpenBal = 0;
        for (AggregateResult ar : salesInvoices) {
            if (accId == (Id)ar.get('Advertiser__c')) {
                openBalance = (Double)ar.get('expr0');
                break;
            }
        } 
        for (AggregateResult ar : creditNotes) {
            if (accId == (Id)ar.get('c2g__Account__c')) {
                openBalance += (Double)ar.get('expr0');
                break;
            }
        } 
        for (AggregateResult ar : cjEntries) {
            if (accId == (Id)ar.get('c2g__Account__c')) {
                openBalance += (Double)ar.get('expr0');
                break;
            }
        } 
        return openBalance;
    }
*/    
}