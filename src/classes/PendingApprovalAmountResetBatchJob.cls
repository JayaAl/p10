/**
* Copyright 2014 EquiFAX.  All rights reserved.
*/

global class PendingApprovalAmountResetBatchJob implements Database.Batchable<SObject>, Schedulable, Database.Stateful {

    // Global
    
    global PendingApprovalAmountResetBatchJob() {}
    
    global void execute(SchedulableContext sc) {
        Database.executeBatch(this, 200);
    }
    
    global Database.QueryLocator start(Database.BatchableContext bc) {
        Database.QueryLocator ql;
        try {
            ql = Database.getQueryLocator(getQuery());
        }
        catch (Exception e) {
            ForsevaUtilities.decodeExceptionAndSendEmail(e, 'Exception in PendingApprovalAmountResetBatchJob.start()', '', 'Forseva Administrators');
            throw e;
        }
        return ql;
    }
    
    global void execute(Database.BatchableContext bc, List<SObject> scope) {
        batchNum++;
        calculatePendingApprovalAmount(scope);
    }
    
    global void finish(Database.BatchableContext bc) {
        AsyncApexJob a = [select Id, Status, NumberOfErrors, JobItemsProcessed, TotalJobItems, CreatedBy.Email
                          from   AsyncApexJob 
                          where  Id = :bc.getJobId()];
        String subjectText = 'PendingApprovalAmountResetBatchJob completed';
        String totalJobItems = '' + a.TotalJobItems;
        String numberOfErrors = '' + a.NumberOfErrors;
        String msgText = 'The batch Apex job processed ' + totalJobItems + ' batches with ' + numberOfErrors + ' failures';
        String[] toAddresses = new String[] {a.CreatedBy.Email};
        ForsevaUtilities.sendEmailNotification(subjectText, msgText, toAddresses);     
    }
    
    // Public

    public String getQuery() {
        String today = System.now().format('yyyy-MM-dd');
        String query = 'select   Id, Credit_Approved_Amount__c, Open_Balance__c, Pending_Approval__c, Available_Credit__c, Pending_Approval_Date__c ' +
                       'from     Account ' +
                       'where    Pending_Approval_Date__c != today'; 
        if (Test.isRunningTest()) {
            query += ' limit 1';
        }
        return query;
    }
    
    // Private
  
    private Integer batchNum = 0;
    private String log = 'rp: \n';
    
    @TestVisible
    private void calculatePendingApprovalAmount(List<SObject> scope) {
        List<Id> accIds = new List<Id>();
        Double availableCredit, openBalance;
        for (SObject sobj : scope) {
            accIds.add((Id)sobj.get('Id'));
        }         
/*        List<AggregateResult> salesInvoices = [select   Advertiser__c, sum(c2g__OutstandingValue__c)
                                               from     c2g__codaInvoice__c 
                                               where    Advertiser__c in :accIds 
                                               and      c2g__OutstandingValue__c != 0
                                               group by Advertiser__c];
        
        List<AggregateResult> creditNotes = [select   c2g__Account__c, sum(c2g__OutstandingValue__c)
                                             from     c2g__codaCreditNote__c 
                                             where    c2g__Account__c in :accIds
                                             and      c2g__OutstandingValue__c != 0
                                             group by c2g__Account__c];

        List<AggregateResult> cjEntries = [select   c2g__Account__c, sum(c2g__DocumentOutstandingTotal__c)
                                           from     c2g__codaTransaction__c 
                                           where    c2g__Account__c in :accIds  
                                           and      c2g__TransactionType__c in ('Journal', 'Cash') 
                                           and      c2g__DocumentOutstandingTotal__c != 0 
                                           group by c2g__Account__c];
*/

/*        
        Map<Id, Double> totalARMap = new Map<Id, Double>(); 
        List<forseva1__ARSummary__c> arsList = [Select forseva1__Account__r.id, forseva1__Total_AR_Balance__c 
                            from forseva1__ARSummary__c 
                            where forseva1__Account__r.id in :accIds];   

        for(forseva1__ARSummary__c ars : arsList){
            totalARMap.put(ars.forseva1__Account__r.id, ars.forseva1__Total_AR_Balance__c);
        }

*/
        
        List<AggregateResult> openBalances = [select forseva1__account__c, sum(forseva1__Amount_Owed__c) 
                                              from forseva1__FInvoice__c 
                                              where forseva1__Payment_Status__c = 'Open' 
                                              and forseva1__account__c in :accIds  
                                              and forseva1__Amount_Owed__c != 0
                                              and advertiser__c != null
                                              group by forseva1__account__c ];        

        
        Account acc;
        List<Account> accs = new List<Account>();
        for (SObject sobj : scope) {
            try {
                acc = (Account)sobj;
//                openBalance = getOpenBalanceForAccount(acc.Id, salesInvoices, creditNotes, cjEntries);
//                openBalance = totalARMap.get(acc.Id) != null ? (Double)totalARMap.get(acc.Id) : 0;    
				openBalance = getOpenBalanceForAccount(acc.Id, openBalances);                
                availableCredit = acc.Credit_Approved_Amount__c != null ? acc.Credit_Approved_Amount__c - openBalance : 0 - openBalance;
                log += 'Id = ' + acc.Id + ', Available Credit = ' + availableCredit + '\n';
                acc.Open_Balance__c = openBalance;
                acc.Pending_Approval__c = 0.0;
                acc.Available_Credit__c = availableCredit;
                acc.Pending_Approval_Date__c = System.today();
                accs.add(acc);
            }
            catch (Exception e) {
                ForsevaUtilities.decodeExceptionAndSendEmail(e, 'Error in PendingApprovalAmountResetBatchJob', 'Error processing opportunities for account ' + acc.Id, 'Forseva Administrators');
            }

        }
        try {
            log += 'Before leaving calculatePendingApprovalAmount(): accs.size() = ' + accs.size() + '\n\n';
            update accs;
        }
        catch (Exception e) {
            ForsevaUtilities.decodeExceptionAndSendEmail(e, 'Error in PendingApprovalAmountResetBatchJob', 'Error updating accounts\n', 'Forseva Administrators');
        }
    }

    private Double getOpenBalanceForAccount(Id acctId, List<AggregateResult> openBalances){
        Double openBal = 0;
        for(AggregateResult ar: openBalances){
            if(acctId == (Id)ar.get('forseva1_Account__c')){
                openBal = (Double)ar.get('expr0');
                break;
            }
        }
        return openBal;
    }
    
/*    private Double getOpenBalanceForAccount(Id accId, List<AggregateResult> salesInvoices, List<AggregateResult> creditNotes, List<AggregateResult> cjEntries) {
        Double openBalance = 0, salesInvOpenBal = 0, creditNotesOpenBal = 0, cjEntriesOpenBal = 0;
        for (AggregateResult ar : salesInvoices) {
            if (accId == (Id)ar.get('Advertiser__c')) {
                openBalance = (Double)ar.get('expr0');
                break;
            }
        } 
        for (AggregateResult ar : creditNotes) {
            if (accId == (Id)ar.get('c2g__Account__c')) {
                openBalance += (Double)ar.get('expr0');
                break;
            }
        } 
        for (AggregateResult ar : cjEntries) {
            if (accId == (Id)ar.get('c2g__Account__c')) {
                openBalance += (Double)ar.get('expr0');
                break;
            }
        } 
        return openBalance;
    }
*/    
}