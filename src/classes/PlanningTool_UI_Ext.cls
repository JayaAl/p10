public without sharing class PlanningTool_UI_Ext {
    
    public List<ForecastWrapper> lstOfFW {get;set;}//Main List
    public List<TimeperiodsWrapper> lstTPWDisplay {get;set;}
    
    public Map<Id, Set<Id>> roleSubordinateUsers { get; private set; }
    Set<Id> userIds;
    public String selectedFiscalYear {get;set;}
    public String selectedFiscalQuarter {get;set;}
    public SelectOption[] selectedUsers { get; set; }
    public SelectOption[] allUsers { get; set; }
    public String message {get;set;}
    Integer currentQuarter;
    public Double userQuotaTotal {get;set;}
    public Double userBookedTotal {get;set;}
    public Double userGapTotal {get;set;}
    public Double userUnweightedPPTotal {get;set;}
    public Boolean saveSuccess {get;set;}
    Date currentDate = System.today();
    public List<Decimal> listOfTotals {get;set;}
    public string title {set; get;}
    
/* Constructor */
    public PlanningTool_UI_Ext() {
        initialize();
        Period p = [
            SELECT Number 
            FROM Period 
            WHERE type = 'Quarter' 
            AND StartDate = THIS_FISCAL_QUARTER LIMIT 1
        ];
        currentQuarter = p.Number;
        getRoleSubordinateUsers(UserInfo.getUserId());
    }
    
    public void initialize() {
        lstOfFW = new List<ForecastWrapper>();
        lstTPWDisplay = new List<TimeperiodsWrapper>();
        saveSuccess = false;
        selectedUsers = new List<SelectOption>();
        allUsers = new List<SelectOption>();
        roleSubordinateUsers = new Map<Id, Set<Id>>();
    }
    
    public void getRoleSubordinateUsers(Id userId) {
     
        // get requested user's role
        Id roleId = UserInfo.getUserRoleId();
        // get all of the roles underneath the user
        Set<Id> allSubRoleIds = getAllSubRoleIds(new Set<ID>{roleId});
        // get all of the ids for the users in those roles
        /*Map<Id,User> users = new Map<Id, User>([Select Id, Name From User where 
          UserRoleId IN :allSubRoleIds AND IsActive = true Order By Name]);
        // return the ids as a set so you can do what you want with them*/
        allUsers.add(new SelectOption(UserInfo.getUserId(),UserInfo.getName()));
        for ( User u : [
            SELECT Id, Name 
            FROM User 
            WHERE (UserRoleId IN :allSubRoleIds)
            AND IsActive = true 
            AND Profile.UserLicense.Name = 'Salesforce' 
            AND Id != :UserInfo.getUserId() 
            AND (UserRole.Name Like '%Executive%' OR UserRole.Name Like '%AE Management%' OR UserRole.Name Like '%VP%')  
            ORDER By Name 
            LIMIT 999
        ]){
            allUsers.add(new SelectOption(u.Id, u.Name));
        }
        //return users.keySet();
        
    }
    
    private static Set<ID> getAllSubRoleIds(Set<ID> roleIds) {
        Set<ID> currentRoleIds = new Set<ID>();
        // get all of the roles underneath the passed roles
        for(UserRole userRole :[
            SELECT Id, Name 
            FROM UserRole 
            WHERE ParentRoleId IN :roleIds 
            AND ParentRoleID != null 
        ]) {
            currentRoleIds.add(userRole.Id);
        }
        
        // go fetch some more roles!
        if(currentRoleIds.size() > 0){
            currentRoleIds.addAll(getAllSubRoleIds(currentRoleIds));
        }
        return currentRoleIds;
    } 
    
    public List<SelectOption> getBookingsFiscalYears() {
        List<SelectOption> options = new List<SelectOption>();
        if(selectedFiscalYear == null){
            selectedFiscalYear = string.valueOf(currentDate.year());
        }
        options.add(new SelectOption(string.valueOf(currentDate.year()),'Fiscal Year - ' + string.valueOf(currentDate.year())));
        options.add(new SelectOption(string.valueOf(currentDate.year() + 1),'Fiscal Year - ' + string.valueOf(currentDate.year() + 1)));
        options.add(new SelectOption(string.valueOf(currentDate.year() + 2),'Fiscal Year - ' + string.valueOf(currentDate.year() + 2)));
        options.add(new SelectOption(string.valueOf(currentDate.year() + 3),'Fiscal Year - ' + string.valueOf(currentDate.year() + 3)));
        return options;
    }
    
    public List<SelectOption> getBookingsFiscalQuarters() {
        if(selectedFiscalYear == null){
            selectedFiscalYear = string.valueOf(currentDate.year());
        }
        if(selectedFiscalQuarter == null){   
            selectedFiscalQuarter = string.valueOf(currentQuarter);
        }
        List<SelectOption> options = new List<SelectOption>();
        options.add(new SelectOption('1','FQ 1'));
        options.add(new SelectOption('2','FQ 2'));
        options.add(new SelectOption('3','FQ 3'));
        options.add(new SelectOption('4','FQ 4'));
        return options;
    }
    
    
    public void viewPlanning() {
        system.debug('***selectedUsers' + selectedUsers);
        userIds = new Set<Id>();
        lstOfFW = new List<ForecastWrapper>();
        listOfTotals = new List<Decimal>();
        userQuotaTotal = 0;
        userBookedTotal = 0;
        userGapTotal = 0;
        userUnweightedPPTotal = 0;
        saveSuccess = false;
        lstTPWDisplay = new List<TimeperiodsWrapper>();
        
        for ( SelectOption so : selectedUsers ) {
            userIds.add((Id)so.getValue());
        }
        
        if(! userIds.isEmpty()){
            loadPlanningData(selectedFiscalYear, selectedFiscalQuarter);
        }
    }
    
    public void loadPlanningData(String fiscalYear, String fiscalQuarter) {    
        
        //Load Pipeline and Bookead data for selected users
        Map<Id, Double> mapOfBooked = new Map<Id, Double>();
        Map<Id, Double> mapOfPipeline = new Map<Id, Double>();
        Map<String, PlanningTool_Quarters__c> mapOfCustSettings = PlanningTool_Quarters__c.getAll();

        Date startDate = Date.newInstance(
            (integer.valueOf(mapOfCustSettings.get(fiscalQuarter).Start_Year__c) + integer.valueOf(fiscalYear)), 
            integer.valueOf(mapOfCustSettings.get(fiscalQuarter).Start_Month__c), 
            1
        );
        
        
        integer noOfDays = (integer)mapOfCustSettings.get(fiscalQuarter).Weeks__c * 7 * -1;
        startDate = startDate.addDays(noOfDays);
        
        
        Integer endYear = (integer.valueOf(mapOfCustSettings.get(fiscalQuarter).End_Year__c) + integer.valueOf(fiscalYear));
        Integer endMonth = integer.valueOf(mapOfCustSettings.get(fiscalQuarter).End_Month__c);
        
        Date endDate = Date.newInstance(
            endYear, 
            endMonth, 
            Date.daysInMonth(endYear, endYear)
        );
        for(AggregateResult ar : [
            SELECT SUM(Amount__c) amt, Opportunity_Split__r.Salesperson__c salesRep, Opportunity_Split__r.Opportunity__r.StageName stageName
            FROM Split_Detail__c 
            WHERE Opportunity_Split__r.Salesperson__c In  :userIds
            AND FISCAL_YEAR(Date__c) = :integer.valueOf(fiscalYear)
            AND FISCAL_QUARTER(Date__c) = :integer.valueOf(fiscalQuarter)
            GROUP BY Opportunity_Split__r.Salesperson__c, Opportunity_Split__r.Opportunity__r.StageName
        ]) {
            Id salesRepId = (Id)string.valueOf(ar.get('salesRep'));
            Double amt = double.ValueOf(ar.get('amt'));
            String stageName = string.valueOf(ar.get('stageName'));
            
            if(stageName == 'Closed Won' ) {
                if(! mapOfBooked.containsKey(salesRepId)) {
                    mapOfBooked.put(salesRepId, amt);
                } else {
                    mapOfBooked.put(salesRepId, (mapOfBooked.get(salesRepId) + amt));
                }
            } else if(stageName != 'Closed Won' && stageName != 'Closed Lost') {
                if(! mapOfPipeline.containsKey(salesRepId)) {
                    mapOfPipeline.put(salesRepId, amt);
                } else {
                    mapOfPipeline.put(salesRepId, (mapOfPipeline.get(salesRepId) + amt));
                }
            }
        }
        Map<Id, User> mapOfUsers = new Map<Id, User> ([
            SELECT Id, Name, IsActive, Current_Quarter_Goal__c, UserRole.Name, DefaultCurrencyIsoCode 
            FROM user 
            WHERE Id =: userIds
        ]);
        Map<Id, Map<Date, Forecast__c>> mapOfUserIdToForecast = new Map<Id, Map<Date, Forecast__c>>();
        //mapOfUserIdToForecast.put(id.valueOf('00540000001b2ZuAAI'), new Map<Date, Forecast__c>());
        Time_Periods__c[] lstTP = [
            SELECT Id, Wednesday_Date__c, Wednesday_Upsert_Key__c, Friday_Date__c
            FROM Time_Periods__c
            WHERE Wednesday_Date__c >=: startDate
            AND Wednesday_Date__c <=: endDate
            ORDER BY Wednesday_Date__c
        ];
        
        
        if(! lstTP.isEmpty()) {
            for(Id id: mapOfUsers.keySet()) {
                for(Time_Periods__c tp: lstTP) {
                    if(mapOfUserIdToForecast.containsKey(id)) {
                        if( mapOfUserIdToForecast.get(id).containsKey(tp.Wednesday_Date__c)) {
                            mapOfUserIdToForecast.get(id).put(tp.Wednesday_Date__c, new Forecast__c(User__c = id, Time_Periods__r = tp));
                        } else {
                            mapOfUserIdToForecast.get(id).put(tp.Wednesday_Date__c, new Forecast__c(User__c = id, Time_Periods__r = tp));
                        }
                    } else {
                        mapOfUserIdToForecast.put(id, new Map<Date, Forecast__c>{tp.Wednesday_Date__c => new Forecast__c(User__c = id, Time_Periods__r = tp)});
                    }
                }
            }
            Forecast__c[] result = [
                SELECT Id, Time_Periods__c, User__c, Forecast_Amount__c, Quota__c, Fiscal_Year__c, Fiscal_Quarter__c,
                Notes__c, //added notes as per the new CR - https://jira.savagebeast.com/browse/ESS-20327
                Time_Periods__r.Wednesday_Date__c, Time_Periods__r.Id, Time_Periods__r.Friday_Date__c, Time_Periods__r.Wednesday_Upsert_Key__c
                FROM Forecast__c
                WHERE User__c =: mapOfUserIdToForecast.keySet() 
                AND Time_Periods__c =: lstTP 
                AND Fiscal_Year__c =:integer.valueOf(fiscalYear) 
                AND Fiscal_Quarter__c =: integer.valueOf(fiscalQuarter)  
            ];
            //Get Existing forecast if they exist
            if(! result.isEmpty()) {
                for(Forecast__c f: result) {
                    if(mapOfUserIdToForecast.containsKey(f.User__c)) {
                        if( mapOfUserIdToForecast.get(f.User__c).containsKey(f.Time_Periods__r.Wednesday_Date__c)) {
                            mapOfUserIdToForecast.get(f.User__c).put(f.Time_Periods__r.Wednesday_Date__c, f);
                        } else {
                            mapOfUserIdToForecast.get(f.User__c).put(f.Time_Periods__r.Wednesday_Date__c, f);
                        }
                    } 
                }
            }

            Integer idx = 0;
            Map<Date, Decimal> mapOfDateToTotal = new Map<Date, Decimal>();
            for(Id id: mapOfUserIdToForecast.keySet()) {
                ForecastWrapper fw = new ForecastWrapper();
                fw.userName = mapOfUsers.get(id).Name;
                fw.userActive = mapOfUsers.get(id).IsActive;
                fw.notes = '';
                fw.userQuota = (integer.valueOf(fiscalQuarter) == currentQuarter && mapOfUsers.get(id).Current_Quarter_Goal__c <> null) ? mapOfUsers.get(id).Current_Quarter_Goal__c.setScale(0) : 0;
                userQuotaTotal += fw.userQuota;
                fw.userRole = mapOfUsers.get(id).UserRole.Name;
                fw.userCurrency = mapOfUsers.get(id).DefaultCurrencyIsoCode;
                fw.userObj = mapOfUsers.get(id);
                fw.userBooked = (mapOfBooked.containsKey(id) && mapOfBooked.get(id) <> null )? ((Decimal)mapOfBooked.get(id)).setScale(0) : 0;
                userBookedTotal += fw.userBooked;
                fw.userUnweightedPP = (mapOfPipeline.containsKey(id) && mapOfPipeline.get(id) <> null) ? ((Decimal)mapOfPipeline.get(id)).setScale(0) : 0;
                userUnweightedPPTotal += fw.userUnweightedPP;
                fw.userGap = 0;
                fw.userToQuota = 0;
                if(integer.valueOf(fiscalQuarter) == currentQuarter && fw.userQuota != null && fw.userQuota > 0) {
                    fw.userToQuota = ((Decimal)((fw.userBooked/fw.userQuota) * 100)).setScale(0);
                    if(fw.userBooked != null) {
                        fw.userGap = ((Decimal)(fw.userQuota - fw.userBooked)).setScale(0);
                        userGapTotal += fw.userGap;
                    }
                }
                List<TimeperiodsWrapper> lstTPW = new List<TimeperiodsWrapper>();
                for(Forecast__c f: mapOfUserIdToForecast.get(id).values()) {
                    TimeperiodsWrapper tpw = new TimeperiodsWrapper();
                    f.Fiscal_Quarter__c = integer.valueOf(fiscalQuarter);
                    f.Fiscal_Year__c = integer.valueOf(fiscalYear);
                    tpw.objF = f;
                    tpw.placeholder = new Opportunity(Amount=f.Forecast_Amount__c); // create and initially populate the placeholder
                    tpw.originalObjFA = f.Forecast_Amount__c;
                    tpw.wedDate = f.Time_Periods__r.Wednesday_Date__c.format();
                    tpw.wedDateActual = f.Time_Periods__r.Wednesday_Date__c;
                    tpw.friDateActual = f.Time_Periods__r.Friday_Date__c;
                    lstTPW.add(tpw);
                    if(idx == 0) {
                        lstTPWDisplay.add(tpw);
                    }
                    if(mapOfDateToTotal.containsKey(f.Time_Periods__r.Wednesday_Date__c)) {
                        Decimal tempAmt = f.Forecast_Amount__c <> null ? f.Forecast_Amount__c : 0;
                        mapOfDateToTotal.put(f.Time_Periods__r.Wednesday_Date__c, (mapOfDateToTotal.get(f.Time_Periods__r.Wednesday_Date__c) + tempAmt));
                    } else {
                        Decimal tempAmt = f.Forecast_Amount__c <> null ? f.Forecast_Amount__c : 0;
                        mapOfDateToTotal.put(f.Time_Periods__r.Wednesday_Date__c,tempAmt);
                    }
                    
                }
                if(idx == 0){
                    lstTPWDisplay.sort();
                }
                lstTPW.sort();
                if(! lstTPW.isEmpty()) {
                    fw.notes = lstTPW[0].objF.Notes__c;//its a dirty way but we cannot create a new object to store comments in the last minute.
                    fw.originalNotes = lstTPW[0].objF.Notes__c;
                }
                system.debug('****' + lstTPW);
                fw.timePeriodWrapper = lstTPW;
                lstOfFW.add(fw);
                idx ++;
            }
            if(! mapOfDateToTotal.isEmpty()) {
                List<Date> dateList = new List<Date>();
                dateList.addAll(mapOfDateToTotal.keyset());
                dateList.sort();
                for(Date d: dateList) {
                    listOfTotals.add(mapOfDateToTotal.get(d));
                }
            }
        }
    }
    
    public void buttonSaveAndUpdateClicked() {
        List<Forecast__c> lstOfForecastToUpdate = new List<Forecast__c>();
        for(ForecastWrapper fw: lstOfFW) {
            for(TimeperiodsWrapper tpw: fw.timePeriodWrapper) {
                tpw.objF.Forecast_Amount__c = tpw.placeholder.Amount; // Get the total from the user-populateable placeholder field
                tpw.objF.Time_Periods__c = tpw.objF.Time_Periods__r.Id;
                tpw.objF.Quota__c = fw.userQuota;
                tpw.objF.Notes__c = fw.notes;
                system.debug('****tpw.objF.Forecast_Amount__c' + tpw.objF.Forecast_Amount__c);
                system.debug('****tpw.originalObjFA' + tpw.originalObjFA);
                if(tpw.objF.Id == null) {
                    lstOfForecastToUpdate.add(tpw.objF);
                    tpw.objF.Time_Periods__r = null;
                } else if(tpw.objF.Id != null) {
                    if(tpw.objF.Forecast_Amount__c != tpw.originalObjFA || fw.originalNotes != fw.notes) {
                        lstOfForecastToUpdate.add(tpw.objF);
                        tpw.objF.Time_Periods__r = null;
                    }
                }
            }
        }
        if(! lstOfForecastToUpdate.isEmpty()) {
            upsert lstOfForecastToUpdate ;
            viewPlanning();
            saveSuccess = true;
        }
    }
    
    public PageReference renderPage() {
        title = 'Exported Director Forecast Data by ' + UserInfo.getName();
        PageReference p = new PageReference ('/apex/PlanningTool_UI_Export?title=Planning Tool Export');
        p.setRedirect(false);
        return p;
    } 
    
    public class ForecastWrapper {
        public String userName {get;set;}
        public User userObj {get;set;}
        public Boolean userActive {get;set;}
        public String userRole {get;set;}
        public Double userQuota {get;set;}
        public String notes {get;set;}//added notes as per the new CR - https://jira.savagebeast.com/browse/ESS-20327
        public String originalNotes {get;set;}
        public Double userBooked {get;set;}
        public String userCurrency {get;set;}
        public Double userToQuota {get;set;}
        public Double userGap {get;set;}
        public Double userUnweightedPP {get;set;}
        public List<TimeperiodsWrapper> timePeriodWrapper {get;set;}
    }
    
    public class TimeperiodsWrapper implements comparable{
        public String wedDate {get;set;}
        public Forecast__c objF {get;set;}
        // Placeholder with user-enterable Amount field.
        // Used to temporarily store forecast amount as most users do not have FLS 
        // access to the Forecast_Amount__ field.
        public Opportunity placeholder {get;set;} 
        public Decimal originalObjFA {get;set;}
        public Date wedDateActual {get;set;}
        public Date friDateActual {get;set;}
        public Decimal oTotal {get;set;}
        /*
        Comparable.compareTo() implementation 
        */
        public Integer compareTo(Object other) {
            Date otherWed = other != null ? ((TimeperiodsWrapper)other).wedDateActual : System.today();   
            return otherWed.daysBetween(this.wedDateActual);
        }
    }

}