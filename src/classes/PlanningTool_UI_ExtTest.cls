@isTest(seealldata=true)
public class PlanningTool_UI_ExtTest {
	static testmethod void testPlanningTool_UI_Ext() {
        List<Time_Periods__c> allTimePeriods = new List<Time_Periods__c>();
        allTimePeriods = [Select Id from Time_Periods__c Limit 1];
        if(allTimePeriods.isEmpty()) {
            Time_Periods__c thisTimePeriod;
            Date limitTill = date.today().addyears(3);
            //Default values
            
            Date firstDayOfWeekCurrentMonth3YearsAfter =  date.newInstance(system.now().year(), 1, 1).adddays(7).toStartOfWeek();
            
            
            while(firstDayOfWeekCurrentMonth3YearsAfter <= limitTill) {
                thisTimePeriod = new Time_Periods__c();
                thisTimePeriod.Friday_Date__c = firstDayOfWeekCurrentMonth3YearsAfter.addDays(5);
                thisTimePeriod.Monday_Date__c = firstDayOfWeekCurrentMonth3YearsAfter.addDays(1);
                thisTimePeriod.Saturday_Date__c = firstDayOfWeekCurrentMonth3YearsAfter.addDays(6);
                thisTimePeriod.Sunday_Date__c = firstDayOfWeekCurrentMonth3YearsAfter;
                thisTimePeriod.Thursday_Date__c = firstDayOfWeekCurrentMonth3YearsAfter.addDays(4);
                thisTimePeriod.Tuesday_Date__c = firstDayOfWeekCurrentMonth3YearsAfter.addDays(2);
                thisTimePeriod.Wednesday_Date__c = firstDayOfWeekCurrentMonth3YearsAfter.addDays(3);
                thisTimePeriod.Name = thisTimePeriod.Wednesday_Date__c.format() + ' through ' + thisTimePeriod.Wednesday_Date__c.addDays(7).format();
                Datetime dt = datetime.newInstance(thisTimePeriod.Wednesday_Date__c.year(), thisTimePeriod.Wednesday_Date__c.month(),thisTimePeriod.Wednesday_Date__c.day());
                thisTimePeriod.Wednesday_Upsert_Key__c = dt.format('mmddyyyy');
                system.debug('thisTimePeriod: ---->>>>' + thisTimePeriod);
                firstDayOfWeekCurrentMonth3YearsAfter = firstDayOfWeekCurrentMonth3YearsAfter.addDays(7);
                allTimePeriods.add(thisTimePeriod);
            }
            
            if(! allTimePeriods.isEmpty()) {
                insert allTimePeriods;
            }
        }
    	PlanningTool_UI_Ext testExtension = new PlanningTool_UI_Ext();
        testExtension.getBookingsFiscalYears();
        testExtension.getBookingsFiscalQuarters();
        testExtension.selectedUsers = new List<SelectOption>{new SelectOption(UserInfo.getUserId(), UserInfo.getUserName())};
        testExtension.viewPlanning();
        testExtension.buttonSaveAndUpdateClicked();
        
    }
}