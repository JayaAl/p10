@isTest
public class Planning_MultiselectUserControllerTest {
    static testMethod void testMultiselectUserController() {
        Planning_MultiselectUserController c = new Planning_MultiselectUserController();
        
        c.leftOptionsSelect = new List<SelectOption>();
        c.rightOptionsSelect = new List<SelectOption>();
        
        c.leftOptionsHidden = 'A&a&b&b&C&c';
        c.rightOptionsHidden = '';
        
        System.assertEquals(c.leftOptionsSelect.size(), 3);
        System.assertEquals(c.rightOptionsSelect.size(), 0);
    }
}