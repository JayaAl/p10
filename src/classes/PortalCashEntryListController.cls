/*
@(#)Last changed:   $Date: 07/30/2015 $By: Lakshman Ace
@(#)Purpose:        List view of payments on customer portal
@(#)Author:         Lakshman Ace (sfdcace@gmail.com)
@(#)Project: 		ERP
*/
global class PortalCashEntryListController {

    public CUST_PortalUtils FLS{get{return new CUST_PortalUtils();}set;}
    
    public String current_type_filter {get;set;}
    public String accountFilterName {get; set;}

    public String current_account_filter {get;set;}

    public Boolean no_records_available {get;set;}

    public List<PortalCashEntryView> cashEntryListView {get; set;}

    // Set controller (handle more records)
    public ApexPages.StandardSetController setController {get;set;} 
    public String soql {get;set;}
    public String base_soql {get;set;}
    public String selected_account_id {get;set;}    
    
    // Pagination Methods for the set controller
    public Integer PAGE_SIZE{get;set;}

    // The list of accounts to filter
    public List<Account> accounts_related{get;set;}
    public Boolean multiple_accounts {get;set;}

    public void GoPrevious(){ setController.previous(); }
    public void GoNext(){ setController.next(); }
    public void GoLast(){ setController.last(); }
    public void GoFirst(){ setController.first(); }
    public Boolean getRenderPrevious(){return setController.getHasPrevious();}
    public Boolean getRenderNext(){return setController.getHasNext();}
    public Integer getRecordSize(){return setController.getResultSize();}
    public Integer getPageNumber(){return setController.getPageNumber();}
    public boolean has_pages {get;set;}
    
    public Integer sort_column_number{get;set;}  
    public Integer sort_param{get;set;}
    private String order_by;

    private List<Id> filter_notAllowedRecords;
    private String filter_hiddenUntilNumber;
    private Date filter_hiddenUntilDate;
    private String filter_hiddenByNamePattern;

    
    public CUST_PortalUtils filters{get;set;}
    private String filters_extraString_for_soql = '';
    private String filters_saveSortOrder = '';
    private Boolean filters_setOneTime_sort = false;
    private String filters_extraCash_filter1 = '';
    private Date filters_extraCash_filter2 = Date.newInstance(1800,1,1);
    private Date filters_extraCash_filter3 = Date.newInstance(2300,1,1);
    public void processFilters(){
        this.filters.receiveFilters();
        this.filters_extraString_for_soql = this.buildFiltersStringForSoql();
        this.filters_setOneTime_sort = true;        
        this.reloadTable();
    }
    private String buildFiltersStringForSoql(){
        String s = ' ';
        String name = '';
        Integer saveNumberStart = -1;
        Date saveDateStart = null;
        
        //Filters by Date
        this.filters_extraCash_filter2 = Date.newInstance(1800,1,1);
        if(this.filters.filterByDate_start != null && this.filters.filterByDate_start != ''){
            try{
                String[] split1 = this.filters.filterByDate_start.split('/');
                if(split1.size() == 3){
                    Date dt;
                    String dateFormat =CUST_PortalUtils.getCurrentDateFormat(); 
                    if(dateFormat.equals('mm/dd/yyyy')){
                        dt = Date.newInstance(Integer.valueOf(split1[2]),Integer.valueOf(split1[0]),Integer.valueOf(split1[1]));
                    }else{                      
                        dt = Date.newInstance(Integer.valueOf(split1[2]),Integer.valueOf(split1[1]),Integer.valueOf(split1[0]));
                    }
                    saveDateStart = dt;
                    if(dateFormat.equals('mm/dd/yyyy')){
                        this.filters.filterByDate_start = CUST_PortalUtils.getFormattedDateString(dt, 'MM/dd/yyyy');
                    }else{
                        this.filters.filterByDate_start = CUST_PortalUtils.getFormattedDateString(dt, 'dd/MM/yyyy');
                    }
                    s += 'AND Payment_Date__c >= ' + String.valueOf(dt) + ' ';
                }
            }catch(Exception e){}
        }
        this.filters_extraCash_filter3 = Date.newInstance(2300,1,1);
        if(this.filters.filterByDate_end != null && this.filters.filterByDate_end != ''){
            try{
                String[] split1 = this.filters.filterByDate_end.split('/');
                if(split1.size() == 3){
                    Date dt;
                    String dateFormat =CUST_PortalUtils.getCurrentDateFormat(); 
                    if(dateFormat.equals('mm/dd/yyyy')){
                        dt = Date.newInstance(Integer.valueOf(split1[2]),Integer.valueOf(split1[0]),Integer.valueOf(split1[1]));
                    }else{
                        dt = Date.newInstance(Integer.valueOf(split1[2]),Integer.valueOf(split1[1]),Integer.valueOf(split1[0]));
                    }
                    if(saveDateStart != null && dt < saveDateStart){
                        dt = saveDateStart;
                    }
                    if(dateFormat.equals('mm/dd/yyyy')){
                        this.filters.filterByDate_end = CUST_PortalUtils.getFormattedDateString(dt, 'MM/dd/yyyy');
                    }else{
                        this.filters.filterByDate_end = CUST_PortalUtils.getFormattedDateString(dt, 'dd/MM/yyyy');
                    }
                    s += ' AND Payment_Date__c <= ' + String.valueOf(dt) + ' ';
                }
            }catch(Exception e){}
        }

        //Filters by column:
        this.filters_extraCash_filter1 = '';
        if(this.filters.selectionInList != null && !this.filters.selectionInList.equals('all')){
            String st = this.filters.selectionInList;
            if(st.equals('payment')){
                this.filters_extraCash_filter1 = 'receipt';
            }else if(st.equals('refund')){
                this.filters_extraCash_filter1 = 'refund';              
            }
            s += ' AND Payment_Type__c  = \'' + filters_extraCash_filter1 + '\' ';    
        }
        

        if(s.length() > 1){
            s = ' ' + s.trim();
        }else{
            s = '';
        }

        return s;
    }   
 
     
    public String debugSt{get;set;}
    
    
    public Integer getTotalPages(){
        if(this.setController != null){
            if(this.setController.getResultSize() > 0){
                double div = (this.setController.getResultSize())/double.valueOf(String.valueOf(this.PAGE_SIZE));
                if (div > div.intValue()) {
                  div = div + 1 ;          
                }           
                return div.intValue();              
            }
        }
        return 1;
    }

    public Boolean page_validation{get;set;}

    public Integer recordsCount{get;set;} 

    public PortalCashEntryListController(){

        // +++++++++++++++++++++++++    
        // CHECK USER PRIVILEGES TO ACCESS:
        this.page_validation = true;

        this.recordsCount = 200000;
        
        this.sort_param = 1;
        this.sort_column_number = 2;
        
        this.accounts_related = new List<Account>();
        this.PAGE_SIZE = 25;
        
        
        // Set On Page document filters:
        this.filters = new CUST_PortalUtils();
        this.filters.filterByList = CUST_PortalUtils.getFiltersList2();
        this.filters.selectionInList = 'all';
        // ------------------------------

        
        prepareQueryValues();
        
        // Hide Records until Number:
        List<Id> accounts_ids = new List<Id>();
        for(Account a : this.accounts_related){
            accounts_ids.add(a.Id);
        }
                        
        this.multiple_accounts = false;
        if(this.accounts_related != null && this.accounts_related.size() > 1){          
            this.multiple_accounts = true;
        }
                    
        this.getCashEntryList();
        
    }

    public class PortalCashEntryView
    {
        public string cashEntryId{get;set;}
        public String lineId{get;set;}
        public String transactionLineItemId{get;set;}
                
        public String company{get;set;}
        public String account{get;set;}
        public String paymentReference {get;set;}
        public String paymentType{get;set;}
        public Date paymentDate{get;set;}

        public String paymentCurrency{get;set;}
        
        public Decimal paymentTotal{get;set;}
        public Decimal totalUnallocated{get;set;}
        public Decimal totalAllocated{get;set;}
        
                

        public PortalCashEntryView(ERP_Payment__c lineItem){ 

            this.cashEntryId = lineItem.id;
            
            this.account = lineItem.Account__r.Name;
            
            this.paymentType = lineItem.Payment_Type__c ;

            this.paymentDate = lineItem.Payment_Date__c ;
            
            this.paymentCurrency = lineItem.CurrencyISOCode;
            
            this.paymentTotal = -(lineItem.Payment_Total__c );
            this.totalUnallocated = -(lineItem.Total_Unallocated__c);
            this.paymentReference = lineItem.Payment_Reference__c;
            this.totalAllocated = this.paymentTotal - this.totalUnallocated;
            
            this.lineId = lineItem.Name;
            this.transactionLineItemId = lineItem.id;
            
        }
        
    }
    
    private void prepareQueryValues(){
        
        String myContactId = '';
            
        
        myContactId = [SELECT u.ContactId FROM User u WHERE u.id = :UserInfo.getUserId() LIMIT 1].ContactId;            
        
    
        Contact current_contact = [SELECT c.id,c.Name,c.AccountId FROM Contact c WHERE c.id = :myContactId limit 1];
            
        
        if(this.selected_account_id != null && this.selected_account_id != '' && this.selected_account_id.toLowerCase() != 'all'){ // accountFilter != null
            //this.accounts_related = [SELECT a.id, a.Name FROM Account a WHERE a.CODAFinanceContact__c = :myContactId];//:myContactId AND a.id = :this.selected_account_id];
            this.accounts_related = [SELECT a.id, a.Name FROM Account a WHERE a.id = :current_contact.AccountId];//:myContactId AND a.id = :this.selected_account_id];
        }else{
            //this.accounts_related = [SELECT a.id, a.Name FROM Account a WHERE a.CODAFinanceContact__c = :myContactId];
            this.accounts_related = [SELECT a.id, a.Name FROM Account a WHERE a.id = :current_contact.AccountId];
        }
    }
    
    public void queryCashEntries(){

        
        this.prepareQueryValues();


            if(this.sort_param == 1){
                if(this.sort_column_number == 1){
                    this.sort_column_number = 2;
                this.order_by = 'ORDER BY Payment_Date__c ASC';
                }else{
                    this.sort_column_number = 1;
                this.order_by = 'ORDER BY Payment_Date__c DESC';
                }
            }else if(this.sort_param == 2){
                if(this.sort_column_number == 3){
                    this.sort_column_number = 4;
                this.order_by = 'ORDER BY CurrencyIsoCode DESC';
                }else{
                    this.sort_column_number = 3;
                this.order_by = 'ORDER BY CurrencyIsoCode ASC';
                }
            }else if(this.sort_param == 3){
                if(this.sort_column_number == 5){
                    this.sort_column_number = 6;
                this.order_by = 'ORDER BY Payment_Total__c DESC';
                }else{
                    this.sort_column_number = 5;
                this.order_by = 'ORDER BY Payment_Total__c ASC';
                }
            }else if(this.sort_param == 4){
                if(this.sort_column_number == 11){
                    this.sort_column_number = 12;
                this.order_by = 'ORDER BY Company__r.Name DESC';
                }else{
                    this.sort_column_number = 11;
                this.order_by = 'ORDER BY Company__r.Name ASC';
                }
            }else if(this.sort_param == 5){
                if(this.sort_column_number == 9){
                    this.sort_column_number = 10;
                this.order_by = 'ORDER BY Total_Unallocated__c DESC';
                }else{
                    this.sort_column_number = 9;
                this.order_by = 'ORDER BY Total_Unallocated__c ASC';
                }
            }
            

            
            Boolean reloadQuery = false;
            if(this.sort_param > 0){
                reloadQuery = true;
            }
            this.sort_param = 0;

        String extra_filters = this.filters_extraString_for_soql;
        if(this.selected_account_id != null && this.selected_account_id != '' && this.selected_account_id.toLowerCase() != 'all'){
            this.soql = 'SELECT Account__c,Account__r.Name, Company__c,CreatedById,CreatedDate,CurrencyIsoCode,Id,IsDeleted,LastModifiedById,LastModifiedDate,Name,OwnerId,Payment_Date__c,Payment_Reference__c,Payment_Total__c,Payment_Type__c,SystemModstamp,Total_Allocated__c,Total_Unallocated__c,UpsertKey__c FROM ERP_Payment__c where Account__c = :selected_account_id AND Account__c IN: accounts_related' + extra_filters + ' ' + this.order_by;
        }else{
            this.soql = 'SELECT Account__c,Account__r.Name, Company__c,CreatedById,CreatedDate,CurrencyIsoCode,Id,IsDeleted,LastModifiedById,LastModifiedDate,Name,OwnerId,Payment_Date__c,Payment_Reference__c,Payment_Total__c,Payment_Type__c,SystemModstamp,Total_Allocated__c,Total_Unallocated__c,UpsertKey__c FROM ERP_Payment__c where Account__c IN: accounts_related' + extra_filters + ' ' + this.order_by;
        }
    
        // Base SOQL sentence
        this.base_soql = this.soql;
        
        if(this.setController == null || reloadQuery == true){
            this.setController = new ApexPages.StandardSetController(Database.getQueryLocator(this.soql));
        }
        this.setController.setPageSize(this.PAGE_SIZE);

        if(getTotalPages() > 1){
            this.has_pages = true;
        } else {
            this.has_pages = false;
        }

        
        if(this.setController.getResultSize() > 0){
            this.no_records_available = false;
        }else{
            this.no_records_available = true;
        }
        
        
        
        if(ApexPages.currentPage().getParameters().get('p') != null){
            this.setController.setpageNumber(integer.valueOf(ApexPages.currentPage().getParameters().get('p')));
        }
        
    }
    
    
    // Getter for the table 
    public List<PortalCashEntryView> getCashEntryList(){
        List<PortalCashEntryView> aList = new List<PortalCashEntryView>();
        
        // ______________________   
        // TRANSACTION LINE ITEMS
        this.filterCashEntries();
        this.queryCashEntries();
		
         List<ERP_Payment__c> cashEntryLineItems = (List<ERP_Payment__c>) this.setController.getRecords();
         
        for(Integer i = 0; i < cashEntryLineItems.size(); i++){
        	aList.add(new PortalCashEntryView(cashEntryLineItems[i]));
        }
        
                
             

        
        this.recordsCount = aList.size();


        return aList;
    }
    private void search_CashEntry_From_CashEntryLineItem(){
        
    }
    
   

    
    public void filterCashEntries(){

        
        if(ApexPages.currentPage().getParameters().get('filter') != null && ApexPages.currentPage().getParameters().get('filter') != ''){
            this.selected_account_id = ApexPages.currentPage().getParameters().get('filter');
        }
        if(this.selected_account_id == null || this.selected_account_id == ''){
            this.selected_account_id = 'all';
        }

    }

    public void reloadTable(){

        this.selected_account_id = this.current_account_filter;
        
        this.setController = null;
        
        this.queryCashEntries();
        
        ApexPages.currentPage().getParameters().put('p',null);


    }   

    // Get the list of accounts for the current contact
    public List<SelectOption> getActiveContactAccounts() {
        List<SelectOption> options = new List<SelectOption>();

        options.add(new SelectOption('all','All'));
        
        for(Account a:this.accounts_related){
            options.add(new SelectOption(a.Id,a.Name));
        }
                
        return options;
    }
    
    
                    
}