@isTest
public class PortalCashEntryListControllerTest {
	enum PortalType { CSPLiteUser, PowerPartner, PowerCustomerSuccess, CustomerSuccess }
    static testMethod void unitTestCashEntryList() {
        //Generate test data
		User pu = getPortalUser(PortalType.PowerCustomerSuccess, null, true);
        System.assert([select isPortalEnabled
                         from user
                        where id = :pu.id].isPortalEnabled,
                      'User was not flagged as portal enabled.');      
        System.RunAs(pu) {
            System.assert([select isPortalEnabled
                             from user
                            where id = :UserInfo.getUserId()].isPortalEnabled,
                          'User wasnt portal enabled within the runas block. ');
    
        
			
            PortalCashEntryListController cont = new PortalCashEntryListController();
            cont.getActiveContactAccounts();
            cont.getCashEntryList();
            cont.reloadTable();
            
            cont.GoPrevious();
            cont.GoNext();
            cont.GoLast();
            cont.GoFirst();
            cont.getRenderPrevious();
            cont.getRenderNext();
            cont.getRecordSize();
            cont.getPageNumber();       
            cont.getTotalPages();
            
            cont.filters.selectionInList = 'payment';
            cont.filters.filterByNumber_start = '15';
            cont.filters.filterByNumber_end = '120';
            cont.filters.filterByDate_start = '01/01/2010'; 
            cont.filters.filterByDate_end = '01/01/2011'; 
            
            cont.processFilters();
            

            
    	}
    }
    
    
    public static User getPortalUser(PortalType portalType, User userWithRole, Boolean doInsert) {

        /* Make sure the running user has a role otherwise an exception
           will be thrown. */
        if(userWithRole == null) {  
            if(UserInfo.getUserRoleId() == null) {
                UserRole r = new UserRole(name = 'TEST ROLE ERP');
                Database.insert(r);
                userWithRole = new User(alias = 'hasrole', email='userwithrole@roletest1.com', userroleid = r.id,
                                    emailencodingkey='UTF-8', lastname='Testing', languagelocalekey='en_US', 
                                    localesidkey='en_US', profileid = UserInfo.getProfileId(), 
                                    timezonesidkey='America/Los_Angeles', username='userwithrole@testorg.com');
            } else {
                userWithRole = new User(Id = UserInfo.getUserId(), UserRoleId = UserInfo.getUserRoleId());
            }
            System.assert(userWithRole.userRoleId != null,
                          'This test requires the target org to have at least one UserRole created. Please create a user role in this organization and try again.');
        }
        Account a;
        Contact c;

        System.runAs(userWithRole) {
            a = UTIL_TestUtil.generateAccount();
            a.Credit_Card_Payments_Accepted__c = true;
            insert a;
            
            c = UTIL_TestUtil.createContact(a.Id);
            
            ERp_Payment__c p = new ERp_Payment__c(Account__c = a.Id,  Payment_Date__c = Date.today(), Name = '12345', Payment_Total__c = 1000, Total_Unallocated__c = 2000);
            insert p;
            
        }
        /* Get any profile for the given type.*/
        Profile p = [select id
                      from profile
                     where usertype = :portalType.name()
                     limit 1];  
        String testemail = 'puser000@amamama.com';
        User pu = new User(profileId = p.id, username = testemail, email = testemail,
                           emailencodingkey = 'UTF-8', localesidkey = 'en_US',
                           languagelocalekey = 'en_US', timezonesidkey = 'America/Los_Angeles',
                           alias='cspu', lastname='lastname', contactId = c.id);
        if(doInsert) {
            Database.insert(pu);
        }
        return pu;
    }
}