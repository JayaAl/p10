global class PortalCreditNoteListController {
    
    public CUST_PortalUtils FLS{get{return new CUST_PortalUtils();}set;}
    
    // List view
    public List<PortalCreditNoteView> creditNoteListView {get; set;}
    public Id accountFilterId {get; set;}
    public String accountFilterName {get; set;}
    public String selected_account_id {get;set;}    
    public Boolean no_records_available {get;set;}
    
    // The list of accounts to filter 
    private List<Account> accounts_related = new List<Account>();
    public Boolean multiple_accounts {get;set;}
    
    // Set controller (handle more records)
    public ApexPages.StandardSetController setController {get;set;} 
    private String soql {get;set;}
    private String base_soql {get;set;}
    
    // Pagination Methods for the set controller
    public Integer PAGE_SIZE = 25;
    
    public void GoPrevious(){ setController.previous(); }
    public void GoNext(){ setController.next(); }
    public void GoLast(){ setController.last(); }
    public void GoFirst(){ setController.first(); }
    public Boolean getRenderPrevious(){return setController.getHasPrevious();}
    public Boolean getRenderNext(){return setController.getHasNext();}
    public Integer getRecordSize(){return setController.getResultSize();}
    public Integer getPageNumber(){return setController.getPageNumber();}
    public boolean has_pages {get;set;}
    
    // Static vars, needed inside of the credit note inner class
    public static String current_filter_static {get;set;}
    public static String current_page_static {get;set;}
    public static String current_sort_order_static {get;set;}
    public static String current_sort_field_static {get;set;}

    public static boolean isApexTest = false;

    // __________________________________
    private List<Id> filter_notAllowedRecords;
    private String filter_hiddenUntilNumber;
    private Date filter_hiddenUntilDate;
    
    
    public CUST_PortalUtils filters{get;set;}
    private String filters_extraString_for_soql = '';
    private String filters_saveSortOrder = '';
    private Boolean filters_setOneTime_sort = false;    
    public void processFilters(){
        this.filters.receiveFilters();
        this.filters_extraString_for_soql = this.buildFiltersStringForSoql();
        this.filters_setOneTime_sort = true;        
        this.filterCreditNotes();
    }
    private String buildFiltersStringForSoql(){
        String s = ' ';
        Date saveDateStart = null;
        //Filters by Date
        if(this.filters.filterByDate_start != null && this.filters.filterByDate_start != ''){
            try{
                String[] split1 = this.filters.filterByDate_start.split('/');
                if(split1.size() == 3){
                    Date dt;
                    String dateFormat =CUST_PortalUtils.getCurrentDateFormat(); 
                    if(dateFormat.equals('mm/dd/yyyy')){
                        dt = Date.newInstance(Integer.valueOf(split1[2]),Integer.valueOf(split1[0]),Integer.valueOf(split1[1]));
                    }else{
                        dt = Date.newInstance(Integer.valueOf(split1[2]),Integer.valueOf(split1[1]),Integer.valueOf(split1[0]));
                    }
                    saveDateStart = dt;
                    if(dateFormat.equals('mm/dd/yyyy')){
                        this.filters.filterByDate_start = CUST_PortalUtils.getFormattedDateString(dt, 'MM/dd/yyyy');
                    }else{
                        this.filters.filterByDate_start = CUST_PortalUtils.getFormattedDateString(dt, 'dd/MM/yyyy');
                    }
                    s += 'AND Credit_Note_Date__c >= ' + String.valueOf(dt) + ' ';
                }
            }catch(Exception e){this.filters.filterByDate_start = '';}
        }
        if(this.filters.filterByDate_end != null && this.filters.filterByDate_end != ''){
            try{
                String[] split1 = this.filters.filterByDate_end.split('/');
                if(split1.size() == 3){
                    Date dt;
                    String dateFormat =CUST_PortalUtils.getCurrentDateFormat(); 
                    if(dateFormat.equals('mm/dd/yyyy')){
                        dt = Date.newInstance(Integer.valueOf(split1[2]),Integer.valueOf(split1[0]),Integer.valueOf(split1[1]));
                    }else{
                        dt = Date.newInstance(Integer.valueOf(split1[2]),Integer.valueOf(split1[1]),Integer.valueOf(split1[0]));
                    }
                    if(saveDateStart != null && dt < saveDateStart){
                        dt = saveDateStart;
                    }
                    if(dateFormat.equals('mm/dd/yyyy')){
                        this.filters.filterByDate_end = CUST_PortalUtils.getFormattedDateString(dt, 'MM/dd/yyyy');
                    }else{
                        this.filters.filterByDate_end = CUST_PortalUtils.getFormattedDateString(dt, 'dd/MM/yyyy');
                    }
                    s += 'AND Credit_Note_Date__c <= ' + String.valueOf(dt) + ' ';
                }
            }catch(Exception e){this.filters.filterByDate_end = '';}
        }

        //Filters by column:
        if(this.filters.selectionInList != null && !this.filters.selectionInList.equals('all')){
            String st = this.filters.selectionInList;
            if(st.equals('paid')){
                s += 'AND Payment_Status__c = \'' + 'paid' + '\' ';             
            }else if(st.equals('unpaid')){
                s += 'AND Payment_Status__c = \'' + 'unpaid' + '\' ';               
            }else if(st.equals('partpaid')){
                s += 'AND Payment_Status__c = \'' + 'part paid' + '\' ';                
            }
        }
        
        if(s.length() > 1){
            s = ' ' + s.trim();
        }else{
            s = '';
        }

        return s;
    }
        
    

    public Integer getTotalPages(){
        double div = (setController.getResultSize())/double.valueOf(String.valueOf(PAGE_SIZE));
        if (div > div.intValue()) {
          div = div + 1 ;          
        }           
        return div.intValue();
    }
    
    public Boolean page_validation{get;set;}
    
    // Constructor
    public PortalCreditNoteListController()
    {
        this.page_validation = true;
        //no_records_available = false;
        Id currentUserId = UserInfo.getUserId();
        User u = null;

        u = [Select ContactId, Contact.Name, Contact.Id, Contact.AccountId  from User where Id =: currentUserId Limit 1];
        String contact_id = u.contact.id;
        sortedByUrl = false;
        
        
        //accounts_related = [Select a.id, a.Name From Account a where a.c2g__CODAFinanceContact__c =: contact_id];
        accounts_related = [Select a.id, a.Name From Account a where a.id = :u.Contact.AccountId];
        
        
        // construct invoice list view
        if(accounts_related.size() > 0){
            selected_account_id = 'all';
            //selected_account_id = accounts_related[0].Id;
        }
        
        // Set On Page document filters:
        this.filters = new CUST_PortalUtils();
        // ESS-37237
        this.filters.filterByList = CUST_PortalUtils.getFiltersList1('PortalCreditNote'); // Adding the additiotnal parameter to cope up with the change in helper method
        this.filters.selectionInList = 'all';
        // Load Extra Filters:
        if(ApexPages.currentPage().getParameters().get('fby') != null){
            this.filters.selectionInList = ApexPages.currentPage().getParameters().get('fby');
        }
        if(ApexPages.currentPage().getParameters().get('fbyn1') != null){
            this.filters.filterByNumber_start = ApexPages.currentPage().getParameters().get('fbyn1');
        }
        if(ApexPages.currentPage().getParameters().get('fbyn2') != null){
            this.filters.filterByNumber_end = ApexPages.currentPage().getParameters().get('fbyn2');
        }
        if(ApexPages.currentPage().getParameters().get('fbyd1') != null){
            this.filters.filterByDate_start = ApexPages.currentPage().getParameters().get('fbyd1');
        }
        if(ApexPages.currentPage().getParameters().get('fbyd2') != null){
            this.filters.filterByDate_end = ApexPages.currentPage().getParameters().get('fbyd2');
        }
        this.filters_extraString_for_soql = this.buildFiltersStringForSoql();
        // ------------------------------
        
        multiple_accounts = false;
        
        if(accounts_related.size() > 1){
            multiple_accounts = true;
        }
        
        // Hide Records until Number:
        List<Id> accounts_ids = new List<Id>();
        for(Account a : this.accounts_related){
            accounts_ids.add(a.Id);
        }

                
        String extra_filters = this.filters_extraString_for_soql;               
        if(selected_account_id == 'all'){
            soql = 'Select Total_Credit_Note__c,ERP_Invoice_Id__c, ERP_Org_Id__c, Account__r.Name, Company__r.Name, c.Company__c, c.Total_Unallocated__c, c.Credit_Note_Date__c, c.Total_Allocated__c, c.Payment_Status__c, c.Account__c,  c.Name,  c.Id, c.CurrencyIsoCode, Advertiser__r.Name  From ERP_Credit_Note__c c where Account__c  in :accounts_related ' + extra_filters + ' ';
        } else {
            soql = 'Select Total_Credit_Note__c,ERP_Invoice_Id__c, ERP_Org_Id__c, Account__r.Name, Company__r.Name, c.Company__c, c.Total_Unallocated__c, c.Credit_Note_Date__c, c.Total_Allocated__c, c.Payment_Status__c, c.Account__c,  c.Name,  c.Id, c.CurrencyIsoCode, Advertiser__r.Name  From ERP_Credit_Note__c c where Account__c  =: selected_account_id ' + extra_filters + ' ';
        }       
        
        // Base SOQL sentence
        base_soql = soql;
                    
        if(ApexPages.currentPage().getParameters().get('filter') != null){
            selected_account_id = ApexPages.currentPage().getParameters().get('filter');
            filterCreditNotes();
        }
        
        if(ApexPages.currentPage().getParameters().get('sort_field') != null){
            String sort_field = ApexPages.currentPage().getParameters().get('sort_field');
            if(sort_field == 'CreditNoteCurrencies'){ sortCreditNoteCurrencies(); }
            if(sort_field == 'CreditNoteTotals'){ sortCreditNoteTotals(); }
            if(sort_field == 'OutstandingAmounts'){ sortOutstandingAmounts(); }
            if(sort_field == 'Companies'){ sortCompanies(); }
            if(sort_field == 'Advertisers'){ sortAdvertisers(); }
            if(sort_field == 'AllocatedAmount'){ sortAllocatedAmount(); }
            
        } else {
            queryCreditNotes();
        } 
        
        if(ApexPages.currentPage().getParameters().get('p') != null){
            setController.setpageNumber(integer.valueOf(ApexPages.currentPage().getParameters().get('p')));
        }
            
    }
    
    
    // Credit Notes / Cash Receipts / Invoices filter
    public List<SelectOption> gettype_filter_options(){
        
        List<SelectOption> type_filter_options = new List<SelectOption>();
        
        // Construct type filter
        type_filter_options.add(new SelectOption('cash_entries','Cash Entries'));
        type_filter_options.add(new SelectOption('credit_notes','Credit Notes'));       
        type_filter_options.add(new SelectOption('sales_invoices','Sales Invoices'));
        type_filter_options.add(new SelectOption('all','All'));
        
        return type_filter_options; 
    }
    
    public String current_type_filter {get;set;}
        
    // Get the list of accounts for the current contact
    public List<SelectOption> getActiveContactAccounts() {
        List<SelectOption> options = new List<SelectOption>();

        options.add(new SelectOption('all','All'));     
        
        for(Account a:accounts_related){
            options.add(new SelectOption(a.Id,a.Name));
        }
        
        return options;
    }
    
    public void filterCreditNotes(){

        String extra_filters = this.filters_extraString_for_soql;
        if(selected_account_id == 'all'){
            base_soql = 'Select Total_Credit_Note__c,ERP_Invoice_Id__c, ERP_Org_Id__c, Account__r.Name, Company__r.Name, c.Company__c, c.Total_Unallocated__c, c.Credit_Note_Date__c, c.Total_Allocated__c, c.Payment_Status__c, c.Account__c,  c.Name,  c.Id, c.CurrencyIsoCode, Advertiser__r.Name  From ERP_Credit_Note__c c where Account__c  in :accounts_related ' + extra_filters + ' ';
        } else {
            base_soql = 'Select Total_Credit_Note__c,ERP_Invoice_Id__c, ERP_Org_Id__c, Account__r.Name, Company__r.Name, c.Company__c, c.Total_Unallocated__c, c.Credit_Note_Date__c, c.Total_Allocated__c, c.Payment_Status__c, c.Account__c,  c.Name,  c.Id, c.CurrencyIsoCode, Advertiser__r.Name  From ERP_Credit_Note__c c where Account__c  =: selected_account_id ' + extra_filters + ' ';
        }
        
        if(this.filters_setOneTime_sort == true){
            this.filters_setOneTime_sort = false;
            base_soql += this.filters_saveSortOrder;
        }
        
        soql = base_soql;
        
        queryCreditNotes();
    }
    
    public void queryCreditNotes(){
            
        this.setController = new ApexPages.StandardSetController(Database.getQueryLocator(soql));
        this.setController.setPageSize(PAGE_SIZE);
        
        if(getTotalPages() > 1){
            has_pages = true;
        } else {
            has_pages = false;
        }
        
        no_records_available = true;
        if( this.setController.getResultSize() > 0 ){
            no_records_available = false;
        }
        
    }
    
    // Sorting
    public String previousSortField {get;set;}  
    public String sortOrder {get;set;}
    public Boolean sortedByUrl {get;set;}

    private Boolean setReOrder = false;
    
    public void doSort(String soql_query, String sortField){
        
        // The first time we sort, if not set previousSortField and the order comes through parameters, avoid the sorting change.
        if(!sortedByUrl && ApexPages.currentPage().getParameters().get('sort_order') != null){
            sortOrder = ApexPages.currentPage().getParameters().get('sort_order');
            previousSortField = sortField;
            sortedByUrl = true;
        } else {
            sortOrder = 'asc';
            
            if(setReOrder == true){
                previousSortField = '-';
                setReOrder = false;
            }
                        
            if(previousSortField == sortField){
                sortOrder = 'desc';
                setReOrder = true;
            }else{
                previousSortField = sortField;
            }
        }
        soql = soql_query+sortOrder;
        
        this.filters_saveSortOrder += sortOrder;
        
        queryCreditNotes();
    }
    
    
    public void sortCreditNoteNumbers() {
        this.removeCurentSortOrder();
        String soql_query = base_soql+'order by Name ';
        this.filters_saveSortOrder = 'order by Name ';
        doSort(soql_query,'CreditNoteNumbers');
    }

    public void sortCreditNoteDates() {
        this.removeCurentSortOrder();
        String soql_query = base_soql+'order by Credit_Note_Date__c ';
        this.filters_saveSortOrder = 'order by Credit_Note_Date__c ';
        doSort(soql_query,'CreditNoteDates');
    }
    
    
    public void sortCreditNoteCurrencies() {
        this.removeCurentSortOrder();
        String soql_query = base_soql+'order by CurrencyIsoCode ';
        this.filters_saveSortOrder = 'order by CurrencyIsoCode ';        
        doSort(soql_query,'CreditNoteCurrencies');
    }
    
    public void sortCreditNoteTotals() {
        this.removeCurentSortOrder();
        String soql_query = base_soql+'order by Total_Credit_Note__c ';
        this.filters_saveSortOrder = 'order by Total_Credit_Note__c ';
        doSort(soql_query,'CreditNoteTotals');
    }
    
    public void sortOutstandingAmounts() {
        this.removeCurentSortOrder();
        String soql_query = base_soql+'order by Total_Unallocated__c ';
        this.filters_saveSortOrder = 'order by Total_Unallocated__c ';
        doSort(soql_query,'OutstandingAmounts');
    }
    
    public void sortCompanies() {
        this.removeCurentSortOrder();
        String soql_query = base_soql+'order by Company__r.Name ';
        this.filters_saveSortOrder = 'order by Company__r.Name ';
        doSort(soql_query,'Companies');
    }
        
    public void sortAdvertisers() {
        this.removeCurentSortOrder();
        String soql_query = base_soql+'order by Advertiser__r.Name ';
        this.filters_saveSortOrder = 'order by Advertiser__r.Name ';
        doSort(soql_query,'Advertisers');
    }
    
    public void sortAllocatedAmount(){
        this.removeCurentSortOrder();
        String soql_query = base_soql+'order by Total_Allocated__c ';
        this.filters_saveSortOrder = 'order by Total_Allocated__c ';
        doSort(soql_query,'AllocatedAmount');
    }
        
    private void removeCurentSortOrder(){
        if(base_soql != null && !base_soql.equals('')){
            String aux = base_soql.toLowerCase();
            Integer pos = aux.indexOf('order by');
            if(pos > -1){
                base_soql = base_soql.substring(0,pos);
            }
        }
    }
            
    // Getter for the table 
    public List<PortalCreditNoteView> getCreditNoteList(){
        
        creditNoteListView = new List<PortalCreditNoteView>();
        
        current_page_static = String.valueOf(setController.getPageNumber());
        current_filter_static = selected_account_id;
        
        // Set statics for link creation.
        current_sort_order_static = sortOrder;
        current_sort_field_static = previousSortField; 
        
        for(ERP_Credit_Note__c credit_note : getQuery())
        {
            creditNoteListView.add(new PortalCreditNoteView(credit_note,this));
        }
        
        return creditNoteListView;
    } 
    
    
    public List<ERP_Credit_Note__c> getQuery(){
        return (List<ERP_Credit_Note__c>) this.setController.getRecords();
    }
    
    public class PortalCreditNoteView
    {
        public ERP_Credit_Note__c creditNote {get;set;}
        
        public String creditNoteName {get;set;}
        public Date creditNoteDate {get;set;}
        public Double creditNoteTotal {get;set;}
        public String creditNoteCompany {get;set;}
        public String creditNoteAccount {get;set;}
        public String creditNoteCustomerReference {get;set;}
        public Date creditNoteDueDate {get;set;}
        public String creditNoteCurrency {get;set;}
        public String creditNotePaymentStatus {get;set;} 
        public String creditNoteOutstandingAmount {get;set;}
        public String type_of_record {get;set;}
        public Double creditNoteAllocatedAmount{get; set;}
        public Decimal creditNoteOutstandingAmount2 {get;set;}
        public String advertiserName {get;set;}  
        public String viewCreditNoteURL {get;set;}
        public PortalCreditNoteListController parentController;
        
        public PortalCreditNoteView(){}
        
        public PortalCreditNoteView(ERP_Credit_Note__c myCreditNote, PortalCreditNoteListController parent)
        {
            this.creditNote = myCreditNote;
            
            creditNoteName = creditNote.Name;
            creditNoteDate = creditNote.Credit_Note_Date__c;
            creditNoteTotal = creditNote.Total_Credit_Note__c;
            creditNoteCompany = String.valueOf(creditNote.Company__r.Name );
            creditNoteCurrency = String.valueOf(creditNote.CurrencyIsoCode);
            creditNoteOutstandingAmount = String.valueOf(creditNote.Total_Unallocated__c);
            creditNoteAllocatedAmount = creditNote.Total_Allocated__c;
            viewCreditNoteURL = '/apex/CUST_ViewPDF?invoiceNum=' + creditNote.Name + '&ordId=' + creditNote.ERP_Org_Id__c +'&type=CM';
            
            
            SObject advertiser = creditNote.getSObject('Advertiser__r');
            if(advertiser != null)
            {
                advertiserName = (String)advertiser.get('Name');
            }
            
            
            try{
                this.creditNoteOutstandingAmount2 = Math.abs(creditNote.Total_Unallocated__c);
            }catch(Exception e){
                this.creditNoteOutstandingAmount2 = 0;
            }
            
            parentController = parent;
                        
        }       
    }
}