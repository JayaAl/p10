@isTest
public class PortalCreditNoteListControllerTest {
	enum PortalType { CSPLiteUser, PowerPartner, PowerCustomerSuccess, CustomerSuccess }
    static testMethod void unitTestCashEntryList() {
        //Generate test data
		User pu = getPortalUser(PortalType.PowerCustomerSuccess, null, true);
        System.assert([select isPortalEnabled
                         from user
                        where id = :pu.id].isPortalEnabled,
                      'User was not flagged as portal enabled.');      
        System.RunAs(pu) {
            System.assert([select isPortalEnabled
                             from user
                            where id = :UserInfo.getUserId()].isPortalEnabled,
                          'User wasnt portal enabled within the runas block. ');
    
        
			
            ApexPages.currentPage().getParameters().put('fby','1111');
            ApexPages.currentPage().getParameters().put('fbyn1','2222');
            ApexPages.currentPage().getParameters().put('fbyn2','3333');
            ApexPages.currentPage().getParameters().put('fbyd1','4444');
            ApexPages.currentPage().getParameters().put('fbyd2','5555');
                       
            ApexPages.currentPage().getParameters().put('sort_field','Companies');

            PortalCreditNoteListController cont = new PortalCreditNoteListController();         
            
            cont.getCreditNoteList();
            
            cont.GoPrevious();
            cont.GoNext();
            cont.GoLast();
            cont.GoFirst();
            cont.getRenderPrevious();
            cont.getRenderNext();
            cont.getRecordSize();
            cont.getPageNumber();
    
            cont.sortCreditNoteNumbers();
            cont.sortCreditNoteDates();
            cont.sortCreditNoteCurrencies();
            cont.sortCreditNoteTotals();
            cont.sortOutstandingAmounts();
            cont.sortCompanies();

            cont.filterCreditNotes();
            cont.gettype_filter_options();

            cont.filters.selectionInList = 'paid';
            cont.filters.filterByNumber_start = '15';
            cont.filters.filterByNumber_end = '120';
            cont.filters.filterByDate_start = '01/01/2010'; 
            cont.filters.filterByDate_end = '01/01/2011'; 
                        
            PortalCreditNoteListController.PortalCreditNoteView pcv = new PortalCreditNoteListController.PortalCreditNoteView(new ERP_Credit_Note__c(), cont);
            
            
            cont.processFilters();
            cont.getActiveContactAccounts();
    
                      
            ApexPages.currentPage().getParameters().put('sort_field','CreditNoteNumbers');
            cont = new PortalCreditNoteListController();            
            
            ApexPages.currentPage().getParameters().put('sort_field','CreditNoteDates');
            cont = new PortalCreditNoteListController();            
            
            ApexPages.currentPage().getParameters().put('sort_field','CreditNoteCurrencies');
            cont = new PortalCreditNoteListController();            
            
            ApexPages.currentPage().getParameters().put('sort_field','CreditNoteTotals');
            cont = new PortalCreditNoteListController();            
            
            ApexPages.currentPage().getParameters().put('sort_field','PaymentsStatus');
            cont = new PortalCreditNoteListController();            
            
            ApexPages.currentPage().getParameters().put('sort_field','OutstandingAmounts');
            cont = new PortalCreditNoteListController();            
            
            ApexPages.currentPage().getParameters().put('sort_field','Companies');
            cont = new PortalCreditNoteListController();
            

            
    	}
    }
    
    
    public static User getPortalUser(PortalType portalType, User userWithRole, Boolean doInsert) {

        /* Make sure the running user has a role otherwise an exception
           will be thrown. */
        if(userWithRole == null) {  
            if(UserInfo.getUserRoleId() == null) {
                UserRole r = new UserRole(name = 'TEST ROLE ERP');
                Database.insert(r);
                userWithRole = new User(alias = 'hasrole', email='userwithrole@roletest1.com', userroleid = r.id,
                                    emailencodingkey='UTF-8', lastname='Testing', languagelocalekey='en_US', 
                                    localesidkey='en_US', profileid = UserInfo.getProfileId(), 
                                    timezonesidkey='America/Los_Angeles', username='userwithrole@testorg.com');
            } else {
                userWithRole = new User(Id = UserInfo.getUserId(), UserRoleId = UserInfo.getUserRoleId());
            }
            System.assert(userWithRole.userRoleId != null,
                          'This test requires the target org to have at least one UserRole created. Please create a user role in this organization and try again.');
        }
        Account a;
        Contact c;

        System.runAs(userWithRole) {
            a = UTIL_TestUtil.generateAccount();
            a.Credit_Card_Payments_Accepted__c = true;
            insert a;
            
            c = UTIL_TestUtil.createContact(a.Id);
            
            ERP_Credit_Note__c p = new ERP_Credit_Note__c(Account__c = a.Id,  Name = '12345', Total_Unallocated__c = 2000);
            insert p;
            
        }
        /* Get any profile for the given type.*/
        Profile p = [select id
                      from profile
                     where usertype = :portalType.name()
                     limit 1];  
        String testemail = 'puser000@amamama.com';
        User pu = new User(profileId = p.id, username = testemail, email = testemail,
                           emailencodingkey = 'UTF-8', localesidkey = 'en_US',
                           languagelocalekey = 'en_US', timezonesidkey = 'America/Los_Angeles',
                           alias='cspu', lastname='lastname', contactId = c.id);
        if(doInsert) {
            Database.insert(pu);
        }
        return pu;
    }
}