global class PortalDocumentFilterController {
    
    public String current_type_filter {get;set;}
    public String active_type_filter {get;set;}
    
    // Credit Notes / Cash Receipts / Invoices filter
    
    public List<SelectOption> gettype_filter_options(){
        
        List<SelectOption> type_filter_options = new List<SelectOption>();
        
        // Construct type filter
        type_filter_options.add(new SelectOption('sales_invoices','Invoices'));
        type_filter_options.add(new SelectOption('prepayorders','Prepay Orders'));
        type_filter_options.add(new SelectOption('credit_notes','Credit Notes'));
        type_filter_options.add(new SelectOption('cash_entries','Payments'));
        type_filter_options.add(new SelectOption('sales_receipts','Receipts'));
        
        active_type_filter = current_type_filter;
        
        return type_filter_options; 
    }
    
    
    
    public PageReference showDocuments(){
        // by default show the list of invoices
        
        PageReference doc_list_page = Page.CUST_portallistview;
        if(active_type_filter != current_type_filter){
            if(active_type_filter == 'cash_entries'){
                doc_list_page = Page.portalcashentrylist;
            }
            if(active_type_filter == 'credit_notes'){
                doc_list_page = Page.portalcreditnotelist;  
            }
            if(active_type_filter == 'sales_invoices'){
                doc_list_page = Page.CUST_portallistview; 
            }
            if(active_type_filter == 'sales_receipts') {
                doc_list_page = Page.receiptList; 
            }
            if(active_type_filter == 'prepayorders') {
                doc_list_page = Page.SI_opportunitylist; 
            }
        }       
        return doc_list_page;
        
        return this.resolvePageReference(false);
    }
    
    public PageReference showDocumentsFromBTN(){
        // by default show the list of invoices
        return this.resolvePageReference(true);     
    }
    
    private Pagereference resolvePageReference(boolean checkCurrent){
        if(active_type_filter != current_type_filter || checkCurrent == true){
            if(active_type_filter == 'cash_entries'){
                Pagereference pr = Page.portalcashentrylist;
                pr.setRedirect(true);
                return null;
            }
            if(active_type_filter == 'credit_notes'){
                Pagereference pr = Page.portalcreditnotelist;
                pr.setRedirect(true);
                return null;
            }
            if(active_type_filter == 'sales_invoices'){
                Pagereference pr = Page.CUST_portallistview;
                pr.setRedirect(true);
                return pr;
            }
            if(active_type_filter == 'prepayorders') {
              Pagereference pr = Page.SI_opportunitylist;
                pr.setRedirect(true);
                return pr;
            }
            if(active_type_filter == 'sales_receipts') {
              Pagereference pr = Page.receiptList;
                pr.setRedirect(true);
                return pr;
            }
        }
        Pagereference pr = ApexPages.currentPage();
        pr.setRedirect(true);
        return pr;  
    }
        

    static testMethod void testPortalInvoiceListController(){
       
            
        try
        {
            

            PortalDocumentFilterController cont = new PortalDocumentFilterController();
            
            cont.gettype_filter_options();
            cont.showDocuments();
            cont.showDocumentsFromBTN();                                    

            cont.active_type_filter = 'cash_entries';
            cont.showDocuments();
            cont.showDocumentsFromBTN();                                    
            cont.active_type_filter = 'credit_notes';
            cont.showDocuments();
            cont.showDocumentsFromBTN();                                    
            cont.active_type_filter = 'sales_invoices';
            cont.showDocuments();
            cont.showDocumentsFromBTN();  
            cont.active_type_filter = 'prepayorders';
            cont.showDocuments();
            cont.showDocumentsFromBTN();                                   
            
            System.assert(cont != null);
        }catch(Exception e){
            //This is intended
        }
    }   
}