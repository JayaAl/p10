public class PortalERPViewController {
    
    public PortalERPViewController(ApexPages.StandardController ctrl) {
        
    }
    public PageReference redirectToViewPDF() {
        String recordId = ApexPages.currentPage().getParameters().get('id');
        String recordCode  = recordId.subString(0,3); // geting first 3 digits
        String objectAPIName;
        Map<String, Schema.SObjectType> gDesc = Schema.getGlobalDescribe();
        for(Schema.SObjectType objInstance : gDesc.values()) {
            if(objInstance.getDescribe().getKeyPrefix() == recordCode) {
                objectAPIName = objInstance.getDescribe().getName();
            }
        }
        if(objectAPIName != null && objectAPIName.equalsIgnoreCase('erp_invoice__c')) {
            ERP_Invoice__c invoice = [Select Id, ERP_Invoice_Id__c, ERP_Org_Id__c, Name, INVOICE_DESCRIPTION__C from ERP_Invoice__c where Id =: recordId];
            
            String attachemtId = '/apex/CUST_ViewPDF?invoiceNum=' + invoice.Name + '&orgId=' + invoice.ERP_Org_Id__c + '&type=INV';
            
            if(invoice.INVOICE_DESCRIPTION__C == 'HISTORICAL RECORD'){
                // Override URL for historical archives
                attachemtId = '/' + invoice.Id + '?nooverride=1';
            }


            
            PageReference pg = new PageReference(attachemtId);
            pg.setRedirect(true);
            return pg;
        } else if(objectAPIName != null && objectAPIName.equalsIgnoreCase('erp_credit_note__c')) {
            ERP_Credit_Note__c invoice = [Select Id, ERP_Org_Id__c, Name from ERP_Credit_Note__c where Id =: recordId];
            String attachemtId = '/apex/CUST_ViewPDF?invoiceNum=' + invoice.Name + '&orgId=' + invoice.ERP_Org_Id__c + '&type=CM';
            PageReference pg = new PageReference(attachemtId);
            pg.setRedirect(true);
            return pg;
        }
        return null;
        
    }                 
}