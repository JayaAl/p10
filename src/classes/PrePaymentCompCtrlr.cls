/*
*@Author : AB
*@description : Controller for PrePayment Component used in Email template
*/
public with sharing class PrePaymentCompCtrlr{
    
    public String IOApprovalId ;
    public List<Receipt_Junction__c> juncObjLst {get;set;}
    public Decimal amountRemaining {get;set;}
    
    public void setIOApprovalId(String ioVar){
        IOApprovalId = ioVar;
        PrePaymentCompCtrlr();
    }
    
    public Id getIOApprovalId(){
        return IOApprovalId;
    }
    
    /*
    *@Author : AB
    *@description : Constructor for PrePaymentCompCtrlr
    */  
    public void PrePaymentCompCtrlr(){                
        amountRemaining = 0;        
        Receipt_Junction__c currentReceiptJuncObj = [Select Id,IO_Approval__c,IO_Approval__r.Total_IO_Amount__c from Receipt_Junction__c where id =: IOApprovalId Limit 1];
        juncObjLst = [Select Id,Name,Contract_End_Date__c,IO_Approval__r.Name,IO_Approval__r.Total_Paid_Amount_From_Portal__c,IO_Approval__r.Total_IO_Amount__c,Contract_Start_Date__c,Credit_Collections_Notes__c,Sales_Invoice_Outstanding_Amount__c,Scheduled_Payment_Amount__c,Scheduled_Payment_Date__c,IO_Approval__r.Opportunity__r.Slingshot_Order_ID__c,IO_Approval__r.Slingshot_ID__c,IO_Approval__r.PortalPaymentStatus__c from Receipt_Junction__c where (Amount__c = null OR Amount__c = 0 ) AND IO_Approval__c =: currentReceiptJuncObj.IO_Approval__c Order by Scheduled_Payment_Date__c ASC];
        
        if(juncObjLst != null && juncObjLst.size() != 0){
            Decimal paidAmount = juncObjLst[0].IO_Approval__r.Total_Paid_Amount_From_Portal__c != null ? juncObjLst[0].IO_Approval__r.Total_Paid_Amount_From_Portal__c : 0;
            Decimal totalIOAmount = juncObjLst[0].IO_Approval__r.Total_IO_Amount__c != null ? juncObjLst[0].IO_Approval__r.Total_IO_Amount__c : 0;     
            amountRemaining = totalIOAmount - paidAmount;
        }
        
        /*for(Receipt_Junction__c juncObj : juncObjLst){
            if(juncObj.Scheduled_Payment_Amount__c != null)
                amountRemaining  += juncObj.Scheduled_Payment_Amount__c;     
        }*/
    }   

  
}