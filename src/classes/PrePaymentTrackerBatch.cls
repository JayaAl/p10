global class PrePaymentTrackerBatch implements Database.Batchable<sObject>{
   Map<String,List<Receipt_Junction__c>> segregatedMap ;    
   List<Emailtemplate> emailTemplatelst ;
   global Database.QueryLocator start(Database.BatchableContext BC){
      string prev_days_cnt = '5';
      string next_days_cnt = '3';
      String paymentTypeStr = 'Scheduled Payment';
      system.debug('in start');
      // Receipt__c != null AND Amount__c = null AND Receipt__r.Payment_Type__c = \''+paymentTypeStr+'\' AND
      String query = 'Select id,name,CreatedById,IO_Approval__r.Sales_Representative_Email__c,IO_Approval__r.Opportunity__r.Primary_Billing_Contact__r.Email,IO_Approval__r.Opportunity__r.LEad_Campaign_Manager__c,Scheduled_Payment_Date__c,IO_Approval__r.Sales_Planner_email__c  from Receipt_Junction__c where (Amount__c = null OR Amount__c = 0 ) AND IO_Approval__r.PortalPaymentStatus__c != \'Paid\' AND Scheduled_Payment_Amount__c != null AND (Scheduled_Payment_Date__c = Last_n_days :'+next_days_cnt+' OR Scheduled_Payment_Date__c = next_n_days : '+prev_days_cnt+' )' ;  
      system.debug('in start'+query);
      system.debug('in start'+Database.getQueryLocator(query));
      return Database.getQueryLocator(query);
   }

   // 05 Records -> R1=>1,R2=>3,R3=>5,R4=>-3,R5=-3

   global void execute(Database.BatchableContext BC, List<Receipt_Junction__c> scope){
     system.debug('scope ==> '+scope);
     segregatedMap = new Map<String,List<Receipt_Junction__c>>();
     String prev_3_days_str = 'prev_3_days';
     String prev_5_days_str = 'prev_5_days';
     String next_3_days_str = 'next_3_days';
     String senderDispName = 'Salesforce Admin';

     List<String> keyWordLst = new List<String>{prev_3_days_str,prev_5_days_str,next_3_days_str};
     
     system.debug(scope);

     for(Receipt_Junction__c receiptJunctionVar : scope){
        if(receiptJunctionVar.Scheduled_Payment_Date__c.daysBetween(Date.Today()) == -5){ // 5 days
            system.debug('5 days');
            buildMap(prev_5_days_str,receiptJunctionVar); 
        }else if(receiptJunctionVar.Scheduled_Payment_Date__c.daysBetween(Date.Today()) == -1){ // 1 day
            system.debug('1 days');
            buildMap(prev_3_days_str,receiptJunctionVar);
        }else if(receiptJunctionVar.Scheduled_Payment_Date__c.daysBetween(Date.Today()) == 3){ // after 3 days
            system.debug('next 3 days');
            buildMap(next_3_days_str,receiptJunctionVar);
        }
     }

     system.debug('segregatedMap : '+segregatedMap);
     
     /*
     * prev_3_days => R1,R2
     * prev_5_days => R3
     * next_3_days => R4,R5
     */

      
      List<Messaging.SingleEmailMessage> mails = new List<Messaging.SingleEmailMessage>();    
      for(String key : keyWordLst){

          if(segregatedMap.containsKey(key)){
          for (Receipt_Junction__c juncObj  : segregatedMap.get(key)) {       
            // Step 1: Create a new Email
            
            String templateNameStr = '';        
            String externalTemplateStr = '';
            List<String> ccaddressLst = new List<String>();

            if(key==prev_3_days_str){

              if(juncObj.IO_Approval__r.Sales_Planner_email__c != '' && juncObj.IO_Approval__r.Sales_Planner_email__c != null) 
                ccaddressLst.add(juncObj.IO_Approval__r.Sales_Planner_email__c);
              if(juncObj.IO_Approval__r.Sales_Representative_Email__c != '' && juncObj.IO_Approval__r.Sales_Representative_Email__c != null) 
                ccaddressLst.add(juncObj.IO_Approval__r.Sales_Representative_Email__c);

              templateNameStr = 'Prepayment_Scheduled_Payment_Tomorrow_VF';
              externalTemplateStr = 'External_Prepayment_Tomorrow_VF';

            }else if(key==prev_5_days_str){

                // internal email brfore 5 days
                templateNameStr = 'Prepayment_Scheduled_Payment_5_Days_VF';
                externalTemplateStr = 'Ext_Prepayment_Schedule_Payment_5_Day_VF';
                 
                if(juncObj.IO_Approval__r.Sales_Planner_email__c != '' && juncObj.IO_Approval__r.Sales_Planner_email__c != null) 
                  ccaddressLst.add(juncObj.IO_Approval__r.Sales_Planner_email__c);
                if(juncObj.IO_Approval__r.Sales_Representative_Email__c != '' && juncObj.IO_Approval__r.Sales_Representative_Email__c != null) 
                  ccaddressLst.add(juncObj.IO_Approval__r.Sales_Representative_Email__c);

            }else if(key==next_3_days_str){

                templateNameStr = 'Prepayment_Not_Received_VF';
                
               
                if(juncObj.IO_Approval__r.Sales_Planner_email__c != '' && juncObj.IO_Approval__r.Sales_Planner_email__c != null) 
                  ccaddressLst.add(juncObj.IO_Approval__r.Sales_Planner_email__c); // Sales Team
                if(juncObj.IO_Approval__r.Sales_Representative_Email__c != '' && juncObj.IO_Approval__r.Sales_Representative_Email__c != null) 
                  ccaddressLst.add(juncObj.IO_Approval__r.Sales_Representative_Email__c); // Sales Team

            }                     
            

            id targetObjId = (juncObj.IO_Approval__r.Opportunity__r.LEad_Campaign_Manager__c != null) ? juncObj.IO_Approval__r.Opportunity__r.LEad_Campaign_Manager__c : juncObj.CreatedById;

            
              
            if(templateNameStr != ''){
                // Removing the AR Team email id list as per revised Requirements
                /*for(String strVar : System.Label.AR_Team_EmailId_lst.Split(',')){             // AR Team
                  if(strVar != null)
                    ccaddressLst.add(strVar);
                }*/
                
                Messaging.SingleEmailMessage singleEmail = EmailUtil.generateEmail(juncObj.Id,senderDispName,getTemplateId(templateNameStr),ccaddressLst,targetObjId);  
                mails.add(singleEmail);
            } 
            
            if(externalTemplateStr != ''){
                if(juncObj.IO_Approval__r.Opportunity__r.Primary_Billing_Contact__c != null )
                ccaddressLst.add(juncObj.IO_Approval__r.Opportunity__r.Primary_Billing_Contact__r.Email);
                Messaging.SingleEmailMessage extSingleEmail = EmailUtil.generateEmail(juncObj.Id,senderDispName,getTemplateId(externalTemplateStr),ccaddressLst,targetObjId);  
                mails.add(extSingleEmail);
            }

         }

        }
      }

      system.debug('Mails ==> '+mails);
      if(!mails.isEmpty()  && !Test.isRunningTest())
        EmailUtil.sendEmail(mails);
      
    }   

   private void buildMap(String keyStr, Receipt_Junction__c receiptJunctionVar ){
        if(segregatedMap.containskey(keyStr)){
            segregatedMap.get(keyStr).add(receiptJunctionVar);  
        }else{
            segregatedMap.put(keyStr, new List<Receipt_Junction__c>{receiptJunctionVar});   
        }
   }

   private Id getTemplateId(String templateDevName){

        if(emailTemplatelst == null){
            emailTemplatelst = [SELECT DeveloperName,Id,Name FROM EmailTemplate where Folder.DeveloperName = 'IO_Approval' Limit 50000]; // Add folder condition             
        }

        for(EmailTemplate templateVar : emailTemplatelst){
            if((templateVar.DeveloperName).equals(templateDevName)){
                return templateVar.Id;                  
            }
        }
        return null;
   }

   global void finish(Database.BatchableContext BC){
   }
}