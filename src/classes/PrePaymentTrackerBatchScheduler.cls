/**
* ─────────────────────────────────────────────────────────────────────────────────────────────────┐
* PrePaymentTrackerBatchhScheduler: schedules PrePaymentTrackerBatch.
* ──────────────────────────────────────────────────────────────────────────────────────────────────
* @author         Abhishek Bidap
* @maintainedBy   Abhishek Bidap
* @version        1.0
* @created        2017-06-15
* @modified       
* @systemLayer    Service
* @see            ????
* @see            ????
* ──────────────────────────────────────────────────────────────────────────────────────────────────
* @changes
* vX.X            Abhishek
* Y  CaseExpiration batch checks the lastmodifeddate of cases and sends email and closes cases based upon the requirment
* ─────────────────────────────────────────────────────────────────────────────────────────────────┘
*/
global class PrePaymentTrackerBatchScheduler implements Schedulable 
{
    global void execute(SchedulableContext sc) 
    {
        PrePaymentTrackerBatch batch = new PrePaymentTrackerBatch(); 
        database.executebatch(batch);
    }
}