@isTest(seeAllData = false)
public class PrePaymentTrackerBatchTest{
    static Account testAccount;
    static Contact testContact;
    static Opportunity testOppty;
    static IO_Detail__c testIOApproval;
    static Receipt_Junction__c testReceiptJunc;
    static Receipt__c testReceipt;

    public static void setupdata(){

      testAccount = UTIL_TestUtil.createAccount();
      testContact = UTIL_TestUtil.createContact(testAccount.id); 
      testOppty = UTIL_TestUtil.createOpportunity(testAccount.id);
      testOppty.LEad_Campaign_Manager__c = UserInfo.getUserId();
      update testOppty;
        
      testIOApproval = new IO_Detail__c();
      testIOApproval.Opportunity__c = testOppty.Id;
      testIOApproval.Paperwork_Origin__c = 'Pandora Paperwork - no changes';
      insert testIOApproval;

      testReceipt = new Receipt__c();
      testReceipt.Account__c = testAccount.id;
      testReceipt.Payment_Type__c = 'Scheduled Payment';      
      testReceipt.Type__c = 'Prepay Order';
      testReceipt.Date_Of_Transaction__c = Date.Today() + 1;
      testReceipt.IsPaymentSuccess__c = true;

      insert testReceipt;

      testReceiptJunc = new Receipt_Junction__c();
      testReceiptJunc.Receipt__c = testReceipt.id;      
      testReceiptJunc.IO_Approval__c = testIOApproval.Id;
      testReceiptJunc.Opportunity__c = testOppty.id;
      testReceiptJunc.Scheduled_Payment_Date__c = Date.Today();
      testReceiptJunc.Scheduled_Payment_Amount__c = 35;

      insert testReceiptJunc;      

    }

    private static testmethod void BatchTestPositive_prev_5_days(){

        setupdata();

        testReceiptJunc.Scheduled_Payment_Date__c = Date.Today() + 5;
        update testReceiptJunc;
        Test.starttest();
            
            ID batchprocessid = Database.executeBatch(new PrePaymentTrackerBatch());            
            system.assert([SELECT Id, Status, JobItemsProcessed, TotalJobItems, NumberOfErrors 
                    FROM AsyncApexJob WHERE ID =: batchprocessid ][0].NumberOfErrors == 0);
        Test.stopTest();
    }


    private static testmethod void BatchTestPositive_prev_1_days(){

        setupdata();
        
        testReceiptJunc.Scheduled_Payment_Date__c = Date.Today() + 1;
        update testReceiptJunc;
        Test.starttest();
          ID batchprocessid = Database.executeBatch(new PrePaymentTrackerBatch());
          system.assert([SELECT Id, Status, JobItemsProcessed, TotalJobItems, NumberOfErrors 
                    FROM AsyncApexJob WHERE ID =: batchprocessid ][0].NumberOfErrors == 0);
        Test.stopTest();
    }


    private static testmethod void BatchTestPositive_next_3_days(){

        setupdata();
        
        testReceiptJunc.Scheduled_Payment_Date__c = Date.Today() - 3;
        update testReceiptJunc;
        Test.starttest();
            ID batchprocessid = Database.executeBatch(new PrePaymentTrackerBatch());
            system.assert([SELECT Id, Status, JobItemsProcessed, TotalJobItems, NumberOfErrors 
                    FROM AsyncApexJob WHERE ID =: batchprocessid ][0].NumberOfErrors == 0);
        Test.stopTest();
    }

    private static testmethod void testReceiptJunctionHandler(){
        setupdata();        
        testReceiptJunc.Amount__c = 35;
        update testReceiptJunc;

        testReceiptJunc = new Receipt_Junction__c();
        testReceiptJunc.Receipt__c = testReceipt.id;      
        testReceiptJunc.IO_Approval__c = testIOApproval.Id;
        testReceiptJunc.Opportunity__c = testOppty.id;
        testReceiptJunc.Scheduled_Payment_Date__c = Date.Today();
        testReceiptJunc.Scheduled_Payment_Amount__c = 35;
        testReceiptJunc.Amount__c = 35;
        insert testReceiptJunc;      
    }
    
    private static testMethod void testPrepaymentCOmController(){
        setupdata();        
        PrePaymentCompCtrlr ctrlr = new PrePaymentCompCtrlr();
        ctrlr.IOApprovalId = testIOApproval.Id;
        ctrlr = new PrePaymentCompCtrlr();
    }
    
    public static testmethod void testScheduler(){
         setupdata();
         Test.starttest();
         PrePaymentTrackerBatchScheduler sh1 = new PrePaymentTrackerBatchScheduler();      
         String sch = '0  00 1 3 * ?';
       system.schedule('Test', sch, sh1);
       test.stopTest();
    }
}