public with sharing class PreviewPageController {

	public PageReference preview() {
		//find the root folder this page belongs to
		String pageid = System.currentPageReference().getParameters().get('pageid');
		String console = System.currentPageReference().getParameters().get('console');
		String fullscreen = System.currentPageReference().getParameters().get('fullscreen');
		
		// Internal Wall Update
		// - direct user to internal version of page rather than site version
		String previewurl = '/apex/page?pageid=' + pageid;
		
		//if we're in the console, pass on the console = true param and provide the callback edit url from sites to the org		
		if(console == 'true') {
			String sfdcorg = '';
			if(CMSForceDomain__c.getAll().get('cmsforcedomain') != null) sfdcorg = CMSForceDomain__c.getAll().get('cmsforcedomain').Url__c;			
			previewurl = previewurl + '&console=true&pod='+Encodingutil.urlEncode(sfdcorg, 'UTF-8');
		}
		//if we're also in fullscreen preview, pass that as well
		if(fullscreen=='true') {
			previewurl = previewurl+ '&fullscreen=true';
		}
		//add a random integer var to avoid the page being pulled from the browser or proxy caches
		previewurl = previewurl + '&nocache=' + Encodingutil.urlEncode(Math.random().format(), 'UTF-8');
		return new PageReference(previewurl);
	}


	 

}