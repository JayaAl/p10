@isTest
private class PreviewPageControllerTest {
	
	/** TESTS**/
	private static testMethod void t1() {
		Site site = [select Id from Site limit 1];
		String siteid = site.Id;		
		//set up page and template folders
		CMSFolder__c pagefolder = new CMSFolder__c(Site_Id__c = siteid.substring(0,15),Type__c = 'Page', Name='test');
		insert pagefolder;
		CMSFolder__c templatefolder = new CMSFolder__c(Type__c = 'PageTemplate', Name='test');
		insert templatefolder;
		//set up a test template
		PageTemplate__c template = new PageTemplate__c(Name='test', VisualForce_Page_Name__c='TestTemplate', Folder__c = templatefolder.Id);
		insert template;
		//set up a test page
		Page__c pg = new Page__c(Name='testpage', PageTemplate__c = template.Id, Folder__c = pagefolder.Id);
		insert pg;
		
		Test.setCurrentPage(Page.PreviewPage);
		ApexPages.currentPage().getParameters().put('pageid',pg.Id);
		ApexPages.currentPage().getParameters().put('console','true');
		ApexPages.currentPage().getParameters().put('fullscreen','true');
		PreviewPageController ppc = new PreviewPageController();
		System.assert(ppc.preview() != null);
		
	}

}