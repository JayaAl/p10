/**
* ─────────────────────────────────────────────────────────────────────────────────────────────────┐
* Opportunity Related Products Util
* 
* This class is servr side controler util for Related products on
*  opportunity in lightning mode.
* Refered in relatedlistDisplay.cls.
* ──────────────────────────────────────────────────────────────────────────────────────────────────
* @author         Jaya Alaparthi
* @maintainedBy   Jaya Alaparthi
* @version        1.0
* @created        2017-01-30
* @modified       YYYY-MM-DD
* @systemLayer    Util
* @see            ????
* @see            ????
* ──────────────────────────────────────────────────────────────────────────────────────────────────
* @changes
* vX.X            Jaya Alaparthi
* YYYY-MM-DD      Explanation of the change.  Multiple lines can be used to explain the change, but
*                 each line should be indented till left aligned with the previous description text.
*
* vX.X            Jaya Alaparthi
* YYYY-MM-DD      Each change to this file should be documented by incrementing the version number,
*                 and adding a new entry to this @changes list. Note that there is a single blank
*                 line between each @changes entry.
* ─────────────────────────────────────────────────────────────────────────────────────────────────┘
*/
public with sharing class ProductOpportunityRelatedUtil {
	
	public static String AUDIO_EVERYWHERE = 'Audio Everywhere';
	public static String PB_ONE = 'Pandora One%';
	public static String PB_STANDARD = 'Standard Price Book';
	public static String OPPTY_RT_P1SUB = 'Opportunity_Pandora_One_Subs';

	//─────────────────────────────────────────────────────────────────────────┐
	// validateOpptyRecordType: validate Opportunity based of 
	//							TaxonomyRecordTypes custom settings 
	//							and return PBE.
	// @param oppty  opportunity 
	// @return PricebookEntry
    //─────────────────────────────────────────────────────────────────────────┘
	public static PricebookEntry validateOpptyRecordType(Opportunity oppty) {

		String opptyRT;
		Boolean opptyFlag = false;
		PricebookEntry pbeRec = new PricebookEntry();
		System.debug('oppty:'+oppty);
		oppty = getOpportunity(oppty.Id);
		if(oppty.Id != null && String.valueOf(oppty.Id).startsWith('006')) {

			System.debug('oppty.RecordType.DeveloperName:'+oppty.RecordType.DeveloperName);
			for(TaxonomyRecordTypes__c t:TaxonomyRecordTypes__c.getAll().values()) {

				System.debug('t : '+t.Name+'opptyRecordType:'+oppty);
				if(oppty.RecordType.DeveloperName.equalsIgnoreCase(t.Name)
					&& !opptyFlag) {
					// take the record type and get the PBE
					//opptyRT = oppty.RecordType;
					opptyFlag = true;
				}        	
        	}
        }
        // get respective PBE Id 
        if(opptyFlag) {
        	pbeRec = getPBE(oppty);
        }
        System.debug('pbdRec:'+pbeRec);
        return pbeRec;
	}

	public static Opportunity getOpportunity(Id opptyId) {

		Opportunity oppty = new Opportunity();
		oppty = [SELECT Name,Amount,OwnerId,AccountId,
						Probability, CurrencyIsoCode, 
						Splits_Locked__c, ContractStartDate__c, 
						RecordType.DeveloperName, RecordType.Name 
				FROM Opportunity
				WHERE Id =: opptyId];
		return (oppty);
	}
	//─────────────────────────────────────────────────────────────────────────┐
	// getPBE: retrive respective PBE Id based on the opportunity record type.
	// @param oppty  opportunity 
	// @return PricebookEntry
    //─────────────────────────────────────────────────────────────────────────┘
	private static PricebookEntry getPBE(Opportunity oppty) {

		list<PricebookEntry> pbeList = new list<PricebookEntry>();
		PricebookEntry pbeObj = new PricebookEntry();
		String queryString = 'SELECT Id, Name, UnitPrice '
                			+' FROM PricebookEntry '
                			+' WHERE IsActive=true '
                			+' AND CurrencyIsoCode ='+'\''+oppty.CurrencyIsoCode+'\'';

        if(!Test.isRunningTest()) {

                queryString += ' AND Pricebook2.Name ='+'\''+ PB_STANDARD+'\'';
        }
        if(oppty.RecordType.DeveloperName.equalsIgnoreCase(OPPTY_RT_P1SUB)) {  // Modified by Lakshman on 2014-04-23 to fix multiple products issue for Pandora One Subs opps

			queryString += ' AND Name Like : PB_ONE';
        } else {

        	queryString += ' AND ( NOT Name LIKE : PB_ONE )'
						+' AND Product2.Auto_Schedule__c = true';
        }
        
        if(Test.isRunningTest()) {

        	queryString += ' limit 1';
        } else {

        	queryString  += ' ORDER BY Name desc ';
        }
        System.debug('queryString:'+queryString);
        pbeList = Database.query(queryString);
        if(!pbeList.isEmpty()) {
        	for(PricebookEntry pbe:pbeList) {
        		if(pbe.Name.equalsIgnoreCase(AUDIO_EVERYWHERE)) {
        			pbeObj = pbe;
        		}
        	}
        }
        return pbeObj;
    }
}