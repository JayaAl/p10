public class RSSNewsReader {
     
    public String rssQuery {get;set;}
    private String rssURL {get;set;}
    
         
    public RSSNewsReader(ApexPages.StandardController controller) {
         
        rssURL = 'http://news.google.com/news?pz=1&cf=all&ned=us&hl=en&output=rss&num=20&q=';
        rssQuery = [Select Name from Account where Id=: ApexPages.currentPage().getParameters().get('id')].Name; //default on load
         
    }
     
    public RSS.channel getRSSFeed() {
        return RSS.getRSSData(rssURL + EncodingUtil.urlEncode(rssQuery,'UTF-8'));
    }
     
    static testMethod void RSSNewsReaderTest() {
        Account testAcc = new Account(Name = 'Test Account');
        insert testAcc;
        ApexPages.StandardController sc = new ApexPages.StandardController(testAcc);
        ApexPages.currentPage().getParameters().put('id', testAcc.Id);
        RSSNewsReader con = new RSSNewsReader(sc);
        RSS.channel rssFeed = con.getRSSFeed();
    }
 
}