public class ReceiptJuncTriggerHandlerHelper {
    public static List<Emailtemplate> emailTemplatelst ;
    
    public static void assignReceiptByUser(List<Receipt_Junction__c> newJuncLst, Map<Id,Receipt_Junction__c> oldReceiptJuncMap){
        Boolean executeLogic;
        system.debug('inside assignReceiptByUser');
        for(Receipt_Junction__c juncObj : newJuncLst){
            Receipt_Junction__c oldRecord = (oldReceiptJuncMap != null && oldReceiptJuncMap.containsKey(juncObj.Id)) ? oldReceiptJuncMap.get(juncObj.Id) : null;
            executeLogic = false;
            if(juncObj.Amount__c != null && juncObj.Amount__c > 0 ){
                system.debug('inside assignReceiptByUser juncObj'+juncObj);
                if(oldRecord != null){
                    executeLogic = (juncObj.Amount__c != oldRecord.Amount__c) ? true:false;  // after update, if Amount is updated
                }else{
                    executeLogic = true; // after insert
                }
                system.debug('executeLogic ::'+executeLogic);
                if(executeLogic){
                    juncObj.Receipt_By__c = UserInfo.getUserId();           
                }               
            }
        }

        system.debug(newJuncLst);
    }

    public static void sendNotificationOnPaymentReceipt(List<Receipt_Junction__c> newJuncLst,Map<Id,Receipt_Junction__c> oldReceiptJuncMap){
        
        String senderDispName = 'Salesforce Admin';
        String externalTemplateStr = 'Payment_receipt_notification';
        List<Messaging.SingleEmailMessage> mails = new List<Messaging.SingleEmailMessage>();
        Set<Id> IO_DetailIdSet = new Set<id>();     
        Map<id,Id> io_CS_Map = new Map<id,Id>();
        
        system.debug('inside sendNotificationOnPaymentReceipt '+newJuncLst+'oldReceiptJuncMap :::'+oldReceiptJuncMap);

        Boolean executeLogic;
        for(Receipt_Junction__c juncObj : newJuncLst){
            Receipt_Junction__c oldRecord = (oldReceiptJuncMap != null && oldReceiptJuncMap.containsKey(juncObj.Id)) ? oldReceiptJuncMap.get(juncObj.Id) : null;
            system.debug('inside sendNotificationOnPaymentReceipt oldRecord ::'+oldRecord);
            executeLogic = false;
            if(juncObj.Amount__c != null && juncObj.Amount__c > 0 ){
                
                if(oldRecord != null){
                    executeLogic = (juncObj.Amount__c != oldRecord.Amount__c) ? true:false;  // after update, if Amount is updated
                }else{
                    executeLogic = true; // after insert
                }

                if(executeLogic){
                    system.debug('Condn satified');
                    IO_DetailIdSet.add(juncObj.IO_Approval__c);                     
                }               
            }
        }

        if(IO_DetailIdSet != null && !IO_DetailIdSet.isEmpty()){
            for(IO_Detail__c IOObj : [Select id,name,Sales_Planner_email__c,Opportunity__c,Sales_Representative_Email__c,Opportunity__r.LEad_Campaign_Manager__c from IO_Detail__c where id in: IO_DetailIdSet]){
                if(IOObj.Opportunity__c != null && IOObj.Opportunity__r.LEad_Campaign_Manager__c != null)
                    io_CS_Map.put(IOObj.id,IOObj.Opportunity__r.LEad_Campaign_Manager__c);
            }   
        }
            

        if(io_CS_Map != null && io_CS_Map.values().size() != 0){

            for(Receipt_Junction__c juncObj : newJuncLst){
            
                List<String> ccaddressLst = new list<String>();             
                
                
                Id targetObjId = (io_CS_Map.containsKey(juncObj.IO_Approval__c))? io_CS_Map.get(juncObj.IO_Approval__c) : juncObj.CreatedById;
                                
                Messaging.SingleEmailMessage extSingleEmail = EmailUtil.generateEmail(juncObj.Id,senderDispName,getTemplateId(externalTemplateStr),ccaddressLst,targetObjId);  
                mails.add(extSingleEmail);          
            }   
        }
         
        
        system.debug('Mails ==> '+mails);
        if(!mails.isEmpty() && !Test.isRunningTest())
          EmailUtil.sendEmail(mails);
    }

    public static Id getTemplateId(String templateDevName){

        if(emailTemplatelst == null){
            emailTemplatelst = [SELECT DeveloperName,Id,Name FROM EmailTemplate where Folder.DeveloperName = 'IO_Approval' Limit 50000]; // Add folder condition             
        }

        for(EmailTemplate templateVar : emailTemplatelst){
            if((templateVar.DeveloperName).equals(templateDevName)){
                return templateVar.Id;                  
            }
        }
        return null;
   }
}