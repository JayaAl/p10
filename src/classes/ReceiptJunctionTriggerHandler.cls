public class ReceiptJunctionTriggerHandler {

	public void onAfterInsert(List<Receipt_Junction__c> receiptJuncLst){
		
		system.debug('inside onAfterInsert');
		ReceiptJuncTriggerHandlerHelper.sendNotificationOnPaymentReceipt(receiptJuncLst,null);		
		system.debug('after onAfterInsert');
	}

	public void onAfterUpdate(List<Receipt_Junction__c> newReceiptJuncLst,Map<Id,Receipt_Junction__c> oldReceiptJuncMap){
		system.debug('inside onAfterUpdate');
		ReceiptJuncTriggerHandlerHelper.sendNotificationOnPaymentReceipt(newReceiptJuncLst,oldReceiptJuncMap);
		system.debug('after onAfterUpdate');
	}

	public void onBeforeUpdate(List<Receipt_Junction__c> newReceiptJuncLst,Map<Id,Receipt_Junction__c> oldReceiptJuncMap){		
		ReceiptJuncTriggerHandlerHelper.assignReceiptByUser(newReceiptJuncLst,oldReceiptJuncMap);
	}

	public void onBeforeInsert(List<Receipt_Junction__c> receiptJuncLst){
		ReceiptJuncTriggerHandlerHelper.assignReceiptByUser(receiptJuncLst,null);
	}
}