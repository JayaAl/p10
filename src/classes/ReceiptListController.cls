public class ReceiptListController {
  public CUST_PortalUtils FLS{get{return new CUST_PortalUtils();}set;}

    // List view
    public List<PortalReceiptView> receiptListView {get; set;}
    public Id accountFilterId {get; set;}
    public String accountFilterName {get; set;}
    public String selected_account_id {get;set;}    
    public Boolean no_records_available {get;set;}
    
    // The list of accounts to filter 
    private List<Account> accounts_related = new List<Account>();
    public Boolean multiple_accounts {get;set;}
    
    public String defaultAmount {get;set;}
    
    // Set controller (handle more records)
    public ApexPages.StandardSetController setController {get;set;} 
    private String soql {get;set;}
    private String base_soql {get;set;}
    
    public String currentUserId;
     
    // Pagination Methods for the set controller
    public Integer PAGE_SIZE = 25;
    
    public void GoPrevious(){ setController.previous(); }
    public void GoNext(){ setController.next(); }
    public void GoLast(){ setController.last(); }
    public  void GoFirst(){ setController.first(); }
    public Boolean getRenderPrevious(){return setController.getHasPrevious();}
    public Boolean getRenderNext(){return setController.getHasNext();}
    public Integer getRecordSize(){return setController.getResultSize();}
    public Integer getPageNumber(){return setController.getPageNumber();}
    public boolean has_pages {get;set;}
    
    // Static vars, needed inside of the receipt inner class
    public static String current_filter_static {get;set;}
    public static String current_page_static {get;set;}
    public static String current_sort_order_static {get;set;}
    public static String current_sort_field_static {get;set;}
    public String paymentPageURL {get;set;}
    


    public CUST_PortalUtils filters{get;set;}
    private String filters_extraString_for_soql = '';
    private String filters_saveSortOrder = '';
    private Boolean filters_setOneTime_sort = false;
    public void processFilters(){
        this.filters.receiveFilters();
        this.filters_extraString_for_soql = this.buildFiltersStringForSoql();
        this.filters_setOneTime_sort = true;
        this.filterReceipts();
    }
    private String buildFiltersStringForSoql(){
        String s = ' ';
        //Filters by column:
        if(this.filters.selectionInList != null && !this.filters.selectionInList.equals('all')){
            String st = this.filters.selectionInList;
            if(st.equals('invoice')){
                s += 'AND Type__c = \'' + 'Sales Invoice' + '\' ';
            }else if(st.equals('prepayorder')){
                s += 'AND Type__c = \'' + 'Prepay Order' + '\' ';
            }
        }
        
        if(s.length() > 1){
            s = ' ' + s.trim();
        }else{
            s = '';
        }

        return s;
    }
    
    public String debugSt{get;set;}
    

    

    public Boolean page_validation{get;set;}
   
    
    public Integer getTotalPages(){
        double div = (setController.getResultSize())/double.valueOf(String.valueOf(PAGE_SIZE));
        if (div > div.intValue()) {
          div = div + 1 ;          
        }           
        return div.intValue();
    }
    
    // Constructor
    public ReceiptListController()
    {

        // +++++++++++++++++++++++++    
        // CHECK USER PRIVILEGES TO ACCESS:
        this.page_validation = true;
        //if(!PortalActivateMyAccountController.CHECK_FFPORTAL_PRIVILEGES(UserInfo.getUserId())){
        //    this.page_validation = false;
        //    return;
        //}
        // +++++++++++++++++++++++++    

            
        this.debugSt = '';

        
        //this.filter_notAllowedRecords = ReceiptListController.notAllowedRecords();
        
        
        no_records_available = false;
        sortedByUrl = false;
        paymentPageURL = '';
        

        // Set On Page document filters:
        this.filters = new CUST_PortalUtils();
        this.filters.filterByList = CUST_PortalUtils.getFiltersList3();
        this.filters.selectionInList = 'all';
        // Load Extra Filters:
        if(ApexPages.currentPage().getParameters().get('fby') != null){
            this.filters.selectionInList = ApexPages.currentPage().getParameters().get('fby');
        }
        if(ApexPages.currentPage().getParameters().get('fbyn1') != null){
            this.filters.filterByNumber_start = ApexPages.currentPage().getParameters().get('fbyn1');
        }
        if(ApexPages.currentPage().getParameters().get('fbyn2') != null){
            this.filters.filterByNumber_end = ApexPages.currentPage().getParameters().get('fbyn2');
        }
        if(ApexPages.currentPage().getParameters().get('fbyd1') != null){
            this.filters.filterByDate_start = ApexPages.currentPage().getParameters().get('fbyd1');
        }
        if(ApexPages.currentPage().getParameters().get('fbyd2') != null){
            this.filters.filterByDate_end = ApexPages.currentPage().getParameters().get('fbyd2');
        }
        this.filters_extraString_for_soql = this.buildFiltersStringForSoql();
        // ------------------------------
        
        
        
        
        currentUserId = UserInfo.getUserId();        
        String extra_filters = this.filters_extraString_for_soql;       
        // Base SOQL sentence
        soql = 'select Account__c, Authorization__c, Date_Of_Transaction__c,IsSuccess__c, Total_Amount__c, Type__c, Name, Id, (select Amount__c, Contract_End_Date__c, Contract_Start_Date__c, Opportunity__c, Opportunity_Name__c, Receipt__c, Receipt__r.Type__c, Sales_Invoice_Name__c, Sales_Invoice_Status__c, ERP_Invoice__r.Name, ERP_Invoice__r.Outstanding_Value__c, ERP_Invoice__r.Invoice_Status__c, Sales_Invoice_Outstanding_Amount__c, IO_Approval__r.Total_IO_Amount__c, IO_Approval__r.Total_Paid_Amount_From_Portal__c from Receipt_Junctions__r) from Receipt__c where Customer_Portal_User__c = :currentUserId AND IsSuccess__c = true ';
        base_soql = soql;
                    
        if(ApexPages.currentPage().getParameters().get('filter') != null){
            selected_account_id = ApexPages.currentPage().getParameters().get('filter');
            filterReceipts();
        }
    ApexPages.currentPage().getParameters().put('sort_field', 'TransactionDates');
    ApexPages.currentPage().getParameters().put('sort_order', 'desc');
        if(ApexPages.currentPage().getParameters().get('sort_field') != null){
            String sort_field = ApexPages.currentPage().getParameters().get('sort_field');
            if(sort_field == 'ReceiptNumbers'){ sortReceiptNumbers(); }
            if(sort_field == 'TransactionDates'){ sortTransactionDates(); }
            if(sort_field == 'TransactionTotals'){ sortTransactionTotals(); }
        } else {
            queryReceipts();
        }
        
        if(ApexPages.currentPage().getParameters().get('p') != null){
            setController.setpageNumber(integer.valueOf(ApexPages.currentPage().getParameters().get('p')));
        }

    }
        
    
    
    /*
    * Filter Receipts.
    *
    */
    public void filterReceipts(){

        String extra_filters = this.filters_extraString_for_soql;               
        base_soql = 'select Account__c, Authorization__c, Date_Of_Transaction__c,IsSuccess__c, Total_Amount__c, Type__c, Name, Id, (select Amount__c, Contract_End_Date__c, Contract_Start_Date__c, Opportunity__c, Opportunity_Name__c, Receipt__c, Receipt__r.Type__c, Sales_Invoice_Name__c, Sales_Invoice_Status__c, ERP_Invoice__r.Name, ERP_Invoice__r.Outstanding_Value__c, ERP_Invoice__r.Invoice_Status__c, Sales_Invoice_Outstanding_Amount__c, IO_Approval__r.Total_IO_Amount__c, IO_Approval__r.Total_Paid_Amount_From_Portal__c from Receipt_Junctions__r) from Receipt__c where Customer_Portal_User__c = :currentUserId AND IsSuccess__c = true ' + extra_filters + ' ';

        if(this.filters_setOneTime_sort == true){
            this.filters_setOneTime_sort = false;
            base_soql += this.filters_saveSortOrder;
        }
        
        soql = base_soql;
        
        queryReceipts();
    }
    
    /*
    * Do a query to the list of receipts.
    */
    public void queryReceipts(){
        this.setController = new ApexPages.StandardSetController(Database.getQueryLocator(soql));
        
        this.setController.setPageSize(PAGE_SIZE);
        
        if(getTotalPages() > 1){
            has_pages = true;
        } else {
            has_pages = false;
        }
        
        if(this.setController.getResultSize() > 0){
            no_records_available = false;
        } else {
            no_records_available = true;
        }
        
    }
    
    // Sorting
    public String previousSortField {get;set;}  
    public String sortOrder {get;set;}
    public Boolean sortedByUrl {get;set;}

    private Boolean setReOrder = false;
        
    public void doSort(String soql_query, String sortField){
        
        // The first time we sort, if not set previousSortField and the order comes through parameters, avoid the sorting change.
        if(!sortedByUrl && ApexPages.currentPage().getParameters().get('sort_order') != null){
            sortOrder = ApexPages.currentPage().getParameters().get('sort_order');
            previousSortField = sortField;
            sortedByUrl = true;
        } else {
            sortOrder = 'asc';
            
            if(setReOrder == true){
                previousSortField = '-';
                setReOrder = false;
            }
            
            if(previousSortField == sortField){
                sortOrder = 'desc';
                setReOrder = true;
            }else{
                previousSortField = sortField;
            }
        }
                        
        soql = soql_query+sortOrder;
        
        this.filters_saveSortOrder += sortOrder;
        
        queryReceipts();
    }
    
    
    
    public void sortReceiptNumbers() {
        this.removeCurentSortOrder();
        String soql_query = base_soql+'order by Name ';
        this.filters_saveSortOrder = 'order by Name ';
        doSort(soql_query,'ReceiptNumbers');
    }
    
    public void sortTransactionDates() {
        this.removeCurentSortOrder();
        String soql_query = base_soql+'order by Date_Of_Transaction__c ';
        this.filters_saveSortOrder = 'order by Date_Of_Transaction__c ';
        doSort(soql_query,'TransactionDates');
    }
    
    public void sortTransactionTotals() {
        this.removeCurentSortOrder();
        String soql_query = base_soql + 'order by Total_Amount__c ';
        this.filters_saveSortOrder = 'order by Total_Amount__c ';
        doSort(soql_query,'TransactionTotals');
    }
    
    public void sortReceiptTypes() {
        this.removeCurentSortOrder();
        String soql_query = base_soql + 'order by Type__c ';
        this.filters_saveSortOrder = 'order by Type__c ';
        doSort(soql_query,'ReceiptTypes');
    }
    
    private void removeCurentSortOrder(){
        if(base_soql != null && !base_soql.equals('')){
            String aux = base_soql.toLowerCase();
            Integer pos = aux.indexOf('order by');
            if(pos > -1){
                base_soql = base_soql.substring(0,pos);
            }
        }
    }
    
    // Getter for the table 
    public List<PortalReceiptView> getReceiptList(){
        
        receiptListView = new List<PortalReceiptView>();
        
        current_page_static = String.valueOf(setController.getPageNumber());
        current_filter_static = selected_account_id;
        
        // Set statics for link creation.
        current_sort_order_static = sortOrder;
        current_sort_field_static = previousSortField; 
        
        for(sObject receipt : getQuery())
        {
            receiptListView.add(new PortalReceiptView(receipt, this));
        }
        
        return receiptListView;
    } 
    
    
    public List<sObject> getQuery(){
        
        
        return (List<sObject>) this.setController.getRecords();
        
    }
    
    // viewstate
    public class PortalReceiptView
    {
        public sObject receipt {get;set;}
        
        public String receiptName {get;set;}
        public Double receiptTotal {get;set;}
        public Date receiptDate {get;set;}
        public String type_of_record {get;set;}
    
    public List<PortalReceiptJunctionView> receiptJunctions {get;set;}
    
    
        public ReceiptListController parentController;
    
            
        public PortalReceiptView(sObject myReceipt, ReceiptListController parent)
        {
            this.receipt = myReceipt;
            this.receiptJunctions = new List<PortalReceiptJunctionView>();
            Object recName = receipt.get('Name');
            if(recName != null){
                receiptName = (String)recName;
            }
            Object totalAmt = receipt.get('Total_Amount__c');
            if(totalAmt != null){
                receiptTotal = (Double)totalAmt;
            }
            Object recDate = receipt.get('Date_Of_Transaction__c');
            if(recDate != null){
                receiptDate = (Date)recDate;
            }
            Object recType = receipt.get('Type__c');
            if(recType != null){
                type_of_record = (String)recType;
                if(type_of_record == 'Prepay Order') {
                    List<SObject> divs = myReceipt.getSObjects('Receipt_Junctions__r');
                    if(divs!=null){ // possible that the current Receipt doesnot have any Receipt junctions if the Scheduled Payments have been made.
                        for (SObject div : divs) {
                            this.receiptJunctions.add(new PortalReceiptJunctionView(div, type_of_record));
                        }
                    }
                } else {
                    List<SObject> divs = myReceipt.getSObjects('Receipt_Junctions__r');
                    if(divs!=null){
                        for (SObject div : divs) {
                            this.receiptJunctions.add(new PortalReceiptJunctionView(div, type_of_record));
                        }
                    }
                }
            }
            system.debug('**************this.receiptJunctions' + this.receiptJunctions);
            system.debug('**************this.receiptJunctions' + this.receiptJunctions.size());
            parentController = parent;
        }       
        
    }
    
    public class PortalReceiptJunctionView{
      
      public String opportunityName {get;set;}
        public String invoiceName {get;set;}
        public Date contractStartDate {get;set;}
        public Date contractEndDate {get;set;}
        public Double paymentAmount {get;set;}
        public Double outstandingAmount {get;set;}
        public String status {get;set;}
        
        public PortalReceiptJunctionView(sObject div, String type_of_record) {
          if(type_of_record == 'Prepay Order') {
              Object recName = div.get('Opportunity_Name__c');
              if(recName != null){
                  opportunityName = (String)recName;
              }
              Object conStDate = div.get('Contract_Start_Date__c');
              if(conStDate != null){
                  contractStartDate = (Date)conStDate;
              }
              Object conEnDate = div.get('Contract_End_Date__c');
              if(conEnDate != null){
                  contractEndDate = (Date)conEnDate;
              }   
                Object paymentAmt = div.get('Amount__c');
              if(paymentAmt != null){
                  paymentAmount = (Double)paymentAmt;
              }
              status = 'Processed';
              outstandingAmount = 0;
              SObject ioDetailsObj = div.getSObject('IO_Approval__r');
              
              if(ioDetailsObj != null) {
                  Double totalAmt = 0;
                  Double pendingAmt = 0;
                  Object ioTotalAmount = ioDetailsObj.get('Total_IO_Amount__c');
                  if(ioTotalAmount != null) {
                      totalAmt = (Double)ioTotalAmount;
                  }
                  Object ioPendingAmount = ioDetailsObj.get('Total_Paid_Amount_From_Portal__c');
                  if(ioPendingAmount != null) {
                      pendingAmt = (Double)ioPendingAmount;
                  }
                  outstandingAmount = totalAmt - pendingAmt;
              }
              
            } else if(type_of_record == 'Sales Invoice'){
            Object invName = div.get('Sales_Invoice_Name__c');
              if(invName != null){
                  invoiceName = (String)invName;
              }
              Object istat = div.get('Sales_Invoice_Status__c');
              if(istat != null){
                  status = (String)istat;
              } 
              Object iAmt = div.get('Sales_Invoice_Outstanding_Amount__c');
              if(iAmt != null){
                  outstandingAmount = (Double)iAmt;
              }
              Object paymentAmt = div.get('Amount__c');
              if(paymentAmt != null){
                  paymentAmount = (Double)paymentAmt;
              } 
              
            } else if(type_of_record == 'ERP Invoice'){
            	SObject invoice_obj = div.getSObject('ERP_Invoice__r');
                if(invoice_obj != null) {
                    Object invName = invoice_obj.get('Name');
                    if(invName != null) {
                        invoiceName = (String)invName;
                    }
                    Object istatus = invoice_obj.get('Invoice_Status__c');
                    if(istatus != null) {
                        status = (String)istatus;
                    }
                    Object iAmt = invoice_obj.get('Outstanding_Value__c');
                    if(iAmt != null) {
                        outstandingAmount = (Double)iAmt;
                    }
            }
              
              Object paymentAmt = div.get('Amount__c');
              if(paymentAmt != null){
                  paymentAmount = (Double)paymentAmt;
              } 
              
            }
        }
        
    }
    
    
    
    


    public PageReference backToReceiptList2(){
        
        
        //PageReference listPage = new PageReference(System.currentPageReference().getUrl() + '?receiptid=' + this.receipt.Id);
        
        //listPage.setRedirect(false);
        
        return null;
    }

    
    static testMethod void testReceiptListController(){
       
            
        try{
            
            
            Account A1 = new Account(Name = 'Test Account');
          insert A1;
            
            sObject sObj = Schema.getGlobalDescribe().get('Receipt__c').newSObject() ;  
            sObj.put('Account__c' , A1.Id) ;  
            sObj.put('Authorization__c' , 'Test Auth') ; 
            sObj.put('Date_Of_Transaction__c' , date.today()) ; 
            sObj.put('IsSuccess__c' , true) ; 
            sObj.put('Type__c' , 'Prepay Order') ;  
            sObj.put('Customer_Portal_User__c' , UserInfo.getUserId()) ;  
            
            insert sObj ;
            
            
            sObject sObj1 = Schema.getGlobalDescribe().get('Receipt_Junction__c').newSObject() ;  
            sObj1.put('Amount__c' , 5000) ;  
            sObj1.put('Receipt__c' , sObj.Id) ; 
            
            insert sObj1 ;
            
            ReceiptListController pilc = new ReceiptListController();
            pilc.filterReceipts();
            pilc.queryReceipts();
            pilc.sortReceiptNumbers();
            pilc.sortTransactionDates(); 
            pilc.sortTransactionTotals();
            pilc.getReceiptList();
            
            
            
            sObj.put('Type__c' , 'Sales Invoice') ;  
            update sObj ;
            pilc = new ReceiptListController();
            
            ApexPages.currentPage().getParameters().put('sort_field','ReceiptNumbers');
            ApexPages.currentPage().getParameters().put('sort_order','asc');
             pilc = new ReceiptListController();
            ApexPages.currentPage().getParameters().put('sort_field','TransactionDates');
            ApexPages.currentPage().getParameters().put('sort_order','asc');
             pilc = new ReceiptListController();
            ApexPages.currentPage().getParameters().put('sort_field','TransactionTotals');
            ApexPages.currentPage().getParameters().put('sort_order','asc');
             pilc = new ReceiptListController();
            
            pilc.GoPrevious();
            pilc.GoNext();
            pilc.GoLast();
            pilc.GoFirst();
            pilc.getRenderPrevious();
            pilc.getRenderNext();
            pilc.getRecordSize();
            pilc.getPageNumber();
            
            

            pilc.filters.selectionInList = 'invoice';
            pilc.filters.filterByNumber_start = '15';
            pilc.filters.filterByNumber_end = '120';
            pilc.filters.filterByDate_start = '01/01/2010'; 
            pilc.filters.filterByDate_end = '01/01/2011'; 
            pilc.processFilters();
            
            
            //System.assert(piv != null);
        } catch(Exception e ){
            system.debug('Exception--------' + e.getStackTraceString());
        }
        
    }
}