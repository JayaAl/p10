/**
* ─────────────────────────────────────────────────────────────────────────────────────────────────┐
* Opportunity Related Products
* 
* This class is servr side controler for Related products on
*  opportunity in lightning mode.
* Refered in relatedProduct component.
* ──────────────────────────────────────────────────────────────────────────────────────────────────
* @author         Jaya Alaparthi
* @maintainedBy   Jaya Alaparthi
* @version        1.0
* @created        2017-01-12
* @modified       YYYY-MM-DD
* @systemLayer    Service
* @see            ????
* @see            ????
* ──────────────────────────────────────────────────────────────────────────────────────────────────
* @changes
* vX.X            Jaya Alaparthi
* YYYY-MM-DD      Explanation of the change.  Multiple lines can be used to explain the change, but
*                 each line should be indented till left aligned with the previous description text.
*
* vX.X            Jaya Alaparthi
* YYYY-MM-DD      Each change to this file should be documented by incrementing the version number,
*                 and adding a new entry to this @changes list. Note that there is a single blank
*                 line between each @changes entry.
* ─────────────────────────────────────────────────────────────────────────────────────────────────┘
*/
public with sharing class RelatedListDisplay {
	
	//─────────────────────────────────────────────────────────────────────────┐
	// getPLIList: get opportunity based on Opportunity Id
	// @param opptyId  opportunity Id
	// @return list<OpportunityLineItem> 
    //─────────────────────────────────────────────────────────────────────────┘
	@AuraEnabled
	public static Opportunity getOppty(String opptyId) {

		Opportunity oppty = new Opportunity();
		System.debug('opptyId recived in RelatedListDisplay:'+opptyId);
		oppty = ProductOpportunityRelatedUtil.getOpportunity(opptyId);
		System.debug('oppty:'+oppty);
		return oppty;
	}
	//─────────────────────────────────────────────────────────────────────────┐
	// getPLIList: get the list of plis related to the current opptyId
	// @param opptyId  opportunity Id
	// @return list<OpportunityLineItem> 
    //─────────────────────────────────────────────────────────────────────────┘
	@AuraEnabled
	public static list<OpportunityLineItem> getPLIList(String opptyId) {

		List<OpportunityLineItem> pliList = new List<OpportunityLineItem>();
		//opptyId='00622000002UkmbAAC';
		if(!String.isBlank(opptyId) 
			&& opptyId.startsWith('006')) { 

			try {
				pliList = [SELECT Id,Offering_Type__c,
							Medium__c,Platform__c,
							Sub_Platform__c,Banner__c,
							Banner_Type__c,Size__c,
							Cost_Type__c,Guaranteed__c,
							UnitPrice,End_Date__c,ServiceDate 
						FROM OpportunityLineItem
						WHERE OpportunityId =: opptyId];
			} catch(Exception e) {
				System.debug('Error while retriving PLIs for this Opportuntiy :'
						+opptyId);
			}
		}
		return pliList;
	}
	//─────────────────────────────────────────────────────────────────────────┐
	// getOfferingTypeValues: retrive offering type picklist values form PLI Object
	// @return list<String> 
    //─────────────────────────────────────────────────────────────────────────┘
	@AuraEnabled
	public static list<String> getOfferingTypeValues() {

		list<String> offeringTypesList = new list<String>();
		OpportunityLineItem pli = new OpportunityLineItem();
		offeringTypesList = getPickValues(pli,'Offering_Type__c');
		System.debug('List retrived for offering type:'+offeringTypesList);
		return offeringTypesList;
	}
	//─────────────────────────────────────────────────────────────────────────┐
	// getMediumValues: retrive offering type picklist values form PLI Object
	// @return list<String> 
    //─────────────────────────────────────────────────────────────────────────┘
	@AuraEnabled
	public static list<String> getMediumValues() {
		
		list<String> mediumList = new list<String>();	
		OpportunityLineItem pli = new OpportunityLineItem();
		mediumList = getPickValues(pli,'Medium__c');
		System.debug('List retrived for medium :'+mediumList);
		return mediumList;
	}

	//─────────────────────────────────────────────────────────────────────────┐
	// getPickValues: return the list of picklist values retrived from metadata.
	// @param object_name  sObject
	// @param field_name   String
	// @return list<String> 
    //─────────────────────────────────────────────────────────────────────────┘
	private static list<String> getPickValues(Sobject object_name, 
												String field_name) {
      list<String> picklistOptions = new list<String>(); //new list for holding all of the picklist options
      
      Schema.sObjectType sobject_type = object_name.getSObjectType(); 
      Schema.DescribeSObjectResult sobject_describe = sobject_type.getDescribe(); 
      Map<String, Schema.SObjectField> field_map = sobject_describe.fields.getMap();
      list<Schema.PicklistEntry> pick_list_values = field_map.get(field_name).getDescribe().getPickListValues();
      for (Schema.PicklistEntry a : pick_list_values) { //for all values in the picklist list
                  
            picklistOptions.add(a.getValue()); //add the value and label to our final list
      }
      return picklistOptions; //return the List
	}
	//─────────────────────────────────────────────────────────────────────────┐
	// getPLI: return pli either new or existing.
	// @param pliId  	String
	// @param opptyId   String
	// @return OpportunityLineItem
    //─────────────────────────────────────────────────────────────────────────┘
	@AuraEnabled
	public static OpportunityLineItem getPLI(String pliId, 
											Opportunity oppty) {
		//getNewPLI
		System.debug('in getPLI:'+pliId+'oppty:'+oppty);
		OpportunityLineItem pli = new OpportunityLineItem(
									OpportunityId = oppty.Id,
									Quantity = 1.0,
									Discount__c = 0.0,
									Duration__c = 0,
									UnitPrice = 0.0);
		
		if(pliId != null && pliId != '') {
			try {
				pli = [SELECT Id, Offering_Type__c,
							Medium__c,ServiceDate,
							End_Date__c,UnitPrice,
							Discount__c,Duration__c,
							OpportunityId
						FROM OpportunityLineItem
						WHERE Id =: pliId];
				System.debug('pli:'+pli);
			} catch (Exception e) {
				System.debug('e:'+e.getMessage());
			}
		} 
		System.debug('pli:'+pli);
		return pli;
	}
	
	//─────────────────────────────────────────────────────────────────────────┐
	// savePLI: get the list of plis related to the current opptyId.
	// 			returns pli id if errors return error.
	// @param oppty  opportunity 
	// @param pliStr string format of pli
	// @param actType account type string
	// @return String
    //─────────────────────────────────────────────────────────────────────────┘
	@AuraEnabled
	public static String savePLI(Opportunity oppty,
								String pliStr,
								String actType) {
		System.debug('in save PLI');
		System.debug('in save PLI oppty:'+oppty+'pliStr:'+pliStr);
		list<OpportunityLineItem> pliList = new list<OpportunityLineItem>();
		OpportunityLineItem pli = new OpportunityLineItem();
		
		String str = 'Insert is blocked for now';

		if(!String.isBlank(pliStr)) {

			pliList = (list<OpportunityLineItem>)JSON.deserialize(pliStr,List<OpportunityLineItem>.class);
			System.debug('pliList: '+pliList);

			PricebookEntry pbe = new PricebookEntry();			
			pbe = ProductOpportunityRelatedUtil.validateOpptyRecordType(oppty);
			
			if(actType.equalsIgnoreCase('del') 
				|| actType.equalsIgnoreCase('create')) {

				pli = pliList[0];
			} else if(actType.equalsIgnoreCase('edit')
					|| actType.equalsIgnoreCase('clone')){

				pli.ServiceDate = pliList[0].ServiceDate;
				pli.UnitPrice = pliList[0].UnitPrice;
				pli.Medium__c = pliList[0].Medium__c;
				pli.Duration__c = pliList[0].Duration__c;
				pli.OpportunityId = pliList[0].OpportunityId;
				pli.End_Date__c = pliList[0].End_Date__c;
				pli.Offering_Type__c = pliList[0].Offering_Type__c;
				pli.Quantity = 1;
				pli.PricebookEntryId = pbe.Id;
				if(actType.equalsIgnoreCase('edit')) {

					pli.Id = pliList[0].Id;
				}
			} 
			System.debug('pli:'+pli);
			if(actType.equalsIgnoreCase('create')) {
				pli.PricebookEntryId = pbe.Id;
			}
			// updating duration in case of new pli
			pli.Duration__c = pli.ServiceDate.daysBetween(pli.End_Date__c)+1;
			// discount calculation this calculation is only for old plis.
			// verify this.
			if(pli.Discount__c != null ) {
			//&& pli.OpportunityRecordType.Name == ProductOpportunityRelatedUtil.OPPTY_RT_P1SUB
				 pli.UnitPrice = pli.UnitPrice - 
				 			(pli.UnitPrice * pli.Discount__c.divide(100,2));
			}

			try{
				System.debug('pli:'+pli);
				if(actType.equalsIgnoreCase('edit')) {
					OpportunityLineItem temp = pli.clone(false);
					System.debug('clone pli before deleting:'+temp);
					// insted of upsert  need to delete and insert a 
					//new record only then split trigger will be executed.
					//upsert pli;	
					delete pli;
					pli = temp;
				}
				insert pli;
				str = pli.Id;
			} catch(Exception e) {
				str = 'Error Saving Product : '+e.getMessage();
				System.debug('str:'+str);
			}
		}
		return str;
	}

	//─────────────────────────────────────────────────────────────────────────┐
	// deletePLI: delete pli
	// @param pliId Product Line Item Id to be deleted	
	// @return String
    //─────────────────────────────────────────────────────────────────────────┘
	@AuraEnabled
	public static String deletePLI(String pliId) {

		String deleteStr = '';
		try{
			if(!String.isBlank(pliId)) {

				OpportunityLineItem pli = new OpportunityLineItem(Id = pliId);
				delete pli;
				deleteStr = 'Product Deleted. ';
			} else {

				deleteStr = 'Product to be deleted is not provided.';
			}
		} catch(Exception e) {
			deleteStr = 'Error Deleting Product : '+e.getMessage();
		}
		return deleteStr;
	}
}