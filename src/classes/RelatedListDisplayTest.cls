/**
* ─────────────────────────────────────────────────────────────────────────────────────────────────┐
* Opportunity Related Products Test
* 
* Test class for:
* RelatedListDisplay.cls
* ProductOpportunityRelatedUtil.cls
* ──────────────────────────────────────────────────────────────────────────────────────────────────
* @author         Jaya Alaparthi
* @maintainedBy   Jaya Alaparthi
* @version        1.0
* @created        2017-05-09
* @modified       YYYY-MM-DD
* @systemLayer    Test
* ──────────────────────────────────────────────────────────────────────────────────────────────────
* @changes
* vX.X            Jaya Alaparthi
* YYYY-MM-DD      Explanation of the change.  Multiple lines can be used to explain the change, but
*                 each line should be indented till left aligned with the previous description text.
* ─────────────────────────────────────────────────────────────────────────────────────────────────┘
*/
@isTest
private class RelatedListDisplayTest {

	@testSetUp static void setupData() {

		// create Products
		Product2 testProduct = new Product2(Name='Audio Everywhere',
								CurrencyIsoCode = 'USD',
								IsActive = true,
								Auto_Schedule__c = true);
		insert testProduct;

		Id pricebookId = Test.getStandardPricebookId();
		PricebookEntry standardPB = new PricebookEntry(
								Pricebook2Id = pricebookId,
								Product2Id = testProduct.Id,
								UnitPrice = 100,
								IsActive = true);
		insert standardPB;

		Pricebook2 customPB = new Pricebook2(Name='Audio EverywherePBE',
								isActive = true);
		insert customPB;
		PricebookEntry usdPB = new PricebookEntry(
								Pricebook2Id = customPB.Id,
								Product2Id = testProduct.Id,
								UnitPrice = 100,
								IsActive = true,
								CurrencyIsoCode = 'USD');
		insert usdPB;

		// create Account
		Account account =  new Account();
		account = UTIL_TestUtil.generateAccount();
		account.Name = 'productsTest'+account.Name;
		account.BillingCountry = 'USA';
		insert account;
		// create Opportunity\
		/*
		If used with soql reaching Too many SOQL quries: 101
		So i have to use Schema
		RecordType opptyRECType = [SELECT Description,DeveloperName,
									Id,IsActive,LastModifiedById,
									LastModifiedDate,Name,SobjectType 
								FROM RecordType 
								WHERE SobjectType = 'Opportunity'
								AND DeveloperName = 'Opportunity_Pandora_One_Subs'];
		*/
		Id opptyRecordTypeId = Schema.SObjectType.Opportunity.getRecordTypeInfosByName().get('Opportunity - Inside/Local Full').getRecordTypeId();
        
		System.debug('opptyRecordTypeId:'+opptyRecordTypeId);
		Opportunity opportunity = new Opportunity();
		opportunity = UTIL_TestUtil.generateOpportunity(account.Id);
		opportunity.CurrencyIsoCode = 'USD';
		opportunity.RecordTypeId = opptyRecordTypeId;
		insert opportunity;

		// create PLIS
		OpportunityLineItem pli = new OpportunityLineItem(
							ServiceDate = Date.today(),
							UnitPrice = 12,
							Medium__c = 'Audio',
							Duration__c = 1,
							OpportunityId = opportunity.Id,
							End_Date__c = Date.today(),
							Offering_Type__c = 'Standard',
							Quantity = 1,
							PricebookEntryId = standardPB.Id);
		insert pli;
		
		// create taxmony record
		Opportunity oppty = [SELECT Id,RecordType.DeveloperName
							 FROM Opportunity LIMIT 1];
		TaxonomyRecordTypes__c recordTypeMap = new TaxonomyRecordTypes__c(
												Name = 'Opportunity_Inside_Sales');
		insert recordTypeMap;
	}
	//─────────────────────────────────────────────────────────────────────────┐
	// createProduct: Testing CreateProduct Use Case
    //─────────────────────────────────────────────────────────────────────────┘
	@isTest
	static void createProduct()
	{
		OpportunityLineItem getEditClonePLI = new OpportunityLineItem();
		Opportunity oppty = [SELECT Id,RecordType.DeveloperName
							 FROM Opportunity LIMIT 1];
		RelatedListDisplay ref = new RelatedListDisplay();

		list<String> offeringValues = new list<String>();
		list<String> mediumValues = new list<String>();
		String pliCreated;

		Test.startTest();

		Id pricebookId = Test.getStandardPricebookId();
		System.debug('standard PBE:'+pricebookId);
		PricebookEntry standardPBE = [SELECT Id,Pricebook2.Name 
						FROM PricebookEntry 
						WHERE Pricebook2Id = :pricebookId];
		System.debug('standardPBE:'+standardPBE.Pricebook2.Name);
		OpportunityLineItem pli = new OpportunityLineItem(
							ServiceDate = Date.today(),
							UnitPrice = 92,
							Medium__c = 'Display',
							Duration__c = 1,
							OpportunityId = oppty.Id,
							End_Date__c = Date.today(),
							Offering_Type__c = 'Standard',
							Quantity = 1,
							Discount__c = 20,
							PricebookEntryId = standardPBE.Id);
		for (PriceBookEntry pbeList : [SELECT Id, Name,Pricebook2.Name,
                              CurrencyIsoCode, UnitPrice,IsActive   
							FROM PricebookEntry]) {
			System.debug('pbe:'+pbeList);
			System.debug('pbe:'+pbeList.Pricebook2.Name);
		}
		RelatedListDisplay.getOppty(oppty.Id);				
		RelatedListDisplay.getPLIList(oppty.Id);
		String serializedPLI = '['+JSON.serialize(pli)+']';
		pliCreated = RelatedListDisplay.savePLI(oppty,
									serializedPLI,
									'create');
		System.debug('pliCreated:'+pliCreated);
		OpportunityLineItem createdPLI = [SELECT Id FROM OpportunityLineItem
											WHERE OpportunityId =: oppty.Id
											AND Medium__c = 'Display'];
		
		getEditClonePLI = RelatedListDisplay.getPLI(createdPLI.Id,oppty);
		System.assert(String.valueOf(pliCreated).startsWith('00k'),'create pli failed.');
		Test.stopTest();
		
		

	}
	//─────────────────────────────────────────────────────────────────────────┐
	// editProduct: Edit Product Use case
    //─────────────────────────────────────────────────────────────────────────┘
	@isTest
	static void editProduct() {
		
		Test.startTest();
		list<String> offeringValues = new list<String>();
		list<String> mediumValues = new list<String>();

		OpportunityLineItem getEditClonePLI = new OpportunityLineItem();
		Opportunity oppty = [SELECT Id,RecordType.DeveloperName
							 FROM Opportunity LIMIT 1];
		OpportunityLineItem retrivePLI = [SELECT Id FROM OpportunityLineItem
											WHERE OpportunityId =: oppty.Id];
		RelatedListDisplay ref = new RelatedListDisplay();
		RelatedListDisplay.getOppty(oppty.Id);				
		RelatedListDisplay.getPLIList(oppty.Id);
		offeringValues = RelatedListDisplay.getOfferingTypeValues();
		mediumValues = RelatedListDisplay.getMediumValues();
		getEditClonePLI = RelatedListDisplay.getPLI(retrivePLI.Id,oppty);
		//ref.savePLI(oppty,null,'create');
		//ref.deletePLI
		getEditClonePLI.UnitPrice = 1234;
		String serializedPLI = '['+JSON.serialize(getEditClonePLI)+']';
		RelatedListDisplay.savePLI(oppty,
									serializedPLI,
									'edit');

		System.assert(offeringValues.size() > 0,'offering values retrival error.');
		System.assert(mediumValues.size() > 0,'medium values retrival error.');
		System.assert(getEditClonePLI.OpportunityId == oppty.Id,'editclone pli Oppty oes not match retrived oppty');

		Test.stopTest();

		//getEditClonePLI = RelatedListDisplay.getPLI(getEditClonePLI.Id,oppty);
		//System.assert(getEditClonePLI.Id != null,'Unit price update failed getEditClonePLI:'+getEditClonePLI);
	}
	//─────────────────────────────────────────────────────────────────────────┐
	// deleteProduct: delete product use case
	// @param opptyId  opportunity Id
	// @return list<OpportunityLineItem> 
    //─────────────────────────────────────────────────────────────────────────┘
	@isTest
	static void deleteProduct() {

		Test.startTest();
		list<String> offeringValues = new list<String>();
		list<String> mediumValues = new list<String>();

		Opportunity oppty = [SELECT Id,RecordType.DeveloperName
							 FROM Opportunity LIMIT 1];
		OpportunityLineItem retrivePLI = [SELECT Id FROM OpportunityLineItem
											WHERE OpportunityId =: oppty.Id];
		RelatedListDisplay ref = new RelatedListDisplay();
		RelatedListDisplay.getOppty(oppty.Id);				
		RelatedListDisplay.getPLIList(oppty.Id);
		offeringValues = RelatedListDisplay.getOfferingTypeValues();
		mediumValues = RelatedListDisplay.getMediumValues();
		String deletePLISTR = RelatedListDisplay.deletePLI(retrivePLI.Id);
		System.assert(deletePLISTR.startsWithIgnoreCase('Product Deleted.'),'Error in deleting PLI.');
		// if pli id is blank test case for delete
		String deletePLISTRWithNull = RelatedListDisplay.deletePLI('');
		System.assert(deletePLISTRWithNull.startsWithIgnoreCase('Product to be deleted is not provided.'),'Error in deleting PLI.');
		Test.stopTest();
	}
}