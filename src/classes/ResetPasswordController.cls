public class ResetPasswordController{
    
    public string usernameStr {get;set;}
    
    public ResetPasswordController(){
    
    }
    
    public PageReference resetAction(){
        
        boolean success = Site.forgotPassword(usernameStr);
        PageReference pr = Page.ForgotPasswordConfirm;
        system.debug('is success : '+success);
        pr.setRedirect(true);
        
        return pr;
    }
    
}