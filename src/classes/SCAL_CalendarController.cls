/***************************************************
   Name: SCAL_CalendarController

   Usage: This class shows Opportunity and Product on Calendar

   Author – Clear Task

   Date – 12/20/2010

  Modified By - Vardhan Gupta (02/13/2013) - code Depracated. All functionality moved into Matterhorn. Please contact "cgraham@pandora.com" for mode details   
   
   
   Revision History
******************************************************/
 
  
public class SCAL_CalendarController {
/*
* Variable used -start

//private String sponsorType=ApexPages.currentPage().getParameters().get('type');
private String booked='Booked';
public String selectedYear{get;set;} 
public String selectedMonth{get;set;} 
public String showProduct{get;set;} 
private String pitched='Pitched';
private String sold='Sold';


//Variable used -ends

public String sponsorType{get;set;} 
public String getBooked(){return booked;}//getBooked method for book status type
public String getPitched(){return pitched;}//getPitched method for Pitched status type
public String getSold(){return sold;}//getSold method for Sold status type
public date dropDown;
private date selectedDT;
public Set<Id> sprIds = new Set<Id>(); 
Date StartDate;
Date EndDate;

//next method used for go nest month

public void next() { 
addMonth(1);
}


// prev method used for go prev month

public void prev() { 
addMonth(-1); 
}


 //constructure

public SCAL_CalendarController() {

Date d = system.today(); // default to today 
Integer mo = d.month(); 
String m_param = System.currentPageReference().getParameters().get('mo');
String y_param = System.currentPageReference().getParameters().get('yr');
String type_param = System.currentPageReference().getParameters().get('type');

StartDate = Date.Today().toStartOfMonth();
EndDate = StartDate.addDays(Date.daysInMonth(Date.Today().Month(),Date.Today().year()));
EndDate = EndDate.adddays(7);
StartDate= startdate.toStartOfWeek();
// allow a month to be passed in on the url as mo=10
if (m_param != null) { 
Integer mi = Integer.valueOf(m_param); 
if (mi > 0 && mi <= 12) {
d = Date.newInstance(d.year(),mi,d.day());
}
}
System.debug('#############m_param ='+m_param ); 
// and year as yr=2008
if (y_param != null) { 
Integer yr = Integer.valueOf(y_param); 
d = Date.newInstance(yr, d.month(), d.day());
}
dropDown=d;
setMonth(d);
showProduct='All';
if (type_param != null && (!type_param.equals(''))) {
sponsorType = type_param;
SearchSponsorByType();
}else{
sponsorType = '';
SearchSponsorByType();
}
selectedMonth=''+d.month();
selectedYear=''+d.year();


}


//getWeeks method used for getting weeks

public List<SCAL_Month.Week> getWeeks() { 
system.assert(month!=null,'month is null');
return month.getWeeks();
}

public SCAL_Month getMonth() { return month; } // return month


//setMonth method used for setting months

private void setMonth(Date d) { 


StartDate = d.toStartOfMonth();
EndDate = StartDate.addDays(Date.daysInMonth(d.Month(),d.year()));
EndDate = EndDate.adddays(7);

month = new SCAL_Month(d); 
system.assert(month != null); 
StartDate= startdate.toStartOfWeek();

Date[] da = month.getValidDateRange(); // gather events that fall in this month
List<Sponsorship__c> sponsorships = new List<Sponsorship__c>();
List<Sponsorship_Product__c> sponsorshipProduct = new List<Sponsorship_Product__c>();

if(sponsorType==null ||sponsorType==''){ 
showProduct='All';
sponsorships = [ select Name, id,Date__c,Status__c,Ad_Product__c, Opportunity__c,Opportunity__r.name,End_Date__c from Sponsorship__c where Type__c ='Heavy-ups/Roadblocks/Limited Interruption' and Status__c!='Lost' and end_date__c >=: StartDate and Date__c <=:EndDate order by Date__c];
for(Sponsorship__c s: sponsorships){
s.Opportunity__r.name='null'; 
}
System.debug('sponsorships'+sponsorships);
List<Sponsorship__c> sponsorshipsOpp = new List<Sponsorship__c>();
List<Sponsorship_Product__c> sponsorshipsProduct = new List<Sponsorship_Product__c>();
sponsorshipsOpp = [ select Name,id,Date__c,Status__c,Ad_Product__c, Opportunity__c,Opportunity__r.name,End_Date__c from Sponsorship__c where Type__c !='Heavy-ups/Roadblocks/Limited Interruption' and Status__c!='Lost' and end_date__c >=: StartDate and Date__c <=:EndDate order by Date__c];
for(Sponsorship__c s: sponsorshipsOpp ){
s.Ad_Product__c=''; 
}
sponsorships.addAll(sponsorshipsOpp );
sponsorshipProduct = [Select s.Sponsorship__c, s.Name, s.Id, s.Sponsorship__r.Date__c, s.Sponsorship__r.End_Date__c, s.Sponsorship__r.Status__c, s.Sponsorship__r.Opportunity__c From Sponsorship_Product__c s where s.Sponsorship__r.Status__c!='Lost' and s.Sponsorship__r.end_date__c >=: StartDate and s.Sponsorship__r.Date__c <=:EndDate order by s.Sponsorship__r.Date__c]; 
}else{
if(sponsorType.equals('Heavy-ups/Roadblocks/Limited Interruption')){
showProduct='Product';
} else{showProduct='Opp';} 
sponsorships = [ select Name,id,Date__c,Status__c,Ad_Product__c, Opportunity__c,Opportunity__r.name,End_Date__c from Sponsorship__c where (Type__c=:sponsorType and Status__c!='Lost') and end_date__c >=: StartDate and Date__c <=:EndDate order by Date__c]; 
for(Sponsorship__c s :sponsorships ){
sprIds.add(s.Id); 
}
sponsorshipProduct = [Select s.Sponsorship__c, s.Name, s.Id, s.Sponsorship__r.Date__c, s.Sponsorship__r.End_Date__c, s.Sponsorship__r.Status__c, s.Sponsorship__r.Opportunity__c From Sponsorship_Product__c s where s.Sponsorship__r.Type__c ='Heavy-ups/Roadblocks/Limited Interruption' and s.Sponsorship__r.Status__c!='Lost' and s.Sponsorship__r.end_date__c >=: StartDate and s.Sponsorship__r.Date__c <=:EndDate order by s.Sponsorship__r.Date__c]; 
System.debug('sponsorshipProduct '+sponsorshipProduct ); 

}
month.setEvents(sponsorships ,sponsorshipProduct); // merge those events into the month class

}


//addMonth method used for adding months

Date currentDate;
private void addMonth(Integer val) { 
currentDate = month.getFirstDate();
currentDate = currentDate.addMonths(val);
selectedMonth=''+currentDate.month();
selectedYear=''+currentDate.year();
setMonth(currentDate);
}

//getSponsorStatusList method used for Status label

public List<Map<String, String>> getSponsorStatusList() { 
List<Map<String, String>> optionsStatus = new List<Map<String, String>>();
Schema.DescribeFieldResult implMtrxStage = Sponsorship__c.Status__c.getDescribe(); 
List<Schema.PicklistEntry> Ple = implMtrxStage.getPicklistValues();
for(Schema.PicklistEntry p : ple){
Map<String, String> mapForStatus = new Map<String, String>();
if(!(p.getlabel().equals('Lost')) && !(p.getlabel().equals('Pending')) && !(p.getlabel().equals('Withdrawn')) && !(p.getlabel().equals('Denied')) && !(p.getlabel().equals('Pending Conflicts'))){
if(p.getlabel()=='Pitched'){
mapForStatus.put('Label','Pitched');
mapForStatus.put('colorCode','leaveBlock');
}
if(p.getlabel()=='Sold'){
mapForStatus.put('Label','Reserved');
mapForStatus.put('colorCode','holidayBlock');
}
optionsStatus.add(mapForStatus); 
}

}
return optionsStatus;
}


//getSponsorTypeList method used for Type Dopdown
 
public List<SelectOption> getSponsorTypeList() { 
List<SelectOption> options = new List<SelectOption>();
Schema.DescribeFieldResult implMtrxStage = Sponsorship__c.Type__c.getDescribe(); 
List<Schema.PicklistEntry> Ple = implMtrxStage.getPicklistValues();
options.add(new SelectOption('','---All---'));
for(Schema.PicklistEntry p : ple){
options.add(new SelectOption(p.getValue(),p.getlabel()));
}
return options;
}



//SearchSponsorByType method used for Type Dopdown by Type
 
public void searchSponsorByType(){
if(currentDate == null) {
currentDate = system.today();
}
if(selectedYear== null) { 
selectedYear= toString(currentDate.year()); 
}
if(selectedMonth== null) {
selectedMonth= toString(currentDate.month()); 
}
startDate = Date.newInstance(Integer.valueOf(selectedYear),Integer.valueOf(selectedMonth),1);
EndDate = StartDate.addDays(Date.daysInMonth(Integer.valueOf(selectedMonth),Integer.valueOf(selectedYear)));
EndDate = EndDate.adddays(7);
// StartDate= startdate.toStartOfWeek();

System.debug('######currentDate ='+ currentDate );

month = new SCAL_Month(startDate); 
system.assert(month != null); 
Date[] da = month.getValidDateRange();
StartDate= startdate.toStartOfWeek();
List<Sponsorship_Product__c> sponsorshipProduct = new List<Sponsorship_Product__c>();
List<Sponsorship__c> sponsorships = new List<Sponsorship__c>();
if(sponsorType==null ||sponsorType==''){ 
showProduct='All';
sponsorships = [ select Name, id,Date__c,Status__c,Ad_Product__c, Opportunity__c,Opportunity__r.name,End_Date__c from Sponsorship__c where Type__c ='Heavy-ups/Roadblocks/Limited Interruption' and Status__c!='Lost' and end_date__c >=: StartDate and Date__c <=:EndDate order by Date__c];
// sponsorships = [ select Name, id,Date__c,Type__c ,Status__c,Ad_Product__c, Opportunity__c,Opportunity__r.name,End_Date__c from Sponsorship__c where Status__c!='Lost' order by Date__c , Type__c desc];
for(Sponsorship__c s: sponsorships){
// if(s.Type__c.contains('Heavy-ups/Roadblocks/Limited Interruption')) 
s.Opportunity__r.name='null'; 
// else 
// s.Ad_Product__c=''; 

// System.debug('s---'+s.Id);

}
List<Sponsorship__c> sponsorshipsOpp = new List<Sponsorship__c>();
sponsorshipsOpp = [ select Name,id,Date__c,Status__c,Ad_Product__c, Opportunity__c,Opportunity__r.name,End_Date__c from Sponsorship__c where Type__c !='Heavy-ups/Roadblocks/Limited Interruption' and Status__c!='Lost' and end_date__c >=: StartDate and Date__c <=:EndDate order by Date__c];
for(Sponsorship__c s: sponsorshipsOpp ){
s.Ad_Product__c=''; 
}
sponsorships.addAll(sponsorshipsOpp );
sponsorshipProduct = [Select s.Sponsorship__c, s.Name, s.Id, s.Sponsorship__r.Date__c, s.Sponsorship__r.End_Date__c, s.Sponsorship__r.Status__c, s.Sponsorship__r.Opportunity__c From Sponsorship_Product__c s where s.Sponsorship__r.Status__c!='Lost' and s.Sponsorship__r.end_date__c >=: StartDate and s.Sponsorship__r.Date__c <=:EndDate order by s.Sponsorship__r.Date__c]; 

}else{
if(sponsorType.equals('Heavy-ups/Roadblocks/Limited Interruption')){
showProduct='Product';
} else{showProduct='Opp';} 
sponsorships = [ select Name,id,Date__c,Status__c,Ad_Product__c, Opportunity__c,Opportunity__r.name,End_Date__c from Sponsorship__c where (Type__c=:sponsorType and Status__c!='Lost') and end_date__c >=: StartDate and Date__c <=:EndDate order by Date__c]; 
sponsorshipProduct = [Select s.Sponsorship__c, s.Name, s.Id, s.Sponsorship__r.Date__c, s.Sponsorship__r.End_Date__c, s.Sponsorship__r.Status__c, s.Sponsorship__r.Opportunity__c From Sponsorship_Product__c s where s.Sponsorship__r.Type__c ='Heavy-ups/Roadblocks/Limited Interruption' and s.Sponsorship__r.Status__c!='Lost' and s.Sponsorship__r.end_date__c >=: StartDate and s.Sponsorship__r.Date__c <=:EndDate order by s.Sponsorship__r.Date__c]; 

}
month.setEvents(sponsorships , sponsorshipProduct); // merge those events into the month class
}


//toString method used for type Cast integer toString


public static String toString(integer Value){

System.debug('value = ' + Value);
string str=Value.format();
string[] arrString=str.split(',');
System.debug('arrString = ' + arrString);
String st = '';
if(arrString!=null){
for(String s : arrString){
st = st + s;
}
return st;
}else
return str;
}


//SearchSponsorByYear method used for Year Dopdown by Year

public void searchSponsorByYear(){
if(currentDate == null) {
currentDate = system.today();
}
if(selectedYear== null) {
selectedYear= toString(currentDate.year()); 
}
if(selectedMonth== null) {
selectedMonth= toString(currentDate.month()); 
}

startDate = Date.newInstance(Integer.valueOf(selectedYear),Integer.valueOf(selectedMonth),1);
EndDate = StartDate.addDays(Date.daysInMonth(Integer.valueOf(selectedMonth),Integer.valueOf(selectedYear)));
EndDate = EndDate.adddays(7);

system.debug('##############currentDate '+currentDate );
if(startDate == null) {
startDate = system.today();
}
month = new SCAL_Month(startDate ); 

system.assert(month != null); 
Date[] da = month.getValidDateRange();
StartDate= startdate.toStartOfWeek();
List<Sponsorship_Product__c> sponsorshipProduct = new List<Sponsorship_Product__c>();
List<Sponsorship__c> sponsorships = new List<Sponsorship__c>();
if(sponsorType==null ||sponsorType==''){ 
showProduct='All';
sponsorships = [ select Name, id,Date__c,Status__c,Ad_Product__c, Opportunity__c,Opportunity__r.name,End_Date__c from Sponsorship__c where Type__c ='Heavy-ups/Roadblocks/Limited Interruption' and Status__c!='Lost' and end_date__c >=: StartDate and Date__c <=:EndDate order by Date__c];
for(Sponsorship__c s: sponsorships){
s.Opportunity__r.name='null'; 
}
List<Sponsorship__c> sponsorshipsOpp = new List<Sponsorship__c>();
sponsorshipsOpp = [ select Name,id,Date__c,Status__c,Ad_Product__c, Opportunity__c,Opportunity__r.name,End_Date__c from Sponsorship__c where Type__c !='Heavy-ups/Roadblocks/Limited Interruption' and Status__c!='Lost' and end_date__c >=: StartDate and Date__c <=:EndDate order by Date__c];
for(Sponsorship__c s: sponsorshipsOpp ){
s.Ad_Product__c=''; 
}
sponsorships.addAll(sponsorshipsOpp );
sponsorshipProduct = [Select s.Sponsorship__c, s.Name, s.Id, s.Sponsorship__r.Date__c, s.Sponsorship__r.End_Date__c, s.Sponsorship__r.Status__c, s.Sponsorship__r.Opportunity__c From Sponsorship_Product__c s where s.Sponsorship__r.Status__c!='Lost' and s.Sponsorship__r.end_date__c >=: StartDate and s.Sponsorship__r.Date__c <=:EndDate order by s.Sponsorship__r.Date__c]; 
}else{
if(sponsorType.equals('Heavy-ups/Roadblocks/Limited Interruption')){
showProduct='Product';
} else{showProduct='Opp';} 
sponsorships = [ select Name,id,Date__c,Status__c,Ad_Product__c, Opportunity__c,Opportunity__r.name,End_Date__c from Sponsorship__c where (Type__c=:sponsorType and Status__c!='Lost') and end_date__c >=: StartDate and Date__c <=:EndDate order by Date__c]; 
sponsorshipProduct = [Select s.Sponsorship__c, s.Name, s.Id, s.Sponsorship__r.Date__c, s.Sponsorship__r.End_Date__c, s.Sponsorship__r.Status__c, s.Sponsorship__r.Opportunity__c From Sponsorship_Product__c s where s.Sponsorship__r.Type__c ='Heavy-ups/Roadblocks/Limited Interruption' and s.Sponsorship__r.Status__c!='Lost' and s.Sponsorship__r.end_date__c >=: StartDate and s.Sponsorship__r.Date__c <=:EndDate order by s.Sponsorship__r.Date__c]; 

}
month.setEvents(sponsorships , sponsorshipProduct); // merge those events into the month class

}
//to create the Month drop down 
public void searchSponsorByMonth(){
if(currentDate == null) {
currentDate = system.today();
}
if(selectedMonth== null) {
selectedMonth= toString(currentDate.month()); 
}
if(selectedYear== null) {
selectedYear= toString(currentDate.year()); 
}

startDate = Date.newInstance(Integer.valueOf(selectedYear),Integer.valueOf(selectedMonth),1);
EndDate = StartDate.addDays(Date.daysInMonth(Integer.valueOf(selectedMonth),Integer.valueOf(selectedYear)));
EndDate = EndDate.adddays(7);
//StartDate= startdate.toStartOfWeek();
system.debug('##############currentDate '+currentDate );
if(startDate == null) {
startDate = system.today();
}
month = new SCAL_Month(startDate ); 

system.assert(month != null); 
Date[] da = month.getValidDateRange();
StartDate= startdate.toStartOfWeek();
List<Sponsorship_Product__c> sponsorshipProduct = new List<Sponsorship_Product__c>();
List<Sponsorship__c> sponsorships = new List<Sponsorship__c>();
if(sponsorType==null ||sponsorType==''){ 
showProduct='All';
sponsorships = [ select Name, id,Date__c,Status__c,Ad_Product__c, Opportunity__c,Opportunity__r.name,End_Date__c from Sponsorship__c where Type__c ='Heavy-ups/Roadblocks/Limited Interruption' and Status__c!='Lost' and end_date__c >=: StartDate and Date__c <=:EndDate order by Date__c];
for(Sponsorship__c s: sponsorships){
s.Opportunity__r.name='null'; 
}
List<Sponsorship__c> sponsorshipsOpp = new List<Sponsorship__c>();
sponsorshipsOpp = [ select Name,id,Date__c,Status__c,Ad_Product__c, Opportunity__c,Opportunity__r.name,End_Date__c from Sponsorship__c where Type__c !='Heavy-ups/Roadblocks/Limited Interruption' and Status__c!='Lost' and end_date__c >=: StartDate and Date__c <=:EndDate order by Date__c];
for(Sponsorship__c s: sponsorshipsOpp ){
s.Ad_Product__c=''; 
}
sponsorships.addAll(sponsorshipsOpp );
sponsorshipProduct = [Select s.Sponsorship__c, s.Name, s.Id, s.Sponsorship__r.Date__c, s.Sponsorship__r.End_Date__c, s.Sponsorship__r.Status__c, s.Sponsorship__r.Opportunity__c From Sponsorship_Product__c s where s.Sponsorship__r.Status__c!='Lost' and s.Sponsorship__r.end_date__c >=: StartDate and s.Sponsorship__r.Date__c <=:EndDate order by s.Sponsorship__r.Date__c]; 
}else{
if(sponsorType.equals('Heavy-ups/Roadblocks/Limited Interruption')){
showProduct='Product';
} else{showProduct='Opp';} 
sponsorships = [ select Name,id,Date__c,Status__c,Ad_Product__c, Opportunity__c,Opportunity__r.name,End_Date__c from Sponsorship__c where (Type__c=:sponsorType and Status__c!='Lost') and end_date__c >=: StartDate and Date__c <=:EndDate order by Date__c]; 
sponsorshipProduct = [Select s.Sponsorship__c, s.Name, s.Id, s.Sponsorship__r.Date__c, s.Sponsorship__r.End_Date__c, s.Sponsorship__r.Status__c, s.Sponsorship__r.Opportunity__c From Sponsorship_Product__c s where s.Sponsorship__r.Type__c ='Heavy-ups/Roadblocks/Limited Interruption' and s.Sponsorship__r.Status__c!='Lost' and s.Sponsorship__r.end_date__c >=: StartDate and s.Sponsorship__r.Date__c <=:EndDate order by s.Sponsorship__r.Date__c]; 

}
month.setEvents(sponsorships ,sponsorshipProduct);
System.debug('#############sponsorships ='+sponsorships );
//month.setEvents(sponsorships); // merge those events into the month class

}
//to create the year drop down 

public List<SelectOption> getSelectedYearList() { 
List<SelectOption> yearList = new List<SelectOption>();
Date currentDateForYear = Date.today(); 
Integer currentYear = currentDateForYear.year(); 
yearList.add(new SelectOption(''+(currentYear-1),''+(currentYear-1)));
yearList.add(new SelectOption(''+(currentYear),''+(currentYear)));
yearList.add(new SelectOption(''+(currentYear+1),''+(currentYear+1)));
yearList.add(new SelectOption(''+(currentYear+2),''+(currentYear+2))); 
yearList.add(new SelectOption(''+(currentYear+3),''+(currentYear+3))); 
return yearList;
}
public List<SelectOption> getSelectedMonthList() {
List<SelectOption> monthList = new List<SelectOption>(); 
monthList.add(new SelectOption('1','January' ));
monthList.add(new SelectOption('2','February'));
monthList.add(new SelectOption('3','March'));
monthList.add(new SelectOption('4','April'));
monthList.add(new SelectOption('5','May'));
monthList.add(new SelectOption('6','June'));
monthList.add(new SelectOption('7','July'));
monthList.add(new SelectOption('8','August'));
monthList.add(new SelectOption('9','September'));
monthList.add(new SelectOption('10','October'));
monthList.add(new SelectOption('11','November'));
monthList.add(new SelectOption('12','December'));
return monthList;
} 

//private List<Event> events;
private SCAL_Month month;

**/
}