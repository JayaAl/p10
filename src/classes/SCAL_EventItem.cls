public class SCAL_EventItem {

/**

 Modified By - Vardhan Gupta (02/13/2013) - code Depracated. All functionality moved into Matterhorn. Please contact "cgraham@pandora.com" for mode details   
 
 
    public Sponsorship__c ev;
    public Special_Event__c evSpe;
    public String formatedDate;     
    public String name;
    public String opportunity;
    public String opportunityId;
    public String[] product;
    public String[] productName;
    public String status;
    public String statusSp;
    public String Discription;
    public String formatedDateSpe ;
    public String formatedDateSp ;
    public Sponsorship_Product__c evSp;
    public String opportunitySp;
    public String opportunityIdSp;
    
    
    
    public SCAL_EventItem(Sponsorship__c e) { 
        ev= e;
        // build formated date
        //9:00 AM - 1:00 PM
        //  system.debug(e.activitydatetime.format('MMM a'));
        //  system.debug(e.DurationInMinutes);        
        //system.debug(e.activitydatetime.format('h:mm a '));
        formatedDate = String.valueOf(e.Date__c);
        system.debug(formateddate);
    }
    public SCAL_EventItem(Special_Event__c e) { 
        evSpe= e;
        // build formated date
        //9:00 AM - 1:00 PM
        //  system.debug(e.activitydatetime.format('MMM a'));
        //  system.debug(e.DurationInMinutes);        
        //system.debug(e.activitydatetime.format('h:mm a '));
        formatedDateSpe = String.valueOf(e.Date__c);
        system.debug(formateddate);
    }
    public SCAL_EventItem(Sponsorship_Product__c e) { 
        evSp= e;
        // build formated date
        //9:00 AM - 1:00 PM
        //  system.debug(e.activitydatetime.format('MMM a'));
        //  system.debug(e.DurationInMinutes);        
        //system.debug(e.activitydatetime.format('h:mm a '));
        formatedDateSp = String.valueOf(e.Sponsorship__r.Date__c);
        system.debug(formateddate);
    }

    
    
    public Sponsorship__c getEv() { return ev; }
    public Sponsorship_Product__c getEvSp() { return evSp; }
    public Special_Event__c getEvSpe() { return evSpe; }
    
    public String getFormatedDate() { return formatedDate; }
    public String getName() {return ev.Name;}
    public String getOpportunity() {return ev.Opportunity__r.name;}
    public String getOpportunityId() {return ev.Opportunity__r.Id;}
    
    public String[] getProduct(){
       String strProduct=ev.Ad_Product__c;
       List<String> strProductArr=new  List<String>();
       if(strProduct!=null){
           strProductArr=strProduct.split(';');}
       else{strProductArr=new  List<String>();}
       return strProductArr;
    }  
    
    public String getOpportunitySp() {return evSp.Sponsorship__c;} 
    public String getOpportunityIdSp() {return evSp.Sponsorship__r.Opportunity__c;}
    public String getProductName() {return evSp.Name;}
    public String getSProductId() {return evSp.Id;}
    public String getStatus() {return ev.Status__c;}
    public String getStatusSp() {return evSp.Sponsorship__r.Status__c  ;}
    public String getDiscription() {return evSpe.Description__c;}
    public String getFormatedDateSpe () {return formatedDateSpe ;}
    public String getFormatedDateSp () {return formatedDateSp ;}
    
    **/
}