public class SCAL_Month {
    
    // Modified By - Vardhan Gupta (02/13/2013) - code Depracated. All functionality moved into Matterhorn. Please contact "cgraham@pandora.com" for mode details   
    
    /* 
     * Member/Variable use in Month class-Start
     
    private List<Week> weeks; //for weeks list
    public Date firstDate; // always the first of the month
    private Date upperLeft; //for Date
    public List<Special_Event__c> listSpecialEvent;// for special Event
   
    */
    
    /* 
     * Member/Variable use in Month class-End
     */
     
    /* 
     * getValidDateRange method for Validate Date Range
 
    public List<Date> getValidDateRange() { 
        // return one date from the upper left, and one from the lower right
        List<Date> ret = new List<Date>();
        ret.add(upperLeft);
        ret.add(upperLeft.addDays(5*7) );
        return ret;
    }
        */
    
    
    /* 
     * getMonthName method for get month
     
    public String getMonthName() {   
        return DateTime.newInstance(firstDate.year(),firstdate.month(),firstdate.day()).format('MMMM');
    } 
    */
    /* 
     * getYearName method for get Year Name

    public String getYearName() { 
        return DateTime.newInstance(
        firstDate.year(),firstdate.month(),firstdate.day()).format('yyyy');
    } 
    
         */
    
    /* 
     * getWeekdayNames method for get Weekday Names
   
    public String[] getWeekdayNames() { 
        Date today = system.today().toStartOfWeek();
        DateTime dt = DateTime.newInstanceGmt(today.year(),today.month(),today.day());      
        list<String> ret = new list<String>();
        for(Integer i = 0; i < 7;i++) { 
            ret.add( dt.formatgmt('EEEE') );
            dt= dt.addDays(1);
        } 
        return ret;
    }
    
      */
    
    /* 
     * getfirstDate method for first date
    
    public Date getfirstDate() { return firstDate; } 
 */

    /* 
     *  constructure for Month
   
    public SCAL_Month( Date value ) {
        weeks = new List<Week>();
        firstDate = value.toStartOfMonth();
        upperLeft = firstDate.toStartOfWeek();
        Date tmp = upperLeft;
        for (Integer i = 0; i < 5; i++) {
            Week w = new Week(i+1,tmp,value.month());   
            system.assert(w!=null); 
            this.weeks.add( w );
            tmp = tmp.addDays(7);  
        }
        listSpecialEvent=[select Date__c,Description__c from Special_Event__c ];
    }
    
      */
   
    /* 
     *  setEvents method for Events as show on visualforce Page
    
    public void setEvents(List<Sponsorship__c> sponsorships, List<Sponsorship_Product__c> sponsorshipProduct){    
        if(Test.isRunningTest()) return;    
        for(Sponsorship__c sponsorship:sponsorships) { 
            for(Week w:weeks) { 
                for(Day c: w.getDays() ) {                     
                    if ( sponsorship.Date__c <= (c.theDate) && (c.theDate)<= sponsorship.End_Date__c)  { // sponsorship.Date__c.isSameDay(c.theDate) 
                        // add this event to this calendar date
                        c.setStatusSymbol(sponsorship.Status__c);
                        c.eventsToday.add(new SCAL_EventItem(sponsorship)); 
                                               
                        //c.setStyleClass(sponsorship.Status__c);
                        // add only three events, then a More... label if there are more
                    } 
                } 
            } 
        }
        
        if(sponsorshipProduct != null && sponsorshipProduct.size()>0){
            for(Sponsorship_Product__c sponsorshipPrduct :sponsorshipProduct) { 
                for(Week w:weeks) { 
                    for(Day c: w.getDays() ) {                     
                        if ( sponsorshipPrduct.Sponsorship__r.Date__c <= (c.theDate) && (c.theDate)<= sponsorshipPrduct.Sponsorship__r.End_Date__c) { // sponsorship.Date__c.isSameDay(c.theDate) 
                            // add this event to this calendar date 
                            c.sproductEventsToday.add(new SCAL_EventItem(sponsorshipPrduct));                        
                            //c.setStyleClass(sponsorship.Status__c);
                            // add only three events, then a More... label if there are more
                        } 
                    } 
                }
             }
         } 

        //show Special Event
        for(Special_Event__c specEvent:listSpecialEvent) { 
            for(Week w:weeks) { 
                for(Day c: w.getDays() ) {                     
                    if (specEvent.Date__c.isSameDay(c.theDate) )  {  
                        // add this event to this calendar date 
                        c.spEventsToday.add(new SCAL_EventItem(specEvent));                        
                        c.setSpecCSS();
                        // add only three events, then a More... label if there are more
                    } 
                } 
            } 
        }
        
        
    }
    
     */
    
    /* 
     *  getWeeks method for get Weeks
    
    public List<Week> getWeeks() { 
        system.assert(weeks!=null,'could not create weeks list');
        return this.weeks; 
    }
        
 */
 
 
    /* 
     * helper classes to define a month in terms of week and day
  
    public class Week {
     public List<Day> days; // list for days
     public Integer weekNumber; // week number
     public Date startingDate; // the date that the first of this week is on
     // so sunday of this week
     
     // return list of days
     public List<Day> getDays() { return this.days; }
     
     //constructure
     public Week () { 
        days = new List<Day>();     
     }
     
      
     
     //constructure
     public Week(Integer value,Date sunday,Integer month) { 
        this();
        weekNumber = value;
        startingDate = sunday;
        Date tmp = startingDate;
        for (Integer i = 0; i < 7; i++) {
            Day d = new Day( tmp,month ); 
            tmp = tmp.addDays(1);
            d.dayOfWeek = i+1;          
        //  system.debug(d);
            days.add(d);
        } 
        
     }
     
     //return Week Number
     public Integer getWeekNumber() { return this.weekNumber;}
     //return Starting Date
     public Date getStartingDate() { return this.startingDate;}
     
    }
    
    public class Day {
         
        public Date         theDate;
        public List<SCAL_EventItem>  eventsToday; // list of events for this date
        public List<SCAL_EventItem>  spEventsToday; // list of special events for this date
        public List<SCAL_EventItem>  sproductEventsToday;
        public Integer      month, dayOfWeek;
        public String       formatedDate; // for the formated time  
        private String      cssclass = 'calActive';
        private String      dateCssClass = 'date';//'date';
        private String      statusSymbol;
         
        public Date         getDate() { return theDate; }
        public Integer      getDayOfMonth() { return theDate.day(); }
        public String       getDayOfMonth2() { 
            if ( theDate.day() <= 9 ) 
                return '0'+theDate.day(); 
            return String.valueof( theDate.day()); 
        }
        public Integer getDayOfYear() { return theDate.dayOfYear(); }
        public List<SCAL_EventItem>  getDayAgenda() { return eventsToday; }
        public String       getFormatedDate() { return formatedDate; }
        public Integer      getDayNumber() { return dayOfWeek; }
        public List<SCAL_EventItem>  getEventsToday() { return eventsToday; }
        public List<SCAL_EventItem>  getSpEventsToday() { return spEventsToday; }
        public List<SCAL_EventItem>  getSproductEventsToday() { return sproductEventsToday;}
        public String       getCSSName() {  return cssclass; }
        public String       getDateCSSName() {  return dateCssClass; }
        public String       getStatusSymbol() {  return statusSymbol; }
        
        public Day(Date value,Integer vmonth) { 
            theDate=value; month=vmonth;        
            formatedDate = '12 21 08';// time range..
            //9:00 AM - 1:00 PM
            eventsToday = new List<SCAL_EventItem>();  
            spEventsToday= new List<SCAL_EventItem>();
            sproductEventsToday = new List<SCAL_EventItem>();  
            // three possible Inactive,Today,Active  
            if ( theDate.daysBetween(System.today()) == 0 ){ cssclass ='calToday';}
            // define inactive, is the date in the month?
            if ( theDate.month() != month) {cssclass = 'calInactive';}
            
            dateCssClass = 'date';
        }
        
        */  
        
        
    /* 
     * setStyleClass method is for set style type in page
  
        public void setStyleClass(String status) {
            if (status == null) return;
            
            if (status.equalsIgnoreCase('Booked')) {
                cssclass ='calBooked';
            } else if (status.equalsIgnoreCase('Reserved')) {
                cssclass ='calReserved';
            } else if (status.equalsIgnoreCase('Tentative')) {
                cssclass ='calTentative';
            }
        }
        
          */ 
        
    /* 
     * setStatusSymbol method is for Status Symbol in page
   
        public void setStatusSymbol(String status) {
            if (status == null) return;
            
            if (status.equalsIgnoreCase('Booked')) {
                statusSymbol='pendingBlock';
            } else if (status.equalsIgnoreCase('Reserved')||status.equalsIgnoreCase('Pitched')) {
                statusSymbol='holidayBlock';
            } else if (status.equalsIgnoreCase('Tentative') ||status.equalsIgnoreCase('Sold')) {
                statusSymbol='leaveBlock';
            }
        }
         */ 
        
    /* 
     * set cssName method for Day-block
   
        public void setSpecCSS() {
            cssclass = 'specEvent';
            dateCssClass ='speEventsHeader';
        }    
    }
   
         */
            
   
/*  static testMethod void testMe() {
        Month m = new Month( Date.newInstance(2007,11,1) );
        system.assert(m!=null); 
        List<Week> l = m.getWeeks(); 
        repeatcon r = new repeatcon(); 
        system.debug(m.getMonthName());
        Month mm = r.getMonth();
        //system.debug(mm); 
        system.debug(m.getFirstDate());
        system.debug(m.getWeekdayNames());
        for(Week w:r.getWeeks()) { 
            for(Day c:w.days) {   
                if (c.eventsToday.size() > 0 ) { 
                    String ss = String.valueOf(c.eventsToday[0].ev.ActivityDate);
                    ss = c.eventsToday[0].ev.ActivityDateTime.format('MMMM a');
                    //system.debug(ss);
                    //system.debug(c.eventsToday[0].ev);
                } 
            } 
        } 
    }*/
}