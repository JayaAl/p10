public class SCAL_Script_UpdateProducts {

// Modified By - Vardhan Gupta (02/13/2013) - code Depracated. All functionality moved into Matterhorn. Please contact "cgraham@pandora.com" for mode details 

/**
    public static void runScript() {
        List<Sponsorship_Product__c> l = new List<Sponsorship_Product__c>();
        List<Sponsorship__c> sponsorships = new List<Sponsorship__c>();
        if (Test.isRunningTest()) {
            sponsorships = [Select Ad_Product__c from Sponsorship__c where Ad_Product__c != null limit 5];
        } else {
            sponsorships = [Select Ad_Product__c from Sponsorship__c where Ad_Product__c != null];
        }
        for (Sponsorship__c s : sponsorships) {
            String adProduct = s.Ad_Product__c;
            if (adProduct == null || adProduct.equals('')) continue;
            
            for (String p : adProduct.split(';')) {
                l.add(new Sponsorship_Product__c(Sponsorship__c = s.Id, Name = p));
            }
            
        }
        
        if (l.size() > 0) {
            insert l;
        }
    }
    
    testMethod static void unit() {
        SCAL_Script_UpdateProducts.runScript();
    }
   **/ 
    
}