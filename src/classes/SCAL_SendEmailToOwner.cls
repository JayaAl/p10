/***************************************************
   Name: SCAL_SendEmailToOwner
   Usage: This class sends Email to Owner of Sponsorship
   Author – Clear Task
   Revision History
******************************************************/

public class SCAL_SendEmailToOwner{

/***

// Modified By - Vardhan Gupta (02/13/2013) - code Depracated. All functionality moved into Matterhorn. Please contact "cgraham@pandora.com" for mode details 

    public String productStr { get; set; }
    public Static final String SOLD= 'Sold'; 
    public String templateEmailLink{get; set;}
    public Static Sponsorship__c sponsorshipObj{ get; set; }
   
    
     Method for Send Mail to owner of sponsorship having Staus=Pitched and Type=Heavy-ups/Roadblocks/Limited Interruption

    public static void sendEmailStatusPitched(List<Sponsorship__c> sprList){
        System.debug('############sprList='+sprList);
        Set<String> setForType = new Set<String>();
        Set<Date> setForDate= new Set<Date>();
        List<User> salesEmail = new List<User>(); 

        String status;
        String sprType;
        for(Sponsorship__c spr : sprList){
            status=spr.Status__c;
            sprType=spr.Type__c;
            if(spr.Type__c.equals('Heavy-ups/Roadblocks/Limited Interruption')&&(spr.Status__c.equals('Pitched')||spr.Status__c.equals('Sold'))){  
                setForDate.add(spr.Date__c);
                System.debug('############setForDate=In If'+setForDate); 
            }   
        }
        System.debug('############setForDate='+setForDate); 
        List<Sponsorship__c> listEmail= [select OwnerId,Ad_Product__c,Type__c,Account__c , Start_Date_Month__c,Opportunity__c,Status__c,Date__c,Sales_Planner__c,Opportunity__r.X1st_Salesperson__r.Email,name 
                                            from  Sponsorship__c where (Type__c = 'Heavy-ups/Roadblocks/Limited Interruption' and Date__c in :setForDate)];  
        System.debug('#######listEmailfor sold#####'+listEmail);
        Set<String> salesName=new Set<String>();
        System.debug('#######Sales Rep  Email#####'+salesName);
        for(Sponsorship__c sp :listEmail){
            salesName.add(sp.Sales_Planner__c);
            System.debug('#######Sales Rep  Email#####'+sp.Opportunity__r.X1st_Salesperson__r.Email);
        }

        if (salesName.size() > 0 ) salesEmail = [select Email from User where Name in :salesName]; //updated by VG 10/11/2012
        
        
        System.debug('#######Sales Planner Email#####'+salesEmail);
        Set<Id> ownerId = new Set<Id> ();
            
        Set<String> strTo=new Set<String>(); 
            
        if(listEmail != null && listEmail.size()>0){
            for(Sponsorship__c s:listEmail){                    
                strTo.add(s.Opportunity__r.X1st_Salesperson__r.Email);
                ownerId.add(s.OwnerId);   
            }
            if( salesEmail !=null && salesEmail.size()>0){
                for (User  u :salesEmail){
                    strTo.add(u.Email);                          
                } 
             }
            String typeSponsor;
            String strOpportunity;
            date startDate;
            String startMonth;
            string product;
            String strStatus;
            String strAccount;
            String strId ;
            for(Sponsorship__c sp :sprList){
                typeSponsor=sp.Type__c;
                strOpportunity =sp.Opportunity__c;
                startDate=sp.Date__c;
                product=sp.Ad_Product__c;
                startMonth=sp.Start_Date_Month__c;
                strStatus=sp.Status__c;
                strAccount = sp.Account__c;
                strId = sp.Id;
                sponsorshipObj = sp;
            }
          //  String url = SiteSettings__c.getValues('Site URL').Site_URL__c;
          //  System.debug('url'+url );
          //  String templateEmailLink = url +'/'+strOpportunity;
            Opportunity oppName=[select name from Opportunity  where id=:strOpportunity limit 1];
            Boolean flag = false;
            String productStr = '';
            List<Sponsorship_Product__c> productName = [Select s.Name From Sponsorship_Product__c s where Sponsorship__c = :strId];
            for(Sponsorship_Product__c sp :productName){
                if(!flag){
                    productStr = productStr + sp.Name;
                    flag = true;
                     System.debug('productStr '+productStr);   
                }
                else {
                    productStr = productStr + ',' + sp.Name;
                    System.debug('productStr1 '+productStr);                     
                }
            } 
            System.debug('productStr '+productStr);
            List<String> toEmail = new List<String>();
            toEmail.addAll(strTo);
            List<String> ccEmailIdList =new List<String>();
            List<String> bccEmailIdList =new List<String>();   
            sendEmailTemplate(toEmail, ccEmailIdList, bccEmailIdList, 'SCAL Sponsorship_HeavyUp', sponsorshipObj );
   
           // Messaging.SingleEmailMessage email = new Messaging.SingleEmailMessage ();
            //String emailBody = 'There is a new Heavy-Ups/RoadBlock/Limited Interruption sponsorship in status '+strStatus+' for '+startDate.format()+'. You are being notified because you also have a Heavy-Ups/RoadBlock/Limited Interruption ' + strStatus + ' for the same date.'+'\n'+'\n'+'Sponsorship Details:' +'\n'+'\n'+ 'Opportunity Name: '  + oppName.Name+'\n'+'Ad Products: '+ productStr +'\n'+ 'Sponsorship Status: '+ strStatus+ '\n' +'Sponsorship Start Date: '+startDate.format()+'\n'+'Sponsorship Account: '+ strAccount +''+'\n'+'Opportunity Link: '+ templateEmailLink +'';
            //email.setPlainTextBody(emailBody);
            //email.setSubject('There is new Heavy-Ups/RoadBlock/Limited Interruption for '+startDate.format());  
            //email.setSaveAsActivity(false);                
            //List<String> toEmail = new List<String>();
            //toEmail.addAll(strTo);   
            //email.setToAddresses( toEmail);                 
            //try{           
              //  Messaging.SendEmailResult [] r =Messaging.sendEmail(new Messaging.SingleEmailMessage [] {email});         
            //}catch(Exception e){
              //  System.debug('Exception in sending email ' + e.getMessage());
            //}
                
           }     
    }
    
    
     // Method for Send Mail to owner of sponsorship having Staus updated from Sold 
    
    public static void sendEmailStatusSold(List<Sponsorship__c> sprList){
        Set<String> setForType = new Set<String> ();
        Set<String> setForMonth= new Set<String> ();
        String status;

        List<User> salesEmail = new List<User>();
        
        for(Sponsorship__c spr : sprList){
            System.debug('#######spr.Status__c#####'+spr.Status__c);
            status=spr.Status__c;
            if(spr.Status__c.equals('Sold')&& (!spr.Type__c.equals('Heavy-ups/Roadblocks/Limited Interruption'))){
                System.debug('!!!!!!!!!!!in if!!!!!!!!!!!');  
                setForType.add(spr.Type__c);
                setForMonth.add(spr.Start_Date_Month__c);                    
            }
        }
            
        System.debug('#######setForType#####='+setForType);
       
        List<Sponsorship__c> listEmail= [select OwnerId,Account__c ,Type__c,Start_Date_Month__c,Opportunity__c,Status__c,Date__c,Sales_Planner__c,Opportunity__r.X1st_Salesperson__r.Email,name 
                                            from  Sponsorship__c  where Type__c  in :setForType and Start_Date_Month__c in :setForMonth];
        System.debug('#######listEmailfor sold#####'+listEmail);
        Set<String> salesName=new Set<String>();

        for(Sponsorship__c sp :listEmail){
            salesName.add(sp.Sales_Planner__c);
            System.debug('#######Sales Rep  Email#####'+sp.Opportunity__r.X1st_Salesperson__r.Email);
        }
        

        if (salesName.size() > 0 ) salesEmail = [select Email from User where Name in :salesName];  //updated by VG 10/11/2012
 
  
        Set<Id> ownerId = new Set<Id> (); 
        Set<String> strTo=new Set<String>(); 
            
        if(listEmail != null && listEmail.size()>0){
            for(Sponsorship__c s:listEmail){                    
               salesName.add(s.Sales_Planner__c);               
               strTo.add(s.Opportunity__r.X1st_Salesperson__r.Email);
               ownerId.add(s.OwnerId);               
            } 
                
            if( salesEmail !=null && salesEmail.size()>0){
                for (User  u :salesEmail){
                      strTo.add(u.Email);                          
                }
            } 
           
            String typeSponsor;
            String strOpportunity;
            date startDate;
            String startMonth;
            String strStatus;
            String strAccount;
            for(Sponsorship__c sp :sprList){
                 typeSponsor=sp.Type__c;
                 strOpportunity =sp.Opportunity__c;
                 startDate=sp.Date__c;
                 startMonth=sp.Start_Date_Month__c;
                 strStatus=sp.Status__c;
                 strAccount = sp.Account__c;
                 sponsorshipObj = sp;
            }
           // String url = SiteSettings__c.getValues('Site URL').Site_URL__c;
           // System.debug('url'+url );
           // String templateEmailLink = url +'/'+strOpportunity;
            Opportunity oppName=[select name from Opportunity  where id=:strOpportunity limit 1];
            List<String> toEmail = new List<String>();
            toEmail.addAll(strTo);
            List<String> ccEmailIdList =new List<String>();
            List<String> bccEmailIdList =new List<String>();  
            
            
            //VG: updated on 10/1/2012 - send the email only if it has no warnings.
             System.debug('haswarnings-sendEmailStatusSold--- '+sponsorshipObj.Has_Warnings__c + 'SponsorhsipID :' + sponsorshipObj.Id);
            if (!sponsorshipObj.Has_Warnings__c){ 
            sendEmailTemplate(toEmail, ccEmailIdList, bccEmailIdList, 'SCAL Sponsorship_Sold', sponsorshipObj );
            }
            
           // Messaging.SingleEmailMessage email = new Messaging.SingleEmailMessage ();
           // String emailBody = 'A Sponsorship has been sold for the month of ' +startMonth+' of type '+typeSponsor+'. \n'+'You are being notified because you had a sponsorship pitched for the same type and month.'+'\n'+'\n'+'Thanks'+ '\n'+'\n'+'Sponsorship Details:' + '\n'+'\n'+ 'Sponsorship Type: ' +typeSponsor+'\n'+ 'Opportunity Name: ' + oppName.Name+'\n'+'Sponsorship Status: '+ status+'\n'+'Sponsorship Start Date: '+startDate.format()+'\n'+'Sponsorship Account: '+ strAccount + ' ' +'\n'+'Opportunity Link: '+ templateEmailLink +'';
           // email.setPlainTextBody(emailBody);
           // email.setSubject('Sponsorship sold for the month of '+startMonth);               
           // email.setSaveAsActivity(false);                
           // List<String> toEmail = new List<String>();
           // toEmail.addAll(strTo);   
           // email.setToAddresses( toEmail);                 
           // try{           
             //   Messaging.SendEmailResult [] r =Messaging.sendEmail(new Messaging.SingleEmailMessage [] {email});         
            //}catch(Exception e){ }   
        } 
    }
    
    public static void sendEmailStatusUpdatedFromSold(List<Sponsorship__c> sprList ,Sponsorship__c beforeUpdate){
        Set<Id> ownerId = new Set<Id> ();
        Set<String> setForMonth= new Set<String> ();
        Set<String> strTo=new Set<String>();
        List<User> ownerEmail = new List<User>();        
         
        for(Sponsorship__c spr : sprList){
             String status = spr.Status__c == null ? '' : spr.Status__c; 
            if(beforeUpdate.Status__c.equals(SOLD)&& (!status.equals(SOLD))){
                setForMonth.add(spr.Start_Date_Month__c); 
                system.debug('setForMonth'+setForMonth);  
            }
        }
        List<Sponsorship__c> listEmail= [select OwnerId, Account__c ,Type__c,Start_Date_Month__c,Opportunity__c,Status__c,Date__c,name from  Sponsorship__c  where Status__c = :'Pitched' and Start_Date_Month__c in :setForMonth];
        system.debug('listEmail'+listEmail);
        if(listEmail != null && listEmail.size()>0){
            for(Sponsorship__c s:listEmail){
                ownerId.add(s.OwnerId); 
            }  
        } 
        system.debug('ownerId'+ownerId); 


        if (ownerId.size() > 0) ownerEmail = [select Email from User where Id in :ownerId];   //updated by VG 10/11/2012     
        
        system.debug('ownerEmail'+ownerEmail); 
        if( ownerEmail !=null && ownerEmail.size()>0){
            for (User  u :ownerEmail){
                strTo.add(u.Email);                          
            }
        } 
        String typeSponsor;
        String strOpportunity;
        date startDate;
        String startMonth;
        String strStatus;
        String strAccount;
        for(Sponsorship__c sp :sprList){
            typeSponsor=sp.Type__c;
            strOpportunity = sp.Opportunity__c;
            startDate=sp.Date__c;
            startMonth=sp.Start_Date_Month__c;
            strStatus=sp.Status__c;
            strAccount = sp.Account__c;
            sponsorshipObj = sp;
        }
        
        //String url = SiteSettings__c.getValues('Site URL').Site_URL__c;
        //System.debug('url'+url );
        //String templateEmailLink = url +'/'+strOpportunity;
        Opportunity oppName=[select name from Opportunity  where id=:strOpportunity limit 1];
        List<String> toEmail = new List<String>();
        toEmail.addAll(strTo);
        List<String> ccEmailIdList =new List<String>();
        List<String> bccEmailIdList =new List<String>();   
        sendEmailTemplate(toEmail, ccEmailIdList, bccEmailIdList, 'SCAL Sponsorship_UpdateFrom_Sold', sponsorshipObj );
      //  Messaging.SingleEmailMessage email = new Messaging.SingleEmailMessage ();
     //   String emailBody = 'A Sponsorship which was sold for the month of '+ startMonth + ' is now free to pitch. You are being notified because you had a sponsorship pitched for the same type and month '+'\n'+'\n'+'Thanks'+ '\n'+'\n'+'Sponsorship Details:' + '\n'+'\n'+ 'Sponsorship Type: ' +typeSponsor+'\n'+ 'Opportunity Name: ' + oppName.Name+'\n'+'Sponsorship Status: '+ strStatus+'\n'+'Sponsorship Start Date: '+startDate.format()+'\n'+'Sponsorship Account: '+ strAccount +''+'\n'+'Opportunity Link: '+ templateEmailLink +''; 
     //   email.setPlainTextBody(emailBody);
     //   email.setSubject('Sponsorship sold for the month of '+startMonth + ' is now '+ strStatus +'');               
     //   email.setSaveAsActivity(false);
        
        //SCAL_EmailBodyToOwner obj = new SCAL_EmailBodyToOwner(sponsorshipObj); 
        //obj.setSponsorshipObj(sponsorshipObj);
      //  EmailTemplate templateName = [select id, ownerid from EmailTemplate where Name ='Sponsorship_Owner'];               
     //   System.debug('templateName ='+ templateName );
     //   email.setTemplateId(templateName.id);
     //   email.setTargetObjectId(templateName.ownerid); 
    //    email.setWhatId(sponsorshipObj.Id);  
     //   email.setSaveAsActivity(false);
          
        
     //   email.setToAddresses( toEmail);                 
     //   try{           
     //       Messaging.SendEmailResult [] r =Messaging.sendEmail(new Messaging.SingleEmailMessage [] {email});         
     //   }catch(Exception e){ } 
        
    }
    
       **/ 
/**
    public static void sendEmailTemplate(List<String> toMail, List<String> ccEmailIdList, List<String> bccEmailIdList, String strTemplateName, SObject sObj){
        Messaging.SingleEmailMessage email = new Messaging.SingleEmailMessage();
        email.setToAddresses(toMail);
        if(ccEmailIdList != null && ccEmailIdList.size()>0){
            email.setCcAddresses(ccEmailIdList);
        }
        if(bccEmailIdList != null && bccEmailIdList.size()>0){
            email.setBccAddresses(bccEmailIdList);
            email.setBccSender(true);
        }
        
        System.debug('strTemplateName ='+ strTemplateName );
        EmailTemplate templateName = [select id, ownerid from EmailTemplate where Name =:strTemplateName];               
        System.debug('templateName ='+ templateName );
        email.setTemplateId(templateName.id);
        email.setTargetObjectId(templateName.ownerid); 
        email.setWhatId(sObj.Id);  
        email.setSaveAsActivity(false);                       
        try{           
            Messaging.SendEmailResult [] r =Messaging.sendEmail(new Messaging.SingleEmailMessage [] {email});         
        }catch(Exception e){ } 
    } 
    
    
   **/ 
}