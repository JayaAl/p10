/***********************************************
    Class: SCAL_SponsorshipController
    This class show sponsorship detail page values
    Author – Cleartask
    Date – 18/02/2011    
    Revision History    
    
     Modified By - Vardhan Gupta (02/13/2013) - code Depracated. All functionality moved into Matterhorn. Please contact "cgraham@pandora.com" for mode details   
 ***********************************************/
public class SCAL_SponsorshipController {

/***********************************************

    //get set Type value
    public String sponsorType{get;set;} 
    //for getting record of sponsorship 
    public Sponsorship__c sponsorship{get;set;}
    List <Sponsorship_Product__c> sponsorProductList = new List<Sponsorship_Product__c>();
    //flag for rendering section
    public Boolean flagProduct{get;set;}
    //list for values from page
    List<SponsorshipProductWrapper> adProductsList ;
    //for sponsorship date
    private String  sponsorshipDate= ApexPages.currentPage().getParameters().get('sd');
    //for sponsorship type
    private String  sponsorshipType= ApexPages.currentPage().getParameters().get('st');
    //for opportunity Id 
    private String  opportunityId = ApexPages.currentPage().getParameters().get('CF00N40000001oiKq_lkid');
    //for opportunity Id 
    private String  oppIds = ApexPages.currentPage().getParameters().get('oppId');
    //static variable of heavy-ups type
    public Static final String HEAVYUPS = 'Heavy-ups/Roadblocks/Limited Interruption';
    //Message to display on page
    public String msgInfo{get; set;}
    //flag for showing error message
    public boolean msgFlag{get; set;}       
    //flag for showing type in read only mode   
    public boolean outputFlag{get; set;}
    
    public boolean isNew{get; set;}  //updated by VG 01/14/2013
    
    //Updated by VG 07/18/2012
         //Warning Message to display on page
    public String warning_msgInfo{get; set;}
    public String warning_PYMInfo{get; set;}
    //flag for showing error message
    public boolean warning_msgFlag{get; set;}
    private Sponsorship_Type__c currentSponsorshipTypeInfo = null;
     
     
    //updated by VG: 10/02/2012 - Set the HasWarnings field.
    public boolean hasWarnings{get; set;}
     
     
    //Constructor for Sponsorship values      
    public SCAL_SponsorshipController(ApexPages.StandardController stdController) {
        sponsorship = (Sponsorship__c)stdController.getRecord();
        flagProduct = false;
        msgFlag = false;
        warning_msgFlag =false;
        //updated by VG: 10/02/2012
        hasWarnings = false;
        
        if(sponsorship.Id == null){
            isNew = true;  //updated by VG 01/14/2013
            if(sponsorshipDate!= null || sponsorshipType!= null){
                Date dateSponsorship = setStringToDateFormat(sponsorshipDate);
                if(sponsorshipDate!= null){
                   sponsorship.Date__c = dateSponsorship;
                }
                if(sponsorshipType!= null){
                   sponsorship.Type__c = sponsorshipType;
                   if(sponsorshipType.equals(HEAVYUPS)){
                       flagProduct = true;
                   }else{
                       flagProduct = false;
                   }
                }
            }
        }
        else if(sponsorship.Id != null){
            isNew = false;  //updated by VG 01/14/2013
            outputFlag = true;
            Sponsorship__c sponsor = [select Name,Date__c,Comments__c,Status__c, Opportunity__c,Opportunity__r.name,Type__c,End_Date__c,Has_Warnings__c from Sponsorship__c where id = :sponsorship.Id];   
            String oppId = sponsor.Opportunity__c;  
            sponsorship.Type__c = sponsor.Type__c;
            sponsorship.Status__c = sponsor.Status__c;
            sponsorship.Comments__c = sponsor.Comments__c;
            sponsorship.Date__c = sponsor.Date__c;
            sponsorship.End_Date__c = sponsor.End_Date__c;
            sponsorship.Opportunity__c = sponsor.Opportunity__c;
            
            //VG: updated on 10/2/2012. set the hasWarnings flag.
            sponsorship.Has_Warnings__c = sponsor.Has_Warnings__c;
            if(sponsor.Type__c != null){
                if(sponsor.Type__c.equals(HEAVYUPS)){
                    flagProduct = true;
                }
                else{
                    flagProduct = false;
                     
                } 
            }
        }
    }
    
    //convert string to date format method
    public Date setStringToDateFormat(String sponsorshipDate) {
        String[] arrSponsorshipDate= sponsorshipDate.split(' ');
        String[] strDate = arrSponsorshipDate[0].split('/');
        Integer spMonth = integer.valueOf(strDate[0]);
        Integer spDate = integer.valueOf(strDate[1]);
        Integer spYear = integer.valueOf(strDate[2]);
        Date d = Date.newInstance(spYear , spMonth , spDate);
    
        return d;
    }
    
    //method for change event show ad product block
    public void showproduct(){
        if(sponsorship.Id == null){
            sponsorType = sponsorship.Type__c;
            if(sponsorType != null){
                if(sponsorType.equals(HEAVYUPS)){
                    flagProduct = true;
                }
                else{
                    flagProduct = false;
                }
            }   
        }
    }
    
    //method for getting Ad products values
    public List<SponsorshipProductWrapper> getSponsorshipProducts() {            
        adProductsList = new List<SponsorshipProductWrapper>();
        Schema.DescribeFieldResult adProductDesc = Sponsorship__c.Ad_Product__c.getDescribe(); 
        List<Schema.PicklistEntry> Ple = adProductDesc.getPicklistValues();
        List<Sponsorship_Product__c> spList = [select id ,Name,SOV__c, Impressions__c,Sponsorship__c from Sponsorship_Product__c where Sponsorship__c = :sponsorship.Id];
        Map<String, Sponsorship_Product__c> spMap = new Map<String, Sponsorship_Product__c>();
        for(Sponsorship_Product__c s :spList){
            spMap.put(s.Name, s);  
        }
        for(Schema.PicklistEntry p : ple){
            if(spMap != null && spMap.containsKey(p.getValue())){
                adProductsList.add(new SponsorshipProductWrapper(true,new Sponsorship_Product__c(name=p.getValue(), Impressions__c = spMap.get(p.getValue()).Impressions__c , SOV__c = spMap.get(p.getValue()).SOV__c , Id =spMap.get(p.getValue()).Id)));
            }
            else{
                adProductsList.add(new SponsorshipProductWrapper(false,new Sponsorship_Product__c(name=p.getValue(), Impressions__c = null , SOV__c = null , Id = null)));            
            } 
        }
      return adProductsList;  
    }
    public class SponsorshipProductWrapper{
        public boolean selected {get;set;}
        public Sponsorship_Product__c sprProduct {get;set;}
        
        public SponsorshipProductWrapper(Sponsorship_Product__c pSpProduct){
            this.sprProduct = pSpProduct;
        }
        
        public SponsorshipProductWrapper(boolean pSelected, Sponsorship_Product__c pSpProduct){
            this.sprProduct = pSpProduct;
            this.selected = pSelected;
        }
        
    }
    
    //method for inserting values in sponsorship probuct object 
    public PageReference save(){

        try{
            
            if(adProductsList != null && adProductsList.size()>0){
                String strAdProduct ='';
                for(SponsorshipProductWrapper sp :adProductsList){
                    if(sp.selected){
                        strAdProduct = strAdProduct + sp.sprProduct.Name + ','; 
                    }  
                }
                if(strAdProduct != null && strAdProduct.length()>0){
                    strAdProduct = strAdProduct.substring(0, strAdProduct.length()-1);
                    if(sponsorship != null){
                        sponsorship.Ad_Product__c = strAdProduct ;
                    }
                } 
                System.debug('strAdProduct ='+ strAdProduct );       
            }

              sponsorship.Has_Warnings__c = hasWarnings;

            upsert sponsorship;
            
            
            
        }catch(DMLException e){
            ApexPages.addMessages(e);
            return null;
        }
        List<Sponsorship_Product__c> selectedSpProductList = new List<Sponsorship_Product__c>();
        List<Sponsorship_Product__c> deleteSpProductList = new List<Sponsorship_Product__c>();
        try{   
            for(SponsorshipProductWrapper spProductWrapper : adProductsList) {
                if(spProductWrapper.selected){
                    Sponsorship_Product__c spProduct = spProductWrapper.sprProduct;
                    if(spProduct.Id == null){
                        spProduct.Sponsorship__c = sponsorship.Id;
                    }
                    if(spProduct.Impressions__c == null){
                        throw new RequiredFieldException ();
                    }else{
                        selectedSpProductList.add(spProduct);
                    }
                }else if((spProductWrapper.selected == false) && spProductWrapper.sprProduct.Impressions__c != null){
                    Sponsorship_Product__c spProduct = spProductWrapper.sprProduct;
                    System.debug('deleteSpProductList');
                    deleteSpProductList.add(spProduct);
                }
            }
            if(selectedSpProductList.size()>0){
                upsert selectedSpProductList;
            }else if( sponsorship.Type__c.equals(HEAVYUPS)){
                throw new RequiredFieldException.FieldException();
            }
            delete deleteSpProductList ;
        }catch(RequiredFieldException e){
            msgFlag  = true;
            msgInfo = System.Label.Error_Message_for_impression;
            return null;
        }
        catch(RequiredFieldException.FieldException e){
            msgFlag  = true;
            msgInfo = System.Label.Error_Message_for_product;
            return null;
        }

         PageReference pr = new PageReference('/'+sponsorship.Id);
        pr.setRedirect(true);
        return pr; 
        
       
           
    }
         
    
    //===============================================
    
     //Check if any warnings exists before this sponsorship record can be created.
    public PageReference checkWarnings(){
        Date startDate;
    
                     //added by VG 10/08/2012
        try {
            
           if (sponsorship.Type__c != null) {
           //currentSponsorshipTypeInfo = sponsorship.Type__c;
                
                
                if (sponsorship.Date__c != null) {  startDate = sponsorship.Date__c;}
                
         List<Sponsorship_Type__c>  sponsorshipTypeList =[select id,name,Type__c,Exclusive__c,Minimum_Gap__c , Max_per_Week__c ,Duration__c,Interval__c,Max_Per_Month__c from Sponsorship_Type__c where Type__c =:sponsorship.Type__c];
         if (sponsorshipTypeList != null && sponsorshipTypeList.size() > 0) {
            Map<String, Sponsorship_Type__c> sponsorshipByTypeMap = new Map<String, Sponsorship_Type__c>();
            for(Sponsorship_Type__c sponsorshipTypeInfo : sponsorshipTypeList) {
                sponsorshipByTypeMap.put(sponsorshipTypeInfo.Type__c, sponsorshipTypeInfo);
            }
         
                if ( sponsorshipByTypeMap !=null &&  sponsorshipByTypeMap.size() > 0)currentSponsorshipTypeInfo = sponsorshipByTypeMap.get(sponsorship.Type__c);
                
                  if (currentSponsorshipTypeInfo.Max_per_Week__c != null ){ 
                           
                           System.debug('currentSponsorshipTypeInfo.Max_per_Week__c--'+currentSponsorshipTypeInfo.Max_per_Week__c);
                       
                           integer sponsorshipWeekCount = 0;
                           sponsorshipWeekCount  =  SCAL_Utils.getAllSponsorshipsByTypeforWeek(sponsorship, true); 
                          
                           if(sponsorshipWeekCount >= currentSponsorshipTypeInfo.Max_per_Week__c) {
                          warning_msgFlag = true;
                              warning_msgInfo = 'A maximum of ' + currentSponsorshipTypeInfo.Max_per_Week__c  + ' per week for the '  + sponsorship.Type__c + ' Sponsorship type have been reached for this period.';
                              warning_PYMInfo = 'Make sure you’ve reached out to PYM for appropriate approvals to book this exception.';
                      
                      if (warning_msgFlag) { hasWarnings = true; }else hasWarnings =false;
              
             sponsorship.Date__c = startDate;
             sponsorship.Has_Warnings__c = hasWarnings;   
                      
          
                      

                     // ApexPages.Message myMsg = new ApexPages.Message(ApexPages.Severity.WARNING, warning_msgInfo);
                   // ApexPages.addMessage(myMsg);
           return null;
                      }
                               // sponsorshipInfo.addError('There can be a maximum of ' + currentSponsorshipTypeInfo.Max_per_Week__c  + ' '  + sponsorshipInfo.Type__c + ' Sponsorships per Week ');
                       }
                
                       Integer sponsorshipMinCount = 0;
                       if(currentSponsorshipTypeInfo.Minimum_Gap__c != null){
                            
                            sponsorshipMinCount = SCAL_Utils.getSponsorshipCountFallsInMinGapCurrentSponsorshipDate(sponsorship, true, currentSponsorshipTypeInfo);
                            
                           if(sponsorshipMinCount != 0) { 
                           warning_msgFlag = true;
                          warning_msgInfo = 'There must be at least ' + currentSponsorshipTypeInfo.Minimum_Gap__c + ' day gap between bookings for ' +  sponsorship.Type__c + ' Sponsorship type.';
                          warning_PYMInfo = 'Make sure you’ve reached out to PYM for appropriate approvals to book this exception.';
                               
                               if (warning_msgFlag) { hasWarnings = true; }else hasWarnings =false;

                          sponsorship.Date__c = startDate;
                          sponsorship.Has_Warnings__c = hasWarnings;   
  
         return null;
                        }                      
                   }
             
             //updated by VG 01/07/2012.
             //Display warning message for Pandora One trial subscriptions if trying to book more that one.
              Set<String> sponsorshipTypeSet = new Set<String>();
              sponsorshipTypeSet.add(sponsorship.Type__c); 
               Map<String,List<Sponsorship__c>> sponsorshipListByTypeMap = SCAL_Utils.getAllSponsorshipsByType(sponsorshipTypeSet);
                 Integer sponsorshipCount = 0;
                  sponsorshipCount = SCAL_Utils.getSponsorshipCountFallsInCurrentSponsorshipDate(sponsorship, sponsorshipListByTypeMap.get(sponsorship.Type__c) , true); 
                      
                        if (sponsorshipCount > 0 && sponsorship.Type__c.equalsIgnoreCase('Pandora One Free Trial') && sponsorship.status__c.equalsIgnoreCase('Sold') ) {
                            
                            
                            warning_msgFlag = true;
                          warning_msgInfo = 'There is a '+ sponsorship.Type__c +' sold for this period.';
                          warning_PYMInfo = 'Make sure you’ve reached out to Ad Product Packaging for appropriate approvals to book this exception.';
                               
                               if (warning_msgFlag) { hasWarnings = true; }else hasWarnings =false;
                                  sponsorship.Has_Warnings__c = hasWarnings;   
                        
                                     return null;
                            
                        }
             
               }                    
          }       
   
    }
        catch(exception e) {
            ApexPages.Message myMsg = new ApexPages.Message(ApexPages.Severity.ERROR, 'Invalid Sponsdr ID');
            ApexPages.addMessage(myMsg);
                   return null;
         
        }
        
       //If no warnings then continue with SAVE.
       if (!warning_msgFlag) {

        try{
            
            if(adProductsList != null && adProductsList.size()>0){
                String strAdProduct ='';
                for(SponsorshipProductWrapper sp :adProductsList){
                    if(sp.selected){
                        strAdProduct = strAdProduct + sp.sprProduct.Name + ','; 
                    }  
                }
                if(strAdProduct != null && strAdProduct.length()>0){
                    strAdProduct = strAdProduct.substring(0, strAdProduct.length()-1);
                    if(sponsorship != null){
                        sponsorship.Ad_Product__c = strAdProduct ;
                    }
                } 
                System.debug('strAdProduct ='+ strAdProduct );       
            }
                 
            
           
            
            
                sponsorship.Date__c = startDate;
             sponsorship.Has_Warnings__c = hasWarnings;   
                      
          


            upsert sponsorship;
            
            
            
        }catch(DMLException e){
            ApexPages.addMessages(e);
            return null;
        }
        List<Sponsorship_Product__c> selectedSpProductList = new List<Sponsorship_Product__c>();
        List<Sponsorship_Product__c> deleteSpProductList = new List<Sponsorship_Product__c>();
        try{   
            for(SponsorshipProductWrapper spProductWrapper : adProductsList) {
                if(spProductWrapper.selected){
                    Sponsorship_Product__c spProduct = spProductWrapper.sprProduct;
                    if(spProduct.Id == null){
                        spProduct.Sponsorship__c = sponsorship.Id;
                    }
                    if(spProduct.Impressions__c == null){
                        throw new RequiredFieldException ();
                    }else{
                        selectedSpProductList.add(spProduct);
                    }
                }else if((spProductWrapper.selected == false) && spProductWrapper.sprProduct.Impressions__c != null){
                    Sponsorship_Product__c spProduct = spProductWrapper.sprProduct;
                    System.debug('deleteSpProductList');
                    deleteSpProductList.add(spProduct);
                }
            }
            if(selectedSpProductList.size()>0){
                upsert selectedSpProductList;
            }else if( sponsorship.Type__c.equals(HEAVYUPS)){
                throw new RequiredFieldException.FieldException();
            }
            delete deleteSpProductList ;
        }catch(RequiredFieldException e){
            msgFlag  = true;
            msgInfo = System.Label.Error_Message_for_impression;
            return null;
        }
        catch(RequiredFieldException.FieldException e){
            msgFlag  = true;
            msgInfo = System.Label.Error_Message_for_product;
            return null;
        }

        
         PageReference pr = new PageReference('/'+sponsorship.Id);
        pr.setRedirect(true);
        return pr; 
        

       }
       else return null;
                
       
    }
    
    
    //==================================================
    
    
//updated by VG 07/18/2012
//navigate method - Redirect back to detail page.
  public PageReference navigate(){
   PageReference pr = new PageReference('/'+sponsorship.Id);
       pr.setRedirect(true);
        return pr; 
  }
    
    //updated by VG 08/14/2012
    //delete existing sponsorship and navigate back to the calendar.
    public PageReference cancelNavigate() {
          //updated by VG 07/31/2012
        //if the user hits cancel on the pop-up stay on the edit page.
       if (warning_msgFlag && sponsorship.Id != null) {
  
           delete sponsorship; //updated by VG 01/14/2013
              
          
        //PageReference currentSponsorshipPage = new ApexPages.StandardController(sponsorship).edit();
        //currentSponsorshipPage.setRedirect(true);
        //return currentSponsorshipPage;
            
      }
        return new PageReference('/apex/SponsorshipCalendar'); 
    }
    
    //cancel method for returning to opportunity 
    public PageReference cancel(){
       if(sponsorship.Id == null){
           if(sponsorshipDate!= null || sponsorshipType!= null){
               return new PageReference('/apex/SponsorshipCalendar');
           }
       }
              
       if(sponsorship.Id != null){
           Sponsorship__c sponsor = [select Opportunity__c,Opportunity__r.name from Sponsorship__c where id = :sponsorship.Id];
           String oppId = sponsor.Opportunity__c;
           PageReference pre = new PageReference('/'+oppId);
           pre.setRedirect(true);
           return pre;
       
       }

       PageReference pr = new PageReference('/'+opportunityId);
       pr.setRedirect(true);
       return pr;
    }
    
    ***********************************************/
}