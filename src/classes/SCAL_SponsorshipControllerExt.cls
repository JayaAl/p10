public without sharing class SCAL_SponsorshipControllerExt {

/*********************************************************
 Modified By - Vardhan Gupta (02/13/2013) - code Depracated. All functionality moved into Matterhorn. Please contact "cgraham@pandora.com" for mode details   


    private static final String SELECTED_ID_STR='selectedId';
    //public List<Sponsorship_Type__c> sponsorshipTypeList {get; set;}
    private Integer nextId = 0;
    private String selectedId;
    public Boolean error {get; private set;}
    public List<SponsorshipRow> sponsorshipTypeRows { get; private set; }
    private List<Sponsorship_Type__c> deleted = new List<Sponsorship_Type__c>();
    public List<SelectOption> sponsorshipOptions {get; private set;}
     
    public SCAL_SponsorshipControllerExt(ApexPages.StandardController controller) {
        //this.sponsorshipTypeList = [ select name,Duration__c,Exclusive__c,Interval__c,Max_Per_Month__c,Type__c from Sponsorship_Type__c order by Type__c];
        this.sponsorshipTypeRows = new List<SponsorshipRow>();
        loadSponsorshipTypeOptions();
    }
    
    public PageReference onLoad() {
        this.sponsorshipTypeRows.clear();
        for (Sponsorship_Type__c sponsorshipTypeInfo : [select name,Duration__c,Exclusive__c,Interval__c,Max_Per_Month__c,
            Max_per_Week__c, Minimum_Gap__c, Type__c from Sponsorship_Type__c order by Name]) 
        {
            nextId++;
            this.sponsorshipTypeRows.add(new SponsorshipRow(String.valueOf(nextId),sponsorshipTypeInfo ));
        }
        
        if (sponsorshipTypeRows.size() == 0) {
            nextId++;
            this.sponsorshipTypeRows.add(new SponsorshipRow(String.valueOf(nextId), new Sponsorship_Type__c()));
        }
        
        return null;
    }   
    
    public PageReference onSave() {
        delete deleted;
        try {
            List<Sponsorship_Type__c> batch = new List<Sponsorship_Type__c>();
            for (SponsorshipRow row : sponsorshipTypeRows) {  
                Sponsorship_Type__c sponsorshipType = row.getSponsorshipType();
                sponsorshipType.Name = sponsorshipType.Type__c;
                batch.add(sponsorshipType);
            }
            upsert batch;
        }
        catch (Exception e) {
            ApexPages.addMessages(e); 
            return null;
        }
        return null;
    }
    
    public PageReference onCancel() {
        return null;
    }
    
    
    public PageReference onAddCondition() {
        String selectedId = ApexPages.currentPage().getParameters().get(SELECTED_ID_STR);
        if (selectedId != null) {
            for (Integer i=0;i<sponsorshipTypeRows.size();i++) {
                SponsorshipRow row = sponsorshipTypeRows.get(i);
                if (row.getId().equals(selectedId)) {
                    nextId++;
                    if (i == (sponsorshipTypeRows.size() - 1)) {
                        sponsorshipTypeRows.add(new SponsorshipRow(String.valueOf(nextId), new Sponsorship_Type__c()));
                    } else {
                        sponsorshipTypeRows.add(i + 1, new SponsorshipRow(String.valueOf(nextId),new Sponsorship_Type__c()));
                    }
                    return null;
                }
            }
        }        
        return null;
    }
    
    public PageReference onRemoveCondition() {
        String selectedId = ApexPages.currentPage().getParameters().get(SELECTED_ID_STR);
        if (selectedId != null) {
            for (Integer i = 0; i < sponsorshipTypeRows.size(); i++) {
                SponsorshipRow row = sponsorshipTypeRows.get(i);
                if (row.getId().equals(selectedId)) {
                    sponsorshipTypeRows.remove(i);
                    if (row.getSponsorshipType().Id != null) {
                        deleted.add(row.getSponsorshipType());
                    }
                    return null;
                }
            }
        }
        return null;
    }
    
    private void loadSponsorshipTypeOptions() {
        Schema.DescribeFieldResult field = Sponsorship__c.Type__c.getDescribe();
        List<Schema.PicklistEntry> pickListValues = field.getPicklistValues();
        this.sponsorshipOptions = new List<SelectOption>();
        this.sponsorshipOptions.add(new SelectOption('','None'));
        for (Schema.PicklistEntry picklistEntry : pickListValues) {
            this.sponsorshipOptions.add(new SelectOption(picklistEntry.getValue(),picklistEntry.getLabel()));
        }
    }

    
    public List<SelectOption> getSponsorshipTypes() {
        return sponsorshipOptions;      
    }
    
     public class SponsorshipRow {
        private String id;
        public Sponsorship_Type__c sponsorshipType {get;set;}
        
        public SponsorshipRow (String id, Sponsorship_Type__c sponsorshipType) {
            this.Id = id;
            this.sponsorshipType = sponsorshipType;
        }
        
        public String getId() {
            return id;
        }
        
        public Sponsorship_Type__c getSponsorshipType() {
            return sponsorshipType;
        }
          
     }
    
     ***********************************************************/

}