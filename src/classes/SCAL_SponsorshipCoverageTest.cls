@istest
private class SCAL_SponsorshipCoverageTest {
    
    /*********************************************************
 Modified By - Vardhan Gupta (02/13/2013) - code Depracated. All functionality moved into Matterhorn. Please contact "cgraham@pandora.com" for mode details   

 static testmethod void sposnsorshiptest()
 
 {  
    
    //O1RestHelper.isApexTest=true;
      
          System.test.starttest();
        User u = [Select Id from User where Isactive = true and Profile.Name = 'System Administrator' limit 1];
         //create Account
        Account acc = new   Account(Name ='test Account1',Type='Advertiser', BillingStreet='2101 Webster St', BillingCity='Oakland',  BillingState='CA', BillingPostalCode='94619');
        insert(acc);
        
         // updated by VG 12/26/2012
          Contact conObj = UTIL_TestUtil.generateContact(acc.id);
        insert conObj;
        
      //create new Opportunity - Start            
        datetime t = System.now()+2;            
        date d = Date.newInstance(t.year(),t.month(),t.day());            
        Opportunity opp=new Opportunity(name='test opportunity',AccountId=acc.id, Primary_Billing_Contact__c =conObj.Id, StageName='Closed Won', CloseDate=System.today().adddays(-6),
                        ContractStartDate__c=d,ContractEndDate__c=System.today().addMonths(6),Industry_Category__c='Political',
                        Sales_Planner__c='Victoria Bair',Psychographic_Audience_Targets__c='Browser',Target_Audience_End_Age__c='35',
                        Target_Audience_Gender__c='Male',Target_Audience_Start_Age__c='15',OwnerId=u.id,Confirm_direct_relationship__c=true,Industry_Sub_Category__c='Political Campaign Marketing',Type='Advertiser');            
       opp.StageName='Qualified';
        insert(opp);            
       Sponsorship__c spr1 = new Sponsorship__c(Type__c='Heavy-ups/Roadblocks/Limited Interruption',Status__c='Lost', Opportunity__c=opp.id, Date__c=System.today().addMonths(2));
       Sponsorship__c spr2 = new Sponsorship__c(Type__c='Heavy-ups/Roadblocks/Limited Interruption',Status__c='Lost', Opportunity__c=opp.id, Date__c=System.today().addMonths(2));
       
       Sponsorship__c spr3 = new Sponsorship__c(Type__c='CE Device Daily Takeover',Status__c='Sold', Opportunity__c=opp.id, Date__c=System.today().adddays(2));
        Sponsorship__c spr4 = new Sponsorship__c(Type__c='CE Device Daily Takeover',Status__c='Sold', Opportunity__c=opp.id, Date__c=System.today().adddays(3));
        
        Sponsorship__c spr5 = new Sponsorship__c(Type__c='CE Device Daily Takeover',Status__c='Sold', Opportunity__c=opp.id, Date__c=System.today().adddays(4));
        
          Sponsorship__c spr6 = new Sponsorship__c(Type__c='Pandora One Free Trial',Status__c='Sold', Opportunity__c=opp.id, Date__c=System.today().adddays(7));
        
          Sponsorship__c spr7 = new Sponsorship__c(Type__c='Pandora One Free Trial',Status__c='Sold', Opportunity__c=opp.id, Date__c=System.today().adddays(7));
        
        insert(spr1);
        
         insert(spr3);
        insert(spr4);
        insert(spr5);
       // insert(spr6);
      //  insert(spr7);
        
         Sponsorship_Type__c sprTypeCE = new Sponsorship_Type__c(Type__c='CE Device Daily Takeover',Duration__c='1',Interval__c='Day',Max_per_Week__c=2, Minimum_Gap__c=1);
       insert (sprTypeCE);
        
        PageReference VFpage = Page.SponsorshipOverride;
        VFpage.getParameters().put('sd',String.valueof(Date.today()));
         VFpage.getParameters().put('st','Heavy-ups/Roadblocks/Limited Interruption');
          VFpage.getParameters().put('oppId',opp.Id);
         VFpage.getParameters().put('CF00N40000001oiKq_lkid','1234');
        
        test.setCurrentPage(VFpage);
        ApexPages.StandardController VFpage_Extn = new ApexPages.StandardController(spr1);
        SCAL_SponsorshipController CLS = new SCAL_SponsorshipController(VFpage_Extn);
        
      cls.showproduct();
        cls.getSponsorshipProducts();
        
           cls.checkWarnings(); //update by VG 10/09/2012
          cls.save();
          cls.cancel();
         // cls.setStringToDateFormat(String.valueof(Date.today()));
         
         
           VFpage = Page.SponsorshipOverride;
        VFpage.getParameters().put('sd',String.valueof(System.Now().format('mm/dd/yyyy')));
         VFpage.getParameters().put('st','Heavy-ups/Roadblocks/Limited Interruption');
          VFpage.getParameters().put('oppId',opp.Id);
         VFpage.getParameters().put('CF00N40000001oiKq_lkid','1234');
        
        test.setCurrentPage(VFpage);
       VFpage_Extn = new ApexPages.StandardController(spr2);
         CLS = new SCAL_SponsorshipController(VFpage_Extn);
        
      cls.showproduct();
        cls.getSponsorshipProducts();
        cls.checkWarnings(); //update by VG 10/09/2012
          cls.save();
          cls.cancel();
          
          
            VFpage = Page.SponsorshipOverride;
        VFpage.getParameters().put('sd',String.valueof(System.Now().format('mm/dd/yyyy')));
         VFpage.getParameters().put('st','CE Device Daily Takeover');
          VFpage.getParameters().put('oppId',opp.Id);
         VFpage.getParameters().put('CF00N40000001oiKq_lkid','1234');
        
        test.setCurrentPage(VFpage);
       VFpage_Extn = new ApexPages.StandardController(spr3);
         CLS = new SCAL_SponsorshipController(VFpage_Extn);
        
     cls.showproduct();
        cls.getSponsorshipProducts();
        cls.checkWarnings(); //update by VG 10/09/2012
         cls.save();
          cls.navigate();
          cls.cancel();
        
          
          
             VFpage = Page.SponsorshipOverride;
        VFpage.getParameters().put('sd',String.valueof(System.Now().format('mm/dd/yyyy')));
         VFpage.getParameters().put('st','CE Device Daily Takeover');
          VFpage.getParameters().put('oppId',opp.Id);
         VFpage.getParameters().put('CF00N40000001oiKq_lkid','1234');
        
        test.setCurrentPage(VFpage);
       VFpage_Extn = new ApexPages.StandardController(spr4);
         CLS = new SCAL_SponsorshipController(VFpage_Extn);
        
     cls.showproduct();
        cls.getSponsorshipProducts();
        cls.checkWarnings(); //update by VG 10/09/2012
         cls.save();
           cls.navigate();
         cls.cancelNavigate();
         
         
           VFpage = Page.SponsorshipOverride;
        VFpage.getParameters().put('sd',String.valueof(System.Now().format('mm/dd/yyyy')));
         VFpage.getParameters().put('st','CE Device Daily Takeover');
          VFpage.getParameters().put('oppId',opp.Id);
         VFpage.getParameters().put('CF00N40000001oiKq_lkid','1234');
        
        test.setCurrentPage(VFpage);
       VFpage_Extn = new ApexPages.StandardController(spr5);
         CLS = new SCAL_SponsorshipController(VFpage_Extn);
        
     cls.showproduct();
        cls.getSponsorshipProducts();
        cls.checkWarnings(); //update by VG 10/09/2012
         cls.save();
           cls.navigate();
         cls.cancelNavigate();
          
          
          
      System.test.stoptest();
        
 } 
 
 static testmethod void sposnsorshiptest1()
 
 {  
    
    //O1RestHelper.isApexTest=true;
      
       System.test.starttest();
        User u = [Select Id from User where Isactive = true and Profile.Name = 'System Administrator' limit 1];
         //create Account
        Account acc = new   Account(Name ='test Accountss1',Type='Advertiser', BillingStreet='2101 Webster St', BillingCity='Oakland',  BillingState='CA', BillingPostalCode='94619');
        insert(acc);
  
  // updated by VG 12/26/2012
          Contact conObj = UTIL_TestUtil.generateContact(acc.id);
        insert conObj;
  
      //create new Opportunity - Start            
        datetime t = System.now()+2;            
        date d = Date.newInstance(t.year(),t.month(),t.day());            
        Opportunity opp=new Opportunity(name='test opportunity',AccountId=acc.id, Primary_Billing_Contact__c =conObj.Id, StageName='Closed Won', CloseDate=System.today().adddays(-6),
                        ContractStartDate__c=d,ContractEndDate__c=System.today().addMonths(6),Industry_Category__c='Political',
                        Sales_Planner__c='Victoria Bair',Psychographic_Audience_Targets__c='Browser',Target_Audience_End_Age__c='35',
                        Target_Audience_Gender__c='Male',Target_Audience_Start_Age__c='15',OwnerId=u.id,Confirm_direct_relationship__c=true,Industry_Sub_Category__c='Political Campaign Marketing',Type='Advertiser');            
       opp.StageName='Qualified';
        insert(opp);            
       Sponsorship__c spr1 = new Sponsorship__c(Type__c='Heavy-ups/Roadblocks/Limited Interruption',Status__c='Lost', Opportunity__c=opp.id, Date__c=System.today().addMonths(2));
      
        insert(spr1);
          Sponsorship_Type__c spt = new Sponsorship_Type__c(Type__c='Heavy-ups/Roadblocks/Limited Interruption',Duration__c='1',Interval__c='Day',Max_Per_Month__c=10);
          insert (spt);
        PageReference VFpage = Page.AddSponsorshipTypes;
           VFpage.getParameters().put('selectedId',spt.Id);
        test.setCurrentPage(VFpage);
        ApexPages.StandardController VFpage_Extn = new ApexPages.StandardController(spt);
        SCAL_SponsorshipControllerExt CLS = new SCAL_SponsorshipControllerExt(VFpage_Extn);
        cls.getSponsorshipTypes();
        cls.onLoad();
        cls.onAddCondition();
           cls.onSave();
         cls.onAddCondition();  
           cls.onRemoveCondition(); 
    
      System.test.stoptest();
        
 }
    static testmethod void testpopulateDataValuesOnSponsorship (){
        System.test.starttest();
        User u = [Select Id from User where Isactive = true and Profile.Name = 'System Administrator' limit 1];
         //create Account
        Account acc = new   Account(Name ='test Accountss1',Type='Advertiser' , BillingStreet='2101 Webster St', BillingCity='Oakland',  BillingState='CA', BillingPostalCode='94619');
        insert(acc);
  
   // updated by VG 12/26/2012
          Contact conObj = UTIL_TestUtil.generateContact(acc.id);
        insert conObj;
  
      //create new Opportunity - Start            
        datetime t = System.now()+2;            
        date d = Date.newInstance(t.year(),t.month(),t.day());   
        Sponsorship_Type__c spt = new Sponsorship_Type__c(Type__c='Heavy-ups/Roadblocks/Limited Interruption',Duration__c='1',Interval__c='Day',Max_Per_Month__c=10, Minimum_Gap__c = 1, Max_per_Week__c = 2);
        insert (spt);         
        Opportunity opp=new Opportunity(name='test opportunity',AccountId=acc.id, StageName='Closed Won',  Primary_Billing_Contact__c =conObj.Id,  CloseDate=System.today().adddays(-6),
                        ContractStartDate__c=d,ContractEndDate__c=System.today().addMonths(6),Industry_Category__c='Political',
                        Sales_Planner__c='Victoria Bair',Psychographic_Audience_Targets__c='Browser',Target_Audience_End_Age__c='35',
                        Target_Audience_Gender__c='Male',Target_Audience_Start_Age__c='15',OwnerId=u.id,Confirm_direct_relationship__c=true,Industry_Sub_Category__c='Political Campaign Marketing',Type='Advertiser');            
       opp.StageName='Qualified';
       insert(opp);            
       Sponsorship__c spr1 = new Sponsorship__c(Type__c='Heavy-ups/Roadblocks/Limited Interruption',Status__c='Lost', Opportunity__c=opp.id, Date__c=System.today().addMonths(1));
      
        insert(spr1);
        Date nextMonth = System.today().addMonths(1);
        Sponsorship__c spr2 = new Sponsorship__c(Type__c='Heavy-ups/Roadblocks/Limited Interruption',Status__c='Lost', Opportunity__c=opp.id, Date__c= nextMonth.addDays(2));
        insert(spr2); 
        Sponsorship__c spr3 = new Sponsorship__c(Type__c='Heavy-ups/Roadblocks/Limited Interruption',Status__c='Lost', Opportunity__c=opp.id, Date__c= nextMonth.addDays(3), End_Date__c = System.today().addMonths(3));
        insert(spr3);  
         spr3.Date__c =   nextMonth.addDays(4);
         update(spr3);   
          System.test.stoptest();     
    }
    
     ***********************************************************/
    
}