@isTest
private class SCAL_Test_CalendarController {


/*********************************************************
 Modified By - Vardhan Gupta (02/13/2013) - code Depracated. All functionality moved into Matterhorn. Please contact "cgraham@pandora.com" for mode details   


    static testmethod void testCalendarController(){    
       SCAL_CalendarController obj = new SCAL_CalendarController();
       datetime t = System.now()+2;           
       date d = Date.newInstance(t.year(),t.month(),t.day());        
       obj.getWeeks();      
       obj.getSponsorStatusList();
       obj.getSponsorTypeList();
       obj.searchSponsorByType();
       obj.toString();
       //obj.searchSponsorByYear();
       //obj.searchSponsorByMonth();
       obj.getSelectedYearList();
       obj.getSelectedMonthList();
    }
    static testmethod void testEventItem(){
        Sponsorship__c sObj=new Sponsorship__c();
        List<Sponsorship__c> sponsorships = [ select Name, id,Date__c,Status__c,Ad_Product__c, Opportunity__c,Opportunity__r.name,End_Date__c from Sponsorship__c where Type__c ='Heavy-ups/Roadblocks/Limited Interruption' and Status__c!='Lost' order by Date__c limit 1];
        if(sponsorships != null && sponsorships.size()>0)sObj=sponsorships[0];
        SCAL_EventItem obj = new SCAL_EventItem(sObj);
        obj.getEv();
        obj.getFormatedDate();
        obj.getName();
        obj.getOpportunity();
        obj.getProduct();
        obj.getStatus();
        Special_Event__c seObj=new Special_Event__c();
        List<Special_Event__c> listSpecialEvent=[select Date__c,Description__c from Special_Event__c ];
        if(listSpecialEvent!= null && listSpecialEvent.size()>0)seObj=listSpecialEvent[0];
        SCAL_EventItem objSe = new SCAL_EventItem(seObj);
        objSe.getEvSpe();
        objSe.getFormatedDateSpe();
        Sponsorship_Product__c spObj = new Sponsorship_Product__c();
        List<Sponsorship_Product__c> sponsorshipProduct = [Select s.Sponsorship__c, s.Name, s.Id, s.Sponsorship__r.Date__c, s.Sponsorship__r.End_Date__c, s.Sponsorship__r.Status__c, s.Sponsorship__r.Opportunity__c  From Sponsorship_Product__c s where s.Sponsorship__r.Type__c ='Heavy-ups/Roadblocks/Limited Interruption' and s.Sponsorship__r.Status__c!='Lost' order by s.Sponsorship__r.Date__c];  
        SCAL_EventItem objSp = new SCAL_EventItem(spObj);
        objSp.getEvSp();
        objSp.getOpportunitySp();
        objSp.getProductName();
        objSp.getStatusSp();
        //objSp.formatedDateSpe();   
    }
    static testmethod void testMonth(){
        Date today = system.today().toStartOfWeek();
        SCAL_Month obj = new SCAL_Month(today );
        obj.getValidDateRange();
        obj.getMonthName();
        obj.getYearName();
        obj.getWeekdayNames();
        obj.getfirstDate();
        List<Sponsorship__c> sponsorships = [ select Name, id,Date__c,Status__c,Ad_Product__c, Opportunity__c,Opportunity__r.name,End_Date__c from Sponsorship__c where Type__c ='Heavy-ups/Roadblocks/Limited Interruption' and Status__c!='Lost' order by Date__c limit 100];
        List<Sponsorship_Product__c> sponsorshipProduct = [Select s.Sponsorship__c, s.Name, s.Id, s.Sponsorship__r.Date__c, s.Sponsorship__r.End_Date__c, s.Sponsorship__r.Status__c, s.Sponsorship__r.Opportunity__c  From Sponsorship_Product__c s where s.Sponsorship__r.Type__c ='Heavy-ups/Roadblocks/Limited Interruption' and s.Sponsorship__r.Status__c!='Lost' order by s.Sponsorship__r.Date__c]; 
        obj.setEvents(sponsorships ,sponsorshipProduct );
        obj.getWeeks();
        SCAL_Month.Week  objWeek=new  SCAL_Month.Week();
        Integer value=1;
        Integer month=1;
        SCAL_Month.Week objWeek1=new SCAL_Month.Week(value,today ,month);
        objWeek.getDays();
        objWeek.getWeekNumber();
        objWeek.getStartingDate();
        SCAL_Month.Day objDay =new  SCAL_Month.Day(today ,month);
        objDay.getDate();
        objDay.getDayOfMonth();
        objDay.getDayOfMonth2();
        objDay.getDayOfYear();
        objDay.getDayAgenda();
        objDay.getFormatedDate();
        objDay.getDayNumber();
        objDay.getEventsToday();
        objDay.getSpEventsToday();
        objDay.getCSSName();
        objDay.getDateCSSName();
        objDay.getStatusSymbol();
        objDay.setStatusSymbol('Booked');
        //objDay.objDaysetSpecCSS();
    }
    
    static testmethod void testStatusValues() {
        SCAL_CalendarController obj = new SCAL_CalendarController();
        System.assertEquals(obj.getBooked(),'Booked');
        System.assertEquals(obj.getPitched(),'Pitched');
        System.assertEquals(obj.getSold(),'Sold');
    }
    
    static testmethod void testSearchSponsorByMonth() {
        SCAL_CalendarController obj = new SCAL_CalendarController();
        obj.sponsorType = null;
        obj.searchSponsorByMonth();
        System.assertEquals(obj.ShowProduct,'All');

        obj.sponsorType = 'Heavy-ups/Roadblocks/Limited Interruption';
        obj.searchSponsorByMonth();
        System.assertEquals(obj.ShowProduct,'Product');

        obj.sponsorType = 'Test';
        obj.searchSponsorByMonth();
        System.assertEquals(obj.ShowProduct,'Opp');

    }
    
    static testmethod void testSearchSponsorByYear() {
        SCAL_CalendarController obj = new SCAL_CalendarController();
        obj.sponsorType = null;
        obj.searchSponsorByYear();
        System.assertEquals(obj.ShowProduct,'All');

        obj.sponsorType = 'Heavy-ups/Roadblocks/Limited Interruption';
        obj.searchSponsorByYear();
        System.assertEquals(obj.ShowProduct,'Product');

        obj.sponsorType = 'Test';
        obj.searchSponsorByYear();
        System.assertEquals(obj.ShowProduct,'Opp');

    }
 ***********************************************************/
}