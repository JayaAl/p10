@isTest
private class SCAL_Test_SendEmailToOwner{


/*********************************************************
 Modified By - Vardhan Gupta (02/13/2013) - code Depracated. All functionality moved into Matterhorn. Please contact "cgraham@pandora.com" for mode details   


    static testmethod void testSendEmailForSold(){
        //O1RestHelper.isApexTest=true;
      //create new User - Start
         Profile p = [select id from Profile where name like 'Standard Platform%' limit 1];  
          User u = new User(LastName='Test User',alias = 'test',email='test@testorg.com',emailencodingkey='UTF-8',
                           languagelocalekey='en_US',localesidkey='en_US',timezonesidkey='America/Los_Angeles', 
                           username='testuser@testorg.com',profileId = p.Id);
          insert u;
          System.test.starttest();
        User u = [Select Id from User where Isactive = true and Profile.Name = 'System Administrator' limit 1];
      //create new User - End


      //create Account
        Account acc = new   Account(Name ='test Account1',Type='Advertiser', BillingStreet='2101 Webster St', BillingCity='Oakland',  BillingState='CA', BillingPostalCode='94619');
        insert(acc);
  
      // updated by VG 12/26/2012
          Contact conObj = UTIL_TestUtil.generateContact(acc.id);
        insert conObj;
  
      //create new Opportunity - Start            
        datetime t = System.now()+2;            
        date d = Date.newInstance(t.year(),t.month(),t.day());            
        Opportunity opp=new Opportunity(name='test opportunity',AccountId=acc.id, Primary_Billing_Contact__c =conObj.Id, StageName='Closed Won', CloseDate=System.today().adddays(-6),
                        ContractStartDate__c=d,ContractEndDate__c=System.today().addMonths(6),Industry_Category__c='Political',
                        Sales_Planner__c='Victoria Bair',Psychographic_Audience_Targets__c='Browser',Target_Audience_End_Age__c='35',
                        Target_Audience_Gender__c='Male',Target_Audience_Start_Age__c='15',OwnerId=u.id,Confirm_direct_relationship__c=true,Industry_Sub_Category__c='Political Campaign Marketing',Type='Advertiser');            
        insert(opp);            
      //create new Opportunity - End
    
      //create New Sponsorship - start
        datetime t1 = System.now();            
        date d1 = Date.newInstance(t1.year(),t1.month(),t1.day());  
        List<Sponsorship_Type__c>   sprList = new List<Sponsorship_Type__c>();        
             
          Sponsorship_Type__c spr1 = new Sponsorship_Type__c(Type__c='Heavy-ups/Roadblocks/Limited Interruption',Duration__c='1',Interval__c='Day',Max_Per_Month__c=10);
       Sponsorship_Type__c spr2 = new Sponsorship_Type__c(Type__c='Ipad',Duration__c='1',Interval__c='Week',Max_Per_Month__c=5);
     Sponsorship_Type__c spr3 = new Sponsorship_Type__c(Type__c='CE Device Takeover',Duration__c='1',Interval__c='Day',Max_Per_Month__c=20);
       sprList.add(spr1);
       sprList.add(spr2);
       sprList.add(spr3);
        insert (sprList );      
      //create New Sponsorship - End
      List<Sponsorship__c> spList = new List<Sponsorship__c>();
           Sponsorship__c spr = new Sponsorship__c(Type__c='Ipad',Status__c='Lost', Opportunity__c=opp.id, Date__c=System.today().addMonths(2));
           Sponsorship__c sprs1 = new Sponsorship__c(Type__c='Heavy-ups/Roadblocks/Limited Interruption',Status__c='Lost', Opportunity__c=opp.id, Date__c=System.today().addMonths(2));
       spList.add(spr);
       spList.add(sprs1);
        insert(spList);
        spr.Type__c='CE Device Takeover'; 
        spr.Status__c='Sold';
        spr.Date__c= System.today().addMonths(2) ;
        update spr;
    System.test.stoptest();
  
         
      //update Sponsorship
        spr.Type__c='Heavy-ups/Roadblocks/Limited Interruption';
        spr.Status__c='Pitched';
        spr.Impressions__c=1;
        spr.Ad_Product__c='Web Display';
        spr.Date__c=d;
        spr.End_Date__c=d.addDays(1);
        spr.Has_Warnings__c = false; //updated by VG 10-09-2012
        update spr;
         //update Sponsorship
         spr.Status__c='Sold';
         update spr;
         //update Sponsorship
         spr.Status__c = 'Pitched';
          update spr;
        SCAL_Utils.getDaysGivenInterval('3');
        
        SCAL_SendEmailToOwner.sendEmailStatusUpdatedFromSold(spList, spr);
       
    }
    ***********************************************************/ 
    
}