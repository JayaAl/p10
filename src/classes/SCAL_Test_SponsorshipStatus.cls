@isTest
private class SCAL_Test_SponsorshipStatus{


/*********************************************************
 Modified By - Vardhan Gupta (02/13/2013) - code Depracated. All functionality moved into Matterhorn. Please contact "cgraham@pandora.com" for mode details   


    static testmethod void testSponsorshipStatus(){
     
        //create new User - Start
      //  Profile p = [select id from Profile where name like 'Standard Platform%' limit 1];  
      //  User u = new User(LastName='Test User',alias = 'test',email='test@testorg.com',emailencodingkey='UTF-8',
      //  languagelocalekey='en_US',localesidkey='en_US',timezonesidkey='America/Los_Angeles', 
      //  username='testuser@testorg.com',profileId = p.Id);
     //   insert u;
        User u = [Select Id from User where Isactive = true and Profile.Name = 'Finance - Accounting User' limit 1];
        //create new User - End
       
      System.runas(u)
      {
        test.starttest();
        //create Account
        Account acc = new  Account(Name ='test Account',Type='Advertiser', BillingStreet='2101 Webster St', BillingCity='Oakland',  BillingState='CA', BillingPostalCode='94619');
        insert(acc);
        
       //create new Opportunity - Start            
        datetime t = System.now();            
        date d = Date.newInstance(t.year(),t.month(),t.day());
                    
        Opportunity opp=new Opportunity(name='test opportunity', AccountId=acc.id, StageName='Closed Won', CloseDate=System.today().adddays(-6),
                        ContractStartDate__c=d, ContractEndDate__c=System.today().addMonths(7), Industry_Category__c='Political',
                        Sales_Planner__c='Victoria Bair', Psychographic_Audience_Targets__c='Browser', Target_Audience_End_Age__c='35',
                        Target_Audience_Gender__c='Male',Target_Audience_Start_Age__c='15',OwnerId=u.id,Confirm_direct_relationship__c=true,Industry_Sub_Category__c='Political Campaign Marketing',Type='Advertiser');            
        insert(opp);            
      //create new Opportunity - End
      
      //create New Sponsorship - start
        Sponsorship__c spr = new Sponsorship__c(Type__c='CE Devices Takeover', Status__c='Pitched', Has_Warnings__c = false, Opportunity__c=opp.id, Date__c=System.today().addMonths(2), End_Date__c=System.today().addMonths(3));          
        insert (spr );      
      //create New Sponsorship - End
      
      //Update Opportunity
      opp.StageName='Closed Lost';
      opp.CloseDate =System.today().addMonths(7);
      update opp;
      
      //Check if Status of Sponsorship is updated to lost
      Sponsorship__c strSponsor =[select Status__c from Sponsorship__c where Opportunity__c =:opp.id ];
      System.assertEquals('Lost', strSponsor.Status__c);
      
      //Upadte Oppornuity
      opp.Probability=75;
      update opp;
      
      Sponsorship__c strSponsorshp =[select Status__c from Sponsorship__c where Opportunity__c =:opp.id];
      System.assertEquals('Sold', strSponsorshp.Status__c);
      test.stoptest();
      
      }
     }
     
      ***********************************************************/
}