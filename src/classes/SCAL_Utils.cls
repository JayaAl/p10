public with sharing class SCAL_Utils {
    
    
    /*********************************************************
 Modified By - Vardhan Gupta (02/13/2013) - code Depracated. All functionality moved into Matterhorn. Please contact "cgraham@pandora.com" for mode details   

    
    public static final String DAY ='Day';
    public static final String MONTH ='Month';
    public static final String WEEK ='Week';
    
    
  
  //  Method to check the data values changed on a record.
      
   public static Boolean hasChanges(String field, SObject oldRecord, SObject newRecord) {
        if (oldRecord == null) {
            return true;
        }
        return (oldRecord.get(field) != newRecord.get(field));
    }
     
     public static Integer getDaysGivenInterval(String interval) {
        Integer days = 0;
        if (DAY.equalsIgnoreCase(interval)) {
            days = 1;
        } else if (WEEK.equalsIgnoreCase(interval)) {
            days = 7;
        } else if (MONTH.equalsIgnoreCase(interval)) {
            days = 30;
        }       
        return days;
     }
     
     public static void populateEndDateOnSponsorship(Sponsorship_Type__c currentSponsorshipTypeInfo, Sponsorship__c currentSponsorship ) {
        //if user has entered end date do not overwrite
        if (currentSponsorship.End_Date__c != null) {
            return;
        }
         Integer duration = Integer.valueOf(currentSponsorshipTypeInfo.Duration__c);
         Date endDate = currentSponsorship.Date__c;
         if (DAY.equalsIgnoreCase(currentSponsorshipTypeInfo.Interval__c)) {
            if (duration > 1) {
                endDate = endDate.addDays((1 * Integer.valueOf(duration)) - 1);
            }
         } else if(WEEK.equalsIgnoreCase(currentSponsorshipTypeInfo.Interval__c)) {
             endDate = endDate.addDays((7 * duration) - 1);
         } else if (MONTH.equalsIgnoreCase(currentSponsorshipTypeInfo.Interval__c)) {
             endDate =endDate.addMonths(duration); 
             endDate = endDate.addDays(-1);
             
           //  Integer dayOfMonth = currentSponsorship.Date__c.day();
        //     if (dayOfMonth == 1) { // first of month
         //        endDate = endDate.addDays(-1);
         //        endDate =endDate.addMonths(duration);      
         //    } else {
          //       endDate =endDate.addMonths(duration);  
          //       endDate = endDate.addDays(-1);     
          //   }
             
         }  
         currentSponsorship.End_Date__c = endDate;
     }
     
     public static Map<String,List<Sponsorship__c>> getAllSponsorshipsByType(Set<String> sponsorshipTypeSet) {
         Date monthStartingDate = Date.newInstance(System.today().year(), System.today().month(), 1) ;
         Map<String,List<Sponsorship__c>> sponsorshipListByTypeMap = new Map<String,List<Sponsorship__c>>();    
         List<Sponsorship__c> sponsorshipList =[Select Type__c, Status__c, Name, CreatedDate, Id, End_Date__c, Date__c From Sponsorship__c Where Type__c IN:sponsorshipTypeSet AND Status__c='Sold' AND Date__c >= :monthStartingDate order by Date__c]; //order by Date__c and 
         if (sponsorshipList != null && sponsorshipList.size() > 0) {
            for(Sponsorship__c sponsorshipInfo : sponsorshipList) {
                if (!sponsorshipListByTypeMap.containsKey(sponsorshipInfo.Type__c)) {
                    sponsorshipListByTypeMap.put(sponsorshipInfo.Type__c, new List<Sponsorship__c>());
                }
                sponsorshipListByTypeMap.get(sponsorshipInfo.Type__c).add(sponsorshipInfo);
            }
         }
         
         return sponsorshipListByTypeMap;
        
     }
     
     
     
     public static Integer getAllSponsorshipsByTypeforWeek(Sponsorship__c currentSponsor, Boolean isUpdate) {
        Integer count = 0;
        if(currentSponsor == null || (!currentSponsor.Status__c.Equals('Sold'))){
            return count;
        }
        date myDate = currentSponsor.Date__c;
        System.debug('myDate---'+myDate);
        date weekStart = myDate.toStartofWeek();
        System.debug('weekStart---'+weekStart);
        //integer dayOfWeek = weekStart.daysBetween(myDate);
        //System.debug('dayOfWeek---'+dayOfWeek);
        weekStart  = weekStart.addDays(1);
        datetime myfDate = datetime.newInstance(myDate.year(), myDate.month(), myDate.day());  
        String dayofWeek = myfDate.format('EEEE');  
        System.debug('dayofWeek ---'+dayofWeek );
        if(dayofWeek .equals('Sunday')){
            weekStart =  weekStart.addDays(-7);
        }
        
        System.debug('weekStart---'+weekStart); 
        Date weekEnd = weekStart.addDays(6);
        System.debug('weekEnd ---'+weekEnd );
        List<Sponsorship__c> sponsorshipList =[Select Type__c, Status__c, Name, CreatedDate, Id, End_Date__c, Date__c From Sponsorship__c Where Type__c = :currentSponsor.Type__c AND Status__c='Sold' AND Date__c <= :weekEnd  and Date__c >= :weekStart  order by Date__c]; //order by Date__c and 
        System.debug('sponsorshipList -- week'+sponsorshipList );
        if (sponsorshipList != null && sponsorshipList.size() > 0) {
           for(Sponsorship__c sponsorshipInfo : sponsorshipList) {
                if(isUpdate && sponsorshipInfo.Id == currentSponsor.Id) {
                    System.debug('---isUpdate '+isUpdate );
                    //if (currentSponsorshipInfo.Status__c =='Pitched') {
                         continue;
                    //}
                }else{
                    count ++;
                }
           }
           System.debug('---count'+count);
         }
         
         return count ;
        
     }
     
     public static Integer getSponsorshipCountFallsInCurrentSponsorshipDate(Sponsorship__c currentSponsorshipInfo , List<Sponsorship__c> sponsorshipList, boolean isUpdate) {
        Integer sponsorshipCnt = 0;
        if (sponsorshipList != null && sponsorshipList.size() > 0) {
            for(Sponsorship__c sponsorshipInfo : sponsorshipList) {
                if (isUpdate) {
                    if(sponsorshipInfo.Id == currentSponsorshipInfo.Id) {
                        //if (currentSponsorshipInfo.Status__c =='Pitched') {
                             continue;
                        //}
                    }
                }
                
                if ((currentSponsorshipInfo.Date__c >= sponsorshipInfo.Date__c &&  currentSponsorshipInfo.Date__c <= sponsorshipInfo.End_Date__c) || (currentSponsorshipInfo.End_Date__c >= sponsorshipInfo.Date__c &&  currentSponsorshipInfo.End_Date__c <= sponsorshipInfo.End_Date__c)) {
                    sponsorshipCnt++;
                }
            }
        }
        
        return sponsorshipCnt;
     }


    public static Integer getSponsorshipCountFallsInMinGapCurrentSponsorshipDate(Sponsorship__c currentSponsorshipInfo ,  boolean isUpdate, Sponsorship_Type__c sponsrType) {
        Integer index = 0;
        System.debug('sponsrType ----'+sponsrType );
        if(sponsrType == null || (!currentSponsorshipInfo.Status__c.Equals('Sold'))){
            return index ;
        }
        date myDate = currentSponsorshipInfo.Date__c;
        date weekStart = currentSponsorshipInfo.Date__c.toStartofWeek();
        weekStart = weekStart.addDays(1); //.addDays(1)
        //System.debug('weekStart---'+weekStart);
        datetime myfDate = datetime.newInstance(myDate.year(), myDate.month(), myDate.day());  
        String dayofWeek = myfDate.format('EEEE');  
        System.debug('dayofWeek ---'+dayofWeek );
        if(dayofWeek .equals('Sunday')){
            weekStart =  weekStart.addDays(-7);
        }
        
        System.debug('weekStart---'+weekStart);
        integer weekDiff = weekStart.daysBetween(currentSponsorshipInfo.Date__c);
        System.debug('weekDiff ---'+weekDiff );
        Integer finalDate;
        if(weekDiff < sponsrType.Minimum_Gap__c){
            finalDate = weekDiff ;      
        }else{
            finalDate = Integer.valueof(sponsrType.Minimum_Gap__c);
        }
        System.debug('finalDate ---'+finalDate );
        Date weekDate = currentSponsorshipInfo.Date__c.addDays(-finalDate);
        System.debug('weekDate ---'+weekDate );
        currentSponsorshipInfo.Date__c = currentSponsorshipInfo.Date__c.addDays(finalDate);
        List<Sponsorship__c> sponsorshipList =[Select Type__c, Status__c, Name, CreatedDate, Id, End_Date__c, Date__c From Sponsorship__c Where Type__c =:currentSponsorshipInfo.Type__c AND Status__c='Sold' AND Date__c  > =: weekDate and  Date__c <= :currentSponsorshipInfo.Date__c  order by Date__c]; //order by Date__c and 
        System.debug('sponsorshipList ----'+sponsorshipList );
        if(sponsorshipList != null && sponsorshipList.size()>=0){
            System.debug('sponsorshipList ----'+sponsorshipList );
            for(Integer i = 0 ; i <  sponsorshipList.size(); i++){
            System.debug('---currentSponsorshipInfo.Id'+currentSponsorshipInfo.Id);
            System.debug('---sponsorshipList[i].Id'+sponsorshipList[i].Id);
                if(isUpdate && sponsorshipList[i].Id == currentSponsorshipInfo.Id) {
                    System.debug('---isUpdate '+isUpdate );
                    sponsorshipList.remove(i);
                    System.debug('---isUpdate '+isUpdate );
                            
                }
            }
        }
        if(sponsorshipList != null && sponsorshipList.size()>0){
            index ++;    
        }else{
            return index ;
        }
        
        return index ;
        //Date          
    }
    
    ***********************************************************/ 
}