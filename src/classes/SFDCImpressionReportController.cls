public with sharing class SFDCImpressionReportController {

/* Filters on the page?

    Opportunity.StageName = 'Closed Won'
    Opportunity.RecordType.Name = 'Opportunity - Performance'
    ScheduleDate = THIS_FISCAL_QUARTER

    Opportunity.Name
    Opportunity.Account.Name
    Opportunity.Owner.Name

*/

    public static List<OpportunityLineItemSchedule> listOLISched{get;set;}
    public static List<OpportunityLineItem> listOLI{get;set;}
    public static Map<Id,Map<Date,Impressions_Data__c>> mapLineItemToImpression{get;set;}
    public static Map<Id,List<Opportunity_Split__c>> mapOpportunityToSplitList{get;set;}
    // public static Set<Id> setOppIds{get;set;}
    public static String filterRecordType{get;set;}
    public static String filterStageName{get;set;}
    public static String currentPeriod{get;set;}
    public static List<SelectOption> periodOptions{get;set;}
    public static Map<String,Map<String,Date>> mapPeriodDates{get;set;}
    
    public static void populateOptions(){
        if(filterRecordType==null || filterRecordType==''){
            if( ApexPages.currentPage() != null
                && ApexPages.currentPage().getParameters().get('filterRecordType')!=null
            ){
                filterRecordType = ApexPages.currentPage().getParameters().get('filterRecordType');
            } else {
                filterRecordType = 'Opportunity - Performance';
            }
        }
        if(filterStageName==null || filterStageName==''){
            if( ApexPages.currentPage() != null
                && ApexPages.currentPage().getParameters().get('filterStageName')!=null
            ){
                filterStageName = ApexPages.currentPage().getParameters().get('filterStageName');
            } else {
                filterStageName = 'Closed Won';
            }
        }

        if(currentPeriod==null || currentPeriod==''){
            if( ApexPages.currentPage() != null
                && ApexPages.currentPage().getParameters().get('currentPeriod')!=null
            ){
                currentPeriod = ApexPages.currentPage().getParameters().get('currentPeriod');
            } else {
                Date today = Date.today();
                for (String s : mapPeriodDates.keySet()){
                    Map<String,Date> thePeriod = mapPeriodDates.get(s);
                    if(thePeriod.get('start') <= today && thePeriod.get('end') >= today){
                        currentPeriod = s;
                        break;
                    }
                }
            }
        }
    }

    public static void populateQuarters(){
        periodOptions = new List<SelectOption>();
        mapPeriodDates = new Map<String,Map<String,Date>>(); 
        Date today = Date.Today();
        
        periodOptions.add(new SelectOption('','-- Select Period --', true));
        for(Period p:[
            SELECT Id, StartDate, EndDate, Number
            FROM Period 
            WHERE type = 'quarter'
            AND StartDate >= LAST_N_QUARTERS:4
            AND EndDate <= NEXT_N_QUARTERS:4
            ORDER BY StartDate
        ]){
            String s = 'Q'+p.Number+'-'+p.EndDate.Year();
            periodOptions.add(new SelectOption(s,s));
            mapPeriodDates.put(s,new Map<String,Date>());
            mapPeriodDates.get(s).put('start',p.StartDate);
            mapPeriodDates.get(s).put('end',p.EndDate);
        }
    }

    public SFDCImpressionReportController(){
        /* Commenting out, will perform queries via JS Remote Action */
        // populateQuarters();
        // populateOptions();
        // performQueries();
    }
    
    private static void performQueries(){
        // wrapperList = new List<impressionsWrapper>();
        listOLI = new List<OpportunityLineItem>();
        Set<Id> setOppIds = new Set<Id>();
        mapLineItemToImpression = new Map<Id,Map<Date,Impressions_Data__c>>();
        mapOpportunityToSplitList = new Map<Id,List<Opportunity_Split__c>>();

        for(OpportunityLineItem oli:queryOppLI()){
            Id OLIId = oli.Id;
            Date OLIDate = oli.ServiceDate;

            // List of all Line Item Schedules
            listOLI.add(oli); 
            setOppIds.add(oli.OpportunityId);
            
            // Prepare Impressions_Data__c Map
            if(!mapLineItemToImpression.containsKey(OLIId)){
                mapLineItemToImpression.put(OLIId,new Map<Date,Impressions_Data__c>());
            }
            if(!mapLineItemToImpression.get(OLIId).containsKey(OLIDate)){
                // for each line put a placeholder Impressions_Date__c record with specific ImpressionsKey__c we can reference later
                mapLineItemToImpression.get(OLIId).put(OLIDate,new Impressions_Data__c(ImpressionsKey__c='missing'));
            }

            // Prepare Opportunity_Split__c Map
            if(!mapOpportunityToSplitList.containsKey(OLIId)){
                mapOpportunityToSplitList.put(OLIId,new List<Opportunity_Split__c>());
            }
        }

        // populate Impressions_Data__c Map
        for(Impressions_Data__c imp:queryImpressions(setOppIds)){
            if(!mapLineItemToImpression.containsKey(imp.OpportunityProductId__c)){
                mapLineItemToImpression.put(imp.OpportunityProductId__c,new Map<Date,Impressions_Data__c>());
            }
            mapLineItemToImpression.get(imp.OpportunityProductId__c).put(imp.Date__c,imp);
        }

        // populate Opportunity_Split__c Map
        for(Opportunity_Split__c split:querySplitDetails(setOppIds)){
            if(!mapOpportunityToSplitList.containsKey(split.Opportunity__c)){
                mapOpportunityToSplitList.put(split.Opportunity__c,new List<Opportunity_Split__c>());
            }
            mapOpportunityToSplitList.get(split.Opportunity__c).add(split);
        }
    }
    
    @ReadOnly
    @RemoteAction
    public static List<OpportunityLineItem> remoteQueryOppLi(){
        // Get period information		
        populateQuarters();
        populateOptions();
        return queryOppLI();
    }
    public static List<OpportunityLineItem> queryOppLI(){

        Date filterStartDate;
        Date filterEndDate;
        // Confirm that mapPeriodDates contains the 
        if(mapPeriodDates.containsKey(currentPeriod)){
            if(mapPeriodDates.get(currentPeriod).containsKey('start') && mapPeriodDates.get(currentPeriod).containsKey('end')){
                filterStartDate = mapPeriodDates.get(currentPeriod).get('start');
                filterEndDate = mapPeriodDates.get(currentPeriod).get('end');
            } else {
                // TODO: Add on-screen message indicating error
                return null;
            }
        } else {
            // TODO: Add on-screen message indicating error
            return null;
        }
        
        return [
            SELECT
                Id,
            	Banner__c,
                Banner_Type__c,
                Click_Rate__c,
                Clicks__c,
                Conv_Rate_clicks__c,
                Conversions__c,
                Cost_Type__c,
                eCPM__c,
                End_Date__c,
                Guaranteed__c,
                Impression_s__c,
                Medium__c,
                Opportunity.AccountId,
                Opportunity.Account.Name,
                Opportunity.Amount,
                Opportunity.CloseDate,
                Opportunity.ExpectedRevenue, 
                Opportunity.Hybrid__c,
                Opportunity.Id,
                Opportunity.Industry_Category__c,
                Opportunity.Industry_Sub_Category__c,
                Opportunity.Lead_Campaign_Manager__c,
                Opportunity.Lead_Campaign_Manager__r.Name,
                Opportunity.Name,
                Opportunity.OwnerId,
                Opportunity.Owner.Name,
                Opportunity.Owner.UserRole.Name,
                Opportunity.Performance_Action__c,
                Opportunity.Probability,
                Opportunity.Slingshot_Order_ID__c,
                Opportunity.StageName,
            	CurrencyISOCode,
	            Performance_Type__c,
                Platform__c,
                Product_Name__c, 
                Quantity, 
                Rate__c,
                ServiceDate,
                Sub_Platform__c,
                UnitPrice,
            	(
                    SELECT
                        Id,
                        Revenue,
                        ScheduleDate
                     FROM
                        OpportunityLineItemSchedules
                     WHERE
                    	ScheduleDate >= :filterStartDate
                		AND ScheduleDate <= :filterEndDate
                    Order By ScheduleDate
                )
            FROM
                OpportunityLineItem
            WHERE
                Opportunity.RecordType.Name = :filterRecordType
                AND ServiceDate >= :filterStartDate
                AND End_Date__c <= :filterEndDate
                AND Opportunity.StageName = :filterStageName
            ORDER BY
                Opportunity.Name
            // LIMIT 1   // 6 records in testing
            // LIMIT 10   // 124 records in testing
            // LIMIT 100  // 2,156 records in testing.
            LIMIT 500  // 22,212 records in testing
            // LIMIT 1000 // 53,894 records in testing
            // NO LIMIT   // 55,970 records in testing (~ 4 minutes to export to Excel, ~)
        	
		];
    }

    @ReadOnly
    @RemoteAction
    public static List<Opportunity_Split__c> remoteQuerySplits(List<Id> listOppIds){
    	Set<Id> setOppIds = new Set<Id>();
        setOppIds.addAll(listOppIds);
        return querySplitDetails(setOppIds);
    }
    public static List<Opportunity_Split__c> querySplitDetails(Set<Id> setOppIds){
        return [
            SELECT 
                Opportunity__c, 
                Salesperson__c, Salesperson__r.Name,
                Split__c, 
                Split_Review__c,
                Territory__c
            FROM
                Opportunity_Split__c
                
            WHERE
                Opportunity__c IN :setOppIds
        ];
    }

    @ReadOnly
    @RemoteAction
    public static List<Impressions_Data__c> remoteQueryImps(List<Id> listOppIds){
    	Set<Id> setOppIds = new Set<Id>();
        setOppIds.addAll(listOppIds);
        return queryImpressions(setOppIds);
    }
    public static List<Impressions_Data__c> queryImpressions(Set<Id> setProdIds){
        return [
            SELECT 
                // Date__c,
                DFP_Clicks__c,
                DFP_CTR__c,
                DFP_CVR__c,
                DFP_eCPM__c,
                DFP_Impressions__c,
                DFP_Revenue__c,
                ImpressionsKey__c,
                // Name, 
                // Opportunity__r.Name,
                // OpportunityProductId__c,
                X3rd_Party_Clicks__c,
                X3rd_Party_Conversions__c,
                X3rd_Party_Impressions__c,
                X3rd_Party_Revenue__c
            FROM 
                Impressions_Data__c
            WHERE
                OpportunityProductId__c IN :setProdIds
        ];
    }
    

    
}