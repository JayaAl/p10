public class SI_CCRECEIPT_PDF_Controller {
    public String ccNumber{get;set;}
    public String ccHolderName{get;set;}
    public Integer selectedMonth{get;set;}
    public String selectedYear{get;set;}
    public String invoiceNumber{get;set;}
    public String ccAmount{get;set;}
    public String textReference{get;set;}
    public String accountName {get;set;}
    public String localDate {get;set;}
    public String localTime {get;set;}    
    
    
    public SI_CCRECEIPT_PDF_Controller(){
        User objUser = [Select AccountId from User where Id =: UserInfo.getUserId()];
        if(!Test.isRunningTest()){
        Account objAccount = [Select Name from Account where Id= : objUser.AccountId];
            accountName = objAccount.Name;
        } else{
            accountName = 'Test Pandora';
        }
        invoiceNumber = ApexPages.currentPage().getParameters().get('invoiceNumber');
        ccAmount = ApexPages.currentPage().getParameters().get('ccAmount');
        textReference = ApexPages.currentPage().getParameters().get('textReference');
        system.debug(invoiceNumber + '>' + ccAmount + '>' + textReference);
        
        //Set local date and local time
        DateTime cDT = system.Now();
        localDate = cDT.format('MM/dd/yyyy');
        localTime = cDT.format('hh:mm:ss a');
        
        String umMaskedCcNumber = ApexPages.currentPage().getParameters().get('ccNumber');
        ccNumber = '';
        Integer i=0;
        do{
            if(i < umMaskedCcNumber.length()){
                //umMaskedCcNumber.substirng(i,i+1) is what you use to get each character
                if(umMaskedCcNumber.length() - i <=4){
                    ccNumber += umMaskedCcNumber.substring(i,i+1);
                }     
                else{
                    if(umMaskedCcNumber.substring(i,i+1).isNumeric()){
                        ccNumber += 'X';
                    }else{
                        ccNumber += umMaskedCcNumber.substring(i,i+1);
                    }
                }
            }
            i=i+1;
        }while(i < umMaskedCcNumber.length());
        
        
        ccHolderName = ApexPages.currentPage().getParameters().get('ccHolderName');
        selectedMonth = integer.valueOf(ApexPages.currentPage().getParameters().get('selectedMonth'));
        selectedYear = ApexPages.currentPage().getParameters().get('selectedYear');
    }
    
     @isTest //(seealldata=true)
    private static void testSalesInvoicePDFController() {
        
        ERP_Invoice__c testInvoice = new ERP_Invoice__c(Name = 'testERP');
        insert testInvoice;
        
            String invoiceName = [Select Name from ERP_Invoice__c Limit 1].Name;
            ApexPages.currentPage().getParameters().put('invoiceNumber',invoiceName);
            ApexPages.currentPage().getParameters().put('ccAmount','50000');
            ApexPages.currentPage().getParameters().put('textReference','1234567486342485743');
            ApexPages.currentPage().getParameters().put('ccNumber','1234567486342485743');
            ApexPages.currentPage().getParameters().put('selectedMonth','12');
            ApexPages.currentPage().getParameters().put('selectedYear','2012');
        system.runAs(new User(id=UserInfo.getUserId())) {

            system.debug('**********invName'+ApexPages.currentPage().getParameters().get('invoiceNumber'));
            SI_CCRECEIPT_PDF_Controller objSalesInvoicePDFTest = new SI_CCRECEIPT_PDF_Controller();
        }
    }
}