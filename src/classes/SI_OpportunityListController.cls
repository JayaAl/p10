public without sharing class SI_OpportunityListController {
    
    public CUST_PortalUtils FLS{get{return new CUST_PortalUtils();}set;}
    // List view
    public List<OpportunityView> opportunityListView {get;set;}
    public Boolean no_records_available {get;set;}
    
    // The list of accounts to filter 
    public String user_account_id;
    public Boolean multiple_accounts {get;set;}
    public Boolean hasPaymentAccess {get;set;}
    // Set controller (handle more records)
    private String soql {get;set;}
    private String base_soql {get;set;}
    public String oppIds{get;set;}
    
    public String defaultAmount {get;set;}
    
    // Pagination Methods for the set controller
    
    private Boolean firstOff;    // previous part
    private Boolean lastOff;     // next part
    private Integer queryLimit;
    private Integer offset;
    private String limits;
    private Integer pageNumber;
    private Integer listSize;    // for total record size
    
    public void GoPrevious(){ 
        if(offset-queryLimit <= 0){
            offset = 0;
            firstOff = false;
        }
        else offset -= queryLimit;
        lastOff = true;
        pageNumber--;
        queryOpportunities();
    }
    public void GoNext(){ 
        offset += queryLimit;
        if(offset+queryLimit >= listSize) lastOff = false;
        firstOff = true;
        pageNumber++;
        queryOpportunities();
    }
    public void GoLast(){ 
        // set page number of and offset
        if(Math.Mod(listSize,queryLimit) == 0){
            offset = listSize-queryLimit;
            pageNumber = listSize/queryLimit;
        }
        else{
            offset = (listSize/queryLimit)*queryLimit;
            pageNumber = (listSize/queryLimit)+1;
        }
        lastOff = false;
        firstOff = true;
        queryOpportunities();
    }
    public  void GoFirst(){ 
        offset = 0;
        firstOff = false;
        lastOff = true;
        pageNumber = 1;
        queryOpportunities();
    }
    public Boolean getRenderPrevious(){return firstOff;}
    public Boolean getRenderNext(){return lastOff;}
    public Integer getRecordSize(){return listSize;}
    public Integer getPageNumber(){return pageNumber;}
    public boolean has_pages {get;set;}
    
    // Static vars, needed inside of the invoice inner class
    public static String current_filter_static {get;set;}
    public static String current_page_static {get;set;}
    public static String current_sort_order_static {get;set;}
    public static String current_sort_field_static {get;set;}
    public String paymentPageURL {get;set;}
    
    
    
    private String filter_hiddenUntilNumber;
    private Date filter_hiddenUntilDate;
    private String filter_hiddenByNamePattern;


    private String filters_saveSortOrder = '';
    private Boolean filters_setOneTime_sort = false;
    
    public String debugSt{get;set;}
    
    
    public Boolean page_validation{get;set;}
    
    
    public Integer getTotalPages(){
        if(Math.Mod(listSize,queryLimit) == 0) return listSize/queryLimit;
        else return (listSize/queryLimit)+1;
    }
    
    
    // Constructor
    public SI_OpportunityListController()
    {

        // +++++++++++++++++++++++++    
        // CHECK USER PRIVILEGES TO ACCESS:
        this.page_validation = true;
        //if(!PortalActivateMyAccountController.CHECK_FFPORTAL_PRIVILEGES(UserInfo.getUserId())){
        //    this.page_validation = false;
        //    return; 
        //}
        // +++++++++++++++++++++++++    

        defaultAmount = '0.00';    
        this.debugSt = '';

        
        
        
        no_records_available = false;
        Id currentUserId = UserInfo.getUserId();
        User u = [Select ContactId, Contact.Name, Contact.Id, AccountId  from User where Id =: currentUserId Limit 1];
        if(u.AccountId != null) {
            this.hasPaymentAccess = CUST_PortalUtils.hasPaymentAccess(u.AccountId);
        } else {
            this.hasPaymentAccess = false;
        }
        user_account_id = u.AccountId;
        user_account_id = user_account_id.substring(0, 15);
        String contact_id = u.contact.id;
        sortedByUrl = false;
        
        
        paymentPageURL = '';
        
        
        /*Set Pagination Variables*/
        firstOff = false;
        queryLimit = 25;
        offset = 0;
        limits = '25';
        pageNumber = 1;
        
        aggregateResult res = database.query('select COUNT(id) cnt from IO_Detail__c where Primary_Billing_Contact_Account_Id__c =: user_account_id AND PortalPaymentStatus__c IN (\'Partial Paid\', \'Unpaid\',\'Paid\') AND Opportunity__r.Probability = 100');
        
        // fill size of all records
        listSize = Integer.valueOf(res.get('cnt'));
        
        // initialy check page more then 1 or not
        if(listSize > queryLimit) lastOff = true;
        else lastOff = false;
            
        // Base SOQL sentence
        soql = 'SELECT ID, Total_IO_Amount__c, Opportunity__r.Probability, Opportunity__r.Name, Name, Contract_Start_Date__c, Contract_End_Date__c, PortalPaymentStatus__c, Total_Paid_Amount_From_Portal__c FROM IO_Detail__c ' + 
                'where Primary_Billing_Contact_Account_Id__c =: user_account_id ' +
                'AND PortalPaymentStatus__c IN (\'Partial Paid\', \'Unpaid\',\'Paid\') AND Payment_Terms_Preferred__c = \'Pre-Pay\'AND Opportunity__r.Probability = 100 ' ;
        base_soql = soql;
        ApexPages.currentPage().getParameters().put('sort_field', 'OriginalPaymentTerm');
        if(ApexPages.currentPage().getParameters().get('sort_field') != null){
            String sort_field = ApexPages.currentPage().getParameters().get('sort_field');
            if(sort_field == 'Opportunities'){ sortOpportunites(); }
            else if(sort_field == 'ContractStartDate'){ sortContractStartDate(); }
            else if(sort_field == 'ContractEndDate'){ sortContractEndDate(); }
            else if(sort_field == 'TotalIOAmount'){ sortTotalIOAmount(); }
            else if(sort_field == 'OriginalPaymentTerm'){ sortOriginalPaymentTerm(); }
        } else {
            queryOpportunities();
        }
                    
        
        if(ApexPages.currentPage().getParameters().get('p') != null){
            pageNumber = integer.valueOf(ApexPages.currentPage().getParameters().get('p'));
        }

    }
    
    
    /*
    * Do a query to the list of opportunites.
    */
    private List<sObject> sObjectList;
    public void queryOpportunities(){
        sObjectList = database.query(soql+' LIMIT '+queryLimit+' OFFSET '+offset);
        
        
        if(getTotalPages() > 1){
            has_pages = true;
        } else {
            has_pages = false;
        }
        
        if(listSize > 0){
            no_records_available = false;
        } else {
            no_records_available = true;
        }
        
    }
    
    // Sorting
    public String previousSortField {get;set;}  
    public String sortOrder {get;set;}
    public Boolean sortedByUrl {get;set;}

    private Boolean setReOrder = false;
        
    public void doSort(String soql_query, String sortField){
        
        // The first time we sort, if not set previousSortField and the order comes through parameters, avoid the sorting change.
        if(!sortedByUrl && ApexPages.currentPage().getParameters().get('sort_order') != null){
            sortOrder = ApexPages.currentPage().getParameters().get('sort_order');
            previousSortField = sortField;
            sortedByUrl = true;
        } else {
            sortOrder = 'asc';
            
            if(setReOrder == true){
                previousSortField = '-';
                setReOrder = false;
            }
            
            if(previousSortField == sortField){
                sortOrder = 'desc';
                setReOrder = true;
            }else{
                previousSortField = sortField;
            }
        }
                        
        soql = soql_query+sortOrder;
        
        this.filters_saveSortOrder += sortOrder;
        
        queryOpportunities();
    }
    
    public void sortOpportunites() {
        this.removeCurentSortOrder();
        String soql_query = base_soql+'order by Opportunity__r.Name ';
        this.filters_saveSortOrder = 'order by Opportunity__r.Name ';
        doSort(soql_query,'Opportunities');
    }
    
    public void sortContractStartDate() {
        this.removeCurentSortOrder();
        String soql_query = base_soql+'order by Contract_Start_Date__c ';
        this.filters_saveSortOrder = 'order by Contract_Start_Date__c ';
        doSort(soql_query,'ContractStartDate');
    }
    
    public void sortContractEndDate() {
        this.removeCurentSortOrder();
        String soql_query = base_soql+'order by Contract_End_Date__c ';
        this.filters_saveSortOrder = 'order by Contract_End_Date__c ';
        doSort(soql_query,'ContractEndDate');
    }
    
    public void sortTotalIOAmount() {
        this.removeCurentSortOrder();
        String soql_query = base_soql+'order by Total_IO_Amount__c ';
        this.filters_saveSortOrder = 'order by Total_IO_Amount__c ';
        doSort(soql_query,'TotalIOAmount');
    }
    
    public void sortOriginalPaymentTerm() {
        this.removeCurentSortOrder();
        String soql_query = base_soql + 'order by PortalPaymentStatus__c ';
        this.filters_saveSortOrder = 'order by PortalPaymentStatus__c ';
        doSort(soql_query,'OriginalPaymentTerm');
    }
    
    private void removeCurentSortOrder(){
        if(base_soql != null && !base_soql.equals('')){
            String aux = base_soql.toLowerCase();
            Integer pos = aux.indexOf('order by');
            if(pos > -1){
                base_soql = base_soql.substring(0,pos);
            }
        }
    }
    
    // Getter for the table 
    public List<OpportunityView> getOpportunityList(){
        
        opportunityListView = new List<OpportunityView>();
        
        current_page_static = String.valueOf(pageNumber);
        
        // Set statics for link creation.
        current_sort_order_static = sortOrder;
        current_sort_field_static = previousSortField; 
        
        for(Sobject cs : getQuery())
        {
            opportunityListView.add(new OpportunityView(cs, this));
        }
        
        return opportunityListView;
    } 
    
    
    public List<Sobject> getQuery(){
        
        return this.sObjectList;
        
    }
    
    // viewstate
    public class OpportunityView {
        public SObject objCase {get;set;}
        
        public String opportunityName {get;set;}
        public String caseNumber {get;set;}
        public Date contractStartDate {get;set;}
        public Date contractEndDate {get;set;}
        public Double opportunityAmount {get;set;}
        public Double opportunityAmountTotal {get;set;}
        public String paymentAmount {get;set;}
        public String opportunityStatus {get;set;}
        
        public SI_OpportunityListController parentController;
        
                
        public OpportunityView(SObject myCase, SI_OpportunityListController parent) {
            this.objCase = myCase;
            SObject oppName = objCase.getSobject('Opportunity__r');
            if(oppName != null){
                opportunityName = (String)oppName.get('Name');
            }
            Object cn = objCase.get('Name');
            if(cn != null){
                caseNumber = (String)cn;
            }
            Object conStDate = objCase.get('Contract_Start_Date__c');
            if(conStDate != null){
                contractStartDate = (Date)conStDate;
            }
            Object conEnDate = objCase.get('Contract_End_Date__c');
            if(conEnDate != null){
                contractEndDate = (Date)conEnDate;
            }
            Object totalAmt = objCase.get('Total_IO_Amount__c');
            if(totalAmt != null){
                Double paidAmountTillNow = (Double)objCase.get('Total_Paid_Amount_From_Portal__c');
                opportunityAmountTotal = (Double)totalAmt;
                opportunityAmount = (Double)totalAmt - (paidAmountTillNow <> null ? paidAmountTillNow : 0);
                paymentAmount = string.valueOf(((Decimal)opportunityAmount).setScale(2));
            }
            Object oaptr = objCase.get('PortalPaymentStatus__c');
            if(oaptr != null){
                opportunityStatus = (String)oaptr;
            }
            
            parentController = parent;
        }
    }
    

    
    /*Method used to pass selected invoices to payment page*/
    public PageReference processSelected() {
        system.debug( '********oppIds' + oppIds);
        String baseURL = paymentPageURL + '/apex/SI_UI_CUST_PORTAL_OpportunityPaymentPage?selectedOpportunities=';
        baseURL += EncodingUtil.base64Encode(Blob.valueOf(oppIds));
        system.debug('baseURL----------' + baseURL);
        PageReference pg = new PageReference (baseURL) ;
        return pg;
    
    }
    
   
    
    
    
}