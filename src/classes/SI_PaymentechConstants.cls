/**
 * @name : SI_PaymentechConstants
 * @desc : maintains all the project specific constants
 * @version: 1.0
 * @author: Lakshman(sfdcace@gmail.com)
 */ 
public class SI_PaymentechConstants{
	public static String SI_GENERIC_ERROR_MESSAGE = 'There was an issue with processing your credit card payment, please try again if there are further issues, please contact customer support.';
	public static String SI_FAILED = 'Failed';
	public static String SI_SUCCESS = 'Success';
	public static String SI_MESSAGE_TYPE_OUTBOUND = 'Outbound'; 
	public static String SI_CASHENTRY_OWNERID = '00540000001awh4'; 
}