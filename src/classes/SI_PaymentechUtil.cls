/**
 * @name : SI_PaymentechUtil
 * @desc : Utility class used for Paymentech purpose
 * @version: 1.0
 * @author: Lakshman(sfdcace@gmail.com)
 */
public class SI_PaymentechUtil {
  
  /**
   * This method is used to make log entry for success/faliure for any Paymentech operation
   * @param: status  This is the status(Success/Failed)
   * @param: callingClassName  This is the name of interface been called
   * @param: requestParameters     This is the request parameter send to interface
   * @param: responseParameters  This is the result after the processing of request
   * @param: messageType  This is type of message sent, Inbound or Outbound
   * @param: whatId  Id/Invoice Number of the SI object where we are doing operation
   * @return: Paymentech_Log__c  Returns object of Paymentech_Log__c with information provided in the parameters
   */
    
    public static Paymentech_Log__c createLogEntry(
        String status, String callingClassName, String callingMethodName, String requestParameters, String responseParameters, String messageType, String whatId)
    {
      Paymentech_Log__c objPaymentechLog = new Paymentech_Log__c();
	  objPaymentechLog.Status__c = status;
	  objPaymentechLog.Webservice__c =  callingClassName;
	  objPaymentechLog.API_Method__c = callingMethodName;
      objPaymentechLog.Request_Parameters__c = requestParameters;
      objPaymentechLog.Response_Parameters__c = responseParameters;
      objPaymentechLog.Message_Type__c = messageType;
      objPaymentechLog.WhatId__c = whatId;
      return objPaymentechLog;
    }
    
/**
 * Updates to combine common functions from both payment pages into one utility method.
 * Referenced by SI_UI_CUST_PORTAL_PaymentExt and SI_UI_CUST_PORTAL_OpportunityPaymentExt
 */
    // SelectOption values for VF-page
    public static List<SelectOption> yearOptions(){
        List<SelectOption> theList = new List<SelectOption>();
        for(Integer i= system.now().year(); i<=system.now().year()+10; i++){
            theList.add(new SelectOption(string.valueOf(i), string.valueOf(i)));
        }
        return theList;
    }
    public static List<SelectOption> monthOptions(){
		List<SelectOption> theList = new List<SelectOption>();
        theList.add(new SelectOption('1', '01 - January'));
        theList.add(new SelectOption('2', '02 - February'));
        theList.add(new SelectOption('3', '03 - March'));
        theList.add(new SelectOption('4', '04 - April'));
        theList.add(new SelectOption('5', '05 - May'));
        theList.add(new SelectOption('6', '06 - June'));
        theList.add(new SelectOption('7', '07 - July'));
        theList.add(new SelectOption('8', '08 - August'));
        theList.add(new SelectOption('9', '09 - September'));
        theList.add(new SelectOption('10', '10 - October'));
        theList.add(new SelectOption('11', '11 - November'));
        theList.add(new SelectOption('12', '12 - December'));
        return theList;
    }
    public static List<SelectOption> cardOptions(){
        List<SelectOption> theList = new List<SelectOption>();
        theList.add(new SelectOption('MasterCard', 'MasterCard'));
        theList.add(new SelectOption('Visa', 'Visa'));
        theList.add(new SelectOption('AmEx', 'American Express'));
        theList.add(new SelectOption('Discover', 'Discover'));
        return theList;
    }
    
/* Method to decode Base64 encoded parameters */
    public static Map<String, String> decodeParameter(String encodedParameter){
        Map<String, String> theMap = new Map<String, String>();
        if(encodedParameter != null) {
            String decoded = EncodingUtil.base64Decode(EncodingUtil.urlDecode(encodedParameter, 'UTF-8')).toString();
            for(String each : decoded.split(',')) {
                String[] single = each.split('-');
                theMap.put(single[0], single[1]);
            }
        }
        return theMap;
    }
    
/**
* Method used to check if credit card details have changed or not
* @param currentCreditDetails : checks previous with current credit card details
* @return Boolean : Returns true if any of the credit card details are changed else returns false
*/
    public static Boolean hasCCDChanged(CreditCardDetails existingCreditDetails, CreditCardDetails currentCreditDetails){
        if(currentCreditDetails.getCustomerName() != existingCreditDetails.getCustomerName() ||
           currentCreditDetails.getZipCode() != existingCreditDetails.getZipCode() || 
           currentCreditDetails.getExpirationMonth() != existingCreditDetails.getExpirationMonth() ||
           currentCreditDetails.getExpirationYear() != existingCreditDetails.getExpirationYear()){
               return true;                
           }
        return false;
    }
    
/**
* Method used to check if credit card number has changed or not
* @param currentCardNumber : checks previous with current current card number
* @return Boolean : Returns true if credit card number is changed else returns false
*/
    public static Boolean hasCCNChanged(CreditCardDetails existingCreditDetails, String currentCardNumber){
        if(currentCardNumber != existingCreditDetails.getCreditCardNumber()){
            return true;
        }
        return false;
    }
}