/**
* @name : SI_UI_CUST_PORTAL_OpportunityPaymentExt
* @desc : Controller for payment processing
* @version: 1.1
* @author: Lakshman(sfdcace@gmail.com)
*/
public without sharing class SI_UI_CUST_PORTAL_OpportunityPaymentExt{
    /* Variables used on page level */
    public String ccNumber {get;set;}
    public String cvvNumber {get;set;}
    public String ccHolderName {get;set;}
    public String ccHolerNumber {get;set;}
    public String ccZipCode {get;set;}
    public Boolean ccAgree {get;set;}
    public List<SelectOption> monthOptions {get;set;}
    public List<SelectOption> yearOptions {get;set;}
    public List<SelectOption> cardOptions {get;set;}
    public String selectedMonth {get;set;}
    public String selectedYear {get;set;}
    public String selectedCardType {get;set;}
    public Decimal ccAmount {get;set;}
    //public c2g__codaInvoice__c objSI {get;set;}
    public List<IO_Detail__c> listIO;
    
    /*Variables used for paymentTech*/
    public String accountId;
    public String extVaultId;
    public User objUser;
    public CommerceProfileResponse profileResponse;
    public CommerceService serv;
    public CreditCardDetails existingCreditDetails;
    public String transactionErrorMessage {get;set;}
    Map<String, String> mapOfOpportunityToAmount;
    public Boolean isSuccess {get;set;}
    public Receipt__c objReceipt {get;set;}
    
/**
* Default Constructor
*/
    public SI_UI_CUST_PORTAL_OpportunityPaymentExt(){
        
        // gather options from util method
        monthOptions = SI_PaymentechUtil.monthOptions();
        yearOptions = SI_PaymentechUtil.yearOptions();
        cardOptions = SI_PaymentechUtil.cardOptions();
		mapOfOpportunityToAmount = SI_PaymentechUtil.decodeParameter(ApexPages.currentPage().getParameters().get('selectedOpportunities'));
        
        // Finish setup
        selectedMonth = string.valueOf(date.today().month()); 
        selectedYear = string.valueOf(system.now().year());
        objReceipt = new Receipt__c();
        isSuccess = true;
        
        // Gather all IOs for the selected Opportunities
        listIO = [
            SELECT i.Contract_End_Date__c,
                i.Contract_Start_Date__c,  i.Id,
                i.Opportunity__r.Amount,
                i.Total_IO_Amount__c,
                i.Opportunity__r.Name,
                i.Opportunity__c,
                //i.CC_Authorized__c, Lakshman: Removed as we now using PortalPaymentStatus__c 
                i.PortalPaymentStatus__c,
                i.Total_Paid_Amount_From_Portal__c,
                i.Name
            FROM IO_Detail__c i
            WHERE Name =: mapOfOpportunityToAmount.keySet()
        ] ;
        
        ccAmount = 0; // total amount to be charged, based on the selectedOpportunities url parameter
        for(IO_Detail__c ci : listIO) {
            if(decimal.valueOf(mapOfOpportunityToAmount.get(ci.Name)) != 0) {// && decimal.valueOf(mapOfOpportunityToAmount.get(ci.Name)) <= ci.Opportunity__r.Amount.setScale(2)) {//Removed Opp Amount check by Lakshman on 29-04-2014
                ccAmount += decimal.valueOf(mapOfOpportunityToAmount.get(ci.Name));
                ci.Total_Paid_Amount_From_Portal__c = (ci.Total_Paid_Amount_From_Portal__c <> null ? ci.Total_Paid_Amount_From_Portal__c : 0) + decimal.valueOf(mapOfOpportunityToAmount.get(ci.Name));
                if(ci.Total_Paid_Amount_From_Portal__c == ci.Total_IO_Amount__c) {
                    ci.PortalPaymentStatus__c = 'Paid';
                } else if(ci.Total_Paid_Amount_From_Portal__c < ci.Total_IO_Amount__c) {
                    ci.PortalPaymentStatus__c = 'Partial Paid';
                }
            }
        }
        
        objUser = [
            Select IsPortalEnabled, AccountId, External_Vault_Id__c, Email 
            FROM User 
            WHERE Id =: UserInfo.getUserId()
        ];
        
        serv = CommerceServiceFactory.getCommerceService();  
        if(objUser.IsPortalEnabled){
            if(objUser.External_Vault_Id__c != null &&  objUser.External_Vault_Id__c != ''){
                extVaultId = objUser.External_Vault_Id__c;
                profileResponse = serv.fetchCustomerProfile(extVaultId);
                existingCreditDetails = profileResponse.getCreditCardDetails();
                ccNumber = existingCreditDetails.getCreditCardNumber();
                ccHolderName = existingCreditDetails.getCustomerName();
                ccZipCode = existingCreditDetails.getZipCode();
                selectedMonth = string.valueOf(existingCreditDetails.getExpirationMonth());
                selectedYear = string.valueOf(existingCreditDetails.getExpirationYear());
            }
            accountId = objUser.AccountId; 
        }           
    }
    
/**
* Method used to insert Receipts
*/
    private Map<Receipt_Junction__c, Id> mapJunctionHistoricalReceipt;
    public void insertReceipt() { // called as "action" within the VF page declaration
        // find out if there are existing payment schedule Receipt_Junction__c records for the IOs for this payment.
        Map<Id, Receipt_Junction__c> mapIOJunction = new Map<Id, Receipt_Junction__c>();
        for(Receipt_Junction__c rj: [
            SELECT Id, Amount__c, Opportunity__c, IO_Approval__c, Receipt__c
            FROM Receipt_Junction__c 
            WHERE IO_Approval__r.Name in :mapOfOpportunityToAmount.keySet()
            AND ( Amount__c = 0 OR Amount__c = null )
            AND Scheduled_Payment_Date__c != null
            ORDER BY Scheduled_Payment_Date__c desc
        ]){
            // Loop through the returned Receipts, ordered by Scheduled_Payment_Date__c desc.
            // As a result the map will contain one Junction for each potential IO, that being the 'oldest' unpaid junction
            mapIOJunction.put(rj.IO_Approval__c, rj);
        }
        
        if(objReceipt.Id == null) {
            objReceipt.Account__c = objUser.AccountId;
            objReceipt.Customer_Portal_User__c = objUser.Id;
            objReceipt.Type__c = 'Prepay Order';
            objReceipt.Payment_Type__c = 'Portal-Draft';//Lakshman - ticket - ESS-21663/ESS-23393, added new field payment type which determines if the payment was done from portal or any other medium
            insert objReceipt;
            List<Receipt_Junction__c> listRJ = new List<Receipt_Junction__c>();
            mapJunctionHistoricalReceipt = new Map<Receipt_Junction__c, Id>();
            for(IO_Detail__c ci : listIO) {
                if(mapOfOpportunityToAmount.containsKey(ci.Name)) {
                    if(mapIOJunction.containsKey(ci.Id)){ // If we found a Receipt Junction earlier add that one to the List of Junctions
                        Receipt_Junction__c thisRJ = mapIOJunction.get(ci.Id);
                        mapJunctionHistoricalReceipt.put(thisRJ, thisRJ.Receipt__c); // Add any moved Receipt Junctions to a Map to be reverted later if necessary.
                        thisRJ.Amount__c = decimal.valueOf(mapOfOpportunityToAmount.get(ci.Name)); // what to do here, we use this as one of the criteria to determine if the RJ is "paid" or not
                        thisRJ.Receipt__c = objReceipt.Id; // We need to associate to the new Receipt so that CC processing info saved on the Receipt is properly associated to the schedule.
                        listRJ.add(thisRJ);
                    } else {
                        listRJ.add(
                            new Receipt_Junction__c(
                                Amount__c = decimal.valueOf(mapOfOpportunityToAmount.get(ci.Name)), 
                                Opportunity__c = ci.Opportunity__c , 
                                IO_Approval__c = ci.Id, 
                                Receipt__c = objReceipt.Id)
                        );
                    }
                }
            }
            upsert listRJ;
        }
        objReceipt = [Select Id, Name, IsSuccess__c, Authorization__c from Receipt__c where Id =: objReceipt.Id];
    }
    
/**
* Method to initiate payment
*/
    public void payInvoice(){
        Paymentech_Log__c errorLogEntry; // placeholder for error logging
        try{
            CreditCardDetails creditCardDetails = new CreditCardDetails(
                ccHolderName, integer.valueOf(selectedMonth), integer.valueOf(selectedYear), ccZipCode, '' , cvvNumber
            );
            creditCardDetails.setCreditCardNumber(ccNumber); 
            // system.debug('******creditCardDetails'+creditCardDetails); commented so as to not expose cc information in the debug logs
            transactionErrorMessage = SI_PaymentechConstants.SI_GENERIC_ERROR_MESSAGE;                                                                      
            if(extVaultId == null){
                profileResponse = serv.createCustomerProfile(creditCardDetails);
                
                if(profileResponse.isSuccesful()){
                    extVaultId = profileResponse.getExtVaultId();
                } else {
                    isSuccess = false;
                    errorLogEntry = SI_PaymentechUtil.createLogEntry(
                        'Failed', 'CommerceService', 'createCustomerProfile', '', profileResponse.getErrorMessage(), 
                        'Outbound', JSON.serialize(mapOfOpportunityToAmount)
                    );
                }
            } else {
                if(SI_PaymentechUtil.hasCCNChanged(existingCreditDetails,ccNumber)){
                    profileResponse = serv.createCustomerProfile(creditCardDetails);
                    
                    if(profileResponse.isSuccesful()){
                        extVaultId = profileResponse.getExtVaultId();
                    } else {
                        isSuccess = false;
                        errorLogEntry = SI_PaymentechUtil.createLogEntry(
                            SI_PaymentechConstants.SI_FAILED, 'CommerceService', 'createCustomerProfile', '', profileResponse.getErrorMessage(),
                            SI_PaymentechConstants.SI_MESSAGE_TYPE_OUTBOUND, JSON.serialize(mapOfOpportunityToAmount)
                        );
                    }
                } else if(SI_PaymentechUtil.hasCCDChanged(existingCreditDetails,creditCardDetails)){
                    profileResponse = serv.updateCustomerProfile(extVaultId, creditCardDetails);
                    if(!profileResponse.isSuccesful()){
                        isSuccess = false;
                        errorLogEntry = SI_PaymentechUtil.createLogEntry(
                            SI_PaymentechConstants.SI_FAILED, 'CommerceService', 'updateCustomerProfile', '', profileResponse.getErrorMessage(), 
                            SI_PaymentechConstants.SI_MESSAGE_TYPE_OUTBOUND, JSON.serialize(mapOfOpportunityToAmount)
                        );
                    }
                }
            }
            if(isSuccess){
                system.debug('**** profileResponse: '+profileResponse);
                profileResponse = serv.fetchCustomerProfile(extVaultId);
                system.debug('**** extVaultId: '+extVaultId);
                system.debug('**** profileResponse: '+profileResponse);
                if(!profileResponse.isSuccesful()){
                    isSuccess = false;
                    errorLogEntry = SI_PaymentechUtil.createLogEntry(
                        SI_PaymentechConstants.SI_FAILED, 'CommerceService', 'fetchCustomerProfile', '', profileResponse.getErrorMessage(), 
                        SI_PaymentechConstants.SI_MESSAGE_TYPE_OUTBOUND, JSON.serialize(mapOfOpportunityToAmount)
                    );
                } else {
                    creditCardDetails = profileResponse.getCreditCardDetails();
                    creditCardDetails.setSecurityCode(cvvNumber);
                    Integer ccAmt = integer.valueOf(ccAmount*100);
                    system.debug('******ccAmount'+string.valueOf(ccAmt));
                    
                    CommerceTransactionResponse transactionResponse = serv.makeSettlement(creditCardDetails ,string.valueOf(ccAmt), objReceipt.Name, extVaultId);
                    if(transactionResponse.getApproved()){
                        system.debug('******transactionResponse'+transactionResponse);
                        //createCashEntry(transactionResponse.getTxRefNumber());
                        //updateSalesInvoice(transactionResponse.getTxRefNumber());
                        // system.debug('*********ccNumber'+ccNumber);
                        
                        objReceipt.Authorization__c = transactionResponse.getTxRefNumber();
                        objReceipt.IsSuccess__c = true;
                        objReceipt.Date_Of_Transaction__c = Date.today();
                        objReceipt.Payment_Type__c = 'Portal';
                        update objReceipt;
                        update listIO;
                        SI_CC_RECEIPT_SendEmail.sendReceipt(
                            accountId, objUser.Email, objReceipt.Name, string.valueOf(ccAmount), transactionResponse.getTxRefNumber(), ccNumber, ccHolderName, 
                            selectedMonth, selectedYear, 'Prepay Order'
                        ); 
                    } else {
                        errorLogEntry = SI_PaymentechUtil.createLogEntry(
                            SI_PaymentechConstants.SI_FAILED, 'CommerceService', 'makeSettlement', '', transactionResponse.getErrorMessage(), 
                            SI_PaymentechConstants.SI_MESSAGE_TYPE_OUTBOUND, JSON.serialize(mapOfOpportunityToAmount)
                        );
                        transactionErrorMessage = transactionResponse.getErrorMessage();
                        isSuccess = false;
                        system.debug('******transactionResponse'+transactionResponse);
                        system.debug('******creditCardDetails'+creditCardDetails);
                    }
                }                                                                             
            }
            if(!isSuccess){
                if(objReceipt.Id != null){
                    cleanReceipt();
                }
                if(errorLogEntry != null){
                    insert errorLogEntry;
                }
            }
            
        } catch(Exception ex){
            if(!objReceipt.IsSuccess__c){
                cleanReceipt();
            }
            system.debug('****Exception =>' + ex);
            if(!ex.getMessage().contains('Current company not set') && !isSuccess){
                transactionErrorMessage = SI_PaymentechConstants.SI_GENERIC_ERROR_MESSAGE;
            }
            errorLogEntry = SI_PaymentechUtil.createLogEntry(
                SI_PaymentechConstants.SI_FAILED, 'Exception', 'Exception', '', 'Message:'+ex.getMessage()+'\nStack trace:'+ex.getStackTraceString(), 
                SI_PaymentechConstants.SI_MESSAGE_TYPE_OUTBOUND, JSON.serialize(mapOfOpportunityToAmount)
            );
            insert errorLogEntry;                                                    
        } finally {
            if(objUser.External_Vault_Id__c != extVaultId){
                objUser.External_Vault_Id__c = extVaultId;
                update objUser;
            }
        }
    }
    
    private void cleanReceipt(){

        // If we included any receipt junctions from other receipts reassign them back to their initial Receipt record before removing the failed payment receipt record.
        if(!mapJunctionHistoricalReceipt.isEmpty()){
            List<Receipt_Junction__c> toRevert = new List<Receipt_Junction__c>();
            for(Receipt_Junction__c rj:mapJunctionHistoricalReceipt.keySet()){
                rj.Receipt__c = mapJunctionHistoricalReceipt.get(rj);
                rj.Amount__c = 0.00;
                toRevert.add(rj);
            }
            update toRevert;
        }
        delete objReceipt;
    }
    
    @isTest(seealldata=true)
    private static void testOpportunityController() {
        User testUser = [Select Id from User where Id =: SI_TestData__c.getValues('UserId').Value__c];
        Opportunity testOppty = UTIL_TestUtil.createOpportunity(UTIL_TestUtil.createAccount().id);
        IO_Detail__c testIO = new IO_Detail__c(Opportunity__c = testOppty.Id);
        insert testIO;
        
        Test.startTest();    
        String ioNumber = [Select Name from IO_Detail__c where Id =: testIO.Id].Name;
        UTIL_TestUtil.closeWinOpportunity(testOppty);
        testOppty.Amount = 2000;
        update testOppty;
        ApexPages.currentPage().getParameters().put('selectedOpportunities', EncodingUtil.base64Encode(Blob.valueOf(ioNumber + '-' + '10')));
        system.runAs(testUser) {
            system.debug('**********selectedOpportunities'+ApexPages.currentPage().getParameters().get('selectedOpportunities'));
            SI_UI_CUST_PORTAL_OpportunityPaymentExt objOpportunityTest = new SI_UI_CUST_PORTAL_OpportunityPaymentExt();
            objOpportunityTest.insertReceipt();
            objOpportunityTest.payInvoice();
            system.debug('***ValutId'+[Select External_Vault_Id__c from User where Id = : testUser.Id].External_Vault_Id__c);
            objOpportunityTest = new SI_UI_CUST_PORTAL_OpportunityPaymentExt();
            objOpportunityTest.insertReceipt();
            objOpportunityTest.payInvoice();
            
        }
        Test.stopTest();
    }
}