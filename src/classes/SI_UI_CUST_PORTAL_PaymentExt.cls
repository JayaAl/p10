/**
 * @name : SI_UI_CUST_PORTAL_PaymentExt
 * @desc : Controller for payment processing
 * @version: 1.1
 * @author: Lakshman(sfdcace@gmail.com)
 */
public class SI_UI_CUST_PORTAL_PaymentExt{
    /*Variables used on page level*/
    public String ccNumber {get;set;}
    public String cvvNumber {get;set;}
    public String ccHolderName {get;set;}
    public String ccHolerNumber {get;set;}
    public String ccZipCode {get;set;}
    public Boolean ccAgree {get;set;}
    public List<SelectOption> monthOptions {get;set;}
    public List<SelectOption> yearOptions {get;set;}
    public List<SelectOption> cardOptions {get;set;}
    public String selectedMonth {get;set;}
    public String selectedYear {get;set;}
    public String selectedCardType {get;set;}
    public Decimal ccAmount {get;set;}
    //public ERP_Invoice__c objSI {get;set;}
    public List<ERP_Invoice__c> listSI;
    
    /*Variables used for paymentTech*/
    public String accountId;
    public String extVaultId;
    public User objUser;
    public CommerceProfileResponse profileResponse;
    public CommerceService serv;
    public CreditCardDetails existingCreditDetails;
    public String transactionErrorMessage {get;set;}
    Map<String, String> mapOfInvoiceToAmount;
    public Boolean isSuccess {get;set;}
    public Receipt__c objReceipt {get;set;}
    
    /**
     * Default Constructor
     */
    public SI_UI_CUST_PORTAL_PaymentExt(){
        monthOptions = new List<SelectOption>();
        yearOptions = new List<SelectOption>();
        cardOptions = new List<SelectOption>();
        objReceipt = new Receipt__c();
        mapOfInvoiceToAmount = new Map<String, String> ();
        addYears();
        addMonths();  
        addCardTypes();
        isSuccess = true;
        
        mapOfInvoiceToAmount = decryptParameter(ApexPages.currentPage().getParameters().get('selectedInvoices'));
        
        listSI = [Select c.Payment_Status__c,
                        c.Name, c.Id, c.CC_Authorized__c,
                        c.Outstanding_Value__c ,
                        c.Company__c,
                        c.CurrencyIsoCode,
                        c.Advertiser__c
                   From ERP_Invoice__c c 
                  where Name =: mapOfInvoiceToAmount.keySet()] ;
        
        ccAmount = 0;
        for(ERP_Invoice__c ci : listSI) {
            if(decimal.valueOf(mapOfInvoiceToAmount.get(ci.Name)) != 0 && decimal.valueOf(mapOfInvoiceToAmount.get(ci.Name)) <= ci.Outstanding_Value__c) {
                ccAmount += decimal.valueOf(mapOfInvoiceToAmount.get(ci.Name));
                ci.CC_Authorized__c = true;
            }
        }
        
        objUser = [Select IsPortalEnabled, AccountId, External_Vault_Id__c, Email from User where Id =: UserInfo.getUserId()];
        serv = CommerceServiceFactory.getCommerceService();  
        if(objUser.IsPortalEnabled){
            if(objUser.External_Vault_Id__c != null &&  objUser.External_Vault_Id__c != ''){
                extVaultId = objUser.External_Vault_Id__c;
                profileResponse = serv.fetchCustomerProfile(extVaultId);
                existingCreditDetails = profileResponse.getCreditCardDetails();
                ccNumber = existingCreditDetails.getCreditCardNumber();
                ccHolderName = existingCreditDetails.getCustomerName();
                ccZipCode = existingCreditDetails.getZipCode();
                selectedMonth = string.valueOf(existingCreditDetails.getExpirationMonth());
                selectedYear = string.valueOf(existingCreditDetails.getExpirationYear());
            }
            accountId = objUser.AccountId; 
        }           
    }
    
    /**
     * Method to add months
     */
    public void addMonths(){
        monthOptions.clear();
        selectedMonth = '1';
        monthOptions.add(new SelectOption('1', '01 - January'));
        monthOptions.add(new SelectOption('2', '02 - February'));
        monthOptions.add(new SelectOption('3', '03 - March'));
        monthOptions.add(new SelectOption('4', '04 - April'));
        monthOptions.add(new SelectOption('5', '05 - May'));
        monthOptions.add(new SelectOption('6', '06 - June'));
        monthOptions.add(new SelectOption('7', '07 - July'));
        monthOptions.add(new SelectOption('8', '08 - August'));
        monthOptions.add(new SelectOption('9', '09 - September'));
        monthOptions.add(new SelectOption('10', '10 - October'));
        monthOptions.add(new SelectOption('11', '11 - November'));
        monthOptions.add(new SelectOption('12', '12 - December')); 
        if(selectedYear == string.valueOf(date.today().year()))
        selectedMonth = string.valueOf(date.today().month()); 
    }
    
    /**
     * Method to add years
     */
    public void addYears(){
        Integer currentYear = system.now().year();
        for(Integer i= currentYear; i<=currentYear+10; i++)
        yearOptions.add(new SelectOption(string.valueOf(i), string.valueOf(i)));
        
        selectedYear = string.valueOf(currentYear); 
        
    }
    
    /**
     * Method to add supported card types
     */
    public void addCardTypes(){
        cardOptions.add(new SelectOption('MasterCard', 'MasterCard'));
        cardOptions.add(new SelectOption('Visa', 'Visa'));
        cardOptions.add(new SelectOption('AmEx', 'American Express'));
        cardOptions.add(new SelectOption('Discover', 'Discover'));
    }
    
    /**
     * Method to decrypt encrypted parameters
     */
    public Map<String, String> decryptParameter(String encryptedParameter){
        if(encryptedParameter != null) {
            if(! mapOfInvoiceToAmount.isEmpty()) {
                mapOfInvoiceToAmount.clear();
            }
            String dp = EncodingUtil.base64Decode(EncodingUtil.urlDecode(encryptedParameter, 'UTF-8')).toString();
            for(String ia : dp.split(',')) {
                String[] invAmtSplit = ia.split('-');
                mapOfInvoiceToAmount.put(invAmtSplit[0], invAmtSplit[1]);
            }
        }
        return mapOfInvoiceToAmount;
    }
    
    /**
     * Method used to check if credit card details have changed or not
     * @param currentCreditDetails : checks previous with current credit card details
     * @return Boolean : Returns true if any of the credit card details are changed else returns false
     */
    public Boolean hasCCDChanged(CreditCardDetails currentCreditDetails){
        if(currentCreditDetails.getCustomerName() != existingCreditDetails.getCustomerName() ||
           currentCreditDetails.getZipCode() != existingCreditDetails.getZipCode() || 
           currentCreditDetails.getExpirationMonth() != existingCreditDetails.getExpirationMonth() ||
           currentCreditDetails.getExpirationYear() != existingCreditDetails.getExpirationYear()){
            return true;                
       }
       return false;
    }
    
    /**
     * Method used to check if credit card number has changed or not
     * @param currentCardNumber : checks previous with current current card number
     * @return Boolean : Returns true if credit card number is changed else returns false
     */
    
    public Boolean hasCCNChanged(String currentCardNumber){
        if(currentCardNumber != existingCreditDetails.getCreditCardNumber()){
            return true;
        }
        return false;
    }
    
    /**
     * Method used to update User object if we create a new Customer Profile
     */
    
    public void updateUser(){
        objUser.External_Vault_Id__c = extVaultId;
       update objUser;
    }
    
    /**
     * Method used to insert Receipts
     */
    
    public void insertReceipt() {
        if(objReceipt.Id == null) {
            objReceipt.Account__c = objUser.AccountId;
            objReceipt.Customer_Portal_User__c = objUser.Id;
            objReceipt.Type__c = 'ERP Invoice';
            insert objReceipt;
            List<Receipt_Junction__c> listRJ = new List<Receipt_Junction__c>();
            for(ERP_Invoice__c ci : listSI) {
                if(mapOfInvoiceToAmount.containsKey(ci.Name)) {
                    listRJ.add(new Receipt_Junction__c(Amount__c = decimal.valueOf(mapOfInvoiceToAmount.get(ci.Name)), ERP_Invoice__c = ci.Id, Receipt__c = objReceipt.Id));
    
                }
            }
            insert listRJ;
        }
        objReceipt = [Select Id, Name, IsSuccess__c, IsPaymentSuccess__c, Authorization__c from Receipt__c where Id =: objReceipt.Id];
    }
    
    
    /**
     * Method to initiate payment
     */
     
    public void payInvoice(){
        Paymentech_Log__c errorLogEntry; 
        try{
            CreditCardDetails creditCardDetails = new CreditCardDetails(ccHolderName, integer.valueOf(selectedMonth), integer.valueOf(selectedYear),
                                                                        ccZipCode, '' , cvvNumber);
            creditCardDetails.setCreditCardNumber(ccNumber); 
            system.debug('******creditCardDetails'+creditCardDetails); 
            transactionErrorMessage = SI_PaymentechConstants.SI_GENERIC_ERROR_MESSAGE;                                                                      
            if(extVaultId == null){
                profileResponse = serv.createCustomerProfile(creditCardDetails);
                
                if(profileResponse.isSuccesful()){
                    extVaultId = profileResponse.getExtVaultId();
                    //updateUser();
                }else{
                    isSuccess = false;
                    errorLogEntry = SI_PaymentechUtil.createLogEntry('Failed', 'CommerceService', 'createCustomerProfile', 
                                                     '', profileResponse.getErrorMessage(), 'Outbound', JSON.serialize(mapOfInvoiceToAmount));
                }
                
                
            }else{
                if(hasCCNChanged(ccNumber)){
                    profileResponse = serv.createCustomerProfile(creditCardDetails);
                    
                    if(profileResponse.isSuccesful()){
                        extVaultId = profileResponse.getExtVaultId();
                        //updateUser();
                    }else{
                        isSuccess = false;
                        errorLogEntry = SI_PaymentechUtil.createLogEntry(SI_PaymentechConstants.SI_FAILED, 'CommerceService', 'createCustomerProfile', 
                                                     '', profileResponse.getErrorMessage(), SI_PaymentechConstants.SI_MESSAGE_TYPE_OUTBOUND, JSON.serialize(mapOfInvoiceToAmount));
                    }
                }else if(hasCCDChanged(creditCardDetails)){
                   profileResponse = serv.updateCustomerProfile(extVaultId, creditCardDetails);
                   if(!profileResponse.isSuccesful()){
                        isSuccess = false;
                        errorLogEntry = SI_PaymentechUtil.createLogEntry(SI_PaymentechConstants.SI_FAILED, 'CommerceService', 'updateCustomerProfile', 
                                                     '', profileResponse.getErrorMessage(), SI_PaymentechConstants.SI_MESSAGE_TYPE_OUTBOUND, JSON.serialize(mapOfInvoiceToAmount));
                   }
                }
            }
            if(isSuccess){
                system.debug('****profileResponse'+profileResponse);
                profileResponse = serv.fetchCustomerProfile(extVaultId);
                system.debug('****extVaultId'+extVaultId);
                system.debug('****profileResponse'+profileResponse);
                if(!profileResponse.isSuccesful()){
                        isSuccess = false;
                        errorLogEntry = SI_PaymentechUtil.createLogEntry(SI_PaymentechConstants.SI_FAILED, 'CommerceService', 'fetchCustomerProfile', 
                                                     '', profileResponse.getErrorMessage(), SI_PaymentechConstants.SI_MESSAGE_TYPE_OUTBOUND, JSON.serialize(mapOfInvoiceToAmount));
                }else{
                    creditCardDetails = profileResponse.getCreditCardDetails();
                    creditCardDetails.setSecurityCode(cvvNumber);
                    Integer ccAmt = integer.valueOf(ccAmount*100);
                    system.debug('******ccAmount'+string.valueOf(ccAmt));
                    
                    CommerceTransactionResponse transactionResponse = serv.makeSettlement(creditCardDetails , 
                                                                                      string.valueOf(ccAmt), objReceipt.Name, extVaultId);
                    if(transactionResponse.getApproved()){
                        system.debug('******transactionResponse'+transactionResponse);
                        //createCashEntry(transactionResponse.getTxRefNumber());
                        //updateSalesInvoice(transactionResponse.getTxRefNumber());
                        system.debug('*********ccNumber'+ccNumber);
                        objReceipt.Authorization__c = transactionResponse.getTxRefNumber();
                        objReceipt.IsSuccess__c = true;
                        objReceipt.IsPaymentSuccess__c = true;
                        objReceipt.Date_Of_Transaction__c = Date.today();
                        update objReceipt;
                        update listSI;
                        SI_CC_RECEIPT_SendEmail.sendReceipt(accountId, objUser.Email, objReceipt.Name, string.valueOf(ccAmount), transactionResponse.getTxRefNumber(), ccNumber, ccHolderName, 
                                                            selectedMonth, selectedYear, 'Sales Invoice'); 
                    }else{
                        errorLogEntry = SI_PaymentechUtil.createLogEntry(SI_PaymentechConstants.SI_FAILED, 'CommerceService', 'makeSettlement', 
                                                         '', transactionResponse.getErrorMessage(), SI_PaymentechConstants.SI_MESSAGE_TYPE_OUTBOUND, JSON.serialize(mapOfInvoiceToAmount));
                        transactionErrorMessage = transactionResponse.getErrorMessage();
                        isSuccess = false;
                        system.debug('******transactionResponse'+transactionResponse);
                        system.debug('******creditCardDetails'+creditCardDetails);
                    }
                }                                                                             
            }
            if(!isSuccess){
                if(objReceipt.Id != null && !objReceipt.IsPaymentSuccess__c)
                delete objReceipt;
                if(errorLogEntry != null)
                insert errorLogEntry;
            }
            
        }catch(Exception ex){
            if(objReceipt.Id != null && !objReceipt.IsSuccess__c && !objReceipt.IsPaymentSuccess__c)
            delete objReceipt;
            system.debug('****Exception =>' + ex);
            if(!ex.getMessage().contains('Current company not set') && !isSuccess){
                transactionErrorMessage = SI_PaymentechConstants.SI_GENERIC_ERROR_MESSAGE;
            }
            errorLogEntry = SI_PaymentechUtil.createLogEntry(SI_PaymentechConstants.SI_FAILED, 'Exception', 'Exception', 
                                                     '', 'Message:'+ex.getMessage()+'\nStack trace:'+ex.getStackTraceString(), 
                                                     SI_PaymentechConstants.SI_MESSAGE_TYPE_OUTBOUND, JSON.serialize(mapOfInvoiceToAmount));
            insert errorLogEntry;                                                    
        }finally{
            if(objUser.External_Vault_Id__c != extVaultId){
                updateUser();
            }
        }
    }
    
    @isTest(seealldata=true)
    private static void testSalesInvoiceController() {
            User testUser = [Select Id from User where Id =: SI_TestData__c.getValues('UserId').Value__c];
            ApexPages.currentPage().getParameters().put('selectedInvoices',EncodingUtil.base64Encode(Blob.valueOf(SI_TestData__c.getValues('InvoiceNumber').Value__c + '-' + '10')));
            system.runAs(testUser) {
                system.debug('**********invName'+ApexPages.currentPage().getParameters().get('selectedInvoices'));
                SI_UI_CUST_PORTAL_PaymentExt objSalesInvoiceTest = new SI_UI_CUST_PORTAL_PaymentExt();
                objSalesInvoiceTest.insertReceipt();
                objSalesInvoiceTest.payInvoice();
                system.debug('***ValutId'+[Select External_Vault_Id__c from User where Id = : testUser.Id].External_Vault_Id__c);
                objSalesInvoiceTest = new SI_UI_CUST_PORTAL_PaymentExt();
                objSalesInvoiceTest.insertReceipt();
                objSalesInvoiceTest.payInvoice();
        }
    }
}