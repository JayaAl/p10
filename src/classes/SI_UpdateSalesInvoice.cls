/* TODO: Delete this Class as it has been deprecated/supersceded by ERP functionality. */
    global class SI_UpdateSalesInvoice implements Messaging.InboundEmailHandler {
        global Messaging.InboundEmailResult handleInboundEmail(Messaging.InboundEmail email, 
                                                               Messaging.Inboundenvelope envelope) {
                                                                   
/*
    // Create an inboundEmailResult object for returning 
    // the result of the Force.com Email Service
    Messaging.InboundEmailResult result = new Messaging.InboundEmailResult();
    
    String salesInvoiceNumber = '';
    try{
    salesInvoiceNumber = email.subject.substringAfter('Pandora Receipt # ');
    system.debug('salesInvoiceNumber****'+salesInvoiceNumber);
    }catch (System.StringException e){
    Paymentech_Log__c errorLogEntry = SI_PaymentechUtil.createLogEntry('Failed', 'SI_UpdateSalesInvoice', 'handleInboundEmail', 
    '', 'No \'Pandora Receipt # \' in email: ' + e.getMessage(), 'Outbound', salesInvoiceNumber);
    insert errorLogEntry;
    System.debug('No \'Pandora Receipt # \' in email: ' + e);
    }
    if(salesInvoiceNumber != ''){
    try{
    List<c2g__codaInvoice__c> listSalesInvoice = [Select Id,
    CC_Authorized__c, 
    Name 
    From c2g__codaInvoice__c
    Where Id In (Select Sales_Invoice__c from Receipt_Junction__c where Receipt__r.Name =: salesInvoiceNumber)];
    
    for(c2g__codaInvoice__c ci :listSalesInvoice) {
    ci.CC_Authorized__c = true;
    }
    if(! listSalesInvoice.isEmpty()) {
    update listSalesInvoice;
    }
    }catch(Exception ex){
    Paymentech_Log__c errorLogEntry = SI_PaymentechUtil.createLogEntry('Failed', 'SI_UpdateSalesInvoice', 'handleInboundEmail', 
    '', ex.getMessage(), 'Outbound', salesInvoiceNumber);
    insert errorLogEntry;                                                      
    system.debug('Exception ex=>'+ex);
    }                                                         
    }
    // Set the result to true, no need to send an email back to the user
    // with an error message
    
    result.success = true;
    
    // Return the result for the Force.com Email Service
    return result;
*/
    return null;                                                               
	}
        /*
    //Test Method
    @isTest(seealldata=true)
    private static void testSalesInvoiceUpdate() {
    //Insert Receipt
    Receipt__c objR = new Receipt__c(Type__c = 'Sales Invoice');
    insert objR;
    String invoiceId = [Select Id from c2g__codaInvoice__c where Name = 'SIN009073' Limit 1].Id;
    Receipt_Junction__c objRJ = new Receipt_Junction__c(Sales_Invoice__c = invoiceId, Receipt__c = objR.Id);
    insert objRJ;
    // Create a new email and envelope object
    Messaging.InboundEmail email = new Messaging.InboundEmail();
    Messaging.InboundEnvelope env = new Messaging.InboundEnvelope();
    
    // Create the plainTextBody and fromAddres for the test
    email.plainTextBody = 'Here is my plainText body of the email';
    email.fromAddress ='sfdcace@pandora.com';
    objR = [Select Name, Id from Receipt__c where Id = :objR.Id];
    email.subject ='Pandora Receipt # ' + objR.Name;
    
    SI_UpdateSalesInvoice objSI = new SI_UpdateSalesInvoice();
    objSI.handleInboundEmail(email, env);
    }
    */
    }