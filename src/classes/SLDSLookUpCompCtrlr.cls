global with sharing class SLDSLookUpCompCtrlr{
    
    public transient String sobjectNameStr {get;set;}
    public transient String sobjectLabelStr {get;set;}
    public transient String sobjectFilterStr {get;set;}
        
    
    
    /*@Author :AB
    *@Description : Remote Action used for querying data for specified SObject type with filters for keywords to searcha nd any additional filter
    */
    @RemoteAction
    global static list<ResultWrapper> queryData(String keyword,String sobjectNameStr,String sobjectFilterStr ) {
        
        List<SObject> contactList = new List<Contact>();

        
        if (keyword != null && keyword.trim() != '') {
            
            keyword ='\'%' + keyword   + '%\'';

            String queryStr = 'Select Id,Name from '+sobjectNameStr+' where Name like '+keyword;
            queryStr += (!String.isBlank(sobjectFilterStr))? sobjectFilterStr : '';
            queryStr +=' limit 5';
            system.debug('queryStr ::: '+queryStr);
            system.debug(DataBase.Query(queryStr));
            //sobjectFilterStr += (sobjectFilterStr != null && sobjectFilterStr != '') ? (' '+sobjectFilterStr )
            List<ResultWrapper> resultWrapperlst = new List<ResultWrapper>(); 
            for(SObject sobjectVar : DataBase.Query(queryStr)){
                // TODO: Remove if blocks and replace it with SObject Logic
                if(sobjectNameStr == 'Contact'){
                    Contact contactObj = new Contact();
                    contactObj = (Contact) sobjectvar;
                    resultWrapperlst.add(new ResultWrapper(contactObj.id,contactObj.name)); 
                }else if(sobjectNameStr == 'Account'){
                    Account accObj = new Account();
                    accObj = (Account) sobjectvar;
                    resultWrapperlst.add(new ResultWrapper(accObj.id,accObj.name)); 
                }
                
            }

            return resultWrapperlst;
        }

        return null;
    }
    
    global class ResultWrapper{
        
        public String IdStr {get;set;}
        public String NameStr {get;set;}
        
        public ResultWrapper(String IdStrVal, String NameStrVal){
            IdStr  = IdStrVal;
            NameStr = NameStrVal;
        }
    } 

}