@istest(seeAllData = false)
public class SLDSLookUpCompCtrlrTest{
    
    public static Account testAccount;
    public static Contact testContact;

    public static void setUpdata(){
      testAccount = UTIL_TestUtil.createAccount();
      testContact = UTIL_TestUtil.createContact(testAccount.id);
    }
    
    public static testMethod void querydataTest(){
        setUpdata();
        Test.starttest();
        list<SLDSLookUpCompCtrlr.ResultWrapper> resultLst = SLDSLookUpCompCtrlr.queryData('Te','Account','');
        system.assert(resultLst != null);
        resultLst = SLDSLookUpCompCtrlr.queryData('Te','Contact','');
        system.assert(resultLst != null);
        Test.stoptest();
        
    }

}