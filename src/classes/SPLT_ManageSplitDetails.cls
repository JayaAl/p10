/*
* This class is used to reevaluate Split detail objects for Opportunity Splits.
* Author: Apprivo. 
    Changes:
    10/01: nsk:- ISS-10470 - Adding custom logging to capture exceptions 
*/
public with sharing class SPLT_ManageSplitDetails {

/*  
    ##################################################
    ##### New Methods Start ##########################
    ##################################################
*/
    public static List<OpportunityLineItem> listOLIByOpptyId(Set<Id> setOppId){
        Integer queryRowLimit = Limits.getLimitQueryRows() - Limits.getQueryRows();

        List<OpportunityLineItem> listOLI = [
            SELECT End_Date__c, ServiceDate, OpportunityId, TotalPrice, HasSchedule, HasRevenueSchedule, PricebookEntry.Product2Id, PricebookEntry.Name, CurrencyIsoCode,
                Banner__c, Banner_Type__c, Cost_Type__c, Medium__c, Offering_Type__c, Platform__c, Size__c, Sub_Platform__c, Takeover__c, 
                (SELECT Revenue, ScheduleDate FROM OpportunityLineItemSchedules)
            FROM OpportunityLineItem
            WHERE OpportunityId IN :setOppId Limit : queryRowLimit
        ];
        return listOLI;
    }

    public static Split_Detail__c splitDetailFromOLI(OpportunityLineItem OLI){
        String fixedOffer = (OLI.Offering_Type__c==null||OLI.Offering_Type__c=='')?'[None]':OLI.Offering_Type__c;
        String fixedMedium = (OLI.Medium__c==null||OLI.Medium__c=='')?'[None]':OLI.Medium__c;
        Split_Detail__c splitDetail = new Split_Detail__c(
            Name                = OLI.PricebookEntry.Name,
            Product__c          = OLI.PricebookEntry.Product2Id,
            CurrencyIsoCode     = OLI.CurrencyIsoCode,
            Banner__c           = OLI.Banner__c,
            Banner_Type__c      = OLI.Banner_Type__c,
            Cost_Type__c        = OLI.Cost_Type__c,
            Medium__c           = OLI.Medium__c,
            Offering_Type__c    = OLI.Offering_Type__c,
            Platform__c         = OLI.Platform__c,
            Size__c             = OLI.Size__c,
            Sub_Platform__c     = OLI.Sub_Platform__c,
            Takeover__c         = OLI.Takeover__c,
            Offering_Type_Medium__c = fixedOffer+' / '+fixedMedium
            /* TODO: Figure this out
            String tempId       = oppSplit.Salesperson__c;
            tempId              = tempId.substring(0,15);
            Salesperson__c      = userMap.get(tempId),
            
            Opportunity_Split__c = oppSplit.Id,
            Date__c             = splitDate,
            Amount__c           = productMap.get(splitDate)*(oppSplit.Split__c/100),
            
            Salesperson_User__c = oppSplit.Salesperson__c,
            */
        );
        return splitDetail;
    }

/*
    ##################################################
    ##### New Methods End ############################
    ##################################################
*/    
    public static void createSplitsAfterDelete(Set<id> opportunityIdSet) {       

        // Fetching all OpportunityLineItem records associated with the opportunities.
        List<OpportunityLineItem> lineItemList = listOLIByOpptyId(opportunityIdSet);

        // This outer map has Opportunity Line Item's Id as key.
        // The inner map has Date values as key and amount as its value.
        // The dates are from the schedule split of opportunity line items with day values (retaining month and year) set to 1.
        // Since the day value is set to 1, the amount for the entire month is added up and is stored as the inner map's value.
        // Map<OLI.Id, Map<Schedule.ScheduleDate, Schedule.Revenue>>
        Map<Id,Map<Date,Decimal>> nestedMap = new Map<Id,Map<Date,Decimal>>();  
        
        // Map of Opportunity ID to a List of OLIs
        Map<Id,List<OpportunityLineItem>> oppLineItemMap = new Map<Id,List<OpportunityLineItem>>();

        // Loop through the OLIs, populating the above Maps
        for(OpportunityLineItem oppLineItem : lineItemList){
            
            // Formulating a list of opportunity line items associated with each of the opportunity (to be used later).
            List<OpportunityLineItem> oppLineItemList = oppLineItemMap.get(oppLineItem.OpportunityId);
            if(oppLineItemList == null){
                oppLineItemList = new List<OpportunityLineItem>();
                oppLineItemMap.put(oppLineItem.OpportunityId,oppLineItemList);
            }
            oppLineItemList.add(oppLineItem);
            
            // Formulating the nested map.
            Map<Date,Decimal> productMap = nestedMap.get(oppLineItem.Id);
            if(productMap == null){
                productMap = new Map<Date,Decimal>();
                nestedMap.put(oppLineItem.Id,productMap);
            }
            
            // create or update productMap values based on Opportunity OLIs 
            if(oppLineItem.HasSchedule){ // If the OLI has Product Schedules use them
                for(OpportunityLineItemSchedule scheduleItem: oppLineItem.OpportunityLineItemSchedules){
                    Date key = Date.newInstance(scheduleItem.ScheduleDate.year(), scheduleItem.ScheduleDate.month(), 1);
                    Decimal amount = productMap.get(key);
                    if(amount == null){
                        amount=0;
                    }   
                    amount = amount + scheduleItem.Revenue;
                    productMap.put(key,amount);
                }
            } else if (oppLineItem.ServiceDate != null) { // Otherwise base the details off of the ServiceDate and End_Date__c of the OLI, splitting evenly between all months spanned.
                Integer months = oppLineItem.ServiceDate.monthsBetween(oppLineItem.End_Date__c);
                for(Integer i=0; i < (months+1);i++){
                    Date key = Date.newInstance(oppLineItem.ServiceDate.year(), oppLineItem.ServiceDate.month()+i, 1);
                    Decimal amount = productMap.get(key);
                    if(amount == null){
                        amount = 0;
                     }  
                     amount = amount + oppLineItem.TotalPrice/(months+1);    
                     productMap.put(key,amount);
                }
            }
        }
        
        List<Opportunity_Split__c> oppSplitList = [
            SELECT Split__c, Opportunity__c, Opportunity__r.Probability, Probability__c, CurrencyIsoCode, Salesperson__c 
            FROM Opportunity_Split__c 
            WHERE Opportunity__c IN :opportunityIdSet for update
        ];//8_6_2012, Added for update, To resolve incorrect split details creation issue
         
        // Creating a new set of split detail objects.
        // For each Opportunity Line Item and for each schedule date (month, year) combination a new split detail
        // object is created.
        List<Split_Detail__c> insertList = new List<Split_Detail__c>();
        for(Opportunity_Split__c oppSplit: oppSplitList){
            List<OpportunityLineItem> oppLineItemList = oppLineItemMap.get(oppSplit.Opportunity__c);
            if (oppLineItemList == null) oppLineItemList = new List<OpportunityLineItem>();
            for(OpportunityLineItem lineItem: oppLineItemList){
                Map<Date,Decimal> productMap = nestedMap.get(lineItem.Id);
                System.debug('productMap: ' + productMap);
                for(Date splitDate: productMap.keySet()){
                     Split_Detail__c splitDetail = splitDetailFromOLI(lineItem);
                     splitDetail.Opportunity_Split__c = oppSplit.Id;
                     splitDetail.Date__c = splitDate;
                     splitDetail.Salesperson_User__c = oppSplit.Salesperson__c;//Updated by Lakshman on 16-4-2013 to update the Salesperson User for Hoopla
                     splitDetail.Amount__c = productMap.get(splitDate)*(oppSplit.Split__c/100);
                     insertList.add(splitDetail);                   
                }
            }
        }
        
        String listVal = '' +opportunityIdSet;
        try{ // Delete existing Split_Detail__c records for the Opportunities, then insert those we just created
            // 8_6_2012 Moved here instaed of invoking function deleteExistingSplits, To resolve incorrect split details creation issue
            delete [ SELECT Id FROM Split_Detail__c WHERE Opportunity_Split__r.Opportunity__c in :opportunityIdSet];
            if(insertList.size() > 0){          
                insert insertList;
            }
        } catch(Exception e){
            Logger.logMessage(
                'SPLT_ManageSplitDetails',
                'createSplitsAfterDelete',
                'Failed to delete and insert Split Details - Err: '+e,
                'Opportunity Split',
                listVal,
                'Error'
            );        
        }
    }

/*
* This method creates/deletes split types for opportunity split objects within each of the opportunity.
*/
    @future
    public static void evaluateSplitTypesAsync(Set<Id> opportunityIdSet) {
        evaluateSplitTypes(opportunityIdSet);
    }
    
/*
* This method creates/deletes split types for opportunity split objects within each of the opportunity.
*/
    public static void evaluateSplitTypes(Set<Id> opportunityIdSet){
        createSplitsAfterDelete(opportunityIdSet);
    }

    public static List<Split_Detail__c> getSplitForecast(List<Opportunity_Split__c> oppSplitList, Set<Id> opportunityIdSet) {
                // Fetching all the line itmes associated with the opportunities.
        List<OpportunityLineItem> lineItemList = listOLIByOpptyId(opportunityIdSet);

        System.debug('lineItemList = ' + lineItemList);
        if(lineItemList == null || lineItemList.size() == 0) return null;
            
        Map<Id,List<OpportunityLineItem>> oppLineItemMap = new Map<Id,List<OpportunityLineItem>>();
                
        // This outer map has Opportunity Line Item's Id as key.
        // The inner map has Date values as key and amount as its value.
        // The dates are from the schedule split of opportunity line items with day values (retaining month and year) set to 1.
        // Since the day value is set to 1, the amount for the entire month is added up and is stored as the inner map's value.
        Map<Id,Map<Date,Decimal>> nestedMap = new Map<Id,Map<Date,Decimal>>();  
        
        for(OpportunityLineItem oppLineItem : lineItemList){
            
            // Formulating a list of opportunity line items associated with each of the opportunity (to be used later).
            List<OpportunityLineItem> oppLineItemList = oppLineItemMap.get(oppLineItem.OpportunityId);
            if(oppLineItemList == null){
                oppLineItemList = new List<OpportunityLineItem>();
                oppLineItemMap.put(oppLineItem.OpportunityId,oppLineItemList);
            }
            oppLineItemList.add(oppLineItem);
            
            // Formulating the nested map.
            Map<Date,Decimal> productMap = nestedMap.get(oppLineItem.Id);
            if(productMap == null){
                productMap = new Map<Date,Decimal>();
                nestedMap.put(oppLineItem.Id,productMap);
            }
            
            // If a line item does not have schedule, then using it's start date instead of the Schedule's date.
            // to sum up values.
            if(oppLineItem.HasSchedule){
                List<OpportunityLineItemSchedule> scheduleList = oppLineItem.OpportunityLineItemSchedules;
                for(OpportunityLineItemSchedule scheduleItem: scheduleList){
                    Date key = Date.newInstance(scheduleItem.ScheduleDate.year(), scheduleItem.ScheduleDate.month(), 1);
                    Decimal amount = productMap.get(key);
                    if(amount == null){
                        amount=0;
                    }   
                    amount = amount + scheduleItem.Revenue;
                    productMap.put(key,amount);
                }
            }else{
                if (oppLineItem.ServiceDate != null) {
                    Integer months = oppLineItem.ServiceDate.monthsBetween(oppLineItem.End_Date__c);
                    for(Integer i=0; i < (months+1);i++){
                        Date key = Date.newInstance(oppLineItem.ServiceDate.year(), oppLineItem.ServiceDate.month()+i, 1);
                        Decimal amount = productMap.get(key);
                        if(amount == null){
                            amount = 0;
                         }  
                         amount = amount + oppLineItem.TotalPrice/(months+1);    
                         productMap.put(key,amount);
                    }
                }
            }
        }
        
        //Find Salespeople names for use in Forecast Area
        Map<String, String>userMap = new Map<String, String>();
        if (oppSplitList != null && !oppSplitList.isEmpty()) {
            Set<String>uIds = new Set<String>();
            for (Opportunity_Split__c cur : oppSplitList) {
                uIds.add(cur.Salesperson__c);
            }   
            List<User>uList = [select FirstName, LastName, Id from User where id in:uIds];
            
            if (uList != null && !uList.isEmpty()) {
                for (User u : uList) {
                    String temp = u.Id;
                    temp = temp.substring(0,15);
                    userMap.put(temp, u.FirstName + ' ' + u.LastName);
                }
            }
        }
        
        // Creating a new set of split detail objects.
        // For each Opportunity Line Item and for each schedule date (month, year) combination a new split detail
        // object is created.
        List<Split_Detail__c> returnList = new List<Split_Detail__c>();
        for(Opportunity_Split__c oppSplit: oppSplitList){
            
            List<OpportunityLineItem> oppLineItemList = oppLineItemMap.get(oppSplit.Opportunity__c);
            for(OpportunityLineItem lineItem: oppLineItemList){
                Map<Date,Decimal> productMap = nestedMap.get(lineItem.Id);
                for(Date splitDate: productMap.keySet()){
                     Split_Detail__c splitDetail = splitDetailFromOLI(lineItem);
                     splitDetail.Opportunity_Split__c = oppSplit.Id;
                     splitDetail.Date__c = splitDate;
                     splitDetail.Amount__c = productMap.get(splitDate)*(oppSplit.Split__c/100);
                     //splitDetail.Expected_Revenue__c = splitDetail.Amount__c * (oppSplit.Probability__c/100);
                     String tempId = oppSplit.Salesperson__c;
                     tempId = tempId.substring(0,15);
                     splitDetail.Salesperson__c = userMap.get(tempId);
                     splitDetail.Salesperson_User__c = oppSplit.Salesperson__c;  //Updated by VG on 05/082013 to update the Salesperson User for Hoopla
                     returnList.add(splitDetail);                   
                }
            }
        }
        return returnList;
    }
    
    public static List<SPLT_SplitForecastWrapper> organizeSplitForecase(List<Split_Detail__c> splitDetails) {
        List<SPLT_SplitForecastWrapper> returnList = new List<SPLT_SplitForecastWrapper>();
        Map<String,Map<String, Decimal>>productTotalMap = new Map<String,Map<String, Decimal>>();

        for (Split_Detail__c cur : splitDetails) {
            String salespersonKey = cur.Salesperson__c + ',' + cur.Date__c;
            if (productTotalMap.containsKey(salespersonKey)) {
                Map<String, Decimal>tMap = productTotalMap.get(salespersonKey); 
                if (tMap.containsKey(cur.Medium__c)) {
                    Decimal temp = tMap.get(cur.Medium__c) + cur.Amount__c;
                    tMap.put(cur.Medium__c, temp);
                } else { 
                    tMap.put(cur.Medium__c, cur.Amount__c);
                }
            } else {
                productTotalMap.put(salespersonKey, new Map<String, Decimal>{cur.Medium__c => cur.Amount__c});
            }
        }
        Set<String>salespersonKeys = productTotalMap.keySet();
        
        //Sort by date
        List<String>salesKeysList = new List<String>();
        salesKeysList.addAll(salespersonKeys);
        salesKeysList.sort();
        
        //Fill in blank cells
        Set<String>prodKeys = new Set<String>();
        for (String curKey : salespersonKeys) {
            prodKeys.addAll(productTotalMap.get(curKey).keySet());
        }
        for (String curKey : salespersonKeys) {
            Map<String, Decimal>prodMap = productTotalMap.get(curKey);
            for (String prodKey : prodKeys) {
                if(!prodMap.containsKey(prodKey)) {
                    prodMap.put(prodKey, 0);
                }
            }   
        }
        
        List<String>productNames = new List<String>();
        List<Decimal>productTotals = new List<Decimal>();
        for (String curKey : salesKeysList) {
            Map<String, Decimal>productMap = productTotalMap.get(curKey); 
            Set<String>productKeys = productMap.keySet(); 
            List<String>nameDate = curKey.split(',');
            List<Decimal>amounts = new List<Decimal>(); 
            for (String productKey : productKeys) {
                amounts.add(productMap.get(productKey));
            }
            returnList.add(new SPLT_SplitForecastWrapper(nameDate[0], fixDate(nameDate[1]), amounts, prodKeys));
        }
        
        return returnList; 
    }
    
    public static SPLT_SplitForecastWrapper getSubtotalLine(List<SPLT_SplitForecastWrapper> sList) {
        List<Decimal>prodTotals = new List<Decimal>();
        Boolean firstTimeThrough = true;
        for (SPLT_SplitForecastWrapper cur : sList) {
            Integer index = 0;
            for (Decimal curDec : cur.productTotals) { 
                if (firstTimeThrough) {
                    prodTotals.add(curDec);
                } else {
                    prodTotals[index] += curDec;
                }
                index++;
            }
            firstTimeThrough = false;
        }
        
        return new SPLT_SplitForecastWrapper('Subtotal', prodTotals); 
    }
    
    public static String fixDate(String origDate) {
        List<String>sList = origDate.split('-');
        return sList[1] + '/' + sList[0];
    }
    
    public static Set<String> getProductNames(SPLT_SplitForecastWrapper splitWrapper) {
        return splitWrapper.prodNames;
    }
}