public class SPLT_OpportunitySplit {

    // called from OpportunityBefore trigger
    public static void Assign1stSalesperson(List<Opportunity> Opportunities) {
        for (Opportunity o : Opportunities)
            if (o.X1st_Salesperson__c != o.OwnerId)
                o.X1st_Salesperson__c = o.OwnerId;
    }
    
    // This method initializes split and associated Salesperson fields to 0 and null.
    // called from OpportunityBefore trigger
    public static void initializeOpportunityFields(List<Opportunity> opportunityList){
        for(Opportunity opportunity:opportunityList){
            opportunity.X1st_Salesperson_Split__c = 100;
            opportunity.X2nd_Salesperson__c = null;
            opportunity.X2nd_Salesperson_Split__c = 0;
            opportunity.X3rd_Salesperson_Split__c = 0;
            opportunity.X3rd_Salesperson__c = null;
            opportunity.X4th_Salesperson_Split__c = 0;
            opportunity.X4th_Salesperson__c = null;  
            opportunity.X5th_Salesperson_Split__c = 0;
            opportunity.X5th_Salesperson__c = null;  
            opportunity.X6th_Salesperson_Split__c = 0;
            opportunity.X6th_Salesperson__c = null;  
        }
    }
    // This method checks whether the owner for any opportunity has changed and if so
    // updates the Opportunity_Owner__c flag on Opportunity_Split__c accordingly.
    // Used by OpportunityAfterUpdate trigger.
    public static void checkOpportunityOwner(Map<Id,Opportunity> oldMap,Map<Id,Opportunity> newMap){
        // Set to hold opportunity ids that have new owner.
        Set<Id> affectedOpportunitySet = new Set<Id>();
        
        // formulating affected opportunities that have new owners
        for(Opportunity oldOpportunity: oldMap.values()){
            Opportunity newOpportunity = newMap.get(oldOpportunity.Id);
            if(newOpportunity.OwnerId != oldOpportunity.OwnerId){
                affectedOpportunitySet.add(newOpportunity.Id);
            }
        }
        
        // retrieving opportunity split records for all the affected opportunities.
        List<Opportunity> opportunityList = 
            [Select Id,OwnerId,(Select Opportunity_Owner__c,Salesperson__c From Opportunity_Split__r) 
                                                            From Opportunity o where id in :affectedOpportunitySet];
        
        // flag to determine if the opportunity split object has undergone changes or not.
        boolean dirtyFlag = false;
        List<Opportunity_Split__c> oSplitList = new List<Opportunity_Split__c>();
        
        // checking and updating the Opportunity_Split__c field
        for(Opportunity opportunity:opportunityList){
            for(Opportunity_Split__c oSplit: opportunity.Opportunity_Split__r){
                dirtyFlag = false;
                if(oSplit.Opportunity_Owner__c){
                    dirtyFlag = true;
                    oSplit.Opportunity_Owner__c = false;
                }
                if(oSplit.Salesperson__c == opportunity.OwnerId){
                    dirtyFlag = true;
                    oSplit.Opportunity_Owner__c = true;
                } 
                if(dirtyFlag){
                    oSplitList.add(oSplit);
                }
            }
        } 
        
        // updating affected objects into the database.
        update oSplitList;                                                   
    }
    /*
    *    @authur: Lakshman(sfdcace@gmail.com)
    *    @date: 16-10-2013
    *    @desc: Checks if Opp Currency ISO is changed and updates Splits currency if mismatch is found
    */
    public static void checkOpportunityCurrencyISO(Map<Id,Opportunity> oldMap,Map<Id,Opportunity> newMap){
        // Set to hold opportunity ids that have new curency.
        Set<Id> affectedOpportunitySet = new Set<Id>();
        
        // formulating affected opportunities that have new currencies
        for(Opportunity oldOpportunity: oldMap.values()){
            Opportunity newOpportunity = newMap.get(oldOpportunity.Id);
            if(newOpportunity.CurrencyIsoCode != oldOpportunity.CurrencyIsoCode){
                affectedOpportunitySet.add(newOpportunity.Id);
            }
        }
        
        if(! affectedOpportunitySet.isEmpty()) {
            // retrieving opportunity split records for all the affected opportunities.
            List<Opportunity> opportunityList = 
                [Select Id,CurrencyIsoCode,(Select Id, CurrencyIsoCode From Opportunity_Split__r) 
                                                                From Opportunity o where id in :affectedOpportunitySet];
            
            List<Opportunity_Split__c> oSplitList = new List<Opportunity_Split__c>();
            
            // checking and updating the Opportunity_Split__c currency iso code
            for(Opportunity opportunity: opportunityList){
                for(Opportunity_Split__c oSplit: opportunity.Opportunity_Split__r){
                    if(oSplit.CurrencyIsoCode != opportunity.CurrencyIsoCode){
                        oSplit.CurrencyIsoCode = opportunity.CurrencyIsoCode;
                        oSplitList.add(oSplit);
                    }
                }
            } 
            
            if(! oSplitList.isEmpty()) {
                // updating affected objects into the database.
                update oSplitList; 
            }
        }
    }
     
    public static void updateSplitsOnOpptyChange(Map<Id,Opportunity> oldMap,Map<Id,Opportunity> newMap){

        try{

            // Set to hold opportunity ids that have new owner.
            Set<Id> affectedOpportunitySet = new Set<Id>();
            Set<Id> affectedOwnerIdOpptySet = new Set<Id>();
            Set<Id> affectedISOCodeOpptySet = new Set<Id>();
            // formulating affected opportunities that have new owners
            for(Opportunity oldOpportunity: oldMap.values()){
                Opportunity newOpportunity = newMap.get(oldOpportunity.Id);
                
                if(newOpportunity.OwnerId != oldOpportunity.OwnerId){
                    affectedOpportunitySet.add(newOpportunity.Id);
                    affectedOwnerIdOpptySet.add(newOpportunity.Id);
                }

                if(newOpportunity.CurrencyIsoCode != oldOpportunity.CurrencyIsoCode){
                    affectedOpportunitySet.add(newOpportunity.Id);
                    affectedISOCodeOpptySet.add(newOpportunity.Id);                
                }
            }

             List<Opportunity> opportunityList = 
                [Select Id,CurrencyIsoCode,OwnerId,(Select Opportunity_Owner__c,CurrencyIsoCode,Salesperson__c From Opportunity_Split__r) 
                                                                From Opportunity o where id in :affectedOpportunitySet];
            // flag to determine if the opportunity split object has undergone changes or not.
            boolean dirtyFlag = false;
            List<Opportunity_Split__c> oSplitList = new List<Opportunity_Split__c>();
            
            // checking and updating the Opportunity_Split__c field
            for(Opportunity opportunity:opportunityList){
                for(Opportunity_Split__c oSplit: opportunity.Opportunity_Split__r){
                    
                    if(affectedOwnerIdOpptySet.contains(opportunity.Id)){
                        dirtyFlag = false;
                        if(oSplit.Opportunity_Owner__c){
                            dirtyFlag = true;
                            oSplit.Opportunity_Owner__c = false;
                        }
                        if(oSplit.Salesperson__c == opportunity.OwnerId){
                            dirtyFlag = true;
                            oSplit.Opportunity_Owner__c = true;
                        } 
                        if(dirtyFlag){
                            oSplitList.add(oSplit);
                        }
                    }else if(affectedISOCodeOpptySet.contains(opportunity.Id)){

                        if(oSplit.CurrencyIsoCode != opportunity.CurrencyIsoCode){
                            oSplit.CurrencyIsoCode = opportunity.CurrencyIsoCode;
                            oSplitList.add(oSplit);
                        }
                    }
                }
            } 

            if(!oSplitList.isEmpty()){
                update oSplitList;
            }

        }catch(Exception e){
            system.debug('Error in SPLT_OpportunitySplit.updateSplitsOnOpptyChange '+e);
            Logger.logMessages(new List<Error_Log__c> {new Error_Log__c(Class__c = 'SPLT_OpportunitySplit',
                        Method__c = 'updateSplitsOnOpptyChange',
                        Error__c = e.getMessage()+'\n'+e.getStackTraceString(),
                        Object__c = 'Opportunity',
                        ID_List__c = null,
                        Running_user__c = UserInfo.getUserId(),
                        Type__c = 'Error'
                    )});           
        }
    }
}