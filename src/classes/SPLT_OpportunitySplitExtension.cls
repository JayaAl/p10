/**
* This is an extension class for Opportunity Split object.
* This class handles all the operations required to display Opportunity split objects
* as a table for the user to perform CRUD operations.
* @Author: APprivo
*/
public class SPLT_OpportunitySplitExtension {
    
/* Values displayed within the Visualforce page */
    public List<OpportunitySplitWrapper> oSplitWrapperList {get;set;} // List of Opportunity_Split__c rows displayed.
    public Opportunity opportunity {get;set;} // Opportunity object that is associated with these split objects.
    public Integer maxRowCount {get { return 6; } private set;} // Maximum number of opportunity split objects [table rows]
    public Boolean noForecastAvailable {get; set;} // Set if there are no Opportunity Splits or Split Details to aggregate, used to display message in the VF page
    public String approvalId {get;set;} // Used in "View Existing Request" button on layout
    // Values for display within the output page Forecast section, set as Transient to reduce View State size
    public transient Map<String, Map<Date, Map<String, Decimal>>> mapResults{get;set;} // Map<Salesperson, Map<Date, Map<Offering_Type_Medium__c, Amount>>>
    public transient Map<String, Decimal> mapOverallTotal{get;set;} // Map<Offering_Type_Medium__c, Amount> to contain overall totals
    public transient List<String> listSPName{get;set;} // list of all returned SalesPerson Names for display
    public transient List<Date> listDates{get;set;} // list of all returned Dates for display
    public transient List<String> listColumn{get;set;} // list of all returned Offering_Type_Medium__c values for display

/* Internal values used in this Class.  Consider making these Private, or only instancing them as needed within Methods */
    public Map<Integer,OpportunitySplitWrapper> deleteOSplitMap {get;set;} // Map containing objects that need to be deleted on Save. Populated through the deleteRow method
    private Integer idIterator = 1; // Integer identifier for OpportunitySplitWrapper objects
    private List<Split_Changes__c> addApprovalList = new List<Split_Changes__c>(); // contains changes submitted for approval
    private Splits_Approval__c approvalRequest; // contains changes submitted for approval
    private Opportunity_Split__c tempOSplit; // Used to create placeholder Opportunity_Split__c on page load
    private String oppId; // Used to create placeholder Opportunity_Split__c on page load
    private Opportunity theOppty; // Opportunity
    public Opportunity_Split__c ops {get;set;}
    public List<SelectOption> roleOptions {get;set;}
    public String selectedRole
    {
       get
        {
           if(selectedRole!=null)
               selectedRole='Seller';
               
           return selectedRole;
        }
    set;
    }
    
    
    
    
    public List<SelectOption> getRoles() {
            List<SelectOption> options = new List<SelectOption>();
            options.add(new SelectOption('Seller','Seller'));
            options.add(new SelectOption('Network (Shadow)','Network (Shadow)'));
            options.add(new SelectOption('Performance (Shadow)','Performance (Shadow)'));
            options.add(new SelectOption('Multicultural (Shadow)','Multicultural (Shadow)'));
            options.add(new SelectOption('Programmatic (Shadow)','Programmatic (Shadow)'));
            options.add(new SelectOption('Healthcare (Shadow)','Healthcare (Shadow)'));
            options.add(new SelectOption('Casinos (Shadow)','Casinos (Shadow)'));
            options.add(new SelectOption('RBO (Shadow)','RBO (Shadow)'));
            options.add(new SelectOption('T3 (Shadow)','T3 (Shadow)'));
            return options;
    }

    

    public SPLT_OpportunitySplitExtension(ApexPages.StandardController controller){
        
        ops = new Opportunity_Split__c();        
        roleOptions = new List<SelectOption>();
        Schema.DescribeFieldResult roleFieldDescription = Opportunity_Split__c.Role_Type__c.getDescribe();

        // For each picklist value, create a new select option
        for (Schema.Picklistentry picklistEntry:roleFieldDescription.getPicklistValues()){

            roleOptions.add(new SelectOption(pickListEntry.getValue(),pickListEntry.getLabel()));

            // obtain and assign default value
            if (picklistEntry.defaultValue){
                ops.Role_Type__c = pickListEntry.getValue();
            }  
        }
        
        tempOSplit = (Opportunity_Split__c)controller.getRecord();
        if (tempOSplit.Id == null) {
            String key = getOppIdKey();
            if (key == null) {
                ApexPages.Message errorMessage = new ApexPages.Message(ApexPages.Severity.ERROR, 'Unable to get the Opportunity associated to these splits.');
                ApexPages.addMessage(errorMessage);
                return;
            }
            oppId = ApexPages.currentPage().getParameters().get(key);//CF00NT0000001BMl3
            tempOSplit = new Opportunity_Split__c(Opportunity__c = oppId);
        } else {
            // retrieving the opportunity associated with this split object
            tempOSplit = [
                SELECT Opportunity__c, Salesperson_Name__c,Opportunity__r.Probability 
                FROM Opportunity_Split__c 
                WHERE id = :tempOSplit.Id
            ];
        }

        this.opportunity = OppFromId(tempOSplit.Opportunity__c);
        this.oSplitWrapperList = new List<OpportunitySplitWrapper>();

        approvalId = '';
        if(this.opportunity.Splits_Locked__c) {
            approvalId = [
                SELECT Id 
                FROM Splits_Approval__c 
                WHERE Opportunity__c =: this.opportunity.Id 
                    AND Status__c = 'In Progress' 
                ORDER BY CreatedDate desc 
                LIMIT 1
            ].Id;
        }

        List<OpportunitySplitWrapper> first = new List<OpportunitySplitWrapper>();
        List<OpportunitySplitWrapper> rest = new List<OpportunitySplitWrapper>();
        
        // Populating the list to be displayed in the UI table. 
        for(Opportunity_Split__c oSplit:opportunity.Opportunity_Split__r){
            OpportunitySplitWrapper wrapper = new OpportunitySplitWrapper(this.idIterator++,oSplit);
            if(oSplit.Opportunity_Owner__c){
                first.add(wrapper);
            }else{
                rest.add(wrapper);
            }
        }
        oSplitWrapperList.addAll(first);
        oSplitWrapperList.addAll(rest);
        this.deleteOSplitMap = new Map<Integer,OpportunitySplitWrapper>();
        
        listSplitForecastLine(this.opportunity);

    }
    
    private String getOppIdKey() {
        for (String key : ApexPages.currentPage().getParameters().keySet()) {
            if (key.startsWith('CF') && key.endsWith('_lkid')) {
                return key;
            }
        }
        return null;
    }

    private Opportunity OppFromId(Id theId){
        Opportunity o = null;
        if(theId!=null){
            o = [
                SELECT Name,Amount,OwnerId,AccountId,Probability, CurrencyIsoCode, Splits_Locked__c, ContractStartDate__c, RecordType.DeveloperName,
                    (SELECT Opportunity__c,Name, Opportunity_Owner__c,Salesperson__c,Role_Type__c, Salesperson_Name__c, CurrencyIsoCode, Split__c,Probability__c 
                     FROM Opportunity_Split__r
                    ) 
                FROM Opportunity o 
                WHERE id = :theId
            ];
        }
        return o;
    }

/**
* This method gets called on Save.
*/
    public PageReference doSave(){ 
        // validating user input and generating an error message
        String errorMessageString = generateErrorMessage();
        
        // If the error message is not empty it is an indication that there is an error message
        if(!''.equals(errorMessageString)){
            ApexPages.Message errorMessage = new ApexPages.Message(ApexPages.Severity.ERROR, errorMessageString);
            ApexPages.addMessage(errorMessage);
            return null;
        }
        system.debug('START HERE CONTRACT START DATE' + this.opportunity.ContractStartDate__c);
        system.debug('START HERE PROBABILITY' +  this.opportunity.Probability);
        system.debug('START HERE CONDITIONS' + (this.opportunity.ContractStartDate__c < system.today() && this.opportunity.Probability == 100));
        if(this.opportunity.ContractStartDate__c < system.today() && this.opportunity.Probability == 100) {
            system.debug('START HERE SPLITS LOCKED' + this.Opportunity.Splits_Locked__c);
        
            if(!this.Opportunity.Splits_Locked__c) {
                User loggedInUser = [Select Id, Territory__c from User where Id =: UserInfo.getUserId()];
                Map<String, Splits_Approvers__c> listApprovers = Splits_Approvers__c.getAll();
                String approverId = '';
                if(loggedInUser.Territory__c == '' || loggedInUser.Territory__c == null || !listApprovers.containsKey(loggedInUser.Territory__c)) {
                    approverId = listApprovers.get('BLANK').Approver__c;
                } else {
                    approverId = listApprovers.get(loggedInUser.Territory__c).Approver__c;
                }
                approvalRequest = new Splits_Approval__c(Requested_By__c = loggedInUser.Id, Approver__c = approverId, Opportunity__c = this.opportunity.Id);
                updateSplitsApprovalOppFields();
                //insertDeleteObjectsForApproval();
                if(! this.oSplitWrapperList.isEmpty()) {
                    insertUpdatedOpportunityField();
                }
                
                insertDeleteObjectsForApproval();
                
                try {
                    if( !addApprovalList.isEmpty()) {
                        this.opportunity.Splits_Locked__c = true;
                        insert approvalRequest;
                        for(Split_Changes__c sc: addApprovalList) {
                            sc.Splits_Approval__c = approvalRequest.Id;
                        }
                        insert addApprovalList;
                        submitForApproval(approvalRequest);
                        update this.opportunity;
                    }
                } catch (Exception ex) {
                    ApexPages.Message errorMessage = new ApexPages.Message(ApexPages.Severity.ERROR, ex.getMessage());
                    ApexPages.addMessage(errorMessage);
                    return null;
                }
            }
        } else {
        
            system.debug('START HERE INSIDE ELSE' + this.Opportunity.Splits_Locked__c);
        
            // inserted new objects.
            Boolean successOp = insertNewObjects();
            if(!successOp){
                return null;
            }
            // deleting objects removed from the table.
            deleteObjects();
            
            //Updating legacy fields in Opportunity.
            updateOpportunityFields();
            
            try{
                // calculating the split detail objects for each of the opportunity splits.
                SPLT_ManageSplitDetails.evaluateSplitTypes(new Set<Id>{this.opportunity.Id});
            }
            Catch(exception ex)
            {
                system.debug('......................................Exception '+ ex);
                system.debug('......................................Exception '+ ex.getStackTraceString());
                    
            } 
            system.debug('......................................SPLIT EVAL SUCCESSFUL');
            
        }
        system.debug('......................................SPLIT EVAL SUCCESSFUL . '+ opportunity.id);
        
        
        // redirecting the user back to the page reference.
        return new PageReference('/'+opportunity.id);
    }

    
    public void insertUpdatedOpportunityField() {
        for(OpportunitySplitWrapper wrapper: this.oSplitWrapperList){
            Split_Changes__c splitChanges = new Split_Changes__c();
            if(wrapper.oSplit.Id == null){
                splitChanges.Operation__c = 'INSERT';
            } else {
                splitChanges.Opportunity_Split__c = wrapper.oSplit.Id;
                splitChanges.Operation__c = 'UPDATE';
            }
            if(wrapper.oSplit.Salesperson__c == this.opportunity.OwnerId){
                splitChanges.Opportunity_Owner__c = true;
            }
            splitChanges.CurrencyIsoCode = this.opportunity.CurrencyIsoCode;
            splitChanges.Salesperson__c = wrapper.oSplit.Salesperson__c;
            if(wrapper.oSplit.Role_Type__c!=null)
                splitChanges.Role_Type__c = wrapper.oSplit.Role_Type__c;
            else
                splitChanges.Role_Type__c = 'Seller';
           
            splitChanges.Split__c = wrapper.oSplit.Split__c;
            addApprovalList.add(splitChanges);          
        }
    }
    
    public void insertDeleteObjectsForApproval() {
        List<OpportunitySplitWrapper> wrapperList = this.deleteOSplitMap.values();
        for(OpportunitySplitWrapper wrapper: wrapperList){
            Split_Changes__c splitChanges = new Split_Changes__c();
            if(wrapper.oSplit.Id != null && !''.equals(wrapper.oSplit.Id)) {
                splitChanges.Opportunity_Split__c = wrapper.oSplit.Id;
                splitChanges.Operation__c = 'DELETE';
                addApprovalList.add(splitChanges); 
            }
        }
    }

    //Setting SP fields on Splits Approval by Lakshman on 04-09-2014
    private void updateSplitsApprovalOppFields() {
        
        if(this.oSplitWrapperList.size() == 0) return;
        approvalRequest.X1st_Salesperson_Split__c = this.oSplitWrapperList.get(0).oSplit.Split__c;
        Opportunity_Split__c tempSplit;
        approvalRequest.X2nd_Salesperson__c = null;
        approvalRequest.X2nd_Salesperson_Split__c = 0;
        approvalRequest.X3rd_Salesperson_Split__c = 0;
        approvalRequest.X3rd_Salesperson__c = null;
        approvalRequest.X4th_Salesperson_Split__c = 0;
        approvalRequest.X4th_Salesperson__c = null;                  
        if(this.oSplitWrapperList.size() > 1){
            tempSplit = this.oSplitWrapperList.get(1).oSplit;
            approvalRequest.X2nd_Salesperson__c = tempSplit.Salesperson__c;
            approvalRequest.X2nd_Salesperson_Split__c = tempSplit.Split__c;     
                        
        }
        if(this.oSplitWrapperList.size() > 2){
            tempSplit = this.oSplitWrapperList.get(2).oSplit;
            approvalRequest.X3rd_Salesperson_Split__c = tempSplit.Split__c;
            approvalRequest.X3rd_Salesperson__c = tempSplit.Salesperson__c;
        }
        if(this.oSplitWrapperList.size() > 3){
            tempSplit = this.oSplitWrapperList.get(3).oSplit;
            approvalRequest.X4th_Salesperson_Split__c = tempSplit.Split__c;
            approvalRequest.X4th_Salesperson__c = tempSplit.Salesperson__c;  
        }
        
        if(this.oSplitWrapperList.size() > 4){
            tempSplit = this.oSplitWrapperList.get(4).oSplit;
            approvalRequest.X5th_Salesperson_Split__c = tempSplit.Split__c;
            approvalRequest.X5th_Salesperson__c = tempSplit.Salesperson__c;
        }
        
        if(this.oSplitWrapperList.size() > 5){
            tempSplit = this.oSplitWrapperList.get(5).oSplit;
            approvalRequest.X6th_Salesperson_Split__c = tempSplit.Split__c;
            approvalRequest.X6th_Salesperson__c = tempSplit.Salesperson__c; 
        }
    }
    
    private void updateOpportunityFields(){
// TODO:
// Of the 4 people that we are splitting among, re-write this so that we place the highest % at the top, sorting by %


        if(this.oSplitWrapperList.size() == 0) return;
        this.opportunity.X1st_Salesperson_Split__c = this.oSplitWrapperList.get(0).oSplit.Split__c;
        Opportunity_Split__c tempSplit;
        this.opportunity.X2nd_Salesperson__c = null;
        this.opportunity.X2nd_Salesperson_Split__c = 0;
        this.opportunity.X3rd_Salesperson_Split__c = 0;
        this.opportunity.X3rd_Salesperson__c = null;
        this.opportunity.X4th_Salesperson_Split__c = 0;
        this.opportunity.X4th_Salesperson__c = null; 
        
        this.opportunity.X5th_Salesperson_Split__c = 0;
        this.opportunity.X5th_Salesperson__c = null; 
        
        this.opportunity.X6th_Salesperson_Split__c = 0;
        this.opportunity.X6th_Salesperson__c = null; 
                         
        if(this.oSplitWrapperList.size() > 1){
            tempSplit = this.oSplitWrapperList.get(1).oSplit;
            this.opportunity.X2nd_Salesperson__c = tempSplit.Salesperson__c;
            this.opportunity.X2nd_Salesperson_Split__c = tempSplit.Split__c;
            system.debug('......................................'+this.opportunity.X2nd_Salesperson_Split__c);            
        }
        if(this.oSplitWrapperList.size() > 2){
            tempSplit = this.oSplitWrapperList.get(2).oSplit;
            this.opportunity.X3rd_Salesperson_Split__c = tempSplit.Split__c;
            this.opportunity.X3rd_Salesperson__c = tempSplit.Salesperson__c;
             system.debug('......................................'+this.opportunity.X3rd_Salesperson__c);             
        }
        if(this.oSplitWrapperList.size() > 3){
            tempSplit = this.oSplitWrapperList.get(3).oSplit;
            this.opportunity.X4th_Salesperson_Split__c = tempSplit.Split__c;
            this.opportunity.X4th_Salesperson__c = tempSplit.Salesperson__c;
             system.debug('......................................'+this.opportunity.X4th_Salesperson__c);             
        }
        
        
        if(this.oSplitWrapperList.size() > 4){
            tempSplit = this.oSplitWrapperList.get(4).oSplit;
            this.opportunity.X5th_Salesperson_Split__c = tempSplit.Split__c;
            this.opportunity.X5th_Salesperson__c = tempSplit.Salesperson__c;
             system.debug('......................................'+this.opportunity.X5th_Salesperson__c);             
        }
        if(this.oSplitWrapperList.size() > 5){
            tempSplit = this.oSplitWrapperList.get(5).oSplit;
            this.opportunity.X6th_Salesperson_Split__c = tempSplit.Split__c;
            this.opportunity.X6th_Salesperson__c = tempSplit.Salesperson__c;
             system.debug('......................................'+this.opportunity.X6th_Salesperson__c);             
        }
        try
        {
            update this.opportunity;
        }
        Catch(exception ex)
        {
            system.debug('......................................Exception '+ ex);
            system.debug('......................................Exception '+ ex.getStackTraceString());
                
        }
        
    }
    
/**
* This method validates the user input and generates an appropriate error message.
*/
    private String generateErrorMessage(){
        Decimal splitTotal = 0;
        String errorMessageString = '';
        string roleType = 'Shadow';
        boolean roleError = false;
        for(Integer index=0;index < this.oSplitWrapperList.size(); index++){
            OpportunitySplitWrapper wrapper = this.oSplitWrapperList.get(index);
            
            splitTotal = splitTotal + wrapper.oSplit.Split__c;
            for(Integer subindex=index+1; subindex < this.oSplitWrapperList.size();subindex++){
                if(wrapper.oSplit.Salesperson__c == this.oSplitWrapperList.get(subindex).oSplit.Salesperson__c){
                    errorMessageString = 'Multiple splits cannot be made to the same Salesperson';
                    return errorMessageString; 
                }
            }
            
            
            if(wrapper.oSplit.Role_Type__c!=null && wrapper.oSplit.Role_Type__c.contains(roleType) && wrapper.oSplit.Split__c>0){
                errorMessageString = 'Split cannot be greater than 0 for Shadow Reps';
                return errorMessageString; 
            }
        }
        
        
        
        
        
        
        if(splitTotal != 100){  
            errorMessageString = 'The Split amount across all Opportunity Split objects must total to 100';
            return errorMessageString; 
        }  
        return '';
    }
    
    private Boolean insertNewObjects(){
        if(this.oSplitWrapperList.size() == 0) return true;
        List<Opportunity_Split__c> addList = new List<Opportunity_Split__c>();
        for(OpportunitySplitWrapper wrapper: this.oSplitWrapperList){
            if(wrapper.oSplit.Opportunity__c == null){
                wrapper.oSplit.Opportunity__c = this.opportunity.Id;
            }
            if(wrapper.oSplit.Salesperson__c == this.opportunity.OwnerId){
                wrapper.oSplit.Opportunity_Owner__c = true;
            }
            wrapper.oSplit.CurrencyIsoCode = this.opportunity.CurrencyIsoCode;//Updated Lakshman on 12-10-2013
            addList.add(wrapper.oSplit);          
        }
        try{
            upsert addList;
        }catch(System.DmlException exp){
            ApexPages.addMessages(exp);
            return false;
        }
        return true;
    }
    
    private List<Opportunity_Split__c> getDeleteList(){
        List<Opportunity_Split__c> deleteList = new List<Opportunity_Split__c>();
        List<OpportunitySplitWrapper> wrapperList = this.deleteOSplitMap.values();
        for(OpportunitySplitWrapper wrapper: wrapperList){
            if(wrapper.oSplit.Id != null && !''.equals(wrapper.oSplit.Id))
            deleteList.add(wrapper.oSplit);
        }
        return deleteList;  
    }
    
    private void deleteObjects(){
        try {
            delete getDeleteList();
        } catch(Exception ex) {
            ApexPages.Message errorMessage = new ApexPages.Message(ApexPages.Severity.ERROR, ex.getMessage());
            ApexPages.addMessage(errorMessage);
            return;     
        }
    }
    
/**
* On cancel, the user is redirected to the opportunity page.
*/
    public PageReference doCancel(){
        return new PageReference('/'+opportunity.id);
    }
    
/**
* This method updates the list with a new entry.
*/
    public PageReference addRow(){
    
        /*
        OpportunitySplitWrapper newOSplitWrapper = new OpportunitySplitWrapper( 
            this.idIterator++, 
            new Opportunity_Split__c(
                Opportunity_Owner__c=false, 
                Opportunity__c = opportunity.id,
                Role_Type__c = 'Seller'
            )
        );
        this.oSplitWrapperList.add(newOSplitWrapper);
        
        return null;
        */
        
        ops = new Opportunity_Split__c(Opportunity_Owner__c=false, 
                Opportunity__c = opportunity.id
                );
        OpportunitySplitWrapper newOSplitWrapper = new OpportunitySplitWrapper( 
            this.idIterator++, 
            ops
        );
        this.oSplitWrapperList.add(newOSplitWrapper);
        
        return null;
    }
    
/**
* This method deletes the record from the list based on the id parameter passed.
* The id used is a custom generated id.
*/
    public PageReference deleteRow(){
        String deleteIdString = ApexPages.currentPage().getParameters().get('deleteId');
        Integer deleteId = Integer.valueOf(deleteIdString);
        // Iterating over the list to identify the record to remove based on the id parameter.
        for(Integer index=0;index < this.oSplitWrapperList.size();index++){
            OpportunitySplitWrapper wrapper = this.oSplitWrapperList.get(index);
            if(wrapper.id == deleteId){
                this.oSplitWrapperList.remove(index);
                deleteOSplitMap.put(deleteId,wrapper);
                break;
            }
        }
        return null;
    }
    
// This method will submit the opportunity split automatically
    public void submitForApproval(Splits_Approval__c sa){
        // Create an approval request for the Account Ownership
        Approval.ProcessSubmitRequest req1 = new Approval.ProcessSubmitRequest();
        req1.setComments('Submitting request for approval');
        req1.setObjectId(sa.id);

        // Submit the approval request for the Account Ownership
        Approval.ProcessResult result = Approval.process(req1);

    }
    public PageReference deleteAcc(){
        Id profileId=userinfo.getProfileId();
        String profileName=[Select Id,Name from Profile where Id=:profileId].Name;
        system.debug('ProfileName'+profileName);
        pageReference pg;
        if(profileName=='System Administrator'){
            ID deleteVal = ApexPages.currentPage().getParameters().get('delID');
            //id oppIdValue =tempOSplit.Opportunity__c;
            String IsdVal = ApexPages.currentPage().getParameters().get('retURL');
            system.debug('====='+deleteVal);
            if(deleteVal!=null){
                // Opportunity_Split__c tempOSplit = (Opportunity_Split__c)controller.getRecord();
                delete tempOSplit ;
                pg=new pageReference('/'+IsdVal);
                pg.setRedirect(true);
            }
        }
        return pg;   
    }
    
// Wrapper class for Opportunity Split object.
   public class OpportunitySplitWrapper {
        public Integer id {get;set;}
        public Opportunity_Split__c oSplit {get;set;}
        public OpportunitySplitWrapper(Integer id,Opportunity_Split__c oSplit){
            this.id = id;
            this.oSplit = oSplit;
        }
    }

/*  Re-writing query and methods to build a more-efficient and straightforward collection
    End result will be similar to Map<Salesperson, Map<Date, Map<Column, Amount>>>
*/

    
    public Set<String> setTaxonomyRTs{
        get{
            if(setTaxonomyRTs == null||setTaxonomyRTs.isEmpty()){
                setTaxonomyRTs = new Set<String>();
                for(TaxonomyRecordTypes__c t:[Select Name from TaxonomyRecordTypes__c]){
                    setTaxonomyRTs.add(t.Name);
                }
            }
            return setTaxonomyRTs;
        }
        set;
    }
    public Boolean isTaxonomyRT{
        get{
            return setTaxonomyRTs.contains(opportunity.RecordType.DeveloperName);
        }
        set;
    }

    /* methods to populate Salesperson, Date, Column, and aggregate result collections */
    public pagereference updateForecastTable(){ // pagereference used to re-generate teh forecast table
        listSplitForecastLine(OppId);
        return null;
    }
    public void listSplitForecastLine(Id OppId){
        Opportunity o = OppFromId(OppId);
        listSplitForecastLine(o);
    }
    public void listSplitForecastLine(Opportunity o){

        List<AggregateResult> listAR = new List<AggregateResult>();

        if(setTaxonomyRTs.contains(o.RecordType.DeveloperName)){
            listAR= [
                SELECT Salesperson_User__r.Name Salesperson, Date__c Date, Offering_Type_Medium__c Column, sum(Amount__c) Amount
                FROM Split_Detail__c 
                WHERE Opportunity_Split__r.Opportunity__c  = :o.Id
                GROUP BY Salesperson_User__r.Name, Date__c, Offering_Type_Medium__c
                ORDER BY Salesperson_User__r.Name, Date__c, Offering_Type_Medium__c
            ];
        } else {
            listAR= [
                SELECT Salesperson_User__r.Name Salesperson, Date__c Date, Name Column, sum(Amount__c) Amount
                FROM Split_Detail__c 
                WHERE Opportunity_Split__r.Opportunity__c  = :o.Id
                GROUP BY Salesperson_User__r.Name, Date__c, Name
                ORDER BY Salesperson_User__r.Name, Date__c, Name
            ];
        }
        
        
        if(listAR.isEmpty()){
            noForecastAvailable = true; // No lines returned, set flag to notify the VF page
        } else {
            Set<String> setSPName = new Set<String>(); // Set containing all queried SalesPerson Names
            Set<Date> setDates = new Set<Date>(); // Set containing all queried Dates
            Set<String> setColumn = new Set<String>(); // Set containing all queried 'Column' values
            for(AggregateResult ar:listAR){ // populate those Sets
                setSPName.add((String)ar.get('Salesperson'));
                setDates.add((Date)ar.get('Date'));
                setColumn.add((String)ar.get('Column'));
            }

            // Set up final collections to be populated
            Date userTotalDate = Date.newInstance(2099, 1, 1);
            Boolean includeNoneColumn = false;
            mapResults = new Map<String, Map<Date, Map<String, Decimal>>>();
            mapOverallTotal = new Map<String, Decimal>(); // Overall Total

            // Loop through all collected values, creating the framework of values to be updated with actual values
            for(String sp:setSPName){
                mapResults.put(sp,new Map<Date, Map<String, Decimal>>());
                for(Date linedate:setDates){
                    mapResults.get(sp).put(linedate, new Map<String, Decimal>());
                    for(String column:setColumn){
                        if(column==null||column.trim()==''){ // if Offering_Type_Medium__c is empty they use '[None]' instead.
                            column = '[None]';
                            includeNoneColumn = true;
                        }
                        mapResults.get(sp).get(linedate).put(column,0.00);
                    }
                }
            }

            // Populate our data framework with the details returned in the query
            for(AggregateResult ar:listAR){
                String sp = (String)ar.get('Salesperson');
                Date linedate = (Date)ar.get('Date');
                String column = (String)ar.get('Column');
                Decimal amount = (Decimal)ar.get('Amount');
                column = (column==null||column.trim()=='')?'[None]':column; // if Offering_Type_Medium__c is empty they use '[None]' instead.
                mapResults.get(sp).get(linedate).put(column,amount);                
            }
            
            // Loop through our collections again, this time populating additional aggregate values as we go for line totals, user totals, and overall totals
            for(String sp:setSPName){
                mapResults.get(sp).put(userTotalDate,new Map<String, Decimal>()); // Create totals for each user
                for(Date linedate:setDates){
                    Decimal lineTotal = 0.00; // Create individual line totals for each user/date
                    for(String column:setColumn){
                        column = (column==null||column.trim()=='')?'[None]':column; // if Offering_Type_Medium__c is empty they use '[None]' instead.
                        lineTotal += mapResults.get(sp).get(linedate).get(column); // aggregate the line total
                        
                        if(!mapResults.get(sp).get(userTotalDate).containsKey(column)){ // Create additional line for all Dates for this user
                            mapResults.get(sp).get(userTotalDate).put(column,0.00);
                        }
                        mapResults.get(sp).get(userTotalDate).put(column, mapResults.get(sp).get(linedate).get(column) + mapResults.get(sp).get(userTotalDate).get(column)); // populate additional line for all Dates for this user
                        
                        if(!mapOverallTotal.containsKey(column)){ // Create additional aggregation for all lines with this Offering_Type_Medium__c value
                            mapOverallTotal.put(column,0.00);
                        }
                        mapOverallTotal.put(column, mapResults.get(sp).get(linedate).get(column) + mapOverallTotal.get(column)); // populate additional aggregation for all lines with this Offering_Type_Medium__c value
                    }
                    mapResults.get(sp).get(linedate).put('Total',lineTotal); // Create and populate 'Total' column value for this user/date
                    
                    if(!mapResults.get(sp).get(userTotalDate).containsKey('Total')){ // Create total value for this user aggregate line
                        mapResults.get(sp).get(userTotalDate).put('Total',0.00);
                    }
                    mapResults.get(sp).get(userTotalDate).put('Total', mapResults.get(sp).get(linedate).get('Total') + mapResults.get(sp).get(userTotalDate).get('Total')); // populate total value for this user aggregate line
                    
                    if(!mapOverallTotal.containsKey('Total')){ // Create overall total for all users
                        mapOverallTotal.put('Total',0.00);
                    }
                    mapOverallTotal.put('Total', mapResults.get(sp).get(linedate).get('Total') + mapOverallTotal.get('Total')); // populate overall total for all users
                }
            }

            // up until now the collections can be returned in any order, now we need to convert our Sets to Lists, sort them, and add any additional groupings needed for final display
            listSPName = new List<String>(setSPName);
            listSPName.sort();

            listDates = new List<Date>(setDates);
            listDates.sort();
            listDates.add(userTotalDate); // Add "1/1/2099" row to the listDates so it appears at the end

            listColumn = new List<String>(setColumn);
            listColumn.sort();
            if(includeNoneColumn){listColumn.add('[None]');} // Add '[None]' row to the listColumn if needed
            listColumn.add('Total'); // Add "Total" column to the listColumn so it appears at the end
        }
    }
}