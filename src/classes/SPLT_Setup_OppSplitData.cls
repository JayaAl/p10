/***********************************************************************
*                                                                      *
*      End User Company    :         Pandora                           *
*      Client Company      :         Clear Task Inc                    *
*      Author              :         Abhishek Singh                    *
*      Class Name          :         UpdateOpportunitySponserForSplit  *
*                                                                      *
************************************************************************/
public with sharing class SPLT_Setup_OppSplitData {
  /*
   * updateSponserNameSplit method used to update the salesperson in sequence order
   */  
    public static void updateSponserNameSplit(List<Opportunity> listNewOpportunity){
        System.debug('########listNewOpportunity='+listNewOpportunity);
        // list for delete Opportunity Split
        List<Opportunity_Split__c> deleteListOpportunitySplit = [select opportunity__c, Salesperson__c, Salesperson_Name__c, Split__c from Opportunity_Split__c where Opportunity__c in :listNewOpportunity];//new List<Opportunity_Split__c>(); 
        System.debug('########listOpportunitySplit='+deleteListOpportunitySplit); 
     /*   Map<Id, List<Opportunity_Split__c>> oppSplitMap = new Map<Id, List<Opportunity_Split__c>>();
            
        for(Opportunity_Split__c split : [select opportunity__c, Salesperson__c, Salesperson_Name__c, Split__c from Opportunity_Split__c where Opportunity__c in :listNewOpportunity]){
            List<Opportunity_Split__c> oppSplitList = new List<Opportunity_Split__c>();
            if(oppSplitMap.containskey(split.Opportunity__c)){
                oppSplitList = oppSplitMap.get(split.Opportunity__c);
            }
            oppSplitList.add(split);
            oppSplitMap.put(split.Opportunity__c,oppSplitList);
        }*/
        // list for insert Opportunity Split
        List<Opportunity_Split__c> insertOpportunitySplit=new List<Opportunity_Split__c>();
        for(Opportunity o:listNewOpportunity){ 
            /*if(oppSplitMap.containsKey(o.Id)){
                List<Opportunity_Split__c> deleteList = oppSplitMap.get(o.Id) ;
                deleteListOpportunitySplit.addAll(deleteList);
            }*/
            // check for 1st 2nd 3rd are empty or any one is not          
            if((isNotNullOrZero(o.X2nd_Salesperson_Split__c)) || (isNotNullOrZero(o.X3rd_Salesperson_Split__c)) || (isNotNullOrZero(o.X4th_Salesperson_Split__c)) ){
                      System.debug('########In IFFFF#########');
                      Opportunity_Split__c recordOwnerOppSplit=new Opportunity_Split__c(Opportunity__c=o.id, Salesperson__c=o.ownerid,Split__c=o.X1st_Salesperson_Split__c, opportunity_owner__c = true);
                      insertOpportunitySplit.add(recordOwnerOppSplit);
                      if(isNotNullOrEmpty(o.X2nd_Salesperson__c) && isNotNullOrZero(o.X2nd_Salesperson_Split__c)){
                          Opportunity_Split__c recordOppSplit=new Opportunity_Split__c(Opportunity__c=o.id, Salesperson__c=o.X2nd_Salesperson__c,Split__c=o.X2nd_Salesperson_Split__c);
                          insertOpportunitySplit.add(recordOppSplit);
                      }
                      if(isNotNullOrEmpty(o.X3rd_Salesperson__c) && isNotNullOrZero(o.X3rd_Salesperson_Split__c)){
                          Opportunity_Split__c recordOppSplit=new Opportunity_Split__c(Opportunity__c=o.id, Salesperson__c=o.X3rd_Salesperson__c,Split__c=o.X3rd_Salesperson_Split__c);
                          insertOpportunitySplit.add(recordOppSplit);
                      }
                      if(isNotNullOrEmpty(o.X4th_Salesperson__c) && isNotNullOrZero(o.X4th_Salesperson_Split__c)){
                          Opportunity_Split__c recordOppSplit=new Opportunity_Split__c(Opportunity__c=o.id, Salesperson__c=o.X4th_Salesperson__c,Split__c=o.X4th_Salesperson_Split__c);
                          insertOpportunitySplit.add(recordOppSplit);
                      }
                      if(isNotNullOrEmpty(o.X5th_Salesperson__c) && isNotNullOrZero(o.X5th_Salesperson_Split__c)){
                          Opportunity_Split__c recordOppSplit=new Opportunity_Split__c(Opportunity__c=o.id, Salesperson__c=o.X5th_Salesperson__c,Split__c=o.X5th_Salesperson_Split__c);
                          insertOpportunitySplit.add(recordOppSplit);
                      }
                      if(isNotNullOrEmpty(o.X6th_Salesperson__c) && isNotNullOrZero(o.X6th_Salesperson_Split__c)){
                          Opportunity_Split__c recordOppSplit=new Opportunity_Split__c(Opportunity__c=o.id, Salesperson__c=o.X6th_Salesperson__c,Split__c=o.X6th_Salesperson_Split__c);
                          insertOpportunitySplit.add(recordOppSplit);
                      }
            }else{
                 System.debug('########In ELSEEE#########');
                 Opportunity_Split__c recordOwnerOppSplit=new Opportunity_Split__c(Opportunity__c=o.id, Salesperson__c=o.ownerid,Split__c=100, opportunity_owner__c = true);
                 insertOpportunitySplit.add(recordOwnerOppSplit);
            }      
        }
        try{
            // delete the record from OpportunitySplit
            delete deleteListOpportunitySplit;            
            // insert the record from OpportunitySplit
            insert insertOpportunitySplit;            
        }catch(Exception e){
        }   
    }
  /*
   * isNotNullOrEmpty method used to check for input in null or empty
   */     
    public static boolean isNotNullOrEmpty(String temp){        
        if(temp==null  || temp.trim()=='' ){
            return false;
        }else{
            return true;
        }
    }
  /*
   * isNotNullOrEmpty method used to check for input in null or empty
   */     
    public static boolean isNotNullOrZero(decimal temp){
        System.debug('########isNotNullOrZero# temp='+temp);
        if(temp==null  || temp==0|| temp==0.0){
            return false;
        }else{
            return true;
        }
    }

}