public class SPLT_SplitForecastWrapper {
    public String name {get; set;}
    public String theDate {get; set;}
    public List<Decimal>productTotals {get; set;}
    public Decimal total {get; set;}
    public Set<String>prodNames {get; set;}
    
    public SPLT_SplitForecastWrapper(String n, String d, List<Decimal> pT, Set<String>pNames) {
        name = n;
        theDate = d;
        productTotals = pT;
        total = 0;
        for (Decimal cur : pT) {
            total += cur;
        }
        prodNames = pNames;
    }
    
    public SPLT_SplitForecastWrapper(String n, List<Decimal> pT) {
        name = '';
        theDate = 'Sub Total';
        productTotals = pT;
        total = 0;
        for (Decimal cur : pT) {
            total += cur;
        }
        
    }
}