@isTest
private with sharing class SPLT_Test_DataSetup {
  static testMethod void updateOpportunitySplitAfterSave() {
    System.Test.startTest();
      Account acc = new  Account(Name ='test Account',Type='Advertiser');
        insert(acc);
    Opportunity o = new Opportunity(AccountId=acc.id, Name = 'Test Opportunity', StageName = 'Negotiation', CloseDate = System.Today(),Confirm_direct_relationship__c=true,Industry_Sub_Category__c='Political Campaign Marketing',Type='Advertiser');
    insert o;
    List<User> listUser=[select id, username from user];
    Opportunity testUpdate = [select Id,  X1st_Salesperson_Split__c, X2nd_Salesperson_Split__c ,X2nd_Salesperson__c, X3rd_Salesperson_Split__c ,X3rd_Salesperson__c, X4th_Salesperson_Split__c ,X4th_Salesperson__c FROM Opportunity WHERE id =:o.id];
    testUpdate.X1st_Salesperson_Split__c=50;
    testUpdate.X2nd_Salesperson__c=listUser[0].id;
    testUpdate.X2nd_Salesperson_Split__c=30;
    testUpdate.X3rd_Salesperson__c=listUser[1].id;
    testUpdate.X3rd_Salesperson_Split__c=20;
    testUpdate.X4th_Salesperson__c=listUser[2].id;
    testUpdate.X4th_Salesperson_Split__c=10;
    update testUpdate;  
    System.Test.stoptest();  
  }


}