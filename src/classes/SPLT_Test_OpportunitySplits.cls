@isTest
private class SPLT_Test_OpportunitySplits {
    public static testMethod void testOpportunitySplit(){
       
        OpportunityStage oStage = 
            [Select o.MasterLabel, o.IsWon, o.IsClosed From OpportunityStage o limit 1];
        if(oStage == null) return;
            
        Opportunity opp = new Opportunity(Name='Test',StageName=oStage.MasterLabel,CloseDate=Date.today());
        insert opp;
        
        List<Opportunity_Split__c> split = 
            [Select Id from Opportunity_Split__c where Opportunity__c = :opp.Id];
        System.assertNotEquals(null, split);
        
        User user = [Select Id from User where IsActive=true limit 1];
        opp.OwnerId = user.Id;
        update opp; 
    }
    
    public static testMethod void testOpportunitySplitCurrencyChange(){
      

        OpportunityStage oStage = 
            [Select o.MasterLabel, o.IsWon, o.IsClosed From OpportunityStage o limit 1];
        if(oStage == null) return;
            
        Opportunity opp = new Opportunity(Name='Test',StageName=oStage.MasterLabel,CloseDate=Date.today(),CurrencyIsoCode='USD');
        insert opp;
        
        List<Opportunity_Split__c> split = 
            [Select CurrencyIsoCode from Opportunity_Split__c where Opportunity__c = :opp.Id];

        //System.assertEquals(split.CurrencyIsoCode, opp.CurrencyIsoCode);
        
        
        opp.CurrencyIsoCode = 'AUD';
        update opp; 
    }
    
    public static testMethod void testOpportunitySplitExtension(){
                

        OpportunityStage oStage = 
            [Select o.MasterLabel, o.IsWon, o.IsClosed From OpportunityStage o limit 1];
        if(oStage == null) return;
            
        Pricebook2 pb = [Select Id, CurrencyIsoCode from Pricebook2 where isActive = true Limit 1];
        
        Opportunity opp = new Opportunity(Name='Test',StageName=oStage.MasterLabel,CloseDate=Date.today(), Pricebook2Id = pb.Id, CurrencyIsoCode = pb.CurrencyIsoCode);
        insert opp;
       
        PricebookEntry pbe = [Select Id from PricebookEntry where Pricebook2Id = :pb.Id and IsActive=true AND CurrencyIsoCode = :pb.CurrencyIsoCode limit 1];
        OpportunityLineItem pli = new OpportunityLineItem(OpportunityId = opp.Id, PricebookEntryId = pbe.Id, Quantity = 1, 
            ServiceDate = System.Today(), End_Date__c = System.Today().addDays(100), UnitPrice = 100);
        insert pli;
              
        TEst.startTest();

        Opportunity_Split__c split = 
            [Select Id from Opportunity_Split__c where Opportunity__c = :opp.Id limit 1];
        
        SPLT_OpportunitySplitExtension extension = new SPLT_OpportunitySplitExtension(new ApexPages.StandardController(split));
        System.assertEquals(opp.id ,extension.opportunity.id);
            extension.doSave();
        Test.stopTest();
        opp = [Select OwnerId,X1st_Salesperson_Split__c from Opportunity where id = :opp.Id];
        System.assertEquals(100,opp.X1st_Salesperson_Split__c);
        
        Integer listSize = extension.oSplitWrapperList.size();
        extension.addRow();
        System.assertEquals(listSize+1,extension.oSplitWrapperList.size());
        
        split = new Opportunity_Split__c();
        split.Split__c = 50;
        split.Salesperson__c = opp.OwnerId;
        split.Opportunity__c = opp.Id;          
        insert split;
        
        extension = new SPLT_OpportunitySplitExtension(new ApexPages.StandardController(split));
        extension.doSave();
        extension.doCancel();
        
        
        Integer count = extension.maxRowCount;
    }
    
    /*
     * @author: Lakshman(sfdcace@gmail.com)
     * @desc: Test method to cover code for Insert/Update/Delete operation of Splits
     * @date: @date: 03-07-2014
     */
    
    public static testMethod void testOpportunitySplitExtensionWithApproval(){
        

        OpportunityStage oStage = 
            [Select o.MasterLabel, o.IsWon, o.IsClosed From OpportunityStage o where IsWon = true limit 1];
        if(oStage == null) return;
        
          
        Pricebook2 pb = [Select Id, CurrencyIsoCode from Pricebook2 where isActive = true Limit 1];
        
        Opportunity opp = new Opportunity(Name='Test',StageName= 'Verbal Agreement',CloseDate=Date.today() - 2, Pricebook2Id = pb.Id, Probability = 100, CurrencyIsoCode = pb.CurrencyIsoCode, Bill_on_Broadcast_Calendar2__c = 'Yes', Lead_Campaign_Manager__c = UserInfo.getUserId());
        insert opp;
        

        system.debug('Close Date of Opportunity-----------' + [Select CloseDate, Probability from Opportunity where Id =: opp.Id]);
        PricebookEntry pbe = [Select Id from PricebookEntry where Pricebook2Id = :pb.Id and IsActive=true AND CurrencyIsoCode = :pb.CurrencyIsoCode limit 1];
        OpportunityLineItem pli = new OpportunityLineItem(OpportunityId = opp.Id, PricebookEntryId = pbe.Id, Quantity = 1, 
            ServiceDate = Date.Today() - 1, End_Date__c = Date.Today(), UnitPrice = 100);
        insert pli;
        
        
        
        
        //Logic to check approval for Insert/Update operations
        // TODO: We need to break out eachof the below tests into their own test classes instead of creating one monolithic test as it is
        // test 1
        
        TEst.startTest();
        Opportunity_Split__c split = 
            [Select Id from Opportunity_Split__c where Opportunity__c = :opp.Id limit 10];
        
        
        
        SPLT_OpportunitySplitExtension extension = new SPLT_OpportunitySplitExtension(new ApexPages.StandardController(split));
        System.assertEquals(opp.id ,extension.opportunity.id);
        
        extension.addRow();
        split = new Opportunity_Split__c();
        split.Split__c = 50;
        split.Salesperson__c = opp.OwnerId;
        split.Opportunity__c = opp.Id; 
        
        extension.oSplitWrapperList[1].oSplit = split;
        extension.oSplitWrapperList[0].oSplit.Split__c = 50;
        // test 2
        extension.doSave();
        Test.stopTest();
        Splits_Approval__c sa = [Select Id,Status__c from Splits_Approval__c where Opportunity__c =: opp.Id];
        List<ProcessInstance> processInstances = [select Id, Status from ProcessInstance where TargetObjectId = :sa.id];
        System.assertEquals(processInstances.size(),1);
        sa.Status__c = 'Approved';
        // test 3
        update sa;
        sa.Status__c = 'Rejected';
        // test 4
        update sa;
        
        //Logic to check approval for delete operation
        ApexPages.currentPage().getParameters().put('deleteId','1');
        // test 5
        extension.deleteRow();
        
        extension.oSplitWrapperList[0].oSplit.Split__c = 100;
        // test 6
        extension.doSave();
        
        
    }
    
    /*
     * @author: Lakshman(sfdcace@gmail.com)
     * @desc: Test method to cover code for Owner Change operation of Splits
     * @date: @date: 03-07-2014
     */
    public static testMethod void testOpportunitySplitApprovalOwnerChange(){
        

        User u = UTIL_TestUtil.createUser();
        
        system.runAs(new User(Id=UserInfo.getUserId())) {
        Test.startTest();
        OpportunityStage oStage = 
            [Select o.MasterLabel, o.IsWon, o.IsClosed From OpportunityStage o where IsWon = true limit 1];
        if(oStage == null) return;
        
          
        Pricebook2 pb = [Select Id, CurrencyIsoCode from Pricebook2 where isActive = true Limit 1];
        
        Opportunity opp = new Opportunity(Name='Test',StageName= 'Verbal Agreement',CloseDate=Date.today() - 2, Pricebook2Id = pb.Id, Probability = 100, CurrencyIsoCode = pb.CurrencyIsoCode, Bill_on_Broadcast_Calendar2__c = 'Yes', Lead_Campaign_Manager__c = u.Id);
        insert opp;
        system.debug('Close Date of Opportunity-----------' + [Select CloseDate, Probability from Opportunity where Id =: opp.Id]);
        PricebookEntry pbe = [Select Id from PricebookEntry where Pricebook2Id = :pb.Id and IsActive=true AND CurrencyIsoCode = :pb.CurrencyIsoCode limit 1];
        OpportunityLineItem pli = new OpportunityLineItem(OpportunityId = opp.Id, PricebookEntryId = pbe.Id, Quantity = 1, 
            ServiceDate = Date.Today() - 1, End_Date__c = Date.Today(), UnitPrice = 100);
        insert pli;
        
        Test.stopTest();
            
        Opportunity_Split__c split = 
            [Select Id from Opportunity_Split__c where Opportunity__c = :opp.Id];
        
        opp.OwnerId = u.Id;
        update opp;
        Splits_Approval__c sa = [Select Id,Status__c from Splits_Approval__c where Opportunity__c =: opp.Id];
        List<ProcessInstance> processInstances = [select Id, Status from ProcessInstance where TargetObjectId = :sa.id];
        System.assertEquals(processInstances.size(),1);
        sa.Status__c = 'Approved';
        update sa;
        //Added by Lakshman to Test inactive owner ticket - ESS-13188 
        
            u.isActive = false;
            opp.OwnerId = UserInfo.getUserId();
            update opp;
        }
        
        
    }
    
    public static testMethod void testAssignSalesPersons() {
        

        Account a = new Account(Name = 'Test Account', Type='Advertiser');
        insert a;
        Opportunity o = new Opportunity(Name = 'Test Opportunity', AccountId = a.Id, StageName = 'Negotiation', CloseDate = System.Today());
        insert o;
        Opportunity test = [SELECT Id, OwnerId, X1st_Salesperson__c FROM Opportunity WHERE Id = :o.id];
        System.Debug('Opportunity: ' + test);
        System.AssertEquals(test.OwnerId, test.X1st_Salesperson__c);
    }
    
    public static testMethod void testEvaluateSplitDetails() {
        

        Account a = new Account(Name = 'Test Account', Type='Advertiser');
        insert a;
        Pricebook2 pb = [Select Id, CurrencyIsoCode  from Pricebook2 where isActive = true Limit 1];
        Opportunity o = new Opportunity(Name = 'Test Opportunity', AccountId = a.Id, 
            StageName = 'Negotiation', CloseDate = System.Today(), Pricebook2Id = pb.Id, CurrencyIsoCode = pb.CurrencyIsoCode );
        insert o;
        
        PricebookEntry pbe = [Select Id from PricebookEntry where Pricebook2Id = :pb.Id and IsActive=true AND CurrencyIsoCode = :pb.CurrencyIsoCode limit 1];
        OpportunityLineItem pli = new OpportunityLineItem(OpportunityId = o.Id, PricebookEntryId = pbe.Id, Quantity = 1, End_Date__c = System.Today().addDays(100), UnitPrice = 100);
        // TODO: We need to break out eachof the below tests into their own test classes instead of creating one monolithic test as it is
        // test 1
        insert pli;
        // test 2
        update pli;
        // test 3
        delete pli;
    }
    
}