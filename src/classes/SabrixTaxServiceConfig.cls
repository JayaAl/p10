public class SabrixTaxServiceConfig {
	private String user;
	private String pass;
	private String company_id;
	private String product_code;
	private String partnerName = 'PandoraOne';
	private String partnerNumber = '111';
	private String country = 'US';    
	private String amountType = 'GrossAmountBased';
	private String companyRole = 'Seller';
	private String currencyCode = 'USD';
	private String documentType = 'SalesInvoice';

	private String endPoint;

	private static SabrixTaxServiceConfig config;
	
	private SabrixTaxServiceConfig()
	{
		SabrixTaxInfo__c ST = SabrixTaxInfo__c.getOrgDefaults();
		endPoint = ST.endpoint__c;
		user = ST.username__c;
		pass = EncodingUtil.base64Decode(ST.password__c).toString();
		company_id = ST.company_id__c;
		product_code = ST.product_code__c;
	}
	
	public String getDocumentType()
	{
		return documentType;
	}
	
	public String getCurrencyCode()
	{
		return currencyCode;
	}
	
	public String getCompanyRole()
	{
		return companyRole;
	}
	
	public String getAmountType()
	{
		return amountType;
	}
	
	public String getCountry()
	{
		return country;
	}
	
	public String getPartnerNumber()
	{
		return partnerNumber;
	}
	
	public String getPartnerName()
	{
		return partnerName;
	}
	
	public static SabrixTaxServiceConfig getConfig()
	{
		if(config == null)
			config = new SabrixTaxServiceConfig();
		return config;
	}
	
	public String getEndPoint()
	{
		return endPoint;
	}
	
	public String getUsername()
	{
		return user;
	}
	
	public String getPassword()
	{
		return pass;
	}
	
	public String getCompanyId()
	{
		return company_id;
	}
	
	public String getProductCode()
	{
		return product_code;
	}	
}