/*
    *  Code Created by Lakshman(sfdcace@gmail.com) to Post Credit Notes in Batch
    */ 
global class SalesCreditNoteBulkPost implements Database.Batchable<SObject>, Database.Stateful {
    private final List<ID> m_ids;
    public Boolean isFromCreditNoteVF;
    public SalesCreditNoteBulkPost(List<ID> ids)
    {
        m_ids = ids;
        isFromCreditNoteVF = false;     
    }
    
    global Database.QueryLocator start(Database.BatchableContext ctx)
    {
        // Select either all In Progress Purchase CreditNotes or those specified 
        if(m_ids==null)
            return Database.getQueryLocator([select ID from c2g__codaCreditNote__c where c2g__CreditNoteStatus__c = 'In Progress']);
        else
            return Database.getQueryLocator([select ID from c2g__codaCreditNote__c where id in :m_ids]);
    }

    global void execute(Database.BatchableContext ctx, List<SObject> records)
    {
        // Build list of Purchase CreditNote API references
        List<c2g.CODAAPICommon.Reference> refs = new List<c2g.CODAAPICommon.Reference>();
        for(SObject sobj : records)
        { 
            c2g.CODAAPICommon.Reference ref = new c2g.CODAAPICommon.Reference();
            ref.Id = sobj.id;
            refs.add(ref);
        }
        if(!Test.isRunningTest())
        c2g.CODAAPISalesCreditNote_7_0.BulkPostCreditNote(null, refs); 
    }
    
    
    global void finish(Database.BatchableContext ctx)
    {
        BulkUpdatePostCreditNotes__c configEntry =BulkUpdatePostCreditNotes__c.getOrgDefaults();
        if(configEntry.BatchInProgress__c) {
            configEntry.BatchInProgress__c = false;
            update configEntry;
            if(isFromCreditNoteVF != null && isFromCreditNoteVF) {
                AsyncApexJob a = [Select Id, Status, NumberOfErrors, JobItemsProcessed, CreatedDate, CompletedDate, 
                TotalJobItems, CreatedBy.Email, CreatedById, CreatedBy.Name
                from AsyncApexJob where Id =:ctx.getJobId()];
                
                // Send an email to the Apex job's submitter notifying of job completion.  
                Messaging.SingleEmailMessage mail = new Messaging.SingleEmailMessage();
                String[] toAddresses = new String[] {a.CreatedBy.Email};
                mail.setToAddresses(toAddresses);
                mail.setSubject('CreditNote Batch Post ' + a.Status);
                String body = 'Hi, <br/>CreditNote Batch Post is ' + a.Status;
                body += '<br/>The batch Apex job was created by '+a.CreatedBy.Name+' ('+a.CreatedBy.Email+') processed '+a.TotalJobItems+' batches with '+a.NumberOfErrors+' failures. The process began at '+a.CreatedDate+' and finished at '+a.CompletedDate+'.';
                body += '<br/>Job Id ==>' + a.Id;
                mail.setHtmlBody(body);
                
                Messaging.sendEmail(new Messaging.SingleEmailMessage[] { mail });
            }
        }
    }   
}