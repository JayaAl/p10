/**
 * Copyright (c) 2009, FinancialForce.com, inc
 * All rights reserved.
 * 
 * Redistribution and use in source and binary forms, with or without modification, 
 *   are permitted provided that the following conditions are met:
 *
 * - Redistributions of source code must retain the above copyright notice, 
 *      this list of conditions and the following disclaimer.
 * - Redistributions in binary form must reproduce the above copyright notice, 
 *      this list of conditions and the following disclaimer in the documentation 
 *      and/or other materials provided with the distribution.
 * - Neither the name of the FinancialForce.com, inc nor the names of its contributors 
 *      may be used to endorse or promote products derived from this software without 
 *      specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND 
 *  ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES 
 *  OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL 
 *  THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, 
 *  EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS
 *  OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY
 *  OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 *  ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
**/

/**
 * This example shows how to implement a basic Batch Apex class around the FinancialForce.com BulkPostPurchaseInvoice API
 **/
global class SalesInvoiceBulkPost implements Database.Batchable<SObject>, Database.Stateful {
    private final List<ID> m_ids;
    public Boolean isFromInvoiceVF;
    public SalesInvoiceBulkPost(List<ID> ids)
    {
        m_ids = ids;
        isFromInvoiceVF = false;     
    }
    
    global Database.QueryLocator start(Database.BatchableContext ctx)
    {
        // Select either all In Progress Purchase Invoices or those specified 
        if(m_ids==null)
            return Database.getQueryLocator([select ID from c2g__codaInvoice__c where c2g__InvoiceStatus__c = 'In Progress']);
        else
            return Database.getQueryLocator([select ID from c2g__codaInvoice__c where id in :m_ids]);
    }

    global void execute(Database.BatchableContext ctx, List<SObject> records)
    {
        // Build list of Purchase Invoice API references
        List<c2g.CODAAPICommon.Reference> refs = new List<c2g.CODAAPICommon.Reference>();
        for(SObject sobj : records)
        { 
            c2g.CODAAPICommon.Reference ref = new c2g.CODAAPICommon.Reference();
            ref.Id = sobj.id;
            refs.add(ref);
        }
        
        // Call FinancialForce API to bulk post given Purchase Invoices (users current company)
        if(!SalesInvoiceBulkPostWebService.TestMode)
            c2g.CODAAPISalesInvoice_3_0.BulkPostInvoice(null, refs);    
    }
    
    /*
    *  Code Modified by Lakshman(sfdcace@gmail.com) to uncheck the BatchInProgress flag after posting of invoices is done
    */ 
    global void finish(Database.BatchableContext ctx)
    {
        BulkUpdatePostInvoices__c configEntry =BulkUpdatePostInvoices__c.getOrgDefaults();
        if(configEntry.BatchInProgress__c) {
            configEntry.BatchInProgress__c = false;
            update configEntry;
            if(isFromInvoiceVF != null && isFromInvoiceVF) {
                AsyncApexJob a = [Select Id, Status, NumberOfErrors, JobItemsProcessed, CreatedDate, CompletedDate, 
                TotalJobItems, CreatedBy.Email, CreatedById, CreatedBy.Name
                from AsyncApexJob where Id =:ctx.getJobId()];
                
                // Send an email to the Apex job's submitter notifying of job completion.  
                Messaging.SingleEmailMessage mail = new Messaging.SingleEmailMessage();
                String[] toAddresses = new String[] {a.CreatedBy.Email};
                mail.setToAddresses(toAddresses);
                mail.setSubject('Invoice Batch Post ' + a.Status);
                String body = 'Hi, <br/>Invoice Batch Post is ' + a.Status;
                body += '<br/>The batch Apex job was created by '+a.CreatedBy.Name+' ('+a.CreatedBy.Email+') processed '+a.TotalJobItems+' batches with '+a.NumberOfErrors+' failures. The process began at '+a.CreatedDate+' and finished at '+a.CompletedDate+'.';
                body += '<br/>Job Id ==>' + a.Id;
                mail.setHtmlBody(body);
                
                Messaging.sendEmail(new Messaging.SingleEmailMessage[] { mail });
            }
        }
    }   
}