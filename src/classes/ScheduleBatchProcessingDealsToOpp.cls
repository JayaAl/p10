/**
 * @name: ScheduleBatchProcessingDealsToOpp 
 * @desc: Scheduling the Batch class BatchProcessingDealsToOpp 
 * @author: Priyanka Ghanta
 * @date: 05/11/2015
 */
global class ScheduleBatchProcessingDealsToOpp implements Schedulable {
    global void execute(SchedulableContext sc) {
        BatchProcessingDealsToOpp bpdt = new BatchProcessingDealsToOpp();
        database.executebatch(bpdt);        
    }
 }