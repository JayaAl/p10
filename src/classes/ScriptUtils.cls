/**
* ─────────────────────────────────────────────────────────────────────────────────────────────────┐
* Utils class for Script Generation and Script Pdf generator.
* ──────────────────────────────────────────────────────────────────────────────────────────────────
* @changes
* v1.1            Jaya Alaparthi    <jalaparthi@pandora.com>
* 2016-11-10      adding size of the attachment records per every script
*
* vX.X            Jaya Alaparthi
* YYYY-MM-DD      Each change to this file should be documented by incrementing the version number,
*                 and adding a new entry to this @changes list. Note that there is a single blank
*                 line between each @changes entry.
* ─────────────────────────────────────────────────────────────────────────────────────────────────┘
*/
public class ScriptUtils {
    
     /* Empty Constructor */
     public ScriptUtils(){}
    
    /* Wrapper class containing Scrit and Attachment info */
    public class ScriptItem{
        public Script__c script{get;set;}
        public String recordId{get;set;}
        public Set<Id> setAttachmentIds{get;set;}
        public boolean boolEdit{get;set;}
        
        // v1.1
        public Integer attachmentSize {get;set;}
    }

    /* Method to split a String at each comma, returning a List of resulting Strings */
    public static List<String> spltString(String concatString){
        if(concatString==null || concatString==''){
            return null;
        }
        List<String> toReturn = new List<String>();
        for(String s: concatString.split(',')){
            toReturn.add(s.trim());
        }
        return toReturn;
    }
    
    /* Method to loop through provided ERP_Invoice records and save the generated Script PDF documents as Attachments */
    @future  (callout=true) // getContentAsPDF is treated as a Callout now, therforce this keyword
    public static void savePDFToInvoice(Set<Id> setERPIds){
        List<ERP_invoice__c> listInvoice = new List<ERP_Invoice__c>();
        List<Attachment> listAtt = new List<Attachment>();
        String dateString = Date.today().year()+'-'+Date.today().month()+'-'+Date.today().day();
        for(ERP_Invoice__c inv:[Select Id, Name, Campaign_Name__c from ERP_Invoice__c where Id IN:setERPIds]){ // need to query to find the Invoice details, in order to save teh Name correctly
            Attachment myAttachment  = new Attachment();
            pagereference pr = Page.ViewInvoiceScripts;
            pr.getParameters().put('id', inv.Id);
            myAttachment.Body = Test.isRunningTest() ? Blob.valueOf('UNIT.TEST') : pr.getContentAsPDF();
            String fileName = inv.Campaign_Name__c +'_'+ inv.Name+'.pdf';
            fileName = fileName.replaceAll('[^a-zA-Z0-9.-]', '_'); // replace all non-regular characters so the filename is valid
            myAttachment.Name = fileName;
            myAttachment.ParentId = inv.Id;
            listAtt.add(myAttachment);
            listInvoice.add(new ERP_Invoice__c(Id=inv.Id,Conga_Composer_Last_Run__c=Date.today()));
        }        
        insert listAtt;
        update listInvoice;
    }
       
    /* Method to create a ScriptItem from a single Script */
    public static ScriptItem scriptItemFromScript(Script__c s){
        ScriptItem si = new ScriptItem();
        si.script = s;
        si.recordId = s.Id;
        si.setAttachmentIds = new Set<Id>();
        si.boolEdit = false;
        return si;
    }
    
    /* Methods to return a List of empty Script records to 
       create placeholder records to be inserted after details are populated
    */
    public static List<Script__c> listEmptyScripts(){
        return listEmptyScripts(null);
    }
    public static List<Script__c> listEmptyScripts(Id oppId){
        List<Script__c> bulkScripts = new List<Script__c>();
        for(integer i=0;i<10;i++){
            bulkScripts.add(
                new Script__c(
                    Opportunity__c = oppId,
                    Script_Name__c = '',
                    Script_Start_Date__c = null,
                    Script_End_Date__c = null,
                    Script_Body__c = ''
                    
                )
            ); 
        }
        //
        return bulkScripts;
    }


    
    public static List<ScriptItem> queryScriptsForInvoiceIds(List<Id> listInvoiceIds){
        
        // First determine Opportunities, as well as Start and End Dates for all Invoices provided.
        Map<Id, Map<String, Date>> mapOpportunityToDates = new Map<Id, Map<String, Date>>();
        for(ERP_Invoice__c i:[
            SELECT Id, Conga_Billing_Period_Start__c, Conga_Billing_Period_End__c, Opportunity_Id__c 
            FROM ERP_Invoice__c 
            WHERE Id IN:listInvoiceIds
        ]){
            // populate mapOpportunityToDates
            if(!mapOpportunityToDates.containsKey(i.Opportunity_Id__c)){ // If this is the 1st time we find the Oppty simply populate the Map
                mapOpportunityToDates.put(i.Opportunity_Id__c, new Map<String, Date>{'start'=>i.Conga_Billing_Period_Start__c,'end'=>i.Conga_Billing_Period_End__c});
            } else { // If we previously found the Oppty update the Map if needed
                if(mapOpportunityToDates.get(i.Opportunity_Id__c).get('start') > i.Conga_Billing_Period_Start__c){
                    mapOpportunityToDates.get(i.Opportunity_Id__c).put('start',i.Conga_Billing_Period_Start__c);
                }
                if(mapOpportunityToDates.get(i.Opportunity_Id__c).get('end') < i.Conga_Billing_Period_End__c){
                    mapOpportunityToDates.get(i.Opportunity_Id__c).put('end',i.Conga_Billing_Period_End__c);
                }
            }
        }
        
        return queryScriptsForOpptyDateMap(mapOpportunityToDates);
    }
    
    public static List<ScriptItem> querySingleOpptyScripts(Id opptyId){
        Map<Id, Map<String, Date>> mapOpportunityToDates = new Map<Id, Map<String, Date>>();
        mapOpportunityToDates.put(opptyId, new Map<String, Date>{'start'=>null,'end'=>null});
        return queryScriptsForOpptyDateMap(mapOpportunityToDates);
    }
    
    public static List<ScriptItem> queryScriptsForOpptyDateMap(Map<Id, Map<String, Date>> mapOpportunityToDates){
        List<ScriptItem> listScriptItem = new List<ScriptItem>();
        Map<Id,ScriptItem> mapIdToScript = new Map<Id,ScriptItem>();
        
        // Now query all Scripts associated to those Opptys, adding Scripts to listScriptItem if they fall within Invoice dates 
        List<Id> listOppIds = new List<Id>();
        listOppIds.addAll(mapOpportunityToDates.keySet());
        for(Script__c s:queryScriptsForOpp(listOppIds)){
            // If the Script falls within the Dates outlined for the Opportunity then add to the final List for return
            if(mapOpportunityToDates.containsKey(s.Opportunity__c)){
                Date oppStart = mapOpportunityToDates.get(s.Opportunity__c).get('start');
                Date oppEnd = mapOpportunityToDates.get(s.Opportunity__c).get('end');
                if(
                    (oppEnd == null || s.Script_Start_Date__c <= oppEnd)
                    && (oppStart == null || s.Script_End_Date__c >= oppStart)
                ){
                    ScriptItem si = scriptItemFromScript(s);
                    listScriptItem.add(si);
                    mapIdToScript.put(s.Id,si);
                }
            }
        }
        
        // Add Attachments to the proper Script Item
        for(Attachment a:[
            SELECT Id, ParentId
            FROM Attachment 
            WHERE ParentId IN :mapIdToScript.keySet()
        ]){
            mapIdToScript.get(a.ParentId).setAttachmentIds.add(a.Id);
        }
        
         // v1.1
        for(Id mapId: mapIdToScript.keySet()) {
            
            ScriptItem sItem = new ScriptItem();
            sItem = mapIdToScript.get(mapId);
            sItem.attachmentSize = sItem.setAttachmentIds.size() > 0  ? sItem.setAttachmentIds.size(): 1;
            mapIdToScript.put(mapId,sItem);
        }
        return listScriptItem;
    }

    public static List<Script__c> queryScriptsForOpp(List<Id> listOppIds){
        return [
            SELECT Id, IsDeleted, Name, CurrencyIsoCode, CreatedDate, CreatedById, LastModifiedDate,
                LastModifiedById, SystemModstamp, Opportunity__c, Account_Name__c, Campaign_Name__c, Job__c,
                Override_Ad_Length__c, Override_Billable_Impressions__c, Override_Script_Amount__c,
                Script_Body__c, Script_End_Date__c, Script_Name__c, Script_Start_Date__c
            FROM Script__c
            WHERE Opportunity__c IN:listOppIds
        ];
    }
}