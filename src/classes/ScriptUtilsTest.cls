@isTest
public class ScriptUtilsTest {

    /* Prep testing record data */
    public static Account testAccount;
    public static Opportunity oppNoScripts;
    public static Opportunity oppWithScripts;
    public static ERP_Invoice__c testInvoice;
    public static List<Script__c> listTestScripts;
    public static integer testCount = 10;
    
    public static void prepTests(){
        // Create Opportunity
        testAccount = UTIL_TestUtil.createAccount();
        oppNoScripts = UTIL_TestUtil.generateOpportunity(testAccount.Id);
        oppWithScripts = UTIL_TestUtil.generateOpportunity(testAccount.Id);
        insert new List<Opportunity>{oppNoScripts,oppWithScripts};
        
        // Create ERP_Invoice
        testInvoice = new ERP_Invoice__c(
            Company__c = testAccount.Id, 
            Advertiser__c = testAccount.Id,
            Opportunity_Id__c = oppWithScripts.Id,
            Name = '12345',
            // Need to set Invoice dates to allow for testing
            Conga_Billing_Period_Start__c = null,
            Conga_Billing_Period_End__c = null,
            Invoice_Date__c = Date.today()
        );
        insert testInvoice;
        
        // Create Scripts
        listTestScripts = new List<Script__c>();
        for(Integer i=0;i<testCount;i++){
            listTestScripts.add(
                new Script__c(
                    Opportunity__c = oppWithScripts.Id, 
                    Job__c = UTIL_TestUtil.generateRandomString(16), 
                    Override_Ad_Length__c = Math.round( Math.random() * 1000 ),
                    Override_Billable_Impressions__c = Math.round( Math.random() * 1000 ),
                    Override_Script_Amount__c = Math.round( Math.random() * 1000 ),
                    Script_Body__c= UTIL_TestUtil.generateRandomString(256),                    
                    Script_End_Date__c = Date.today(), 
                    Script_Name__c = UTIL_TestUtil.generateRandomString(16), 
                    Script_Start_Date__c = Date.today()
                )
            );
        }
        
        insert listTestScripts;
    }
    
    public static testmethod void test_spltString(){
        // null string should return null
        String testStr = null;
        String[] test1 = ScriptUtils.spltString(testStr);
        system.assertEquals(null, test1); 
        
        // empty string should return null
        testStr = '';
        String[] test2 = ScriptUtils.spltString(testStr);
        system.assertEquals(null, test2); 
        
        // otherwise Split the string by Comma, removing extra leading and following spaces
        testStr = 'one, two ,      three four';
        String[] test3 = ScriptUtils.spltString(testStr);
        system.assertEquals('one', test3[0]);
        system.assertEquals('two', test3[1]);
        system.assertEquals('three four', test3[2]);
    }
    
    public static testmethod void test_savePDFToInvoice(){
        prepTests();
        Set<Id> setERPIds = new Set<Id>{testInvoice.id};
        test.startTest();
            ScriptUtils.savePDFToInvoice(setERPIds);
        test.stopTest();
        // query Attachments to the ERP Invoice, confirm that the PDF was actially generated
        List<Attachment> test1 = [Select Id from Attachment where ParentId = :testInvoice.id];
        system.assert(!test1.isEmpty());
    }
    
    public static testmethod void test_scriptItemFromScript(){
        prepTests();
        Set<Id> setERPIds = new Set<Id>{testInvoice.id};
        test.startTest();
            ScriptUtils.ScriptItem testSI = ScriptUtils.scriptItemFromScript(listTestScripts[0]);
        test.stopTest();
        // test the contents of the created (ScriptItem)testSI
        system.assertEquals(listTestScripts[0],testSI.script);
        system.assertEquals(listTestScripts[0].Id,testSI.recordId);
        system.assert(testSI.setAttachmentIds.isEmpty());
        system.assertEquals(false,testSI.boolEdit);
    }
    
    public static testmethod void test_listEmptyScripts(){
        prepTests();
        test.startTest();
            // test both overloaded functions
            List<Script__c> testList1 = ScriptUtils.listEmptyScripts();
            List<Script__c> testList2 = ScriptUtils.listEmptyScripts(oppNoScripts.Id);
        test.stopTest();
        system.assert(testList1.size()==10); // confirm the expected count of records in each List
        system.assert(testList2.size()==10); // confirm the expected count of records in each List
        
        // Confirm default values from 1st record in the list
        Script__c firstScript1 = testList1[0];
        system.assertEquals(null, firstScript1.Opportunity__c);
        system.assertEquals('', firstScript1.Script_Name__c);
        system.assertEquals(null, firstScript1.Script_Start_Date__c);
        system.assertEquals(null, firstScript1.Script_End_Date__c);
        system.assertEquals('', firstScript1.Script_Body__c);
        
        Script__c firstScript2 = testList2[0];
        system.assertEquals(oppNoScripts.Id, firstScript2.Opportunity__c);
        system.assertEquals('', firstScript2.Script_Name__c);
        system.assertEquals(null, firstScript2.Script_Start_Date__c);
        system.assertEquals(null, firstScript2.Script_End_Date__c);
        system.assertEquals('', firstScript2.Script_Body__c);
    }
    
    
    /*
    public static Account testAccount;
    public static Opportunity oppNoScripts;
    public static Opportunity oppWithScripts;
    public static ERP_Invoice__c testInvoice;
    public static List<Script__c> listTestScripts;
    */
    public static testmethod void test_queryScriptsForInvoiceIds(){
        prepTests();
        test.startTest();
            List<Id> listInvoiceIds = new List<Id>{testInvoice.Id};
            List<ScriptUtils.ScriptItem> testlist = ScriptUtils.queryScriptsForInvoiceIds(listInvoiceIds);
        test.stopTest();
        System.assert(testlist.size()==testCount);
        ScriptUtils.ScriptItem testSI = testlist[0];
        System.assertEquals(oppWithScripts.Id,testSI.script.Opportunity__c);
        System.assertEquals(Date.today(),testSI.script.Script_End_Date__c);
        System.assertEquals(Date.today(),testSI.script.Script_Start_Date__c);
    }
    
    public static testmethod void test_querySingleOpptyScripts(){
        prepTests();
        test.startTest();
            List<ScriptUtils.ScriptItem> testlist = ScriptUtils.querySingleOpptyScripts(oppWithScripts.Id);
        test.stopTest();
        System.assert(testlist.size()==testCount);
        ScriptUtils.ScriptItem testSI = testlist[0];
        System.assertEquals(oppWithScripts.Id,testSI.script.Opportunity__c);
    }
    
    public static testmethod void test_queryScriptsForOpptyDateMap(){
        prepTests();
        test.startTest();
            // List<ScriptItem> queryScriptsForOpptyDateMap(Map<Id, Map<String, Date>> mapOpportunityToDates)
        test.stopTest();
        System.assert(true);
    }
    
    public static testmethod void test_queryScriptsForOpp(){
        prepTests();
        test.startTest();
            // List<Script__c> queryScriptsForOpp(List<Id> listOppIds)
        test.stopTest();
        System.assert(true);
    }
    
}