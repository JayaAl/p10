/*
 * nsk 03/19/2014
 * File name: SearchInvoicesForCongaController.cls
 * Description: This is a controller class for generate_notarized_script VF page. The main functionalites supported by this controller are summarized below 
 * 1. Save User selected params to custom setting object - These values are later retrieved by Conga composer
 * 2. Search invoices based on the user entered criteria
 * 3. Provide pagination and other UI features
 * 4. Generate conga URL dynamically based on the user selected invoices and other salesforce instance values
 * 
 * 04/14/15 nsk:- ESS-16272 Conga URL is no longer supporting the apiSession which is added on the URL - SessionId='+ApexPages.currentPage().getParameters().get('apiSessionID');
 * This was the only available option till date. The URL is now being populated with the UserInfo.getSessionId() instead.    
 * 
 * Updated:        2015-06-02
 * Author:         Casey Grooms
 * Description:    Updated code to transition fropm using Fianancialforce objects to the internally developed ERP_Invoice__c obect
 * 
 */

public without sharing class SearchInvoicesForCongaController{
    

/* Variable Declarations */
    
    // public String invoiceStartDate{get;set;}
    // public String invoiceEndDate{get;set;}
    public String formattedStartDt{get;set;}    
    public String formattedEndDt{get;set;}    
    public String siInvoicePrinted {get;set;}
    public String siCampaignName{get;set;}
    public String siInvoiceNumber {get;set;}
    public String siAccountName {get;set;}
    public String siAgencyName {get;set;}
    public String siCreditManager {get;set;}
    public String siBillingPeriod {get;set;}
    public String siScriptCount {get;set;}
    public String siInvoiceType {get;set;}
    public String siSlingshotID {get;set;}
    public String siPrefreredInvoicingMethod {get;set;}
    public String signerNameparam {get;set;}
    public String signerCountyparam {get;set;}
    public String signerstateparam {get;set;}
    public String selectedSigner {get;set;}
    public String selectedState {get;set;}
    public String sortDirection = 'ASC';
    public String sortExp = 'Name'; 
    
    public String serverURL{get;set;}
    public String vfSessionId{get;set;} // SessionID passed from the page as a parameter in the commandButton
    public String apexSessionId{get;set;} // SessionID generated within the APEX Controller for this page
    public String urlSessionId{get;set;} // SessionID passed to the page as part of the URL
    public Boolean renderDebug{get;set;}
    public String pageSize {get;set;}
    public String pageNumber {get;set;}
    public String pageSizeOwned {get;set;}
    public String pageNumberOwned {get;set;}
    public String myQuery{get;set;}

/* Collection Declarations */
    public List<SelectOption> siPrintedOptions {get;set;}
    public Map<Id, List<Script__c>> invScripts = new Map<Id, List<Script__c>>();
    public Map<Id, Integer> invScriptsCount = new Map<Id, Integer>();
    public Map<String, String> mapURLParams{get;set;}

/* Object Declarations */
    public OpportunityLineItem oliObj{get;set;}
    public List<ERP_Invoice__c> listInvoices{get;set;}
    public List<InvoiceRowItem> listInvoiceRowItem{get;set;}
    public List<Script__c> scriptResults{get;set;}
    public ApexPages.StandardSetController setConListed{get;set;}
    
/* Page Constructor */
    public SearchInvoicesForCongaController(){
        // Load page defaults, override by URL if params present
        gatherDefaultParams();
    }

/* Method to populate page variables from URL if present, defaulting otherwise */
    private void gatherDefaultParams(){
        mapURLParams = ApexPages.currentPage().getParameters();
        pageSize = mapURLParams.containsKey('pageSize') ? mapURLParams.get('pageSize') : '20';
        pageNumber = mapURLParams.containsKey('pageNumber') ? mapURLParams.get('pageNumber') : '1';
        pageSizeOwned = mapURLParams.containsKey('pageSizeOwned') ? mapURLParams.get('pageSizeOwned') : '20';
        pageNumberOwned = mapURLParams.containsKey('pageNumberOwned') ? mapURLParams.get('pageNumberOwned') : '1';
        renderDebug = mapURLParams.containsKey('debug') ? mapURLParams.get('debug')=='TRUE' : FALSE;
        urlSessionId = mapURLParams.containsKey('apiSessionID') ? mapURLParams.get('apiSessionID') : null;
        serverURL = mapURLParams.containsKey('serverURL') ? mapURLParams.get('serverURL') : 'https://'+ApexPages.currentPage().getHeaders().get('Host')+'/services/Soap/u/16.0/'+UserInfo.getOrganizationId();
        apexSessionId = userInfo.getSessionId();

        // Setup empty collections to populate later
        listInvoices = new List<ERP_Invoice__c>();
        listInvoiceRowItem= new List<InvoiceRowItem>();

        // The following record is only used for reference purpose within the context of this transaction. 
        // Object reference is a must when the standard dates capabilities are needed on the VF page
        oliObj = new OpportunityLineItem(ServiceDate = null, End_Date__c = null);
        
        //The following custom setting records have the predefined list of signer info
        Conga_Invoice_Params__c cInvvalues = Conga_Invoice_Params__c.getValues(UserInfo.getUserId());
        if(cInvvalues!=null)
        {
            system.debug('cInvvalues = '+cInvvalues);
            signerNameparam = cInvvalues.SignerName__c; // the option
            selectedSigner = cInvvalues.SignerName__c; // what is displayed

            signerStateParam = cInvvalues.SignerState__c; // the option
            signerCountyParam = cInvvalues.SignerCounty__c; // the option
            selectedState = cInvvalues.SignerState__c +' - '+cInvvalues.SignerCounty__c; // what is displayed
        }
        siPrefreredInvoicingMethod = System.Label.Script_Preferred_Invoicing_Method;
        // siInvoiceType = Custom_Constants.SalesInvoiceType;
        siPrintedOptions = getsiPrintedOptions();
    }

/* Invoice search method: 
 * This searches for the invoices based on the user selected criteria. 
 * Note that there are some implicit filter crieria for invoices such as
 * Preferred_Invoicing_Method__c = Mail w/ Notarization
 * Invoice Type = Sales Invoice??
 */
    public PageReference searchInvoices(){
               
        string sortFullExp = sortExpression  + ' ' + sortDirection;
        // The following fields do not exist on the new ERP_Invoice__c object, commenting out and leaving here for reference
        //               '    c2g__Opportunity__r.Name, c2g__Opportunity__r.Slingshot_Order_ID__c, '
        //               '    Credit_Manager__c, ffps_edipan__Invoice_Last_Sent__c, c2g__Opportunity__r.Agency__r.Name, '
        myQuery =        'SELECT Id, Name, Billing_Period__c, ' + '\n' +
                         '    Opportunity_ID__c, Opportunity_ID__r.Name, Slingshot_invoice_ID__c, Company__c, Company__r.Name, ' + '\n' + 
                         '    Conga_Composer_Last_Run__c, Opportunity_ID__r.Account.ERP_Credit_Manager__c, ' + '\n' +    
            			 '    Opportunity_Id__r.Primary_Billing_Contact__r.Account.ERP_Credit_Manager__c, ' + '\n' +
            			 '    Opportunity_ID__r.AccountId, Opportunity_ID__r.Account.Name, ' + '\n' + 
            			 '    Opportunity_ID__r.Campaign_Manager__c, ' + '\n' + 
                         '    Invoice_Date__c, Invoice_Status__c, Advertiser__c, Advertiser__r.Name, ' + '\n' +
                         '    Conga_Billing_Period_Start__c, Conga_Billing_Period_End__c ' + '\n' +
                         'FROM ERP_Invoice__c ' + '\n' +
                         'WHERE Opportunity_ID__r.Preferred_Invoicing_Method__c = \''+this.siPrefreredInvoicingMethod+'\' ' + '\n' +
                         // '    AND Invoice_Status__c = \'Complete\' ';
                         '    AND COMPLETE_FLAG__c = \'Complete\' ';
        myQuery += '\n' +'    AND Conga_Composer_Last_Run__c ';
        myQuery += siInvoicePrinted=='NO' ? '= null' : '!= null';
        
        if(oliObj.ServiceDate != null){
            formattedStartDt = DateTime.newInstance(oliObj.ServiceDate, time.newInstance(0,0,0,0)).format('yyyy-MM-dd');
            myQuery += '\n' +'    AND Invoice_Date__c >= ' + formattedStartDt;
        }
        if(oliObj.End_Date__c!= null){
            formattedEndDt = DateTime.newInstance(oliObj.End_Date__c, Time.newInstance(0, 0, 0, 0)).format('yyyy-MM-dd');
            myQuery += '\n' +'    AND Invoice_Date__c <= ' + formattedEndDt;
        }
        if(siInvoiceNumber!= null && siInvoiceNumber!= ''){
            myQuery += '\n' +'    AND Name LIKE \'%' + siInvoiceNumber+ '%\'';
        }
        if(siSlingshotID!= null && siSlingshotID!= ''){
            myQuery += '\n' +'    AND Slingshot_invoice_ID__c LIKE \'%' + siSlingshotID+ '%\'';
        }
        if(siAccountName!= null && siAccountName!= ''){
            myQuery += '\n' +'    AND Company__r.Name LIKE \'%' + String.escapeSingleQuotes(siAccountName) + '%\'';
        }        
        if(siBillingPeriod!= null && siBillingPeriod!= ''){
            myQuery += '\n' +'    AND Billing_Period__c = \'' + siBillingPeriod+ '\'';
        }
        if(siCreditManager!= null && siCreditManager!= ''){
            myQuery += '\n' +'    AND Opportunity_Id__r.Primary_Billing_Contact__r.Account.ERP_Credit_Manager__r.Name LIKE \'%' + String.escapeSingleQuotes(siCreditManager)+ '%\'';
        }
        if(siAgencyName!= null && siAgencyName!= ''){
            myQuery += '\n' +'    AND Advertiser__r.Name LIKE \'%' + String.escapeSingleQuotes(siAgencyName) + '%\'';
        } 
        if(siCampaignName!= null && siCampaignName!= ''){
            myQuery += '\n' +'    AND Opportunity_ID__r.Name LIKE \'%' + String.escapeSingleQuotes(siCampaignName) + '%\'';
        }
        
        myQuery += '\n' +'ORDER BY '+sortFullExp;
        myQuery += '\n' +'LIMIT 1001';

        try{
            setConListed = new ApexPages.StandardSetController(Database.query(myQuery));
            // Here defining pagination step size
            setConListed.setPageSize(integer.valueOf(pageSizeOwned));
            setConListed.setpageNumber(integer.valueOf(pageNumberOwned));
            listInvoiceRowItem = getWrappedInvoices();
        } catch(Exception e) {
            addError('ERP Invoice Search query error. Please contact admin team. Error: ',e);
            return null;
        }

        return null;
    }
    
/* Methods to populate the Signer params from the corresponding custom settings */
    public List<selectOption> signerList {
        get {
            List<selectOption> options = new List<selectOption>();
            for (SignerName__c sName: SignerName__c.getAll().values())
                options.add(new SelectOption(sName.Name__c, sName.Name__c));
            return options;
        }
        set;
    }
    
    public List<selectOption> signerStateList {
        get {
            List<selectOption> options = new List<selectOption>();
            Map<String, SignerState__c> settings = SignerState__c.getAll();
            List<String> settingNames = new List<String>();
            settingNames.addAll(settings.keySet());
            settingNames.sort();
            
            for (String s : settingNames){
                SignerState__c stg = settings.get(s);
                options.add(new SelectOption(stg.Name, stg.Name));
            }             
            return options;
        }
        set;
    }
    
    public List<SelectOption> getsiPrintedOptions(){
        List<SelectOption> options = new List<SelectOption>();
        options.add(new SelectOption('NO','NO'));
        options.add(new SelectOption('YES','YES'));
        return options;
    }
    
/* Method to wrap the invoices with the corresponding params such as the script count. 
 * SOQL query is being built and executed based on the invoice search results to be able 
 * to tie the corresponding scripts to the corresponding invoice record
 */ 
    public List<InvoiceRowItem> getWrappedInvoices(){
        
        List<InvoiceRowItem> rows = new List<InvoiceRowItem>();
        Set<String> invOpptySet = new Set<String>();
        
        //get script count for these invoices that fall under the same date range
        if(this.setConListed != NULL){
            for(sObject i : this.setConListed.getRecords()){
                ERP_Invoice__c a = (ERP_Invoice__c)i;
                invOpptySet.add(a.Opportunity_Id__c);
            }
        }

        if(invOpptySet.size() > 0){
            String scriptQuery = 'select Id, Opportunity__c, Opportunity__r.Name, Script_Start_Date__c, Script_End_Date__c from Script__c where Opportunity__c IN :invOpptySet';        
            scriptQuery += ' Order by Opportunity__c';
            try{
                this.scriptResults = Database.query(scriptQuery);
            }
            catch(Exception e){
                addError('Opportunity search query error. Please contact admin team. Error: ',e);
            }
        }

        if(this.setConListed != NULL){
            for(sObject r : this.setConListed.getRecords()){
                ERP_Invoice__c a = (ERP_Invoice__c)r;
                InvoiceRowItem row = new InvoiceRowItem(a, this.scriptResults, false);
                rows.add(row);            
            }
        }

        return rows;
    }
    
/* The following methods are used for the pagination of the wrapped invoices */
    public void doNextOwned(){
        if(this.setConListed.getHasNext()){
            this.setConListed.next();
            pageNumberOwned = string.valueOf(this.setConListed.getPageNumber());
            listInvoiceRowItem = getWrappedInvoices();
        }
    }
    
    public void doPreviousOwned(){
        if(this.setConListed.getHasPrevious()){
            this.setConListed.previous();
            pageNumberOwned = string.valueOf(this.setConListed.getPageNumber());
            listInvoiceRowItem = getWrappedInvoices();
        }
    }
    
    public String sortExpression{
        get{
            return sortExp;
        }
        set{
            //if the column is clicked on then switch between Ascending and Descending modes
            if (value == sortExp){
                sortDirection = (sortDirection == 'ASC NULLS LAST')? 'DESC NULLS LAST' : 'ASC NULLS LAST';
            } else {
                sortDirection = 'ASC NULLS LAST';
            }
            sortExp = value;
        }
    }
    
    public String getSortDirection(){
        //if not column is selected 
        if (sortExpression == null || sortExpression == ''){
            return 'ASC NULLS LAST';
        } else {
            return sortDirection;
        }
    }
    
    public void setSortDirection(String value){  
        sortDirection = value;
    }    
    
/* Helper class that represents a InvoiceRowItem. 
 * This class acts as a wrapper for invoices and various other params of the associated records/fields.
 */
    public class InvoiceRowItem{
        public Boolean IsSelected{get;set;}
        public ERP_Invoice__c tInvoice{get;set;}
        public Integer tSalesInvoiceScriptCount {get;set;}

        public InvoiceRowItem(ERP_Invoice__c a, List<Script__c> lScripts, Boolean s){
            this.tInvoice = a;
            this.IsSelected = s;

            this.tSalesInvoiceScriptCount = 0;
            if(lScripts.size() > 0){
                for (Script__c scr: lScripts){
                    system.debug('Invoice Opportunity ==> ' + a.Opportunity_Id__c);
                    system.debug('Script Opportunity ==> ' + scr.Opportunity__c);
                    
                    if( a.Opportunity_Id__c.equals(scr.Opportunity__c)
                        && (
                               ( scr.Script_Start_Date__c >= tInvoice.Conga_Billing_Period_Start__c && scr.Script_Start_Date__c <= tInvoice.Conga_Billing_Period_End__c ) // Starts during the billing period
                            || ( scr.Script_End_Date__c >= tInvoice.Conga_Billing_Period_Start__c && scr.Script_End_Date__c <= tInvoice.Conga_Billing_Period_End__c ) // Ends during the billing period
                            || ( scr.Script_Start_Date__c <= tInvoice.Conga_Billing_Period_Start__c && scr.Script_End_Date__c >= tInvoice.Conga_Billing_Period_End__c ) // Starts before, and ends after the billing period
                        )
                    ){
                        tSalesInvoiceScriptCount++;
                    }
                }
            }

        }   
    }
    
/* This method gets the user selected params (signer info) and save them to custom settings. 
 * This is later retrieved by conga composer feature
 */
    public void saveParams(){
        try{
            SignerState__c cStateInfo = SignerState__c.getInstance(selectedState);
            Conga_Invoice_Params__c cInvParams = Conga_Invoice_Params__c.getInstance();
            cInvParams.SignerName__c = selectedSigner;               
            signerNameParam = selectedSigner;
            cInvParams.SignerState__c = cStateInfo.State__c;
            signerStateParam = cStateInfo.State__c;
            cInvParams.SignerCounty__c = cStateInfo.County_Region__c; 
            signerCountyParam = cStateInfo.County_Region__c;
            upsert cInvParams;
        }
        catch(Exception e){
            addError('Signature params could not be saved. Please contact admin team!',e);
        }
    }
    
/* Method to build the required params and send the request to the conga composer tool */
    public PageReference generatePDF(){
        String invoiceToPrint = '';
        // String serverURL = 'https://'+ApexPages.currentPage().getHeaders().get('Host')+'/services/Soap/u/16.0/'+UserInfo.getOrganizationId();
        // String congaURL;
        for(InvoiceRowItem ir: listInvoiceRowItem){
            if(ir.IsSelected){
                if(invoiceToPrint != ''){
                    invoiceToPrint = invoiceToPrint +','+ir.tInvoice.Id;
                } else {
                    invoiceToPrint = ir.tInvoice.Id;
                }
            }
        }
        
        //Check custom settings params before printing
        Conga_Invoice_Params__c cInvvalues = Conga_Invoice_Params__c.getValues(UserInfo.getUserId());
        if(cInvvalues==null){
            addError('Please save the signer params before continuing!',null);
            return null;
        }
        
        //build Wrapper
        if(invoiceToPrint.length()>0){
            try{
                PageReference pageRef = Page.ViewInvoiceScriptsWrapper;
                pageRef.setRedirect(true);
                pageRef.getParameters().put('recid',invoiceToPrint);
                return pageRef;
            } catch(Exception e){
                addError('Error: ',e);
            }
        } else {
            addError('You must select Invoices before generating PDFs!',null);
        }
        return null;
    }

/* Common method to add error messages to the page */
    private void addError(String message, Exception e){
        ApexPages.Message err = new ApexPages.Message(
            ApexPages.SEVERITY.error, 
            message + e
        );
        ApexPages.addMessage(err);
    }
}