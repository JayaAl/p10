/*
    Desc: Test code for SearchInvoicesForCongaController. The corresponding controller is used by the generate_notarized_script page. 
*/

@isTest
public class SearchInvoicesForCongaControllerTest {
  
/*
  Test the save params method in the controller with a new user. These params are stored in the custom settings.   
*/
    public static testMethod void testSaveParams(){

        SearchInvoicesForCongaController invoiceSearchcntrl = new SearchInvoicesForCongaController ();
        PageReference searchPage = new PageReference( '/apex/generate_notarized_script');   
        Test.setCurrentPage(searchPage);
        
        List<SelectOption> signerList = new List<SelectOption>();
        signerList.add(new SelectOption('Aaron','Aaron'));
        String signerName;
           
        List<SelectOption> signerStateList = new List<SelectOption>();
        signerStateList.add(new SelectOption('California - Alameda','California - Alameda'));
        String signerStates;  
        
        List<SelectOption> siPrintedOptions;
        String siPrintedOptionVal;
        
        Conga_Invoice_Params__c cInvvalues = new Conga_Invoice_Params__c();
         
        Profile p = [SELECT Id FROM Profile WHERE Name='Finance - Cash Entry Manager User']; 
        User usr = new User(Alias = 'newUser', Email='newuser@pandora.com', 
             EmailEncodingKey='UTF-8', LastName='Testing', LanguageLocaleKey='en_US', 
         LocaleSidKey='en_US', ProfileId = p.Id, 
         TimeZoneSidKey='America/Los_Angeles', UserName='newuser@pandora.com');

        System.runAs(usr) {
            System.debug('Current User: ' + UserInfo.getUserName());
            System.debug('Current Profile: ' + UserInfo.getProfileId()); 
         }
         
        Test.startTest();

        for ( SelectOption so : signerList){
            signerName = so.getValue();
            continue;
        }
        for ( SelectOption so : signerStateList){
            signerStates = so.getValue();
            continue;
        }
        siPrintedOptions = invoiceSearchcntrl.getsiPrintedOptions();
        
        system.debug('signerName = ' +signerName);
        system.debug('signerStates = ' +signerStates);
        
        SignerState__c cCounty = new SignerState__c(name = 'cSet');
        cCounty.State__c = 'California';
        cCounty.County_Region__c = 'Alameda';
        insert cCounty;
        
        //cCounty = SignerState__c.getInstance(signerStates);
        system.debug('signerStates = ' +signerStates);
        system.assertNotEquals(cCounty,null);
        
        cInvvalues = Conga_Invoice_Params__c.getValues(UserInfo.getUserId());    
        system.assertEquals(cInvvalues,null);

        if(cInvvalues == null) {             
            cInvvalues = new Conga_Invoice_Params__c(Name= 'cSet');
            cInvvalues.SignerName__c = signerName;
            cInvvalues.SignerState__c = cCounty.State__c;
            cInvvalues.SignerCounty__c = cCounty.County_Region__c;
            insert cInvvalues;
        }
        
        system.assertNotEquals(cInvvalues.SignerName__c,null);
        system.assertNotEquals(cInvvalues.SignerState__c,null);
        system.assertNotEquals(cInvvalues.SignerCounty__c,null);
        
        List<SelectOption> opt1 = invoiceSearchcntrl.signerList;
        List<SelectOption> opt2 = invoiceSearchcntrl.signerStateList;
        
        invoiceSearchcntrl.saveParams();
         
        Test.stopTest();
        
    }

/*
  Test the searchInvoices method in the controller class.   
*/
@isTest (SeeAllData=true)
    public static void testSearchInvoices() {
 
        ApexPages.StandardSetController setConListed;
        SearchInvoicesForCongaController invoiceSearchcntrl = new SearchInvoicesForCongaController();
        PageReference searchPage = new PageReference( '/apex/generate_notarized_script');   
        List<SearchInvoicesForCongaController.InvoiceRowItem> lWrapperItems = new List<SearchInvoicesForCongaController.InvoiceRowItem>();
        Test.setCurrentPage(searchPage);
        List<Apexpages.Message> msgs;
        boolean b;
        
        ERP_Invoice__c invoiceObjnew = new ERP_Invoice__c(Flight_Dates_Start__c =null, Flight_Dates_End__c = null);
        invoiceObjnew.Flight_Dates_Start__c = Date.valueOf('2011-01-01');
        invoiceObjnew.Flight_Dates_End__c = Date.valueOf('2014-01-01');
    
        // TODO:Fix this test method:
        // invoiceSearchcntrl.invoiceObj = invoiceObjnew; 
    
        String siPrintedOptionVal;

        //Commenting following code to support the Invoice creation step. The company setting on an invoice is indirectly tied to some FF settings 
        /* 
        Profile p = [SELECT Id FROM Profile WHERE Name='Finance - Cash Entry Manager User']; 
        User usr = new User(Alias = 'newUser', Email='newuser@pandora.com', 
             EmailEncodingKey='UTF-8', LastName='Testing', LanguageLocaleKey='en_US', 
         LocaleSidKey='en_US', ProfileId = p.Id, 
         TimeZoneSidKey='America/Los_Angeles', UserName='newuser@pandora.com');
        */
        
        //System.runAs(usr) {
            System.debug('Current User: ' + UserInfo.getUserName());
            System.debug('Current Profile: ' + UserInfo.getProfileId()); 
         //}

        //Create test data and query for it
        //createTestData();
        ERP_Invoice__c salesInv = createInvoice();
        Script__c scr = createScript(salesInv);
        
        //Valid data test case
        invoiceSearchcntrl.siInvoiceNumber = 'Test';
        invoiceSearchcntrl.siCampaignName = 'Test';
        invoiceSearchcntrl.siSlingshotID = 'Test';
        invoiceSearchcntrl.siAccountName = 'Test';
        invoiceSearchcntrl.siAgencyName = 'Test';
        invoiceSearchcntrl.siBillingPeriod= 'Test';

        searchPage = invoiceSearchcntrl.searchInvoices();
        lWrapperItems = invoiceSearchcntrl.getWrappedInvoices();

        msgs = Apexpages.getMessages();
        b = true;
        for(Apexpages.Message msg:msgs){
            if (msg.getDetail().contains('Error')) b = false;
        }
        system.assert(b);
        
        ////Null input test case
        invoiceSearchcntrl.siInvoiceNumber = '';
        invoiceSearchcntrl.siCampaignName = '';
        invoiceSearchcntrl.siSlingshotID = '';
        invoiceSearchcntrl.siAccountName = '';
        invoiceSearchcntrl.siAgencyName = '';
        invoiceSearchcntrl.siBillingPeriod= '';

        searchPage = invoiceSearchcntrl.searchInvoices();
        lWrapperItems = invoiceSearchcntrl.getWrappedInvoices();

        msgs = Apexpages.getMessages();
        b = true;
        for(Apexpages.Message msg:msgs){
            if (msg.getDetail().contains('Error')) b = false;
        }
        system.assert(b);

        //Invalid data test case
        invoiceSearchcntrl.siInvoiceNumber = '\'';
        invoiceSearchcntrl.siCampaignName = '';
        invoiceSearchcntrl.siSlingshotID = '%';
        invoiceSearchcntrl.siAccountName = '';
        invoiceSearchcntrl.siAgencyName = '';
        invoiceSearchcntrl.siBillingPeriod= '';

        searchPage = invoiceSearchcntrl.searchInvoices();
        lWrapperItems = invoiceSearchcntrl.getWrappedInvoices();

        msgs = Apexpages.getMessages();
        b = false;
        for(Apexpages.Message msg:msgs){
            if (msg.getDetail().contains('Error')) b = true;
        }
        system.assert(b);
        
        invoiceSearchcntrl.setSortDirection('Asc');
        String sDir = invoiceSearchcntrl.getSortDirection();
        System.assertEquals(sDir,'Asc');
        invoiceSearchcntrl.doNextOwned();
        invoiceSearchcntrl.doPreviousOwned();
        
    }
    //Test method to cover code under the wrapper class
    public static TestMethod void testInnerClass(){ 
        ERP_Invoice__c salesInv = createInvoice();
        Script__c scr = createScript(salesInv);
        List<Script__c> lScr = new List<Script__c>();
        lScr.add(scr);
              
        SearchInvoicesForCongaController oc = new SearchInvoicesForCongaController(); 
        oc.getWrappedInvoices();
        SearchInvoicesForCongaController.InvoiceRowItem ic = new SearchInvoicesForCongaController.InvoiceRowItem(salesInv, lScr, false);
        system.assertNotEquals(ic, null);
    }       

 @isTest (SeeAllData=true)
    public static void testGeneratePDF(){ 

        SearchInvoicesForCongaController invoiceSearchcntrl = new SearchInvoicesForCongaController();
        PageReference searchPage = new PageReference( '/apex/generate_notarized_script');   
        Test.setCurrentPage(searchPage);
    
        ERP_Invoice__c invoiceObjnew = new ERP_Invoice__c(Flight_Dates_Start__c =null, Flight_Dates_End__c = null);
        invoiceObjnew.Flight_Dates_Start__c = Date.valueOf('2011-01-01');
        invoiceObjnew.Flight_Dates_End__c = Date.valueOf('2014-01-01');
    
        // TODO:Fix this test method:
        // invoiceSearchcntrl.invoiceObj = invoiceObjnew; 
    
        String siPrintedOptionVal;
        
        //Commenting following code to support the Invoice creation step. The company setting on an invoice is indirectly tied to some FF settings 
        /*
        Profile p = [SELECT Id FROM Profile WHERE Name='Finance - Cash Entry Manager User']; 
        User usr = new User(Alias = 'newUser', Email='newuser@pandora.com', 
             EmailEncodingKey='UTF-8', LastName='Testing', LanguageLocaleKey='en_US', 
         LocaleSidKey='en_US', ProfileId = p.Id, 
         TimeZoneSidKey='America/Los_Angeles', UserName='newuser@pandora.com');
        */
        //System.runAs(usr) {
            System.debug('Current User: ' + UserInfo.getUserName());
            System.debug('Current Profile: ' + UserInfo.getProfileId()); 
         //}
        
        //Create test data and query for it
        // createTestData();
        ERP_Invoice__c salesInv = createInvoice();
        
        //search invoices
        invoiceSearchcntrl.searchInvoices();

        Script__c scr = createScript(salesInv);
        List<Script__c> lScr = new List<Script__c>();
        lScr.add(scr);
        
        SearchInvoicesForCongaController.InvoiceRowItem ic = new SearchInvoicesForCongaController.InvoiceRowItem(salesInv, lScr, false);
        system.assertNotEquals(ic, null);
        
        List<SearchInvoicesForCongaController.InvoiceRowItem> icList = invoiceSearchcntrl.listInvoiceRowItem;
        
        if(icList != null && icList.size() > 0)
            for(SearchInvoicesForCongaController.InvoiceRowItem invRow : icList)
                invRow.isSelected = TRUE;
        
        // TODO:Fix this test method:
        // String apiUrl = invoiceSearchcntrl.getAPIURL();
        searchPage = invoiceSearchcntrl.generatePDF();
    }       

    public static void createTestData() {
        Account acc = UTIL_TestUtil.createAccount();
        Opportunity opp = UTIL_TestUtil.createOpportunity(acc.Id);    
    }

@isTest (SeeAllData=true)
    public static ERP_Invoice__c createInvoice(){
        //Create Invoice
        
        ERP_Invoice__c salesInv = new ERP_Invoice__c();
        Account acc = UTIL_TestUtil.createAccount();
        acc.CurrencyIsoCode = 'USD';
        update acc;
        Opportunity opp = UTIL_TestUtil.createOpportunity(acc.Id);    
        ERP_Invoice__c testInvoice1 = new ERP_Invoice__c(
                // c2g__Account__c = acc.id,
                Invoice_Date__c = Date.today(),
                Opportunity_Id__c = opp.id,
                // c2g__OwnerCompany__c = 'a1A400000009gXjEAI',
                Due_Date__c = Date.Today() + 10,
                Conga_Billing_Period_Start__c = Date.today(),
                Conga_Billing_Period_End__c = Date.today()
            );        
        

        insert testInvoice1;        
        
        return testInvoice1;
    }
    public static Script__c createScript(ERP_Invoice__c e){
        //Create Script__c
        
        Script__c s = new Script__c(
            // Id
            // IsDeleted
            // Name
            // CurrencyIsoCode
            // CreatedDate
            // CreatedById
            // LastModifiedDate
            // LastModifiedById
            // SystemModstamp
            Opportunity__c = e.Opportunity_Id__c,
            // Account_Name__c
            // Campaign_Name__c
            Job__c = 'Job Name Here',
            Override_Ad_Length__c = 22.0,
            Override_Billable_Impressions__c = 33.0,
            Override_Script_Amount__c = 44.0,
            Script_Body__c = 'Sample Script Body Here',  
            Script_End_Date__c = Date.today(),
            Script_Name__c = 'Sample Script Name',
            Script_Start_Date__c = Date.today()
        );
        insert s;        
        return s;
    }
   
}