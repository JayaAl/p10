public with sharing class SelectArtistMarketersCtrlr {

    public String accountIdVar {get;set;}
    public String accountNameVar {get;set;}
    private Opportunity currentOppty;
    public List<OpportunityContactRole> ocrlst {get;set;}
    public List<Oppty_Account_Junction__c> junclstToDisp {get;set;}
    public boolean isAAMSelectedBoolean {get;set;}
    private List<id> allSelectedLabelIdLst ;
    public Id recordToRemove {get;set;}

    public SelectArtistMarketersCtrlr(ApexPages.StandardController controller) {
        List<Opportunity> oppLst = [Select id,name,AccountId from Opportunity where id =: controller.getId() limit 1]; 
        currentOppty = oppLst[0];
        //populateContactRoleLst();
        populateJunctionLst();

    }

    public void addACRRecordAction(){
        system.debug('inside addACRRecordAction accountIdVar -->'+accountIdVar+'accountNameVar -->'+accountNameVar);
        List<OpportunityContactRole> ocrLst = new List<OpportunityContactRole>();
        List<Oppty_Account_Junction__c> juncLst = new List<Oppty_Account_Junction__c>();

        // accountId : Account selected on the oppty
        // New Account selected from the Lookup on page

        Id accountId = currentOppty.AccountId;
        //Account acctObj = ACRDisplayUtil.getAccountDetails(accountId);
        //List<Id> acctIdLst = new List<Id>{accountId}; // AccountId , eventually query junction and pass all accociated Ids of Label Accounts
        // List of Accounts (Labels) which needs to be checked 
        allSelectedLabelIdLst.add(accountId);
        Map<Id,Account> acctMap = ACRDisplayUtil.getAccountDetailsBulkified(allSelectedLabelIdLst);
        system.debug('acctMap ::->'+acctMap);
        ACRDisplayHelper acrRef = new ACRDisplayHelper();
        List<ACRDisplayWrapper> ACRWrapperLst = new List<ACRDisplayWrapper>();
 
        for(Account acct: acctMap.values()) {
            ACRDisplayWrapper acrWrapper = acrRef.displayArtistHirearchy(acct.Id,acct);
            system.debug('acrWrapper :::'+acrWrapper);
            ACRWrapperLst.add(acrWrapper);
        }
        //ACRDisplayWrapper acrWrapper = acrRef.displayArtistHirearchy(accountId,acctObj);
        system.debug(ACRWrapperLst);

        Map<Id,Account> artistMap = new Map<Id,Account>();

        for(ACRDisplayWrapper acrWrapper : ACRWrapperLst){

            for(Id labelVar : acrWrapper.sublabelSet){
                if(acrWrapper.perSubLabelArtistList.containsKey(labelVar)){
                    set<Account> artistSet = acrWrapper.perSubLabelArtistList.get(labelVar);
                    for(Account accountVar : acrWrapper.perSubLabelArtistList.get(labelVar)){
                        artistMap.put(accountVar.Id,accountVar);
                    }
                }
            }   
        }
        

        system.debug('artistMap :-->'+artistMap);
        
        boolean isLabelActVal = isLabelAct(accountIdVar);
        
        if(isLabelActVal){          
            juncLst.add(new Oppty_Account_Junction__c(Account__c = accountIdVar,Opportunity__c = currentOppty.Id, is_AAM_running_Artist__c = isAAMSelectedBoolean));            
        }

        if(!isLabelActVal){ //  && artistMap.containsKey(accountIdVar)
            // create OCR                       
            List<AccountContactRelation> ACRLst = ACRDisplayUtil.getAccountContacts(accountIdVar,'ContactManagedAccounts');         
            for(AccountContactRelation acrVar : ACRLst){
                if(acrVar.isDirect){
                    ocrLst.add(new OpportunityContactRole(ContactId = acrVar.ContactId, OpportunityId = currentOppty.Id));
                    juncLst.add(new Oppty_Account_Junction__c(Account__c = acrVar.AccountId,Opportunity__c = currentOppty.Id,is_AAM_running_Artist__c = isAAMSelectedBoolean));
                }
            }
            system.debug('valid artist');           
        }

        //if(!ocrLst.isEmpty() || !juncLst.isEmpty()){
            if(!ocrLst.isEmpty())
                insert ocrLst;
            if(!juncLst.isEmpty())
                insert juncLst;         
            
            //populateContactRoleLst();
            populateJunctionLst();
            // Create Junction
            ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.CONFIRM,'The Artist/ Label is added successfully to the campaign.'));             

        /*}else{
            // throw error Saying Select Artist from heirarchy
            ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.WARNING,'The Artist you selected is not in the heirarchy of the label or not a valid Label nor an independent artist.'));             
        }*/
        
    }

    private Boolean isLabelAct(id acctId){
        List<Account> acctLst =  [Select id,name,type from Account where id =: acctId];     
        return (acctLst != null && !acctLst.isEmpty()) ? ('Label'.equalsIgnoreCase(acctLst[0].type) ? true : false ) : false ;
    }

    private void populateContactRoleLst(){      
        ocrlst = new List<OpportunityContactRole>();
        ocrlst = [Select Id,OpportunityId,Opportunity.Name,ContactId,Contact.name,Role from OpportunityContactRole where OpportunityId =: currentOppty.Id];
    }

    private void populateJunctionLst(){     
        junclstToDisp = new List<Oppty_Account_Junction__c>();
        allSelectedLabelIdLst = new List<id>();
        isAAMSelectedBoolean = false;
        junclstToDisp = [Select id,name,Account__c,Account__r.Type,Account__r.Name,Opportunity__c,Opportunity__r.Name,is_AAM_running_Artist__c from Oppty_Account_Junction__c where Opportunity__c =: currentOppty.Id];

        for(Oppty_Account_Junction__c juncVar : junclstToDisp){
            if('Label'.equalsIgnoreCase(juncVar.Account__r.Type))
                allSelectedLabelIdLst.add(juncVar.Account__c);
        }

    }

    public void RemoveJuncAction(){
        system.debug('RemoveJuncAction Begins '+recordToRemove);
        Oppty_Account_Junction__c juncObjToDel;

        for(Oppty_Account_Junction__c juncVar : junclstToDisp){
            system.debug('Id =>'+juncVar.Id);           
            if(juncVar.Id == recordToRemove){
                juncObjToDel = juncVar;
                break;
            }
        }

        system.debug(recordToRemove);

        try{

            if(juncObjToDel != null)    
                delete juncObjToDel;            

            populateJunctionLst();  
        }catch(Exception ex){
            ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.WARNING,ex.getMessage()+ex.getStackTraceString()));             
        }
        

        ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.CONFIRM,'The Artist/ Label is removed successfully from the campaign.'));             
        system.debug('RemoveJuncAction Ends ');
    }
}