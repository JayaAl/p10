public class Snaplogic_Anaplan_Controller_Extension {

	public c2g__codaBudget__c codabudget {get; private set;}
	public c2g__codaCompany__c codacompany {get; private set;}
	public c2g__codaYear__c codayear {get; private set;}
	public c2g__codaGeneralLedgerAccount__c codaglaccount {get; private set;}

	// The extension constructor initializes the private member
	// variable acct by using the getRecord method from the standard
	// controller.
	public Snaplogic_Anaplan_Controller_Extension(ApexPages.StandardController stdController) {
	    ffreport = 'P&L';
	}

	private List<SelectOption> periods;
	private String period;

	// notice this method is called in the Visualforce markup without the “get”
	private String company;
	private List<SelectOption> companies;
	
	private List<SelectOption> ffreports;
	private String ffReport;
	
	private List<SelectOption> anaplanmodules;
	private String anaplanmodule;
	
	private String glaccount;
	private List<SelectOption> glaccounts;
	
	public String getPeriod() {
	  return this.period;
	}

	public void setPeriod(String p) {
	  period = p;
	}

	public List<SelectOption> getPeriods() {
	    periods= new List<SelectOption>();
	    List<c2g__codaYear__c> yearList =  [select Name, id from c2g__codaYear__c ];
	    for (c2g__codaYear__c s : yearList) {
	        periods.add(new SelectOption(s.Name,s.Name));
	    }
	    return periods;
	
	}

	public String getCompany() {
	  return this.company;
	}
	
	public void setCompany(String p) {
	  company = p;
	}
	
	public List<SelectOption> getCompanies() {
	    companies= new List<SelectOption>();
	    companies.add(new SelectOption('Consolidated','Consolidated'));  
	    List<c2g__codaCompany__c> companyList =  [select Name, id from c2g__codaCompany__c ];
	    for (c2g__codaCompany__c s : companyList) {
	        companies.add(new SelectOption(s.Name,s.Name));
	    }
	    return companies;
	     
	}
	
	public String getFfreport() {
	  return this.ffreport;
	}
	
	public void setFfreport(String p) {
	  ffreport = p;
	}
	
	public List<SelectOption> getFfreports() {
	    ffreports= new List<SelectOption>();
	    ffreports.add(new SelectOption('P&L','P&L'));   
	    ffreports.add(new SelectOption('Balance Sheet','Balance Sheet'));
	    return ffreports;
	
	}
	
	public String getAnaplanmodule() {
	  return this.anaplanmodule;
	}
	
	public void setAnaplanmodule(String p) {
	  anaplanmodule = p;
	}
	
	public List<SelectOption> getAnaplanmodules() {
	    anaplanmodules= new List<SelectOption>();
	    anaplanmodules.add(new SelectOption('P&L Financial Force Upload','P&L Financial Force Upload'));   
	    anaplanmodules.add(new SelectOption('Balance Sheet Financial Force Upload','Balance Sheet Financial Force Upload'));
	    return anaplanmodules;
	
	}
	
	
	public String getGlaccount() {
	  return this.glaccount;
	}
	
	public void setGlaccount(String p) {
	  glaccount = p;
	}
	
	public List<SelectOption> getGlaccounts() {
	    String concatenatedStr = '';
	    glaccounts= new List<SelectOption>();
	    List<c2g__codaGeneralLedgerAccount__c> glAccountList = null;
	    glaccounts.add(new SelectOption('All','All'));
	    if(ffreport!=null && ffreport.equals('P&L')) {
	         glAccountList =  [select Name, id, c2g__ReportingCode__c from c2g__codaGeneralLedgerAccount__c where c2g__Type__c = 'Profit and Loss' order by c2g__ReportingCode__c ];
	    }
	    else if(ffreport!=null && ffreport.equals('Balance Sheet')) {
	         glAccountList =  [select Name, id, c2g__ReportingCode__c from c2g__codaGeneralLedgerAccount__c where c2g__Type__c = 'Balance Sheet' order by c2g__ReportingCode__c ];
	    }
	    for (c2g__codaGeneralLedgerAccount__c s : glAccountList) {
	        concatenatedStr = s.c2g__ReportingCode__c + ' ' + s.Name;
	        glaccounts.add(new SelectOption(concatenatedStr,concatenatedStr));
	    }
	    return glaccounts;
	     
	}
	
	
	
	public void submit(){
	
	}

	
	static testmethod void test_Snaplogic_Anaplan_Controller_Extension(){

		c2g__codaBudget__c cb = new c2g__codaBudget__c();                       
	    ApexPages.StandardController sc = new ApexPages.StandardController(cb);        
	    Snaplogic_Anaplan_Controller_Extension controller = new Snaplogic_Anaplan_Controller_Extension(sc);       
	                
	    controller.getAnaplanmodule();
	    controller.getAnaplanmodules();
	    controller.getCompanies();
	    controller.getCompany();
	    controller.getFfreport();
	    controller.getFfreports();
	    controller.getGlaccount();
	    controller.getGlaccounts();
	    controller.getPeriod();
	    controller.getPeriods();
	    controller.setAnaplanmodule('test');
	    controller.setCompany('test');
	    controller.setFfreport('test');
	    controller.setGlaccount('test');
	    controller.setPeriod('test');
	    controller.submit();

	}


}