/*
*    Code Modified by Lakshman(sfdcace@gmail.com) on 10-10-2013. Modifications are as below:
*    1. Made Year to be set dynamically, if 2013 is current year then the dropdown will have +2 and -3 year values displayed as well, viz, 2010,2011,2012,2013,2014,2015
*    2. Months are captured from Fisal_Period__c custom settings
nsk: 03/21/14: Switch to ESB service
ESB-85: The code has been modified to point to ESB service instead of snaplogic. The new service will take the params "as is" and will build the queries on the 
ESB service instead of on SFDC side. 

*/
public class Snaplogic_Anaplan_Controller_Extension2 {

    public c2g__codaBudget__c codabudget {get; private set;}
    public c2g__codaCompany__c codacompany {get; private set;}
    public c2g__codaYear__c codayear {get; private set;}
    public c2g__codaGeneralLedgerAccount__c codaglaccount {get; private set;}
    public c2g__codaDimension1__c codadimension {get; private set;}
    Custom_Constants__c stageURL = Custom_Constants__c.getValues('ESBStageURL');
    Custom_Constants__c prodURL = Custom_Constants__c.getValues('ESBProdURL');
   
    // The extension constructor initializes the private member
    // variable acct by using the getRecord method from the standard
    // controller.
    public Snaplogic_Anaplan_Controller_Extension2(ApexPages.StandardController stdController) {
   
        ffreport = 'P&L';
        ffyear = string.valueOf(System.now().year());
        month='All';
        company='Pandora Consolidated';
        anaplanmodule = 'P&L Financial Force Upload 1';
        model = 'Snaplogic Model';
        department = 'All';
        product = 'All';
        location='All';
        dimension4 = ' ';
        glaccount = 'All';
        linkvalue = '';
        workspace = 'Pandora';
        dlValue='';
               
    }
   
    private List<SelectOption> ffyears;
    private String ffyear;
   
    private List<SelectOption> months;
    private String month;
       
    // notice this method is called in the Visualforce markup without the “get”
   
    private String company;
    private List<SelectOption> companies;
   
    private List<SelectOption> ffreports;
    private String ffReport;
   
    private List<SelectOption> anaplanmodules;
    private String anaplanmodule;
   
    private String glaccount;
    private List<SelectOption> glaccounts;
   
    private String department;
    private List<SelectOption> departments;
   
    private String product;
    private List<SelectOption> products;
   
    private String location;
    private List<SelectOption> locations;
   
    private String dimension4;
    private List<SelectOption> dimension4s;
   
    private String linkvalue;
   
    private String dlValue;
   
    private String model;
    private String workspace;
   
    public String getModel() {
      return this.model;
    }
   
    public String getWorkspace() {
      return this.workspace;
    }
   
   
    public void setModel(String p) {
      model = p;
    }
   
   
    public String getMonth() {
      return this.month;
    }
   
    public void setMonth(String p) {
      month = p;
    }
   
    public List<SelectOption> getMonths() {
        months= new List<SelectOption>();
        if(ffreport!=null && !ffreport.equals('Account')) {
        months.add(new SelectOption('All','All'));
        }
        Map<String, Fiscal_Period__c> fiscalYearMap = Fiscal_Period__c.getAll();
        List<String> settingNames = new List<String>();
        settingNames.addAll(fiscalYearMap.keySet());
        settingNames.sort();
        for(String s: settingNames) {
            Fiscal_Period__c f = fiscalYearMap.get(s);
            months.add(new SelectOption(f.Name + '-' + f.Period__c,f.Name + '-' + f.Period__c));
        }
        /*months.add(new SelectOption('001-Feb','001-Feb'));
        months.add(new SelectOption('002-Mar','002-Mar'));
        months.add(new SelectOption('003-Apr','003-Apr'));
        months.add(new SelectOption('004-May','004-May'));
        months.add(new SelectOption('005-Jun','005-Jun'));
        months.add(new SelectOption('006-Jul','006-Jul'));
        months.add(new SelectOption('007-Aug','007-Aug'));
        months.add(new SelectOption('008-Sep','008-Sep'));
        months.add(new SelectOption('009-Oct','009-Oct'));
        months.add(new SelectOption('010-Nov','010-Nov'));
        months.add(new SelectOption('011-Dec','011-Dec'));
        months.add(new SelectOption('012-Jan','012-Jan'));*/
        return months;
   
    }
   
    public String getFfyear() {
   
      return this.ffyear;
    }
   
    public void setFfyear(String p) {
      ffyear = p;
    }
   
    public List<SelectOption> getFfyears() {
       
        ffyears= new List<SelectOption>();
     
        /*
        List<c2g__codaYear__c> yearList =  [select Name,id from c2g__codaYear__c ];
        Set<String> optionSet = new Set<String>();
       
        for (c2g__codaYear__c s : yearList) {
           optionSet.add(s.Name);
         }
       
        for(String s: optionSet)
        {
        ffyears.add(new SelectOption(s,s));
         
        }
        */
        
        //Add years based on current year
        Integer currentYear = System.now().year();
        ffyears.add(new SelectOption(string.valueOf(currentYear-3),string.valueOf(currentYear-3)));
        ffyears.add(new SelectOption(string.valueOf(currentYear-2),string.valueOf(currentYear-2)));
        ffyears.add(new SelectOption(string.valueOf(currentYear-1),string.valueOf(currentYear-1)));
        ffyears.add(new SelectOption(string.valueOf(currentYear),string.valueOf(currentYear)));
        ffyears.add(new SelectOption(string.valueOf(currentYear+1),string.valueOf(currentYear+1))); 
        ffyears.add(new SelectOption(string.valueOf(currentYear+2),string.valueOf(currentYear+2))); 
        
        return ffyears;
   
    }
   
   
    public String getDepartment() {
      return this.department;
    }
   
    public void setDepartment(String p) {
      department = p;
    }
   
    public List<SelectOption> getDepartments() {
   
        departments= new List<SelectOption>();
        departments.add(new SelectOption('All','All'));
            List<c2g__codaDimension1__c> departmentList =  [select Name, c2g__ReportingCode__c, id from c2g__codaDimension1__c ];
            for (c2g__codaDimension1__c s : departmentList) {
                departments.add(new SelectOption(s.c2g__ReportingCode__c + '-' + s.Name,s.c2g__ReportingCode__c + '-' + s.Name));
            }
        return departments;
    }
   
    public String getProduct() {
      return this.product;
    }
   
    public void setProduct(String p) {
      product = p;
    }
   
    public List<SelectOption> getProducts() {
   
        products= new List<SelectOption>();
        if(ffreport!=null && ffreport.equals('P&L')) {
            products.add(new SelectOption('All','All'));
            List<c2g__codaDimension2__c> productList =  [select Name, c2g__ReportingCode__c, id from c2g__codaDimension2__c ];
            for (c2g__codaDimension2__c s : productList) {
                products.add(new SelectOption(s.Name, s.Name));
            }
        }
        else if(ffreport!=null && ffreport.equals('Balance Sheet')) {
             products.add(new SelectOption('Consolidated','Consolidated'));
        }
        else if(ffreport!=null && ffreport.equals('Account')) {
            products.add(new SelectOption('All','All'));
            List<c2g__codaDimension2__c> productList =  [select Name, c2g__ReportingCode__c, id from c2g__codaDimension2__c ];
            for (c2g__codaDimension2__c s : productList) {
                products.add(new SelectOption(s.Name, s.Name));
            }
        }
        return products;
   
    }
   
    public String getLocation() {
      return this.location;
    }
   
    public void setLocation(String p) {
      location = p;
    }
   
    public List<SelectOption> getLocations() {
   
        locations = new List<SelectOption>();
            locations.add(new SelectOption('All','All'));
            List<c2g__codaDimension3__c> locationsList =  [select Name, c2g__ReportingCode__c, id from c2g__codaDimension3__c ];
            for (c2g__codaDimension3__c s : locationsList) {
                locations.add(new SelectOption(s.c2g__ReportingCode__c + '-' + s.Name,s.c2g__ReportingCode__c + '-' + s.Name));
            }
        return locations;
   
    }
   
    public String getDimension4() {
      return this.dimension4;
    }
   
    public void setDimension4(String p) {
      dimension4 = p;
    }
   
    public List<SelectOption> getDimension4s() {
   
        dimension4s= new List<SelectOption>();
        if(ffreport!=null && ffreport.equals('P&L')) {
            dimension4s.add(new SelectOption('All','All'));
            List<c2g__codaDimension4__c> dimension4sList =  [select Name, c2g__ReportingCode__c, id from c2g__codaDimension4__c ];
            for (c2g__codaDimension4__c s : dimension4sList) {
                dimension4s.add(new SelectOption(s.c2g__ReportingCode__c + '-' + s.Name,s.c2g__ReportingCode__c + '-' + s.Name));
            }
        }
        else if(ffreport!=null && ffreport.equals('Balance Sheet')) {
             dimension4s.add(new SelectOption('',''));
        }
        else if(ffreport!=null && ffreport.equals('Account')) {
            dimension4s.add(new SelectOption('All','All'));
            List<c2g__codaDimension4__c> dimension4sList =  [select Name, c2g__ReportingCode__c, id from c2g__codaDimension4__c ];
            for (c2g__codaDimension4__c s : dimension4sList) {
                dimension4s.add(new SelectOption(s.c2g__ReportingCode__c + '-' + s.Name,s.c2g__ReportingCode__c + '-' + s.Name));
            }
        }
       
        return dimension4s;
   
    }
   
    public String getCompany() {
      return this.company;
    }
   
    public void setCompany(String p) {
      company = p;
    }
   
   public List<SelectOption> getCompanies() {
        companies= new List<SelectOption>();
        companies.add(new SelectOption('Pandora Consolidated','Pandora Consolidated')); 
        List<c2g__codaCompany__c> companyList =  [select Name, id from c2g__codaCompany__c ];
        for (c2g__codaCompany__c s : companyList) {
                companies.add(new SelectOption(s.Name, s.Name));
        }
        return companies;
        
    }
   
    public String getFfreport() {
      return this.ffreport;
    }
   
    public void setFfreport(String p) {
      ffreport = p;
    }
   
    public List<SelectOption> getFfreports() {
        ffreports= new List<SelectOption>();
        ffreports.add(new SelectOption('P&L','P&L'));  
        ffreports.add(new SelectOption('Balance Sheet','Balance Sheet'));
        ffreports.add(new SelectOption('Account','Account'));
        return ffreports;
   
    }
   
    public String getAnaplanmodule() {
      return this.anaplanmodule;
    }
   
    public void setAnaplanmodule(String p) {
      anaplanmodule = p;
    }
   
    public List<SelectOption> getAnaplanmodules() {
        anaplanmodules= new List<SelectOption>();
        if(ffreport!=null && ffreport.equals('P&L')) {
        anaplanmodules.add(new SelectOption('P&L Financial Force Upload','P&L Financial Force Upload'));  
        } else if(ffreport!=null && ffreport.equals('Balance Sheet')) {
        anaplanmodules.add(new SelectOption('Balance Sheet Financial Force Upload','Balance Sheet Financial Force Upload'));
        } else if(ffreport!=null && ffreport.equals('Account')) {
        anaplanmodules.add(new SelectOption('GL Account by Vendor','GL Account by Vendor'));
        }
        return anaplanmodules;
   
    }
   
   
    public String getGlaccount() {
      return this.glaccount;
    }
   
    public void setGlaccount(String p) {
      glaccount = p;
    }
   
    public List<SelectOption> getGlaccounts() {
   
        String concatenatedStr = '';
        glaccounts= new List<SelectOption>();
        List<c2g__codaGeneralLedgerAccount__c> glAccountList = null;
        glaccounts.add(new SelectOption('All','All'));
        if(ffreport!=null && ffreport.equals('P&L')) {
             glAccountList =  [select Name, id, c2g__ReportingCode__c from c2g__codaGeneralLedgerAccount__c where c2g__ReportingCode__c>'39999' order by c2g__ReportingCode__c ];
        }
        else if(ffreport!=null && ffreport.equals('Balance Sheet')) {
             glAccountList =  [select Name, id, c2g__ReportingCode__c from c2g__codaGeneralLedgerAccount__c where c2g__ReportingCode__c<'40000' order by c2g__ReportingCode__c ];
        }
        else if(ffreport!=null && ffreport.equals('Account')) {
             glAccountList =  [select Name, id, c2g__ReportingCode__c from c2g__codaGeneralLedgerAccount__c where c2g__ReportingCode__c>'40000' and c2g__ReportingCode__c<'60000' order by c2g__ReportingCode__c ];
        }
        for (c2g__codaGeneralLedgerAccount__c s : glAccountList) {
            concatenatedStr = s.c2g__ReportingCode__c + ' ' + s.Name;
            glaccounts.add(new SelectOption(concatenatedStr,concatenatedStr));
        }
        return glaccounts;
        
    }
   
   //ESB-85 nsk: The following method is no longer being used on the VF page. The query params are passed directly to the ESB service for building the queries
    public void submit(){

/*       
       //default dev server
        String serverName = 'ec2-54-225-75-22.compute-1.amazonaws.com';
   
        //updated by VG 10/08/2012
        //Get the ORG id of the logged in user.
        String prodSalesforceORGId = UserInfo.getOrganizationId();
  
        System.debug('Users Salesforce ORG ID = ' +  prodSalesforceORGId);
        //If orgID <> to PROD ORG ID (00D300000001WOu) then use the dev servername else use the PROD servername.
        if (prodSalesforceORGId <> null && prodSalesforceORGId.equals('00D300000001WOuEAM')) {
        System.debug('Salesforce ORG is PROD ORG= ' +  prodSalesforceORGId);
        //prod server
        serverName = 'ec2-50-17-212-213.compute-1.amazonaws.com';
        }
        //dev server
        else serverName = 'ec2-54-225-75-22.compute-1.amazonaws.com';
         //remove later!
        serverName = 'ec2-50-17-212-213.compute-1.amazonaws.com';
  
        if(anaplanmodule!=null && anaplanmodule.equals('P&L Financial Force Upload')){
            if(month.equals('All')){
                this.linkvalue='https://' + serverName + '/feed/FF_Anaplan_Integration/PL_Annual_Upload/PL_Annual_Upload_Pipleine/Output1?sn.content_type=text/html'
                + '&YEAR=' + getYear()
                + '&YEAR_QUERY=' + getYearQuery()
                + '&GLACCOUNT_QUERY='+ getGLAccountQuery()
                + '&COMPANY_QUERY=' + getCompanyQuery()
                + '&DIMENSION1_QUERY=' + getDimension1Query()
                + '&DIMENSION2_QUERY=' + getDimension2Query()
                + '&DIMENSION3_QUERY=' + getDimension3Query()
                + '&DIMENSION4_QUERY=' + getDimension4Query()
                + '&USERNAME=' + UserInfo.getUserName();
              
            } else {
                this.linkvalue='https://' + serverName + '/feed/FF_Anaplan_Integration/PL_Monthly_Upload/PL_Monthly_Upload_Pipeline/Output1?sn.content_type=text/html'
                + '&YEAR=' + getYear()
                + '&YEAR_QUERY=' + getYearQuery()
                + '&PERIOD=' + getPeriod()
                + '&GLACCOUNT_QUERY='+ getGLAccountQuery()
                + '&COMPANY_QUERY=' + getCompanyQuery()
                + '&DIMENSION1_QUERY=' + getDimension1Query()
                + '&DIMENSION2_QUERY=' + getDimension2Query()
                + '&DIMENSION3_QUERY=' + getDimension3Query()
                + '&DIMENSION4_QUERY=' + getDimension4Query()
                + '&USERNAME=' + UserInfo.getUserName();
         
            }
         }
      
         else if(anaplanmodule!=null && anaplanmodule.equals('Balance Sheet Financial Force Upload')){
            if(month.equals('All')){
                this.linkvalue='https://' + serverName + '/feed/FF_Anaplan_Integration/BS_Annual_Upload/BS_Annual_Upload_Pipeline/Output1?sn.content_type=text/html'
                + '&YEAR=' + getYear()
                + '&YEAR_QUERY=' + getYearQuery()
                + '&GLACCOUNT_QUERY='+ getGLAccountQuery()
                + '&COMPANY_QUERY=' + getCompanyQuery()
                + '&DIMENSION1_QUERY=' + getDimension1Query()
                + '&DIMENSION3_QUERY=' + getDimension3Query()
                + '&USERNAME=' + UserInfo.getUserName();
               
            } else {
                this.linkvalue='https://' + serverName + '/feed/FF_Anaplan_Integration/BS_Monthly_Upload/BS_Monthly_Upload_Pipeline/Output1?sn.content_type=text/html'+
                + '&YEAR=' + getYear()
                + '&YEAR_QUERY=' + getYearQuery()
                + '&PERIOD=' + getPeriod()
                + '&GLACCOUNT_QUERY='+ getGLAccountQuery()
                + '&COMPANY_QUERY=' + getCompanyQuery()
                + '&DIMENSION1_QUERY=' + getDimension1Query()
                + '&DIMENSION3_QUERY=' + getDimension3Query()
                + '&USERNAME=' + UserInfo.getUserName();
            }
         }
         else if(anaplanmodule!=null && anaplanmodule.equals('GL Account by Vendor')){
                this.linkvalue='https://' + serverName + '/feed/FF_Anaplan_Integration/Account_Monthly_Upload/Account_Monthly_Upload_Pipeline/Output1?sn.content_type=text/html'+
                + '&YEAR=' + getYear()
                + '&YEAR_QUERY=' + getYearQuery()
                + '&PERIOD=' + getPeriod()
                + '&GLACCOUNT_QUERY='+ getGLAccountQuery()
                + '&COMPANY_QUERY=' + getCompanyQuery()
                + '&DIMENSION1_QUERY=' + getDimension1Query()
                + '&DIMENSION2_QUERY=' + getDimension2Query()
                + '&DIMENSION3_QUERY=' + getDimension3Query()
                + '&DIMENSION4_QUERY=' + getDimension4Query()
                + '&USERNAME=' + UserInfo.getUserName();
         }
        
        if(ffreport.equals('P&L')){
            this.dlValue = 'https://' + serverName 
                + '/feed/FF_Anaplan_Integration/Data_Validation/PL_Monthly_Upload_Validation/Data_Validation_PL_Monthly_Pipeline'
                + '/Output1?sn.content_type=text/html'+
                + '&YEAR=' + getYear()
                + '&PERIOD=' + getPeriod()
                + '&COMPANY_QUERY=' + getCompanyQuery()
                + '&COMPANY=' + getCompany()
                + '&USERNAME=' + UserInfo.getUserName()
                + '&DEPARTMENT=' + getDepartmentParam()
                + '&DEPARTMENT_QUERY=' + getDepartmentQueryParam()
                
                ;
            
            
        } else  if(ffreport.equals('Balance Sheet')){
            this.dlValue = 'https://' + serverName 
                + '/feed/FF_Anaplan_Integration/Data_Validation/BS_Annual_Upload_Validation/Data_Validation_BS_Monthly_Pipeline'
                + '/Output1?sn.content_type=text/html'+
                + '&YEAR=' + getYear()
                + '&PERIOD=' + getPeriod()
                + '&COMPANY_QUERY=' + getCompanyQuery()
                + '&COMPANY=' + getCompany()
                + '&USERNAME=' + UserInfo.getUserName();
        
        
        } else  if(ffreport.equals('Account')){
            this.dlValue = 'https://' + serverName
                + '/feed/FF_Anaplan_Integration/Data_Validation/Account_Monthly_Upload_Validation/Data_Validation_Account_Monthly_Pipeline'
                + '/Output1?sn.content_type=text/html'
                + '&YEAR=' + getYear()
                + '&PERIOD=' + getPeriod()
                + '&USERNAME=' + UserInfo.getUserName()
                + '&DIMENSION3_QUERY=' + getDimension3Query()
                + '&LOCATION=' + getLocationParam();
        
        } 

*/        
        
     }

//ESB-85 nsk: Commeting out the following methods as they are no longer used in the submit method     
  /*   
   private String getDepartmentQueryParam(){
   
   String deptQueryParam = null;
   
   if(department.equals('All')){
        deptQueryParam = '';
   
   }else{
       String[] paramArr = department.split('-');
       deptQueryParam = 'and c2g__Dimension1__r.c2g__ReportingCode__c %3D' + '\''+ paramArr[0] + '\'';
   }
   return deptQueryParam;
   
   
   }
   
   private String getDepartmentParam(){
   
   String deptParam = null;
   
   if(department.equals('All')){
        deptParam = 'Pandora US';
   
   }else{
   
       String[] paramArr = department.split('-');
       
       String param1 = paramArr[1].replace('&', '%26');
               deptParam = paramArr[0] + ' ' + param1;
   }
   return deptParam;
}
  private String getLocationParam(){
  
   return'Consolidated';
  
  
  }
  private String getPeriod(){
 
      String period = null;
      if(!month.equals('All')){
          String[] monthArr = month.split('-');
          period = monthArr[0];
         }
         else {
          period = '';
         }
 
      return period;
  }
 
  private String getYearQuery(){
     
      String yearQuery =null;
      if(ffreport.equals('Account')){
         yearQuery = 'and c2g__Transaction__r.c2g__Period__r.c2g__YearName__r.Name  %3D \'' + ffyear + '\'';
      
      }else{
         yearQuery = 'and c2g__Year__r.Name %3D \'' + ffyear + '\'';
      }
      return yearQuery;
  }
 
  private String getYear(){
 
      return ffyear;
  }
    
   
    private String getDimension1Query(){
   
       String param = null;
       if(department.equals('All')){
               param = '';
          
           } else{
               String[] paramArr = department.split('-');
               String code = paramArr[0];
               if(code.equals('4099')){
               param = ' and c2g__Dimension1__r.c2g__ReportingCode__c IN (\'4099\',\'\')';
               } else{
              
               param = ' and c2g__Dimension1__r.c2g__ReportingCode__c %3D\'' + code + '\'';
              
               }
              
       }
       return param;
    }
     
    private String getDimension2Query(){
   
       String param = null;
       if(product.equals('All')){
               param = '';
          
       } else{
               String[] paramArr = product.split('-');
               String code = paramArr[0];
               param = ' and c2g__Dimension2__r.c2g__ReportingCode__c %3D\'' + code + '\'';
             
       }
       return param;
    }
   
    private String getDimension3Query(){
   
       String param = null;
       if(location.equals('All')){
       
               param = '';
               //param = 'Consolidated';
          
       } else{
               String[] paramArr = location.split('-');
               String code = paramArr[0];
               param = ' and c2g__Dimension3__r.c2g__ReportingCode__c %3D\'' + code + '\'';
             
       }
       return param;
    }
   
    private String getDimension4Query(){
   
       String param = null;
       if(dimension4.equals('All')){
       
               param = '';
               //param = 'Consolidated';
          
       } else{
               String[] paramArr = dimension4.split('-');
               String code = paramArr[0];
               param = ' and c2g__Dimension4__r.c2g__ReportingCode__c %3D\'' + code + '\'';
             
       }
       return param;
    } 
   
    private String getCompanyQuery(){
   
        String compParam = null;
        if(company.equals('Pandora Consolidated')){
            //when consolidated do not query by company parameter
            //compParam = '%Pandora%';
            compParam = ' ';
        }else{
   
            String[] splitArr= company.split('-');
            compParam = ' and c2g__OwnerCompany__r.Name like \''+ splitArr[0]+'%' + '\'';
        }
        return compParam;
   
    }
   
    private String getGLAccountQuery(){
   
    String acctParam = null;
    if(glaccount.equals('All')){
           
       if(ffreport.equals('P&L')){
            //returns >39,999 for all PL
            acctParam =  'and GeneralLedgerAccountReportingCode__c >\'39999\' and GeneralLedgerAccountReportingCode__c !%3D \'99999\' and GeneralLedgerAccountReportingCode__c !%3D \'99998\'';
       
        }else if(ffreport.equals('Balance Sheet')){
            //BS account
            //returns  <40,000 for all BS
            acctParam =  'and GeneralLedgerAccountReportingCode__c  <\'40000\'';
        }else if(ffreport.equals('Account')){
            //Account upload
           acctParam =  'and c2g__GeneralLedgerAccount__r.c2g__ReportingCode__c > \'39999\'  and c2g__GeneralLedgerAccount__r.c2g__ReportingCode__c < \'80000\' ';
        }
       
    }else{
        String[] splitArr = glaccount.split(' ');
        if(ffreport.equals('P&L') || ffreport.equals('Balance Sheet') ){  
            acctParam = ' and GeneralLedgerAccountReportingCode__c %3D\''+ splitArr[0] + '\'';
        } else if (ffreport.equals('Account')){
            acctParam = ' and c2g__GeneralLedgerAccount__r.c2g__ReportingCode__c %3D\''+ splitArr[0] + '\'';
        }
   
    }
    return acctParam;
    }

*/
   
    public void setLinkvalue(String p)
    {
        this.linkvalue = p;
   
    }
   
    public String getLinkvalue()
    {
        return this.linkvalue;
   
    }
    public void setDlValue(String p)
    {
        this.dlValue = p;
   
    }
   
    public String getDlValue()
    {
        return this.dlValue;
   
    }

   
/*
01/28/14 nsk: Adding new method to save the form params in the new format (to support ESB service) and generate the link in the new format.
ref:- https://wiki.savagebeast.com/display/ENIT/TRD+FFANAPLAN+Anaplan+to+FF+Snap+Migration#
*/
    public void saveFormParams(){
       
      //default dev server
       //String serverName = 'crossbow-stage1.pandora.com';
       String serverName = stageURL.NewURL__c;
       //String flowPath = '/ffanaplan/ff_form1_submit';
       String flowPath = '/route/ffanaplan/import';
         
       //Get the ORG id of the logged in user.
       String prodSalesforceORGId = UserInfo.getOrganizationId();
  
       System.debug('Users Salesforce ORG ID = ' +  prodSalesforceORGId);
       if (prodSalesforceORGId <> null && prodSalesforceORGId.equals('00D300000001WOuEAM')) {
           System.debug('Salesforce ORG is PROD ORG= ' +  prodSalesforceORGId);
           //prod server
           //serverName = 'crossbow1.pandora.com';
           serverName = prodURL.NewURL__c;
       }
        
       this.linkvalue='https://' + serverName + flowPath + '?'
               + 'REPORT=' + EncodingUtil.urlEncode(ffreport, 'UTF-8')
               + '&YEAR=' + ffyear
               + '&MONTH=' + month
               + '&COMPANY=' + EncodingUtil.urlEncode(company, 'UTF-8')
               + '&MODULE=' + EncodingUtil.urlEncode(anaplanmodule, 'UTF-8')
               + '&MODEL=' + EncodingUtil.urlEncode(model, 'UTF-8')
               + '&DIMENSION1=' + EncodingUtil.urlEncode(department, 'UTF-8')
               + '&DIMENSION2=' + EncodingUtil.urlEncode(product, 'UTF-8')
               + '&DIMENSION3=' + EncodingUtil.urlEncode(location, 'UTF-8')
               + '&GLACCOUNT='+ EncodingUtil.urlEncode(glaccount, 'UTF-8')
               + '&USERNAME=' + UserInfo.getUserName()
               + '&REPORT_TYPE=' + EncodingUtil.urlEncode(anaplanmodule, 'UTF-8');         

       system.debug(' url value = '+this.linkvalue);

       if(dimension4!=null && dimension4!='')
              this.linkvalue = this.linkvalue + + '&DIMENSION4=' + EncodingUtil.urlEncode(dimension4, 'UTF-8');

       system.debug(' url value = '+this.linkvalue);
        
     } 

//ESB-85 nsk: The following method will make a callout to the ESB service and pass the input params selected on the VF page    
    public PageReference runUploadPipeline(){
        // Instantiate a new http object
        Http h = new Http();
        HttpResponse res = null;
        PageReference pageRef = new PageReference(ApexPages.currentPage().getUrl());
        
        // Instantiate a new HTTP request, specify the method (GET/POST...) as well as the endpoint
        HttpRequest req = new HttpRequest();
        req.setEndpoint(this.linkvalue);
        req.setMethod('GET');

        // Send the request, and return a response
        try{
            res = h.send(req);
            pageRef.setRedirect(true);
            return pageRef;         
        }
        catch(CalloutException e)
        {
            system.debug('CalloutException = '+e);
            ApexPages.Message reqMsg = new ApexPages.Message(ApexPages.Severity.ERROR, 'Error: The request could not be processed. Please try again or report this error. -- '+e);
            ApexPages.addMessage(reqMsg);
            //pageRef.setRedirect(true);
            return pageRef;         
        }
        return null;
    }
   
   
}