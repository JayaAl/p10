/*
Description: Test class
ESB-85 nsk: This test class is for unit testing and adding code coverage for  Snaplogic_Anaplan_Controller_Extension2 class. The controller class gathers input params selected by the user on the corresponding
VF page and passes the URL Encoded values to the ESB service thru a http post

*/
@isTest(SeeAllData=true)
public with sharing class Snaplogic_Anaplan_Extension2Test {


    private static testMethod void Snaplogic_Ananplan_ExtensionTest()
    {

        List<SelectOption> ffyears;
        String ffyear;
       
        List<SelectOption> months;
        String month;
                  
        String company;
        List<SelectOption> companies;
       
        List<SelectOption> ffreports;
        String ffReport;
       
        List<SelectOption> anaplanmodules;
        String anaplanmodule;
       
        String glaccount;
        List<SelectOption> glaccounts;
       
        String department;
        List<SelectOption> departments;
       
        String product;
        List<SelectOption> products;
       
        String location;
        List<SelectOption> locations;
       
        String dimension4;
        List<SelectOption> dimension4s;
       
        String linkvalue;
       
        String dlValue;
       
        String model;
        String workspace;
    
        Snaplogic_Anaplan_Controller_Extension2 snapExt = new Snaplogic_Anaplan_Controller_Extension2 (new ApexPages.StandardController(new c2g__codaBudget__c()));
        
        PageReference exportPage = new PageReference( '/apex/snaplogic_anaplan_export' );
        Test.setCurrentPage(exportPage);
        
        snapExt.setModel('Snaplogic Model');
        model = snapExt.getModel();
        workspace = snapExt.getWorkspace();
        snapExt.setMonth('001-Jan');
        month = snapExt.getMonth();
        months = snapExt.getMonths();
        snapExt.setFfyear('2014');
        ffyear = snapExt.getFfyear();
        ffyears = snapExt.getFfyears();
        snapExt.setDepartment('All');
        department = snapExt.getdepartment();
        departments = snapExt.getdepartments();
        snapExt.setProduct('All');
        product = snapExt.getProduct();
        products = snapExt.getProducts();
        snapExt.setLocation('All');
        location = snapExt.getLocation();
        locations = snapExt.getLocations();
        snapExt.setDimension4('All');
        dimension4 = snapExt.getdimension4();
        dimension4s = snapExt.getdimension4s();
        snapExt.setCompany('Pandora Consolidated');
        company = snapExt.getcompany();
        companies = snapExt.getcompanies();
        
        snapExt.setFfreport('P&L');
        ffReport = snapExt.getffReport();
        ffReports = snapExt.getffReports();

        snapExt.setFfreport('Balance Sheet');
        ffReport = snapExt.getffReport();
        ffReports = snapExt.getffReports();

        snapExt.setFfreport('Account');
        ffReport = snapExt.getffReport();
        ffReports = snapExt.getffReports();
       
        snapExt.setAnaplanmodule('P&L Financial Force Upload');
        anaplanmodule = snapExt.getanaplanmodule();
        anaplanmodules = snapExt.getanaplanmodules();
        snapExt.setGlaccount('All');
        glaccount = snapExt.getglaccount();
        glaccounts = snapExt.getglaccounts();
        snapExt.submit();
        snapExt.saveFormParams();
        
        //Methods defined as TestMethod do not support Web service callouts
        //snapExt.runUploadPipeline();
    }    

}