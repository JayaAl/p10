@isTest
private class SplitDetailcStatsTT {

   static testMethod void testTrigger() {
      try {
          Split_Detail__c o = new Split_Detail__c();
          insert o;

          System.assertNotEquals(null, o);
      }
      catch(Exception e) {
          List<Split_Detail__c> l = [SELECT Id from Split_Detail__c LIMIT 1];
          update l;
          System.assertNotEquals(null, l);
      }
   }
}