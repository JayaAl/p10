global class StateCountyTreeCompCtrlr{

	@RemoteAction
	global static List<ATG_State__c> getStates(){

		 Map<Id,ATG_State__c> statMap = new Map<Id,ATG_State__c>([Select id,name,ATG_State_Code__c from ATG_State__c]);
		 return statMap.values();
	}


	@RemoteAction
	global static List<ATG_County__c> getCountyLst(Id stateId){
		  List<ATG_County__c> countyLst = [Select id,name,ATG_State__c,ATG_County_Name__c from ATG_County__c where ATG_State__c = : stateId];
		  return countyLst;
	}
}