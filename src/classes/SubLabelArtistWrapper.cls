public class SubLabelArtistWrapper{

    @AuraEnabled
    public Id acctId {get;set;}
    
    @AuraEnabled
    public Set<Account> acctSet {get;set;}
    
}