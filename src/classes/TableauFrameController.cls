/*
 * Class Name: TableauFrameController
 * Author: Srikanth Nelli
 * Desc: Controller class to support xxxxTableauFrame VF pages. This controller retrieves the correct Tableau report URL based on the location of the frame. 
 * Date: 07/30/2014
 */
public class TableauFrameController {
    //Declarations
    public String idParam {get;set;}
    public String id18 {get;set;}    
    public String objectName {get;set;}
    public String urlString {get;set;}
    boolean prodOrg=false;
    String reportName;
    private final Opportunity opptyRec;
    private final Account acctRec;
    private final User userRec;
    private final Id idVal;
    
    // var added to only display the Tableau iframe once users click the button
    // Author: 	Casey Grooms
    // Date:	2014-12-18
    public boolean renderTableau{get;set;}
    public void toggleTableauFrame(){
        if(renderTableau==TRUE){
            renderTableau=FALSE;
        } else {
            renderTableau=TRUE;
        }
    }
    
    //constructor
    public TableauFrameController(ApexPages.StandardController controller){

        //Get the ORG id of the logged in user.
        String salesforceORGId = UserInfo.getOrganizationId();        

        //Determine object name based on the URL ID param
        if(ApexPages.currentPage().getParameters().get('id')!=null)
            idParam = ApexPages.currentPage().getParameters().get('id');
        else
            idParam = UserInfo.getUserId();
        
        if(idParam!=null && idParam!=''){      
            system.debug('idParam = '+idParam);
            idVal = Id.valueOf(idParam);
            objectName = idVal.getSObjectType().getDescribe().getName();
            //objectName = findObjectAPIName(idParam);
        }
        
        //Determine org and build URL string based on the objectName and links from custom settings
        if (salesforceORGId <> null && salesforceORGId.equals('00D300000001WOuEAM')) {
           System.debug('This is PROD ORG= ' +  salesforceORGId );
           prodOrg = true;
        } 

        //check object name
        if(objectName=='Opportunity'){
            this.opptyRec = (Opportunity)controller.getRecord();        
            System.debug('opptyRec = '+opptyRec.id); 
            id18 = opptyRec.id; //[Select X18_Character_ID__c From Opportunity Where Id = :opptyRec.Id].X18_Character_ID__c;
        }
        else if(objectName=='Account'){
            this.acctRec = (Account)controller.getRecord();        
            id18 = acctRec.id;//[Select X18_Character_ID__c From Account Where Id = :acctRec.Id].X18_Character_ID__c;
        }
        else if(objectName=='User'){
            id18 = idParam;//[Select X18_Character_ID_User__c From User Where Id = :userRec.Id].X18_Character_ID_User__c;
        }

        if(prodOrg)
        {
            //check object name
            if(objectName=='Opportunity'){
                reportName = 'OpportunityProdReport';
            }
            else if(objectName=='Account'){
                reportName = 'AccountProdReport';
            }
            else if(objectName=='User'){
                reportName = 'UserProdReport';
            }
        }
        else //point to QA URL's
        {
            //check object name
            if(objectName=='Opportunity'){
                reportName = 'OpportunityStageReport';
            }
            else if(objectName=='Account'){
                reportName = 'AccountStageReport';
            }
            else if(objectName=='User'){
                reportName = 'UserStageReport';
            }
        }

        try{
            Custom_Constants__c reportString = Custom_Constants__c.getValues(reportName);
            if(reportString.URL__c!=null && reportString.URL__c!='')
                urlString = reportString.URL__c + id18 + '&:embed=y&:display_count=no';
            else{
                ApexPages.Message msg = new ApexPages.Message(ApexPages.Severity.INFO, 'Report link is missing');
                ApexPages.addMessage(msg);
                urlString = '';
            }
        }catch(Exception e){
            ApexPages.Message msg = new ApexPages.Message(ApexPages.Severity.INFO, 'Report link error');
            ApexPages.addMessage(msg);
            urlString = '';
        }

    }//end constructor
    
    /*
        Method to determine the object name based on the ID param 
        This is no longer needed and can be deleted.   
    */
    /*public string findObjectAPIName( String recordId ){
        if(recordId == null)
            return null;
        String objectAPIName = '';
        String keyPrefix = recordId.substring(0,3);
         for( Schema.SObjectType obj : Schema.getGlobalDescribe().Values() ){
              String prefix = obj.getDescribe().getKeyPrefix();
               if(prefix == keyPrefix){
                         objectAPIName = obj.getDescribe().getName();
                          break;
                }
         }
         return objectAPIName;
    }
    */
}