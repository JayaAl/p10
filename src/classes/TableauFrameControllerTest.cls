@isTest(seeAllData=true)
private class TableauFrameControllerTest { 

    //Method
    static testMethod void unitTest() { 
    	
        Account testAccount = UTIL_TestUtil.createAccount();
        Opportunity testOpp = UTIL_TestUtil.createOpportunity(testAccount.Id);
        
        
        
        //Test for Account
        ApexPages.Standardcontroller sc2 = new ApexPages.Standardcontroller(testAccount);
		ApexPages.currentPage().getParameters().put('id',testAccount.id);
        TableauFrameController testController2 = new TableauFrameController(sc2);
        
        PageReference pageRef = Page.opptyTableauFrame;
        Test.setCurrentPage(pageRef);
        //Test for Opportunity
        ApexPages.Standardcontroller sc = new ApexPages.Standardcontroller(testOpp);
		ApexPages.currentPage().getParameters().put('id',testOpp.id);
        TableauFrameController testController = new TableauFrameController(sc);
        
        PageReference pageRef1 = Page.userTableauFrame;
        Test.setCurrentPage(pageRef1);
        //Test for User
        ApexPages.Standardcontroller sc3 = new ApexPages.Standardcontroller(new User(Id=UserInfo.getUserId()));
		ApexPages.currentPage().getParameters().put('id',UserInfo.getUserId());
        TableauFrameController testController3 = new TableauFrameController(sc3);
        ApexPages.currentPage().getParameters().put('id',null);
        testController3 = new TableauFrameController(sc3);
    } 
}