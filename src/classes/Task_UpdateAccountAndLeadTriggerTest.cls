@isTest
public class Task_UpdateAccountAndLeadTriggerTest {

    public static testMethod void Task_UpdateAccountAndLeadTriggerTestMethod() {
        Lead testLead = UTIL_TestUtil.createLead();
        Account testAccount = UTIL_TestUtil.createAccount();
        Task testTask1 = UTIL_TestUtil.generateTask();
        testTask1.whoid = testLead.Id;
        insert testTask1;
        Lead assertTestLead = [Select last_updated_activity__c from Lead where Id=: testLead.Id];
        System.assertEquals(assertTestLead.last_updated_activity__c , system.today());

        Task testTask2 = UTIL_TestUtil.generateTask();
        testTask2.whatid = testAccount.Id;
        insert testTask2;
        
        Account assertTestAccount = [Select last_updated_activity__c from Account where Id=: testAccount.Id];
        System.assertEquals(assertTestAccount.last_updated_activity__c , system.today());
    
    
    }
}