/**
 * This class contains unit tests for validating the behavior of Apex classes
 * and triggers.
 *
 * Unit tests are class methods that verify whether a particular piece
 * of code is working properly. Unit test methods take no arguments,
 * commit no data to the database, and are flagged with the testMethod
 * keyword in the method definition.
 *
 * All test methods in an organization are executed whenever Apex code is deployed
 * to a production organization to confirm correctness, ensure code
 * coverage, and prevent regressions. All Apex classes are
 * required to have at least 75% code coverage in order to be deployed
 * to a production organization. In addition, all triggers must have some code coverage.
 * 
 * The @isTest class annotation indicates this class only contains test
 * methods. Classes defined with the @isTest annotation do not count against
 * the organization size limit for all Apex scripts.
 *
 * See the Apex Language Reference for more information about Testing and Code Coverage.
 */
@isTest
private class TestAgreementTriggers {
    static testMethod void testSetContractDate() {
        Case caze = new Case();
        insert caze;
        
        echosign_dev1__SIGN_Agreement__c agreement = new echosign_dev1__SIGN_Agreement__c(Case__c = caze.Id, Name = 'test');
        insert agreement;
        
        agreement.echosign_dev1__Status__c = 'Waiting for Counter-Signature';
        update agreement;
        
        agreement.echosign_dev1__Status__c = 'Signed';
        update agreement;
    }
    
    static testMethod void testSetRecipientsAttachments() {
        Account account = new Account(Name = 'test');
        insert account;
        
        Contact contact = new Contact(AccountId = account.Id, LastName = 'test');
        insert contact;
        
        Opportunity opp = new Opportunity(Name = 'test', AccountId = account.Id, StageName = 'New', CloseDate = Date.today());
        insert opp;
        
        OpportunityContactRole role = new OpportunityContactRole(OpportunityId = opp.Id, ContactId = contact.Id, IsPrimary = true);
        insert role;
        
        Case caze = new Case(Opportunity__c = opp.Id);
        insert caze;
        
        ContentVersion content = new ContentVersion();
        content.VersionData = Blob.valueOf('some content');
        content.PathOnClient = 'test.pdf';
        content.Opportunity__c = opp.Id;
        content.Folder__c = 'Insertion Orders';
        content.FirstPublishLocationId = '05840000000Cdp4';
        insert content;
        
        content = [SELECT ContentDocumentId from ContentVersion where Id = :content.Id];
        
        echosign_dev1__SIGN_Agreement__c agreement = new echosign_dev1__SIGN_Agreement__c(Case__c = caze.Id, Name = 'test');
        insert agreement;
    }
    
    private static Blob body = Blob.valueOf('test');

    static testMethod void testSigned() {        
        echosign_dev1__SIGN_Agreement__c agreement = new echosign_dev1__SIGN_Agreement__c(Name='test');
        insert agreement;
        
        Attachment attachment = new Attachment(Name='Agreement - signed.pdf',Description='Test description',ParentId=agreement.Id,Body=body);
        //insert attachment;
        
        Attachment attachment2 = new Attachment(Name='Agreement - signed.pdf',Description='Test description',ParentId=agreement.Id,Body=body);
        //insert attachment2;
        
        /*ContentVersion content = [SELECT Account__c, Contract_Type__c, VersionData, Description, PathOnClient, Date_Contract_Executed__c, FirstPublishLocationId 
            from ContentVersion where Title = :agreement.Name];
        System.assertEquals(account.Id, content.Account__c);
        System.assertEquals(agreement.Contract_Type__c, content.Contract_Type__c);
        System.assertEquals(attachment.Description, content.Description);
        System.assertEquals(attachment.Body, content.VersionData);
        System.assertEquals(attachment.Name, content.PathOnClient);
        System.assertEquals(agreement.Date_Contract_Executed__c, content.Date_Contract_Executed__c);
        System.assertEquals(nyseLibraryId, content.FirstPublishLocationId);*/
    }
}