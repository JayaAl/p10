/**
 * @name: TestBatchAccTeamdOwnerDupcleanup 
 * @desc: Testclass for both BatchAccTeamdOwnerDupcleanup 
 * @author: Priyanka Ghanta
 * @date: 07/29/2015
 */

@isTest
private class TestBatchAccTeamdOwnerDupcleanup {
    static testMethod void batchAccTeamDupTest() {
        Account a = new Account(Name = 'Test Account', Type='Advertiser',Pandora_Promotion__c = 'yyy;xxx');
        a.AccountSource = 'mytestvalue1';   
        insert a; 
        a.AccountSource = 'mytestvalue2';
        update a;
        Account a1 = new Account(Name = 'Test Account', Type='Advertiser',Pandora_Promotion__c = '');
        a1.AccountSource = 'mytestvalue1';   
        insert a1; 
        a1.AccountSource = 'mytestvalue2';
        update a1;
        Contact conObj = UTIL_TestUtil.generateContact(a.ID);       
        String picklistval = 'mytestvalue';
        conObj.Hidden_Lead_Source__c = picklistval;
        conObj.Pandora_Promotion__c='zzz;';
        insert conObj;
        conobj.Pandora_Promotion__c ='sss';
        update conObj;
        SchedulableContext sc;
        
        Test.startTest();
        BatchAccTeamdOwnerDupcleanup  btd = new BatchAccTeamdOwnerDupcleanup ();
        Database.executeBatch(btd);
        btd.execute(sc);
        Test.stopTest();
    }
}