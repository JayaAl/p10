/**
 * @name: TestBatchCQForcastedRevenue  
 * @desc: TestClass for BatchCQForcastedRevenue 
 * @author: Priyanka Ghanta
 * @date: 09/02/2015
 */
@isTest
public class TestBatchCQForcastedRevenue   {

    public static testMethod void testCQForcastedRevenue () {
    
        Account a = new Account(Name = 'Test Account', Type='Advertiser');
        insert a;
        
        Contact conObj = UTIL_TestUtil.generateContact(a.id);
        insert conObj;
        
        Opportunity o1 = new Opportunity(Name = 'Test Opportunity1', AccountId = a.Id, Primary_Billing_Contact__c =conObj.Id, 
                                         StageName = 'Negotiation', CloseDate = System.Today(),Confirm_direct_relationship__c=true,
                                         Deal_Id__c = '1234',Industry_Sub_Category__c='Political Campaign Marketing',Type='Advertiser');                                      
        insert o1;
        
        IO_Detail__c ioD = new IO_Detail__c(Opportunity__c = o1.Id,Approval_Type__c = 'IO', Paperwork_Origin__c = 'test',
                                             Contact_Order_Approval_Status__c = 'XXX',Revenue_IO_Approval__c = 'YYY',
                                             Legal_Approval_Status__c = 'ZZZ',IO_Signature_Status__c = 'Pandora Paperwork - no changes'  );
        insert ioD;      
  
        
        Product2 prod = new Product2(Name = 'Web',Family = 'Hardware',CanUseQuantitySchedule=true,CanUseRevenueSchedule=true);
        insert prod;
        
        Product2 prod1 = new Product2(Name = 'Mobile',Family = 'Software',CanUseQuantitySchedule=true,CanUseRevenueSchedule=true);
        insert prod1;
 
        // Get standard price book ID.
        // This is available irrespective of the state of SeeAllData.
        Id standard = Test.getStandardPricebookId();
     
        PricebookEntry standardPrice = new PricebookEntry(Pricebook2Id = standard, Product2Id = prod.Id,UnitPrice = 10000, IsActive = true);
        insert standardPrice;
        
        // Create a custom price book
        Pricebook2 customPB = new Pricebook2(Name='Custom Pricebook', isActive=true);
        insert customPB;
  
        // 2. Insert a price book entry with a custom price.
        PricebookEntry customPrice = new PricebookEntry(Pricebook2Id = standard, Product2Id = prod1.Id,UnitPrice = 12000, IsActive = true);
        insert customPrice;      
      
        OpportunityLineItem oli = new OpportunityLineItem(OpportunityId = o1.id,PricebookEntryId=customPrice .Id , Quantity =  1, TotalPrice=20);
        insert oli;
        Test.startTest();
        List<OpportunityLineItemSchedule> scheduleLst = new List<OpportunityLineItemSchedule>();
        
        OpportunityLineItemSchedule olis = new OpportunityLineItemSchedule (OpportunityLineItemId =oli.Id,ScheduleDate = system.Today(),
                                                                            Type = 'Revenue',Revenue=100);
      // insert olis;
      scheduleLst.add(olis);
       OpportunityLineItemSchedule olis1 = new OpportunityLineItemSchedule (OpportunityLineItemId =oli.Id,ScheduleDate = system.Today(),
                                                                            Type = 'Revenue',Revenue=200);
        scheduleLst.add(olis1);
      //insert olis1;
        
        insert scheduleLst;
        
        PriceBookEntry pbe = [select id from pricebookentry where name = 'Web' limit 1];
        
        
        
           BatchCQForcastedRevenue cQFR = new BatchCQForcastedRevenue();
        Database.executeBatch(cQFR);
        String sch = '0 0 23 * * ?';
            system.schedule('Test check', sch, cQFR ); 
        Test.stopTest();
    }
  
 }