@isTest
public class TestClosedWonOpptiesByClientExt {
       public static testmethod void  ClosedWonOpptiesByClientExt() {
      Account a = new Account(Name = 'Test Account', Type='Advertiser');
        insert a;        
        Contact conObj = UTIL_TestUtil.generateContact(a.id);
        insert conObj;
        
        Opportunity o1 = new Opportunity(Name = 'Test Opportunity1', AccountId = a.Id, Primary_Billing_Contact__c =conObj.Id,Amount = 599999, 
                                         StageName = 'Closed Won', CloseDate = System.Today(),Confirm_direct_relationship__c=true,
                                         Deal_Id__c = '1234',Industry_Sub_Category__c='Political Campaign Marketing',Type='Advertiser');                                      
        insert o1;
        
        Case cas = new Case(AccountId = a.Id,Status = 'Pending Review',Proposal_Attached__c = 'yes',Yield_Approval_Request_Type__c ='Below line item minimum spend');
        insert cas;
        
        Upfront__c upf = new Upfront__c(Name='Test',Amount_Spent__c=100);
        insert upf;
        test.starttest();       
        Test.setCurrentPage(Page.CloseWonOpptiesByClient);
        ApexPages.StandardController sc = new ApexPages.Standardcontroller(cas);        
        ClosedWonOpptiesByClientExt clsext =new ClosedWonOpptiesByClientExt(sc);
        test.stoptest();               
    }
}