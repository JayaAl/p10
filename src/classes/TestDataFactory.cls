/*============================================= 
Author : Menaka Nanjappan
Created: 01/06/2017
Purpose: Keep all the common test data in this class and use it anywhere in the test class.  
================================================*/
public class TestDataFactory {

	public TestDataFactory() {}



	 public static List<User> createUsers(Integer totalCount)
    {
        Profile profile = [Select Id from Profile Where Name='Standard User' Limit 1];
        List<user> userList = new List<user>();
        for(Integer i=1; i<= totalCount; i++){
        	   User insertUser = new User(EmailEncodingKey='UTF-8',
        	   LastName='TestUser LN'+i,
        	   LanguageLocaleKey='en_US',
        	   LocaleSidKey='en_US',
        	   TimeZoneSidKey='America/Los_Angeles'
        	   );
        insertUser.function_group__c='test function';
        insertUser.Cost_Center__c='3020';
        insertUser.WD_Location__c='test Location';
        insertUser.Title='Test User';
        insertUser.Is_Manager__c=True;
        insertUser.ProfileId=profile.id;
        insertUser.Email='testemail@testemail.com'+i;
        insertUser.Username='testemail@testemail.com.pandora'+i;
        insertUser.Alias='test'+i;
        userList.add(insertUser);
        }
        return userList;
    }
}