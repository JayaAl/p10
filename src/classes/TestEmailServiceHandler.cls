/*
** @name: TestEmailServiceHandler 
 * @desc: TestClass for EmailServiceHandler 
 * @author: Priyanka Ghanta
 * @date: 06/16/2015 
*/
@isTest
public with sharing class TestEmailServiceHandler {
    
    
     static{
        
     
        //data setup
         List<CSVFieldsMapping__c>  listofSetting = new    List<CSVFieldsMapping__c>();        
          //adding fields  mapping
         CSVFieldsMapping__c adxTag = new CSVFieldsMapping__c(name = 'AdxTag__c',CSVFieldsHeader__c ='Tags',FieldType__c='STRING');
         listofSetting.add(adxTag);
         CSVFieldsMapping__c addimpression = new CSVFieldsMapping__c(name = 'Ad_impressions__c',CSVFieldsHeader__c ='Ad impressions',FieldType__c='DECIMAL');
         listofSetting.add(addimpression);
         CSVFieldsMapping__c startDate = new CSVFieldsMapping__c(name = 'Start_Date__c',CSVFieldsHeader__c ='Days',FieldType__c='DATE');
         listofSetting.add(startDate);
         
         insert listofSetting;
         
        
                
    }

  static testmethod void handleInboundEmailTest(){
    // Create a new email, envelope object and Attachment
   Messaging.InboundEmail email = new Messaging.InboundEmail();
   Messaging.InboundEnvelope env = new Messaging.InboundEnvelope();
   Messaging.InboundEmail.BinaryAttachment inAtt = new Messaging.InboundEmail.BinaryAttachment();

   email.subject = 'test';
   env.fromAddress = 'user@acme.com';
    
   StaticResource sr = [Select  s.Name, s.Id, s.Body From StaticResource s  where name ='TestFileResource'];
   blob tempBlob = sr.Body;
   // set the body of the attachment
   inAtt.body = tempBlob;
   inAtt.filename = 'testfile.csv';
   

   email.binaryAttachments = new Messaging.inboundEmail.BinaryAttachment[] {inAtt }; 

   // call the class and test it with the data in the testMethod
   EmailServiceHandler emailServiceObj = new EmailServiceHandler();
   emailServiceObj.handleInboundEmail(email, env ); 
  }
}