/* Test Class for CntrlrExtension_PandoraIdeaLandingPage.cls
 
 */
@isTest
private class TestIdeaLandingPage {

    static testMethod void myUnitTest() {
        
        PageReference pageRef = Page.pandoraIdeaLandingPage;
        Test.setCurrentPage(pageRef);
        ApexPages.IdeaStandardSetController ideaSetController;
       
        CntrlrExtension_PandoraIdeaLandingPage Controller = new CntrlrExtension_PandoraIdeaLandingPage(ideaSetController);
        
        controller.getCategoryValues();
       
        controller.SelectedCategory='General';
        controller.getCategorizedIdeas();
        
        controller.SelectedCategory='My Ideas';
        controller.getCategorizedIdeas();
        
        controller.SelectedCategory='All';
        controller.getCategorizedIdeas();
        
        Controller.SearchIdeaText='Test';
        Controller.searchIdeas();
        
        
        controller.getIdeasCategory1();
        controller.getIdeasCategory2();
        controller.getIdeasCategory3();
        controller.getIdeasCategory4();
        controller.getIdeasCategory5();
        

        
        controller.first();
        controller.next();
        controller.last();
        controller.previous();
        controller.cancel();
        
        controller.renderAllIdeas=true;
        controller.hasNext=true;
        controller.hasPrevious=true;
        
        controller.pageNumber=3;
        
        controller.getIdeas();

        
        Idea myIdea = new Idea (Title='Test Idea New Two');
        myIdea.CommunityId=[Select c.Id From Community c where c.Name='Internal Ideas' limit 1][0].id;
        
        database.insert(myIdea);
        controller.SelectedCategory= 'All';
        controller.ideaVoted = myIdea.Id;
        controller.upDownFlag = 'up';
        controller.voteUpDownFunctionForLandingPage();

         Profile p = [SELECT Id FROM Profile WHERE Name='Standard Platform User'];         
         User u2 = new User(Alias = 'testu', Email='testusernew@yahoo.com',     
         EmailEncodingKey='UTF-8', LastName='testusernew', LanguageLocaleKey='en_US',   
         LocaleSidKey='en_US', ProfileId = p.Id,TimeZoneSidKey='America/Los_Angeles', 
         UserName='testusernew@yahoo.com');         
         System.runAs(u2) {
            controller.voteUpDownFunctionForLandingPage();
         }
       
         myIdea = new Idea (Title='Test Idea New Two');
        
        myIdea.CommunityId=[Select c.Id From Community c where c.Name='Internal Ideas' limit 1][0].id;
        
        database.insert(myIdea);
        controller.ideaVoted = myIdea.Id;
        controller.upDownFlag = 'up';
        
        controller.voteUpDownFunctionForSearch();
        
        System.runAs(u2) {
            controller.voteUpDownFunctionForSearch();
        
        }          



        
    }
}