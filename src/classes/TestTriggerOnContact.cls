/*
Date: 06/04/14
Desc: This test class covers the TriggerOnContact trigger code. The trigger logic is applicable when it is converted from a Lead.
*/
@isTest(seealldata=false)
private class TestTriggerOnContact {
    public static Zip_Code__c zcTest;
    public static Assignment_Group__c agTest;
    public static Group_Member__c gmTest;


    public static void prePareTest(){
      zcTest = new Zip_Code__c(Active__c = 'True',Valid_Zip__c = True, Name='94538',Territory__c='NW',Region__c='West');
      agTest = new Assignment_Group__c(Name = 'Test Group', Type__c = 'Lead');
      gmTest = new Group_Member__c(Name=UserInfo.getName(), Active__c = 'True', User__c = UserInfo.getUserId());
      insert zcTest;
      insert agTest;
      insert gmTest;

      Group_Member_Zip_Junction__c gmzjTest = new Group_Member_Zip_Junction__c(Assignment_Group__c = agTest.Id, Group_Member__c = gmTest.Id,Zip_Code__c = zcTest.Id);

      insert gmzjTest;
      system.debug('gmzjTest'+gmzjTest);
  }
  static testMethod void testCreateOrUpdateContact() {

    Account account = new Account(Name = 'test');
    insert account;
    prePareTest();

    Contact ctat = new Contact();
    ctat.FirstName = 'Test';
    ctat.LastName = 'Test2';
    ctat.AccountId = account.Id;
    ctat.Hidden_Lead_Source__c = 'AdRelevance';
    ctat.Pandora_Promotion__c = 'Promotion';
    ctat.Pandora_B2B_Event__c = 'Event';
    ctat.MailingPostalCode='94538';
    insert ctat;
     List<Contact> contactList = [select id from contact where id =:ctat.id];
    system.assertEquals(1,contactList.size());

}
static testMethod void myTest2() {
	//insert account
    Account account = new Account(Name = 'test1');
    insert account;
    
    Zip_Code__c zipcode = new Zip_Code__c(Active__c = 'True',Valid_Zip__c = True,Name='97538',Territory__c='NW',Region__c='West');
    insert zipCode;
    Zip_Code__c zipcode2 = new Zip_Code__c(Active__c = 'True',Valid_Zip__c = True,Name='97298',Territory__c='NW',Region__c='West');
    insert zipCode2;
    //insert contact
    List<Contact> conList = new List<Contact>();
    for(Integer i=0; i<205; i++){
        Contact ctat = new Contact();
        ctat.FirstName = 'Test';
        ctat.LastName = 'Test3';
        ctat.AccountId = account.Id;
        ctat.Hidden_Lead_Source__c = '';
        ctat.Pandora_Promotion__c = 'Promotion';
        ctat.Pandora_B2B_Event__c = 'Event';
        ctat.MailingPostalCode='97538'; 
        conList.add(ctat);
    }
    Test.startTest();
    insert conList;
    Test.stopTest();
    Set<Id> ids = new Set<Id>();
    for(Contact con :conList){
        ids.add(con.id);
    }
    List<Contact> contactList = [select id from contact where id IN: ids];
    system.assertEquals(205,contactList.size());
     //Zip_Code__c z
     Contact ctat2 = new Contact(id=contactList[2].id);
     ctat2.Hidden_Lead_Source__c = 'MediaGuide';
     ctat2.Pandora_Promotion__c = 'Event2';
     ctat2.Pandora_B2B_Event__c = 'Promotion2';
     ctat2.MailingPostalCode='97298';
     update ctat2;
     system.debug('ctat2'+ctat2);
     contactRegionTerritoryHandler cntHandler = new contactRegionTerritoryHandler();

 }

}