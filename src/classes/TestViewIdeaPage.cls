/* Test Class for CntrlrExtensions_PandoraViewIdea
 
 */
@isTest
private class TestViewIdeaPage {


     
    static testMethod void myUnitTest() {
        
        PageReference pageRef = Page.PandoraViewIdea;
        Test.setCurrentPageReference(pageRef);
        
        
        Idea myIdea = new Idea (Title='Test Idea');
        myIdea.CommunityId=[Select c.Id From Community c where c.Name='Internal Ideas' limit 1][0].id;
        
        database.insert(myIdea);
       
        IdeaComment c = new IdeaComment();
        c.CommentBody='Commentbody';
        c.IdeaId=+myIdea.id;
        
        database.insert(c);
        
        ApexPages.currentPage().getParameters().put('id', +myidea.id);
        
        ApexPages.IdeaStandardController sc;
        CntrlrExtensions_PandoraViewIdea controllerExt = new CntrlrExtensions_PandoraViewIdea(sc);
        
       controllerExt.i=+myIdea;
       
        controllerExt.getIdeasCategory1();
        controllerExt.getIdeasCategory2();
     //   controllerExt.getIdeasCategory3();
    //    controllerExt.getIdeasCategory4();
    //    controllerExt.getIdeasCategory5();
        
        
        controllerExt.EditIdea();
        
       
       Test.setCurrentPageReference(pageRef);
       ApexPages.currentPage().getParameters().put('id', +myidea.id);
       
        controllerExt.AddComment();
        controllerExt.Comment='Comment';
        
        controllerExt.AddComment();
        //controllerExt.getComments();
        
        controllerExt.getrenderButton();
        
       Test.setCurrentPageReference(pageRef);
       ApexPages.currentPage().getParameters().put('id', +myidea.id);
        controllerExt.voteup();
        
        Test.setCurrentPageReference(pageRef);
       ApexPages.currentPage().getParameters().put('id', +myidea.id);
        controllerExt.votedown();
        
        Test.setCurrentPageReference(pageRef);
       ApexPages.currentPage().getParameters().put('id', +myidea.id);
         controllerExt.voteup();
         
         Test.setCurrentPageReference(pageRef);
       ApexPages.currentPage().getParameters().put('id', +myidea.id);
         controllerExt.votedown();
        controllerExt.getVotedIdeas();
        controllerExt.deleteIdea();
        
    }
}