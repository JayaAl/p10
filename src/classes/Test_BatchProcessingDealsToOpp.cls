/**
 * @name: Test_BatchProcessingDealsToOpp  
 * @desc: TestClass for BatchProcessingDealsToOpp  
 * @author: Priyanka Ghanta
 * @date: 05/17/2015
 */
@isTest
public class Test_BatchProcessingDealsToOpp  {

    public static testMethod void testDealsToOpp() {
    
    List<AdxLog__c> adxList = new List<AdxLog__c>();
    
    
        Account a = new Account(Name = 'Test Account', Type='Advertiser');
        insert a;
        
        Contact conObj = UTIL_TestUtil.generateContact(a.id);
        insert conObj;
        
        Schema.DescribeSObjectResult cfrSchema = Schema.SObjectType.Opportunity; 
        Map<String,Schema.RecordTypeInfo> AccountRecordTypeInfo = cfrSchema.getRecordTypeInfosByName(); 
        Id rtId = AccountRecordTypeInfo .get('Opportunity - Programmatic').getRecordTypeId();
        
        Opportunity o1 = new Opportunity(Name = 'Test Opportunity1', AccountId = a.Id, Primary_Billing_Contact__c =conObj.Id, StageName = 'Negotiation', CloseDate = System.Today(),Confirm_direct_relationship__c=true,
                                            Deal_Id__c = '1234',Industry_Sub_Category__c='Political Campaign Marketing',Type='Advertiser');
                                            
        o1.RecordTypeId = rtId ;
        insert o1;
         
        Opportunity o2 = new Opportunity(Name = 'Test Opportunity2', AccountId = a.Id, Primary_Billing_Contact__c =conObj.Id, StageName = 'Negotiation', CloseDate = System.Today(),Confirm_direct_relationship__c=true,
                                            Deal_Id__c = '1237',Industry_Sub_Category__c='Political Campaign Marketing',Type='Advertiser');
        
        o2.RecordTypeid=rtId ;                                    
        insert o2;  
        List<BatchProcDeals__c>  btcustom = new List<BatchProcDeals__c>();
        
        BatchProcDeals__c batchDeal1 = new BatchProcDeals__c(Name='Android P10 Mobile Advertising only',AdX_Tags__c='Web: 300x600',Performance_Type__c='Display - 300 x 600',SFDC_Products__c='Web');
        BatchProcDeals__c batchDeal2 = new BatchProcDeals__c(Name='Android P10 Mobile Pandora KV only', AdX_Tags__c='Android - P10 Mobile - Advertising ID Only',Performance_Type__c='Display - 300 x 600',SFDC_Products__c='Mobile');
        BatchProcDeals__c batchDeal3 = new BatchProcDeals__c(Name='iPad - P10 Mobile - Pandora KV Only',AdX_Tags__c='iPad - P10 Mobile - Pandora KV Only',Performance_Type__c='Display - 300 x 600',SFDC_Products__c='Mobile');
        btcustom.add(batchDeal1);
        btcustom.add(batchDeal2);
        btcustom.add(batchDeal3);
        insert btcustom;
        
        
         Product2 prod = new Product2(Name = 'Web', 
            Family = 'Hardware');
        insert prod;
        
        Product2 prod1 = new Product2(Name = 'Mobile', 
            Family = 'Software');
        insert prod1;
        
        // Get standard price book ID.
        // This is available irrespective of the state of SeeAllData.
        Id standard = Test.getStandardPricebookId();
     
        PricebookEntry standardPrice = new PricebookEntry(
            Pricebook2Id = standard, Product2Id = prod.Id,
            UnitPrice = 10000, IsActive = true);
        insert standardPrice;
        
        // Create a custom price book
        Pricebook2 customPB = new Pricebook2(Name='Custom Pricebook', isActive=true);
        insert customPB;
        
        // 2. Insert a price book entry with a custom price.
        PricebookEntry customPrice = new PricebookEntry(
            Pricebook2Id = standard, Product2Id = prod1.Id,
            UnitPrice = 12000, IsActive = true);
        insert customPrice;
        
        PriceBookEntry pbe = [select id from pricebookentry where name = 'Web' limit 1];
        system.debug('===========================================================================entry is :' + pbe.id);
                                        
       /****For all matching Mobile Deals*****************/
        AdxLog__c adxobj1 = new AdxLog__c(Deal_Id__c='1234',Estimated_Revenue__C = 200,Ad_impressions__c=20,AdxTag__c='Web: 300x600',Line_Created__c = FALSE);
        AdxLog__c adxobj2 = new AdxLog__c(Deal_Id__c='1234',Estimated_Revenue__C = 300,Ad_impressions__c=30,AdxTag__c='Android - P10 Mobile - Advertising ID Only',Line_Created__c = FALSE);
       /****For all matching Web Deals*****************/
        AdxLog__c adxobj3 = new AdxLog__c(Deal_Id__c='1237',Estimated_Revenue__C = 200,Ad_impressions__c=40,AdxTag__c='Android - P10 Mobile - Advertising ID Only',Line_Created__c = FALSE);
        AdxLog__c adxobj4 = new AdxLog__c(Deal_Id__c='1237',Estimated_Revenue__C = 300,Ad_impressions__c=50,AdxTag__c='iPad - P10 Mobile - Pandora KV Only',Line_Created__c = FALSE);
       /****For all non matching Deals*****************/
        AdxLog__c adxobj5 = new AdxLog__c(Deal_Id__c='1235',Estimated_Revenue__C = 300,Ad_impressions__c=60,AdxTag__c='iPad - P10 Mobile - Pandora KV Only',Line_Created__c = FALSE);
        
       /****For all 'Open Auction' Deals*****************/
        AdxLog__c adxobj6 = new AdxLog__c(Deal_Id__c='Open Auction',Estimated_Revenue__C = 300,Ad_impressions__c=25,AdxTag__c='P10 Web Standalone PMPs',Line_Created__c = FALSE);
        
        adxList.add(adxobj1);
        adxList.add(adxobj2);
        adxList.add(adxobj3);
        adxList.add(adxobj4);
        adxList.add(adxobj5);
        adxList.add(adxobj6);
        
        insert adxList;
        
        Test.startTest();
        BatchProcessingDealsToOpp rrk = new BatchProcessingDealsToOpp();
        Database.executeBatch(rrk);
        Test.stopTest();
        
        OpportunityLineItem[] oppMobile = [Select unitprice,product2.Id from OpportunityLineItem where opportunityid =: o1.id];
        OpportunityLineItem[] oppWeb = [Select unitprice,product2.Id from OpportunityLineItem where opportunityid =: o2.id];
 
       }
        public static testMethod void testSchedule() {
            ScheduleBatchProcessingDealsToOpp scheBat = new ScheduleBatchProcessingDealsToOpp();            
            String sch = '0 0 23 * * ?';
            system.schedule('Test check', sch, scheBat );                
        }
}