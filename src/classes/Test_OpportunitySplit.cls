@isTest
private class Test_OpportunitySplit {

    static testMethod void OpportunityBeforeTriggerTest() {
    System.Test.startTest();
        Account a = new Account(Name = 'Test Account', Type='Advertiser');
        insert a;
        
       // updated by VG 12/26/2012
          Contact conObj = UTIL_TestUtil.generateContact(a.id);
        insert conObj;
        
        Opportunity o = new Opportunity(Name = 'Test Opportunity', AccountId = a.Id, Primary_Billing_Contact__c =conObj.Id, StageName = 'Negotiation', CloseDate = System.Today(),Confirm_direct_relationship__c=true,Industry_Sub_Category__c='Political Campaign Marketing',Type='Advertiser');
        insert o;
        Opportunity test = [SELECT Id, OwnerId, X1st_Salesperson__c FROM Opportunity WHERE Id = :o.id];
        System.Debug('Opportunity: ' + test);
        System.Test.stopTest();
        //System.AssertEquals(test.OwnerId, test.X1st_Salesperson__c);
    }

}