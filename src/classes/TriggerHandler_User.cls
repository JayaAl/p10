public with sharing class TriggerHandler_User {
// This should be used in conjunction with the ApexTriggerComprehensive.trigger template
// The origin of this pattern is http://www.embracingthecloud.com/2010/07/08/ASimpleTriggerTemplateForSalesforce.aspx

public class myException extends Exception{}
private boolean m_isExecuting = false;
private integer BatchSize = 0;

public TriggerHandler_User(boolean isExecuting, integer size){
    m_isExecuting = isExecuting;
    BatchSize = size;
}

public void OnBeforeInsert(List<User> t_New){
// Pass all user edits thorough EnterpriseCapabilitiesController to add any applicable Enterprise Capabilities
EnterpriseCapabilitiesController ecc = new EnterpriseCapabilitiesController();
ecc.updateEnterpriseCapabilities(t_New);
}


public  void OnAfterInsert(List<User> t_New){
    /* Calling the LMS_Group_Assignment_Handler to auto assign the Users to public group based on the LMS Mappings */
    try{
        if(!LMS_Group_Assignment_Handler.isRun){


          LMS_Group_Assignment_Handler.assignPublicGroup(null,t_New);
      } 
      LMS_Group_Assignment_Handler.isRun = true;
      if(Test.isRunningTest()){
        Throw new MyException('Custom Exception');
    }               
    } catch(exception e) {
        system.debug(logginglevel.info, 'USER INSERT/UPDATE ERROR-E' + '---' + e.getStacktracestring());
}

/* Process related Application, ERP_User__c and Contact records when User details change */

}

/* Calling the ManageuserRelatedREcords class to insert Application, ERP, Contact users */
@future                           
public static void OnAfterInsertAsync(set<Id> userIdList){
    try{
        List<User> t_New = [Select Name,ManagerId,Email,UserType, Username,IsActive,FirstName,LastName from User where Id IN : userIdList];
    ManageUserRelatedRecords.manageUserRelatedRecords(t_New,new Map<ID, User>());    
    } catch(Exception ex){
         system.debug(logginglevel.info, 'EXCEPTION IN AFTER INSERT - APPLICATION, ERP, CONTACT USERS' + ex + '---' + ex.getstacktracestring()); 
    } 
}

public void OnBeforeUpdate(List<User> t_old, List<User> t_new, Map<ID, User> t_newMap){
    /* Pass all user edits thorough EnterpriseCapabilitiesController to add any applicable Enterprise Capabilities */
    EnterpriseCapabilitiesController ecc = new EnterpriseCapabilitiesController();
    ecc.updateEnterpriseCapabilities(t_new);
}

public void OnAfterUpdate(List<User> t_old, List<User> t_new, Map<ID, User> t_newMap, Map<ID, User> t_oldMap ){
    /* Calling the LMS_Group_Assignment_Handler to auto assign the Users to public group based on the LMS Mappings */
    try{
        if(!LMS_Group_Assignment_Handler.isRun){
           LMS_Group_Assignment_Handler.assignPublicGroup(t_oldMap, t_new);
           if(Test.isRunningTest()){
            Throw new MyException('Custom Exception');
        }
    }
    LMS_Group_Assignment_Handler.isRun = true;
    } catch(exception e) {
        system.debug(logginglevel.info, 'USER INSERT/UPDATE ERROR' + e + '---' + e.getStacktracestring());
        
        
}


}
/* Calling the ManageuserRelatedREcords class to update Application, ERP, Contact users */
@future
public static void OnAfterUpdateAsync(set<id> newIdList, set<Id> oldIdList){
    try{
        List<User> t_New = [Select Name,ManagerId,Email,UserType, Username,IsActive,FirstName,LastName from User where Id IN : newIdList];
        List<User> t_listUser = [Select Name,ManagerId,Email,UserType, Username,IsActive,FirstName,LastName from User where Id IN : oldIdList];
        Map<ID, User> t_oldMap  = new Map<ID, User>();
        for(User u:t_listUser){
            t_oldMap.put(u.id, u);
        }        
       
           ManageUserRelatedRecords.manageUserRelatedRecords(t_New,t_oldMap);   
        } catch(Exception ex){
            system.debug(logginglevel.info, 'EXCEPTION IN AFTER UPDATE - APPLICATION, ERP, CONTACT USERS' + ex + '---' + ex.getstacktracestring()); 
        }
}

public boolean IsTriggerContext{
    get{ return m_isExecuting;}
}

public boolean IsVisualforcePageContext{
    get{ return !IsTriggerContext;}
}

public boolean IsWebServiceContext{
    get{ return !IsTriggerContext;}
}

public boolean IsExecuteAnonymousContext{
    get{ return !IsTriggerContext;}
}
}