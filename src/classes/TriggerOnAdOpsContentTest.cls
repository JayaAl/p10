/*
 * @name: TriggerOnAdOpsContentTest
 * @author: Lakshman(sfdcace@gmail.com)
 * @desc: Test class to cover Triggers: TriggerOnAdOps & TriggerOnContent
 * @date: 7-8-2014
 */
@isTest
public class TriggerOnAdOpsContentTest {
    static testMethod void testAdOpsContent() {
        
        Account testAccount = UTIL_TestUtil.createAccount();
        Opportunity testOpp = UTIL_TestUtil.createOpportunity(testAccount.id);
        Ad_Operations__c testAdOps = new Ad_Operations__c();
        testAdOps.Opportunity__c = testOpp.Id;
        insert testAdOps;
        
        ContentVersion testContentInsert =new ContentVersion(); 
        testContentInsert.ContentURL='http://www.google.com/'; 
        testContentInsert.Title ='Google.com'; 
        insert testContentInsert; 
        ContentVersion testContent = [SELECT ContentDocumentId FROM ContentVersion where Id = :testContentInsert.Id]; 
        ContentWorkspace testWorkspace = [SELECT Id FROM ContentWorkspace WHERE Name='Default']; 
        ContentWorkspaceDoc newWorkspaceDoc =new ContentWorkspaceDoc(); 
        newWorkspaceDoc.ContentWorkspaceId = testWorkspace.Id; 
        newWorkspaceDoc.ContentDocumentId = testContent.ContentDocumentId; 
        insert newWorkspaceDoc;
        testContent.Opportunity__c =testOpp.Id; 
        update testContent;
        
        testAdOps.Opportunity__c = null;//Blank the opportunity
        update testAdOps;
        System.assertEquals([Select Ad_Ops__c from ContentVersion where Id =: testContentInsert.Id].Ad_Ops__c, null);
        
        testAdOps.Opportunity__c = testOpp.Id;//Set the opportunity again
        update testAdOps;
        System.assertEquals([Select Ad_Ops__c from ContentVersion where Id =: testContentInsert.Id].Ad_Ops__c, testAdOps.Id);
    }
}