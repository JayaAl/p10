public class TriggerOnProject {
    
    public TriggerOnProject(){
        
    }    
    
    public void updateProjectDates_Before(List<Project__c> theList){
        for(Project__c p:theList){
            updateProjectDates_Before(p);
        }
    }
    public void updateProjectDates_Before(Project__c p){
        system.debug('----> Start updateProjectDates_Before');
        
        if( p.Launch_Date__c != NULL && (p.Selected_End_Date__c == NULL || p.Selected_End_Date__c > Date.today() )){
            Integer months = 0;
            if(p.Auto_Renewal_Frequency__c == '6 months'){
                months = 6;
            } else if(p.Auto_Renewal_Frequency__c == '1 year'){
                months = 12;
            }
            system.debug('----> Months: ' + months);
            
            Integer offsetdays = 0;
            if(p.Non_Renewal_Notice__c == '30 days'){
                offsetdays = -30;
            } else if(p.Non_Renewal_Notice__c == '60 days'){
                offsetdays = -60;
            } else if(p.Non_Renewal_Notice__c == '90 days'){
                offsetdays = -90;
            } else if(p.Non_Renewal_Notice__c == '1 Year'){
                offsetdays = -365;
            }
            system.debug('----> Days: ' + offsetdays);
            
            if(months == 0){ // If we did not recognize the Auto_Renewal_Frequency__c value simply null out the renewal date
                p.Next_Renewal_Date__c = NULL;
                p.Next_Reminder_Date__c = NULL;
            } else {
                Date renewalDate = getFutureSchedule(p.Launch_Date__c, months);
                system.debug('----> renewalDate: ' + renewalDate);
                if(p.Selected_End_Date__c == null || renewalDate <= p.Selected_End_Date__c){
                    system.debug('----> renewalDate <= p.Selected_End_Date__c');
                    p.Next_Renewal_Date__c = renewalDate;
                    p.Next_Reminder_Date__c = renewalDate.addDays(offsetdays);
                } else {
                    p.Next_Renewal_Date__c = NULL;
                    p.Next_Reminder_Date__c = NULL;
                }
            }
        } else { // if Launch_Date__c is empty or Selected_End_Date__c is in the past simply null out Next_Renewal_Date__c
            p.Next_Renewal_Date__c = NULL;
            p.Next_Reminder_Date__c = NULL;
        }
        system.debug('----> Start updateProjectDates_Before');
    }

    
    public Date getFutureSchedule(Date startDate, Integer periodInMonths){
        return getFutureSchedule(startDate, periodInMonths, 0);
    }
    public Date getFutureSchedule(Date startDate, Integer periodInMonths, Integer daysOffset){
        Date returnDate = null;
        for(integer i=1;i<100;i++){
            returnDate = startDate.addMonths(i*periodInMonths).addDays(daysOffset);
            if(returnDate > date.today()){ // If the new date is in the future then set Next_Renewal_Date__c and continue to the next Project__c record.
                return returnDate;
            }
        }
        return returnDate;
    }
}