@isTest
public class TriggerOnProjectTest {

    public static testmethod void test_TriggerOnProject(){
        // Test loading the base controller, no contents yet, so there is nothing to test.
        controller = new TriggerOnProject();
    } 
    
    public static testmethod void test_DuplicateName(){
        DataPrep();
        // Clone singleProject, do not change name, expected result: error
        Project__c errorP = singleProject.clone(false, false, false, false);
        boolean errorCaptured = false;
        try{
            insert errorP;
        } catch (exception e){
            system.assert(e.getMessage().contains('A project with the same name already exists'),'Expected "A project with the same name..." error, received: ' + e.getMessage());
        	errorCaptured = true;
        } 
        system.assert(errorCaptured,'Expected "A project with the same name..." error, no error received.');
        
        // Clone singleProject, change name, expected result: record saved correctly.
        Project__c validP = singleProject.clone(false, false, false, false);
        validP.Name = UTIL_TestUtil.generateRandomString();
        try{
            insert validP;
        } catch (exception e){
            system.assert(false,'Received Exception during Project__c insert: ' + e.getMessage());
        } 
    }
    
    public static testmethod void test_updateProjectDates_Before(){
        DataPrep();
        
        singleProject.Auto_Renewal_Frequency__c = '1 year';
        update singleProject;
        
        singleProject.Auto_Renewal_Frequency__c = 'UNKNOWN';
        update singleProject;
        
        singleProject.Non_Renewal_Notice__c = '30 days';
        update singleProject;
        
        singleProject.Non_Renewal_Notice__c = '60 days';
        update singleProject;
        
        singleProject.Non_Renewal_Notice__c = '90 days';
        update singleProject;
        
        singleProject.Non_Renewal_Notice__c = '1 Year';
        update singleProject;
        
        singleProject.Non_Renewal_Notice__c = 'UNKNOWN';
        update singleProject;
        
        singleProject.Auto_Renewal_Frequency__c = '1 year';
        singleProject.Non_Renewal_Notice__c = '30 days';
        singleProject.Selected_End_Date__c = date.today();
        update singleProject;
        
        singleProject.Launch_Date__c = NULL;
        update singleProject;
        
    }    
    
    private static Project__c singleProject;
    private static List<Project__c> listProject;
    private static TriggerOnProject controller;
    private static void DataPrep(){
        controller = new TriggerOnProject();
        Account a = UTIL_TestUtil.createAccount();
        singleProject = new Project__c(
            Name = UTIL_TestUtil.generateRandomString(),
            Account__c = a.Id,
            Auto_Renewal_Frequency__c = '6 months',
            End_Date__c = NULL,
            Launch_Date__c = Date.today(),
            Next_Reminder_Date__c = NULL,
            Next_Renewal_Date__c = NULL,
            Non_Renewal_Notice__c = '30 days'
        );
        insert singleProject;
        listProject = new List<Project__c>();
        for(Integer i=0;i<100;i++){
            listProject.add(
                new Project__c(
                    Name = UTIL_TestUtil.generateRandomString(),
                    Account__c = a.Id,
                    Auto_Renewal_Frequency__c = '6 months',
                    End_Date__c = NULL,
                    Launch_Date__c = Date.today(),
                    Next_Reminder_Date__c = NULL,
                    Next_Renewal_Date__c = NULL,
                    Non_Renewal_Notice__c = '30 days'
                )
            );
        }
        insert listProject;
    }
}