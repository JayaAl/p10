/*
Developer: Ralph Callaway <ralph@callawaycloudconsulting.com>
Description:
	Utility method for accessing case record types by their dev names that 
	caches the record type query as a number of case triggers need access
	to record types.  Could use getRecordTypeInfos(), but the info is returned
	by their label (as opposed to dev name) for some really stupid reason.
*/
public class UTIL_CaseRecordTypeHelper {

	/* Constants, Properties, and Variables */

	private static Map<String, Id> recordTypeMapByDevName {
		get {
			if(recordTypeMapByDevName == null) {
				loadRecordTypeMaps();
			}
			return recordTypeMapByDevName;
		}
		set;
	}
	
	private static Map<Id, String> recordTypeMapById {
		get {
			if(recordTypeMapById == null) {
				loadRecordTypeMaps();
			}
			return recordTypeMapById;
		}
		set;
	}
	
	/* Public Functions */
	
	public static Id getRecordTypeId(String developerName) {
		return recordTypeMapByDevName.get(developerName);
	}
	
	public static String getRecordTypeDevName(Id recordTypeId) {
		return recordTypeMapById.get(recordTypeId);
	}
	
	/* Private Functions */
	
	private static void loadRecordTypeMaps() {
		recordTypeMapByDevName = new Map<String, Id>();
		recordTypeMapById = new Map<Id, String>();
		for(RecordType recordType : [
			select developerName
			from RecordType
			where sObjectType = 'Case'
		]) {
			recordTypeMapByDevName.put(recordType.developerName, recordType.id);
			recordTypeMapById.put(recordType.id, recordType.developerName);
		}
	}
	
	/* Test Method */
	
	@isTest
	private static void testGetRecordTypeFunctions() {
		RecordType testRecordType = [select developerName from RecordType where sObjectType = 'Case' limit 1];
		system.assertEquals(testRecordType.id, getRecordTypeId(testRecordType.developerName));
		system.assertEquals(testRecordType.developerName, getRecordTypeDevName(testRecordType.id));
	}

}