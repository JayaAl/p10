/*
Developer: Ralph Callaway <ralph.w.callaway@gmail.com>
Description:
	Utility class for schema methods that caches the following governor limit controlled requests:
	- Schema.DescribeSObjectResult.fields // FieldsDescribesLimits
	- Schema.DescribeSObjectResults.getRecordTypeInfos() // RecordTypeDescribes 
*/
public class UTIL_SchemaHelper {

	/* Variables and Constants */
	
	private static Map<String, Map<String, Schema.SObjectField>> fieldMapCache = new Map<String, Map<String, Schema.SObjectField>>();
	private static Map<String, List<Schema.RecordTypeInfo>> recordTypeInfoCache = new Map<String, List<Schema.RecordTypeInfo>>();
	
	/* Memoized Variables */
			
	private static Map<String, Schema.SObjectType> globalDescribe {
		get {
			if(globalDescribe == null) {
				globalDescribe = Schema.getGlobalDescribe();
			}
			return globalDescribe;
		}
		set;
	}
	
	/* Public Functions */
	
	public static Schema.DescribeFieldResult getFieldDescribe(String objectName, String fieldName) {
		return (!isValidField(objectName, fieldName)) ? null : getFieldMap(objectName).get(fieldName).getDescribe();
	}
	
	public static Map<String, Schema.SObjectField> getFieldMap(String objectName) {
		if(!isValidObject(objectName))
			return null;
		
		String key = getKey(objectName);
		if(!fieldMapCache.containsKey(key)) {
			fieldMapCache.put(key, getObjectDescribe(objectName).fields.getMap());
		}
		return fieldMapCache.get(key);
	}
	
	public static Schema.DescribeSObjectResult getObjectDescribe(String objectName) {
		return (!isValidObject(objectName)) ? null : globalDescribe.get(objectName).getDescribe();
	}
	
	public static List<Schema.RecordTypeInfo> getRecordTypeInfos(String objectName) {
		if(!isValidObject(objectName))
			return null;
		
		String key = getKey(objectName);
		if(!recordTypeInfoCache.containsKey(key)) {
			recordTypeInfoCache.put(key, getObjectDescribe(objectName).getRecordTypeInfos());
		}
		return recordTypeInfoCache.get(key);
	}
	
	public static Boolean isValidField(String objectName, String fieldName) {
		return (!isValidObject(objectName)) ? false : getFieldMap(objectName).containsKey(fieldName);
	}
	
	public static Boolean isValidObject(String objectName) {
		return globalDescribe.containsKey(objectName);
	}
	
	/* Helper Methods */
	
	private static String getKey(String objectName, String fieldName) {
		return objectName.toLowerCase() + fieldName.toLowerCase();
	}
	
	private static String getKey(String objectName) {
		return objectName.toLowerCase();
	}
	
	/* Test Methods */
	
	@isTest
	private static void testIsValidObject() {
		system.assertEquals(true, isValidObject('aCcount')); 
		system.assertEquals(false, isValidObject('fakeobj'));
		system.assertEquals(false, isValidObject(null));
	}
	
	@isTest
	private static void testIsValidField() {
		system.assertEquals(true, isValidField('account', 'name'));
		system.assertEquals(false, isValidField('account', 'fakefield'));
		system.assertEquals(false, isValidField(null, 'name'));
		system.assertEquals(false, isValidField('account', null));
		system.assertEquals(false, isValidField(null, null));
	}
	
	@isTest
	private static void testGetObjectDescribe() {
		system.assertEquals(Schema.SObjectType.Account, getObjectDescribe('aCcount'));
		system.assertEquals(null, getObjectDescribe('fakeobj'));
		system.assertEquals(null, getObjectDescribe(null));
	}
	
	@isTest
	private static void testGetFieldDescribe() {
		system.assertEquals(Schema.SObjectType.Account.fields.Name, getFieldDescribe('account', 'name'));
		system.assertEquals(null, getFieldDescribe('account', 'fakefield'));
		system.assertEquals(null, getFieldDescribe(null, 'name'));
		system.assertEquals(null, getFieldDescribe('account', null));
		system.assertEquals(null, getFieldDescribe(null, null));
	}
	
	@isTest
	private static void testGetFieldMap() {
		system.assertEquals(Schema.SObjectType.Account.fields.getMap(), getFieldMap('account'));
		system.assertEquals(null, getFieldMap(null));
		system.assertEquals(null, getFieldMap('fakeobj'));
	}
	
	@isTest
	private static void testGetRecordTypeInfos() {
		String testObject = 'account';
		List<Schema.RecordTypeInfo> rts = getRecordTypeInfos(testObject);
		Integer rtCnt = [select count() from RecordType where sObjectType = :testObject];
		
		// Results vary depending on wheter object has record types.  If it does the record type
		// infos returns an extra entry for the "Master" record type that isn't returned 
		// when querying the RecordType table.
		if(rts.size() == 0) {
			system.assertEquals(rtCnt, rts.size()); // don't need +1 since "Mastser" record type not returned in rt infos
		} else {
			system.assertEquals(rtCnt + 1, rts.size()); // +1 since record type infos also include "Master" record type
		}
		
		// test null values
		system.assertEquals(null, getRecordTypeInfos('fakeobj'));
	}
	
	@isTest
	private static void testFieldMapCaching() {
		// Each call that references a new object will require another 
		// fields describe.  Validate that cache prevents calling the
		// fields describe on the same object twice. Invalid objects
		// shouldn't count against describe limits, but invalid fields
		// will.
		system.assertEquals(0, Limits.getFieldsDescribes());
		isValidObject('account');
		system.assertEquals(0, Limits.getFieldsDescribes());
		getObjectDescribe('account');
		system.assertEquals(0, Limits.getFieldsDescribes());
		isValidField('account', 'name');
		system.assertEquals(1, Limits.getFieldsDescribes());
		getFieldDescribe('account', 'name');
		system.assertEquals(1, Limits.getFieldsDescribes());
		getFieldDescribe('account', 'type');
		system.assertEquals(1, Limits.getFieldsDescribes());
		getFieldDescribe('contact', 'lastname');
		system.assertEquals(2, Limits.getFieldsDescribes());
		getFieldDescribe('contact', 'firstName');
		system.assertEquals(2, Limits.getFieldsDescribes());
		getFieldDescribe('fakeojb', 'name');
		system.assertEquals(2, Limits.getFieldsDescribes());
		getFieldDescribe('case', 'fakefield');
		system.assertEquals(3, Limits.getFieldsDescribes());
	}
	
	@isTest
	private static void testRecordTypeCaching() {
		// Each call that references a new object wil require
		// another recordTypeInfos() call.  Validate that cache
		// prevents a second call if we ask for info on the same
		// object. Invalid objects should count against record type
		// describe limit.
		system.assertEquals(0, Limits.getRecordTypesDescribes());
		getRecordTypeInfos('fakeobj');
		system.assertEquals(0, Limits.getRecordTypesDescribes());
		getRecordTypeInfos('account');
		system.assertEquals(1, Limits.getRecordTypesDescribes());
		getRecordTypeInfos('case');
		system.assertEquals(2, Limits.getRecordTypesDescribes());
		getRecordTypeInfos('Account');
		system.assertEquals(2, Limits.getRecordTypesDescribes());
	}
}