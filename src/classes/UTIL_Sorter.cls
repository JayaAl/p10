public class UTIL_Sorter {

    private String column;
    public String sortDirection;
    
    private String defaultSortColumn;
    private String defaultSortDirection;
    
    public final static String SORT_ASC = 'ASC';
    public final static String SORT_DESC = 'DESC';
     
    public UTIL_Sorter(String defaultSortColumn, String defaultSortDirection){
        this.defaultSortColumn = defaultSortColumn;
        this.column = defaultSortColumn;
        this.defaultSortDirection = defaultSortDirection;
        this.sortDirection = defaultSortDirection;
        
    }
    
    public String getSortDirection() {
        return this.sortDirection == null ? defaultSortDirection : this.sortDirection;
    }
    
    public String getColumn() {
        return this.column == null ? defaultSortColumn : this.column;
    }

    public void setColumn(String columnName) {
        if (this.column == null) {
            this.column = columnName;
            sortDirection = defaultSortDirection;
        } else if (column.equalsIgnoreCase(columnName)) {
            sortDirection = (sortDirection.equals(SORT_ASC)) ? SORT_DESC : SORT_ASC;
        } else {
            this.column = columnName;
            sortDirection = defaultSortDirection;
        }
    }
    
    testMethod static void unitTest() {
        UTIL_Sorter s = new UTIL_Sorter('startDate', UTIL_Sorter.SORT_ASC);
        System.assertEquals('startDate', s.getColumn());
        System.assertEquals(UTIL_Sorter.SORT_ASC, s.getSortDirection());
        
        s.setColumn('startDate');
        System.assertEquals('startDate', s.getColumn());
        System.assertEquals(UTIL_Sorter.SORT_DESC, s.getSortDirection());

        s.setColumn('endDate');
        System.assertEquals('endDate', s.getColumn());
        System.assertEquals(UTIL_Sorter.SORT_ASC, s.getSortDirection());
    }
}