/*
Developer: Ralph Callaway <ralph@callawaycloudconsulting.com>
Description:
    Place all data creation utility functions in this class so they can be shared
    across multiple tests.  That way if new validation rules break a test you only
    have to make a change in one place.
*/
@isTest // prevents methods from being called outside test context
public class UTIL_TestUtil {
    
    /* Test Lock */

    static {
        // Per the apex documentation it shouldn't be possible to call methods in this class outside of
        // test context, but as of 6/2/12 that wasn't being enforced in sandboxes.
        system.assert(Test.isRunningTest(), 'UTIL_TestUtil methods may only be called from test methods');
    }
    
    /* Quick Test Values */

    public static Decimal DEFAULT_PRICE = 1;
    public static Decimal DEFAULT_QUANTITY = 1;
    public static Blob TEST_BLOB = Blob.valueOf(generateRandomString(16));
    public static Date TEST_DATE = System.today();
    public static String TEST_STRING = generateRandomString(16);

    /* Standard Object Functions */

    // Accounts
    public static Account createAccount() {
        Account account = generateAccount();
        insert account;
        return account;
    }
    public static Account generateAccount() {
    //Updated by Lakshman 20/3/2013: added required fields for inserting Account
        return new Account(
            name = generateRandomString(8),
            Website = generateRandomString(8)+'.com',
            BillingStreet = generateRandomString(8),
            BillingCity = generateRandomString(8),
            BillingState = 'CA',
            BillingPostalCode = '45897643',
            c2g__CODAAccountTradingCurrency__c = 'USD'//Add by Lakshman on 10/28/2014(ISS-11405)
        );
    }
    
    // Attachments
    public static Attachment createAttachment(Id parentId) {
        Attachment attachment = generateAttachment(parentId);
        insert attachment;
        return attachment;
    }
    public static Attachment generateAttachment(Id parentId) {
        return new Attachment(
              parentId = parentId
            , name = TEST_STRING
            , body = TEST_BLOB
        );
    }
    
    // Case
    public static Case createCase() {
        Case testCase = generateCase();
        insert testCase;
        return testCase;
    }
    public static Case generateCase() {
        return new Case(subject = TEST_STRING, description = TEST_STRING, Billing_Type__c='Advertiser'); //updated by VG 12/26/2012
    }
    
    // Contacts
    public static Contact createContact(Id accountId) {
        Contact contact = generateContact(accountId);
        Database.DMLOptions dml = new Database.DMLOptions(); 
        dml.DuplicateRuleHeader.allowSave = true;
        dml.DuplicateRuleHeader.runAsCurrentUser = true;

        Database.SaveResult sr = Database.insert(contact, dml);
        if (sr.isSuccess()) {
            System.debug('Duplicate account has been inserted in Salesforce!');
        }
        //insert contact;
        return contact;
    }
    //updated by VG 03/22/2013
    public static Contact generateContact(Id accountId) {
        return new Contact(lastName = System.now().millisecond()+'G', accountId = accountId, MailingStreet = 'MyStreet1'+System.now().millisecond(), MailingCountry = 'USA', MailingState = 'NY', MailingCity = 'Fremont', Email = 'myemail1111'+Math.round(Math.random()*1000)+'@mydomain.com', Title = 'MyTitle1'+System.now().millisecond(), firstname = 'MyFristName1'+System.now().millisecond(), Phone = '650334'+Math.round(Math.Random()*10000));
    }
    
    // Content
    public static ContentVersion createContentVersion() {
        ContentVersion content = generateContentVersion();
        insert content;
        return content;
    }
    public static ContentVersion generateContentVersion() {
        return new ContentVersion(
              title = TEST_STRING
            , pathOnClient = TEST_STRING
            , versionData = TEST_BLOB
        );
    }
    
    // Events
    public static Event createEvent() {
        Event testEvent = generateEvent();
        insert testEvent;
        return testEvent;
    }
    public static Event generateEvent() {
        fixEventTriggers();
        return new Event(startDateTime = system.now(), endDateTime = system.now());
    }
    
    // Leads
    public static Lead createLead() {
        Lead lead = generateLead();
        insert lead;
        return lead;
    }
    public static Lead generateLead() {
        return new Lead(lastName = TEST_STRING, company = TEST_STRING, Cancel_Workflow__c = TRUE); // 2013-10-24 CGrooms: added Cancel_Workflow__c parameter to ensure that new Time Based WF Rules are not triggered.
    }
     
    // Opportunity
    public static Opportunity createOpportunity(Id accountId) {
        
        Opportunity opportunity = generateOpportunity(accountId);
        insert opportunity;
        return opportunity;
    }
    public static Opportunity generateOpportunity(Id accountId) {
        //updated by VG 12/21/2012
        Contact conObj = UTIL_TestUtil.generateContact(accountId);
                
        Database.DMLOptions dml = new Database.DMLOptions(); 
        dml.DuplicateRuleHeader.allowSave = true;
        dml.DuplicateRuleHeader.runAsCurrentUser = true;
        Database.SaveResult sr = Database.insert(conObj, dml);
        //insert conObj;
        
        return new Opportunity(
              accountId = accountId
            , bill_on_broadcast_calendar2__c = TEST_STRING
            , closeDate = TEST_DATE
            , confirm_direct_relationship__c = true
            , name = TEST_STRING
            , stageName = TEST_STRING
            , Primary_Billing_Contact__c = conObj.id  // This is to address the validation rule where Primary Billing Contact is now a required field for all opportunities 
        );
    }
    
    //updated by VG 12/21/2012
    public static Opportunity generateOpportunity(Id accountId, Id contactID) {
        return new Opportunity(
              accountId = accountId
            , bill_on_broadcast_calendar2__c = TEST_STRING
            , closeDate = TEST_DATE
            , confirm_direct_relationship__c = true
            , name = TEST_STRING
            , stageName = TEST_STRING
            , Primary_Billing_Contact__c = contactID
        );
    }
    
    /* Method to return the ID or an Opportunity Record Type by Name */
    public static Id findOpptyRTByName(String Name){
        Id theId = NULL;
        Schema.DescribeSObjectResult R = Opportunity.SObjectType.getDescribe();
        Map<String,Schema.RecordTypeInfo> rtMapById = R.getRecordTypeInfosByName();
        if(rtMapById.containsKey(Name)){
            theId = rtMapById.get(Name).getRecordtypeId();
        }
        return theId;
    }
    
    
    // Opportunity Line Items
    public static OpportunityLineItem createOpportunityLineItem(Id opportunityId, Id pricebookEntryId) {
        OpportunityLineItem lineItem = generateOpportunityLineItem(opportunityId, pricebookEntryId);
        insert lineItem;
        return lineItem;
    }
    public static OpportunityLineItem generateOpportunityLineItem(Id opportunityId, Id pricebookEntryId) {
        return new OpportunityLineItem(
              opportunityId = opportunityId
            , pricebookEntryId = pricebookEntryId
            , quantity = DEFAULT_QUANTITY
            , totalPrice = DEFAULT_PRICE
            , serviceDate = System.today()
            , end_date__c = System.today()
        );
    }
    
    // Pricebook
    public static Pricebook2 createPricebook() {
        Pricebook2 pricebook = generatePricebook();
        insert pricebook;
        return pricebook;
    }
    public static Pricebook2 generatePricebook() {
        return new Pricebook2(
            name = TEST_STRING,
            isActive = true
        );
    }
    
    // Pricebook Entries
    // fails if seeAllDate=true not set
    public static PricebookEntry createPricebookEntry(Id productId, Id pricebookId) {
        return createPricebookEntries(new Id[] { productId }, pricebookId)[0];
    }
    public static List<PricebookEntry> createPricebookEntries(List<Id> productIds, Id pricebookId) {
        Set<Id> haveStandardPrice = new Set<Id>();
        for(PricebookEntry pbe : [
            select product2Id 
            from PricebookEntry 
            where pricebook2.isStandard = true 
            and product2Id in :productIds 
        ]) {
            haveStandardPrice.add(pbe.product2Id);
        }
        List<PricebookEntry> standardPBEs = new List<PricebookEntry>();
        List<PricebookEntry> pricebookEntries = new List<PricebookEntry>();
        for(Id productId : productIds) {
            if(!haveStandardPrice.contains(productId)) {
                standardPBEs.add(generatePricebookEntry(productId, STANDARD_PRICEBOOK.id));
            }
            pricebookEntries.add(generatePricebookEntry(productId, pricebookId));
        }
        if(!standardPBEs.isEmpty())
            insert standardPBEs;
        insert pricebookEntries;
        return pricebookEntries;
    }
    public static PricebookEntry generatePricebookEntry(Id productId, Id pricebookId) {
        return new PricebookEntry(
              product2Id = productId
            , pricebook2Id = pricebookId
            , isActive = true
            , unitPrice = DEFAULT_PRICE
        );
    }
    
    // Products
    public static Product2 createProduct() {
        Product2 product = generateProduct();
        insert product;
        return product;
    }
    public static Product2 generateProduct() {
        return new Product2 (
            name=TEST_STRING,
            isActive = true,
            productcode = TEST_STRING,
            CanUseRevenueSchedule = True,
            NumberOfRevenueInstallments = 12,
            RevenueInstallmentPeriod = 'Monthly',
            RevenueScheduleType = 'Repeat'
        );
    }

    // Task
    public static Task createTask() {
        Task task = generateTask();
        insert task;
        return task;
    }
    public static Task generateTask() {
        return new Task();
    }
    
    // Task - Historical
    public static Task createCompletedTask(Account a) {
        Task task = generateCompletedTask(a);
        insert task;
        return task;
    }
    public static Task createCompletedTask(Opportunity o) {
        Task task = generateCompletedTask(o);
        insert task;
        return task;
    }
    public static Task generateCompletedTask(Account a) {
        return new Task(
            ActivityDate = Date.today().addDays(-30),
            Subject=generateRandomString(8),
            WhatId = a.Id,
            OwnerId = UserInfo.getUserId(),
            Status='Completed'
        );
    }
    public static Task generateCompletedTask(Opportunity o) {
        return new Task(
            ActivityDate = Date.today().addDays(-30),
            Subject=generateRandomString(8),
            WhatId = o.Id,
            OwnerId = UserInfo.getUserId(),
            Status='Completed'
        );
    }

    // User
    public static User createUser() {
        User testUser = generateUser();
        insert testUser;
        return testUser;
    }
    public static User generateUser() {
        String testString = generateRandomString(8);
        String testDomain = generateRandomString(12) + '.com';
        String testEmail = generateRandomEmail(testDomain);
        return new User(lastName = testString,
            userName = testEmail,
            profileId = SYSADMIN_PROFILE_ID,
            alias = testString,
            email = testEmail,
            emailEncodingKey = 'ISO-8859-1',
            languageLocaleKey = 'en_US',
            localeSidKey = 'en_US',
            timeZoneSidKey = 'America/Los_Angeles'
        );
    }
    
    /* Custom Object Generators */
    
    public static Traffic_Product__c createTrafficProduct() {
        Traffic_Product__c trafficProduct = generateTrafficProduct();
        insert trafficProduct;
        return trafficProduct;
    }
    public static Traffic_Product__c generateTrafficProduct() {
        return new Traffic_Product__c();
    }
    
    /* Object Process Functions */
    
    // Case Close
    public static Case closeCase(Case record) {
        record.status = CLOSED_CASE_STATUS;
        return record;
    }
    
    // Lead Convert
    public static Database.LeadConvert getLeadConvertObject(Id leadId) {
        Database.LeadConvert convertObject = new Database.Leadconvert();
        convertObject.setLeadId(leadId);
        convertObject.setConvertedStatus(CONVERTED_LEAD_STATUS);
        return convertObject;
    }
    
    // Opportunity Lose
    public static Opportunity closeLoseOpportunity(Opportunity opportunity) {
        opportunity.stageName = OPPTY_CLOSED_LOST_STAGE;
        return opportunity;
    }
    
    // Opportunity Win
    public static Opportunity closeWinOpportunity(Opportunity opportunity) {
        opportunity.stageName = OPPTY_CLOSED_WON_STAGE;
        return opportunity;
    }

    /* Memoized Properties */
    

    public static String CLOSED_CASE_STATUS {
        get {
            if(null == CLOSED_CASE_STATUS) {
                CLOSED_CASE_STATUS = [select masterLabel from CaseStatus where isClosed = true limit 1][0].masterLabel;
            }
            return CLOSED_CASE_STATUS;
        }
        private set;
    }
    
    public static String CONVERTED_LEAD_STATUS {
        get {
            if(null == CONVERTED_LEAD_STATUS) {
                CONVERTED_LEAD_STATUS = [select masterLabel from LeadStatus where isConverted = true limit 1][0].masterLabel;
            }
            return CONVERTED_LEAD_STATUS;
        }
        private set;
    }
    
    public static String OPPTY_CLOSED_LOST_STAGE {
        get {
            if(null == OPPTY_CLOSED_LOST_STAGE) {
                 OpportunityStage opptyStage = [select masterLabel from OpportunityStage where isWon = false and isClosed = true limit 1];
                 OPPTY_CLOSED_LOST_STAGE = opptyStage.masterLabel;
            }
            return OPPTY_CLOSED_LOST_STAGE;
        }
        private set;
    }

    public static String OPPTY_CLOSED_WON_STAGE
    {
        get {
            if(null == OPPTY_CLOSED_WON_STAGE) {
                OpportunityStage opptyStage = [select masterLabel from OpportunityStage where isWon = true limit 1];
                OPPTY_CLOSED_WON_STAGE = opptyStage.masterLabel;    
            }
            return OPPTY_CLOSED_WON_STAGE;
        }
        private set;
    }
    
    // fails if @seeAllData=true is not specified
    public static Pricebook2 STANDARD_PRICEBOOK {
        get {
            if(STANDARD_PRICEBOOK == null) {
                STANDARD_PRICEBOOK = [select name, isActive from Pricebook2 where isStandard = true];
            }
            return STANDARD_PRICEBOOK;
        }
        private set;
    }
    
    public static Id SYSADMIN_PROFILE_ID
    {
        get {
            if(null == SYSADMIN_PROFILE_ID) {
                SYSADMIN_PROFILE_ID = [select id from Profile where name = 'System Administrator'][0].id;
            }
            return SYSADMIN_PROFILE_ID; 
        }
        private set;
    }
        
    /* Random Functions */

    private static Set<String> priorRandoms;
    public static String generateRandomString(){return generateRandomString(null);}
    public static String generateRandomString(Integer length){
        if(priorRandoms == null)
            priorRandoms = new Set<String>();

        if(length == null) length = 1+Math.round( Math.random() * 8 );
        String characters = 'abcdefghijklmnopqrstuvwxyz1234567890';
        String returnString = '';
        while(returnString.length() < length){
            Integer charpos = Math.round( Math.random() * (characters.length()-1) );
            returnString += characters.substring( charpos , charpos+1 );
        }
        if(priorRandoms.contains(returnString)) {
            return generateRandomString(length);
        } else {
            priorRandoms.add(returnString);
            return returnString;
        }
    }
    public static String generateRandomEmail(){return generateRandomEmail(null);}
    public static String generateRandomEmail(String domain){
        if(domain == null || domain == '')
            domain = generateRandomString() + '.com';
        return generateRandomString() + '@' + domain;
    }

    public static String generateRandomUrl() {
        return 'http://' + generateRandomString() + '.com'; 
    }

    /* Trigger Fix Functions */
    
    // Functions in this section address data dependencies in existing triggers
    
    // Event
    private static Boolean eventTriggersFixed = false;
    private static void fixEventTriggers() {
        if(!eventTriggersFixed) {
            // GSA_EventTrigger (after delete, after insert, after update)
            // Data dependency on CustomSetting_GSA_Public_Calendar__c having
            // an entry for 'Production Deployment Calendar ID'
            if(CustomSetting_GSA_Public_Calendar__c.getInstance('Production Deployment Calendar ID') == null) {
                insert new CustomSetting_GSA_Public_Calendar__c(
                    name = 'Production Deployment Calendar ID'
                );
            }
            eventTriggersFixed = true;
        }
    }

    /* Test Functions */
    @isTest
    private static void testFindOpptyRTByName() {
        // Validate that a random String matching no RT returns NULL
        System.assertEquals(NULL,findOpptyRTByName(generateRandomString()));
        
        // Validate that "Master" returns an ID. "Master" is the default RT, and cannot be renamed/deleted
        system.assertNotEquals(null, findOpptyRTByName('Master'));
    }
    
    @isTest
    private static void customObjectCreatorsTest() {
        assertIdNotNull(createTrafficProduct());
    }

    @isTest
    private static void standardObjectCreatorsTest() {
        assertIdNotNull(createUser());
        assertIdNotNull(createCase());
        assertIdNotNull(createLead());
        Account testAccount = createAccount();
        assertIdNotNull(testAccount);
        assertIdNotNull(createContact(testAccount.id));
        assertIdNotNull(createOpportunity(testAccount.id));
        assertIdNotNull(createAttachment(testAccount.id));
        assertIdNotNull(createTask());
        assertIdNotNull(createEvent());
        assertIdNotNull(createContentVersion());
    }
    
    @isTest(seeAllData=true)
    private static void testProductCreators() {
        Pricebook2 pricebook = createPricebook();
        assertIdNotNull(pricebook);
        Product2 product = createProduct();
        assertIdNotNull(product);
        PricebookEntry pricebookEntry = createPricebookEntry(product.id, pricebook.id);
        assertIdNotNull(pricebookEntry);
        Account testAccount = createAccount();
        Opportunity testOppty = generateOpportunity(testAccount.id);
        testOppty.pricebook2Id = pricebook.id;
        insert testOppty;
        assertIdNotNull(createOpportunityLineItem(testOppty.id, pricebookEntry.id));
    }

    @isTest
    private static void testCloseCase() {
        Case testCase = createCase();
        closeCase(testCase);
        update testCase;
        testCase = [select isClosed from Case where id = :testCase.id];
        system.assertEquals(true, testCase.isClosed);
    }

    @isTest
    private static void testLeadConversion() {
        Lead testLead = createLead();
        Database.convertLead(getLeadConvertObject(testLead.id));
        testLead = [select isConverted from Lead where id = :testLead.id];
        system.assertEquals(true, testLead.isConverted);
    }
    
    @isTest
    private static void testCloseLoseOpportunity() {
        Opportunity testOppty = createOpportunity(createAccount().id);
        closeLoseOpportunity(testOppty);
        update testOppty;
        testOppty = [select isClosed, isWon from Opportunity where id = :testOppty.id];
        system.assertEquals(true, testOppty.isClosed);
        system.assertEquals(false, testOppty.isWon);
    }
    
    @isTest
    private static void testCloseWinOpportunity() {
        Opportunity testOppty = createOpportunity(createAccount().id);
        closeWinOpportunity(testOppty);
        Test.startTest();
        testOppty.Lead_Campaign_Manager__c = UserInfo.getUserId();
        update testOppty;
        testOppty = [select isWon from Opportunity where id = :testOppty.id];
        system.assertEquals(true, testOppty.isWon);
        Test.stopTest();
    }

    @isTest 
    private static void randomFunctionsTest() {
        system.assertNotEquals(null, generateRandomString());
        system.assertNotEquals(null, generateRandomEmail());
        system.assertNotEquals(null, generateRandomUrl());
    }
    
    @isTest
    private static void testRandomDoesNotRepeat() {
        // With 36 potential characters, a single length string will have a 75% chance
        // of recurring after 10 iterations.  To ensure we skip repeated randoms run through
        // 10 iterations adding each to the set and confirm the final set size is 10.  Because
        // this is probabilistic this test will catch a bug only 75% of the time.
        Set<String> randoms = new Set<String>();
        for(Integer i = 0; i < 10; i++) {
            randoms.add(generateRandomString(1));
        }    
        system.assertEquals(10, randoms.size());
    }
    
    @isTest(seeAllData=true) // see all data required for STANDARD_PRICEBOOK param
    private static void testMemoizedFunctions() {
        system.assertNotEquals(null, CLOSED_CASE_STATUS);
        system.assertNotEquals(null, CONVERTED_LEAD_STATUS);
        system.assertNotEquals(null, OPPTY_CLOSED_LOST_STAGE);
        system.assertNotEquals(null, OPPTY_CLOSED_WON_STAGE);
        system.assertNotEquals(null, SYSADMIN_PROFILE_ID);
        assertIdNotNull(STANDARD_PRICEBOOK);
    }
    
    /* Test Helpers */
    
    private static void assertIdNotNull(SObject obj) {
        system.assertNotEquals(null, obj.id);
    }
  
     public static LIst<Auto_Assign_Biller__c> createAutoAssignCS(Id userId,String username, Integer noofRecords){

        LIst<Auto_Assign_Biller__c> settingLst = new List<Auto_Assign_Biller__c>();

        for(Integer index = 0;index < noofRecords; index++){
            Auto_Assign_Biller__c setting = new Auto_Assign_Biller__c();
            setting.Name = userId;
            setting.Campaign_Manager__c = userName;
            setting.Billing_Coordinator_ID__c = userId;
            setting.Billing_Coordinator__c = userName;
            
            settingLst.add(setting);
        }
        
        return settingLst;
    }   
}