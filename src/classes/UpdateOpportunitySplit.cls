public Class UpdateOpportunitySplit{
    public static Boolean hasChanges(String field, SObject oldRecord, SObject newRecord) {
        if (oldRecord == null) {
            return true;
        }
        return (oldRecord.get(field) != newRecord.get(field));
    } 
    public static Map<Id,List<Opportunity_Split__c>> getOppMap(Set<Id>oppIds,Set<Id>ownerIds,List<Opportunity>oppList){
        /*mapOppSpilt as SalesPerson Id as key & List Opportunity_Split__c as value*/
        System.debug('oppIds ='+oppIds+'ownerIds ='+ownerIds+'oppList ='+oppList);
        Map<Id,List<Opportunity_Split__c>> mapOppSpilt = new Map<Id,List<Opportunity_Split__c>>();
        List<Opportunity_Split__c> oppSplitList = [Select o.Split__c, o.Salesperson_Name__c,o.Salesperson__c, o.Name From Opportunity_Split__c o where o.Salesperson__c in :ownerIds and o.Opportunity__c in :oppIds];
        System.debug('mapList'+oppSplitList);
        /*for putting values in mapOppSpilt*/
        for(Opportunity opp :oppList){
            for(Opportunity_Split__c op :oppSplitList){
                List<Opportunity_Split__c> oppSpList = new List<Opportunity_Split__c>();
                if(mapOppSpilt != null && mapOppSpilt.containsKey(op.Salesperson__c)){
                    System.debug('list in if' + op);
                    oppSpList = mapOppSpilt.get(op.Salesperson__c);
                    oppSpList.add(op);
                    mapOppSpilt.put(op.Salesperson__c ,oppSpList);  
                }else{
                    System.debug('list in else' + op);
                    oppSpList.add(op);
                    mapOppSpilt.put(op.Salesperson__c ,oppSpList);
                }
                
            }   
            System.debug('mapOppSpilt' + mapOppSpilt);
            //System.debug('oppSpList' + oppSpList);
             
        }
         return  mapOppSpilt;  
    }
}