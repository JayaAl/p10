/**
 * @name: UpfrontJunctionTriggerHandler
 * @desc: Used to handle all trigger operation for Upfront_Junction__c object
 * @author: Lakshman(sfdcace@gmail.com)
 */
public class UpfrontJunctionTriggerHandler {
    public Boolean isExecuting;
    public Integer size;
    
    public UpfrontJunctionTriggerHandler(Boolean isExecuting, Integer size){
        this.isExecuting = isExecuting;
        this.size = size;       
    }
    
    /**
     * @desc: Method - onBeforeInsert is used to perform the before insert operation
     * @param: upfrontJunctions array of Upfront_Junction__c records
     */ 
    public void onAfterInsert(Upfront_Junction__c[] upfrontJunctions){
        Upfront_SubmitAndNotifyQueue submitAndNotify = new Upfront_SubmitAndNotifyQueue();
        submitAndNotify.submitAndNotifyQueueAfterInsert(upfrontJunctions);
    }
    
    
     /**
      * @desc: Method onAfterUpdate is used to perform the after update operation
      * @param: upfrontJunctions array of Upfront_Junction__c records
      * @param: oldUpfrontJunctionMap: Upfront_Junction__c map with old values
      * @param: newUpfrontJunctionMap Upfront_Junction__c map with new values
      */
    public void onAfterUpdate(Upfront_Junction__c[] upfrontJunctions, Map<Id,Upfront_Junction__c> oldUpfrontJunctionMap, Map<Id,Upfront_Junction__c> newUpfrontJunctionMap){
        
    }  
    
}