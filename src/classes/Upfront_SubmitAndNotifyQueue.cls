/**
 * @name: Upfront_SubmitAndNotifyQueue
 * @desc: This class is used automatically submit Ufront for approval once it is attached to Upfront Junction and send email notification
 * @author: Lakshman(sfdcace@gmail.com)
 */
public class Upfront_SubmitAndNotifyQueue{
	/**
	 * @name: submitAndNotifyQueueAfterInsert: This method is used to automatically submit Ufront for approval once it is attached to Upfront Junction 
	 * 		  and send email notification after insert og Upfront Junction
	 * @param: upfrontJunctions Accepts Array of Upfront_Junction__c
	 */
    public void submitAndNotifyQueueAfterInsert(Upfront_Junction__c[] upfrontJunctions){
        Set<Id> ujIds = new Set<Id>();
        for(Upfront_Junction__c  uj : upfrontJunctions){
            ujIds.add(uj.Id);
        }
        Set<Id> userIds = new Set<Id>();
        for(GroupMember g: [Select UserOrGroupId from GroupMember where GroupId In (Select q.QueueId From QueueSobject q where sobjecttype = 'Upfront__c' AND Queue.DeveloperName = 'PYM_Team')]){
            userIds.add(g.UserOrGroupId);
        }
        system.debug('*****userIds'+userIds);
        for(Upfront_Junction__c  uj : upfrontJunctions){
            ujIds.add(uj.Id);
        }
        
        Map<Id, Upfront_Junction__c> mapOfUpfrontToUJ = new Map<Id, Upfront_Junction__c>();
        for(Upfront_Junction__c  uj : [Select Upfront__r.Id,Upfront__r.Submitted_for_Approval__c, Account__r.Name, Upfront__r.Name, Upfront__r.Amount_Spent__c, Upfront__r.Current_Tier__c from Upfront_Junction__c  where Id In: ujIds]){
        	if(!uj.Upfront__r.Submitted_for_Approval__c){
            	mapOfUpfrontToUJ.put(uj.Upfront__r.Id, uj);
        	}
        }
        
        if( !mapOfUpfrontToUJ.isEmpty()){
	        /* The process request argument to operate on later. */
	        List<Approval.ProcessRequest> reqs = new List<Approval.ProcessRequest>();
	        
	        for(Id id : mapOfUpfrontToUJ.keySet()) {
	            /* Create ProcessSubmitRequest for Approval. */    
	            Approval.ProcessSubmitRequest req = new Approval.ProcessSubmitRequest();
	            req.setObjectId(id);
	            req.setComments('AUTOMATED: Submitting request for approval.');
	            reqs.add(req);
	        }
	        /* Process the requests. */  
	        Approval.ProcessResult[] results = Approval.process(reqs, false);  
	
	        for(Approval.ProcessResult result: results){
	            if(!result.isSuccess()){
	                mapOfUpfrontToUJ.remove(result.getEntityId());
	                system.debug('Error caught=>' + result.getErrors()[0]);
	            }
	        }
	        
	        List<Messaging.SingleEmailMessage> mails = new List<Messaging.SingleEmailMessage>();
	        
	        for(Id id : mapOfUpfrontToUJ.keySet()){
	            for(Id uId : userIds){
	                Messaging.SingleEmailMessage mail = new Messaging.SingleEmailMessage();
	                mail.setSubject('New Upfront Associated: '+mapOfUpfrontToUJ.get(id).Account__r.Name);
	                string body = mapOfUpfrontToUJ.get(id).Upfront__r.Name + ' has been added to '+ mapOfUpfrontToUJ.get(id).Account__r.Name+
	                              '. Please review for Approval<br/><br/>' +
	                              'Amount Spent: ' + mapOfUpfrontToUJ.get(id).Upfront__r.Amount_Spent__c + '<br/>'+
	                              'Current Tier: ' + mapOfUpfrontToUJ.get(id).Upfront__r.Current_Tier__c + '<br/><br/>'+
	                              'Upfront Link: ' + URL.getSalesforceBaseUrl().toExternalForm() + '/'+ id;
	                mail.setTargetObjectId(uId);
	                mail.setSaveAsActivity(false);
	                mail.setHtmlBody(body);
	                mails.add(mail);
	            }
	        }
	        if(! mails.isEmpty()){
	            try{
	                Messaging.sendEmail(mails);
	            }catch(Exception ex){
	                system.debug('Exception caught'+ex);
	            }
	        }
        }
    }
}