global with sharing class UploadAttachmentCtrlr{

    public Attachment attachment {
      get {
          if (attachment == null)
            attachment = new Attachment();
          return attachment;
        }
      set;
      }

    /*public Attachment attachmentAudio {
      get {
          if (attachmentAudio == null)
            attachmentAudio = new Attachment();
          return attachmentAudio;
        }
      set;
      }*/
    public string audioVoiceValue {get;set;} 
    public string uploadTypeStr ;
    public string getUploadTypeStr(){
      return uploadTypeStr;
    }

    public void setUploadTypeStr(String uploadTypeStrVar ){
      this.uploadTypeStr = uploadTypeStrVar;
      system.debug('setUploadTypeStr..');
      //fetchExistingAttachmentLst([Select Id,Name,Description from Attachment where parentId =: parentIdStr]);
      //system.debug('dispAttachmentAudio :::'+dispAttachmentAudio);
      //system.debug('dispAttachmentBanner :::'+dispAttachmentBanner);
    }
    public string parentIdStr;
    public boolean noteSuccessful {get;set;}

    public string getparentIdStr(){
      return parentIdStr;
    }

    public void setParentIdStr(String parentId){
        system.debug('setter parentidStr'+parentIdStr);
        this.parentIdStr = parentId;
        
    }
    public String quoteLineTypeStr {get;set;}
    public string bannerNameStr {get;set;}
    public string audioNameStr {get;set;}
    public string audioScriptStr {get;set;}
    public string opptyId {get;set;}
    public Attachment dispAttachmentAudio;
    public Id attachmentToDeleteId {get;set;}

    public Attachment getDispAttachmentAudio(){
      fetchExistingAttachmentLst([Select Id,Name,Description from Attachment where parentId =: parentIdStr]);
      system.debug('dispAttachmentAudio :::'+dispAttachmentAudio);
      return dispAttachmentAudio;
    }  

    public void setDispAttachmentAudio(Attachment attachmentVar){
        system.debug('setter setDispAttachmentAudio'+attachmentVar);

        this.dispAttachmentAudio = attachmentVar;
        
    }

    public Attachment dispAttachmentBanner;
    
    public Attachment getDispAttachmentBanner(){
      fetchExistingAttachmentLst([Select Id,Name,Description from Attachment where parentId =: parentIdStr]);
      system.debug('dispAttachmentAudio :::'+dispAttachmentAudio);
      return dispAttachmentBanner;
    }  

    public void setDispAttachmentBanner(Attachment attachmentVar){
        system.debug('setter setDispAttachmentAudio'+attachmentVar);
        
        this.dispAttachmentBanner = attachmentVar;
        
    }
    public List<Attachment> existingAttachmentLst {get;set;}

    public void fetchExistingAttachmentLst(List<Attachment> attachmentLst){     
      

      system.debug('attachmentLst :::::==>'+attachmentLst);
      if(attachmentLst != null && attachmentLst.size() > 0) {
        for(Attachment attachment : attachmentLst){
          if((attachment.Name).startsWith(uploadTypeStr)){
             if(uploadTypeStr == 'Audio' ){
              dispAttachmentAudio = new Attachment();
              dispAttachmentAudio.Id = attachment.Id;
              dispAttachmentAudio.Name = attachment.Name;
            }else{
              dispAttachmentBanner = new Attachment();
              dispAttachmentBanner.Id = attachment.Id;
              dispAttachmentBanner.Name = attachment.Name;

            }
          }
        }
      }

      
    }

    public UploadAttachmentCtrlr(){
      system.debug('Component constructor'+parentIdStr);
      audioVoiceValue = 'Male';
      noteSuccessful =false;
      fetchExistingAttachmentLst([Select Id,Name,Description from Attachment where parentId =: parentIdStr]);
      system.debug('dispAttachmentAudio :::'+dispAttachmentAudio);
      system.debug('dispAttachmentBanner :::'+dispAttachmentBanner);
      system.debug('Component constructor ends'+existingAttachmentLst);
    }

    public void addNoteAction(){
      system.debug('inside addNoteAction ...');
    Note noteObj = new Note();
    noteObj.body = audioScriptStr;
    noteObj.title = 'Note for Quote in Voice '+audioVoiceValue;
    noteObj.isPrivate = false;
    noteObj.ParentId = parentIdStr;
    try{

      insert noteObj; 
      noteSuccessful = true;
       ApexPages.addMessage(new ApexPages.message(ApexPages.severity.INFO,'Script added successfully.'));  
       system.debug('inside addNoteAction ...'+noteObj.Id);
    }catch(Exception ex){
      ApexPages.addMessage(new ApexPages.message(ApexPages.severity.ERROR,'Error uploading script, please contact Admin.'));
    }
    

  }

   public PageReference upload() {
        List<Attachment> attchmentLst = new List<Attachment>();
        if(attachment.Body == null){
            ApexPages.addMessage(new ApexPages.message(ApexPages.severity.ERROR,'Please Select file to upload.'));  
            attachment = new Attachment();
            return null;
        }
        attachment.OwnerId = UserInfo.getUserId();
        attachment.ParentId = parentIdStr; // the record the file is attached to
        attachment.name = (uploadTypeStr+'-'+bannerNameStr);
        //attachment.IsPrivate = true;
        attchmentLst.add(attachment);

       /* if(attachmentAudio != null && attachmentAudio.body != null){
            attachmentAudio.OwnerId = UserInfo.getUserId();
            attachmentAudio.ParentId = parentIdStr; // the record the file is attached to
            attachmentAudio.name = ('Audio-'+audioNameStr);
          //  attachmentAudio.IsPrivate = true;
            attchmentLst.add(attachmentAudio);          
        }*/

        try {
          insert attchmentLst;
          
          if(uploadTypeStr == 'Audio' ){
            dispAttachmentAudio = new Attachment();
            dispAttachmentAudio.Id = attchmentLst[0].Id;
            dispAttachmentAudio.Name = attchmentLst[0].Name;
          }else{
            dispAttachmentBanner = new Attachment();
            dispAttachmentBanner.Id = attchmentLst[0].Id;
            dispAttachmentBanner.Name = attchmentLst[0].Name;

          }
        } catch (DMLException e) {
          ApexPages.addMessage(new ApexPages.message(ApexPages.severity.ERROR,'Error uploading Creative, Please Retry.'));
          return null;
        } finally {
          attachment = new Attachment(); 
          
        }
    
        ApexPages.addMessage(new ApexPages.message(ApexPages.severity.INFO,'Creative uploaded successfully, CLick on Preview to view it. '));
        return null;
      } 

     
   global void delAttachmentAction(){
        system.debug('inside delAttachmentAction...'+attachmentToDeleteId);
        Id attachmentToDel; 
        if(uploadTypeStr == 'Audio'){
            if(dispAttachmentAudio != null){
              system.debug('audio..'+dispAttachmentAudio.id);
              attachmentToDel = dispAttachmentAudio.id;
              dispAttachmentAudio = null;
            }
          }else{
            if(dispAttachmentBanner != null){
              system.debug('banner..'+dispAttachmentBanner.id);
              attachmentToDel = dispAttachmentBanner.id;
              dispAttachmentBanner = null;  
            }            
          }

        delete [Select id from Attachment where id =: attachmentToDel ];


    }

    public Pagereference reloadPage(){
      Id quoteiD = ApexPages.currentPage().getParameters().get('QID');
      Pagereference pageRef = new PageReference('/apex/ATG_CreativePage?QID='+quoteiD+'&QLID='+parentIdStr);
      return pageRef;
    }

    @RemoteAction
    global static void RemoveAttachmentAction(Id attachmentToDel){

      delete [Select id from Attachment where id =: attachmentToDel ];
    }
}