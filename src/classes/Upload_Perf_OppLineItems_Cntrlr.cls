/* Class Name: Upload_Perf_OppLineItems_Cntrlr
* Created Date: 10/29/2012
* Author: Vardhan Gupta (vgupta@pandora.com)
* Purpose: Accept a CSV file from the user, parse it and upload it in Opportunity Line Items. The CSV file is not stored in the system.
* Dependent Pages: This class is referenced by this page --> Upload_Perf_OpportunityLineItems.page 
* 
* Updated: 2016-09-02
* Updated By: Casey Grooms
* Changes: Re-Wrote most functionality to break out individual functions from monolithic codeblock
* 			Updated functionality to allow for multi-currency, Currency now dependent on the parent Opportunitiy's Currency
* 			Removed many deprecated and unused functions and variables, cleaned up process order to make more logical (or so I believe)
* 			Added additional comments documenting the process where I thought it would be helpful
*/

public class Upload_Perf_OppLineItems_Cntrlr {
    
    /* file details from apex:inputFile  */
    public string nameFile{get;set;}
    public Blob contentFile{get;set;}
    
    /* vars for display on the Visualforce page */
    public List<OpportunityLineItem> opportunityLineItemsToUpload {get;set;} // displayed on Page.Upload_Perf_OppLineItems
    public List<ErrorWrapper> listErrorWrapper{get;set;} // displayed on Page.Upload_Perf_OppLineItems
    public Boolean uploadSuccessful{get;set;} // flags to conditionally display info on the VF page
    public Boolean uploadFailure{get;set;} // flags to conditionally display info on the VF page
    public static Id PERF_CSV_SAMPLE_TEMPLATE_DOCID {get;set;} // displayed on Page.Upload_Perf_OppLineItems
    
    //These three variables are used to display user-friendly error messages if required fields are empty.
    private static Integer rowCount {get;set;}
    public Integer currentColumn;
    
    /* collections used to collect data during processing of the files */
    public static Set<String> PerformanceTypeValues; // populated in instantiatePicklistValues, used during processing
    public static Set<String> costTypeValues;  // populated in instantiatePicklistValues, used during processing
    public static Map<String, OpportunityLineItem> mapOpliKeys;
    
    /* Constructor simply gathers the sample template */
    public Upload_Perf_OppLineItems_Cntrlr() {
        PERF_CSV_SAMPLE_TEMPLATE_DOCID = Label.Performance_Bulk_Upload_Products_Template_Sample;
    }
    
    /* Function: Primary function invoked from CommandButton to process the selected CSV File */
    public Pagereference ReadFile(){
        
        // prepare containers for later use
        listErrorWrapper = new List<ErrorWrapper>();
        Set<Id> setOpptyIds = new Set<Id>();
        mapOpliKeys = new Map<String, OpportunityLineItem>();
        
        // Perform data preperation
        instantiatePicklistvalues(); //Get the picklist values from Salesforce for Performance Type and Cost Type
        mapProductName_to_Currency_to_Id = populateMapProductCurrencyId();
        
        List<List<String>> parsedCSV = parseCSV();
        for(List<String> theRow:parsedCSV){ // Get a Set of Opportunity Ids from column 0 of the import file
            setOpptyIds.add((Id)theRow[0]);
        }
        mapOpptyCurrency = populateMapOpptyCurrency(setOpptyIds);
        
        rowCount = 0;
        List<OpportunityLineItem> opportunityLineItemsList = new List<OpportunityLineItem>();
        for (List<String> theRow:parsedCSV){ // For each ROW in the CSV
            rowCount++;
            currentColumn = 0;
            try{
                OpportunityLineItem opli = generateLineItemFromRow(theRow); // generate the OLI based on data from the CSV 
                // if we have *any* errors parsing the results there is no need to add to opportunityLineItemsList 
                // as we will not be inserting any records, however we wil still process the other rows so that we can capture all validation error messages
                if (listErrorWrapper.isEmpty()) { 
                    opportunityLineItemsList.add(opli); //add Opportunity Line Item to the List. This list will be used to populate the wrapper class.
                }
            } catch (Exception e) {
                uploadSuccessful = false;
                OPT_PROD_STATIC_HELPER.runForProds = false;
                ApexPages.Message errormsg = new ApexPages.Message(ApexPages.severity.ERROR,'An error has occured. Please check the template. ' + 'Row# =  ' + (rowcount+1) + ', ' + ' Column# =  ' + currentColumn + ', ' + '  Description = -- ' +e.getMessage() );
                ApexPages.addMessage(errormsg);
            }
        } // END ROW
        
        if (!listErrorWrapper.isEmpty()) { //if there are any errors in the CSV, stop the process and return back to VF page.
            uploadFailure = true;
            OPT_PROD_STATIC_HELPER.runForProds = false;
            return null;
        }
        
        //if the OPLI list was created correctly, then create a List<OLIWrapper>. This function also populates the mapOpliKeys collection
        List<OLIWrapper> listOLIWrapper = new List<OLIWrapper>();
        if (!opportunityLineItemsList.isEmpty()) {
            listOLIWrapper = generateListOLIWrapper(opportunityLineItemsList);
        }
        
        //if the Wrapper List size is greater than zero then calculate the roll-ups for the 4 roll-up fields. 
        if (!listOLIWrapper.isEmpty()) {
            opportunityLineItemsToUpload = calculateRollUps(listOLIWrapper);
        }
        
        // finally insert the new OpportunityLineItem records
        if(!opportunityLineItemsToUpload.isEmpty()){
            insert_OpportunityLineItems(opportunityLineItemsToUpload);
        }
        
        // reload opportunityLineItemsToUpload for display in the UI, this step is necessary as
        // prior to inserting these records do not have a CurrencyISOCode assigned as they inherit
        // it from the Opportunity, we also can not assign this value during insert.
        Set<Id> SetNewIds = new Set<Id>();
        for(OpportunityLineItem li:opportunityLineItemsToUpload){
            SetNewIds.add(li.Id);
        }
        opportunityLineItemsToUpload = [
            SELECT Id,Opportunityid,Performance_Type__c,Impression_s__c,Clicks__c,Conversions__c,UnitPrice,eCPM__c
            FROM OpportunityLineItem
            WHERE Id IN :setNewIds
        ];        
        return null;
    }
    
    /* Function: CSV parse logic; read Blob data from the apex:inputFile and populate top level allRows and allRowFields values */
    public List<List<String>> parseCSV() {
        String contents = contentFile.toString();
        boolean skipHeaders = true;
        List<List<String>> allFields = new List<List<String>>();
        
        // replace instances where a double quote begins a field containing a comma in this case you get 
        // a double quote followed by a doubled double quote do this for beginning and end of a field
        contents = contents.replaceAll(',"""',',"DBLQT').replaceall('""",','DBLQT",');
        // now replace all remaining double quotes - we do this so that we can reconstruct fields with 
        // commas inside assuming they begin and end with a double quote
        contents = contents.replaceAll('""','DBLQT');

        List<String> lines = new List<String>();
        try {
            lines = contents.split('\n');
        } catch (System.ListException e) {
            System.debug('Limits exceeded?' + e.getMessage());
        }
        Integer num = 0;
        for(String line : lines) {
            if (line.replaceAll(',','').trim().length() == 0) break; // check for blank CSV lines (only commas)
            List<String> fields = line.split(',');
            List<String> cleanFields = new List<String>();
            String compositeField;
            Boolean makeCompositeField = false;
            for(String field : fields) {
                field = field.trim();
                if (field.startsWith('"') && field.endsWith('"')) {
                    cleanFields.add(field.replaceAll('DBLQT','"'));
                } else if (field.startsWith('"')) {
                    makeCompositeField = true;
                    compositeField = field;
                } else if (field.endsWith('"')) {
                    compositeField += ',' + field;
                    cleanFields.add(compositeField.replaceAll('DBLQT','"'));
                    makeCompositeField = false;
                } else if (makeCompositeField) {
                    compositeField +=  ',' + field;
                } else {
                    cleanFields.add(field.replaceAll('DBLQT','"'));
                }
            }
            allFields.add(cleanFields); // Add all fields back to the List<String> for the current row
        }
        if (skipHeaders) allFields.remove(0);
        return allFields;   
    }  
    
    /* Function: generate List<OLIWrapper> based on the provided List<OpportunityLineItem>, also populates the mapOpliKeys collection */
    public List<OLIWrapper> generateListOLIWrapper(List<OpportunityLineItem> listOLIs){
        List<OLIWrapper> theList = new List<OLIWrapper>();
        //start populating the list wrapper.
        for (integer i = 0; i < listOLIs.size() ; i++) {
            OLIWrapper wrapper = new OLIWrapper(i);
            wrapper.isNew = true;
            wrapper.item = listOLIs.get(i); //item is OPLI object.
            
            //Roll-up Key a concatenation of Opportunity.Id+Performance_Type__c+Rate__c+integer.
            wrapper.OpliRollUpKey = listOLIs.get(i).OpportunityId + '-' 
                + listOLIs.get(i).Performance_Type__c + '-' 
                + listOLIs.get(i).Rate__c
                + '#' + i; // add the integer to the end to force all keys to be unique. This forces all rows in the file to be saved as individual OLIs
            mapOpliKeys.put(wrapper.OpliRollUpKey, wrapper.item); //Populate the Map which will be used to calcualte the roll-ups.
            theList.add(wrapper);
        }
        return theList;
    }
    

    
    /* Function: Create an OpportunityLineItem given the CSV Row data */
    public OpportunityLineItem generateLineItemFromRow(List<String> theRow ){
        OpportunityLineItem opli = new OpportunityLineItem(Quantity = 1.00); // Instantiate new Opportunity Line Item, Quantity always defaulted to 1.00
        String product_type = ''; //To determine whether its web/mobile.
        String theOLI = theRow[0]; // TODO: Dynamically find the Opportunity ID column instead of hard coding
        String currencyISO = mapOpptyCurrency.get(theOLI); // get the Currency
        
        for(integer column=0;column<theRow.Size();column++){ // For each COLUMN
            currentColumn = column; // populate the top level variable in case there are any errors generated
            if(mapColumnMetaData.containsKey(column)){ // If this Column is one of those Mapped
                OpptyBulkUploadColumn__mdt colMetaData = mapColumnMetaData.get(column);
                String fieldName = colMetaData.Label;
                Boolean required = colMetaData.Required__c;
                String fieldType = colMetaData.Field_Type__c;
                Object value = null;
                system.debug('>>>> Expected Column: '+colMetaData);
                system.debug('>>>> Cell Value: '+theRow[column]);
                
                // Cast the value as a variable with the proper type
                if(fieldType == 'String'){
                    value = (String)theRow[column];
                } else if(fieldType == 'Double'){
                    value = Double.valueOf(theRow[column]);
                } else if(fieldType == 'Date'){
                    value = Date.parse(theRow[column]);
                } else if(fieldType == 'Id'){
                    value = (Id)theRow[column];
                }
                
                if(fieldName=='Cost_Type__c' && !isValidCostType((String)value)){
                    addError(rowCount, value + ' --- Cost Type not found in Salesforce', theOLI);
                    continue;
                } else if(fieldName=='Performance_Type__c'){
                    if(isValidPerformanceType((String)value)){
                        product_type = isWebOrMobile((String)value);
                    } else {
                        addError(rowCount, value + ' --- Performance Type not found in Salesforce', theOLI);
                        continue;
                    }
                }
                
                opli = populateOLI(opli, rowCount, theOLI, fieldName, value, required);
            }
        } // End COLUMN
        
        // Use updated mapCurrency_to_ProductName_to_Id to dynamically determine the PBE.Id to use here
        if(product_type <> '' && product_type.equalsIgnoreCase('web')){
            opli.PricebookEntryID = mapProductName_to_Currency_to_Id.get('Web').get(currencyISO); // WEB_PRICEBOOK_ENTRY_ID;
        } else {
            opli.PricebookEntryID = mapProductName_to_Currency_to_Id.get('Mobile').get(currencyISO); // MOBILE_PRICEBOOK_ENTRY_ID;
        }
        
        if(opli.ServiceDate <> null && opli.End_Date__c <> null){
            opli.Duration__c = opli.ServiceDate.daysBetween(opli.End_Date__c)+1;
        } else {
            opli.Duration__c = null;
        }
        return opli;
    }
    
    /* Function will group all records by a combination of (Performance Type - Opportunity ID - Rate) and 
    * 		perform a roll-up/summation on the following fields mentioned in the spreadsheet. 
    * 	1)  Impressions: Sum total of 'Impressions'.
    * 	2)  Clicks: Sum total of 'Clicks'.
    * 	3)  Conversions: Sum total of 'Conversions'.
    * 	4)  Revenue: Sum total of 'Revenue' amount.
    */
    public List<OpportunityLineItem> calculateRollUps(List<OLIWrapper> listOLIWrp) {
        Map<String, OpportunityLineItem> mapOLIs = new Map<String, OpportunityLineItem>();
        OpportunityLineItem opliToInsert = new OpportunityLineItem();
        List<OpportunityLineItem> toReturn = new List<OpportunityLineItem>();
        String rollUpKey = '';
        //Wrapper list may contain duplicates.
        for (integer i=0; i < listOLIWrp.size(); i++ ) {
            //check if the Map of OPLIs contains the rollupKey
            if (mapOpliKeys.containsKey(listOLIWrp[i].OpliRollUpKey)) {
                rollUpKey = listOLIWrp[i].OpliRollUpKey;
                //If the OPLI in the Wrapper list is not the same as the OPLI in the Map then perform the summation of the 4 desired fields.
                if (listOLIWrp[i].item != mapOpliKeys.get(listOLIWrp[i].OpliRollUpKey)){      
                    opliToInsert = mapOpliKeys.get(listOLIWrp[i].OpliRollUpKey); //get the Opportunity Line Item based on the rollUpKey
                    opliToInsert.Impression_s__c += listOLIWrp[i].item.Impression_s__c; //Roll-up the number of impressions.
                    if (listOLIWrp[i].item.Clicks__c != null) opliToInsert.Clicks__c += listOLIWrp[i].item.Clicks__c; // Roll-up the number of clicks.
                    if(listOLIWrp[i].item.Conversions__c != null) opliToInsert.Conversions__c += listOLIWrp[i].item.Conversions__c; // Roll-up the number of conversions.
                    opliToInsert.UnitPrice += listOLIWrp[i].item.UnitPrice; //Roll-up the total Revenue.
                    mapOLIs.put(rollUpKey, opliToInsert); //put the OPLI in the main OPLI map which will be used to perform the insert.
                    System.debug('VG--Map Values - Rollup key = ' + rollUpKey + ' ****Opli value = ' + opliToInsert.Impression_s__c);
                } else { //else put the OPLI in the main OPLI map which will be used to perform the insert.
                    mapOLIs.put(rollUpKey, listOLIWrp[i].item);
                }  
            }
        }
        if (mapOLIs.size() > 0) {
            for (OpportunityLineItem opli: mapOLIs.values()) {
                //Calculate eCPM for the new roll-up fields.
                if(opli.Impression_s__c != 0){
                    opli.eCPM__c = (opli.UnitPrice/opli.Impression_s__c)*1000;
                } else {
                    opli.eCPM__c = 0;
                }
                toReturn.add(opli);  //take the Map values and put it in a list. This list should now contain only unique OPLIs will rolled-up values.
            }
        }
        return toReturn;
    }
    
    /* Wrapper: comprises of Oppurtunity Line Item, roll-up key and whether the OPLI is new. */
    class OLIWrapper{
        public OpportunityLineItem item { get; set;}
        public String oliId{get;set;}
        public Boolean isNew{get;set;} 
        public String OpliRollUpKey {get;set;}
        
        public OLIWrapper(Integer oliId){
            item = new OpportunityLineItem();
            this.oliId = ''+ oliId;
            isNew = true;
            OpliRollUpKey = '';
        }
    }
    
    // Function: read OpptyBulkUploadColumn__mdt Metadata Records and build a map for parsing CSV file contents 
    public Map<Integer, OpptyBulkUploadColumn__mdt> mapColumnMetaData{
        get{
            if(mapColumnMetaData == null){
                mapColumnMetaData = new Map<Integer, OpptyBulkUploadColumn__mdt>();
                for(OpptyBulkUploadColumn__mdt col:[
                    Select Label, Required__c, Field_Type__c, Column__c
                    from OpptyBulkUploadColumn__mdt
                ]){
                    mapColumnMetaData.put(Integer.valueOf(col.Column__c),col);
                }
            }
            return mapColumnMetaData;
        }
        set;
    }
    
    /* Function: for the provided OpportunityLineItem populate the field value presented */
    public OpportunityLineItem populateOLI(OpportunityLineItem updateOLI, Integer rowNum, String theOLI, String fieldName, Object value, Boolean required){
        String theError = '';
        if (!required || (value != null && value != '')) {
            try {
                updateOLI.put(fieldName , value) ; 
            } catch (Exception e) {
                theError = 'Unable to populate \''+ fieldName + '\'\n' + e.getMessage();    
            }
        } else { 
            theError = fieldName + ' cannot be blank';
        }
        if(theError!=''){
            addError(rowNum, theError, String.valueOf(theOLI));
        }
        return updateOLI;
    }

/* Data Preperation */
    /* Function: generate a Map<Product Name <CurrencyISOCOde, PricebookEntry Id>> for "Web" and "Mobile" PricebookEntries */
    public Map<Id,String> mapOpptyCurrency;
    public Map<String,Map<String,Id>> populateMapProductCurrencyId(){
        Map<String,Map<String,Id>> toReturn = new Map<String,Map<String,Id>>();
        for(PricebookEntry pbi:[
            SELECT Name, CurrencyIsoCode, Id 
            FROM PricebookEntry 
            WHERE IsActive = true AND Name IN ('Web','Mobile') AND Pricebook2.IsStandard = true 
            ORDER BY Name, CurrencyIsoCode
        ]){
            if(!toReturn.containsKey(pbi.Name)){
                toReturn.put(pbi.Name,new Map<String,Id>());
            }
            toReturn.get(pbi.Name).put(pbi.CurrencyIsoCode,pbi.Id);
        }
        return toReturn;
    }
    
    /* Function: Get the picklist values for Performance Type and Cost type and store it in a Set. We will use this to validate the values in the spreadsheet. */
    public void instantiatePicklistValues(){
        Schema.DescribeFieldResult perffieldResult = OpportunityLineItem.Performance_Type__c.getDescribe();
        PerformanceTypeValues = new Set<String>();
        for(Schema.PicklistEntry f  :  perffieldResult.getPicklistValues() ){
            PerformanceTypeValues.add(f.getValue());
        }
        Schema.DescribeFieldResult costTypefieldResult = OpportunityLineItem.Cost_Type__c.getDescribe();
        costTypeValues = new Set<String>();
        for(Schema.PicklistEntry c  :  costTypefieldResult.getPicklistValues() ){
            costTypeValues.add(c.getValue());
        }
    }
    
    /* Function: for a set of Opportunity IDs return a Map of Id to CurrencyISOCode */
    Map<String,Map<String,Id>> mapProductName_to_Currency_to_Id;
    public Map<Id,String> populateMapOpptyCurrency(Set<Id> setOpptyIds){
        map<Id,String> toReturn = new map<Id,String>();
        for(Opportunity o:[
            SELECT Id, CurrencyIsoCode 
            FROM Opportunity 
            WHERE Id IN:setOpptyIds
        ]){
            toReturn.put(o.Id,o.CurrencyIsoCode);
        }
        return toreturn;
    }
    
    /* Function:  Get the unique list of OpportunityLineItem records and perform the insert DML Operation.. */
    public void insert_OpportunityLineItems(List<OpportunityLineItem> oplisToInsert){
        try{
            OPT_PROD_STATIC_HELPER.runForProds = true; //This flag will trigger the 'OPT_PROD_UI_ProductScheduleTriggerBulk' trigger to create the Forecast schedule for the inserted Opportunity line items.
            system.debug(oplisToInsert);
            insert oplisToInsert; //perform the bulk insert. This is the only DML operation performed in the code.
            uploadSuccessful = true;
        } catch (Exception e) {
            uploadSuccessful = false;
            OPT_PROD_STATIC_HELPER.runForProds = false;
            ApexPages.Message errormsg = new ApexPages.Message(ApexPages.severity.ERROR,'An error has occured during INSERT. Please check the template. ' + '  Description = -- ' +e.getMessage() );
            ApexPages.addMessage(errormsg);
        }
    } 
    
/* Data Validation*/
    
    /* Function: Validate if the Performance Picklist value provided in the CSV is present in SFDC. */
    public boolean isValidPerformanceType(String perf) {
        string ptype = perf;
        if(perf.startsWithIgnoreCase('web')) {
            ptype = perf.removeStartIgnoreCase('web').trim();
        } else if(perf.startsWithIgnoreCase('mobile')) {
            ptype = perf.removeStartIgnoreCase('mobile').trim();
        }        
        return (ptype=='' || PerformanceTypeValues.contains(ptype));
    }
    
    /* Function: Validate if the Cost Type Picklist value provided in the CSV is present in SFDC. */
    public boolean isValidCostType(String ct) {
        if (costTypeValues.contains(ct)) return true; else return false;
    }
    
    /* Function: Take the performance type/product from the CSV and determine if the product is web or mobile. */
    public String isWebOrMobile(string perf_type){
        if (perf_type!=null && perf_type.startsWithIgnoreCase('web')){
            return 'Web';
        }
        return 'Mobile';
    }
    
    /* Function: Take the performance type/product from the CSV and truncate Web/Mobile from the word to get the actual performance type 
    as stored in SFDC. */
    public String getPerformanceType(string product) {
        if(product == null || product == ''){
            return '';
        } else if(product.startsWithIgnoreCase('web')) {
            return product.removeStartIgnoreCase('web').trim();
        } else if(product.startsWithIgnoreCase('mobile')) { 
            return product.removeStartIgnoreCase('mobile').trim();
        } 
        return product;
    }

/* Error handling */    
    
    /* Function: Add error to the ErrorWrapper for display on the page */
    public void addError(Integer rowNum, String errMsg, String opporId) {
        ErrorWrapper errWrapper = new ErrorWrapper(rowNum);
        errWrapper.oppId = opporId;
        errWrapper.rowNum = rowNum;
        errWrapper.errorDesc = errMsg;
        listErrorWrapper.add(errWrapper);
    }
    
    /* Wrapper: comprises of Oppurtunity Line Item, roll-up key and whether the OPLI is new. */
    public class ErrorWrapper{
        public String oppId{get;set;}
        public Integer rowNum {get;set;} 
        public String errorDesc {get;set;}
        public ErrorWrapper(Integer rowNum){
            rowNum = 0;
            oppID = '';
            errorDesc = '';
        }
    }
}