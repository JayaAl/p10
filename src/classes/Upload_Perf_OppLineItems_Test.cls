/* Dependent Classes: This Test class is for Upload_Perf_OppLineItems_Cntrlr and Upload_Perf_OpportunityLineItems.page */

/* Deprecated fields, cuatom labels, and documents which can also be removed:
 * Custom Label: OP1_SYNC_MobilePricebookEntrytId
 * Custom Label: OP1_SYNC_WebPricebookEntrytId
 * Document: Perf_Bulk_Upload_Opli_Test
 * 
*/

@IsTest
private class Upload_Perf_OppLineItems_Test {
    
    /* Prep testing record data */
    private static Account testAccount;
    private static List<Opportunity> listTestOpptys;
    private static integer bulkCount = 1;
	private static Document importCSV;
    private static OpportunityLineItem negativeTestOLI;
    private static User testUser1;
    private static User testUser2;
    private static String testCSV;
    
    private static void prepCSV(){
        // Create a test CSV String will result in a file formatted as follows:
        // Header 0,Header 1,Header 2,Header 2,...
        // Data 0,Data 1,Data 2,Data 2,...
        testCSV = '';
        for(integer i=0;i<100;i++){ // columns
            String rowType = '';
            if(i==0){
                rowType = '"Header '; 
            } else {
                rowType = '"Data ';
            }
            for(integer j=0;j<100;j++){ // rows
                if(j>=1){testCSV += ',';}
                testCSV += rowType+j+'"';
            }
            testCSV += '\n'; // newline
        }
    }
    
    private static void prepTests(){
        // Create Opportunities
        testAccount = UTIL_TestUtil.createAccount();
        listTestOpptys = new List<Opportunity>();
        for(integer i=0;i<bulkCount;i++){
            listTestOpptys.add(UTIL_TestUtil.generateOpportunity(testAccount.Id));
        }
        insert listTestOpptys;

        Product2 testProductWeb = UTIL_TestUtil.generateProduct();
        testProductWeb.Name = 'Web';
        insert testProductWeb;
        Pricebook2 testPB = UTIL_TestUtil.createPricebook();
        PricebookEntry testPBE = UTIL_TestUtil.createPricebookEntries(new Id[] { testProductWeb.Id }, testPB.Id)[0];

        negativeTestOLI = UTIL_TestUtil.generateOpportunityLineItem(listTestOpptys[0].Id, testPBE.Id); // record not inserted, to be inserted during individual tests as needed.
        negativeTestOLI.ServiceDate = System.today();
        negativeTestOLI.End_Date__c = date.parse('9/29/2012');
        negativeTestOLI.Rate__c = 125;
        negativeTestOLI.Impression_s__c = 1000;
        negativeTestOLI.Clicks__c = 10000;
        negativeTestOLI.Performance_Type__c = 'Web Display - Top';
        negativeTestOLI.Cost_Type__c = 'CPA';
        negativeTestOLI.Age__c = '20';
        negativeTestOLI.Gender__c = 'M';
        negativeTestOLI.Zip__c = 94538;
        negativeTestOLI.MSA__c =  '';         
        negativeTestOLI.DMA__c = '';
        negativeTestOLI.Add_l_Targets__c = 'test';
        negativeTestOLI.Frequency_Cap__c = 'test';
        negativeTestOLI.Conversions__c = 1000; 
        
        // create a document to insert
        // get fields that are part of the metadata setting
        
        
        
        // test users to RunAs, needs to occur last as User updates cause Group updates
        testUser1 = UTIL_TestUtil.generateUser();
        testUser2 = UTIL_TestUtil.generateUser();
        insert new List<User>{testUser1, testUser2};
    }
    
    
    @isTest(seeAllData=true) // required in order for UTIL_TestUtil.createPricebookEntries() to work
    public static void runNegativeTestCases() {
        prepTests();
        List<OpportunityLineItem> oplisToUpload = new List<OpportunityLineItem>();
        Upload_Perf_OppLineItems_Cntrlr file = new Upload_Perf_OppLineItems_Cntrlr ();
        User u3 = [SELECT Id FROM User WHERE profileid='00e30000000hI3h' and isactive = true limit 1];
        System.RunAs(u3){
            oplisToUpload.add(negativeTestOLI);
            try {
test.startTest();
                file.insert_OpportunityLineItems(oplisToUpload);
test.stopTest();
            } catch (DmlException e) {
                String expectedError = 'Insert failed. First exception on  row 0; first error: FIELD_CUSTOM_VALIDATION_EXCEPTION, End Date must be past or equal to Start Date';
                System.assert( e.getMessage().contains(expectedError),  e.getMessage()); //Assert Error Message 
                System.assertEquals( OpportunityLineItem.End_Date__c, e.getDmlFields(0)[0] ); //Assert field
                System.assertEquals( 'FIELD_CUSTOM_VALIDATION_EXCEPTION' , e.getDmlStatusCode(0) ); //Assert Status Code
            }
        }
    }
    
    public static testmethod void test_instantiatePicklistValues(){
        Upload_Perf_OppLineItems_Cntrlr file = new Upload_Perf_OppLineItems_Cntrlr();
        file.instantiatePicklistValues();
        // Confirm collections are not empty
        system.assert(!Upload_Perf_OppLineItems_Cntrlr.PerformanceTypeValues.isEmpty());
        system.assert(!Upload_Perf_OppLineItems_Cntrlr.costTypeValues.isEmpty());
        // No further tests possible without hard-coding the expected values
    }
    
    @isTest(seeAllData=true) // required in order for UTIL_TestUtil.createPricebookEntries() to work
    public static void test_populateMapProductCurrencyId(){
        prepTests();
        Upload_Perf_OppLineItems_Cntrlr file = new Upload_Perf_OppLineItems_Cntrlr();
        Map<String,Map<String,Id>> testMap = file.populateMapProductCurrencyId();
        // confirm that the Map contains <Product Name , <CurrencyISOCode, Id>>
        system.assert(!testMap.isEmpty()); // Map is not empty
        for(String s:testMap.keySet()){
            system.assert(s=='Web'||s=='Mobile'); // key is either 'Web' or 'Mobile'
        }
    }
    
    @isTest(seeAllData=true) // required in order for UTIL_TestUtil.createPricebookEntries() to work
    public static void test_populateMapOpptyCurrency(){
        prepTests();
		Upload_Perf_OppLineItems_Cntrlr file = new Upload_Perf_OppLineItems_Cntrlr();
        Set<Id> opptyIds = new Set<Id>();
        for(Opportunity o:listTestOpptys){
            opptyIds.add(o.Id);
        }
        Map<Id,String> testMap = file.populateMapOpptyCurrency(opptyIds);
        // We are not overriding the Currency for any Opptys created so far, as a result all will be created with the current User's default CurrencyISOCode
		String currentISO = UserInfo.getDefaultCurrency();
        System.assertEquals(bulkCount,testMap.size()); // confirm that the Map contains the expected number of values
        for(String s:testMap.values()){
            System.assert(s==currentISO); // Confirm that the values all match
        }
    }
    
    public static testmethod void test_isValidPerformanceType(){
        Upload_Perf_OppLineItems_Cntrlr file = new Upload_Perf_OppLineItems_Cntrlr();
        file.instantiatePicklistValues();
        system.assert(!file.isValidPerformanceType('BAD TESTING VALUE')); // confirm that a nonsensical value returns FALSE
        for(String ptype:Upload_Perf_OppLineItems_Cntrlr.PerformanceTypeValues){
            system.assert(file.isValidPerformanceType(ptype)); // confirm that the values in PerformanceTypeValues return TRUE
            system.assert(file.isValidPerformanceType('Web ' + ptype)); // confirm that the values in PerformanceTypeValues return TRUE even when appended with 'Web'
            system.assert(file.isValidPerformanceType('Mobile ' + ptype)); // confirm that the values in PerformanceTypeValues return TRUE even when appended with 'Mobile'
        }
    }

    public static testmethod void test_isValidCostType(){
        Upload_Perf_OppLineItems_Cntrlr file = new Upload_Perf_OppLineItems_Cntrlr();
        file.instantiatePicklistValues();
        system.assert(!file.isValidCostType('BAD TESTING VALUE')); // confirm that a nonsensical value returns FALSE
        for(String ctype:Upload_Perf_OppLineItems_Cntrlr.costTypeValues){
            system.assert(file.isValidCostType(ctype)); // confirm that the values in costTypeValues return TRUE
        }
    }
    
    public static testmethod void test_addError(){
        Upload_Perf_OppLineItems_Cntrlr file = new Upload_Perf_OppLineItems_Cntrlr();
        file.listErrorWrapper = new List<Upload_Perf_OppLineItems_Cntrlr.ErrorWrapper>();
        file.addError(2, 'String errMsg', 'String opporId'); // add one error with example data
        system.assertEquals(1, file.listErrorWrapper.size()); // confirm that listErrorWrapper contains one element
        system.assertEquals(2, file.listErrorWrapper[0].rowNum);// confirm the details of that element
        system.assertEquals('String opporId', file.listErrorWrapper[0].oppId);// confirm the details of that element
        system.assertEquals('String errMsg', file.listErrorWrapper[0].errorDesc);// confirm the details of that element
        // bulk add a known number of records to listErrorWrapper
        for(integer i=0;i<500;i++){
            file.addError(i, 'errMsg '+i, 'opporId '+i);
        }
        system.assertEquals(501, file.listErrorWrapper.size()); // confirm the list contains the proper number of records (1+ the bulk adds)
    }
    
    public static testmethod void test_getPerformanceType(){
        Upload_Perf_OppLineItems_Cntrlr file = new Upload_Perf_OppLineItems_Cntrlr();
        system.assertEquals('', file.getPerformanceType(null)); // on null or empty return ''
        system.assertEquals('', file.getPerformanceType(''));
        system.assertEquals('_test3', file.getPerformanceType('web_test3')); // remove leading 'web'
        system.assertEquals('_test4', file.getPerformanceType('mobile_test4')); // remove leading 'mobile'
        system.assertEquals('mobile_test5', file.getPerformanceType('webmobile_test5')); // only removes the first instance of 'web' or 'mobile'
        system.assertEquals('web_test6', file.getPerformanceType('mobileweb_test6'));
        system.assertEquals('noneOfTheAbove_test7', file.getPerformanceType('noneOfTheAbove_test7')); // if web or mobile not found then simply return the full product name
    }
    
    public static testmethod void test_isWebOrMobile(){
        Upload_Perf_OppLineItems_Cntrlr file = new Upload_Perf_OppLineItems_Cntrlr();
        system.assertEquals('Web', file.isWebOrMobile('web_test1')); // if perf_type starts with 'web' return 'Web'
        system.assertEquals('Mobile', file.isWebOrMobile(null)); // Default to 'Mobile' for all other instances
        system.assertEquals('Mobile', file.isWebOrMobile(''));
        system.assertEquals('Mobile', file.isWebOrMobile('mobile_test4'));
        system.assertEquals('Mobile', file.isWebOrMobile('noneOfTheAbove_test5'));
    }
    
    public static testmethod void test_mapColumnMetaData(){
        Upload_Perf_OppLineItems_Cntrlr file = new Upload_Perf_OppLineItems_Cntrlr();
        List<OpptyBulkUploadColumn__mdt> testList = [Select Label, Required__c, Field_Type__c, Column__c from OpptyBulkUploadColumn__mdt];
        system.assertNotEquals(0, testList.size()); // confirm that Metadata OpptyBulkUploadColumn__mdt table contains values
        // confirm that for each record in testList corresponds to a key=>value pair in Upload_Perf_OppLineItems_Cntrlr.mapColumnMetaData
        for(OpptyBulkUploadColumn__mdt meta:testList){
            integer theKey = Integer.valueOf(meta.Column__c);
            system.assert(file.mapColumnMetaData.containsKey(theKey));
            system.assertEquals(meta.Label,file.mapColumnMetaData.get(theKey).Label);
            system.assertEquals(meta.Required__c,file.mapColumnMetaData.get(theKey).Required__c);
            system.assertEquals(meta.Field_Type__c,file.mapColumnMetaData.get(theKey).Field_Type__c);
            system.assertEquals(meta.Column__c,file.mapColumnMetaData.get(theKey).Column__c);
        }
    }
    
    public static testmethod void test_parseCSV(){
        prepCSV();
        Upload_Perf_OppLineItems_Cntrlr file = new Upload_Perf_OppLineItems_Cntrlr();
        file.contentFile = blob.valueOf(testCSV);
        file.nameFile = UTIL_TestUtil.TEST_STRING;
        List<List<String>> stringResults = file.parseCSV();
        system.assert(!stringResults.isEmpty());
        // test to confirm results conform to the pattern that we defined in testPrep
        for(Integer i=0;i<stringResults.size();i++){
            for(String s:stringResults[i]){
                s = s.replace('"','');
                system.assert(s.startsWithIgnoreCase('Data') || s.startsWithIgnoreCase('Header'));
                s = s.replace('Header','').replace('Data','').trim();
                system.assert(s.isNumeric());
            }
        }
    }
    
    public static testmethod void test_populateOLI(){
        Upload_Perf_OppLineItems_Cntrlr file = new Upload_Perf_OppLineItems_Cntrlr();
        file.listErrorWrapper = new List<Upload_Perf_OppLineItems_Cntrlr.ErrorWrapper>();
        OpportunityLineItem successfulOLI = new OpportunityLineItem();
        OpportunityLineItem missingRequired = new OpportunityLineItem();
        OpportunityLineItem updateReadOnlyField = new OpportunityLineItem();
        OpportunityLineItem invalidField = new OpportunityLineItem();
        OpportunityLineItem invalidValue = new OpportunityLineItem();
        
        file.populateOLI(successfulOLI, 1, 'String theOLI', 'UnitPrice', 100, true);
        file.populateOLI(missingRequired, 2, 'String theOLI', 'UnitPrice', null, true);
        file.populateOLI(updateReadOnlyField, 3, 'String theOLI', 'Product_ID__c', 100, true);
        file.populateOLI(invalidField, 4, 'String theOLI', '*1iV*Mv0Zm2GYNj37#n07lII80C*8*7@UkzefJe8', 100, true);
        file.populateOLI(invalidValue, 5, 'String theOLI', 'UnitPrice', false, true);
        /* TODO: assert that errors have been generated by reviewing file.listErrorWrapper */
    }
    
    /* Original tests created by Lakshman */
    @IsTest(SeeAllData=true)
    public static void test_ReadFile() {
        Test.startTest();  
        PageReference VFpage = Page.Upload_Perf_OpportunityLineItems;
        test.setCurrentPage(VFpage);
        Document lstDoc = [select id,name,Body from Document where Id = :Label.Performance_Bulk_Upload_Products_Template_Sample];
        Upload_Perf_OppLineItems_Cntrlr file = new Upload_Perf_OppLineItems_Cntrlr ();
        Blob content= lstDoc.Body;
        file.contentFile = content; 
        file.ReadFile(); 
        file.nameFile=content.toString();
        String[] filelines = new String[]{};
            List<OpportunityLineItem> oplisToUpload = new List<OpportunityLineItem>();
        for (Integer i=1;i<filelines.size();i++){
            String[] inputvalues = new String[]{};
                inputvalues = filelines[i].split(',');
            OpportunityLineItem opli = new OpportunityLineItem();
            opli.Team_Type__c = 'Performance'; 
            opli.OpportunityId = '0064000000Nknic';
            opli.ServiceDate = date.parse('9/23/2012'); 
            opli.End_Date__c = date.parse('9/29/2012');
            opli.Rate__c = 125;
            opli.Impression_s__c = 1000;
            opli.Clicks__c = 10000;
            opli.Performance_Type__c = 'Web Display - Top';
            opli.Cost_Type__c = 'CPA'; 
            opli.Age__c = '20';
            opli.Gender__c = 'M';
            opli.Zip__c = 94538; 
            opli.MSA__c =  '';           
            opli.DMA__c = ''; 
            opli.Add_l_Targets__c = 'test';
            opli.Frequency_Cap__c = 'test';
            opli.Conversions__c = 1000;
            opli.UnitPrice = 1000;
            oplisToUpload.add(opli);
            try{
                //file.getUploadedOpportunityLineItems();
                file.insert_OpportunityLineItems(oplisToUpload);
                Test.stopTest();
            } catch (Exception e) {
                ApexPages.Message errormsg = new ApexPages.Message(ApexPages.severity.ERROR,'An error has occured. Please check the template');
                ApexPages.addMessage(errormsg);
            }
        }
    }
}