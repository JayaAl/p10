/**
* ─────────────────────────────────────────────────────────────────────────────────────────────────┐
* Controller class for Lightning/Console Bulk case accept logic. Loads all selected records
* and assigns them to the current user if they pass validation.
* ──────────────────────────────────────────────────────────────────────────────────────────────────
* @author         Jason West
* @maintainedBy   
* @version        1.0
* @created        2018-03-30
* @modified       
* @systemLayer    Service
* ──────────────────────────────────────────────────────────────────────────────────────────────────
* @maintainedBy   
* @version			1.1
* @modified_Date	2018/04/12 [YYYY/MM/DD]	  
* @modified_By		Jaya Alaparthi 
* @modified_Purpose	Case routing Issue for Service cloud
* ─────────────────────────────────────────────────────────────────────────────────────────────────┘
*/
public with sharing class VBulkAcceptCasesExt {
	private ApexPages.StandardSetController controller;

	public List<String> errorMessages { get; private set; }
	public List<String> successMessages { get; private set; }
	public List<Case> caseList { get; private set; }

	public Boolean hasErrors {
		get {
			return errorMessages.size() > 0;
		}
	}

	public Boolean hasSuccesses {
		get {
			return successMessages.size() > 0;
		}
	}

	public VBulkAcceptCasesExt(ApexPages.StandardSetController controller) {
		this.controller = controller;
		this.errorMessages = new List<String>();
		this.successMessages = new List<String>();
		loadCases();
	}

	public PageReference acceptCases() {
		PageReference res = null;

		if(caseList.size() > 0) {
			if(!hasErrors) {
				List<Case> finalList = validateAndRemoveBadCases();

				if(finalList.size() > 0) {
					for(Case cse : finalList) {
						cse.OwnerId = UserInfo.getUserId();
						cse.Accepted_Date_Time__c = DateTime.now();
						// v1.1
						cse.Status = 'Working';
					}

					try {
						update finalList;
						loadSuccessMessages(finalList);
					} catch(Exception e) {
						errorMessages.add(Label.BulkAcceptError);
					}
				}
			}
		} else {
			errorMessages.add(Label.BulkAcceptNoCasesSelected);			
		}

		return res;
	}

	private void loadSuccessMessages(List<Case> lst) {
		for(Case cse : lst) {
			successMessages.add(Label.BulkAcceptCaseAccepted + ': ' + cse.CaseNumber);
		}
	}

	private List<Case> validateAndRemoveBadCases() {
		List<Case> res = new List<Case>();

		for(Case cse : caseList) {
			String ownerString = cse.OwnerId;
			if(!ownerString.startsWith(Schema.SObjectType.Group.getKeyPrefix())) {
				errorMessages.add(cse.CaseNumber + ': ' + Label.BulkAcceptOwnerError);
				continue;
			}

			if(cse.Accepted_Date_Time__c != null) {
				errorMessages.add(cse.CaseNumber + ': ' + Label.BulkAcceptAlreadyAcceptedError);
				continue;
			}

			res.add(cse);
		}

		return res;
	}

	private void loadCases() {
		try {
			caseList = [select Id, CaseNumber, OwnerId, Accepted_Date_Time__c
				from Case
				where Id in :controller.getSelected()];
		} catch(Exception e) {
			errorMessages.add(Label.BulkAcceptLoadError);
		}
	}


}