/**
* ─────────────────────────────────────────────────────────────────────────────────────────────────┐
* Test class for VBulkAcceptCasesExt
* ──────────────────────────────────────────────────────────────────────────────────────────────────
* @author         Jason West
* @maintainedBy   
* @version        1.0
* @created        2018-03-29
* @modified       
* @systemLayer    Service
* ─────────────────────────────────────────────────────────────────────────────────────────────────┘
*/
@isTest
public with sharing class VBulkAcceptCasesExtTest {
	public static final ID QUEUE_OWNER_ID;
	public static final ID CASE_RECORD_TYPE_ID;

	static {
		QueueSObject queueSobj = [select QueueId from QueueSObject
			where SObjectType = 'Case'
			and Queue.DeveloperName = 'User_Support_P9'];
		QUEUE_OWNER_ID = queueSobj.QueueId;

		RecordType rt = [select Id from RecordType
			where SObjectType = 'Case'
			and DeveloperName = 'User_Support'];
		CASE_RECORD_TYPE_ID = rt.Id;
	}

	@testSetup
	static void testSetup() {
		Case cse1 = new Case(Subject = 'Test',
			OwnerId = QUEUE_OWNER_ID,
			RecordTypeId = CASE_RECORD_TYPE_ID);

		Case cse2 = new Case(Subject = 'Test',
			OwnerId = QUEUE_OWNER_ID,
			RecordTypeId = CASE_RECORD_TYPE_ID);

		insert new List<Case> { cse1, cse2 };
	}

	@isTest
	static void testAccept() {
		List<Case> caseList = [select Id from Case];

		ApexPages.StandardSetController controller = new ApexPages.StandardSetController(caseList);
		controller.setSelected(caseList);

		VBulkAcceptCasesExt ext = new VBulkAcceptCasesExt(controller);

		System.assertEquals(null, ext.acceptCases());
		System.assertEquals(2, ext.successMessages.size());
		System.assertEquals(0, ext.errorMessages.size());
		System.assert(ext.hasSuccesses, 'hasSuccesses should be true');
		System.assert(!ext.hasErrors, 'hasErrors should be false');

		caseList = [select Id, OwnerId, Accepted_Date_Time__c from Case];

		System.assertEquals(2, ext.caseList.size());
		System.assertEquals(UserInfo.getUserId(), caseList[0].OwnerId);
		System.assertEquals(UserInfo.getUserId(), caseList[1].OwnerId);
		System.assertNotEquals(null, caseList[0].Accepted_Date_Time__c);
		System.assertNotEquals(null, caseList[1].Accepted_Date_Time__c);

	}

	@isTest
	static void testNoSelection() {
		List<Case> caseList = [select Id from Case];

		ApexPages.StandardSetController controller = new ApexPages.StandardSetController(caseList);
		VBulkAcceptCasesExt ext = new VBulkAcceptCasesExt(controller);

		System.assertEquals(null, ext.acceptCases());
		System.assertEquals(0, ext.caseList.size());
		System.assertEquals(0, ext.successMessages.size());
		System.assertEquals(1, ext.errorMessages.size());
	}

	@isTest
	static void testNotOwnedByQueue() {
		List<Case> caseList = [select Id from Case];

		for(Case cse : caseList) {
			cse.OwnerId = UserInfo.getUserId();
		}

		update caseList;

		ApexPages.StandardSetController controller = new ApexPages.StandardSetController(caseList);
		controller.setSelected(caseList);

		VBulkAcceptCasesExt ext = new VBulkAcceptCasesExt(controller);

		System.assertEquals(null, ext.acceptCases());
		System.assertEquals(0, ext.successMessages.size());
		System.assertEquals(2, ext.errorMessages.size());
	}

	@isTest
	static void testAcceptedDateTimeNotNull() {
		List<Case> caseList = [select Id from Case];

		for(Case cse : caseList) {
			cse.Accepted_Date_Time__c = DateTime.now();
		}

		update caseList;

		ApexPages.StandardSetController controller = new ApexPages.StandardSetController(caseList);
		controller.setSelected(caseList);

		VBulkAcceptCasesExt ext = new VBulkAcceptCasesExt(controller);

		System.assertEquals(null, ext.acceptCases());
		System.assertEquals(0, ext.successMessages.size());
		System.assertEquals(2, ext.errorMessages.size());
	}

	@isTest
	static void testPartialAccept() {
		List<Case> caseList = [select Id from Case];
		caseList[0].OwnerId = UserInfo.getUserId();
		update caseList;

		ApexPages.StandardSetController controller = new ApexPages.StandardSetController(caseList);
		controller.setSelected(caseList);

		VBulkAcceptCasesExt ext = new VBulkAcceptCasesExt(controller);

		System.assertEquals(null, ext.acceptCases());
		System.assertEquals(1, ext.successMessages.size());
		System.assertEquals(1, ext.errorMessages.size());
	}

	@isTest
	static void testDMLException() {
		List<Case> caseList = [select Id from Case];

		ApexPages.StandardSetController controller = new ApexPages.StandardSetController(caseList);
		controller.setSelected(caseList);

		VBulkAcceptCasesExt ext = new VBulkAcceptCasesExt(controller);
		delete caseList;

		System.assertEquals(null, ext.acceptCases());
		System.assertEquals(0, ext.successMessages.size());
		System.assertEquals(1, ext.errorMessages.size());
	}
}