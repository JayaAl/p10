/**
* ─────────────────────────────────────────────────────────────────────────────────────────────────┐
* Controller class for Lightning/Console Bulk case open logic. Loads all selected records
* and prepares them to be opened in console tabs or browser tabs.
* ──────────────────────────────────────────────────────────────────────────────────────────────────
* @author         Jason West
* @maintainedBy   
* @version        1.0
* @created        2018-03-29
* @modified       
* @systemLayer    Service
* ─────────────────────────────────────────────────────────────────────────────────────────────────┘
*/
public with sharing class VBulkOpenCasesExt {
	private ApexPages.StandardSetController controller;

	public List<ID> caseIdList { get; private set; }
	public String initialError { get; private set; }

	public String caseIds {
		get {
			return JSON.serialize(caseIdList);
		}
	}

	public List<String> caseNumberList { get; private set; }
	public String caseNumbers {
		get {
			return JSON.serialize(caseNumberList);
		}
	}

	public VBulkOpenCasesExt(ApexPages.StandardSetController controller) {
		this.controller = controller;
		this.caseIdList = new List<ID>();
		this.caseNumberList = new List<String>();
		this.initialError = '';

		loadRecords();
	}

	private void loadRecords() {
		try {
			for(sObject sobj : controller.getSelected()) {
				Case cse = (Case)sobj;
				caseIdList.add(sobj.Id);
			}

			Map<ID, Case> caseMap = new Map<ID, Case>([
				select Id, CaseNumber from Case where Id in :caseIdList
			]);

			for(ID i : caseIdList) {
				caseNumberList.add(caseMap.get(i).CaseNumber);
			}
		} catch(Exception e) {
			initialError = Label.BulkOpenCaseError;
		}
	}
}