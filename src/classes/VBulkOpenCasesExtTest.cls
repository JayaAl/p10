/**
* ─────────────────────────────────────────────────────────────────────────────────────────────────┐
* Test class for VBulkOpenCasesExt
* ──────────────────────────────────────────────────────────────────────────────────────────────────
* @author         Jason West
* @maintainedBy   
* @version        1.0
* @created        2018-01-10
* @modified       
* @systemLayer    Service
* ─────────────────────────────────────────────────────────────────────────────────────────────────┘
*/
@isTest
public with sharing class VBulkOpenCasesExtTest {
	@testSetup
	static void testSetup() {
		Case cse1 = new Case(Subject = 'Test');
		Case cse2 = new Case(Subject = 'Test');

		insert new List<Case> { cse1, cse2 };
	}

	@isTest
	static void testLoadCases() {
		List<Case> caseList = [select Id from Case where Subject = 'Test'];

		ApexPages.StandardSetController controller = new ApexPages.StandardSetController(caseList);
		controller.setPageSize(1);
		controller.setSelected(caseList);

		VBulkOpenCasesExt ext = new VBulkOpenCasesExt(controller);
		System.assertEquals(2, ext.caseIdList.size());
		System.assertNotEquals(null, ext.caseIds);
		System.assertEquals(2, ext.caseNumberList.size());
		System.assertNotEquals(null, ext.caseNumbers);
		System.assert(String.isBlank(ext.initialError), 'There should not be an error.');
	}

	@isTest
	static void testException() {
		List<Case> caseList = [select Id from Case where Subject = 'Test'];

		ApexPages.StandardSetController controller = new ApexPages.StandardSetController(caseList);
		controller.setPageSize(1);
		controller.setSelected(caseList);

		delete caseList[1];

		VBulkOpenCasesExt ext = new VBulkOpenCasesExt(controller);
		System.assert(String.isNotBlank(ext.initialError), 'There should be an error.');
	}
}