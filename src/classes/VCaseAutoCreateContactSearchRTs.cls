public with sharing class VCaseAutoCreateContactSearchRTs {
	private static List<String> FIELDS = new List<String> {
		'MasterLabel',
		'DeveloperName',
		'ContactRecordTypeId__c'
	};

	private static String QUERY_STRING = 'select {0} from vCaseAutoCreateContactSearchRTs__mdt'.replace('{0}', String.join(FIELDS, ','));

	private static Set<ID> mSearchRecordTypes = null;

	public static Set<ID> searchRecordTypes {
		get {
			if(mSearchRecordTypes == null) {
				mSearchRecordTypes = new Set<ID>();

				for(vCaseAutoCreateContactSearchRTs__mdt setting : (List<vCaseAutoCreateContactSearchRTs__mdt>)Database.query(QUERY_STRING)) {
					mSearchRecordTypes.add(setting.ContactRecordTypeId__c);
				}
			}

			return mSearchRecordTypes;
		}
	}
}