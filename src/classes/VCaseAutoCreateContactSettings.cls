public with sharing class VCaseAutoCreateContactSettings {
	private static List<String> FIELDS = new List<String> {
		'MasterLabel',
		'DeveloperName',
		'CaseRecordTypeId__c',
		'NewContactRTId__c',
		'NewAccountRTId__c',
		'NewAccountType__c'
	};

	private static String QUERY_STRING = 'select {0} from vCaseAutoCreateContactSetting__mdt'.replace('{0}', String.join(FIELDS, ','));

	private static Map<ID, vCaseAutoCreateContactSetting__mdt> mSettingsMap = null;

	public static Map<ID, vCaseAutoCreateContactSetting__mdt> settingsMap {
		get {
			if(mSettingsMap == null) {
				mSettingsMap = new Map<ID, vCaseAutoCreateContactSetting__mdt>();

				for(vCaseAutoCreateContactSetting__mdt setting : (List<vCaseAutoCreateContactSetting__mdt>)Database.query(QUERY_STRING)) {
					mSettingsMap.put(setting.CaseRecordTypeId__c, setting);
				}
			}

			return mSettingsMap;
		}
	}
}