public without sharing class VCaseResolveController {
	public String surveyLink { get; private set; }
	public String caseId { get; private set; }

	public VCaseResolveController() {
		this.surveyLink = ApexPages.currentPage().getParameters().get('SurveyLink')	;
		this.caseId = ApexPages.currentPage().getParameters().get('cid');
	}

	public PageReference closeCase() {
		PageReference res = null;

		try {
			if(String.isNotBlank(caseId)) {
				Case cse = new Case(
					Id = caseId,
					Status = 'Closed',
					Case_Resolution__c = 'Solved via Auto-Response Email'
				);

				update cse;
			}

			if(String.isNotBlank(surveyLink)) {
				res = new PageReference(surveyLink);
				res.setRedirect(true);
			}
		} catch(Exception e) {}
		return res;
	}
}