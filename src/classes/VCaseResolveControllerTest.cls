@isTest
public with sharing class VCaseResolveControllerTest {
	@isTest
	static void testController() {
		RecordType rt = [select Id from RecordType 
			where DeveloperName = 'User_Support'
			and sObjectType = 'Case'];

		Case cse = new Case(Subject = 'Test', RecordTypeId = rt.Id);
		insert cse;

		PageReference curPage = new PageReference('/case');
		curPage.getParameters().put('cid', cse.Id);
		curPage.getParameters().put('SurveyLink', 'https://google.com');

		Test.setCurrentPage(curPage);

		VCaseResolveController controller = new VCaseResolveController();
		System.assertNotEquals(null, controller.closeCase());

		cse = [select Id, Status from Case where Id = :cse.Id];
		System.assertEquals('Closed', cse.Status);
	}
}