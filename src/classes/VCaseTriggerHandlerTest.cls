@isTest
public with sharing class VCaseTriggerHandlerTest {
    @isTest
    static void testAutoCreateCaseContactInsert() {
        Map<ID, vCaseAutoCreateContactSetting__mdt> autoCreateSettings = VCaseAutoCreateContactSettings.settingsMap;

        Case cse = new Case(
            RecordTypeId = autoCreateSettings.values()[0].CaseRecordTypeId__c,
            Subject = 'Test',
            Description = 'Test',
            SuppliedName = 'Test Test',
            SuppliedEmail = 'test@test.com'
        );

        insert cse;

        cse = [select Id, ContactId, AccountId from Case where Id = :cse.Id];
        System.assertNotEquals(null, cse.AccountId);
        System.assertNotEquals(null, cse.ContactId);
    }

    @isTest
    static void testAutoCreateCaseContactExisting() {
        Map<ID, vCaseAutoCreateContactSetting__mdt> autoCreateSettings = VCaseAutoCreateContactSettings.settingsMap;
        Account acc = new Account(
            Type = autoCreateSettings.values()[0].NewAccountType__c,
            RecordTypeId = autoCreateSettings.values()[0].NewAccountRTId__c,
            Name = 'Test Test - test@test.com'
        );

        insert acc;

        Contact con = new Contact(
            RecordTypeId = autoCreateSettings.values()[0].NewContactRTId__c,
            AccountId = acc.Id,
            FirstName = 'Test',
            LastName = 'Test',
            Email = 'test@test.com'
        );

        insert con;

        Case cse = new Case(
            RecordTypeId = autoCreateSettings.values()[0].CaseRecordTypeId__c,
            Subject = 'Test',
            Description = 'Test',
            SuppliedName = 'Test Test',
            SuppliedEmail = 'test@test.com'
        );

        insert cse;

        cse = [select Id, ContactId, AccountId from Case where Id = :cse.Id];
        System.assertEquals(acc.Id, cse.AccountId);
        System.assertEquals(con.Id, cse.ContactId);
    }
    
    static testMethod void validateVCloseOpenMilestones(){
        
        Map<ID, vCaseAutoCreateContactSetting__mdt> autoCreateSettings = VCaseAutoCreateContactSettings.settingsMap;
        
        Account acc = new Account(
            Type = autoCreateSettings.values()[0].NewAccountType__c,
            RecordTypeId = autoCreateSettings.values()[0].NewAccountRTId__c,
            Name = 'Test Test - test@test.com'
        );

        insert acc;

        Contact con = new Contact(
            RecordTypeId = autoCreateSettings.values()[0].NewContactRTId__c,
            AccountId = acc.Id,
            FirstName = 'Test',
            LastName = 'Test',
            Email = 'test@test.com'
        );

        insert con;

    
        
        SlaProcess sla = [SELECT Id FROM SlaProcess WHERE Name = 'User Support - Time' AND IsActive = True AND IsVersionDefault = True LIMIT 1];
        
        Entitlement e = new entitlement(name='Test Entitlement', AccountId = acc.Id, SlaProcessId = sla.Id, StartDate = Date.valueOf(System.now().addDays(-2)), EndDate=Date.valueof(System.now().addYears(2)));
        insert e;
        
        Case cse = new Case(
            RecordTypeId = autoCreateSettings.values()[0].CaseRecordTypeId__c,
            Subject = 'Test',
            Description = 'Test',
            SuppliedName = 'Test Test',
            SuppliedEmail = 'test@test.com',
            EntitlementId = e.Id,
            accountId = acc.Id,
            contactId = con.Id,
            Status = 'New',
            slaStartDate = system.now(),
            Priority = 'P9',
            Original_Priority__c = 'P9',
            Reason = 'Email and Password'
        );

        insert cse;
        
        vCaseTriggerHandler.EXECUTE_BEFORE_UPDATE = true;
        
        cse.Accepted_Date_Time__c = system.now();
        update cse;
        
        vCaseTriggerHandler.EXECUTE_BEFORE_UPDATE = true;
        
        cse.Status = 'Waiting on Case Contact';
        update cse;
        
        vCaseTriggerHandler.EXECUTE_BEFORE_UPDATE = true;
        
        cse.Status = 'Resolved';
        update cse;
    }
    
    @isTest
    public static void testAssignToQueue(){
        
        Map<ID, vCaseAutoCreateContactSetting__mdt> autoCreateSettings = VCaseAutoCreateContactSettings.settingsMap;
        
        Account acc = new Account(
            Type = autoCreateSettings.values()[0].NewAccountType__c,
            RecordTypeId = autoCreateSettings.values()[0].NewAccountRTId__c,
            Name = 'Test Test - test@test.com'
        );

        insert acc;

        Contact con = new Contact(
            RecordTypeId = autoCreateSettings.values()[0].NewContactRTId__c,
            AccountId = acc.Id,
            FirstName = 'Test',
            LastName = 'Test',
            Email = 'test@test.com'
        );

        insert con;
        
        Group testQueue = [SELECT Id
                        FROM Group
                        WHERE Type = 'Queue'
                   		LIMIT 1];
        
        Case cse = new Case(
            RecordTypeId = autoCreateSettings.values()[0].CaseRecordTypeId__c,
            Subject = 'Test',
            Description = 'Test',
            SuppliedName = 'Test Test',
            SuppliedEmail = 'test@test.com',
            accountId = acc.Id,
            contactId = con.Id,
            Status = 'New',
            slaStartDate = system.now(),
            Priority = 'P9',
            Original_Priority__c = 'P9',
            Reason = 'Email and Password',
            OwnerId = testQueue.Id
        );

        insert cse;
        System.debug(cse.Priority);
        System.debug(cse.OwnerId);
        
        Test.startTest();
        	cse.Priority = 'P1';
        	Update cse;
        Test.stopTest();
    }
}