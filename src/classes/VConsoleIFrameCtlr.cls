public with sharing class VConsoleIFrameCtlr {

    @AuraEnabled
    public static String getCaseDetails(Id caseId){
        
        Case curCase = [SELECT Id, ContactEmail, Pandora_Account_Email__c
                       FROM Case
                       WHERE Id = :caseId
                       LIMIT 1];
        
        String emailTokens = '';
        
        if (String.isBlank(curCase.ContactEmail)==false){
            emailTokens += curCase.ContactEmail;
        }
        
        if (String.isBlank(curCase.Pandora_Account_Email__c)==false){
            emailTokens += ';' + curCase.Pandora_Account_Email__c;
        }
        return emailTokens;
        
    }
}