@isTest
public class VConsoleIFrameCtlrTest {
	
    @isTest
    public static void testGetCaseDetails(){
        
        Id userSupportRecordTypeId = Schema.SObjectType.Case.getRecordTypeInfosByName().get('User Support').getRecordTypeId();
        Id contactUserRecordTypeId = Schema.SObjectType.Contact.getRecordTypeInfosByName().get('User').getRecordTypeId();
        Id accountUserRecordTypeId = Schema.SObjectType.Account.getRecordTypeInfosByName().get('User').getRecordTypeId();
        
        Account testAccount = new Account(Name='TestAccount',
                                         RecordTypeId=accountUserRecordTypeId,
                                         Type='User');
        insert testAccount;
        
        Contact testContact  = new Contact(RecordTypeId=contactUserRecordTypeId,
                                          LastName='McTesterson',
                                          FirstName='Testy',
                                          AccountId=testAccount.Id,
                                          Email='testy.mctesterson@fake.com');
        insert testContact;
        
        //BusinessHours testBH = [SELECT Id FROM BusinessHours LIMIT 1];
        
        Case testCase = new Case(RecordTypeId=userSupportRecordTypeId,
                                Status='New',
                                Origin='Web',
                                ContactId=testContact.Id,
                                AccountId=testAccount.Id,
                                Pandora_Account_Email__c='testy.mctesterson@fake.com');
        insert testCase;
        
        Test.startTest();
        	VConsoleIFrameCtlr.getCaseDetails(testCase.Id);
        Test.stopTest();
    }
}