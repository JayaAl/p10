public with sharing class VGeneralSettings {
	public static String SETTINGS_NAME = 'Default';

	private static List<String> FIELDS = new List<String> {
		'EnableCaseContactAutoCreate__c',
		'EnableCaseTrigger__c'
	};

	private static String QUERY = 'select {0} from vGeneralSetting__mdt where DeveloperName = :SETTINGS_NAME';
	private static vGeneralSetting__mdt gs;

	public static vGeneralSetting__mdt settings {
		get {
			if(gs == null) {
				gs = (vGeneralSetting__mdt)Database.query(QUERY.replace('{0}', String.join(FIELDS, ',')));
			}

			return gs;
		}
	}
}