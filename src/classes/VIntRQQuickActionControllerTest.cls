@isTest
public with sharing class VIntRQQuickActionControllerTest {
	@isTest
	static void testController() {
		Case cse = new Case(Subject = 'Test');
		insert cse;

		VInternalRequestQuickActionController controller = new VInternalRequestQuickActionController(
			new ApexPages.StandardController(cse));

		controller.internalRequest.Initial_Request__c = 'Test';

		System.assertEquals(null, controller.primaryTabId);
		System.assertEquals(null, controller.saveRecord());
		System.assert(controller.shouldClose);
	}
}