global with sharing class VInternalRequestQuickActionController {
	public static final ID RECORD_TYPE_ID = [select Id 
		from RecordType
		where SObjectType = 'Internal_Request__c'
		and DeveloperName = 'Case'].Id;

	public Case cse { get; private set; }
	public Internal_Request__c internalRequest { get; private set; }
	public ID internalRequestId { get; set; }
	public Boolean shouldClose { get; private set; }

	public String primaryTabId { get; set; }

	public VInternalRequestQuickActionController(ApexPages.StandardController controller) {
		this.cse = (Case)controller.getRecord();
		this.internalRequest = createBlankInternalRequest();
		this.shouldClose = false;
	}

	public PageReference saveRecord() {
		PageReference res = null;

		try {
			insert internalRequest;
			internalRequestId = internalRequest.Id;

			internalRequest = createBlankInternalRequest();
			shouldClose = true;

			ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.CONFIRM, 'Success'));
		} catch(Exception e) {
			ApexPages.addMessages(e);
		}

		return res;
	}

	private Internal_Request__c createBlankInternalRequest() {
		return new Internal_Request__c(Case__c = cse.Id, RecordTypeId = RECORD_TYPE_ID);
	}
}