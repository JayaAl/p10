public with sharing class VUtil {
	public static List<String> listify(String input, String sep) {
		List<String> res = new List<String>();
		List<String> tmp = input.split(sep);

		for(String s : tmp) {
			res.add(s.trim());
		}

		return res;
	}

	public static List<String> parseName(String input) {
		if(String.isBlank(input)) {
			return null;
		}

		List<String> res = new List<String>();
		List<String> tmp = input.split(' ');

		if(tmp.size() == 1) {
			res.add('');
			res.add(tmp[0]);
		} else {
			res.add(tmp[0]);
			List<String> lastName = new List<String>();

			for(Integer i = 1; i < tmp.size(); i++) {
				lastName.add(tmp[i]);
			}

			res.add(String.join(lastName, ' '));
		}

		return res;
	}
}