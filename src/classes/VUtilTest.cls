@isTest
public with sharing class VUtilTest {
	@isTest
	static void testListify() {
		List<String> tst = VUtil.listify('test1, test2', ',');
		System.assertEquals(2, tst.size());
		System.assertEquals(5, tst[0].length());
		System.assertEquals(5, tst[1].length());
	}

	@isTest
	static void testParseName() {
		System.assertEquals(null, VUtil.parseName(null));
		System.assertEquals(2, VUtil.parseName('Test Test').size());
		System.assertEquals(2, VUtil.parseName('Test Test Test').size());
		System.assertEquals(2, VUtil.parseName('Test').size());
	}
}