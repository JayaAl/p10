public class VerticalCategoryHandler
{
    public Map<string,Vertical_Category_Mapping__c> mapVCM;
    
 
/*****************************************************

        This method is used while Lead Update

*******************************************************/
public void setVerticalCategoryBeforeRecordUpdate(Map<Id, Lead> newMap,  Map<Id, Lead> oldMap)
{
    //set<string> sicCodes =new set<string>();
    set<string> naicsCodes =new set<string>();
    Map<string,Vertical_Category_Mapping__c> mapVCM = new map<string,Vertical_Category_Mapping__c>();
    
    for(id leadId:newMap.keyset())
    {
        system.debug(logginglevel.info,'Here1 32' + newMap.get(leadId).SIC_Code__c);
        system.debug(logginglevel.info,'Here1 33' + (newMap.get(leadId).SIC_Code__c!=oldMap.get(leadId).SIC_Code__c));
        system.debug(logginglevel.info,'Here1 32' + newMap.get(leadId).infer3__Score_Snapshot__c);
        system.debug(logginglevel.info,'Here1 32' + oldMap.get(leadId).infer3__Score_Snapshot__c);
        system.debug(logginglevel.info,'Here1 32' + (newMap.get(leadId).infer3__Score_Snapshot__c!=null && oldMap.get(leadId).infer3__Score_Snapshot__c==null));
        //if(!(String.isBlank(newMap.get(leadId).SIC_Code__c)) && (newMap.get(leadId).SIC_Code__c!=oldMap.get(leadId).SIC_Code__c))
        //else if(!(String.isBlank(newMap.get(leadId).NAICS_Code__c)) && (newMap.get(leadId).NAICS_Code__c!=oldMap.get(leadId).NAICS_Code__c))
        //system.debug(logginglevel.info,'Here1 34' + newMap.get(leadId).infer3__Score_Snapshot__c + '----' + oldMap.get(leadId).infer3__Score_Snapshot__c + '-----' + (!(String.isBlank(newMap.get(leadId).SIC_Code__c))) + '------' + newMap.get(leadId).SIC_Code__c));
        //system.debug(logginglevel.info,'Here1 34' + newMap.get(leadId).infer3__Score_Snapshot__c + '----' + oldMap.get(leadId).infer3__Score_Snapshot__c + '-----' + (!(String.isBlank(newMap.get(leadId).SIC_Code__c))) + '------' + newMap.get(leadId).NAICS_Code__c));
        
        
        /*if(newMap.get(leadId).infer3__Score_Snapshot__c!=null && oldMap.get(leadId).infer3__Score_Snapshot__c==null && (!(String.isBlank(newMap.get(leadId).SIC_Code__c))))
        {
            sicCodes.add(newMap.get(leadId).SIC_Code__c);
        }
        else 
        */
        if(newMap.get(leadId).infer3__Score_Snapshot__c!=null && oldMap.get(leadId).infer3__Score_Snapshot__c==null && (!(String.isBlank(newMap.get(leadId).NAICS_Code__c))))
        {
            naicsCodes.add(newMap.get(leadId).NAICS_Code__c);
        }
        
    }
    
    
    //mapVCM = VerticalCategoryUtil.getVerticalCategoryMappings(sicCodes,naicsCodes);
    mapVCM = VerticalCategoryUtil.getVerticalCategoryMappings(naicsCodes);
    
    system.debug(logginglevel.info,'Here1 34' + newMap);
    system.debug(logginglevel.info,'Here1 35' + oldMap);
    system.debug(logginglevel.info,'Here1 36' + mapVCM);
    VerticalCategoryHelper refHelper = new VerticalCategoryHelper();
    refHelper.setVerticalCategoryForLeads(newMap,oldMap,mapVCM);
    
    
}




/******************************************************

This method is used while Contact Creation

*******************************************************/
public void setVerticalCategoryBeforeRecordInsert(List<sObject> lstSObjects)
{
    system.debug(logginglevel.info,'Here1' + lstSObjects);
    //Map<string,list<sObject>> sicCodeMatched = new map<string,list<sObject>>();
    Map<string,list<sObject>> naicsCodeMatched = new Map<string,list<sObject>>();
    Map<string,Vertical_Category_Mapping__c> mapVCM = new map<string,Vertical_Category_Mapping__c>();
    //list<sObject> lstSicsObjects = new list<sObject>();
    list<sObject> lstNaicssObjects = new list<sObject>();
    Map<id,account> mapAccounts = new Map<id,account>();
    mapAccounts = VerticalCategoryUtil.getAccounts(lstSObjects);
    
    for(sObject obj:lstSObjects)
    {
        if(mapAccounts.size()>0)
        {
            /*if(mapAccounts.get((id)obj.get('accountId')).Sic!=null && !(String.isBlank(mapAccounts.get((id)obj.get('accountId')).Sic)))
            {
                lstSicsObjects.add(obj);
                sicCodeMatched.put(mapAccounts.get((id)obj.get('accountId')).Sic,lstSicsObjects);
            }
            else*/ 
            if(!(String.isBlank(mapAccounts.get((id)obj.get('accountId')).NAICS_Code__c)))
            {
                lstNaicssObjects.add(obj);
                naicsCodeMatched.put(mapAccounts.get((id)obj.get('accountId')).NAICS_Code__c,lstNaicssObjects);
            }
        }
        
    }

    //system.debug(logginglevel.info,'Here1 sicCodeMatched' + sicCodeMatched);
    system.debug(logginglevel.info,'Here1 naicsCodeMatched' + naicsCodeMatched);
    
    
    
    //mapVCM = getVerticalCategoryMapping1(sicCodeMatched,naicsCodeMatched);
    //mapVCM =  VerticalCategoryUtil.getVerticalCategoryMappings(sicCodeMatched.keySet(),naicsCodeMatched.keySet());
    mapVCM =  VerticalCategoryUtil.getVerticalCategoryMappings(naicsCodeMatched.keySet());
    system.debug(logginglevel.info,'Here1 mapVCM' + mapVCM);
    VerticalCategoryHelper refHelper = new VerticalCategoryHelper();
    refHelper.setVerticalCategoryForContacts(lstSObjects,mapVCM);
    
    
}




 

/*******************************************************

    This is used while Account Update
*******************************************************/


    public void setVerticalCategoryAfterRecordUpdate(map<id,sObject> newMapSObjects,map<id,sObject> oldMapSObjects)
    {
        system.debug(logginglevel.info,'Here2' + newMapSObjects);
        //Map<id,string> sicCodeMatched = new map<id,string>();
        Map<id,string> naicsCodeMatched = new Map<id,string>();
        //set<string> sicCodes =new set<string>();
        set<string> naicsCodes =new set<string>();
        Map<string,Vertical_Category_Mapping__c> mapVCM = new map<string,Vertical_Category_Mapping__c>();
        //list<sObject> lstSicsObjects = new list<sObject>();
        list<sObject> lstNaicssObjects = new list<sObject>();
        set<Id> accountId = new set<id>();
        Map<id,sobject> mapAccounts = new map<id,sobject>();
        
        for(id objId:newMapSObjects.keyset())
        {
            //system.debug(logginglevel.info,'Here2 332 sicCodeMatched' + (!(String.isBlank((string)newMapSObjects.get(objId).get('sic'))) && (newMapSObjects.get(objId).get('sic')!=oldMapSObjects.get(objId).get('SIC'))));    
            //system.debug(logginglevel.info,'Here2 331 sicCodeMatched' + ((string)newMapSObjects.get(objId).get('sic')) + '------' + newMapSObjects.get(objId).get('sic') + '--------' + oldMapSObjects.get(objId).get('SIC'));    
            

            system.debug(logginglevel.info,'Here2 332 naicsCodeMatched' + (!(String.isBlank((string)newMapSObjects.get(objId).get('NAICS_Code__c'))) && (newMapSObjects.get(objId).get('NAICS_Code__c')!=oldMapSObjects.get(objId).get('NAICS_Code__c'))));    
            //system.debug(logginglevel.info,'Here2 331 naicsCodeMatched' + ((string)newMapSObjects.get(objId).get('sic')) + '------' + newMapSObjects.get(objId).get('sic') + '--------' + oldMapSObjects.get(objId).get('SIC'));    
            
            /*if(!(String.isBlank((string)newMapSObjects.get(objId).get('sic'))) && (newMapSObjects.get(objId).get('sic')!=oldMapSObjects.get(objId).get('sic')))
            {
                sicCodes.add((string)newMapSObjects.get(objId).get('sic'));
                mapAccounts.put(objId,newMapSObjects.get(objId));
            }
            else */
            if(!(String.isBlank((string)newMapSObjects.get(objId).get('NAICS_Code__c'))) && (newMapSObjects.get(objId).get('NAICS_Code__c')!=oldMapSObjects.get(objId).get('NAICS_Code__c')))
            {
                naicsCodes.add((string)newMapSObjects.get(objId).get('NAICS_Code__c'));
                mapAccounts.put(objId,newMapSObjects.get(objId));
            
            }        
        }
        //system.debug(logginglevel.info,'Here2 331 sicCodeMatched' + sicCodes);    
        system.debug(logginglevel.info,'Here2 331 naicsCodes' + naicsCodes);    
            

        //mapVCM = getVerticalCategoryMapping(sicCodeMatched,naicsCodeMatched);
        VerticalCategoryHelper refHelper = new VerticalCategoryHelper();
        //mapVCM = VerticalCategoryUtil.getVerticalCategoryMappings(sicCodes,naicsCodes);
        mapVCM = VerticalCategoryUtil.getVerticalCategoryMappings(naicsCodes);
        system.debug(logginglevel.info,'Here2 331 mapVCM' + mapVCM);  
        
        refHelper.setVerticalCategoryForContactsFromAccount(newMapSObjects,oldMapSObjects,mapAccounts,mapVCM);
        
        
    }

}