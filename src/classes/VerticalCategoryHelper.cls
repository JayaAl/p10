public with sharing class VerticalCategoryHelper
{

/*****************************************************

        This method is used while Lead Update

*******************************************************/
public void setVerticalCategoryForLeads(Map<Id, Lead> newMap,  Map<Id, Lead> oldMap, Map<string,Vertical_Category_Mapping__c> mapVCM)
{
    system.debug(logginglevel.info,'Here1 11' + newMap);
    system.debug(logginglevel.info,'Here1 12' + oldMap);
    system.debug(logginglevel.info,'Here1 13' + mapVCM);
    for(id leadId:newMap.keyset())
    {
        system.debug(logginglevel.info,'Here1 16' + newMap.get(leadId).SIC_Code__c);
        //if(!(String.isBlank(newMap.get(leadId).SIC_Code__c)) && (newMap.get(leadId).SIC_Code__c!=oldMap.get(leadId).SIC_Code__c))
        //else if(!(String.isBlank(newMap.get(leadId).NAICS_Code__c)) && (newMap.get(leadId).NAICS_Code__c!=oldMap.get(leadId).NAICS_Code__c))
        /*if(newMap.get(leadId).infer3__Score_Snapshot__c!=null && oldMap.get(leadId).infer3__Score_Snapshot__c==null && (!(String.isBlank(newMap.get(leadId).SIC_Code__c))))
        {
            
            if(mapVCM.size()==0)
            {
                newMap.get(leadId).Vertical_Category_Mapping__c = null;
                newMap.get(leadId).Vertical_Category_Mapping_Name__c = null;
            }
            else
            {
                  newMap.get(leadId).Vertical_Category_Mapping__c = mapVCM.get(newMap.get(leadId).SIC_Code__c).Id;
                  newMap.get(leadId).Vertical_Category_Mapping_Name__c = mapVCM.get(newMap.get(leadId).SIC_Code__c).Industry_category__c;
            }
        }
        else */
        if(newMap.get(leadId).infer3__Score_Snapshot__c!=null && oldMap.get(leadId).infer3__Score_Snapshot__c==null && (!(String.isBlank(newMap.get(leadId).NAICS_Code__c))))
        {
            if(mapVCM.size()==0)
            {
                newMap.get(leadId).Vertical_Category_Mapping__c = null;
                newMap.get(leadId).Vertical_Category_Mapping_Name__c = null;
            }
            else
            {
                  newMap.get(leadId).Vertical_Category_Mapping__c = mapVCM.get(newMap.get(leadId).NAICS_Code__c).Id;
                  newMap.get(leadId).Vertical_Category_Mapping_Name__c = mapVCM.get(newMap.get(leadId).NAICS_Code__c).Industry_category__c;
            }
            
        }
    }
}

/******************************************************

This method is used while Contact Creation

*******************************************************/
public void setVerticalCategoryForContacts(List<sObject> lstSObjects,Map<string,Vertical_Category_Mapping__c> mapVCM)
{
    set<Id> accountId = new set<id>();
    Map<id,account> mapAccounts = new map<id,account>();
    mapAccounts = VerticalCategoryUtil.getAccounts(lstSObjects);
    for(sObject obj:lstSObjects)
    {
        system.debug(logginglevel.info,'Here1 149 c' + obj);
        system.debug(logginglevel.info,'Here1 149 c' + (id)obj.get('accountId'));

        system.debug(logginglevel.info,'Here1 149 c' + mapAccounts.get((id)obj.get('accountId')));
        /*if(mapAccounts.get((id)obj.get('AccountId'))!=null && !(String.isBlank(mapAccounts.get((id)obj.get('accountId')).Sic)))
        {
            system.debug(logginglevel.info,'Here1 152' + mapVCM);
            if(mapVCM.size()==0)
            {
                system.debug(logginglevel.info,'Here1 155' + mapVCM);
                obj.put('Vertical_Category_Mapping__c',null);
                obj.put('Vertical_Category__c',null);
            }
            else
            {
                  //system.debug(logginglevel.info,'Here1 161' + mapVCM.get(mapAccounts.get(obj.AccountId).Sic).Id);
                  //system.debug(logginglevel.info,'Here1 162' + mapVCM.get(mapAccounts.get(obj.AccountId).Sic).Industry_category__c);
                  obj.put('Vertical_Category_Mapping__c',mapVCM.get(mapAccounts.get((id)obj.get('AccountId')).Sic).Id);
                  obj.put('Vertical_Category__c', mapVCM.get(mapAccounts.get((id)obj.get('AccountId')).Sic).Industry_category__c);
            }
        }
        else */
        if(mapAccounts.get((id)obj.get('AccountId'))!=null && !(String.isBlank(mapAccounts.get((id)obj.get('AccountId')).NAICS_Code__c)))
        {
            system.debug(logginglevel.info,'Here1 169' + mapVCM);
            if(mapVCM.size()==0)
            {
                system.debug(logginglevel.info,'Here1 mapVCM' + mapVCM);
                obj.put('Vertical_Category_Mapping__c',null);
                obj.put('Vertical_Category__c',null);
            }
            else
            {
                  system.debug(logginglevel.info,'Here1 178' + mapVCM.get(mapAccounts.get((id)obj.get('AccountId')).NAICS_Code__c).Id);
                  system.debug(logginglevel.info,'Here1 179' + mapVCM.get(mapAccounts.get((id)obj.get('AccountId')).NAICS_Code__c).Industry_category__c);
                  obj.put('Vertical_Category_Mapping__c',mapVCM.get(mapAccounts.get((id)obj.get('AccountId')).NAICS_Code__c).Id);
                  obj.put('Vertical_Category__c', mapVCM.get(mapAccounts.get((id)obj.get('AccountId')).NAICS_Code__c).Industry_category__c);
            }
            
        }
    }
} 

/*******************************************************

    This is used while Account Update
*******************************************************/
public void setVerticalCategoryForContactsFromAccount(map<id,sObject> newMapSObjects,map<id,sObject> oldMapSObjects,Map<id,sobject> mapAccounts,Map<string,Vertical_Category_Mapping__c> mapVCM)
{
    
    list<sObject> sObjectsToUpdate = new  list<sObject>();
    for(sObject obj:[select id,Vertical_Category_Mapping__c,Vertical_Category__c,AccountId,account.sic,account.NAICS_Code__c from contact where accountid in:mapAccounts.keyset()])
    {
        system.debug(logginglevel.info,'Here2 354 c' + obj);
        system.debug(logginglevel.info,'Here2 355 c' + (id)obj.get('accountId'));
        system.debug(logginglevel.info,'Here2 356 c' + mapAccounts.get((id)obj.get('accountId')));
        /*if(mapAccounts.get((id)obj.get('AccountId'))!=null && !(String.isBlank((string)mapAccounts.get((id)obj.get('accountId')).get('Sic'))))
        {
            system.debug(logginglevel.info,'Here2 359' + mapVCM);
            if(mapVCM.size()==0)
            {
                system.debug(logginglevel.info,'Here2 362' + mapVCM);
                obj.put('Vertical_Category_Mapping__c',null);
                obj.put('Vertical_Category__c',null);
            }
            else
            {
                  //system.debug(logginglevel.info,'Here1 161' + mapVCM.get(mapAccounts.get(obj.AccountId).Sic).Id);
                  //system.debug(logginglevel.info,'Here1 162' + mapVCM.get(mapAccounts.get(obj.AccountId).Sic).Industry_category__c);
                  obj.put('Vertical_Category_Mapping__c',mapVCM.get((string)mapAccounts.get((id)obj.get('AccountId')).get('Sic')).Id);
                  obj.put('Vertical_Category__c', mapVCM.get((string)mapAccounts.get((id)obj.get('AccountId')).get('Sic')).Industry_category__c);
            }
            
        }
        else */
        if(mapAccounts.get((id)obj.get('AccountId'))!=null && !(String.isBlank((string)mapAccounts.get((id)obj.get('AccountId')).get('NAICS_Code__c'))))
        {
            system.debug(logginglevel.info,'Here1 377' + mapVCM);
            if(mapVCM.size()==0)
            {
                system.debug(logginglevel.info,'Here1 380 mapVCM' + mapVCM);
                obj.put('Vertical_Category_Mapping__c',null);
                obj.put('Vertical_Category__c',null);
            }
            else
            {
                  system.debug(logginglevel.info,'Here2 386' + mapVCM.get((string)mapAccounts.get((id)obj.get('AccountId')).get('NAICS_Code__c')).Id);
                  system.debug(logginglevel.info,'Here2 387' + mapVCM.get((string)mapAccounts.get((id)obj.get('AccountId')).get('NAICS_Code__c')).Industry_category__c);
                  obj.put('Vertical_Category_Mapping__c',mapVCM.get((string)mapAccounts.get((id)obj.get('AccountId')).get('NAICS_Code__c')).Id);
                  obj.put('Vertical_Category__c', mapVCM.get((string)mapAccounts.get((id)obj.get('AccountId')).get('NAICS_Code__c')).Industry_category__c);
            }
            
        }

        sObjectsToUpdate.add(obj);

    }

    if(sObjectsToUpdate.size()>0)
        database.update(sObjectsToUpdate);
    
}

/*
public void setVerticalCategoryBeforeRecordInsert(List<sObject> lstSObjects)
{
    system.debug(logginglevel.info,'Here1' + lstSObjects);
    Map<string,list<sObject>> sicCodeMatched = new map<string,list<sObject>>();
    Map<string,list<sObject>> naicsCodeMatched = new Map<string,list<sObject>>();
    Map<string,Vertical_Category_Mapping__c> mapVCM = new map<string,Vertical_Category_Mapping__c>();
    list<sObject> lstSicsObjects = new list<sObject>();
    list<sObject> lstNaicssObjects = new list<sObject>();
    set<Id> accountId = new set<id>();
    Map<id,account> mapAccounts = new map<id,account>();
    for(sObject obj:lstSObjects)
    {
        accountId.add((id)obj.get('accountId'));
    }
    for(account a:[select id,sic,NAICS_Code__c from account where id in:accountId])
    {
        mapAccounts.put(a.id,a);
    }
    for(sObject obj:lstSObjects)
    {
        if(!(String.isBlank(mapAccounts.get((id)obj.get('accountId')).Sic)))
        {
            lstSicsObjects.add(obj);
            sicCodeMatched.put(mapAccounts.get((id)obj.get('accountId')).Sic,lstSicsObjects);
        }
        else if(!(String.isBlank(mapAccounts.get((id)obj.get('accountId')).NAICS_Code__c)))
        {
            lstNaicssObjects.add(obj);
            naicsCodeMatched.put(mapAccounts.get((id)obj.get('accountId')).NAICS_Code__c,lstNaicssObjects);
        }
        
    }

    system.debug(logginglevel.info,'Here1 sicCodeMatched' + sicCodeMatched);
    system.debug(logginglevel.info,'Here1 naicsCodeMatched' + naicsCodeMatched);
    
    
    
    //mapVCM = getVerticalCategoryMapping1(sicCodeMatched,naicsCodeMatched);
    mapVCM =  getVerticalCategoryMappings(sicCodeMatched.keySet(),naicsCodeMatched.keySet());
    system.debug(logginglevel.info,'Here1 mapVCM' + mapVCM);
    for(sObject obj:lstSObjects)
    {
        system.debug(logginglevel.info,'Here1 149 c' + obj);
        system.debug(logginglevel.info,'Here1 149 c' + (id)obj.get('accountId'));
        system.debug(logginglevel.info,'Here1 149 c' + mapAccounts.get((id)obj.get('accountId')));
        if(mapAccounts.get((id)obj.get('AccountId'))!=null && !(String.isBlank(mapAccounts.get((id)obj.get('accountId')).Sic)))
        {
            system.debug(logginglevel.info,'Here1 152' + mapVCM);
            if(mapVCM.size()==0)
            {
                system.debug(logginglevel.info,'Here1 155' + mapVCM);
                obj.put('Vertical_Category_Mapping__c',null);
                obj.put('Vertical_Category__c',null);
            }
            else
            {
                  //system.debug(logginglevel.info,'Here1 161' + mapVCM.get(mapAccounts.get(obj.AccountId).Sic).Id);
                  //system.debug(logginglevel.info,'Here1 162' + mapVCM.get(mapAccounts.get(obj.AccountId).Sic).Industry_category__c);
                  obj.put('Vertical_Category_Mapping__c',mapVCM.get(mapAccounts.get((id)obj.get('AccountId')).Sic).Id);
                  obj.put('Vertical_Category__c', mapVCM.get(mapAccounts.get((id)obj.get('AccountId')).Sic).Industry_category__c);
            }
        }
        else if(mapAccounts.get((id)obj.get('AccountId'))!=null && !(String.isBlank(mapAccounts.get((id)obj.get('AccountId')).NAICS_Code__c)))
        {
            system.debug(logginglevel.info,'Here1 169' + mapVCM);
            if(mapVCM.size()==0 && string.isBlank(mapAccounts.get((id)obj.get('AccountId')).Sic))
            {
                system.debug(logginglevel.info,'Here1 mapVCM' + mapVCM);
                obj.put('Vertical_Category_Mapping__c',null);
                obj.put('Vertical_Category__c',null);
            }
            else
            {
                  system.debug(logginglevel.info,'Here1 178' + mapVCM.get(mapAccounts.get((id)obj.get('AccountId')).NAICS_Code__c).Id);
                  system.debug(logginglevel.info,'Here1 179' + mapVCM.get(mapAccounts.get((id)obj.get('AccountId')).NAICS_Code__c).Industry_category__c);
                  obj.put('Vertical_Category_Mapping__c',mapVCM.get(mapAccounts.get((id)obj.get('AccountId')).NAICS_Code__c).Id);
                  obj.put('Vertical_Category__c', mapVCM.get(mapAccounts.get((id)obj.get('AccountId')).NAICS_Code__c).Industry_category__c);
            }
            
        }
    }
}
*/




    /*public void setVerticalCategoryBeforeRecordUpdate(Map<Id, Lead> newMap,  Map<Id, Lead> oldMap)
    {
        //Map<id,string> sicCodeMatchedLeads = new map<id,string>();
        //Map<id,string> naicsCodeMatchedLeads = new map<id,string>();
        set<string> sicCodes =new set<string>();
        set<string> naicsCodes =new set<string>();
        Map<string,Vertical_Category_Mapping__c> mapVCM = new map<string,Vertical_Category_Mapping__c>();
        
        for(id leadId:newMap.keyset())
        {
            if(!(String.isBlank(newMap.get(leadId).SIC_Code__c)) && (newMap.get(leadId).SIC_Code__c!=oldMap.get(leadId).SIC_Code__c))
            {
                sicCodes.add(newMap.get(leadId).SIC_Code__c);
            }
            else if(!(String.isBlank(newMap.get(leadId).NAICS_Code__c)) && (newMap.get(leadId).NAICS_Code__c!=oldMap.get(leadId).NAICS_Code__c))
            {
                naicsCodes.add(newMap.get(leadId).NAICS_Code__c);
            }
            
        }
        
        //VerticalCategoryHelper vcHelper = new VerticalCategoryHelper();
        
        //mapVCM = vcHelper.getVerticalCategoryMapping(sicCodeMatchedLeads,naicsCodeMatchedLeads;
        mapVCM = getVerticalCategoryMappings(sicCodes,naicsCodes);
        
        
        for(id leadId:newMap.keyset())
        {
            if(!(String.isBlank(newMap.get(leadId).SIC_Code__c)) && (newMap.get(leadId).SIC_Code__c!=oldMap.get(leadId).SIC_Code__c))
            {
                
                if(mapVCM.size()==0)
                {
                    newMap.get(leadId).Vertical_Category_Mapping__c = null;
                    newMap.get(leadId).Vertical_Category_Mapping_Name__c = null;
                }
                else
                {
                      newMap.get(leadId).Vertical_Category_Mapping__c = mapVCM.get(newMap.get(leadId).SIC_Code__c).Id;
                      newMap.get(leadId).Vertical_Category_Mapping_Name__c = mapVCM.get(newMap.get(leadId).SIC_Code__c).Name;
                }
            }
            else if(!(String.isBlank(newMap.get(leadId).NAICS_Code__c)) && (newMap.get(leadId).NAICS_Code__c!=oldMap.get(leadId).NAICS_Code__c))
            {
                if(mapVCM.size()==0 && string.isBlank(newMap.get(leadId).SIC_Code__c))
                {
                    newMap.get(leadId).Vertical_Category_Mapping__c = null;
                    newMap.get(leadId).Vertical_Category_Mapping_Name__c = null;
                }
                else
                {
                      newMap.get(leadId).Vertical_Category_Mapping__c = mapVCM.get(newMap.get(leadId).NAICS_Code__c).Id;
                      newMap.get(leadId).Vertical_Category_Mapping_Name__c = mapVCM.get(newMap.get(leadId).NAICS_Code__c).Name;
                }
                
            }
        }
    }*/






/*public void setVerticalCategoryAfterRecordUpdate(map<id,sObject> newMapSObjects,map<id,sObject> oldMapSObjects)
{
    system.debug(logginglevel.info,'Here2' + newMapSObjects);
    Map<id,string> sicCodeMatched = new map<id,string>();
    Map<id,string> naicsCodeMatched = new Map<id,string>();
    set<string> sicCodes =new set<string>();
    set<string> naicsCodes =new set<string>();
    Map<string,Vertical_Category_Mapping__c> mapVCM = new map<string,Vertical_Category_Mapping__c>();
    list<sObject> lstSicsObjects = new list<sObject>();
    list<sObject> lstNaicssObjects = new list<sObject>();
    set<Id> accountId = new set<id>();
    Map<id,sobject> mapAccounts = new map<id,sobject>();
    
    for(id objId:newMapSObjects.keyset())
    {
        system.debug(logginglevel.info,'Here2 332 sicCodeMatched' + (!(String.isBlank((string)newMapSObjects.get(objId).get('sic'))) && (newMapSObjects.get(objId).get('sic')!=oldMapSObjects.get(objId).get('SIC'))));    
        system.debug(logginglevel.info,'Here2 331 sicCodeMatched' + ((string)newMapSObjects.get(objId).get('sic')) + '------' + newMapSObjects.get(objId).get('sic') + '--------' + oldMapSObjects.get(objId).get('SIC'));    
        if(!(String.isBlank((string)newMapSObjects.get(objId).get('sic'))) && (newMapSObjects.get(objId).get('sic')!=oldMapSObjects.get(objId).get('sic')))
        {
            sicCodes.add((string)newMapSObjects.get(objId).get('sic'));
            mapAccounts.put(objId,newMapSObjects.get(objId));
        }
        else if(!(String.isBlank((string)newMapSObjects.get(objId).get('NAICS_Code__c'))) && (newMapSObjects.get(objId).get('NAICS_Code__c')!=oldMapSObjects.get(objId).get('NAICS_Code__c')))
        {
            naicsCodes.add((string)newMapSObjects.get(objId).get('NAICS_Code__c'));
            mapAccounts.put(objId,newMapSObjects.get(objId));
        
        }        
    }
    
    
    
    
    //mapVCM = getVerticalCategoryMapping(sicCodeMatched,naicsCodeMatched);
    mapVCM = getVerticalCategoryMappings(sicCodes,naicsCodes);
    system.debug(logginglevel.info,'Here2 351 mapVCM' + mapVCM);
    list<sObject> sObjectsToUpdate = new  list<sObject>();
    for(sObject obj:[select id,Vertical_Category_Mapping__c,Vertical_Category__c,AccountId,account.sic,account.NAICS_Code__c from contact where accountid in:mapAccounts.keyset()])
    {
        system.debug(logginglevel.info,'Here2 354 c' + obj);
        system.debug(logginglevel.info,'Here2 355 c' + (id)obj.get('accountId'));
        system.debug(logginglevel.info,'Here2 356 c' + mapAccounts.get((id)obj.get('accountId')));
        if(mapAccounts.get((id)obj.get('AccountId'))!=null && !(String.isBlank((string)mapAccounts.get((id)obj.get('accountId')).get('Sic'))))
        {
            system.debug(logginglevel.info,'Here2 359' + mapVCM);
            if(mapVCM.size()==0)
            {
                system.debug(logginglevel.info,'Here2 362' + mapVCM);
                obj.put('Vertical_Category_Mapping__c',null);
                obj.put('Vertical_Category__c',null);
            }
            else
            {
                  //system.debug(logginglevel.info,'Here1 161' + mapVCM.get(mapAccounts.get(obj.AccountId).Sic).Id);
                  //system.debug(logginglevel.info,'Here1 162' + mapVCM.get(mapAccounts.get(obj.AccountId).Sic).Industry_category__c);
                  obj.put('Vertical_Category_Mapping__c',mapVCM.get((string)mapAccounts.get((id)obj.get('AccountId')).get('Sic')).Id);
                  obj.put('Vertical_Category__c', mapVCM.get((string)mapAccounts.get((id)obj.get('AccountId')).get('Sic')).Industry_category__c);
            }
            
        }
        else if(mapAccounts.get((id)obj.get('AccountId'))!=null && !(String.isBlank((string)mapAccounts.get((id)obj.get('AccountId')).get('NAICS_Code__c'))))
        {
            system.debug(logginglevel.info,'Here1 377' + mapVCM);
            if(mapVCM.size()==0 && string.isBlank((string)mapAccounts.get((id)obj.get('AccountId')).get('sic')))
            {
                system.debug(logginglevel.info,'Here1 380 mapVCM' + mapVCM);
                obj.put('Vertical_Category_Mapping__c',null);
                obj.put('Vertical_Category__c',null);
            }
            else
            {
                  system.debug(logginglevel.info,'Here2 386' + mapVCM.get((string)mapAccounts.get((id)obj.get('AccountId')).get('NAICS_Code__c')).Id);
                  system.debug(logginglevel.info,'Here2 387' + mapVCM.get((string)mapAccounts.get((id)obj.get('AccountId')).get('NAICS_Code__c')).Industry_category__c);
                  obj.put('Vertical_Category_Mapping__c',mapVCM.get((string)mapAccounts.get((id)obj.get('AccountId')).get('NAICS_Code__c')).Id);
                  obj.put('Vertical_Category__c', mapVCM.get((string)mapAccounts.get((id)obj.get('AccountId')).get('NAICS_Code__c')).Industry_category__c);
            }
            
        }

        sObjectsToUpdate.add(obj);

    }

    if(sObjectsToUpdate.size()>0)
        database.update(sObjectsToUpdate);
    
}
*/










/*******************************************************/

/*  public Map<string,Vertical_Category_Mapping__c> getVerticalCategoryMapping(Map<id,string> sicCodeMatched, Map<id,string> naicsCodeMatched)
    {
        mapVCM = new Map<string,Vertical_Category_Mapping__c>();
        if(sicCodeMatched.size()>0){
            for(Vertical_Category_Mapping__c vcm: [select id, Industry_category__c,SIC_Code__c from Vertical_Category_Mapping__c where SIC_Code__c in: sicCodeMatched.values()])
            {
                mapVCM.put(vcm.SIC_Code__c,vcm);
            }
        }
        else if(naicsCodeMatched.size()>0){
            for(Vertical_Category_Mapping__c vcm: [select id, Industry_category__c,NAICS_Code__c from Vertical_Category_Mapping__c where NAICS_Code__c in: naicsCodeMatched.values()])
            {
                mapVCM.put(vcm.NAICS_Code__c,vcm);
            }
        }
        
        return mapVCM;
    
    }


    public Map<string,Vertical_Category_Mapping__c> getVerticalCategoryMapping1(Map<string,list<contact>> sicCodeMatched, Map<string,list<contact>> naicsCodeMatched)
    {
        mapVCM = new Map<string,Vertical_Category_Mapping__c>();
        if(sicCodeMatched.size()>0){
            for(Vertical_Category_Mapping__c vcm: [select id, Industry_category__c,SIC_Code__c from Vertical_Category_Mapping__c where SIC_Code__c in: sicCodeMatched.keyset()])
            {
                mapVCM.put(vcm.SIC_Code__c,vcm);
            }
        }
        else if(naicsCodeMatched.size()>0){
            for(Vertical_Category_Mapping__c vcm: [select id, Industry_category__c,NAICS_Code__c from Vertical_Category_Mapping__c where NAICS_Code__c in: naicsCodeMatched.keyset()])
            {
                mapVCM.put(vcm.NAICS_Code__c,vcm);
            }
        }
        
        return mapVCM;
    
    }
*/

    /*public void getVerticalCategoryMapping(map<string,list<string>> codes)
    {
        set<string>sicCodes = new set<string>();
        set<string>nicCodes = new set<string>();
        Map<string,Vertical_Category_Mapping__c> categoryMappings = new Map<string,Vertical_Category_Mapping__c>();
        for(string type:codes.keySet())
        {
            if(type=='SICCODE')
            {
                sicCodes.addall(codes.get(type));
            }
            else
            {
                nicCodes.addall(codes.get(type));
            }
        }
        
        public List<Vertical_Category_Mapping__c> lstVM = new List<Vertical_Category_Mapping__c>();
        if(sicCodes.size()>0)
            lstVM = [SELECT Active__c,Description__c,Id,Industry_Category__c,NAICS_Code__c,Name,Sector__c,SIC_Code__c FROM Vertical_Category_Mapping__c where SIC_Code__c in: (sicCodes)];
        if(nicCodes.size()>0)
            lstVM = [SELECT Active__c,Description__c,Id,Industry_Category__c,NAICS_Code__c,Name,Sector__c,SIC_Code__c FROM Vertical_Category_Mapping__c where NAICS_Code__c in: (nicCodes)];
    
        mapCodeToVerticalCategories = new map<string,list<Vertical_Category_Mapping__c>>();
        for(Vertical_Category_Mapping__c vm: lstVM){
            
        
        }
    
    
    
    }
    
    
    public void mapFields(Map<Id, Lead> newMap,  Map<Id, Lead> oldMap)
    {
        mapVM
        
    }*/

    /*******************************************************/
    /*public void setVerticalCategory1(List<contact> lstSObjects)
    {
        system.debug(logginglevel.info,'Here1' + lstSObjects);
        Map<string,list<contact>> sicCodeMatched = new map<string,list<contact>>();
        Map<string,list<contact>> naicsCodeMatched = new Map<string,list<contact>>();
        Map<string,Vertical_Category_Mapping__c> mapVCM = new map<string,Vertical_Category_Mapping__c>();
        list<contact> lstSicContacts = new list<contact>();
        list<contact> lstNaicsContacts = new list<contact>();
        set<Id> accountId = new set<id>();
        Map<id,account> mapAccounts = new map<id,account>();
        for(contact c:lstSObjects)
        {
            accountId.add(c.accountId);
        }
        for(account a:[select id,sic,NAICS_Code__c from account where id in:accountId])
        {
            mapAccounts.put(a.id,a);
        }



        for(contact c:lstSObjects)
        {
            if(!(String.isBlank(mapAccounts.get(c.AccountId).Sic)))
            {
                lstSicContacts.add(c);
                sicCodeMatched.put(mapAccounts.get(c.AccountId).Sic,lstSicContacts);
            }
            else if(!(String.isBlank(mapAccounts.get(c.AccountId).NAICS_Code__c)))
            {
                lstNaicsContacts.add(c);
                naicsCodeMatched.put(mapAccounts.get(c.AccountId).NAICS_Code__c,lstNaicsContacts);
            }
            
        }

        system.debug(logginglevel.info,'Here1 sicCodeMatched' + sicCodeMatched);
        system.debug(logginglevel.info,'Here1 naicsCodeMatched' + naicsCodeMatched);
        
        
        
        mapVCM = getVerticalCategoryMapping1(sicCodeMatched,naicsCodeMatched);
        system.debug(logginglevel.info,'Here1 mapVCM' + mapVCM);
        for(contact c:lstSObjects)
        {
            system.debug(logginglevel.info,'Here1 149 c' + c);
            system.debug(logginglevel.info,'Here1 149 c' + c.AccountId);
            system.debug(logginglevel.info,'Here1 149 c' + mapAccounts.get(c.AccountId));
            if(mapAccounts.get(c.AccountId)!=null && !(String.isBlank(mapAccounts.get(c.AccountId).Sic)))
            {
                system.debug(logginglevel.info,'Here1 152' + mapVCM);
                if(mapVCM.size()==0)
                {
                    system.debug(logginglevel.info,'Here1 155' + mapVCM);
                    c.Vertical_Category_Mapping__c = null;
                    c.Vertical_Category__c = null;
                }
                else
                {
                      system.debug(logginglevel.info,'Here1 161' + mapVCM.get(mapAccounts.get(c.AccountId).Sic).Id);
                      system.debug(logginglevel.info,'Here1 162' + mapVCM.get(mapAccounts.get(c.AccountId).Sic).Industry_category__c);
                      c.Vertical_Category_Mapping__c = mapVCM.get(mapAccounts.get(c.AccountId).Sic).Id;
                      c.Vertical_Category__c = mapVCM.get(mapAccounts.get(c.AccountId).Sic).Industry_category__c;
                }
            }
            else if(mapAccounts.get(c.AccountId)!=null && !(String.isBlank(mapAccounts.get(c.AccountId).NAICS_Code__c)))
            {
                system.debug(logginglevel.info,'Here1 169' + mapVCM);
                if(mapVCM.size()==0 && string.isBlank(mapAccounts.get(c.AccountId).Sic))
                {
                    system.debug(logginglevel.info,'Here1 mapVCM' + mapVCM);
                    c.Vertical_Category_Mapping__c = null;
                    c.Vertical_Category__c = null;
                }
                else
                {
                      system.debug(logginglevel.info,'Here1 178' + mapVCM.get(mapAccounts.get(c.AccountId).NAICS_Code__c).Id);
                      system.debug(logginglevel.info,'Here1 179' + mapVCM.get(mapAccounts.get(c.AccountId).NAICS_Code__c).Industry_category__c);
                      c.Vertical_Category_Mapping__c = mapVCM.get(mapAccounts.get(c.AccountId).NAICS_Code__c).Id;
                      c.Vertical_Category__c = mapVCM.get(mapAccounts.get(c.AccountId).NAICS_Code__c).Industry_category__c;
                }
                
            }
        }
    }
    */

    /*******************************************************
    public void setVerticalCategoryBeforeRecordUpdate(map<id,sObject> newMapSObjects,map<id,sObject> oldMapSObjects)
    {
        system.debug(logginglevel.info,'Here1' + newMapSObjects);
        Map<string,list<sObject>> sicCodeMatched = new map<string,list<sObject>>();
        Map<string,list<sObject>> naicsCodeMatched = new Map<string,list<sObject>>();
        Map<string,Vertical_Category_Mapping__c> mapVCM = new map<string,Vertical_Category_Mapping__c>();
        list<sObject> lstSicsObjects = new list<sObject>();
        list<sObject> lstNaicssObjects = new list<sObject>();
        set<Id> accountId = new set<id>();
        Map<id,account> mapAccounts = new map<id,account>();
        for(sObject obj:newMapSObjects.values())
        {
            accountId.add((id)obj.get('accountId'));
        }
        for(account a:[select id,sic,NAICS_Code__c from account where id in:accountId])
        {
            mapAccounts.put(a.id,a);
        }



        for(sObject obj:newMapSObjects.values())
        {
            if(!(String.isBlank(mapAccounts.get((id)obj.get('accountId')).Sic)))
            {
                lstSicsObjects.add(obj);
                sicCodeMatched.put(mapAccounts.get((id)obj.get('accountId')).Sic,lstSicsObjects);
            }
            else if(!(String.isBlank(mapAccounts.get((id)obj.get('accountId')).NAICS_Code__c)))
            {
                lstNaicssObjects.add(obj);
                naicsCodeMatched.put(mapAccounts.get((id)obj.get('accountId')).NAICS_Code__c,lstNaicssObjects);
            }
            
        }

        system.debug(logginglevel.info,'Here1 sicCodeMatched' + sicCodeMatched);
        system.debug(logginglevel.info,'Here1 naicsCodeMatched' + naicsCodeMatched);
        
        
        
        mapVCM = getVerticalCategoryMapping1(sicCodeMatched,naicsCodeMatched);
        system.debug(logginglevel.info,'Here1 mapVCM' + mapVCM);
        for(sObject obj:newMapSObjects.values())
        {
            system.debug(logginglevel.info,'Here1 149 c' + obj);
            system.debug(logginglevel.info,'Here1 149 c' + (id)obj.get('accountId'));
            system.debug(logginglevel.info,'Here1 149 c' + mapAccounts.get((id)obj.get('accountId')));
            if(mapAccounts.get((id)obj.get('AccountId'))!=null && !(String.isBlank(mapAccounts.get((id)obj.get('accountId')).Sic)))
            {
                system.debug(logginglevel.info,'Here1 152' + mapVCM);
                if(mapVCM.size()==0)
                {
                    system.debug(logginglevel.info,'Here1 155' + mapVCM);
                    obj.put('Vertical_Category_Mapping__c',null);
                    obj.put('Vertical_Category__c',null);
                }
                else
                {
                      //system.debug(logginglevel.info,'Here1 161' + mapVCM.get(mapAccounts.get(obj.AccountId).Sic).Id);
                      //system.debug(logginglevel.info,'Here1 162' + mapVCM.get(mapAccounts.get(obj.AccountId).Sic).Industry_category__c);
                      obj.put('Vertical_Category_Mapping__c',mapVCM.get(mapAccounts.get((id)obj.get('AccountId')).Sic).Id);
                      obj.put('Vertical_Category__c', mapVCM.get(mapAccounts.get((id)obj.get('AccountId')).Sic).Industry_category__c);
                }
            }
            else if(mapAccounts.get((id)obj.get('AccountId'))!=null && !(String.isBlank(mapAccounts.get((id)obj.get('AccountId')).NAICS_Code__c)))
            {
                system.debug(logginglevel.info,'Here1 169' + mapVCM);
                if(mapVCM.size()==0 && string.isBlank(mapAccounts.get((id)obj.get('AccountId')).Sic))
                {
                    system.debug(logginglevel.info,'Here1 mapVCM' + mapVCM);
                    obj.put('Vertical_Category_Mapping__c',null);
                    obj.put('Vertical_Category__c',null);
                }
                else
                {
                      system.debug(logginglevel.info,'Here1 178' + mapVCM.get(mapAccounts.get((id)obj.get('AccountId')).NAICS_Code__c).Id);
                      system.debug(logginglevel.info,'Here1 179' + mapVCM.get(mapAccounts.get((id)obj.get('AccountId')).NAICS_Code__c).Industry_category__c);
                      obj.put('Vertical_Category_Mapping__c',mapVCM.get(mapAccounts.get((id)obj.get('AccountId')).NAICS_Code__c).Id);
                      obj.put('Vertical_Category__c', mapVCM.get(mapAccounts.get((id)obj.get('AccountId')).NAICS_Code__c).Industry_category__c);
                }
                
            }
        }
    }
********************************************************/
    
    
    
}