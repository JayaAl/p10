@isTest
private class VerticalCategoryTest
{
    @testSetup
    static void createTestData()
    {
        
        
        List<Vertical_Category_Mapping__c> verticalCategorList = new List<Vertical_Category_Mapping__c>();
        List<Account> advAccountList = new List<Account>();
        List<Account> agencyAccountList = new List<Account>();
        /*for(integer i=1;i<6;i++)
        {
            Vertical_Category_Mapping__c vc = new Vertical_Category_Mapping__c();
            vc.Name= 'VC Name' + i;
            vc.Industry_category__c = 'Category' + i;
            vc.Sic_Code__c = '111' + i;
            verticalCategorList.add(vc);
        }*/

        
        for(integer i=6;i<10;i++)
        {
            Vertical_Category_Mapping__c vc = new Vertical_Category_Mapping__c();
            vc.Name = 'VC Name' + i;
            vc.Industry_category__c = 'Category' + i;
            vc.Naics_code__c = '222' + i;
            verticalCategorList.add(vc);
        }
        

        // Create Account with type Advertiser and Agancy in bulk.
        /*for(Integer i = 1; i< 6; i++) {

            Account accountADV  = UTIL_TestUtil.generateAccount();
            accountADV.Type = AccountForecastUtil.ADVERTISER_ACCOUNT;
            accountADV.name += 'Adv'+i;
            accountADV.sic='111' + i;
            advAccountList.add(accountADV);
            
        }*/
                       
        for(Integer i = 6; i< 10; i++) {

            Account accountAgency = UTIL_TestUtil.generateAccount();
            accountAgency.Type = AccountForecastUtil.AGENCY_ACCOUNT;
            accountAgency.name += 'Agency'+i;
            accountAgency.naics_code__c='222' + i;
            agencyAccountList.add(accountAgency);
            
        }
        List<lead> lstLeads = new list<lead>();
        lead ld = new lead(firstname='Lead 1',lastname='lead 1',company='Adv1',phone='111-111-1111',infer3__Score_Snapshot__c=null,email='email@gmail.com');
        


        insert verticalCategorList;
        insert advAccountList;
        insert agencyAccountList;  
        insert ld;     
    }
    /*public static testmethod void LeadSICUpdateTest() {

        List<Account> advAccountList = new List<Account>();
        List<Account> agencyAccountList = new List<Account>();
        List<Account_Forecast__c> acctForecastList = 
                            new List<Account_Forecast__c>();

        String query = 'SELECT Id,name FROM Account WHERE Type = :';
        String advertiser = AccountForecastUtil.ADVERTISER_ACCOUNT;
        String agency = AccountForecastUtil.AGENCY_ACCOUNT;
        String advQuery = query+'advertiser';
        String agencyQuery = query+'agency';
        
        for(Account advAccount : Database.query(advQuery)){
            advAccountList.add(advAccount);
        }
        for(Account agencyAccount : Database.query(agencyQuery)){
            agencyAccountList.add(agencyAccount);
        }
        lead ld = [select id,sic_code__c,infer3__Score_Snapshot__c from lead];
        ld.sic_code__c='1111';
        ld.infer3__Score_Snapshot__c = 22;
        Test.startTest();
        update ld;
        
        Test.stopTest();
        
        
        System.assertEquals(ld.infer3__Score_Snapshot__c,22,
                    'infer3__Score_Snapshot__c Updated');
        System.assertEquals(ld.sic_code__c,'1111',
                    'Sic Code Updated');
    }*/

    public static testmethod void LeadNAICSUpdateTest() {

        List<Account> advAccountList = new List<Account>();
        List<Account> agencyAccountList = new List<Account>();
        
        String query = 'SELECT Id,name FROM Account WHERE Type = :';
        String advertiser = AccountForecastUtil.ADVERTISER_ACCOUNT;
        String agency = AccountForecastUtil.AGENCY_ACCOUNT;
        String advQuery = query+'advertiser';
        String agencyQuery = query+'agency';
        
        for(Account advAccount : Database.query(advQuery)){
            advAccountList.add(advAccount);
        }
        for(Account agencyAccount : Database.query(agencyQuery)){
            agencyAccountList.add(agencyAccount);
        }
        lead ld = [select id,Naics_code__c,infer3__Score_Snapshot__c from lead];
        ld.Naics_code__c='2226';
        ld.infer3__Score_Snapshot__c = 22;
        Test.startTest();
        update ld;
        
        Test.stopTest();
        
        
        System.assertEquals(ld.infer3__Score_Snapshot__c,22,
                    'infer3__Score_Snapshot__c Updated');
        System.assertEquals(ld.Naics_code__c,'2226',
                    'Sic Code Updated');
    }

    public static testmethod void ContactCreationTest() {
        List<Account> advAccountList = new List<Account>();
        List<Account> agencyAccountList = new List<Account>();
        
        String query = 'SELECT Id,name,Sic,Naics_code__c FROM Account WHERE Type = :';
        String advertiser = AccountForecastUtil.ADVERTISER_ACCOUNT;
        String agency = AccountForecastUtil.AGENCY_ACCOUNT;
        String advQuery = query+'advertiser';
        String agencyQuery = query+'agency';
        
        for(Account advAccount : Database.query(advQuery)){
            advAccountList.add(advAccount);
        }
        for(Account agencyAccount : Database.query(agencyQuery)){
            agencyAccountList.add(agencyAccount);
        }
        system.debug('149' + advAccountList);
        system.debug('149' + agencyAccountList);
        List<contact> lstContacts = new list<contact>();
        Contact c = new Contact();
        c.FirstName='Contact ';
        c.LastName='Contact ';
        c.account = advAccountList[0];
        c.phone='1231231212';
        c.email='email@email.com';
        c.Title='Title';
        
        Contact c1 = new Contact();
        c1.FirstName='Contact 1';
        c1.LastName='Contact 1';
        c1.account = agencyAccountList[0];
        c1.phone='1231231212';
        c1.email='email@email.com';
        c1.Title='Title';
        lstContacts.add(c);
        lstContacts.add(c1);

        Test.startTest();
        insert lstContacts;
        Test.stopTest();
        
    }


    public static testmethod void AccountUpdateTest() {
        List<Account> advAccountList = new List<Account>();
        List<Account> agencyAccountList = new List<Account>();
        
        String query = 'SELECT Id,name,Sic,Naics_code__c FROM Account WHERE Type = :';
        String advertiser = AccountForecastUtil.ADVERTISER_ACCOUNT;
        String agency = AccountForecastUtil.AGENCY_ACCOUNT;
        String advQuery = query+'advertiser';
        String agencyQuery = query+'agency';
        
        for(Account advAccount : Database.query(advQuery)){
            advAccountList.add(advAccount);
        }
        for(Account agencyAccount : Database.query(agencyQuery)){
            agencyAccountList.add(agencyAccount);
        }
        List<contact> lstContacts = new list<contact>();
        Contact c = new Contact();
        c.FirstName='Contact ';
        c.LastName='Contact ';
        c.account = advAccountList[0];
        c.phone='1231231212';
        c.email='email@email.com';
        c.Title='Title';
        
        Contact c1 = new Contact();
        c1.FirstName='Contact 1';
        c1.LastName='Contact 1';
        c1.account = agencyAccountList[0];
        c1.phone='1231231212';
        c1.email='email@email.com';
        c1.Title='Title';
        lstContacts.add(c);
        lstContacts.add(c1);

        Test.startTest();
        insert lstContacts;
        Test.stopTest();


        /*for(Account advAccount : advAccountList){

            advAccount.sic = '1111';
        }*/
        for(Account agencyAccount : agencyAccountList){
            agencyAccount.Naics_code__c = '2226';
        }
        update advAccountList;
        update agencyAccountList;

    }


    /*public static testmethod void LeadSICUpdateWithNotSetVCTest() {

        List<Account> advAccountList = new List<Account>();
        List<Account> agencyAccountList = new List<Account>();
        List<Account_Forecast__c> acctForecastList = 
                            new List<Account_Forecast__c>();

        String query = 'SELECT Id,name FROM Account WHERE Type = :';
        String advertiser = AccountForecastUtil.ADVERTISER_ACCOUNT;
        String agency = AccountForecastUtil.AGENCY_ACCOUNT;
        String advQuery = query+'advertiser';
        String agencyQuery = query+'agency';

        List<Vertical_Category_Mapping__c> verticalCategorList = [SELECT ID FROM Vertical_Category_Mapping__c];
        Delete verticalCategorList;
        
        for(Account advAccount : Database.query(advQuery)){
            advAccountList.add(advAccount);
        }
        for(Account agencyAccount : Database.query(agencyQuery)){
            agencyAccountList.add(agencyAccount);
        }
        lead ld = [select id,sic_code__c,infer3__Score_Snapshot__c from lead];
        ld.sic_code__c='1111';
        ld.infer3__Score_Snapshot__c = 22;
        Test.startTest();
        update ld;
        
        Test.stopTest();
        
        
        System.assertEquals(ld.infer3__Score_Snapshot__c,22,
                    'infer3__Score_Snapshot__c Updated');
        System.assertEquals(ld.sic_code__c,'1111',
                    'Sic Code Updated');
    }*/

    public static testmethod void LeadNAICSUpdateWithNotSetVCTest() {

        List<Account> advAccountList = new List<Account>();
        List<Account> agencyAccountList = new List<Account>();
        
        String query = 'SELECT Id,name FROM Account WHERE Type = :';
        String advertiser = AccountForecastUtil.ADVERTISER_ACCOUNT;
        String agency = AccountForecastUtil.AGENCY_ACCOUNT;
        String advQuery = query+'advertiser';
        String agencyQuery = query+'agency';
        List<Vertical_Category_Mapping__c> verticalCategorList = [SELECT ID FROM Vertical_Category_Mapping__c];
        Delete verticalCategorList;

        for(Account advAccount : Database.query(advQuery)){
            advAccountList.add(advAccount);
        }
        for(Account agencyAccount : Database.query(agencyQuery)){
            agencyAccountList.add(agencyAccount);
        }
        lead ld = [select id,Naics_code__c,infer3__Score_Snapshot__c from lead];
        ld.Naics_code__c='2226';
        ld.infer3__Score_Snapshot__c = 22;
        Test.startTest();
        update ld;
        
        Test.stopTest();
        
        
        System.assertEquals(ld.infer3__Score_Snapshot__c,22,
                    'infer3__Score_Snapshot__c Updated');
        System.assertEquals(ld.Naics_code__c,'2226',
                    'Sic Code Updated');
    }
    
    /*ORIGINALLY COMMENTED EVEN BEFORE NEW REQ ON SIC AND NAICS
    public static testmethod void ContactCreationTest() {

        List<Account> advAccountList = new List<Account>();
        List<Account> agencyAccountList = new List<Account>();
        List<Account_Forecast__c> acctForecastList = 
                            new List<Account_Forecast__c>();

        String query = 'SELECT Id FROM Account WHERE Type = :';
        String advertiser = AccountForecastUtil.ADVERTISER_ACCOUNT;
        String agency = AccountForecastUtil.AGENCY_ACCOUNT;
        String advQuery = query+'advertiser';
        String agencyQuery = query+'agency';
        
        for(Account advAccount : Database.query(advQuery)){
            advAccountList.add(advAccount);
        }
        for(Account agencyAccount : Database.query(agencyQuery)){
            agencyAccountList.add(agencyAccount);
        }
        // create Account Forecast records using the advertiser and agency
        // records created above
        for(Integer i=0; i<advAccountList.size(); i++) {
            
            Account_Forecast__c acctForecast = new Account_Forecast__c();
            acctForecast.External_ID__c = 'test'+i;
            acctForecast.Advertiser__c = advAccountList[i].Id;
            acctForecast.Agency__c = agencyAccountList[i].Id;
            acctForecastList.add(acctForecast);
        }
        Test.startTest();
        insert acctForecastList;
        Test.stopTest();
        String portfolio = AccountForecastUtil.PROTFOLIO_ACCOUNT;
        String acfCountQuery = 'SELECT Id'
                        +' FROM Account_Forecast__c'
                        +' WHERE Advertiser__c != null'
                        +' AND Agency__c != null'
                        +' AND Advertiser__r.Classification__c =:portfolio'
                        +' AND Agency__r.Classification__c =: portfolio';
        //Integer accountClasficationCount = database.countQuery(acfCountQuery);
        list<Account_Forecast__c> accountList = database.query(acfCountQuery);
        System.assertEquals(accountList.size(),acctForecastList.size(),
                    'Classification__c is not updated on all accountforecast records.');
    }*/
}