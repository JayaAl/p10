public with sharing class VerticalCategoryUtil
{

    
    public static Map<id,account> getAccounts(list<sObject> lstSObjects) {

        set<Id> accountId = new set<id>();
        Map<id,account> mapAccounts = new map<id,account>();
        for(sObject obj:lstSObjects)
        {
            accountId.add((id)obj.get('accountId'));
        }
        for(account a:[select id,sic,NAICS_Code__c from account where id in:accountId])
        {
            mapAccounts.put(a.id,a);
        }
        return mapAccounts;
    }
    
    
    public static Map<string,Vertical_Category_Mapping__c> getVerticalCategoryMappings(set<string> naicsCodeMatched)
    {
        Map<string,Vertical_Category_Mapping__c> mapVCM = new Map<string,Vertical_Category_Mapping__c>();
        if(naicsCodeMatched.size()>0){
            for(Vertical_Category_Mapping__c vcm: [select id, Industry_category__c,NAICS_Code__c from Vertical_Category_Mapping__c where NAICS_Code__c in: naicsCodeMatched])
            {
                mapVCM.put(vcm.NAICS_Code__c,vcm);
            }
        }
        
        return mapVCM;
    
    }

    /*public static Map<string,Vertical_Category_Mapping__c> getVerticalCategoryMappings(set<string> sicCodeMatched, set<string> naicsCodeMatched)
    {
        Map<string,Vertical_Category_Mapping__c> mapVCM = new Map<string,Vertical_Category_Mapping__c>();
        if(sicCodeMatched.size()>0){
            for(Vertical_Category_Mapping__c vcm: [select id, Industry_category__c,SIC_Code__c from Vertical_Category_Mapping__c where SIC_Code__c in: sicCodeMatched])
            {
                mapVCM.put(vcm.SIC_Code__c,vcm);
            }
        }
        else if(naicsCodeMatched.size()>0){
            for(Vertical_Category_Mapping__c vcm: [select id, Industry_category__c,NAICS_Code__c from Vertical_Category_Mapping__c where NAICS_Code__c in: naicsCodeMatched])
            {
                mapVCM.put(vcm.NAICS_Code__c,vcm);
            }
        }
        
        return mapVCM;
    
    }*/


}