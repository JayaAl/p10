public with sharing class ViewInvoiceScriptsExtension {

	public final ERP_Invoice__c theInvoice{get;set;}
    public final List<ScriptUtils.ScriptItem> listScriptItem{get;set;}
    public final Map<ERP_Invoice__c,List<ScriptUtils.ScriptItem>> mapInvoiceToScripts{get;set;} 


    public ViewInvoiceScriptsExtension(ApexPages.StandardController stdController) {
        parseURLParams();
        
        /* Single record via Id uses the StandardController */
        if(System.currentPageReference().getParameters().get('Id')!=null && System.currentPageReference().getParameters().get('Id')!=''){
            System.debug('Single Invoice Detected');
            if (!Test.isRunningTest()) {
                stdController.addFields(
                    new List<String>{ 'Name',  'Id',  'Conga_Billing_Period_End__c',  'Conga_Billing_Period_Start__c',  'Conga_Notarization_County__c',  'Conga_Notarization_State__c',  'Conga_Notarization_User__c', 'Advertiser__c', 'Billing_Period__c', 'Credit_Note_Invoice_Number__c', 'Opportunity_Id__c', 'Script_Length__c', 'ERP_Length__c', 'Billable_Impression__c', 'Gross_Invoice_Amount_Total__c', 'ERP_Org_Id__c'}
                ); // adding fields to the controller scope so they can be direclty referenced
            }
            this.theInvoice = (ERP_Invoice__c)stdController.getRecord();

            Map<Id, Map<String, Date>> mapOpportunityToDates = new Map<Id, Map<String, Date>>();
            mapOpportunityToDates.put(
                theInvoice.Opportunity_Id__c, 
                new Map<String, Date>{
                    'start'=>theInvoice.Conga_Billing_Period_Start__c,
                    'end'=>theInvoice.Conga_Billing_Period_End__c
            	}
            );
            listScriptItem = ScriptUtils.queryScriptsForOpptyDateMap(mapOpportunityToDates);

            mapInvoiceToScripts = new Map<ERP_Invoice__c,List<ScriptUtils.ScriptItem>>();
            mapInvoiceToScripts.put(theInvoice, listScriptItem);
            
        } else {
            /* Else attempt to gather URL Parameters */
            System.debug('Multiple Invoices Detected');
            mapInvoiceToScripts = new Map<ERP_Invoice__c,List<ScriptUtils.ScriptItem>>();
            for(ERP_Invoice__c i: queryInvoices(listInvoiceIds, listInvoiceNames)){
                // BAD PRACTICE, we shold never loop Queries.
                Map<Id, Map<String, Date>> mapOpportunityToDates = new Map<Id, Map<String, Date>>();
                mapOpportunityToDates.put(
                    i.Opportunity_Id__c, 
                    new Map<String, Date>{
                        'start'=>i.Conga_Billing_Period_Start__c,
                        'end'=>i.Conga_Billing_Period_End__c
                	}
                );
                List<ScriptUtils.ScriptItem> listSI = ScriptUtils.queryScriptsForOpptyDateMap(mapOpportunityToDates);
                if(!listSI.isEmpty()){
                    mapInvoiceToScripts.put(i, listSI);
                }
            }
        }
    }

    public List<ERP_Invoice__c> queryInvoices(List<String> listIds, List<String> listNames){
        
        if(
            (listIds==null || listIds.isEmpty()) 
            && (listNames==null || listNames.isEmpty())
        ){
            return null; // no parameters supplied, return null
        }

        Set<String> setIds = new Set<String>();
        if(listIds!=null && !listIds.isEmpty()){
            setIds.addAll(listIds);
        }
        system.debug('setIds: ' + setIds);

        Set<String> setNames = new Set<String>();
        if(listNames!=null && !listNames.isEmpty()){
            setNames.addAll(listNames);
        }
        system.debug('setNames: ' + setNames);

        String queryString = ' SELECT Name, Id, Conga_Billing_Period_End__c, Conga_Billing_Period_Start__c, Conga_Notarization_County__c, '
                           + '     Conga_Notarization_State__c, Conga_Notarization_User__c, Advertiser__c, Billing_Period__c, '
                           + '     Credit_Note_Invoice_Number__c, Opportunity_Id__c, Script_Length__c, ERP_Length__c, '
                           + '     Billable_Impression__c, Gross_Invoice_Amount_Total__c, ERP_Org_Id__c '
                           + ' FROM ERP_Invoice__c ';

        if(!setIds.isEmpty()){
            queryString += ' WHERE Id in : setIds ';
        }

        if(!setIds.isEmpty() && !setNames.isEmpty()){
            queryString += ' AND Name in : setNames ';
        } else if(!setNames.isEmpty()){
            queryString += ' WHERE Name in : setNames ';
        }
        
        system.debug('Query: ' + queryString);

        return Database.query(queryString);
    }

    private static List<String> listInvoiceIds;
    private static List<String> listInvoiceNames;
    private static Boolean savePDF;
    private static void parseURLParams(){
        listInvoiceIds = new List<String>();
        listInvoiceNames = new List<String>();
        savePDF = false;
        
        Map<String,String> urlParams = System.currentPageReference().getParameters();
        if(urlParams.containsKey('Id')){
            listInvoiceIds.addAll(ScriptUtils.spltString(urlParams.get('Id')));
        }
        if(urlParams.containsKey('recId')){
            listInvoiceIds.addAll(ScriptUtils.spltString(urlParams.get('recId')));
        }
        if(urlParams.containsKey('recName')){
            listInvoiceNames.addAll(ScriptUtils.spltString(urlParams.get('recName')));
        }
    }
}