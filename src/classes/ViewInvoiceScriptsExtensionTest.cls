@isTest
public class ViewInvoiceScriptsExtensionTest {

/* Prep testing record data */    
    private static ScriptUtilsTest testUtil;
    private static void prepTests(){
        ScriptUtilsTest.prepTests();
    }

/* Perform individual tests */
    private static testmethod void testLoadPageSingleInvoiceId(){
        prepTests();
        test.startTest();
            Test.setCurrentPage(Page.ViewInvoiceScriptsWrapper);
            ApexPages.currentPage().getParameters().put('id', ScriptUtilsTest.testInvoice.Id);
            ApexPages.StandardController sc = new ApexPages.StandardController(ScriptUtilsTest.testInvoice);
            ViewInvoiceScriptsExtension con = new ViewInvoiceScriptsExtension(sc);
        test.stopTest();
    }
    private static testmethod void testLoadPageSingleInvoiceRecId(){
        prepTests();
        test.startTest();
            Test.setCurrentPage(Page.ViewInvoiceScriptsWrapper);
            ApexPages.currentPage().getParameters().put('recId', ScriptUtilsTest.testInvoice.Id);
            ApexPages.StandardController sc = new ApexPages.StandardController(ScriptUtilsTest.testInvoice);
            ViewInvoiceScriptsExtension con = new ViewInvoiceScriptsExtension(sc);
        test.stopTest();
    }
    private static testmethod void testLoadPageSingleInvoiceRecName(){
        prepTests();
        test.startTest();
            Test.setCurrentPage(Page.ViewInvoiceScriptsWrapper);
            ApexPages.currentPage().getParameters().put('recName', ScriptUtilsTest.testInvoice.Name);
            ApexPages.StandardController sc = new ApexPages.StandardController(ScriptUtilsTest.testInvoice);
            ViewInvoiceScriptsExtension con = new ViewInvoiceScriptsExtension(sc);
        test.stopTest();
    }
}