public class ViewInvoiceScriptsWrapperExtension {
    private static Map<String,String> urlParams;
    public static String pdfURLString{get;set;}
    
    // default Constructor
    public ViewInvoiceScriptsWrapperExtension(){
        // get the current URL parameters, feed into a Map for later reference
        urlParams = System.currentPageReference().getParameters();
        
        // Create the preview URL
        Pagereference pdfURL = Page.ViewInvoiceScripts;
        for(String key:urlParams.keySet()){
            pdfURL.getParameters().put(key,urlParams.get(key));
        }
        pdfURLString = pdfURL.getUrl();
        
        // Automaticaly save PDF Output
        // savePDFOutput();
    }
    
    public static void savePDFOutput(){
        // get URLParams Id or recId
        urlParams = System.currentPageReference().getParameters();
        // populate a Set of Ids
        Set<Id> setRecordIds = new Set<Id>();
        if(urlParams.containsKey('Id')){
            setRecordIds.add(urlParams.get('Id'));
        }
        if(urlParams.containsKey('recId')){
            for(String s:ScriptUtils.spltString(urlParams.get('recId'))){
                setRecordIds.add((Id)s);
            }
        }
        // push to ScriptUtils.savePDFToInvoice
        ScriptUtils.savePDFToInvoice(setRecordIds);
    }
    
    
    
    
    
}