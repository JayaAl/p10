@isTest
public class ViewInvoiceScriptsWrapperExtensionTest {

/* Prep testing record data */    
    private static ScriptUtilsTest testUtil;
    private static void prepTests(){
        ScriptUtilsTest.prepTests();
    }

/* Perform individual tests */
    private static testmethod void testEmptyConstructor(){
        test.startTest();
        ViewInvoiceScriptsWrapperExtension con = new ViewInvoiceScriptsWrapperExtension();
        test.stopTest();
    }

    private static testmethod void testLoadPage(){
        prepTests();
        test.startTest();
        Test.setCurrentPage(Page.ViewInvoiceScriptsWrapper); // set current pagereference
        ApexPages.currentPage().getParameters().put('id', ScriptUtilsTest.listTestScripts[0].Id);
        ApexPages.currentPage().getParameters().put('recId', ScriptUtilsTest.listTestScripts[2].Id);
        ViewInvoiceScriptsWrapperExtension con = new ViewInvoiceScriptsWrapperExtension();
        test.stopTest();

        system.assertNotEquals(null, ViewInvoiceScriptsWrapperExtension.pdfURLString); // confirm that pdfURLString has been populated
    }
    
    private static testmethod void testSavePDFs(){
        prepTests();
        test.startTest();
        Test.setCurrentPage(Page.ViewInvoiceScriptsWrapper); // set current pagereference
        ApexPages.currentPage().getParameters().put('id', ScriptUtilsTest.listTestScripts[0].Id); // add Oppty Id to the pagereference parameters
        ApexPages.currentPage().getParameters().put('recId', ScriptUtilsTest.listTestScripts[2].Id);
        ViewInvoiceScriptsWrapperExtension.savePDFOutput();
        test.stopTest();

        List<Attachment> theList = [Select Id from Attachment];
        System.assert(theLIst.isEmpty()); // confirm that theList has contents
    }
}