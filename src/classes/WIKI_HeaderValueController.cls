public class WIKI_HeaderValueController {
    
    public Integer addMonth{get; set; }
    public Date startDate{get; set; }
    
    public WIKI_HeaderValueController() {
        
    }
    
    public Date getMonth(){
        Date monthDate = startDate.addMonths(addMonth);
        return monthDate;
    }
}