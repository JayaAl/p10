public class WIKI_StatusBarController {
    
    public Integer addMonth{get; set; }
    public Date startDate{get; set; }
    public Story__c storyRecord{get; set; }
    
    
    public WIKI_StatusBarController () {
        
    }
    
    public String getStatus(){
        Date monthDate = startDate.addMonths(addMonth);
        Date storyDate = storyRecord.Estimated_Completion_Date__c;
        String storyStatus;
        if(startDate != null && storyDate != null && monthDate.month() == storyDate.month() && storyRecord.Status__c != null){
            storyStatus = storyRecord.Status__c;
        
        }//else if(storyDate == null){
            //storyStatus = storyRecord.Status__c;
        
        //}
        else{
            storyStatus = '';
        
        }
        
        return storyStatus;
    }
    
    public Boolean getShowStatusValue(){
        Boolean showStatusValue;
        Date monthDate = startDate.addMonths(addMonth);
        Date storyDate = storyRecord.Estimated_Completion_Date__c;
        
        if(startDate != null && storyDate != null && monthDate.month() == storyDate.month() && storyRecord.Status__c != null){
            if(storyRecord.Status__c != null 
                && (!storyRecord.Status__c.equals('Red')
                    && !storyRecord.Status__c.equals('Green')
                    && !storyRecord.Status__c.equals('Yellow'))){
                showStatusValue = true;  
            }else{
                showStatusValue = false;  
            }
        }else{
            showStatusValue = false;  
        }
        
        return showStatusValue;
    }
}