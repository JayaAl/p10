/***********************************************
    Class: WIKI_StoriesStatusController
    This class is used to get the records details 
    for Story object to display them on VF Page.
    Author – Cleartask
    Date – 10/6/2011   
    Revision History    
 ***********************************************/
public class WIKI_StoriesStatusController{
    
    /* list to get all the records of related Story of Epic Story */
    public List<Story__c> storyRelatedList {get; set;}
    public static final String ON_HOLD = 'On Hold';
    public static final String CLOSED = 'Closed-Cancelled';
    /* to store the values of Story record detail */
    public Story__c storyRecord {get; set;}
    public Boolean showTable {get; set;}
    public Boolean showColorBar {get; set;}
    public String startMonth {get; set;}
    public String startYear {get; set;}
    public String selectedUnit {get; set;}
    public Date startDate {get; set;}
    public Date endDate;
    public String selectedNum {get; set;}
    public Integer numOfMonths {get; set;}
    public UTIL_Sorter wikiUTIL_Sorter {get; set;}
    
    private String defaultSortColumn = 'Estimated_Completion_Date__c';
    private String sortDirection = 'ASC';
    public String recType = 'Epic Story';
    public String query = 'Select Source_Partner__c, Estimated_Completion_Date__c, Production_deployment_Date__c, Priority__c, Percent_Complete__c, Owner.Alias, OwnerId, '+
        'Name, Sum_of_Level_of_Effort__c, Status__c, Epic_Story__c, Epic_Story__r.Name, Sprint__r.Name  From Story__c';
    
    public Integer MAX_MONTHS = 6;
    public Integer DEFAULT_MONTHS = 6;
    
    /* --setCon as ApexPages.StandardSetController --*/
    public ApexPages.StandardSetController setCon{
        get {
            if(setCon == null) {
                String stories = query + ' where ((Estimated_Completion_Date__c >= :startDate and Estimated_Completion_Date__c <= :endDate) or Estimated_Completion_Date__c = null) and RecordType.Name = :recType and (Status__c != :ON_HOLD and Status__c != :CLOSED) Order By ' + wikiUTIL_Sorter.getColumn() + ' ' + wikiUTIL_Sorter.getSortDirection();
                setCon = new ApexPages.StandardSetController(Database.getQueryLocator(stories));
            }
            setCon.setPageSize(400);
            return setCon;
        }
        
        set;
    }
    
    
    /* constructor */
    public WIKI_StoriesStatusController() {
        
        if(!paramValues()){
            startMonth = String.valueOf(System.now().month());
            startYear = String.valueOf(System.now().year());
            selectedNum = String.valueOf(DEFAULT_MONTHS);
        }
        startDate = date.newInstance(Integer.valueOf(startYear), Integer.valueOf(startMonth), 1);
        endDate = startDate.addMonths(Integer.valueOf(selectedNum));
        numOfMonths = startDate.monthsBetween(endDate);
        showTable = true;
        wikiUTIL_Sorter = new UTIL_Sorter(defaultSortColumn, UTIL_Sorter.SORT_ASC);
        filter();
    }
    
    public List<Story__c> getStoryList(){
        List<Story__c> strList = setCon.getRecords();
        if(strList != null && strList.size() > 0){
            showTable = true;
            return strList; 
        }else{
            showTable = false;
            return null;
        }     
    }
    
    
    public PageReference filter(){
        
        Date startDate = date.newInstance(Integer.valueOf(startYear), Integer.valueOf(startMonth), 1);
        Date endDate = startDate.addMonths(Integer.valueOf(selectedNum));
        
        numOfMonths = startDate.monthsBetween(endDate);
        this.startDate = startDate;
        this.endDate = endDate;
        
        if(selectedUnit == null || selectedUnit.equals('')){
            setCon = null;
            return null;
        }
        String searchStory = query + ' where ((Estimated_Completion_Date__c >= :startDate and Estimated_Completion_Date__c <= :endDate) or Estimated_Completion_Date__c = null) and Business_Unit__c includes (:selectedUnit) and RecordType.Name = :recType and (Status__c != :ON_HOLD and Status__c != :CLOSED) Order By ' + wikiUTIL_Sorter.getColumn() + ' ' + wikiUTIL_Sorter.getSortDirection(); // Order By Estimated_Completion_Date__c ';
        setCon = new ApexPages.StandardSetController(Database.getQueryLocator(searchStory));
        setCon.setPageSize(400);
        getstoryList();
        return null;
    }
    
    public PageReference sort(){
        setCon = null;
        return null;
    }
   
    /* get the detail of story */
    public void viewStory() {
        String selectedStory = ApexPages.currentPage().getParameters().get('id');
        storyRecord = storyRecordDetail(selectedStory);
    }
    
    /* get the detail of story */
    public void viewEpicStory() {
        String selectedStory = ApexPages.currentPage().getParameters().get('id');
        if(selectedStory != null){
            storyRecord = storyRecordDetail(selectedStory);
            Id storyId = storyRecord.id;
            wikiUTIL_Sorter = new UTIL_Sorter('Requested_Delivery_Date__c', UTIL_Sorter.SORT_ASC);
            String epicQuery = query + ' where Epic_Story__c = :storyId  and (Status__c != :ON_HOLD and Status__c != :CLOSED) Order By ' + wikiUTIL_Sorter.getColumn() + ' ' + wikiUTIL_Sorter.getSortDirection();
            setCon = new ApexPages.StandardSetController(Database.getQueryLocator(epicQuery));
            setCon.setPageSize(400);
            storyRelatedList = setCon.getRecords();
        }
    }
    
    public Integer[] getMonths() {
        List<Integer> months = new List<Integer>();
        for (Integer i = 0; i < this.numOfMonths; i++) {
            months.add(i);
        }
        return months;
    }
    
    /* query the detail of selected story record */
    public Story__c storyRecordDetail(String storyId) {
        Story__c storyDetail;
        if(storyId != null || storyId != ''){
            storyDetail = [Select s.V2MOM_Value__c, s.Type__c, s.SystemModstamp, s.Sum_of_Level_of_Effort__c, 
                s.Status__c, s.Sprint__c, s.Sprint__r.Name, s.Source_Partner__c, s.Size_Scope__c, s.Sandbox_Assigned__c, 
                s.Requester__c, s.Requester__r.Name, s.Requested_Delivery_Date__c, s.RecordTypeId, s.Production_deployment_Date__c, 
                s.Product_Owner__c, s.Product_Backlog__c, s.Product_Backlog__r.Name, s.Priority__c, s.Pricetag__c, 
                s.Percent_Complete__c, s.Owner.Alias, s.OwnerId, s.Next_Step__c, s.Next_Step_History__c, 
                s.Name, s.Milestone__c, s.LastModifiedDate, s.LastModifiedById, s.LastActivityDate, 
                s.IsDeleted, s.Id, s.Executive_Sponsor__c, s.Epic_Story__c, s.Description__c, s.Owner.Name, 
                s.CurrencyIsoCode, s.CreatedDate, s.CreatedById, s.Business_Owner__c, s.Business_Owner__r.Name,
                s.Business_Challenge__c, s.Estimated_Completion_Date__c, s.Acceptance_Criteria__c, s.Epic_Story__r.Name, RecordType.Name
                From Story__c s where id = :storyId];
            if(storyDetail.Status__c != null 
                && (storyDetail.Status__c.equals('Red')
                    || storyDetail.Status__c.equals('Yellow')
                    || storyDetail.Status__c.equals('Green'))){
                
                showColorBar = true;
            }else{
                showColorBar = false;
            }
        }
        return storyDetail;
    }
    
    public List<SelectOption> getSelectedMonthList() {
        List<SelectOption> monthList = new List<SelectOption>();  
        monthList.add(new SelectOption('1','January' ));
        monthList.add(new SelectOption('2','February'));
        monthList.add(new SelectOption('3','March'));
        monthList.add(new SelectOption('4','April'));
        monthList.add(new SelectOption('5','May'));
        monthList.add(new SelectOption('6','June'));
        monthList.add(new SelectOption('7','July'));
        monthList.add(new SelectOption('8','August'));
        monthList.add(new SelectOption('9','September'));
        monthList.add(new SelectOption('10','October'));
        monthList.add(new SelectOption('11','November'));
        monthList.add(new SelectOption('12','December'));
        return monthList;
   } 
    
    public List<SelectOption> getSelectedYearList() {      
        List<SelectOption> yearList = new List<SelectOption>();
        Date currentDateForYear = Date.today(); 
        Integer currentYear = currentDateForYear.year(); 
        yearList.add(new SelectOption(''+(currentYear-1),''+(currentYear-1)));
        yearList.add(new SelectOption(''+(currentYear),''+(currentYear)));
        yearList.add(new SelectOption(''+(currentYear+1),''+(currentYear+1)));
        yearList.add(new SelectOption(''+(currentYear+2),''+(currentYear+2))); 
        yearList.add(new SelectOption(''+(currentYear+3),''+(currentYear+3))); 
        return yearList;
    }
    
    public List<SelectOption> getNoOfMonths() {
        List<SelectOption> numberList = new List<SelectOption>();
        for (Integer i = 1; i <= MAX_MONTHS; i++) {
            numberList.add(new SelectOption(''+i, ''+i)); 
        }
        return numberList;
    }
    
    public List<SelectOption> getBusinessUnitList() {      
            List<SelectOption> options = new List<SelectOption>();
            Schema.DescribeFieldResult businessUnit = Story__c.Business_Unit__c.getDescribe(); 
            List<Schema.PicklistEntry> ple = businessUnit.getPicklistValues();
            options.add(new SelectOption('','---All---'));
            List<String> businessUnitList = new List<String>();
            for(Schema.PicklistEntry p : ple){
                businessUnitList.add(p.getValue());
                //options.add(new SelectOption(p.getValue(),p.getlabel()));
            }
            businessUnitList.sort();
            for(String bu :businessUnitList){
                options.add(new SelectOption(bu, bu));
            }
            return options;
      }
      
      public Boolean paramValues(){
          Map<String, String> params = ApexPages.currentPage().getParameters();
          if(params != null && params.containsKey('m')){
              startMonth = params.get('m');
              startYear = params.get('y');
              selectedNum = params.get('n');
              selectedUnit = params.get('u');
              return true;
          }else{
              return false;
          }
      }
      
}