/***********************************************
    Class: WIKI_Test_StoriesStatusController
    This test class is for WIKI_StoriesStatusController.
    Author – Cleartask
    Date – 10/22/2011   
    Revision History    
 ***********************************************/
@isTest
private class WIKI_Test_StoriesStatusController{
    
    static testMethod void testUnit(){
        
        WIKI_StoriesStatusController sContr = new WIKI_StoriesStatusController();
        
        List<RecordType> recType = [select id from RecordType where name = 'Epic Story' and sObjecttype = 'Story__c' limit 1];
        
        Story__c storyObj = new Story__c(
            Name = 'Test',
            RecordTypeId = recType[0].id
        );
        insert storyObj;
        
        Story__c epicStoryObj = new Story__c(
            Name = 'Test',
            Epic_Story__c = storyObj.id
            
        );
        insert epicStoryObj;
        
        ApexPages.currentPage().getParameters().put('id', storyObj.id);
        String storyid = storyObj.id;
        sContr.viewStory();
        sContr.viewEpicStory();
        sContr.filter();
        sContr.sort();
        sContr.getSelectedMonthList();
        sContr.getSelectedYearList();
        sContr.getStoryList();
        sContr.getMonths();
        sContr.getBusinessUnitList();
    }
    
    static testMethod void testHeaderValue(){
        
        WIKI_HeaderValueController sContr = new WIKI_HeaderValueController();
        sContr.startDate = System.today();
        sContr.addMonth = 1;
        sContr.getMonth();
        
   }
   
   static testMethod void testStatusBar(){
        List<RecordType> recType = [select id from RecordType where name = 'Epic Story' and sObjecttype = 'Story__c' limit 1];
        Story__c storyObj = new Story__c(
            Name = 'Test',
            RecordTypeId = recType[0].id
        );
        insert storyObj;
        WIKI_StatusBarController sContr = new WIKI_StatusBarController();
        sContr.startDate = System.today();
        sContr.addMonth = 1;
        
        sContr.storyRecord = storyObj;
        sContr.getStatus();
        sContr.getShowStatusValue();
   }     
}