/*
========================================================
Developer: Menaka Nanjappan <mnanjappan@pandora.com>
Created on : 12/2/2016
Trigger Name : AdExProductMapping
=========================================================
*/

@isTest(seeAllData=false)

private class adExProductMappingTrigger_Test {
    
    public static  List<CustomAdxProductMapping__c> customObjectList;
    public static List<Adx_Product_Mapping__c> customSettingsList;
    public static Adx_Product_Mapping__c customSettingsAdEx;
    
    /* This method is used to generate test data for the below methods*/
    static List<CustomAdxProductMapping__c> insertAdExObjectProducts(integer count, String Name){        
        List<CustomAdxProductMapping__c> customObjecProdList = new List<CustomAdxProductMapping__c>();
        Integer i = 0;
        while(i< count){
            CustomAdxProductMapping__c customObjecProd = new CustomAdxProductMapping__c();
            customObjecProd.Name = Name+i;
            customObjecProd.Ad_Unit_Size_Name__c= UTIL_TestUtil.generateRandomString(10);
            customObjecProd.Banner__c=UTIL_TestUtil.generateRandomString(10);
            customObjecProd.DFP_Ad_Unit_Id__c='667'+1;
            customObjecProd.Medium__c=UTIL_TestUtil.generateRandomString(10);
            customObjecProd.Offering_Type__c=UTIL_TestUtil.generateRandomString(10);
            customObjecProd.Platform__c=UTIL_TestUtil.generateRandomString(10);
            customObjecProd.SFDC_Products__c=UTIL_TestUtil.generateRandomString(10);
            customObjecProd.Size__c='300x250';
            customObjecProd.Sub_Platform__c=UTIL_TestUtil.generateRandomString(10);
            customObjecProdList.add(customObjecProd);
            i++;
        }
        return customObjecProdList;
    }
    
    @isTest  static  void insertUpdateSingleAdExProduct(){
        customObjectList = new  List<CustomAdxProductMapping__c> ();
        // 1= No of records, '4' - Name parameter
        customObjectList = insertAdExObjectProducts(1,'4');
        Test.startTest();
        insert customObjectList;
        Test.stopTest();       
        
        CustomAdxProductMapping__c customObjProd = [select id, Name,Size__c,Sub_Platform__c from CustomAdxProductMapping__c where id =:customObjectList[0].id limit 1];
        system.assertEquals('40', customObjProd.Name);
        system.assertEquals('300x250', customObjProd.Size__c);
        
        Adx_Product_Mapping__c customSettingsProd = [select id, Name,Medium__c from Adx_Product_Mapping__c where name =:customObjProd.Name limit 1];
        system.assertEquals('40', customSettingsProd.Name);
        
        // Update a single record
        customObjProd.Medium__c = 'Default Testing';
        update customObjProd;
        system.assertEquals('Default Testing', customObjProd.Medium__c);
        
        // Verify the updated record in 'Adx_Product_Mapping' custom settings
        Adx_Product_Mapping__c customSettingsProds = [select id, Name,Ad_Unit_Size_Name__c,Banner__c,DFP_Ad_Unit_Id__c,Medium__c,Offering_Type__c,Platform__c,SFDC_Products__c,Size__c,Sub_Platform__c from Adx_Product_Mapping__c where name =:customObjProd.Name limit 1];
        system.assertEquals('Default Testing', customSettingsProds.Medium__c);
    }
    
    @isTest  static void insertUpdateMultipleAdExProducts(){
        customObjectList = new  List<CustomAdxProductMapping__c> ();
        customObjectList = insertAdExObjectProducts(201, '7');
        
        Test.startTest();
        insert customObjectList;
        Test.stopTest();
        // Collect the id's of all the above inserted records        
        Set<Id> ids = new set<Id>();
        for(CustomAdxProductMapping__c  prod:customObjectList) {
            ids.add(prod.id);
        }
        
        List<CustomAdxProductMapping__c> customObjProd = [select id,Name,Banner__c from CustomAdxProductMapping__c where id IN :ids ];
        system.assertEquals(201,customObjProd.Size());
        
         // Update multile records
        customObjProd[0].Banner__c = 'Mobile Testing';
        customObjProd[1].Banner__c = 'Iphone Testing';
        update customObjProd;
        
        Set<String> nameList = new set<String>();        
        nameList.add(customObjProd[0].Name);
        nameList.add(customObjProd[1].Name);
        
         system.assertEquals('Iphone Testing', customObjProd[1].Banner__c);
         
        
    }
    
    
}