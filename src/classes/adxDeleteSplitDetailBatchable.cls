global class adxDeleteSplitDetailBatchable implements database.batchable<sObject>,database.stateful {
   public Set<Id> setOppId1;
   public Set<Id> setOppId2;
   
   
   public adxDeleteSplitDetailBatchable(Set<Id> setOppId)
   {
      setOppId1 = new Set<Id>();
      setOppId2 = new Set<Id>();
      List<ID> oppIdsList = new List<Id>(setOppId);
      integer MAX_SETSIZE=0;
      if(!String.isBlank( Label.OpportunitySetLimit ))
          MAX_SETSIZE = integer.valueof(Label.OpportunitySetLimit);
      
     
      if(setOppId.size()>MAX_SETSIZE)
      {
          for(integer i=0;i<MAX_SETSIZE;i++)
          {
              setOppId1.Add(oppIdsList[i]);
          }
          for(integer j=MAX_SETSIZE;j<oppIdsList.size();j++)
          {
              setOppId2.Add(oppIdsList[j]);
          }        
          
      }
      else
      {
          this.setOppId1  = setOppId;
      }
      
      system.debug('THIS IS THE TOTAL OPPTY SIZE' + setOppId.size());
      system.debug('THIS IS THE TOTAL OPPTY SIZE' + setOppId1.size());
      system.debug('THIS IS THE TOTAL OPPTY SIZE' + setOppId2.size());
       
   
      
   }
   global database.queryLocator start(database.BatchableContext bc) {
   
      string query = 'SELECT Id FROM Split_Detail__c WHERE Opportunity_Split__r.Opportunity__c in :setOppId1';
      return database.getQueryLocator(query);

   }
   global void execute(database.BatchableContext bc,List<sObject> scope) {
      
      
      try{ 
          if(scope.size() > 0){          
                delete scope;
                Database.emptyRecycleBin(scope);
                 
          }
        } catch(Exception e){
            system.debug(logginglevel.info,'This is the exception caused while Deleting splits details' + e + '---->' + scope);
            Logger.logMessage('adxDeleteSplitDetailBatchable','EXECUTE','Failed to DELETE Split Details - Err: '+e,'Split_DETAIL__C','','Error');        
        }
        
        
   }
   global void finish(database.BatchableContext bc) {
      if(test.isrunningtest()==false){
          //adxInsertSplitDetailsBatchable ib = new adxInsertSplitDetailsBatchable();
          
          if(setOppId2.size()>0)
          {
              adxDeleteSplitDetailBatchable delBatchRecursive = new adxDeleteSplitDetailBatchable(setOppId2);
              Database.executeBatch(delBatchRecursive,2000);
          }
          else
          {
              adxInsertSplitDetailsBatchable ib = new adxInsertSplitDetailsBatchable();
              Database.executeBatch(ib,2000);
          }
          
       }

   }
}