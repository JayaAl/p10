global class adxDeleteSplitDetailBatchable1 implements database.batchable<sObject> {
   public Set<Id> setOppId;
   
   
   
   public adxDeleteSplitDetailBatchable1(Set<Id> setOppId)
   {
   
      this.setOppId  = setOppId;
      
   }
   global database.queryLocator start(database.BatchableContext bc) {
   
      string query = 'SELECT Id FROM Split_Detail__c WHERE Opportunity_Split__r.Opportunity__c in :setOppId';
      return database.getQueryLocator(query);

   }
   global void execute(database.BatchableContext bc,List<sObject> scope) {
      
      
      try{ 
          if(scope.size() > 0){          
                delete scope;
                Database.emptyRecycleBin(scope);
                 
          }
        } catch(Exception e){
            system.debug(logginglevel.info,'This is the exception caused while Deleting splits details' + e + '---->' + scope);
            Logger.logMessage('adxDeleteSplitDetailBatchable','EXECUTE','Failed to DELETE Split Details - Err: '+e,'Split_DETAIL__C','','Error');        
        }
        
        
   }
   global void finish(database.BatchableContext bc) {
      if(test.isrunningtest()==false){
          adxInsertSplitDetailsBatchable ib = new adxInsertSplitDetailsBatchable();
          Database.executeBatch(ib,2000);
       }

   }
}