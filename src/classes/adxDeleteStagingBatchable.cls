global class adxDeleteStagingBatchable implements database.batchable<Sobject>, database.stateful {
   
   public Map<string,decimal> opptyAmount = new Map<string,decimal>();
   
   global Database.QueryLocator start(database.BatchableContext bc) {
   
      //List<sobject> lineItems = new List<sObject>();
      string query = 'select id from Split_Detail_Staging__c';
      
      
      /*return database.getQueryLocator([SELECT End_Date__c, ServiceDate, OpportunityId, TotalPrice, HasSchedule, HasRevenueSchedule, PricebookEntry.Product2Id, PricebookEntry.Name, CurrencyIsoCode,
                Banner__c, Banner_Type__c, Cost_Type__c, Medium__c, Offering_Type__c, Platform__c, Size__c, Sub_Platform__c, Takeover__c, 
                (SELECT Revenue, ScheduleDate FROM OpportunityLineItemSchedules)
            FROM OpportunityLineItem
            WHERE OpportunityId IN :setOppId]).getQuery();
      */
      return database.getQueryLocator(query);

   }
   
   global void execute(database.BatchableContext bc,List<Sobject> scope) {
      
      List<Split_Detail_STAGING__c> staging = (List<Split_Detail_STAGING__c>) scope;
      delete staging;
        
   }
   global void finish(database.BatchableContext bc) {
      
      
        
   }
}