/**
* ─────────────────────────────────────────────────────────────────────────────────────────────────┐
* 
* ──────────────────────────────────────────────────────────────────────────────────────────────────
* @modifiedBy     Jaya Alaparthi
* @version        1.1
* @modified       2016-12-13
* @systemLayer    Batch class
* @see            
* ──────────────────────────────────────────────────────────────────────────────────────────────────
* @changes
* v1.1            Jaya Alaparthi
* 2016-12-13      Adding error logger in finish. this sends in email. 
* 
* ─────────────────────────────────────────────────────────────────────────────────────────────────┘
*/
global class adxInsertSplitDetailsBatchable implements database.batchable<sObject>,database.stateful {
   public Set<Id> setOppId;
   public class dmlOperationException extends exception
   {
   
   }
   public adxInsertSplitDetailsBatchable()
   {
      
   }
   global database.queryLocator start(database.BatchableContext bc) {
       
       string query = 'SELECT Id,Name,Product__c,CurrencyIsoCode,Banner__c,Salesperson_User__c, ';
       query += ' Banner_Type__c,Cost_Type__c,Medium__c,Offering_Type__c, ';
       query += ' Platform__c,Size__c,Sub_Platform__c,Takeover__c, ';
       query += ' Offering_Type_Medium__c,Opportunity_Split__c,Date__c,User__c,  ';
       query += ' Amount__c FROM Split_Detail_Staging__c ';
        
         return database.getQueryLocator(query);
       
   }
   global void execute(database.BatchableContext bc,List<sObject> scope) {
      
      
           if(scope.size() > 0){          
                List<Split_Detail__c> lstSplitDetails = new List<Split_Detail__c>();
                
                for(Split_Detail_Staging__c SDS: (List<Split_Detail_Staging__c>)scope)
                {
                    Split_Detail__c SD = new Split_Detail__c();
                    SD.Opportunity_Split__c = SDS.Opportunity_Split__c;
                    SD.Date__c = SDS.Date__c;
                    SD.Salesperson_User__c = SDS.User__c;
                    SD.Amount__c = SDS.Amount__c;
                    SD.Name                = SDS.Name;
                    SD.Product__c          = SDS.Product__c;
                    SD.CurrencyIsoCode     = SDS.CurrencyIsoCode;
                    SD.Banner__c           = SDS.Banner__c;
                    SD.Banner_Type__c      = SDS.Banner_Type__c;
                    SD.Cost_Type__c        = SDS.Cost_Type__c;
                    SD.Medium__c           = SDS.Medium__c;
                    SD.Offering_Type__c    = SDS.Offering_Type__c;
                    SD.Platform__c         = SDS.Platform__c;
                    SD.Size__c             = SDS.Size__c;
                    SD.Sub_Platform__c     = SDS.Sub_Platform__c;
                    SD.Takeover__c         = SDS.Takeover__c;
                    SD.Offering_Type_Medium__c = SDS.Offering_Type_Medium__c;
                    lstSplitDetails.add(SD);   
                    
                }
                try
                {
                    insert lstSplitDetails;
                    
                    //if insert is not a problem then delete the data from staging.
                    delete scope;
                    database.EmptyRecycleBin(scope);
                }
                catch(Exception e){
            
                    system.debug(logginglevel.info,'This is the exception caused while INSERTING splits details' + e + '---->' + scope);
                    Logger.logMessage(
                    'adxInsertSplitDetailBatchable',
                    'EXECUTE',
                    'Failed to Ins SplitDetails and delete Staging - Err: '+e,
                    'Split_DETAIL__C',
                    '',
                    'Error'
                    
                    );  
                  
                }
                
                //Delete the records from Split_Detail_Staging after inserting the same in to Split_Detail
                
                
            }
        
        
        
        
        
   }
   global void finish(database.BatchableContext bc) {
        
      //───────────────────────────────────────────────────────────────────────┐
      // v1.1 change query
      //───────────────────────────────────────────────────────────────────────┘
      AsyncApexJob aJob = [SELECT Id, 
                              Status,
                              NumberOfErrors,
                              JobItemsProcessed,
                              TotalJobItems,
                              CreatedBy.Email,
                              ExtendedStatus
                          FROM AsyncApexJob 
                          WHERE Id = :bc.getJobId()];
         Logger.logMessage('adxInsertSplitDetailsBatchable','SPLIT INSERT BATCH','BATCH CHAIN FOR UPSERTING Split Details is '+aJob.Status,
                          '',aJob.Id,'Message');   
        
   }
}