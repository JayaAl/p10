global class adxIntegrationBatchable implements database.batchable<Sobject>, database.stateful {
   public Set<Id> setOppId;
   public integer countSchedulesTotal{get;set;}
   public decimal amt = 0;
   
   public List<Split_Detail_STAGING__c> insertList = new List<Split_Detail_STAGING__c>();
   class adxIntegrationBatchableException extends Exception {}
   
   
   
   public Map<string,decimal> opptyAmount = new Map<string,decimal>();
   public adxIntegrationBatchable(Set<Id> setOppId)
   {
       countSchedulesTotal = 0;
      this.setOppId  = setOppId;
   }
   global Database.QueryLocator start(database.BatchableContext bc) {
   
      //List<sobject> lineItems = new List<sObject>();
      string query = 'SELECT Id, End_Date__c, ServiceDate, OpportunityId, TotalPrice, HasSchedule, HasRevenueSchedule, PricebookEntry.Product2Id, PricebookEntry.Name, CurrencyIsoCode, ';
             query += '  Banner__c, Banner_Type__c, Cost_Type__c, Medium__c, Offering_Type__c, Platform__c, Size__c, Sub_Platform__c, Takeover__c ';
             //query += '   (SELECT Revenue, ScheduleDate FROM OpportunityLineItemSchedules)';
            query += ' FROM OpportunityLineItem';
            query += ' WHERE OpportunityId IN :setOppId';
      
      system.debug('Query?' + query);
      /*return database.getQueryLocator([SELECT End_Date__c, ServiceDate, OpportunityId, TotalPrice, HasSchedule, HasRevenueSchedule, PricebookEntry.Product2Id, PricebookEntry.Name, CurrencyIsoCode,
                Banner__c, Banner_Type__c, Cost_Type__c, Medium__c, Offering_Type__c, Platform__c, Size__c, Sub_Platform__c, Takeover__c, 
                (SELECT Revenue, ScheduleDate FROM OpportunityLineItemSchedules)
            FROM OpportunityLineItem
            WHERE OpportunityId IN :setOppId]).getQuery();
      */
      return database.getQueryLocator(query);

   }
   
   global void execute(database.BatchableContext bc,List<Sobject> scope) {
      system.debug('SCOPE SIZE' + scope.size());
      system.debug(scope);
      decimal splitAmount=0;
      insertList = new List<Split_Detail_STAGING__c>();
      /*List<opportunityLineItem> lineItemList = [SELECT End_Date__c, ServiceDate, OpportunityId, TotalPrice, HasSchedule, HasRevenueSchedule, PricebookEntry.Product2Id, PricebookEntry.Name, CurrencyIsoCode,
                Banner__c, Banner_Type__c, Cost_Type__c, Medium__c, Offering_Type__c, Platform__c, Size__c, Sub_Platform__c, Takeover__c, 
                (SELECT Revenue, ScheduleDate FROM OpportunityLineItemSchedules)
            FROM OpportunityLineItem
            WHERE OpportunityId IN :scope];*/
      List<opportunityLineItem> lineItemList = (List<opportunityLineItem>) scope;
      
      Map<Id, List<OpportunityLineItemSchedule>> map1 = new Map<Id, List<OpportunityLineItemSchedule>> ();
      
      for ( OpportunityLineItemSchedule sched : [SELECT Revenue, ScheduleDate,OpportunityLineItemId FROM OpportunityLineItemSchedule WHERE 
                                                       OpportunityLineItemId IN : scope       
                                              ] ) {
          if (!map1.containsKey (sched.OpportunityLineItemId) ) {
              map1.put(sched.OpportunityLineItemId, new List<OpportunityLineItemSchedule>() );
          }
          map1.get(sched.OpportunityLineItemId).add(sched);
      }
      
      Map<Id,Map<Date,Decimal>> nestedMap = new Map<Id,Map<Date,Decimal>>();  
        
        // Map of Opportunity ID to a List of OLIs
        Map<Id,List<OpportunityLineItem>> oppLineItemMap = new Map<Id,List<OpportunityLineItem>>();
        
        // Loop through the OLIs, populating the above Maps
        for(OpportunityLineItem oppLineItem : lineItemList){
                system.debug('This is the oppLineItem '  + oppLineItem);
              system.debug('This is the oppLineItem id'  + oppLineItem.id);
            // Formulating a list of opportunity line items associated with each of the opportunity (to be used later).
            List<OpportunityLineItem> oppLineItemList = oppLineItemMap.get(oppLineItem.OpportunityId);
            if(oppLineItemList == null){
                oppLineItemList = new List<OpportunityLineItem>();
                oppLineItemMap.put(oppLineItem.OpportunityId,oppLineItemList);
            }
            oppLineItemList.add(oppLineItem);
            
            // Formulating the nested map.
            Map<Date,Decimal> productMap = nestedMap.get(oppLineItem.Id);
            if(productMap == null){
                productMap = new Map<Date,Decimal>();
                nestedMap.put(oppLineItem.Id,productMap);
            }
            
            // create or update productMap values based on Opportunity OLIs 
            if(oppLineItem.HasSchedule && map1.containsKey(oppLineItem.Id)){ // If the OLI has Product Schedules use them
                countSchedulesTotal += map1.get(oppLineItem.Id).size();
                for(OpportunityLineItemSchedule scheduleItem: map1.get(oppLineItem.Id)){
                    Date key = Date.newInstance(scheduleItem.ScheduleDate.year(), scheduleItem.ScheduleDate.month(), 1);
                    Decimal amount = productMap.get(key);
                    if(amount == null){
                        amount=0;
                    }   
                    amount = amount + scheduleItem.Revenue;
                    productMap.put(key,amount);
                    
                }
            } else if (oppLineItem.ServiceDate != null) { // Otherwise base the details off of the ServiceDate and End_Date__c of the OLI, splitting evenly between all months spanned.
                Integer months = oppLineItem.ServiceDate.monthsBetween(oppLineItem.End_Date__c);
                for(Integer i=0; i < (months+1);i++){
                    Date key = Date.newInstance(oppLineItem.ServiceDate.year(), oppLineItem.ServiceDate.month()+i, 1);
                    Decimal amount = productMap.get(key);
                    if(amount == null){
                        amount = 0;
                     }  
                     amount = amount + oppLineItem.TotalPrice/(months+1);    
                     productMap.put(key,amount);
                }
            } else {
                throw new adxIntegrationBatchableException('this should not have  happened');
            }
            
            //nestedMap.put(oppLineItem.Id,productMap);
        }
        
        system.debug(logginglevel.info,'This is the future call oppLineItemMap' + oppLineItemMap);
        system.debug(logginglevel.info,'This is the future call nestedMap' + nestedMap);
        
        List<Opportunity_Split__c> oppSplitList = [
            SELECT Split__c, Opportunity__c,Opportunity__r.Name, Opportunity__r.Probability, Probability__c, CurrencyIsoCode, Salesperson__c 
            FROM Opportunity_Split__c 
            WHERE Opportunity__c IN :setOppId //for update
        ];//8_6_2012, Added for update, To resolve incorrect split details creation issue
        
        system.debug(logginglevel.info,'This is the future call setOppId' + setOppId);
        system.debug(logginglevel.info,'This is the future call oppSplitList' + oppSplitList);
        
         
        // Creating a new set of split detail objects.
        // For each Opportunity Line Item and for each schedule date (month, year) combination a new split detail
        // object is created.
        //insertList = new List<Split_Detail__c>();
        
        for(Opportunity_Split__c oppSplit: oppSplitList){
            List<OpportunityLineItem> oppLineItemList = oppLineItemMap.get(oppSplit.Opportunity__c);
            if (oppLineItemList == null) oppLineItemList = new List<OpportunityLineItem>();
            for(OpportunityLineItem lineItem: oppLineItemList){
                Map<Date,Decimal> productMap = nestedMap.get(lineItem.Id);
                
                
                
                System.debug('productMap: ' + productMap);
                for(Date splitDate: productMap.keySet()){
                     /*Split_Detail__c splitDetail = SPLT_ManageSplitDetails.splitDetailFromOLI(lineItem);
                     splitDetail.Opportunity_Split__c = oppSplit.Id;
                     splitDetail.Date__c = splitDate;
                     splitDetail.Salesperson_User__c = oppSplit.Salesperson__c;//Updated by Lakshman on 16-4-2013 to update the Salesperson User for Hoopla
                     splitDetail.Amount__c = productMap.get(splitDate)*(oppSplit.Split__c/100);
                     */
                     //todo copy all from split detail to staging
                     Split_Detail_STAGING__c staging = splitDetailStagingFromOLI(lineItem);
                     staging.Opportunity_Split__c = oppSplit.Id;
                     staging.Date__c = splitDate;
                     staging.User__c = oppSplit.Salesperson__c;//Updated by Lakshman on 16-4-2013 to update the Salesperson User for Hoopla
                     staging.Amount__c = (productMap.get(splitDate)*(oppSplit.Split__c/100));
                     insertList.add(staging);   
                     //amt += staging.Amount__c;
                     
                     
                    if(!opptyAmount.containsKey(oppSplit.Opportunity__c + '-' + oppSplit.Opportunity__r.Name))
                    {
                        splitAmount = staging.Amount__c;
                    }
                    else
                    {
                        splitAmount = opptyAmount.get(oppSplit.Opportunity__c + '-' + oppSplit.Opportunity__r.Name) + staging.Amount__c;
                    }
                    opptyAmount.put(oppSplit.Opportunity__c + '-' + oppSplit.Opportunity__r.Name,splitAmount);
                    
                }
                
                
                
            }
            
        }
        
        insert insertList;
        //system.debug(logginglevel.info,'This is the future call insertList' + insertList);
        
        
        
   }
   private static Split_Detail_Staging__c splitDetailStagingFromOLI(OpportunityLineItem OLI){
        String fixedOffer = (OLI.Offering_Type__c==null||OLI.Offering_Type__c=='')?'[None]':OLI.Offering_Type__c;
        String fixedMedium = (OLI.Medium__c==null||OLI.Medium__c=='')?'[None]':OLI.Medium__c;
        Split_Detail_Staging__c splitDetail = new Split_Detail_Staging__c(
            Name                = OLI.PricebookEntry.Name,
            Product__c          = OLI.PricebookEntry.Product2Id,
            CurrencyIsoCode     = OLI.CurrencyIsoCode,
            Banner__c           = OLI.Banner__c,
            Banner_Type__c      = OLI.Banner_Type__c,
            Cost_Type__c        = OLI.Cost_Type__c,
            Medium__c           = OLI.Medium__c,
            Offering_Type__c    = OLI.Offering_Type__c,
            Platform__c         = OLI.Platform__c,
            Size__c             = OLI.Size__c,
            Sub_Platform__c     = OLI.Sub_Platform__c,
            Takeover__c         = OLI.Takeover__c,
            Offering_Type_Medium__c = fixedOffer+' / '+fixedMedium
            
        );
        return splitDetail;
    }

   global void finish(database.BatchableContext bc) {
      
      String listVal = '' +setOppId;
      
      system.debug('This is the total schedules for all the opptys '  + countSchedulesTotal);
      //system.debug('This is the total insertList '  + insertList.size());
      system.debug('This is the OpptyAmount '  + opptyAmount.size());
      system.debug('This is the OpptyAmount '  + opptyAmount);
      decimal amt = 0;
      decimal count = 0;
      
      set<Id> setOpptyIds = new set<id>();
      
      for(string s:OpptyAmount.Keyset())
      {
          setOpptyIds.add((id)s.split('-')[0]);
          
      }
      
      Map<Id,decimal> opportunityAmt = new Map<Id,decimal>();
      for(opportunity o:[select id,amount from opportunity where id in :setOpptyIds order by Name])
      {
          opportunityAmt.put(o.Id,o.Amount);
      }
      
      
      for(string s:OpptyAmount.Keyset())
      {
          count +=1;
          amt +=OpptyAmount.get(s);
          if((opportunityAmt.get(s.split('-')[0]) - OpptyAmount.get(s).setScale(2)).setScale(2) == 0)
          {
              system.debug('MATCHED PENNY TO PENNY- This is the Oppty Rounded-> ' + count + '. '  + s + 'OPPTY Amount = ' + OpptyAmount.get(s).setScale(2) + ' ZERO ');
          }
          else
          {
              system.debug('NOT MATCHED - This is the Oppty Rounded-> ' + count + '. '  + s + ' Amount = ' + OpptyAmount.get(s).setScale(2) + '---' + OpptyAmount.get(s) + '--Difference -->' + (opportunityAmt.get(s.split('-')[0]) - OpptyAmount.get(s).setScale(2)).setScale(2));
          }
          
      }
      
      
      
      
      system.debug('This is the total amt '  +amt + '---' + amt.setScale(2));
      system.debug('This is the setOpptyIds.size() '  + setOpptyIds.size());
      if(test.isrunningtest()==false){
          adxDeleteSplitDetailBatchable ds = new adxDeleteSplitDetailBatchable(setOpptyIds);
          Database.executeBatch(ds,2000);
          
         
      }

        //AsyncApexJob aaj = [SELECT Id, Status, JobItemsProcessed, TotalJobItems, NumberOfErrors 
        //            FROM AsyncApexJob WHERE ID =: batchprocessid ];

        
   }
}