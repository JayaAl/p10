global class adxLineItemsCreateSchedulesBatchable implements database.batchable<Sobject>,database.stateful {
   public List<opportunityLineItem> setOppId;
   public integer countSchedulesTotal{get;set;}
   
   public List<Split_Detail__c> insertList = new List<Split_Detail__c>();
   public adxLineItemsCreateSchedulesBatchable(List<opportunityLineItem> lstOpportunityLineItems)
   {
       countSchedulesTotal = 0; 
      this.setOppId  = lstOpportunityLineItems;
   }
   global Database.QueryLocator start(database.BatchableContext bc) {
   
      //List<sobject> lineItems = new List<sObject>();
      string query = 'SELECT Id, End_Date__c, ServiceDate, OpportunityId, TotalPrice, HasSchedule, HasRevenueSchedule, PricebookEntry.Product2Id, PricebookEntry.Name, CurrencyIsoCode, ';
             query += '  Banner__c, Banner_Type__c, Cost_Type__c, Medium__c, Offering_Type__c, Platform__c, Size__c, Sub_Platform__c, Takeover__c ';
             //query += '   (SELECT Revenue, ScheduleDate FROM OpportunityLineItemSchedules)';
            query += ' FROM OpportunityLineItem';
            query += ' WHERE OpportunityId IN :setOpptyLineItemsId';
      
      system.debug('Query?' + query);
      /*return database.getQueryLocator([SELECT End_Date__c, ServiceDate, OpportunityId, TotalPrice, HasSchedule, HasRevenueSchedule, PricebookEntry.Product2Id, PricebookEntry.Name, CurrencyIsoCode,
                Banner__c, Banner_Type__c, Cost_Type__c, Medium__c, Offering_Type__c, Platform__c, Size__c, Sub_Platform__c, Takeover__c, 
                (SELECT Revenue, ScheduleDate FROM OpportunityLineItemSchedules)
            FROM OpportunityLineItem
            WHERE OpportunityId IN :setOppId]).getQuery();
      */
      return database.getQueryLocator(query);

   }
   
   global void execute(database.BatchableContext bc,List<Sobject> scope) {
        
        
        
   }
   global void finish(database.BatchableContext bc) {
      
      String listVal = '' +setOppId;
      
      system.debug('This is the total schedules for all the opptys '  + countSchedulesTotal);
      system.debug('This is the total insertList '  + insertList.size());
      decimal amt = 0;
      for(Split_Detail__c sd: insertList)
      {
          amt +=sd.Amount__c;
      }
      system.debug('This is the total amt '  +amt);
      //adxDeleteScheduleBatchable ds = new adxDeleteScheduleBatchable(setOppId,insertList);
      //ID batchprocessid = 
      //Database.executeBatch(ds,200);

        //AsyncApexJob aaj = [SELECT Id, Status, JobItemsProcessed, TotalJobItems, NumberOfErrors 
        //            FROM AsyncApexJob WHERE ID =: batchprocessid ];

        
   }
}