global class adxOpportunitySchedulesMismatchBatch implements database.batchable<Sobject> {
   public Set<Id> setOppId = new Set<Id>();
   
   class adxIntegrationBatchableException extends Exception {}
   
   global Database.QueryLocator start(database.BatchableContext bc) {
       string stageName = 'Closed Lost';
       string currencyType ='AUD';
       //List<sobject> lineItems = new List<sObject>();
      
      ///  CHECK: if there is going to be any limit issues
      //// querylimit
      string query = 'SELECT Id ';
             query += 'FROM Opportunity ';
             query += 'WHERE  ';
             query += 'Amount != null ';
             //query += ' AND Id =\''+ String.escapeSingleQuotes(opid)+'\'';
             query += ' AND StageName !=\''+ String.escapeSingleQuotes(stageName)+'\'';
             query += ' AND CurrencyIsoCode !=\''+ String.escapeSingleQuotes(currencyType)+'\'';
             query += ' AND Contract_Start_Date_Formula__c != null ';
             query += ' AND CloseDate >= 2013-01-01 ';
             query += ' AND ID IN (Select Opportunity__c from Opportunity_Split__c WHERE Does_Split_Equal_Opp_Amount__c = false) ';
             query += ' ORDER BY Name';
      
      system.debug('Query?' + query);
     
      List<Opportunity> optList = database.query(query);
      for(Opportunity s: optList)
      {
          setOppId.add(s.Id);
      }
      system.debug('This is optylist' + setOppId.size());
      string querySchedules = 'select id from OpportunityLineItemSchedule where opportunitylineitem.opportunityid=:setOppId';
      
      return database.getQueryLocator(querySchedules);

   }
   
   global void execute(database.BatchableContext bc,List<Sobject> scope) {
       
       system.debug('SCOPE DUDE' + Scope);
       system.debug('SCOPE DUDE' + Scope.size());
       delete scope;//[select * from schedule where line item in : scope];
      
      
    } 

   global void finish(database.BatchableContext bc) {
      /*if(setOppId!=null && setOppId.SIZE()>0)
      {
        adxIntegrationBatchable IB = new adxIntegrationBatchable(setOppId);
        Database.executeBatch(IB,125);
      }
      
      if(setOppId!=null && setOppId.SIZE()>0)
      {
        adxIntegrationBatchable IB = new adxIntegrationBatchable(setOppId);
        Database.executeBatch(IB,125);
      }*/
      
      // CHECK: this might be ok for smaller dataset
      // Not a great way 
        //adxSchedulesCalculationBatch SC = new adxSchedulesCalculationBatch();
        //Database.executeBatch(SC,25);

        
   }
}