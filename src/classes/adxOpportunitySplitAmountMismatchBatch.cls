global class adxOpportunitySplitAmountMismatchBatch implements database.batchable<Sobject>, database.stateful {
   public Set<Id> setOppId = new Set<Id>();
   
   class adxIntegrationBatchableException extends Exception {}
   
   global Database.QueryLocator start(database.BatchableContext bc) {
       string stageName = 'Closed Lost';
       string currencyType ='AUD';
       Id opid = '0064000000jVXbb';//'0064000000fjmcLAAQ';
      //List<sobject> lineItems = new List<sObject>();
      string query = 'SELECT Id ';
             query += 'FROM Opportunity ';
             query += 'WHERE  ';
             query += 'Amount != null ';
             //query += ' AND Id =\''+ String.escapeSingleQuotes(opid)+'\'';
             query += ' AND StageName !=\''+ String.escapeSingleQuotes(stageName)+'\'';
             query += ' AND CurrencyIsoCode !=\''+ String.escapeSingleQuotes(currencyType)+'\'';
             query += ' AND Contract_Start_Date_Formula__c != null ';
             query += ' AND CloseDate >= 2013-01-01 ';
             query += ' AND ID IN (Select Opportunity__c from Opportunity_Split__c WHERE Does_Split_Equal_Opp_Amount__c = false) ';
             query += ' ORDER BY Name';
      
      system.debug('Query?' + query);
      
      return database.getQueryLocator(query);

   }
   
   global void execute(database.BatchableContext bc,List<Sobject> scope) {
      system.debug('SCOPE SIZE' + scope.size());
      List<Opportunity> opptys = (List<Opportunity>) scope;
      for(Opportunity s: opptys)
      {
          setOppId.add(s.Id);
      }
    }

   global void finish(database.BatchableContext bc) {
      
      if(setOppId!=null)
      {    system.debug('setOppId SIZE' + setOppId.size());
           system.debug('OptyId' + setOppId);
      }
      if(test.isrunningtest()==false){
          adxIntegrationBatchable IB = new adxIntegrationBatchable(setOppId);
          Database.executeBatch(IB,125);
      }

        
   }
}