global class adxSchedulesCalculationBatch implements database.batchable<Sobject>, database.stateful {
   public Set<Id> setOppId = new Set<Id>();
   final String REVENUE = 'Revenue';
   class adxIntegrationBatchableException extends Exception {}
   public adxSchedulesCalculationBatch() 
   {
       string stageName = 'Closed Lost';
       string currencyType ='AUD';
       
      
      
      
      ///  CHECK: if there is going to be any limit issues
      //// querylimit
      string query = 'SELECT Id ';
             query += 'FROM Opportunity ';
             query += 'WHERE  ';
             query += 'Amount != null ';
             //query += ' AND Id =\''+ String.escapeSingleQuotes(opid)+'\'';
             query += ' AND StageName !=\''+ String.escapeSingleQuotes(stageName)+'\'';
             query += ' AND CurrencyIsoCode !=\''+ String.escapeSingleQuotes(currencyType)+'\'';
             query += ' AND Contract_Start_Date_Formula__c != null ';
             query += ' AND CloseDate >= 2013-01-01 ';
             query += ' AND ID IN (Select Opportunity__c from Opportunity_Split__c WHERE Does_Split_Equal_Opp_Amount__c = false) ';
             query += ' ORDER BY Name';
      
      system.debug('Query?' + query);
     
      List<Opportunity> optList = database.query(query);
      for(Opportunity s: optList)
      {
          setOppId.add(s.Id);
      }
      
      system.debug('OPPTYLIST?' + setOppId.SIZE());
      
   }
   global Database.QueryLocator start(database.BatchableContext bc) {
       string stageName = 'Closed Lost';
       string currencyType ='AUD';
       Id opid = '0064000000huqfQ';//'0064000000fjmcLAAQ';
      //List<sobject> lineItems = new List<sObject>();
      string query = 'SELECT Id,pricebookEntryID,opportunityId,UnitPrice,Duration__c,ServiceDate ';
             query += 'FROM Opportunitylineitem ';
             query += 'WHERE  ';
             query += 'Opportunityid =: setOppId';
      
      system.debug('Query?' + query);
      
      return database.getQueryLocator(query);

   }
   
   global void execute(database.BatchableContext bc,List<Sobject> scope) {
      
      
      CreateSchedules(scope);
      
    }
    
private void CreateSchedules(list<OpportunityLineItem> optyLineItems)
{
    
    // Get prepped by retrieving the base information needed 
    Date currentDate; 
    Decimal numberPayments; 
    Decimal paymentAmount; 
    Decimal totalPaid;
    List<OpportunityLineItemSchedule> newScheduleObjects = new List<OpportunityLineItemSchedule>(); 
    
    // For every OpportunityLineItem record, add its associated pricebook entry 
    // to a set so there are no duplicates. 
    Set<Id> pbeIds = new Set<Id>(); 
    for (OpportunityLineItem oli : optyLineItems) {
        pbeIds.add(oli.pricebookentryid);
    } 
    
    // Query the PricebookEntries for their associated info and place the results 
    // in a map. 
    Map<Id, PricebookEntry> entries = new Map<Id, PricebookEntry>([
        select product2.Auto_Schedule__c 
        from pricebookentry where id in :pbeIds
    ]); 
    
    // For every OpportunityLineItem record, add its associated oppty 
    // to a set so there are no duplicates. 
    Set<Id> opptyIds = new Set<Id>(); 
    for (OpportunityLineItem oli : optyLineItems) {
        opptyIds.add(oli.OpportunityId);
    } 
    
    // Query the Opportunities for their associated info and place the results 
    // in a map. 
    Map<Id, Opportunity> Opptys = new Map<Id, Opportunity>([
        select CloseDate, LastModifiedById 
        from Opportunity where id in :opptyIds
    ]); 
    
    // Iterate through the changes 
    for (OpportunityLineItem item : optyLineItems) { 
        if(
            entries.get(item.pricebookEntryID).product2.Auto_Schedule__c == true 
         && opptys.get(item.opportunityId).lastModifiedById != Label.OperativeDashboardUserId // don't run when opportunity was last modified by operative dashboard sync user ?? is this what we want??
         // && opptys.get(item.opportunityId).lastModifiedById != Label.OP1_SYNC_OperativeSyncUser // don't run when opportunity was last modified by operative one sync user ?? is this what we want?? //VG - 04/02/2013 - Code Deprecated as Operative One is currently not in use.
        ) {
            
            //Now, we have an item that needs to be Auto Scheduled 
            //Calculate the payment amount 
            paymentAmount = item.UnitPrice; 
            numberPayments = integer.valueOf(item.Duration__c); 
            paymentAmount = paymentAmount.divide(numberPayments,2); 
            
            // Determine which date to use as the start date. 
            if (item.ServiceDate == NULL) { 
                currentDate = Opptys.get(item.OpportunityId).CloseDate; 
            } else { 
                currentDate = item.ServiceDate; 
            } 
            totalPaid = 0; 
            
            // Loop though the payments 
            for (Integer i = 1;i < numberPayments;i++) { 
                OpportunityLineItemSchedule s = new OpportunityLineItemSchedule(); 
                s.Revenue = paymentAmount; 
                s.ScheduleDate = currentDate; 
                s.OpportunityLineItemId = item.id; 
                s.Type = REVENUE; 
                newScheduleObjects.add(s); 
                totalPaid = totalPaid + paymentAmount;  
                currentDate = currentDate.addDays(1); 
            } 
            
            //Now Calulate the last payment! 
            paymentAmount = item.UnitPrice - totalPaid ; 
            OpportunityLineItemSchedule s = new OpportunityLineItemSchedule(); 
            s.Revenue = paymentAmount; 
            s.ScheduleDate = currentDate; 
            s.OpportunityLineItemId = item.id; 
            s.Type = REVENUE; 
            newScheduleObjects.add(s); 
        } 
    } 
     
    if (newScheduleObjects.size() > 0) { 
        system.debug('newScheduleObjects?' + newScheduleObjects.SIZE());
        system.debug('newScheduleObjects?' + newScheduleObjects);
        
        try { 
            insert newScheduleObjects; 
        } catch (System.DmlException e) { 
            for (Integer ei = 0; ei < e.getNumDml(); ei++) { 
                System.debug(e.getDmlMessage(ei)); 
            }
            
        } 
    }  
    
}

   global void finish(database.BatchableContext bc) {
      
        
        adxOpportunitySplitAmountMismatchBatch adxOPTYSplitsCalculations = new adxOpportunitySplitAmountMismatchBatch();
        Database.executeBatch(adxOPTYSplitsCalculations,25);
        
   }
}