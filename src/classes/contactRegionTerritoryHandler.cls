public class contactRegionTerritoryHandler {
    
    
    public void contactRegionTerritoryHandler()
    {
        
    }
    public void assignRegionTerritory(set<Id> setContactIds)
    {
        Map<id,contact> mapContacts = new Map<id,contact>();
        Map<id,lead> mapConvertedFromLeads = new Map<id,lead>([select id,convertedContactId from lead where isconverted=true and convertedContactId in:setContactIds]);
        for(lead l: mapConvertedFromLeads.values())
        {
            setContactIds.remove(l.convertedContactId);
        }
        Map<id,contact> mapContactsForFutureUpdates = new Map<id,contact>([SELECT ID,Hidden_Lead_Source__c,MailingPostalCode,Region__c,Territory__c from Contact where id in:setContactIds]);
        set<string> contactZipCodes = new set<String>();
        Map<String, Zip_Code__c> mapOfZip = new Map<String, Zip_Code__c>();
        system.debug('THIS IS THE lstContacts'+ mapContactsForFutureUpdates.values());
        for(contact cnt:mapContactsForFutureUpdates.values())
        {
            if(String.isBlank(cnt.Hidden_Lead_Source__c))
            {
                if(!String.isBlank(cnt.MailingPostalCode))
                    contactZipCodes.Add(cnt.MailingPostalCode);
            }
        }
        system.debug('THIS IS THE contactZipCodes'+ contactZipCodes);
        for(Zip_Code__c zc: [Select Name, Territory__c, Region__c, MSA__c from Zip_Code__c where Name =: contactZipCodes]) {
            mapOfZip.put(zc.Name, zc);
        }
        system.debug('THIS IS THE mapOfZip'+ mapOfZip);
        if(mapOfZip.size()>0)
        {
            for(contact cnt:mapContactsForFutureUpdates.values())
            {
                if((!string.isEmpty(cnt.MailingPostalCode)) && mapOfZip.containsKey(cnt.MailingPostalCode))
                {
                    system.debug('THIS IS THE REGION'+ mapOfZip.get(cnt.MailingPostalCode).Region__c);
                    system.debug('THIS IS THE tERRITORY'+ mapOfZip.get(cnt.MailingPostalCode).Territory__c);
                    cnt.Region__c = mapOfZip.get(cnt.MailingPostalCode).Region__c;
                    cnt.Territory__c = mapOfZip.get(cnt.MailingPostalCode).Territory__c;
                }
            }
            
            update mapContactsForFutureUpdates.values();
        }
        
        
    }
    
    
    

}