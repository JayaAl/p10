global class creditVerificationInitiation
{
    public static dtoEaiDnbiCom.MatchCompanyRequestDTO buildWebServiceRequest( comDnbiEaiServiceCreditapplication.CreditApplicationHttpPort stub, String acctId)
    {
        try{
            dtoEaiDnbiCom.AuthenticationDTO_element loginid = new dtoEaiDnbiCom.AuthenticationDTO_element();
                        
            User u = [select dnbiUID__c, DNBI_Password__c from user where DNBi_Login_User__c = true limit 1];
                        
            loginid.LOGIN_ID = u.dnbiUID__c;
            loginid.LOGIN_PASSWORD = u.DNBI_Password__c;
            
            if ( u.dnbiUID__c == null || u.dnbiUID__c == '' || u.DNBI_Password__c == null || u.DNBI_Password__c == '' ) {
                System.debug('Please configure DNBi userID and password in your user object');
            }

            stub.AuthenticationHeader = loginid;
        
            stub.timeout_x = 60000;
            
        
            Account a = [SELECT id,credit_verification2__c, Name,BillingStreet,BillingCity,BillingState,BillingPostalCode,BillingCountry,D_U_N_S__c FROM account WHERE Id =: acctId];

            dtoEaiDnbiCom.MatchCompanyRequestDTO dto= new dtoEaiDnbiCom.MatchCompanyRequestDTO();
            dtoEaiDnbiCom.BusinessEntityDTO bdto = new dtoEaiDnbiCom.BusinessEntityDTO();
            dtoEaiDnbiCom.AddressDTO adto = new dtoEaiDnbiCom.AddressDTO();

            adto.city = a.BillingCity;
            adto.state = a.BillingState;
            adto.street = a.BillingStreet;
            adto.zipCode = a.BillingPostalCode;
            adto.country = a.BillingCountry;

            bdto.businessName = a.Name;

            bdto.address =  adto;
        
            dto.bureauName = 'dnb';
            dto.businessInformation = bdto;

            dto.listOfSimilars = False;
            return(dto);
         }
         catch ( Exception ex ){
                System.debug( 'Building Web Services Request generated following error: ' + ex.getMessage());
                throw (ex);
         }
    }
    public static dtoEaiDnbiCom.CompanySearchResultDTO invokeMatchCompanyWebService( comDnbiEaiServiceCreditapplication.CreditApplicationHttpPort stub, dtoEaiDnbiCom.MatchCompanyRequestDTO dto)
    {
        dtoEaiDnbiCom.CompanySearchResultDTO resp = null;
        try{
            resp = stub.matchCompany(dto);
        }
        catch ( Exception ex ){
            system.debug( 'error: ' + ex.getMessage());
        }
        return(resp);
    }
    public static String handleMatchCompanyWebServiceResponse( comDnbiEaiServiceCreditapplication.CreditApplicationHttpPort stub, dtoEaiDnbiCom.CompanySearchResultDTO resp, String acctId)
    {
        Account a = [SELECT id,credit_verification2__c, Name,BillingStreet,BillingCity,BillingState,BillingPostalCode,BillingCountry,D_U_N_S__c FROM account WHERE Id =: acctId];
        if ( resp == null ) {
            return ('No Match Found resp=null');
        }
        if ( resp.bureauCompanyList == null ) {
            return ('No Match Found bureauList=null');
        }
                       
        businessbureauDtoEaiDnbiCom.ArrayOfBureauCompanyDTO compList = resp.bureauCompanyList;
        businessbureauDtoEaiDnbiCom.BureauCompanyDTO[] compArray = compList.BureauCompanyDTO;
    
        string duns = '';
        if ( compArray.size() > 0 )
        {
            double score = compArray[0].matchScore;
            if ( score > 6 ){
                duns = compArray[0].bureauIdentifierNumber;
            } else 
                return ('No Match Found compArray=null');
        } else {
            return ('No Match Found duns=null');
        }
        return(duns);
    }
    public static dtoEaiDnbiCom.ArrayOfFieldDTO buildCreditApplicationRequest(Account a, String duns, Double amt)
    {        
        dtoEaiDnbiCom.FieldDTO[] appfields = new dtoEaiDnbiCom.FieldDTO[100];
        dtoEaiDnbiCom.ArrayOfFieldDTO appfieldlist = new dtoEaiDnbiCom.ArrayOfFieldDTO();
    
        integer i = 0;
    
        dtoEaiDnbiCom.FieldDTO appfield = new dtoEaiDnbiCom.FieldDTO();
        appfield = new dtoEaiDnbiCom.FieldDTO();
        appfield.name = 'BizInfo-BureauName';
        appfield.value = 'dnb';
        appfield.fieldType = 'GENERAL';
        appfields[i++] = appfield;
       
        appfield = new dtoEaiDnbiCom.FieldDTO();
        appfield.name = 'BizInfo-BusinessName';
        appfield.value = a.Name;
        appfield.fieldType = 'GENERAL';
        appfields[i++] = appfield;

        appfield = new dtoEaiDnbiCom.FieldDTO();
        appfield.name = 'BizInfo-StreetAddress';
        appfield.value = a.BillingStreet;
        appfield.fieldType = 'GENERAL';
        appfields[i++] = appfield;

        appfield = new dtoEaiDnbiCom.FieldDTO();
        appfield.name = 'BizInfo-SecondaryStreetAddress';
        appfield.value = '';
        appfield.fieldType = 'GENERAL';
        appfields[i++] = appfield;

        appfield = new dtoEaiDnbiCom.FieldDTO();
        appfield.name = 'BizInfo-City';
        appfield.value = a.BillingCity;
        appfield.fieldType = 'GENERAL';
        appfields[i++] = appfield;

        appfield = new dtoEaiDnbiCom.FieldDTO();
        appfield.name = 'BizInfo-State';
        appfield.value = a.BillingState;
        appfield.fieldType = 'GENERAL';
        appfields[i++] = appfield;

        appfield = new dtoEaiDnbiCom.FieldDTO();
        appfield.name = 'BizInfo-ZIPCode';
        appfield.value = a.BillingPostalCode;
        appfield.fieldType = 'GENERAL';
        appfields[i++] = appfield;

        appfield = new dtoEaiDnbiCom.FieldDTO();
        appfield.name = 'BizInfo-Country';
        appfield.value = 'US';
        appfield.fieldType = 'GENERAL';
        appfields[i++] = appfield;

        appfield = new dtoEaiDnbiCom.FieldDTO();
        appfield.name = 'BizInfo-BusinessPhone';
        appfield.value = '123456789';
        appfield.fieldType = 'GENERAL';
        appfields[i++] = appfield;

        appfield = new dtoEaiDnbiCom.FieldDTO();
        appfield.name = 'BizProfile-ContactFirstName';
        appfield.value = 'unknown';
        appfield.fieldType = 'GENERAL';
        appfields[i++] = appfield;

        appfield = new dtoEaiDnbiCom.FieldDTO();
        appfield.name = 'BizProfile-ContactLastName';
        appfield.value = 'unknown';
        appfield.fieldType = 'GENERAL';
        appfields[i++] = appfield;
            
                    
        appfield = new dtoEaiDnbiCom.FieldDTO();
        appfield.name = 'BizProfile-ContactTitleinBusiness';
        appfield.value = 'unknown';
        appfield.fieldType = 'GENERAL';
        appfields[i++] = appfield;
    
        appfield = new dtoEaiDnbiCom.FieldDTO();
        appfield.name = 'BizProfile-ContactDepartment';
        appfield.value = 'unknown';
        appfield.fieldType = 'GENERAL';
        appfields[i++] = appfield;

        appfield = new dtoEaiDnbiCom.FieldDTO();
        appfield.name = 'BizProfile-ContactPhoneNumber';
        appfield.value = '123456789';
        appfield.fieldType = 'GENERAL';
        appfields[i++] = appfield;

        
        appfield = new dtoEaiDnbiCom.FieldDTO();
        appfield.name = 'BizProfile-ContactE-mailAddress';
        appfield.value = 'b@c.com';
        appfield.fieldType = 'GENERAL';
        appfields[i++] = appfield;

            
        appfield = new dtoEaiDnbiCom.FieldDTO();
        appfield.name = 'BizProfile-BusinessUnit';
        appfield.value = '';
        appfield.fieldType = 'GENERAL';
        appfields[i++] = appfield;
      
        appfield = new dtoEaiDnbiCom.FieldDTO();
        appfield.name = 'BizInfo-DUNSNumber';
        appfield.value = duns;
        appfield.fieldType = 'GENERAL';
        appfields[i++] = appfield;

        appfield = new dtoEaiDnbiCom.FieldDTO();
        appfield.name = 'BizInfo-RequestedAmount';
        appfield.value = String.valueOf(amt);
        appfield.fieldType = 'GENERAL';
        appfields[i++] = appfield;
     
        appfieldlist.FieldDTO = appfields;
        return(appfieldlist);
    }
    public static creditappMessageEaiDnbiCom.ApplyForCreditResult invokeCreditApplicationWebService(comDnbiEaiServiceCreditapplication.CreditApplicationHttpPort stub, dtoEaiDnbiCom.ArrayOfFieldDTO appfieldlist, creditappMessageEaiDnbiCom.ApplyForCreditResult responseCA)
    {
        responseCA = stub.applyForCredit(appfieldlist, true);
        return(responseCA);
    }
    public static String handleCreditApplicationWebServiceResponse(creditappMessageEaiDnbiCom.ApplyForCreditResult response, Credit_Verification__c cv, Account a)
    {
        if ( response != null && response.applicationECF != null ){
             //cv.Approved_Amount_text__c = String.valueOf(response.applicationECF.decisionOutcome.recmmendedCreditTerms.creditLimit);
             if ( String.valueOf(response.applicationECF.decisionOutcome.recmmendedCreditTerms.creditLimit) != 'NaN'){
                 cv.Approved_Amount__c = response.applicationECF.decisionOutcome.recmmendedCreditTerms.creditLimit;
                 cv.Payment_Terms__c = response.applicationECF.decisionOutcome.recmmendedCreditTerms.paymentTerms;
             } else {
                 cv.Approved_Amount__c = 0;
                 cv.Payment_Terms__c = 'PrePay';
             }             
             cv.Status__c = String.valueOf(response.applicationECF.decisionOutcome.outcome);
             cv.Advertiser__c = a.Id;
             cv.Approved_By__c = 'System Integration with DNBi';
             //cv.Pandora_Application_Score__c = response.applicationECF.scoreList.ScoreDTO;
             creditappDtoEaiDnbiCom.CreditApplicationECFDTO crdAppECF = response.applicationECF;
             if ( crdAppECF != null) {
                dtoEaiDnbiCom.ArrayOfScoreDTO  score =  crdAppECF.scoreList;
                if ( score != null ) {
                    dtoEaiDnbiCom.ScoreDTO[] scorearr = score.ScoreDTO;
                    integer j;
                    for ( j = 0; j < scorearr.size(); j++) {
                        if ( scorearr[j].scoreName ==  'Pandora Application Scorecard')
                        cv.Pandora_Application_Score__c = String.valueOf(scorearr[j].value);
                    }
                }
             }
             upsert cv;
             // Forseva (11/6/13): set the Pandora Score field on the account - start.
             a.Pandora_Score__c = Decimal.valueOf(cv.Pandora_Application_Score__c);
             // Forseva (11/6/13): set the Credit Limit and Pandora Score fields on the account - end.
             a.Credit_Verification2__c = cv.Id;
             update a;
             return ('Application ' + response.applicationId + ' is  submitted, Credit decision from DNBi: ' + cv.Status__c );
        }
        dtoEaiDnbiCom.ArrayOfFieldErrorDTO errorFieldList = response.errorFieldList; 
        if ( errorFieldList != null ){
           dtoEaiDnbiCom.FieldErrorDTO[] fieldErr = errorFieldList.FieldErrorDTO;
           if ( fieldErr != null  ){
               dtoEaiDnbiCom.FieldDTO fdto = fieldErr[0].fieldDTO;
               if ( fdto != null ){
                  return ( 'Error Input: ' + 'Field Name:' + fdto.name + '  Field Value: ' + fdto.value );
               }
           }
        }
        return ('Error');
    }
    
    Webservice static String ApplyForCredit(String acctId, String cvId)
    {
        Account a;
        Credit_Verification__c cv;
        String result = null;
        try {
            // Forseva - 6/9/14: pull in more Account level fields for Forseva processing.
            //a = [SELECT id,credit_verification2__c, Name,BillingStreet,BillingCity,BillingState,BillingPostalCode,BillingCountry,D_U_N_S__c FROM account WHERE Id =: acctId];
            a = [select Id, Name, BillingStreet, BillingCity, BillingState, BillingPostalCode, BillingCountry, D_U_N_S__c, Type, 
                        forseva1__DUNS_Number__c, forseva1__Cortera_Link_Id__c, forseva1__Credit_Policy__c 
                 from   Account 
                 where  Id = :acctId];
            double amt = 0;
            cv = new Credit_Verification__c();
            if (cvId != '') {
                cv = [SELECT  id,Requested_Amount__c,Approved_Amount_text__c,Pandora_Application_Score__c FROM Credit_Verification__c where Id =: cvId];
                amt = cv.Requested_Amount__c;
            } else {
                cv.Approved_Amount_text__c = '0';
            }
            /*
            // Forseva - 6/9/14: don't execute this code anymore 
            comDnbiEaiServiceCreditapplication.CreditApplicationHttpPort stub = new comDnbiEaiServiceCreditapplication.CreditApplicationHttpPort();
            dtoEaiDnbiCom.MatchCompanyRequestDTO dto = buildWebServiceRequest(stub, acctId);
            dtoEaiDnbiCom.CompanySearchResultDTO resp = invokeMatchCompanyWebService(stub, dto);
            String duns = handleMatchCompanyWebServiceResponse(stub, resp, acctId);
            creditappMessageEaiDnbiCom.ApplyForCreditResult responseCA = new creditappMessageEaiDnbiCom.ApplyForCreditResult();
            dtoEaiDnbiCom.ArrayOfFieldDTO appFieldsList = buildCreditApplicationRequest(a, duns, amt);
            responseCA = invokeCreditApplicationWebService(stub, appFieldsList, responseCA);
            result = handleCreditApplicationWebServiceResponse(responseCA, cv, a);
            */
            forseva1.CommercialReportService crs = new forseva1.CommercialReportService('Dun & Bradstreet');
            forseva1__DnBCustomReport__c dnb;
            forseva1__CorteraCPR__c cpr;
            Boolean dnbCalled = false, cprCalled = false;
            if (a.forseva1__DUNS_Number__c == null) {
                try {
                    dnbCalled = true;
                    dnb = (forseva1__DnBCustomReport__c)crs.getAndSaveReport('Verification', 'DnB Custom Report', a.Id, a.forseva1__DUNS_Number__c);
                }
                catch (Exception e) {
                    dnb = null;
                }
            }
            else {
                try {
                    dnb = (forseva1__DnBCustomReport__c)crs.getAndSaveReport('Credit Review', 'DnB Custom Report', a.Id, a.forseva1__DUNS_Number__c);
                }
                catch (Exception e) {
                    dnb = null;
                }
            }
            if (dnbCalled && dnb != null) {
                cv.Commercial_Credit_Score_Class__c = dnb.CMCL_CR_SCR_CLAS__c;
                cv.DNB_Rating__c = dnb.USDS_DNB_RATG__c;
                cv.Portf_Comparison_Score_Class_Score__c = dnb.PTFL_CMPA_SCRCLAS_SCR__c;
                cv.Viability_Score__c = dnb.VBLTY_SCRCLAS_SCR__c;
                cv.Approved_Amount__c = dnb.forseva1__F_Credit_Limit_Approved__c;
                if (dnb.forseva1__F_Credit_Limit_Approved__c == 0) {
                    cv.Payment_Terms__c = 'PrePay';
                }
                cv.Status__c = dnb.forseva1__F_Credit_Review_Status__c != null && dnb.forseva1__F_Credit_Review_Status__c == 'Passed' ? 'Approved' : dnb.forseva1__F_Credit_Review_Status__c;
                cv.Advertiser__c = a.Id;
                cv.Approved_By__c = 'System Integration with DnB';
                //str += 'about to upsert cv... \n';
                upsert cv;
                a.Credit_Verification2__c = cv.Id;
                update a;
                //str += 'after upsert of cv... \n';
                result = 'Application ' + dnb.Name + ' was  submitted.  Credit decision from DNB: ' + cv.Status__c;
            }
            else if (dnbCalled && dnb == null) {
                crs = new forseva1.CommercialReportService('Cortera');
                if (a.forseva1__Cortera_Link_Id__c == null) {
                    try {
                        cprCalled = true;
                        cpr = (forseva1__CorteraCPR__c)crs.getAndSaveReport('Verification', 'Cortera CPR', a.Id, a.forseva1__Cortera_Link_Id__c);
                    }
                    catch (Exception e) {
                        cpr = null;
                    }
                }
                else if (a.forseva1__Cortera_Link_Id__c != null) {
                    try {
                        cprCalled = true;
                        cpr = (forseva1__CorteraCPR__c)crs.getAndSaveReport('Credit Review', 'Cortera CPR', a.Id, a.forseva1__Cortera_Link_Id__c); 
                    }
                    catch (Exception e) {
                        cpr = null;
                    }
                }
                if (cpr != null) {
                    cv.Approved_Amount__c = cpr.forseva1__F_Credit_Limit_Approved__c;
                    if (cpr.forseva1__F_Credit_Limit_Approved__c == 0) {
                        cv.Payment_Terms__c = 'PrePay';
                    }
                    cv.Status__c = cpr.forseva1__F_Credit_Review_Status__c != null && cpr.forseva1__F_Credit_Review_Status__c == 'Passed' ? 'Approved' : cpr.forseva1__F_Credit_Review_Status__c;
                    cv.Advertiser__c = a.Id;
                    cv.Approved_By__c = 'System Integration with Cortera';
                }
                result = 'Application ' + cpr.Name + ' was  submitted.  Credit decision from Cortera: ' + cv.Status__c;
            }
        } catch (Exception e) {
            result = 'Error Generated in main Class - contact System Administrator for assistance in :' + e;
        }
        return result;
    }
    
    Webservice static String applyForCreditInForseva(String acctId, String cvId) {
        String result = null;
        String str = 'In applyForCreditInForseva for acc.Id = ' + acctId + '... \n';
        Boolean sendEmailToAR = false;
        Account acc;
        Credit_Verification__c cv;
        try {
            acc = [select Id, Name, Credit_Verification2__c, forseva1__DUNS_Number__c, forseva1__Cortera_Link_Id__c 
                   from   Account 
                   where  Id =: acctId];
            if (cvId != '') {
                cv = [select Id, Requested_Amount__c, Approved_Amount_text__c, Pandora_Application_Score__c, Status__c 
                      from   Credit_Verification__c 
                      where  Id = :cvId];
            } 
            else {
                cv = new Credit_Verification__c();
                cv.Approved_Amount_text__c = '0';
                cv.Status__c = 'Pending Review';
            }
            forseva1.CommercialReportService crs = new forseva1.CommercialReportService('Dun & Bradstreet');
            forseva1__DnBCustomReport__c dnb = null;
            forseva1__CorteraCPR__c cpr = null;
            Boolean dnbCalled = false, cprCalled = false;
            try {
                //str += 'about to call crs.performReviewCredit() for DnB: acc.Id = ' + acctId + ', name = ' + acc.Name + ' and DUNS = ' + acc.forseva1__DUNS_Number__c + '... \n';
                if (crs.performReviewCredit(acc.Id)) {
                    //str += 'crs.performReviewCredit() is true... \n';
                    acc = [select Id, Name, Credit_Verification2__c, forseva1__DUNS_Number__c, forseva1__Cortera_Link_Id__c 
                           from   Account 
                           where  Id =: acctId];
                    //str += 'acc.DUNS = ' + acc.forseva1__DUNS_Number__c + ' and acc.LinkId = ' + acc.forseva1__Cortera_Link_Id__c + '  \n';
                    if (acc.forseva1__DUNS_Number__c != null) {
                        //str += 'selecting most recent row from DnBCustomReport \n';
                        dnb = [select Id, Name, CMCL_CR_SCR_CLAS__c, USDS_DNB_RATG__c, VBLTY_SCRCLAS_SCR__c, forseva1__F_Credit_Limit_Approved__c, 
                               PTFL_CMPA_SCRCLAS_SCR__c,
                               forseva1__F_Credit_Review_Status__c, forseva1__F_Credit_Review_Comments__c, Payment_Terms__c
                               from   forseva1__DnBCustomReport__c
                               where  forseva1__Account__c = :acc.Id order by CreatedDate desc limit 1];
                        //str += 'DNB cr limit approved = ' + dnb.forseva1__F_Credit_Limit_Approved__c + '... \n';
                    }
                    else if (acc.forseva1__Cortera_Link_Id__c != null) {
                        //str += 'selecting most recent row from CorteraCPR \n';
                        cpr = [select Id, Name, forseva1__CprIndexRating__c, forseva1__CprIndexSegment__c, RiskScorecardRiskRating__c, 
                                      forseva1__F_Credit_Limit_Approved__c, forseva1__F_Credit_Review_Status__c, Payment_Terms__c
                               from   forseva1__CorteraCPR__c
                               where  forseva1__Account__c = :acc.Id order by CreatedDate desc limit 1];
                        //str += 'Cortera cr limit approved = ' + cpr.forseva1__F_Credit_Limit_Approved__c + '... \n';
                    }
                    else {
                        //str += 'Nope... performCreditReview() was true but neither DP Id was set...\n';
                    }
                }
                else {
                    //str += 'performCreditReview() returned false but neither DP Id was set...\n';
                }
                if (dnb != null) {
                    cv.Commercial_Credit_Score_Class__c = dnb.CMCL_CR_SCR_CLAS__c;
                    cv.DNB_Rating__c = dnb.USDS_DNB_RATG__c;
                    cv.Portf_Comparison_Score_Class_Score__c = dnb.PTFL_CMPA_SCRCLAS_SCR__c;
                    cv.Viability_Score__c = dnb.VBLTY_SCRCLAS_SCR__c;
                    cv.Approved_Amount__c = dnb.forseva1__F_Credit_Limit_Approved__c;
                    cv.DnB_Custom_Report__c = dnb.Id;
                    cv.Cortera_CPR__c = null;
                    cv.Payment_Terms__c = dnb.Payment_Terms__c;
                    cv.Status__c = dnb.forseva1__F_Credit_Review_Comments__c;
                    cv.Advertiser__c = acc.Id;
                    cv.Approved_By__c = 'System Integration with DnB';
                    result = 'Application ' + dnb.Name + ' was  submitted.  Credit decision from DNB: ' + cv.Status__c;
                }
                else if (cpr != null) {
                    cv.Approved_Amount__c = cpr.forseva1__F_Credit_Limit_Approved__c;
                    cv.CPR_Index_Rating__c = cpr.forseva1__CprIndexRating__c;
                    cv.CPR_Index_Segment__c = cpr.forseva1__CprIndexSegment__c;
                    cv.Risk_Rating__c = cpr.RiskScorecardRiskRating__c;
                    cv.Cortera_CPR__c = cpr.Id;
                    cv.DnB_Custom_Report__c = null;
                    cv.Approved_Amount__c = cpr.forseva1__F_Credit_Limit_Approved__c;
                    cv.Payment_Terms__c = cpr.Payment_Terms__c;
                    cv.Status__c = cpr.forseva1__F_Credit_Review_Status__c != null && cpr.forseva1__F_Credit_Review_Status__c == 'Passed' ? 'Approved' : cpr.forseva1__F_Credit_Review_Status__c;
                    cv.Advertiser__c = acc.Id;
                    cv.Approved_By__c = 'System Integration with Cortera';
                    result = 'Application ' + cpr.Name + ' was  submitted.  Credit decision from Cortera: ' + cv.Status__c;
                }
                else {
                    sendEmailToAR = true;
                }
            }
            catch (Exception e) {
                //str += 'Exc. caught after to call crs.getAndSaveRpt() for DnB acc verification: ' + e.getMessage() + '... \n';
                result = 'Credit could not be established, the credit team will investigate.';
                sendEmailToAR = true;
            }
            try {
                upsert cv;
                //str += 'after upsert cv and before update of acc... \n';
                acc.Credit_Verification2__c = cv.Id;
                update acc;
            }
            catch (Exception e) {
                //str += 'Exc. caught after to call crs.getAndSaveRpt() for DnB acc verification: ' + e.getMessage() + '... \n';
                result = 'Credit could not be established, the credit team will investigate.';
                sendEmailToAR = true;
            }
        }
        catch (Exception e) {
            //str += 'exception caught in applyForCreditInForseva(): e = ' + e.getMessage() + ' \n';
            result = 'Credit could not be established, the credit team will investigate.';
            sendEmailToAR = true;
        }
        str += 'result = ' + result + '\n' + 'sendEmailToAR = ' + sendEmailToAR + '\n';
        system.debug(str);
        //String[] toAddresses = new String[] {'rpelaez@forseva.com'};
        //ForsevaUtilities.sendEmailNotification('About to return from applyForCreditInForseva()', str + '\n\nresult = ' + result, toAddresses);
        if (sendEmailToAR) {
            EmailTemplate em = [select Id, Name from EmailTemplate where Name = 'Credit Not Established for Account'];
            if (System.Test.isRunningTest()) {
                System.assert(UserInfo.getUserId() != null, 'UserInfo.getUserId() is null');
                System.assert(acc != null, 'acc is null');
                System.assert(em != null, 'email template not found during test');
            }
            ForsevaUtilities.sendEmailUsingTemplate(UserInfo.getUserId(), acc.Id, em.Id, Label.Forseva_AR_Email_Address);
        }
        return result;
    }
       
}