public class googleauthclass{

public string authtoken{get;set;}
public string refereshtoken{get;set;}
public string bodyprint{get;set;}

//Settings needed on the google cloud console.One can store this securely in custom settings or an object.
public static final string REFRESH_TOKEN = '1/s10SWqHKWCsqu8Vd4oF-5S3PcpEchrUispAKcUMuGew';
public static final string CLIENT_SECRET='o4Tk-CtoGA15r1BFmZrj9uFw';//Fill as per your registered app settings in google console
public static final string CLIENT_ID='723789058145-miooatskvlrihp0i8r620uvc2v0gu8k2.apps.googleusercontent.com';//Fill as per your registered app settings in google console
public static final string REDIRECT_URL='https://pandora--pandora10--c.cs27.visual.force.com/apex/googleAuth';

public static final string OAUTH_TOKEN_URL='https://accounts.google.com/o/oauth2/token';
public static final string OAUTH_CODE_END_POINT_URL='https://accounts.google.com/o/oauth2/auth';
public static final string OAUTH_REFRESHTOKEN_URL='https://www.googleapis.com/oauth2/v4/token';

public static final string GRANT_TYPE='grant_type=authorization_code';

//Scope URL as per oauth 2.0 guide of the google 
public static final string SCOPE='https://www.googleapis.com/auth/adexchange.seller';
public static final string STATE='/profile';

//Approval Prompt Constant
public static final string APPROVAL_PROMPT='force';





   public pagereference connect(){
   
     String x=OAUTH_CODE_END_POINT_URL+'?scope='+EncodingUtil.urlEncode(SCOPE,'UTF-8')+'&state='+EncodingUtil.urlEncode(STATE,'UTF-8')+'&redirect_uri='+EncodingUtil.urlEncode(REDIRECT_URL,'UTF-8')+'&response_type=code&client_id='+CLIENT_ID+'&access_type=offline&approval_prompt='+APPROVAL_PROMPT;
     
     pagereference p=new pagereference(x);
     return p;
     
   }
   
   public void refresh(){
   
     String x=OAUTH_REFRESHTOKEN_URL+'?refresh_token='+EncodingUtil.urlEncode(REFRESH_TOKEN,'UTF-8')+'&client_id='+CLIENT_ID+'&client_secret='+EncodingUtil.urlEncode(CLIENT_SECRET,'UTF-8')+'&grant_type=refresh_token';
     system.debug(x);
     Http h = new Http();
     HttpRequest req = new HttpRequest();
        req.setEndpoint(x);
        req.setHeader('Content-Type','application/x-www-form-urlencoded');
        req.setMethod('POST');
        req.setBody('');
        
        //system.debug('REQUEST BODY'+body);
        
        // Send the request, and return a response
        HttpResponse res = h.send(req);
        
        system.debug('body'+res.getbody());
        
        //bodyprint=res.getbody();
     
     
   }
   
    public pagereference showtoken(){
   
       string codeparam=apexpages.currentpage().getparameters().get('code');
          
        // Instantiate a new http object
        Http h = new Http();
        
        String body='code='+codeparam+'&client_id='+CLIENT_ID+'&client_secret='+CLIENT_SECRET+'&redirect_uri='+REDIRECT_URL+'&'+GRANT_TYPE;
        
        // Instantiate a new HTTP request, specify the method (GET) as well as the endpoint
        HttpRequest req = new HttpRequest();
        req.setEndpoint(OAUTH_TOKEN_URL);
        req.setHeader('Content-Type','application/x-www-form-urlencoded');
        req.setMethod('POST');
        req.setBody(body);
        
        system.debug('REQUEST BODY'+body);
        
        // Send the request, and return a response
        HttpResponse res = h.send(req);
        
        system.debug('body'+res.getbody());
        
        bodyprint=res.getbody();
        
        return null;
   
   }
   
   
   public static String getAccesstoken () {
   
   
       /// TODO fetch the access token and refresh the access token with the refresh toke, client id and secret as and when needed
       return null;
   }
  
}