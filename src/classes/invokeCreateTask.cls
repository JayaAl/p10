//ESS-36111
global class invokeCreateTask{

      @InvocableMethod(label='Insert Tasks' description='Inserts the accounts specified and returns the IDs of the new accounts.')
      public static void insertTasks(List<InvocableTaskwrapper> wrapperLst) {
        
        list<Task> tasklstToInsert = new list<Task>(); // Task list to insert
        list<id> IODetailIdLst = new list<id>();
        Task taskObj;

        for(InvocableTaskwrapper wrapperVar :  wrapperLst)
            IODetailIdLst.add(wrapperVar.IODetailId);

        Map<Id,IO_Detail__c> IODetailObjMap = new Map<Id,IO_Detail__c>([Select Id,Opportunity__r.Lead_Campaign_Manager__c from IO_Detail__c where Id IN: IODetailIdLst ]);
       
        for(InvocableTaskwrapper wrapperVar : wrapperLst){

            taskObj = new Task();
            taskObj.WhatId = wrapperVar.IODetailId;
            taskObj.CurrencyIsoCode = 'USD';
            taskObj.Priority = 'Normal';
            taskObj.OwnerId = IODetailObjMap.get(wrapperVar.IODetailId).Opportunity__r.Lead_Campaign_Manager__c;
            taskObj.RecordTypeId = Schema.SObjectType.Task.getRecordTypeInfosByName().get('Original').getRecordTypeId();
            taskObj.Status = 'Not started';
            taskObj.IsReminderSet = true;
            taskObj.Subject = (wrapperVar.isScheduledAction) ? 'Action Needed: Pending IO Scheduled ' : 'Pending IO';
            taskObj.ActivityDate =  (wrapperVar.isScheduledAction) ? (Date.Today() + 14) : Date.Today();
            
            if(!wrapperVar.isScheduledAction){

                // Logic for multiple recurrent tasks
                // taskObj.IsRecurrence = true;
                // taskObj.RecurrenceType = 'RecursWeekly';
                // taskObj.RecurrenceDayOfWeekMask = Integer.ValueOf(Math.pow(2,dayIndexMap.get(((DateTime) Date.Today()).format('E'))));
                //taskObj.RecurrenceStartDateOnly = Date.Today();
                //taskObj.RecurrenceEndDateOnly = Date.Today() + 60;

                // Logic for recurrent tasks one task at a time                
                taskObj.RecurrenceInterval = 1;
                taskObj.RecurrenceRegeneratedType = 'RecurrenceRegenerateAfterDueDate';
            }

            tasklstToInsert.add(taskObj);
        }
            
            if(tasklstToInsert != null && tasklstToInsert.size() > 0){

                database.insert(tasklstToInsert,false);
            }
        
      }
      
      global class InvocableTaskwrapper{          
          @InvocableVariable(required=true)
          global Id IODetailId;
          @InvocableVariable(required=true)
          global Boolean isScheduledAction;
      }
}