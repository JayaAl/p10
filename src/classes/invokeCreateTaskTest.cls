@istest(seeAllData=true)
public class invokeCreateTaskTest{
    
    
    public static testmethod void  testIOProcess(){
        
        Opportunity opp = [Select Id from Opportunity where id = '00622000002GVMFAA4' limit 1];
        List<IO_Detail__c> ioDetailLst = new List<IO_Detail__c>();
        test.startTest();
        for(Integer i=0;i<1000;i++){
        IO_Detail__c io = new IO_Detail__c();
        
        io.Opportunity__c = opp.Id;
        io.Paperwork_Origin__c = 'Pandora Paperwork - no changes';
        io.Payment_Terms_Preferred__c = 'Pre-Pay';
        io.PortalPaymentStatus__c = 'Unpaid';
        ioDetailLst.add(io);
        }
        insert ioDetailLst;
        //system.debug('status : '+[Select Id,status_2__c from IO_Detail__c where id =: io.Id limit 1].status_2__c);
        test.stopTest();
    } 
    

}