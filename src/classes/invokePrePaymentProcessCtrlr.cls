public class invokePrePaymentProcessCtrlr{
    
    public boolean isEditMode {get;set;}

    public void invokePrePaymentProcess(){
        DataBase.executeBatch(new CaseExpirationBatch());
    }

    public invokePrePaymentProcessCtrlr(){
		isEditMode = false;
		if(ApexPages.currentPage().getParameters().containsKey('isEditMode')){
      		isEditMode  = Boolean.valueOf(ApexPages.currentPage().getParameters().get('isEditMode'));
    	}
    }

    @RemoteAction
    public static List<DMAWrapper> getDMAValues(){
    	
    	List<DMAWrapper> wrapperLst = new List<DMAWrapper>();

    	for(ATG_DMA__c dmaVar : [Select id,name from ATG_DMA__c]){

    		wrapperLst.add(new DMAWrapper(dmaVar.Id,dmaVar.Name) );
    	}

    	return wrapperLst;
    }

     @RemoteAction
    public static List<countyWrapper> getCounties(Id stateId){
    	
    	List<countyWrapper> wrapperLst = new List<countyWrapper>();
    	
    	if(stateId != null){
	    	for(ATG_County__c countyVar : [Select id,name,ATG_County_Name__c,ATG_State__c from ATG_County__c where ATG_State__c =: stateId ])
	    		wrapperLst.add(new countyWrapper(countyVar.Id,countyVar.ATG_County_Name__c) );	    	
    	}
    	
    	return wrapperLst;
    }


    @RemoteAction
    public static List<GeoCodeWrapper> getGeoCodeValues(){
    	
    	List<GeoCodeWrapper> wrapperLst = new List<GeoCodeWrapper>();

    	for(ATG_State__c stateVar : [Select id,name,ATG_State_Code__c from ATG_State__c]){

    		wrapperLst.add(new GeoCodeWrapper(stateVar.Id,stateVar.ATG_State_Code__c) );
    	}

    	return wrapperLst;
    }

    public class GeoCodeWrapper{

    	public string geoCodeId;
    	public string geoCodeName;

    	public GeoCodeWrapper(String geoid,String geoName){

    		geoCodeId = geoid;
    		geoCodeName = geoName;
    	}
    }

     public class countyWrapper{

    	public string countyId;
    	public string countyName;

    	public countyWrapper(String countyidVar,String countyidName){

    		countyId = countyidVar;
    		countyName = countyidName;
    	}
    }

    public class DMAWrapper{

    	public string dmaId;
    	public string dmaName;

    	public DMAWrapper(String dmaidVar,String dmaNameVar){

    		dmaId = dmaidVar;
    		dmaName = dmaNameVar;
    	}
    }
}