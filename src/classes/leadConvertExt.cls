public with sharing class leadConvertExt {
    
    public Lead leadRecord {get;set;}
    public Account account {get;set;}
    public leadConvertExt(ApexPages.StandardController controller) {
    
        leadRecord = (Lead)controller.getRecord();

    }
    public PageReference convertLead() {
        
        String resp;
        PageReference pref;
        Lead leadTOConvert = new Lead(Id = leadRecord.Id,
                                Cancel_Workflow__c = true);
        try {
            upsert leadTOConvert;
            String redirect = '/lead/leadconvert.jsp?nooppti=1&id='+leadTOConvert.Id+'&RetURL=/'+leadTOConvert.Id;
            pref = new PageReference(redirect);
        } catch(Exception e) {
            //respStr = 'Error:'+e.getMessage();
        }                    
        return pref;
    }
    public void cancel(){}

}