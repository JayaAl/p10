public without sharing Class manageIOHistory{    
    
/* Method to populate a Map contining Field Name => Field Label */
    private static Map<String, String> mapFieldPathToLabel;
    private void queryFS(){
        mapFieldPathToLabel = new Map<String, String>();
        for(FieldSetMember f:Schema.SObjectType.IO_Detail__c.fieldSets.IO_Detail_History_Tracking.getFields()){
            mapFieldPathToLabel.put(f.getFieldPath(),f.getLabel());
        }
    }
    
/* Method to insert new IO Detail History records */
    private List<IO_Detail_History__c> listNewHistory;
    private void insNewHistoryRecords(){
        if(!listNewHistory.isEmpty()){
            try {
                insert listNewHistory;
            } catch (Exception e) {
                String myerror = 'Unable to insert new IO Detail History records:  ';
                System.assert(false, myerror + e);
            }
        }
    }
    
/* 
    Variable to store a cache of previously updated records
    Values are set directly by Trigger
*/
    public Map<Id,Map<String,Set<String>>> mapRCache;
    
/* Method to check the the current change against the recursive cache to determine if it is unique */
    private boolean uniqueHistory(IO_Detail_History__c newDH){
        // check mapRCache to find if the change already exists
        if( mapRCache.isEmpty()){
            mapRCache = new Map<Id,Map<String,Set<String>>>();
        }
        if( ! mapRCache.containsKey(newDH.IO_Detail__c)){
            // IO_Detail__c not found, add it
            mapRCache.put(newDH.IO_Detail__c,new Map<String,Set<String>>());
        }
        if( ! mapRCache.get(newDH.IO_Detail__c).containsKey(newDH.Field__c)){
            // Field__c not found, add it
            mapRCache.get(newDH.IO_Detail__c).put(newDH.Field__c , new Set<String>());
        }
        if( ! mapRCache.get(newDH.IO_Detail__c).get(newDH.Field__c).contains(newDH.Changed_To__c)){
            // Changed_To__c not found, add it
            mapRCache.get(newDH.IO_Detail__c).get(newDH.Field__c).add(newDH.Changed_To__c);
            // return True
            return true;
        }
        // if the value already exists return False
        return False;
    }

/* Method to update previously created IO Detail History records to te newly created ones. */
    private void reconcileHistory(){
        // gather the details of the newly inserted records
        Set<Id> newHistoryIds = new Set<Id>();
        Map<Id, Map<String, Id>> mapDetailFieldHistory = new Map<Id, Map<String, Id>>();
        for(IO_Detail_History__c i:listNewHistory){
            newHistoryIds.add(i.Id);
            
            // Check if IO Detail Id exists
            if( !mapDetailFieldHistory.containsKey(i.IO_Detail__c) ){
                mapDetailFieldHistory.put(i.IO_Detail__c, new Map<String, Id>());
            }
            // Check if Field exists
            if( !mapDetailFieldHistory.get(i.IO_Detail__c).containsKey(i.Field__c) ){
                mapDetailFieldHistory.get(i.IO_Detail__c).put(i.Field__c,i.Id);
            }
            // If the value was already found in all of the above do nothing, we only can only associate past changes to one update at a time
        }
        
        // For each currently unassigned IO History Detail record on the updated IO Detail records
        List<IO_Detail_History__c> oldToUpdate = new List<IO_Detail_History__c>();
        for(IO_Detail_History__c old:[
            SELECT Id, 
                Changed_From__c, Changed_To__c, 
                Field__c, IO_Detail__c, Next_IO_Detail_History__c
            FROM IO_Detail_History__c
            WHERE IO_Detail__c IN :mapDetailFieldHistory.keySet()
            AND ID Not In :newHistoryIds
            AND Next_IO_Detail_History__c = ''
        ]){
            // try to find the new record that the old record should be associated to
            if(mapDetailFieldHistory.containsKey(old.IO_Detail__c)){
                if(mapDetailFieldHistory.get(old.IO_Detail__c).containsKey(old.Field__c)){
                    // if found update the old record and add it to the list to be updated
                    old.Next_IO_Detail_History__c = mapDetailFieldHistory.get(old.IO_Detail__c).get(old.Field__c);
                    oldToUpdate.add(old);
                }
            }
        }
        
        // Finally update the IO_Detail_History__c records appropriate
        if(!oldToUpdate.isEmpty()){
            try {
                update oldToUpdate;
            } catch (Exception e) {
                String myerror = 'Unable to update old IO Detail History records:  ';
                System.assert(false, myerror + e);
            }
        }
    }


/* Primary method used to process IO Detail History */
    public void ioUpdateTrigger(List<IO_Detail__c> Tnew, Map<Id, IO_Detail__c> ToldMap){

        // Gather the fields to check changes for
        queryFS();
        
        // for each record check to see if the values have changed. If so create a IO_Detail_History__c record
        system.debug('Begin IO_Detail__c loop');
        listNewHistory = new List<IO_Detail_History__c>();
        for(IO_Detail__c newIO : Tnew){
            IO_Detail__c oldIO = ToldMap.get(newIO.Id);
            for(String theField:mapFieldPathToLabel.keyset()){
                if( newIO.get(theField) != oldIO.get(theField) ) {
                    // The values were changed...
                    IO_Detail_History__c newDH = new IO_Detail_History__c(
                        Changed_From__c = String.valueOf(oldIO.get(theField)),
                        Changed_To__c   = String.valueOf(newIO.get(theField)),
                        Field__c        = mapFieldPathToLabel.get(theField),
                        IO_Detail__c    = newIO.Id
                    );
                    
                    if( uniqueHistory(newDH)){
                        // only add the record to be updated if it has not already been captured.
                        listNewHistory.add(newDH);
                    }
                }
            }
        }
        system.debug('End IO_Detail__c loop');
        
        // insert the new records
        insNewHistoryRecords();
        
        // reconcile previously created IO_Detail_History__c records with those just inserted
        reconcileHistory();
    }

/* on insert simply create an empty map for "old" values */
    public void ioInsertTrigger(List<IO_Detail__c> Tnew){
        ioUpdateTrigger(Tnew, new Map<Id, IO_Detail__c>());
    }
}