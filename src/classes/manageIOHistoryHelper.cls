public Class manageIOHistoryHelper{
    /*
        Map to cache records already created on recursve Trigger updates
        For each record, for each field, what values have we created records for?
        Map < Record Id , Map < Field Name , Set< Field Value > > >
    */
    private static Map<Id,Map<String,Set<String>>> mapRecursiveCache = new Map<Id,Map<String,Set<String>>>();
    
    public static Map<Id,Map<String,Set<String>>> queryCache() {
        return mapRecursiveCache;
    }
    
    public static void setCache(Map<Id,Map<String,Set<String>>> newCache) {
        mapRecursiveCache = newCache;
    }
}