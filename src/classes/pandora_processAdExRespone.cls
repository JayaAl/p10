global class pandora_processAdExRespone
{
	
  /*
     the parameters needs to be passed in the same order. If the order of dimensions or metrics is changed in the http request, 
     then the below constructors need to be updated again to reflect the order of the metrics and dimensions returned from the response.


  */

    //ADXJSON AdExResponse=new ADXJSON();
	public static List<RowInfo> processAdExResponse(ADXJSON AdExResponseObj)
	{
      
      map<string,string> rowResponseMap= new map<string,string>();
      List<rowInfo> rowInfoList= new List<rowInfo>();
      List<ADXJSON.headers> Headers= AdExResponseObj.headers;
      List<List<string>> rows=AdExResponseObj.rows; 

        /*
                   String ADVERTISER_NAME,
                   String DFP_AD_UNIT_ID,
                   String DEAL_ID, 
                   String BRANDING_TYPE_NAME,
                   String TRANSACTION_TYPE_NAME,
                   String AD_UNIT_SIZE_NAME
                   String reportDate,
                   String AD_IMPRESSIONS, 
                   String EARNINGS, 
                   String CLICKS, 

        */

      if(Headers.size()==10){
      for(List<string> rowObj: rows)
         {
          rowInfoList.add(new rowInfo(rowObj[0],//ADVERTISER_NAME
                                      rowObj[1],//DFP_AD_UNIT_ID
                                      rowObj[2],//DEAL_ID
                                      rowObj[3],//BRANDING_TYPE_NAME
                                      rowObj[4],//TRANSACTION_TYPE_NAME
                                      rowObj[5],//AD_UNIT_SIZE_NAME 
                                      rowObj[6],//reportDate
                                      rowObj[7],//AD_IMPRESSIONS
                                      rowObj[8],//EARNINGS 
                                      rowObj[9] //CLICKS
                                      ));
         }
      }
      else if(Headers.size()==9){
      	//system.assert(false,''+Headers.size());
         for(List<string> rowObj: rows)  
         {
          rowInfoList.add(new rowInfo('',       //ADVERTISER_NAME
                                      rowObj[0],//DFP_AD_UNIT_ID
                                      rowObj[1],//DEAL_ID
                                      rowObj[2],//BRANDING_TYPE_NAME
                                      rowObj[3],//TRANSACTION_TYPE_NAME
                                      rowObj[4],//AD_UNIT_SIZE_NAME 
                                      rowObj[5],//Date
                                      rowObj[6],//AD_IMPRESSIONS
                                      rowObj[7],//EARNINGS
                                      rowObj[8] //CLICKS 
                                      ));


         }
      }
      else
      system.assert(false,'Headers Changed. Please Update the class pandora_processAdExRespone.');

      

      return rowInfoList;
	}

	
	global class rowInfo{

		public string reportDATE;
		
		public string ADVERTISER_NAME;
		public string DFP_AD_UNIT_ID; 
		public string DEAL_ID;
		public string BRANDING_TYPE_NAME;
		public string TRANSACTION_TYPE_NAME;
		public string AD_IMPRESSIONS;
		public string EARNINGS;
		public string CLICKS;
    public string AD_UNIT_SIZE_NAME;

		global rowInfo(String ADVERTISER_NAME,
                   String DFP_AD_UNIT_ID,
                   String DEAL_ID, 
                   String BRANDING_TYPE_NAME,
                   String TRANSACTION_TYPE_NAME,
                   String AD_UNIT_SIZE_NAME,
                   String reportDate,
                   String AD_IMPRESSIONS, 
                   String EARNINGS, 
                   String CLICKS
                   )
		{
     this.ADVERTISER_NAME=ADVERTISER_NAME;
		 this.DFP_AD_UNIT_ID=DFP_AD_UNIT_ID;
     this.AD_UNIT_SIZE_NAME=AD_UNIT_SIZE_NAME;
		 this.DEAL_ID=DEAL_ID;
		 this.BRANDING_TYPE_NAME=BRANDING_TYPE_NAME;
		 this.TRANSACTION_TYPE_NAME=TRANSACTION_TYPE_NAME;
		 this.AD_IMPRESSIONS=AD_IMPRESSIONS;
		 this.EARNINGS=EARNINGS;
		 this.CLICKS=CLICKS;
		 this.reportDATE=reportDate;

		}  
	}

	/*public static void assignItToOpenAuctionOpportunity()
	{ 
      Id OpportunityId= getOpenAuctionOpportunityId();
      



	}

	public static Id getOpenAuctionOpportunityId()
	{
      //get the opportunity from custom settings.

      //Currently hardcoding it.

      Id OpportunityId='006220000028FaR'; 

      return OpportunityId;

	}

	public static map<Id,List<OpportunityLineItem>> createOpportunityLineItems(Id OpportunityId, pandora_processAdExRespone.rowInfo RowInfoObj)
	{

      map<Id,List<OpportunityLineItem>> opportunityLineItems= new map<Id,List<OpportunityLineItem>>();
      opportunityLineItems.put(OpportunityId, new List<OpportunityLineItem>());

      OpportunityLineItem Oppli= new OpportunityLineItem(

      	                                                 );




      return opportunityLineItems;


	}*/
   
   //Convert the strig from 2016-09-18 to 09/18/2016
   public static Date setStringToDateFormat(String myDate) {
   	  
      system.debug('date:'+mydate);
   	  mydate=mydate.replace('-','/');
      String[] myDateOnly = myDate.split(' ');
      String[] strDate = myDateOnly[0].split('/');
      Integer myIntDate = integer.valueOf(strDate[2]);
      Integer myIntMonth = integer.valueOf(strDate[1]);
      Integer myIntYear = integer.valueOf(strDate[0]);
      Date d = Date.newInstance(myIntYear, myIntMonth, myIntDate);

       return d;
     }

}

/*
Other considerations

Limit of the result rest per sri: 72,000 record per callout.- to be confirmed.
Time out limt
Cache:



*/

/*
 Response 

 INFO|
 HEADERS(
 Headers:[name=DATE, type_Z=null], 
 Headers:[name=AD_CLIENT_ID, type_Z=null], 
 Headers:[name=AD_REQUESTS, type_Z=null], 
 Headers:[name=AD_IMPRESSIONS, type_Z=null], 
 Headers:[name=CLICKS, type_Z=null], 
 Headers:[name=EARNINGS, type_Z=null])
 

*/