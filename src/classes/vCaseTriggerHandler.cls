/**
* ─────────────────────────────────────────────────────────────────────────────────────────────────┐
* Handler class for Case Trigger actions. All case specific trigger actions should be added here
* as additional methods that are called from the routing methods (beforeInsert, beforeUpdate)
* ──────────────────────────────────────────────────────────────────────────────────────────────────
* @author         Jason West, Brian Scott, Rich Englhard
* @maintainedBy   
* @version        1.0
* @created        2018-01-10
* @modified       
* @systemLayer    Service
* ──────────────────────────────────────────────────────────────────────────────────────────────────
* @changes
* v1.1            Jaya Alaparthi
* 2018-02-27      Modified to add Class header with comments
* ──────────────────────────────────────────────────────────────────────────────────────────────────
* @changes
* v1.2            Jason West
* 2018-03-08      Modified contact auto create logic to allow failure if there is an issue creating
*                 those records.
* ──────────────────────────────────────────────────────────────────────────────────────────────────
* @changes
* v1.1            Jaya Alaparthi
* 2018-03-05      Modified to fix error caused for ticket ESS-45194
*                 Service cloud realted cases 'AMP','Artist Support','Next Big Sound','User Support'
*                 only shoudl be considered for this process. As per Mike's Slack Listener support 
*                 should not be considered for this process.
* ──────────────────────────────────────────────────────────────────────────────────────────────────
* @version          v1.2
* @modified_Date    2018/04/12 [YYYY/MM/DD]   
* @modified_By      Jaya Alaparthi 
* @modified_Purpose Case routing Issue for Service cloud
* ─────────────────────────────────────────────────────────────────────────────────────────────────┘
*/
public class vCaseTriggerHandler {
    public static Boolean EXECUTE_BEFORE_INSERT = true;
    public static Boolean EXECUTE_BEFORE_UPDATE = true;
    public static Boolean EXECUTE_AFTER_INSERT = true;
    public static Boolean EXECUTE_AFTER_UPDATE = true;
    public static Boolean COGS_BEFORE_INSERT = true;
    public static Boolean COGS_BEFORE_UPDATE = true;
    
    public list<String> ServiceCloudCaseRT = new list<String> {'AMP','Artist Support','Next Big Sound','User Support'};

    public void beforeInsert(List<Case> newList) {

        autoCreateCaseContact(newList);
    }
    
    public void beforeUpdate(Map<Id,Case> oldMap, Map<Id,Case> newMap){
        
        //v1.1
        Schema.DescribeSObjectResult objRef = Case.SObjectType.getDescribe();
        Map<Id,String> caseRecordTypesMap = new Map<Id,String>();
       
        for(Schema.RecordTypeInfo rt: objRef.getRecordTypeInfosByName().values()) {
            
            if(ServiceCloudCaseRT.contains(rt.getName())) {

                caseRecordTypesMap.put(rt.getRecordTypeId(),rt.getName());
            }   
        }
        //v1.1 end
        List<Case> assignToQueueList = new List<Case>();
        for (Id curId : newMap.keySet()) {

            Case oldCase = oldMap.get(curId);
            Case newCase = newMap.get(curId);
            
            System.debug('Old Case Priority: ' + oldCase.Priority);
            System.debug('New Case Priority: ' + newCase.Priority);
            
            if (!caseRecordTypesMap.isEmpty() 
                && caseRecordTypesMap.containsKey(oldCase.RecordTypeId)) {

                if(oldCase.Priority.equals(newCase.Priority)==false 
                    && oldCase.Status.equals(newCase.Status)
                    && (newCase.Status.equals('New')==true ||
                    String.valueOf(newCase.OwnerId).startsWith('00G')==true)) {

                    assignToQueueList.add(newCase);

                } else if(newCase.Status.equals('New') 
                        && String.valueOf(oldCase.OwnerId).startsWith('00G')
                        && String.valueOf(newCase.OwnerId).startsWith('005')) {
                    // v1.1
                    newCase.Status = 'Working';
                }
            } 

        }
                
        if (assignToQueueList.size() > 0){
            assignToQueue(assignToQueueList);
        }
        
    }

    public static void closeOpenMilestones(MAP<Id,Case> newValues,MAP<Id,Case> oldValues){
        
        SET<Id> casesWaitingOnCaseContact = new SET<Id>();
        SET<Id> closedCases = new SET<Id>();
        
        for(Case c : newValues.values()){
            
            Case oldCase = oldValues.get(c.Id);
            
            if((c.RecordTypeDeveloperName__c == 'User_Support' || c.RecordTypeDeveloperName__c == 'Next_Big_Sound' || c.RecordTypeDeveloperName__c == 'Artist_Support' || c.RecordTypeDeveloperName__c == 'AMP' ) && c.Status != oldCase.Status && c.Status == 'Waiting on Case Contact'){
                casesWaitingOnCaseContact.add(c.Id);
            } else if((c.RecordTypeDeveloperName__c == 'User_Support' || c.RecordTypeDeveloperName__c == 'Next_Big_Sound' || c.RecordTypeDeveloperName__c == 'Artist_Support' || c.RecordTypeDeveloperName__c == 'AMP' ) && c.Status != oldCase.Status && (c.Status == 'Closed' || c.Status == 'Resolved')){
                closedCases.add(c.Id);
            }
            
        }
        
        if(casesWaitingOnCaseContact != Null && casesWaitingOnCaseContact.size() > 0){
            
            LIST<CaseMilestone> waitingOnCaseContactMilestones = [SELECT Id, CaseId FROM CaseMilestone WHERE MilestoneType.Name != 'Resolution' AND CompletionDate = Null AND CaseId IN: casesWaitingOnCaseContact];
            
            if(waitingOnCaseContactMilestones != Null && waitingOnCaseContactMilestones.size() > 0){
                
                for(CaseMilestone cm : waitingOnCaseContactMilestones){
                    
                    cm.CompletionDate = system.now();
                    
                }
                
                update waitingOnCaseContactMilestones;
                
            }
        }
        
        if(closedCases != Null && closedCases.size() > 0){
            
            LIST<CaseMilestone> closedCaseMilestones = [SELECT Id, CaseId FROM CaseMilestone WHERE CompletionDate = Null AND CaseId IN: closedCases];
            
            if(closedCaseMilestones != Null && closedCaseMilestones.size() > 0){
                
                for(CaseMilestone cm : closedCaseMilestones){
                    
                    cm.CompletionDate = system.now();
                    
                }
                
                update closedCaseMilestones;
                
            }
            
        }
        
    }

    private void autoCreateCaseContact(List<Case> newList) {
        vGeneralSetting__mdt settings = VGeneralSettings.settings;
        if(!settings.EnableCaseContactAutoCreate__c) return;

        Map<ID, vCaseAutoCreateContactSetting__mdt> autoCreateSettings = VCaseAutoCreateContactSettings.settingsMap;
        Set<ID> contactRTs = VCaseAutoCreateContactSearchRTs.searchRecordTypes;

        if(autoCreateSettings.size() > 0 && contactRTs.size() > 0) {
            Set<ID> caseRTs = autoCreateSettings.keySet();

            Set<String> emails = new Set<String>();
            List<Case> casesToProcess = new List<Case>();

            for(Case cse : newList) {
                if(caseRTs.contains(cse.RecordTypeId)) {
                    if(cse.SuppliedName != null) {
                        if(cse.SuppliedEmail != null) {
                            emails.add(cse.SuppliedEmail);
                        }
                        casesToProcess.add(cse);
                    }
                }
            }

            if(casesToProcess.size() > 0) {
                Map<String, Contact> emailToContactMap = mapContacts(emails, contactRTs);
                List<Contact> contactsToInsert = new List<Contact>();
                List<Account> accountsToInsert = new List<Account>();
                List<Case> casesToReprocess = new List<Case>();

                for(Case cse : casesToProcess) {
                    Contact relatedContact = null;

                    if(cse.SuppliedEmail != null) {
                        relatedContact = emailToContactMap.get(cse.SuppliedEmail.toLowerCase());
                    }

                    if(relatedContact != null) {
                        cse.ContactId = relatedContact.Id;
                        cse.AccountId = relatedContact.AccountId;
                    } else {
                        Account acc = createAccount(cse, autoCreateSettings);
                        Contact con = createContact(cse, autoCreateSettings);

                        if(acc != null && con != null) {
                            con.Account = acc;
                            cse.Contact = con;

                            accountsToInsert.add(acc);
                            contactsToInsert.add(con);
                            casesToReprocess.add(cse);
                        }
                    }
                }

                if(casesToReprocess.size() > 0) {
                    Database.DMLOptions opts = new Database.DMLOptions();
                    opts.DuplicateRuleHeader.allowSave = true;
                    opts.optAllOrNone = true;

                    Database.insert(accountsToInsert, opts);

                    for(Contact con : contactsToInsert) {
                        con.AccountId = con.Account.Id;
                    }

                    Database.insert(contactsToInsert, opts);

                    for(Case cse : casesToReprocess) {
                        cse.ContactId = cse.Contact.Id;
                        cse.AccountId = cse.Contact.AccountId;

                        System.debug(cse.Contact);
                        System.debug(cse.ContactId);
                        System.debug(cse.AccountId);
                    }
                }
            }
        }
    }

    private Account createAccount(Case cse, Map<ID, vCaseAutoCreateContactSetting__mdt> autoCreateSettings) {
        Account acc = null;
        List<String> nameParts = VUtil.parseName(cse.SuppliedName);

        if(nameParts != null) {
            vCaseAutoCreateContactSetting__mdt settings = autoCreateSettings.get(cse.RecordTypeId);
            if(settings != null) {
                String name = nameParts[0] + ' ' + nameParts[1];

                if(cse.SuppliedEmail != null) {
                    name += ' - ' + cse.SuppliedEmail;
                }

                acc = new Account(
                    Name = name,
                    Type = settings.NewAccountType__c,
                    RecordTypeId = settings.NewAccountRTId__c
                );
            }
        }

        return acc;
    }

    private Contact createContact(Case cse, Map<ID, vCaseAutoCreateContactSetting__mdt> autoCreateSettings) {
        Contact con = null;

        List<String> nameParts = VUtil.parseName(cse.SuppliedName);

        if(nameParts != null) {
            vCaseAutoCreateContactSetting__mdt settings = autoCreateSettings.get(cse.RecordTypeId);
            if(settings != null) {
                con = new Contact(
                    FirstName = nameParts[0],
                    LastName = nameParts[1],
                    Email = cse.SuppliedEmail,
                    RecordTypeId = settings.NewContactRTId__c
                );
            }
        }

        return con;
    }

    private Map<String, Contact> mapContacts(Set<String> emails, Set<ID> contactRTs) {
        System.debug(contactRTs);

        Map<String, Contact> res = new Map<String, Contact>();

        for(Contact con : [select Id, Email, AccountId
            from Contact
            where Email in :emails
            and RecordTypeId in :contactRTs]) {
            res.put(con.Email.toLowerCase(), con);
        }

        return res;
    }
                
     private void assignToQueue(List<Case> caseList){
            
            List<vQueueMapping__c> queueMappingList = [SELECT QueueKey__c, QueueValue__c
                                                      FROM vQueueMapping__c];
            
            Map<String,Id> queueMappingMap = new Map<String,Id>();
         for (vQueueMapping__c curQM : queueMappingList){
             queueMappingMap.put(curQM.QueueKey__c,Id.valueOf(curQM.QueueValue__c));
         }
         
         if (queueMappingMap.size() > 0){
             for (Case curCase : caseList){
                 
                 String searchToken = curCase.Priority + ';' + curCase.Case_Record_Type_Name__c;
                 if (curCase.Case_Record_Type_Name__c.equals('User Support')==true){
                     searchToken += ';' + curCase.Language__c;
                 }
                 
                 if (queueMappingMap.containsKey(searchToken)==true){
                     curCase.ownerId = queueMappingMap.get(searchToken);
                 }
             }
         }
            
            
     }
    
}