/**
* ─────────────────────────────────────────────────────────────────────────────────────────────────┐
* Controller to allow file upload attachment for vCommunityCaseAttachFile
* ──────────────────────────────────────────────────────────────────────────────────────────────────
* @author         Sulung Chandra
* @maintainedBy   Sulung Chandra
* @version        1.0
* @created        2018-02-18
* @modified       
* @systemLayer    Service
* ──────────────────────────────────────────────────────────────────────────────────────────────────
* @changes
* v1.1            Sulung Chandra
* 2018-02-26
* Use custom label for the alerts
* ─────────────────────────────────────────────────────────────────────────────────────────────────┘
*/
public without sharing class vCommunityCaseAttachFileController
{
	private String parentId {get;set;}
	
	public transient Blob newAttachmentBody {get;set;}
	public String newAttachmentFilename {get;set;}
	public String newAttachmentContentType {get;set;}
	public String message {get;set;}
    public Boolean isCompleted {get;set;}
	
	public vCommunityCaseAttachFileController()
	{
		parentId = system.currentPageReference().getParameters().get('parentId');
		message = '';
        isCompleted = false;
	}
	
	/* 
		Save attachment
		
		Notes:
			- Reset the view, Set the view to Comments section
			- Load the comments
	*/
	public PageReference saveAttachment()
	{
		if(parentId == '')
		{
			message = 'Invalid parent. Please contact your administrator.';
			return null;
		}
		
		if(newAttachmentBody != null)
		{
			Attachment newAttachment = new Attachment();
			newAttachment.body = newAttachmentBody;
			newAttachment.name = newAttachmentFilename;
			newAttachment.contentType = newAttachmentContentType;
			newAttachment.parentId = parentId;
			try
			{
				insert newAttachment;
				message = newAttachmentFilename + ' ' + Label.vCommunitySupport_hasbeensuccessfullyuploaded;
                isCompleted = true;
			}
			catch(DMLException e)
			{
				//message = Label.vCommunitySupport_ErrorPleaseselectfile;
				message = e.getMessage();
				return null;
			}
		}
		else
		{
			message = Label.vCommunitySupport_ErrorPleaseselectfile;
		}
		return null;
	}
}