/**
* ─────────────────────────────────────────────────────────────────────────────────────────────────┐
* Contoller class for static methods for Community lightning components
* ──────────────────────────────────────────────────────────────────────────────────────────────────
* @author         Sulung Chandra
* @maintainedBy   Sulung Chandra
* @version        1.0
* @created        2018-01-13
* @modified       
* @systemLayer    Service
* ──────────────────────────────────────────────────────────────────────────────────────────────────
* @changes
* v1.1            Sulung Chandra
* 2018-02-20
* Update saveContactSupportCase() to collect IP Address, OS, and Browser info.
* ──────────────────────────────────────────────────────────────────────────────────────────────────
* @changes
* v1.2            Sulung Chandra
* 2018-02-28
* Add saveContactNextBigSoundCase() to save Next Big Sound case.
* ─────────────────────────────────────────────────────────────────────────────────────────────────┘
*/
public class vCommunityController 
{
    @AuraEnabled
	public static String getSessionId()
    {
        return UserInfo.getSessionId();
    }

    @AuraEnabled
    public static Knowledge__kav getArticle(String articleId) 
    {
        try 
        {
            List<Knowledge__kav> articleList = [Select Id, RecordTypeId, RecordType.Name, KnowledgeArticleId, Summary, Title, UrlName, LastPublishedDate, Body__c, Order__c From Knowledge__kav Where Id = :articleId Limit 1];
            
            if(!articleList.isEmpty())
                return articleList[0];
        } catch (Exception e) {System.debug('#### Exception: ' + e.getMessage());}
        
        return null;
    }

    @AuraEnabled
    public static Knowledge__kav getArticleByUrlName(String urlName) 
    {
        try 
        {
            List<Knowledge__kav> articleList = [Select Id, RecordTypeId, RecordType.Name, KnowledgeArticleId, Summary, Title, UrlName, LastPublishedDate, Body__c, Order__c From Knowledge__kav Where UrlName = :urlName Limit 1];
            
            if(!articleList.isEmpty())
                return articleList[0];
        } catch (Exception e) {System.debug('#### Exception: ' + e.getMessage());}
        
        return null;
    }

    @AuraEnabled
    public static NavigationalTopicObj getTopic(String topicId) 
    {
        try 
        {
            String networkId = Network.getNetworkId();
            ConnectApi.Topic topicRet = ConnectApi.Topics.getTopic(networkId, topicId);
            if(topicRet != null) { 
                return new NavigationalTopicObj(topicRet.id, topicRet.name);
            }
        } catch (Exception e) {System.debug('#### Exception: ' + e.getMessage());}
        
        return null;
    }

    /*
		Get the first navigational topic assigned to the article
	*/
    @AuraEnabled
    public static NavigationalTopicObj getArticleTopic(String articleId) 
    {
        try 
        {            
            Set<String> navigationalSet = new Set<String>();
            String networkId = Network.getNetworkId();
            
            ConnectApi.ManagedTopicCollection navigationalRet;
            if(!Test.isRunningTest()){
            	navigationalRet = ConnectApi.ManagedTopics.getManagedTopics(networkId, ConnectApi.ManagedTopicType.Navigational);
            }
            else{
                navigationalRet = new ConnectApi.ManagedTopicCollection();
                navigationalRet.managedTopics = new List<ConnectApi.ManagedTopic>();
            }
             
            if(navigationalRet != null) 
            {
                for(ConnectApi.ManagedTopic mt : navigationalRet.managedTopics) { navigationalSet.add(mt.topic.id); } 
            
                List<TopicAssignment> topicassignmentList = [Select EntityId, TopicId From TopicAssignment Where TopicId In :navigationalSet And EntityId = :articleId Limit 1];
                if(!topicassignmentList.isEmpty())
                {
                    ConnectApi.Topic topicRet = ConnectApi.Topics.getTopic(networkId, topicassignmentList[0].TopicId);
                    if(topicRet != null) { 
                        return new NavigationalTopicObj(topicRet.id, topicRet.name);
                    }
                }
            }
        } catch (Exception e) {System.debug('#### Exception: ' + e.getMessage());}
        
        return null;
    }
    
    @AuraEnabled
    public static NavigationalTopicObj getArticleTopicByUrlName(String urlName) 
    {
        Knowledge__kav kav = getArticleByUrlName(urlName);
        return getArticleTopic(kav.Id);
    }
    
    @AuraEnabled
    public static List<NavigationalTopicObj> getNavigationalTopics(String language) 
    {
        try 
        {
            List<NavigationalTopicObj> mylist = new List<NavigationalTopicObj>();
            List<TopicAssignment> topicAssignmentList = new List<TopicAssignment>();
        	Map<Id, Knowledge__kav> navigationalArticleMap = new Map<Id, Knowledge__kav>();
        	Map<Id, List<Knowledge__kav>> topicArticleListMap = new Map<Id, List<Knowledge__kav>>(); // Map<Topid Id, List<Knowledge__kav>>
        	Map<Id, Map<String, Knowledge__DataCategorySelection>> articleDCSMapMap = new Map<Id, Map<String, Knowledge__DataCategorySelection>>(); // Map<ArticleId, Map<Category Name, DCS>>
            Set<String> navigationalSet = new Set<String>();
            Set<Id> topicEntitySet = new Set<Id>();
            String publishStatus = 'Online';
            
            if(language == null || language =='')
                language = 'en_US';
            
            String networkId = Network.getNetworkId();
            ConnectApi.ManagedTopicCollection navigationalRet;
            
            if(!Test.isRunningTest()){
            	navigationalRet = ConnectApi.ManagedTopics.getManagedTopics(networkId, ConnectApi.ManagedTopicType.Navigational);
            }
            else{
                navigationalRet = new ConnectApi.ManagedTopicCollection();
                navigationalRet.managedTopics = new List<ConnectApi.ManagedTopic>();
            }
                
            
            if(navigationalRet != null) 
            { 
                
                for(ConnectApi.ManagedTopic mt : navigationalRet.managedTopics) { navigationalSet.add(mt.topic.id); } 
                
                // Collect topic assignment
                for(TopicAssignment ta : [Select EntityId, TopicId From TopicAssignment Where EntityType = 'Knowledge__kav' And TopicId In :navigationalSet And NetworkId = :networkId Limit 1000])
                {
                    topicAssignmentList.add(ta);
                    topicEntitySet.add(ta.EntityId);
                }
                
                // Collect articles
                List<String> whereList = new List<String>();
                whereList.add('PublishStatus = :publishStatus');
                whereList.add('Id In :topicEntitySet');
                
                if(!Test.isRunningTest()) // Unit test is failing. "Exception: Invalid language code. Check that the language is included in your Knowledge language settings."
                	whereList.add('Language = :language');
                
                String query = 'Select Id, RecordTypeId, KnowledgeArticleId, Summary, Title, UrlName, LastPublishedDate, Order__c From Knowledge__kav Where ' + String.join(whereList, ' And ');
            
                for(Knowledge__kav a : Database.Query(query)) { navigationalArticleMap.put(a.Id, a);}
            
                // Collect data category selection
                for(Knowledge__DataCategorySelection dcs : [Select Id, ParentId, DataCategoryName, DataCategoryGroupName From Knowledge__DataCategorySelection Where ParentId In :topicEntitySet Limit 5000])
                {
                    if(!articleDCSMapMap.containsKey(dcs.ParentId))
                        articleDCSMapMap.put(dcs.ParentId, new Map<String, Knowledge__DataCategorySelection>());
                    
                    articleDCSMapMap.get(dcs.ParentId).put(dcs.DataCategoryName, dcs);
                }
                
                // Collect navigational topic articles
                for(TopicAssignment ta : topicAssignmentList)
                {
                    if(!topicArticleListMap.containsKey(ta.TopicId))
                        topicArticleListMap.put(ta.TopicId, new List<Knowledge__kav>());
                    
                    if(navigationalArticleMap.containsKey(ta.EntityId))
                        topicArticleListMap.get(ta.TopicId).add(navigationalArticleMap.get(ta.EntityId));
                }
				
                // Collect topics with articles
                for(ConnectApi.ManagedTopic mt : navigationalRet.managedTopics)
                {
                    NavigationalTopicObj obj = new NavigationalTopicObj(mt.topic.id, mt.topic.name);
                    
                    if(topicArticleListMap.containsKey(mt.topic.id))
                    {
                        for(Knowledge__kav kav : topicArticleListMap.get(mt.topic.id))
                        {
                            ArticleObj o = new ArticleObj(kav);
                
                            if(articleDCSMapMap.containsKey(kav.Id))
                                o.hasIOSDataCategory = (articleDCSMapMap.get(kav.Id).containsKey('iOS') || articleDCSMapMap.get(kav.Id).containsKey('iPhone') || articleDCSMapMap.get(kav.Id).containsKey('iPad'));
                            
                        	obj.articles.add(o);
                        }
                    }
                    
                    // Sort
            		obj.articles.sort();
                    
                    mylist.add(obj);
                }
                
                //System.debug('#### topicAssignmentList: '+topicAssignmentList);
                //System.debug('#### topicEntitySet: '+topicEntitySet);
                //System.debug('#### navigationalSet: '+navigationalSet);
                //System.debug('#### navigationalArticleMap: '+navigationalArticleMap);
                //System.debug('#### topicArticleListMap: '+topicArticleListMap);
                //System.debug('#### mylist: '+mylist);
            }
            
            return mylist;
        } catch (Exception e) {System.debug('#### Exception: ' + e.getMessage());}
        
        return null;
    }
	
    @AuraEnabled
    public static List<ArticleObj> getTopicArticles(String topicId, String language, String sortBy) 
    {
        return getTopicArticles(topicId, language, sortBy, null, null);
    }
    
    @AuraEnabled
    public static List<ArticleObj> getTopicArticles(String topicId, String language, String sortBy, Integer maxNumberOfRecords, String excludeArticleId)
    {
        List<ArticleObj> myList = new List<ArticleObj>();
        List<Knowledge__kav> articleList = new List<Knowledge__kav>();
        Map<Id, Map<String, Knowledge__DataCategorySelection>> articleDCSMapMap = new Map<Id, Map<String, Knowledge__DataCategorySelection>>(); // Map<ArticleId, Map<Category Name, DCS>>
        //NavigationalTopicObj t = getTopic(topicId);
        String publishStatus = 'Online';
        String orderByString = 'LastPublishedDate';
        Integer recordsPerPage = 9999;
        
        if(maxNumberOfRecords != null)
            recordsPerPage = integer.valueof(maxNumberOfRecords);
        
        if(language == null || language =='')
            language = 'en_US';
        
        if(sortBy != null)
        {
            if(sortBy.equalsIgnoreCase('Recent'))
                orderByString = 'LastPublishedDate Desc';            
            else if(sortBy.equalsIgnoreCase('TitleAscending'))
                orderByString = 'Title';          
            else if(sortBy.equalsIgnoreCase('TitleDescending'))
                orderByString = 'Title Desc';
            else if(sortBy.equalsIgnoreCase('Order'))
                orderByString = 'Order__c';
        }
        
        try 
        {
            List<String> whereList = new List<String>();
            whereList.add('PublishStatus = :publishStatus');
            
            if(!Test.isRunningTest()) // Unit test is failing. "Exception: Invalid language code. Check that the language is included in your Knowledge language settings."
            	whereList.add('Language = :language');
            
            
            // whereList.add('Id In (Select EntityId From TopicAssignment Where TopicId = :topicId)'); // This throws governor limit exception
            Set<Id> assignedTopicArticleSet = new Set<Id>();
            for(TopicAssignment ta : [Select EntityId From TopicAssignment Where TopicId = :topicId Limit 1000])
            {
                assignedTopicArticleSet.add(ta.EntityId);
            }
            whereList.add('Id In :assignedTopicArticleSet');
            
            if(excludeArticleId != null)
                whereList.add('Id != :excludeArticleId');
            
            String query = 'Select Id, RecordTypeId, RecordType.Name, KnowledgeArticleId, Summary, Title, UrlName, LastPublishedDate, Order__c From Knowledge__kav Where ' + String.join(whereList, ' And ') + ' Order By ' + orderByString + ' Limit :recordsPerPage';
            
        	System.debug('#### language: '+ language);
        	System.debug('#### PublishStatus: '+ PublishStatus);
        	System.debug('#### topicId: '+ topicId);
        	System.debug('#### query: '+ query);
            
            articleList = Database.Query(query);
            
            // Collect data category selection
            for(Knowledge__DataCategorySelection dcs : [Select Id, ParentId, DataCategoryName, DataCategoryGroupName From Knowledge__DataCategorySelection Where ParentId In :articleList Limit 5000])
            {
                if(!articleDCSMapMap.containsKey(dcs.ParentId))
                    articleDCSMapMap.put(dcs.ParentId, new Map<String, Knowledge__DataCategorySelection>());
                
                articleDCSMapMap.get(dcs.ParentId).put(dcs.DataCategoryName, dcs);
            }
            
            // collect article data
            for(Knowledge__kav kav : articleList)
            {
                ArticleObj o = new ArticleObj(kav);
                
                if(articleDCSMapMap.containsKey(kav.Id))
                	o.hasIOSDataCategory = (articleDCSMapMap.get(kav.Id).containsKey('iOS') || articleDCSMapMap.get(kav.Id).containsKey('iPhone') || articleDCSMapMap.get(kav.Id).containsKey('iPad'));
                
				myList.add(o);
            }
            
        } catch (Exception e) {System.debug('#### Exception: ' + e.getMessage());}
        
        System.debug('#### articleList: '+ articleList);
        System.debug('#### myList: '+ myList);
        
        return myList;
    }
    
    @AuraEnabled
	public static List<Knowledge__kav> getSearchedArticles(String searchString, String language, String sortBy, Integer maxNumberOfRecords)
    {
        List<Knowledge__kav> articleList = new List<Knowledge__kav>();
        String publishStatus = 'Online';
        String orderByString = '';
        Integer recordsPerPage = 9999;
        
        if(maxNumberOfRecords != null)
            recordsPerPage = integer.valueof(maxNumberOfRecords);
        
        if(language == null || language =='')
            language = 'en_US';
        
        if(sortBy != null)
        {
            if(sortBy.equalsIgnoreCase('Recent'))
                orderByString = ' Order By LastPublishedDate Desc';
            else if(sortBy.equalsIgnoreCase('TitleAscending'))
                orderByString = ' Order By Title';
            else if(sortBy.equalsIgnoreCase('TitleDescending'))
                orderByString = ' Order By Title Desc';
            else if(sortBy.equalsIgnoreCase('Order'))
                orderByString = ' Order By Order__c';
        }
        
        try 
        {
            if(searchString != null && searchString.length() >0 ) 
            {
                String keyword = searchString.replace('\'','\\\'');
                String searchquery = 'FIND \'' + keyword + '*\'IN ALL FIELDS RETURNING Knowledge__kav(Id, RecordTypeId, RecordType.Name, KnowledgeArticleId, ArticleNumber, Summary, Title, UrlName, LastPublishedDate, Order__c where PublishStatus =\'' + publishStatus + '\' and Language = \'' + language + '\' ' + orderByString + ') Limit :recordsPerPage';
                List<List<SObject>>searchList = search.query(searchquery);
                articleList = (List<Knowledge__kav>)searchList[0];
                System.debug('### searchstring. query: ' + searchquery);
            }
            else
            {
                String qryString = 'SELECT Id, RecordTypeId, RecordType.Name, KnowledgeArticleId, ArticleNumber, Summary, Title, UrlName, LastPublishedDate, Order__c FROM Knowledge__kav WHERE (PublishStatus = \'' + publishStatus + '\' and Language = \'' + language + '\') ' + orderByString + ' Limit :recordsPerPage';
                articleList = Database.query(qryString);
                System.debug('### qryString. query: ' + qryString);
            }
        } catch (Exception e) {System.debug('#### Exception: ' + e.getMessage());}
        
        System.debug('#### articleList: '+ articleList);
        
        return articleList;
    }
    
    @AuraEnabled
	public static String saveCaseDeflection(String articleId, String sessionId, String language, String searchString, Boolean deflected)
    {
        String ret;
        
        Savepoint sp = Database.setSavepoint();
        
        try
        {
            List<Knowledge__kav> articleList = [Select Id, Title From Knowledge__kav Where Id = :articleId Limit 1];
            
            vCaseDeflection__c cd = new vCaseDeflection__c();
            cd.ArticleId__c = articleId;
            cd.SessionId__c = sessionId;
            cd.Language__c = language;
            cd.SearchString__c = searchString;
            cd.Deflected__c = deflected;
            cd.NetworkId__c = Network.getNetworkId();
            
            if(!articleList.isEmpty())
                cd.ArticleTitle__c = articleList[0].Title;
            
            insert cd;
            
            ret = cd.Id;
        }
        catch(DmlException de){Database.rollback(sp); System.debug('#### DmlException: '+de.getMessage()); ret = de.getMessage();}
        
        return ret;
    }
    
    @AuraEnabled
	public static String updateCaseDeflection(String caseDeflectionId, String caseId)
    {
        String ret;
        
        Savepoint sp = Database.setSavepoint();
        
        try
        {
            List<vCaseDeflection__c> caseDeflectionList = [Select Id, Case__c From vCaseDeflection__c Where Id = :caseDeflectionId Limit 1];
            
            if(!caseDeflectionList.isEmpty())
            {
                caseDeflectionList[0].Case__c = caseId;
                update caseDeflectionList;
                
            
                ret = caseDeflectionList[0].Id;
            }
        }
        catch(DmlException de){Database.rollback(sp); System.debug('#### DmlException: '+de.getMessage()); ret = de.getMessage();}
        
        return ret;
    }
    
    @AuraEnabled
	public static String saveContactSupportCase(String recordTypeId, String channel, String subject, String description, String suppliedName, String suppliedEmail, String pandoraAccountEmail, String reason, String subreason, String device, String ipAddress, String osName, String browser)
    {
        String ret;
        
        Savepoint sp = Database.setSavepoint();
        
        try
        {
            Case c = new Case();
            
            if(recordTypeId != null && recordTypeId != '')
                c.RecordTypeId = recordTypeId;
            
            c.Origin = 'Web';
            c.Status = 'New';
            c.Channel__c = channel;
            c.Subject = subject;
            c.Description = description;
            c.SuppliedName = suppliedName;
            c.SuppliedEmail = suppliedEmail;
            c.Pandora_Account_Email__c = pandoraAccountEmail;
            c.Reason = reason;
            c.Sub_Reason__c = subreason;
            c.Device__c = device;
            c.IP__c = ipAddress;
            c.OS__c = osName;
            c.Browser__c = browser;
            insert c;
            
            ret = c.Id;
        }
        catch(DmlException de){Database.rollback(sp); System.debug('#### DmlException: '+de.getMessage()); ret = de.getMessage();}
        
        return ret;
    }
    
    @AuraEnabled
	public static String saveContactNextBigSoundCase(String recordTypeId, String channel, String subject, String description, String suppliedName, String suppliedEmail, String ipAddress, String osName, String browser)
    {
        String ret;
        
        Savepoint sp = Database.setSavepoint();
        
        try
        {
            Case c = new Case();
            
            if(recordTypeId != null && recordTypeId != '')
                c.RecordTypeId = recordTypeId;
            
            c.Origin = 'Web';
            c.Status = 'New';
            c.Channel__c = channel;
            c.Subject = subject;
            c.Description = description;
            c.SuppliedName = suppliedName;
            c.SuppliedEmail = suppliedEmail;
            c.IP__c = ipAddress;
            c.OS__c = osName;
            c.Browser__c = browser;
            insert c;
            
            ret = c.Id;
        }
        catch(DmlException de){Database.rollback(sp); System.debug('#### DmlException: '+de.getMessage()); ret = de.getMessage();}
        
        return ret;
    }
    
    @AuraEnabled
    public static String uploadCaseFile(String caseId, String fileName, String fileBase64Data, String contentType) 
    { 
        System.debug('#### caseId: '+caseId);
        System.debug('#### fileName: '+fileName);
        System.debug('#### fileBase64Data: '+fileBase64Data);
        System.debug('#### contentType: '+contentType);
        
        String caseFileId = '';
        fileBase64Data = EncodingUtil.urlDecode(fileBase64Data, 'UTF-8');
        
        Attachment newAttachment = new Attachment();
        newAttachment.body = EncodingUtil.base64Decode(fileBase64Data);
        newAttachment.name = fileName;
        newAttachment.contentType = contentType;
        newAttachment.parentId = caseId;
        try
        {
            insert newAttachment;
            caseFileId = newAttachment.Id;
        }
        catch(DMLException e)
        {
             System.debug('#### Exception: '+e.getMessage());
            caseFileId = e.getMessage();
        }
        
        return caseFileId;
    }
    
	@AuraEnabled
    public static List<SelectOptionDetail> getCaseReasonSelectOptions()
    {
        List<SelectOptionDetail> mylist = new List<SelectOptionDetail>();
        Set<String> restrictedValueSet = new Set<String>();
        Boolean restrictionEnforced = false;
        
        List<vCommunityCaseFieldValue__mdt> mdtList = [Select Id, Reasons__c, RecordTypeDeveloperName__c From vCommunityCaseFieldValue__mdt Where DeveloperName = 'User_Support' And RestrictValueEnforced__c = true Limit 1];
        if(!mdtList.isEmpty())
        {
            restrictionEnforced = true;
            
            String reasons = '';
            if(mdtList[0].Reasons__c != null)
            	reasons = mdtList[0].Reasons__c;
            
            for(String reason : reasons.trim().split(';'))
            {
                restrictedValueSet.add(reason.trim());
            }
        }
        
        Schema.DescribeFieldResult fr = Case.Reason.getDescribe();
        List<Schema.PicklistEntry> pleList = fr.getPicklistValues();
			
        for(Schema.PicklistEntry ple : pleList)
        {
            if(!restrictionEnforced || restrictedValueSet.contains(ple.getValue()))
                mylist.add(new SelectOptionDetail(ple.getLabel(), ple.getValue()));           
        }
            
        return mylist;
    }
    
    @AuraEnabled
    public static List<SelectOptionDetail> getCaseSubReasonSelectOptions(String reason)
    {
        List<SelectOptionDetail> mylist = new List<SelectOptionDetail>();
        Map<String,Map<String, String>> valueMap = vDependentPicklistController.GetDependentOptions('Case','Reason','Sub_Reason__c');
        
        if(valueMap.containsKey(reason))
        {
            Map<String, String> subreasonMap = valueMap.get(reason);
            for(String s : subreasonMap.keySet())
            {
                if(subreasonMap.containsKey(s))
                {
                    mylist.add(new SelectOptionDetail(subreasonMap.get(s), s));
                }
            }
        }
        
        return mylist;
    }
    
	@AuraEnabled
    public static List<SelectOptionDetail> getCaseDeviceSelectOptions()
    {
        List<SelectOptionDetail> mylist = new List<SelectOptionDetail>();
        
        Schema.DescribeFieldResult fr = Case.Device__c.getDescribe();
        List<Schema.PicklistEntry> pleList = fr.getPicklistValues();
			
        for(Schema.PicklistEntry ple : pleList)
        {
            mylist.add(new SelectOptionDetail(ple.getLabel(), ple.getValue()));
        }
            
        return mylist;
    }
	
    public class NavigationalTopicObj
    {
        @AuraEnabled
        public Id topicId {get;set;}
        
        @AuraEnabled
        public String topicName {get;set;}
        
        @AuraEnabled
        public List<ArticleObj> articles {get;set;}
        
        public navigationalTopicObj(String tId, String tName)
        {
            topicId = tId;
            topicName = tName;
            articles = new List<ArticleObj>();
        }
    }
	
    public class ArticleObj implements Comparable
    {
        @AuraEnabled
        public Knowledge__kav kav {get;set;}
        
        @AuraEnabled
        public Boolean hasIOSDataCategory {get;set;}
        
        public articleObj(Knowledge__kav k)
        {
            kav = k;
            hasIOSDataCategory = false;
        }
        
        public Integer CompareTo(Object compareTo)
        {
        	ArticleObj compareToObj = (ArticleObj)compareTo;
        	
        	Integer ret = 0;
        	
        	if(this.kav.Order__c > compareToObj.kav.Order__c)
        	{
        		ret = 1;
        	}
        	else if(this.kav.Order__c < compareToObj.kav.Order__c)
        	{
        		ret = -1;
        	}
        	
        	return ret;
        }
    }
    
    /*Select Option class with Aura Enabled properties*/
    public class SelectOptionDetail{
        @AuraEnabled
        public String name {get;set;}
		
        @AuraEnabled
        public String value {get;set;}
		
		public selectOptionDetail(String name, String value){
			this.name = name;
			this.value = value;
		}
    }
}