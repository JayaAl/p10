/**
* ─────────────────────────────────────────────────────────────────────────────────────────────────┐
* Test class for static methods for Community Controllers
* ──────────────────────────────────────────────────────────────────────────────────────────────────
* @author         Sulung Chandra
* @maintainedBy   Sulung Chandra
* @version        1.0
* @created        2018-01-13
* @modified       
* @systemLayer    Service
* ──────────────────────────────────────────────────────────────────────────────────────────────────
* @changes
* v1.1            Sulung Chandra
* 2018-02-18
* Add test method for vCommunityCaseAttachFileController
* ──────────────────────────────────────────────────────────────────────────────────────────────────
* @changes
* v1.1            Sulung Chandra
* 2018-02-28
* Add test method for vCommunityController.saveContactNextBigSoundCase()
* ─────────────────────────────────────────────────────────────────────────────────────────────────┘
*/
@isTest(SeeAllData=true)
private class vCommunityControllerTest 
{
	@IsTest(SeeAllData=false) 
    public static void testTopicAndArticle() 
    {
        String networkId = Network.getNetworkId();
        Boolean createManageTopic = false;
        
        List<Network> networkList = [Select Id, Name From Network Where Name='Support' Limit 1];
        if(!networkList.isEmpty())
        {
            createManageTopic = true;
            networkId = networkList[0].Id;
        }
        
        // Create new topic
        Topic t = new Topic();
        t.Name = 'TEST NAV TOPIC 18921982912';
        t.NetworkId = networkId;
        insert t;
        
        // Create new article
        Knowledge__kav a = new Knowledge__kav();
        a.Title = 'Test article 121212';
        a.UrlName = 'Test-article-121212';
        a.Body__c = 'Test';
        a.Language = 'en_US';
        insert a;
        
        Knowledge__kav a2 = new Knowledge__kav();
        a2.Title = 'Test article 98329482';
        a2.UrlName = 'Test-article-98329482';
        a2.Body__c = 'Test';
        a2.Language = 'en_US';
        insert a2;
        
        List<Knowledge__kav> kavList = [Select Id, KnowledgeArticleId From Knowledge__kav Where Id = :a.Id];
        
        KbManagement.PublishingService.publishArticle(kavList[0].KnowledgeArticleId, true);
        
        // Associate topic to article
        TopicAssignment ta1 = new TopicAssignment();
        ta1.EntityId = a.Id;
        ta1.TopicId = t.Id;
        insert ta1;
        
        test.startTest();
        
        // Test obj compare / sort
        List<vCommunityController.ArticleObj> aobjlist = new List<vCommunityController.ArticleObj>();
        aobjlist.add(new vCommunityController.ArticleObj(a));
        aobjlist.add(new vCommunityController.ArticleObj(a2));
        aobjlist.sort();
        
        
        // Insert featured topic
        if(createManageTopic)
            ConnectApi.ManagedTopics.createManagedTopic(networkId, t.Id, ConnectApi.ManagedTopicType.Navigational);
        
        
        vCommunityController.NavigationalTopicObj topicret = vCommunityController.getTopic(t.Id);
        vCommunityController.NavigationalTopicObj topicret2 = vCommunityController.getArticleTopic(a.Id);
        vCommunityController.NavigationalTopicObj topicret3 = vCommunityController.getArticleTopicByUrlName(a.UrlName);
        List<vCommunityController.NavigationalTopicObj> navtopicList = vCommunityController.getNavigationalTopics(t.Id);
        
        Knowledge__kav kav = vCommunityController.getArticle(a.Id);
        Knowledge__kav kav2 = vCommunityController.getArticleByUrlName(a.UrlName);
        List<vCommunityController.ArticleObj> topicArticleList = vCommunityController.getTopicArticles(t.Id, 'en_US', 'Recent');
        List<Knowledge__kav> searchArticleList = vCommunityController.getSearchedArticles('test', 'en_US', 'Recent', 20);
        
        Test.stopTest();
    }
    
	@IsTest(SeeAllData=true) 
    public static void testTopicAndArticleWithExistingData() 
    {
        Id networkId = Network.getNetworkId();
        
        List<Network> networkList = [Select Id, Name From Network Where Name='Support' Limit 1];
        if(!networkList.isEmpty())
            networkId = networkList[0].Id;
        
        test.startTest();
        
        List<TopicAssignment> topicAssignmentList = [Select Id, TopicId, EntityId From TopicAssignment Where NetworkId = :networkId And EntityType = 'Knowledge__kav' Limit 1];
        if(!topicAssignmentList.isEmpty())
        {
            vCommunityController.NavigationalTopicObj topicret = vCommunityController.getTopic(topicAssignmentList[0].TopicId);
            vCommunityController.NavigationalTopicObj topicret2 = vCommunityController.getArticleTopic(topicAssignmentList[0].EntityId);
            List<vCommunityController.NavigationalTopicObj> navtopicList = vCommunityController.getNavigationalTopics(topicAssignmentList[0].TopicId);
            
            Knowledge__kav kav = vCommunityController.getArticle(topicAssignmentList[0].EntityId);
            List<vCommunityController.ArticleObj> topicArticleList = vCommunityController.getTopicArticles(topicAssignmentList[0].TopicId, '', 'Recent', 20, '');
            List<vCommunityController.ArticleObj> topicArticleList2 = vCommunityController.getTopicArticles(topicAssignmentList[0].TopicId, 'en_US', 'TitleAscending');
            List<vCommunityController.ArticleObj> topicArticleList3 = vCommunityController.getTopicArticles(topicAssignmentList[0].TopicId, 'en_US', 'TitleDescending');
            List<Knowledge__kav> searchArticleList = vCommunityController.getSearchedArticles('test', 'en_US', 'Recent', 20);
            List<Knowledge__kav> searchArticleList2 = vCommunityController.getSearchedArticles('test', 'en_US', 'TitleAscending', 20);
            List<Knowledge__kav> searchArticleList3 = vCommunityController.getSearchedArticles('test', 'en_US', 'TitleDescending', 20);
            
            vCommunityController.NavigationalTopicObj navtopicobj = new vCommunityController.NavigationalTopicObj(topicAssignmentList[0].TopicId, 'Test');
        }
        
        Test.stopTest();
    }
    
    @IsTest(SeeAllData=false) 
    public static void testNewSupportCase() 
    {
        String recordTypeId = '';
        String channel = 'Support';
        String subject = 'Test case 192018210';
        String description = 'test';
        String suppliedName = 'Test sc19829121';
        String suppliedEmail = 'test12321@veltig.com';
        String pandoraAccountEmail = 'test_12321@veltig.com';
        String reason = 'Troubleshooting';
        String subreason = 'Crashes';
        String device = 'Computer';
        String ipAddress = '';
        String osName = '';
        String browser = '';
        
        List<vCommunityController.SelectOptionDetail> reasonOptionList = vCommunityController.getCaseReasonSelectOptions();
        if(!reasonOptionList.isEmpty())
            reason = reasonOptionList[0].value;
        
        List<vCommunityController.SelectOptionDetail> subreasonOptionList = vCommunityController.getCaseSubReasonSelectOptions(reason);
        if(!subreasonOptionList.isEmpty())
            subreason = subreasonOptionList[0].value;
        
        List<vCommunityController.SelectOptionDetail> deviceOptionList = vCommunityController.getCaseDeviceSelectOptions();
        if(!deviceOptionList.isEmpty())
            device = deviceOptionList[0].value;
        
        String caseId = vCommunityController.saveContactSupportCase(recordTypeId, channel, subject, description, suppliedName, suppliedEmail, pandoraAccountEmail, reason, subreason, device, ipAddress, osName, browser);
        
        // Create new article
        Knowledge__kav a = new Knowledge__kav();
        a.Title = 'Test article 121212';
        a.UrlName = 'Test-article-121212';
        a.Details__c = 'Test';
        a.Language = 'en_US';
        insert a;
        
        String sessionId = vCommunityController.getSessionId();
        String language = 'en_US';
        String searchString = 'test';
        
        String caseDeflectionId = vCommunityController.saveCaseDeflection(a.Id, sessionId, language, searchString, false);
        vCommunityController.updateCaseDeflection(caseDeflectionId, caseId);
    }
    
    @IsTest(SeeAllData=false) 
    public static void testNewNextBigSoundCase() 
    {
        String recordTypeId = '';
        String channel = 'Support';
        String subject = 'Test case 192018210';
        String description = 'test';
        String suppliedName = 'Test sc19829121';
        String suppliedEmail = 'test12321@veltig.com';
        String ipAddress = '';
        String osName = '';
        String browser = '';
        
        String caseId = vCommunityController.saveContactNextBigSoundCase(recordTypeId, channel, subject, description, suppliedName, suppliedEmail, ipAddress, osName, browser);
    }
    
    @IsTest(SeeAllData=false) 
    public static void testUserIpController() 
    {
        PageReference pageref = Page.vUserIp;
		test.setCurrentPage(pageref);
    	
    	vUserIpController ctrl = new vUserIpController();
    
        test.startTest();
        
        ctrl.getIpAddress();
    	
    	test.stopTest();
    }
    
    @IsTest(SeeAllData=false) 
    public static void testCommunityCaseAttachFileController() 
    {
    	PageReference pageref = Page.vCommunityCaseAttachFile;
		pageref.getParameters().put('parentid', UserInfo.getUserId());
		test.setCurrentPage(pageref);
    	
    	vCommunityCaseAttachFileController ctrl = new vCommunityCaseAttachFileController();
    	ctrl.newAttachmentBody = Blob.valueOf('Unit Test Attachment Body');
    	ctrl.newAttachmentFilename = 'Test Attachment Filename';
    	ctrl.newAttachmentContentType = 'image/JPEG';
        
        test.startTest();
        
    	System.assertEquals(ctrl.saveAttachment(),null); 
    	
    	test.stopTest();
    }
}