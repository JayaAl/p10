/**
* ─────────────────────────────────────────────────────────────────────────────────────────────────┐
* Test class for vDependentPicklistController
* ──────────────────────────────────────────────────────────────────────────────────────────────────
* @author         Sulung Chandra
* @maintainedBy   Sulung Chandra
* @version        1.0
* @created        2018-01-28
* @modified       
* @systemLayer    Service
* ─────────────────────────────────────────────────────────────────────────────────────────────────┘
*/
@isTest
public class vDependentPicklistControllerTest 
{ 
    static testMethod void getDependentOptionsImplTest()
    {
        Map<String, Map<String, String>> valueMap = VDependentPicklistController.GetDependentOptions('Account','ShippingCountryCode','ShippingStateCode');
        for(String contr : valueMap.keySet())
        {
            System.debug('CONTROLLING FIELD : ' + contr);
            System.debug('DEPENDENT VALUES ...  : ' + valueMap.get(contr));
        }
    }
}