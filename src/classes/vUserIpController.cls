/**
* ─────────────────────────────────────────────────────────────────────────────────────────────────┐
* Controller to load IP Address for vUserIp
* ──────────────────────────────────────────────────────────────────────────────────────────────────
* @author         Sulung Chandra
* @maintainedBy   Sulung Chandra
* @version        1.0
* @created        2018-02-22
* @modified       
* @systemLayer    Service
* ─────────────────────────────────────────────────────────────────────────────────────────────────┘
*/
public class vUserIpController 
{
	public vUserIpController() 
    {
        
    }
    
    public String getIpAddress()
    {
        string ret = ApexPages.currentPage().getHeaders().get('True-Client-IP');
                
        if (ret == '' || ret == null) 
        {
            ret = ApexPages.currentPage().getHeaders().get('X-Salesforce-SIP');
        } // get IP address when no caching (sandbox, dev, secure urls)
        
        if (ret == '' || ret == null) 
        {
            ret = ApexPages.currentPage().getHeaders().get('X-Forwarded-For');
        } // get IP address from standard header if proxy in use
                
        system.debug('#### IP Address: ' + ret);
            
        return ret;
    }
}