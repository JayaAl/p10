<apex:component controller="batchJobs" >
    <apex:attribute name="numberOfJobs" type="Integer" assignTo="{!numberOfJobs}" description="The number of batch jobs to display in the table."/>
 
    <!-- Here is the css styles that will be used for the progress bars -->
    <style>
        .progressBar{
            background-color: #f8f8f8;
            border:1px solid #DDDDDD;
            height: 19px;
            width: 300px;
            -moz-border-radius: 5px; 
            -webkit-border-radius: 5px;
        }
        .progress{
            background-color: Red;
            border:1px solid #E78F08;
            height: 100%;
            margin: -1px;
            text-align: center;
            -moz-border-radius: 5px; 
            -webkit-border-radius: 5px;
            line-height: 18px;
        }
        
        .progressComplete{
            background-color: Green;
            border:1px solid #E78F08;
            height: 100%;
            margin: -1px;
            text-align: center;
            -moz-border-radius: 5px; 
            -webkit-border-radius: 5px;
            line-height: 18px;
        }

    </style>
 
    <!-- This action poller will check the status of the batch jobs every 5 seconds -->
    <!--<apex:actionPoller rerender="jobs" interval="5"/>-->
 
    <apex:pageBlock title="Batch Apex Jobs">
        <apex:pageBlockTable value="{!batchJobs}" var="b" id="jobs" rendered="{!batchJobs.size > 0}">
            <apex:column headerValue="Action">
            <apex:commandButton value="Abort Job" action="{!abortJob}" disabled="{!b.job.Status == 'Completed'}" rerender="jobs">
                <apex:param name="jobId" value="{!b.job.Id}" assignTo="{!abortJobId}"/>
            </apex:commandButton>
            </apex:column>
            <apex:column headerValue="Apex Class" value="{!b.job.ApexClass.Name}"/>
            <apex:column value="{!b.job.CreatedDate}"/>
            <apex:column value="{!b.job.CreatedById}"/>
            <apex:column value="{!b.job.Status}"/>
            <apex:column width="320px" headerValue="Current Progress" >
 
                <!-- Here with have two divs that construct our progresses bar. An outter which is the entire bar,
                and and inner that represents the percent complete. We simply pass the percentComplete value to
                the inner div width and this will show how far along the job is. Brilliant! -->
             <apex:outputpanel rendered="{!b.percentComplete != 100}" > 
                    <div class="progressBar">
                    <div class="progress" style="width: {!b.percentComplete}%;">
                        {!b.percentComplete}%
                    </div>
                </div>
                
             </apex:outputpanel>
             
            <apex:outputpanel rendered="{!b.percentComplete == 100}" > 
                    <div class="progressBar">
                    <div class="progressComplete" style="width: {!b.percentComplete}%;">
                        {!b.percentComplete}%
                    </div>
                </div>
                
             </apex:outputpanel> 
            </apex:column>
            <apex:column value="{!b.job.CompletedDate}"/>
        </apex:pageBlockTable>
      <apex:outputPanel rendered="{!batchJobs.size == 0}" style="color:Red">
      <center><strong>Sorry, no Jobs found!</strong></center>
      </apex:outputPanel>        
    </apex:pageBlock>
 
</apex:component>