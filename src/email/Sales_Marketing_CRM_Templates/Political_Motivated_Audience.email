<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
	<meta charset="utf-8"> <!-- utf-8 works for most cases -->
	<meta name="viewport" content="width=device-width"> <!-- Forcing initial-scale shouldn't be necessary -->
	<meta http-equiv="X-UA-Compatible" content="IE=edge"> <!-- Use the latest (edge) version of IE rendering engine -->
	<title>A MOTIVATED AUDIENCE</title> <!-- The title tag shows in email notifications, like Android 4.4. -->

	<!-- Web Font / @font-face : BEGIN -->
	<!-- NOTE: If web fonts are not required, lines 9 - 26 can be safely removed. -->
	
	<!-- Desktop Outlook chokes on web font references and defaults to Times New Roman, so we force a safe fallback font. -->
	<!--[if mso]>
		<style>
			* {
				font-family: sans-serif !important;
			}
		</style>
	<![endif]-->
	
	<!-- All other clients get the webfont reference; some will render the font and others will silently fail to the fallbacks. More on that here: http://stylecampaign.com/blog/2015/02/webfont-support-in-email/ -->
	<!--[if !mso]><!-->
		<!-- insert web font reference, eg: <link href='https://fonts.googleapis.com/css?family=Roboto:400,700' rel='stylesheet' type='text/css'> -->
	<!--<![endif]-->

	<!-- Web Font / @font-face : END -->
	
  	<!-- CSS Reset -->
    <style type="text/css">

		/* What it does: Remove spaces around the email design added by some email clients. */
		/* Beware: It can remove the padding / margin and add a background color to the compose a reply window. */
        html,
        body {
	        margin: 0 auto !important;
            padding: 0 !important;
            height: 100% !important;
            width: 100% !important;
        }
        
        /* What it does: Stops email clients resizing small text. */
        * {
            -ms-text-size-adjust: 100%;
            -webkit-text-size-adjust: 100%;
        }
        
        /* What is does: Centers email on Android 4.4 */
        div[style*="margin: 16px 0"] {
            margin:0 !important;
        }
        
        /* What it does: Stops Outlook from adding extra spacing to tables. */
        table,
        td {
            mso-table-lspace: 0pt !important;
            mso-table-rspace: 0pt !important;
        }
                
        /* What it does: Fixes webkit padding issue. Fix for Yahoo mail table alignment bug. Applies table-layout to the first 2 tables then removes for anything nested deeper. */
        table {
            border-spacing: 0 !important;
            border-collapse: collapse !important;
            table-layout: fixed !important;
            Margin: 0 auto !important;
        }
        table table table {
            table-layout: auto; 
        }
        
        /* What it does: Uses a better rendering method when resizing images in IE. */
        img {
            -ms-interpolation-mode:bicubic;
        }
        
        /* What it does: Overrides styles added when Yahoo's auto-senses a link. */
        .yshortcuts a {
            border-bottom: none !important;
        }
        
        /* What it does: A work-around for iOS meddling in triggered links. */
        .mobile-link--footer a,
        a[x-apple-data-detectors] {
            color:inherit !important;
            text-decoration: underline !important;
        }
      
    </style>
    
    <!-- Progressive Enhancements -->
    <style>
        
        /* What it does: Hover styles for buttons */
        .button-td,
        .button-a {
            transition: all 100ms ease-in;
        }
        .button-td:hover,
        .button-a:hover {
            background: #555555 !important;
            border-color: #555555 !important;
        }

    </style>

</head>
<body width="100%" height="100%" bgcolor="#ffffff" style="Margin: 0;">
<table cellpadding="0" cellspacing="0" border="0" height="100%" width="100%" bgcolor="#f5f5f5" style="border-collapse:collapse; padding-bottom:40px; padding-top: 10px"><tr><td valign="top">
    <center style="width: 100%;">

        <!-- Visually Hidden Preheader Text : BEGIN -->
        <div style="display:none;font-size:1px;line-height:1px;max-height:0px;max-width:0px;opacity:0;overflow:hidden;mso-hide:all;font-family: sans-serif;">
            A MOTIVATED AUDIENCE
        </div>
        <!-- Visually Hidden Preheader Text : END -->

        <!--    
            Set the email width. Defined in two places:
            1. max-width for all clients set with Outlook, allowing the email to squish on narrow but never go wider than 600px.
            2. MSO tags for Desktop Windows Outlook enforce a 600px width.
        -->
        <div style="max-width: 600px;">
            <!--[if (gte mso 9)|(IE)]>
            <table cellspacing="0" cellpadding="0" border="0" width="600" align="center">
            <tr>
            <td>
            <![endif]-->

            <!-- Email Header : BEGIN -->
      <!--      <table cellspacing="0" cellpadding="0" border="0" align="center" width="100%" style="max-width: 600px;">
				<tr>
					<td style="padding: 20px 0; text-align: center">
						<img src="http://placehold.it/200x50" width="200" height="50" alt="alt_text" border="0">
					</td>
				</tr>
            </table>-->
            <!-- Email Header : END -->
            
            <!-- Email Body : BEGIN -->
            <table cellspacing="0" cellpadding="0" border="0" align="center" bgcolor="#ffffff" width="600" style="max-width: 600px;">
                
                <!-- Hero Image, Flush : BEGIN -->
                <tr>
					<td>
                        <img src="https://pandoraadvertising.files.wordpress.com/2016/10/1441_polaud_header_r2.jpg" alt="When Pandora Plays, Your Message Works" width="600" height="" style="display: block; border: 0px; outline: none; width: 100%; height: auto; max-width: 600px; margin-left: auto; margin-right: auto;">
                    </td>
                </tr>
                
                
                
                
                <!-- Hero Image, Flush : END -->

                <!-- 1 Column Text + Button : BEGIN -->
                <tr width="100%">
                    <td>
                        <table cellspacing="0" cellpadding="0" border="0" width="100%">
                            <tr>
                                <td  style="padding:0% 10% 0% 10%; font-family: sans-serif; font-size: 2em; letter-spacing: 4px; line-height:1.2em; mso-height-rule: exactly; text-align:center; font-weight: bold; color: #000000; background-color: #ffffff">
                                    A MOTIVATED AUDIENCE
                                </td>
                            </tr>
                            <tr>
                                <td  style="padding:2% 8% 5% 8%; font-family: baskerville, serif; font-size: 1.5em; text-align:center; mso-height-rule: exactly; line-height: 1.7em; color: #4b4b4b;">
                                    Pandora listeners speak out
                                </td>
                            </tr>
                            <tr>
                                <td  style="padding:2% 5% 5% 5%; font-family: baskerville, serif; font-size: 1.1em; text-align:center; mso-height-rule: exactly; line-height: 1.7em; color: #4b4b4b;">
                                    Is Europe’s “The Final Countdown” playing in your head? With just a few weeks away from Election Day, Pandora listeners are more motivated than ever to cast their votes. How do we know this? Pandora recently conducted an Election 2016 listener study to take the political pulse of our 83 million<sup>1</sup> voting age listeners. As we enter the final days of the election, here are the results of our Pandora political listener survey.
                                </td>
                            </tr>
                            
                            <tr>
                                <td>
                                    <img src="https://pandoraadvertising.files.wordpress.com/2016/10/1441_yesvoting_r4.jpg" width="600" height="" alt="yes, I'm Voting" width="600" height="" style="display: block; border: 0px; outline: none; width: 100%; height: auto; max-width: 600px; margin-left: auto; margin-right: auto;">
                                </td>
                            </tr>
                            
                            <tr>
                                <td  style="padding:0% 8% 0% 8%; font-family: sans-serif; font-size: 1.6em; letter-spacing: 4px; line-height:1.2em; mso-height-rule: exactly; text-align:center; font-weight: bolder; color: #000000; background-color: #ffffff">
                                    GOING TO THE POLLS ELECTION DAY<sup>3</sup>
                                </td>
                            </tr>
                            <tr>
                                <td  style="padding:2% 8% 5% 8%; font-family: baskerville, serif; font-size: 1.1em; text-align:center; mso-height-rule: exactly; line-height: 1.7em; color: #000000; background-color">
                                    Pandora listeners still prefer going to the polls over voting by mail.   
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <img src="https://pandoraadvertising.files.wordpress.com/2016/10/1441_polls_r2.jpg" width="600" height="" alt="Pandora listeners prefer to go to the polls in person" width="600" height="" style="display: block; border: 0px; outline: none; width: 100%; height: auto; max-width: 600px; margin-left: auto; margin-right: auto;">
                                </td>
                            </tr>
                            
                            
                            <tr>
                                <td  style="padding:5% 10% 0% 10%; font-family: sans-serif; font-size: 1.6em; letter-spacing: 4px; line-height:1.2em; mso-height-rule: exactly; text-align:center; font-weight: bolder; color: #000000; background-color: #ffffff; border-top: solid 3px">
                                    THE PRESIDENTIAL RACE
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <img src="https://pandoraadvertising.files.wordpress.com/2016/10/1441_presrace_r3.jpg" width="600" height="" alt="13% of Pandora’s voting age audience is still undecided going into November" width="600" height="" style="display: block; border: 0px; outline: none; width: 100%; height: auto; max-width: 600px; margin-left: auto; margin-right: auto;">
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <img src="https://pandoraadvertising.files.wordpress.com/2016/10/1441_reasonvote_r2.jpg" width="600" height="" alt="The reason to vote: 1 in 2 listeners say they’re voting to “make sure the ‘other’ candidate doesn’t win" width="600" height="" style="display: block; border: 0px; outline: none; width: 100%; height: auto; max-width: 600px; margin-left: auto; margin-right: auto;">
                                </td>
                            </tr>
                            
                            <tr>
                                <td  style="padding:0% 8% 0% 8%; font-family: sans-serif; font-size: 1.6em; letter-spacing: 4px; line-height:1.2em; mso-height-rule: exactly; text-align:center; font-weight: bolder; color: #000000; background-color: #ffffff">
                                    ISSUES DRIVING THE ELECTION
                                </td>
                            </tr>
                            <tr>
                                <td  style="padding:2% 8% 5% 8%; font-family: baskerville, serif; font-size: 1.1em; text-align:center; mso-height-rule: exactly; line-height: 1.7em; color: #000000; background-color">
                                    Economy, Education, Healthcare, and Jobs are the most important issues for Pandora listeners.   
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <img src="https://pandoraadvertising.files.wordpress.com/2016/10/1441_issues_r2.jpg" width="600" height="" alt="Pandora listeners prefer to go to the polls in person" width="600" height="" style="display: block; border: 0px; outline: none; width: 100%; height: auto; max-width: 600px; margin-left: auto; margin-right: auto;">
                                </td>
                            </tr>
                            
                            <tr>
                                <td  style="padding:5% 8% 0% 8%; font-family: sans-serif; font-size: 1.6em; letter-spacing: 4px; line-height:1.2em; mso-height-rule: exactly; text-align:center; font-weight: bolder; color: #000000; background-color: #ffffff; border-top: solid 3px">
                                    THEME SONGS OF THE POLITICAL SEASON <SUP>7</SUP>
                                </td>
                            </tr>
                            <tr>
                                <td  style="padding:2% 5% 5% 5%; font-family: baskerville, serif; font-size: 1.1em; text-align:center; mso-height-rule: exactly; line-height: 1.7em; color: #4b4b4b;">
                                    <strong>Stuck in the Middle With You</strong> by Stealers Wheel<br />
                                    <strong>Eye of the Tiger</strong> by Survivor<br />
                                    <strong>Born in the USA</strong> by Bruce Springsteen<br />
                                    <strong>Welcome to the Jungle</strong> by Guns N’ Roses<br />
                                    <strong>The Final Countdown</strong> by Europe<br />
                                    <strong>God Bless America</strong><br />
                                </td>
                            </tr>

                            <!--CTA-->
                            <tr>
                                <td  style="padding:5% 8% 2% 8%; font-family: baskerville, serif; font-size: 1.3em; line-height:1.5em; mso-height-rule: exactly; text-align:center; font-weight: bold; color: #ffffff; background-color: #353535">
                                    Contact us today to learn about GOTV strategies and how we can help you win with voters this political season.<br /><br />
                                    
                                    Don’t have a sales representative? <a href="http://pandoraforbrands.com/advertise/" style="color: #ffffff">Click here.</a><br /><br />
                                </td>
                            </tr>                      

                            
<!--SOURCE-->
                    <tr width="100%"style="background-color:#ededed;">
                            <td align="left" style="padding: 5% 10% 3% 10%; font-family: sans-serif; font-size: .6em; mso-height-rule: exactly; line-height: 1.5em; color: #7A7A7A; width:50%">
                                1. ComScore Media Metrix, August 2016<br />
                                2-7. Pandora Soundboard Political Study, September 2016 (n=4000)<br />
                                2. Q. Do you intend to vote in the November 2016 Presidential election?<br />
                                3. Q. Will you vote by mail or in person?<br />
                                4. Q. Who do you intend to vote for in the upcoming Presidential election being held in a few weeks?<br />
                                5. Q. What factors will most likely bring you to the polls?<br />
                                6. Q. Please rank the issues in terms of how important they are to you.<br />
                                7. Q. Enter a song that best describes the 2016 Presidential Election<br />
                                *U.S. Baseline http://www.gallup.com/poll/195806/americans-less-sure-vote-president.aspx<br />
                            </td>
                    </tr>
                    </table>
        </td>
        </tr>
               
    </table>

        </div>
    </center>
</td></tr></table>
</body>
</html>