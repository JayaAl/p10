<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
	<meta charset="utf-8"> <!-- utf-8 works for most cases -->
	<meta name="viewport" content="width=device-width"> <!-- Forcing initial-scale shouldn't be necessary -->
	<meta http-equiv="X-UA-Compatible" content="IE=edge"> <!-- Use the latest (edge) version of IE rendering engine -->
	<title>Pandora Precision Targeting</title> <!-- The title tag shows in email notifications, like Android 4.4. -->

	<!-- Web Font / @font-face : BEGIN -->
	<!-- NOTE: If web fonts are not required, lines 9 - 26 can be safely removed. -->
	
	<!-- Desktop Outlook chokes on web font references and defaults to Times New Roman, so we force a safe fallback font. -->
	<!--[if mso]>
		<style>
			* {
				font-family: sans-serif !important;
			}
		</style>
	<![endif]-->
	
	<!-- All other clients get the webfont reference; some will render the font and others will silently fail to the fallbacks. More on that here: http://stylecampaign.com/blog/2015/02/webfont-support-in-email/ -->
	<!--[if !mso]><!-->
		<!-- insert web font reference, eg: <link href='https://fonts.googleapis.com/css?family=Roboto:400,700' rel='stylesheet' type='text/css'> -->
	<!--<![endif]-->

	<!-- Web Font / @font-face : END -->
	
  	<!-- CSS Reset -->
    <style type="text/css">

		/* What it does: Remove spaces around the email design added by some email clients. */
		/* Beware: It can remove the padding / margin and add a background color to the compose a reply window. */
        html,
        body {
	        margin: 0 auto !important;
            padding: 0 !important;
            height: 100% !important;
            width: 100% !important;
        }
        
        /* What it does: Stops email clients resizing small text. */
        * {
            -ms-text-size-adjust: 100%;
            -webkit-text-size-adjust: 100%;
        }
        
        /* What is does: Centers email on Android 4.4 */
        div[style*="margin: 16px 0"] {
            margin:0 !important;
        }
        
        /* What it does: Stops Outlook from adding extra spacing to tables. */
        table,
        td {
            mso-table-lspace: 0pt !important;
            mso-table-rspace: 0pt !important;
        }
                
        /* What it does: Fixes webkit padding issue. Fix for Yahoo mail table alignment bug. Applies table-layout to the first 2 tables then removes for anything nested deeper. */
        table {
            border-spacing: 0 !important;
            border-collapse: collapse !important;
            table-layout: fixed !important;
            Margin: 0 auto !important;
        }
        table table table {
            table-layout: auto; 
        }
        
        /* What it does: Uses a better rendering method when resizing images in IE. */
        img {
            -ms-interpolation-mode:bicubic;
        }
        
        /* What it does: Overrides styles added when Yahoo's auto-senses a link. */
        .yshortcuts a {
            border-bottom: none !important;
        }
        
        /* What it does: A work-around for iOS meddling in triggered links. */
        .mobile-link--footer a,
        a[x-apple-data-detectors] {
            color:inherit !important;
            text-decoration: underline !important;
        }
      
    </style>
    
    <!-- Progressive Enhancements -->
    <style>
        
        /* What it does: Hover styles for buttons */
        .button-td,
        .button-a {
            transition: all 100ms ease-in;
        }
        .button-td:hover,
        .button-a:hover {
            background: #555555 !important;
            border-color: #555555 !important;
        }

    </style>

</head>
<body width="100%" height="100%" bgcolor="#ffffff" style="Margin: 0;">
<table cellpadding="0" cellspacing="0" border="0" height="100%" width="100%" bgcolor="#f5f5f5" style="border-collapse:collapse; padding-bottom:40px; padding-top: 10px"><tr><td valign="top">
    <center style="width: 100%;">

        <!-- Visually Hidden Preheader Text : BEGIN -->
        <div style="display:none;font-size:1px;line-height:1px;max-height:0px;max-width:0px;opacity:0;overflow:hidden;mso-hide:all;font-family: sans-serif;">
            Pandora Precision Targeting
        </div>
        <!-- Visually Hidden Preheader Text : END -->

        <!--    
            Set the email width. Defined in two places:
            1. max-width for all clients set with Outlook, allowing the email to squish on narrow but never go wider than 600px.
            2. MSO tags for Desktop Windows Outlook enforce a 600px width.
        -->
        <div style="max-width: 600px;">
            <!--[if (gte mso 9)|(IE)]>
            <table cellspacing="0" cellpadding="0" border="0" width="600" align="center">
            <tr>
            <td>
            <![endif]-->

            <!-- Email Header : BEGIN -->
      <!--      <table cellspacing="0" cellpadding="0" border="0" align="center" width="100%" style="max-width: 600px;">
				<tr>
					<td style="padding: 20px 0; text-align: center">
						<img src="http://placehold.it/200x50" width="200" height="50" alt="alt_text" border="0">
					</td>
				</tr>
            </table>-->
            <!-- Email Header : END -->
            
            <!-- Email Body : BEGIN -->
            <table cellspacing="0" cellpadding="0" border="0" align="center" bgcolor="#ffffff" width="600" style="max-width: 600px;">
                
                <!-- Hero Image, Flush : BEGIN -->
                <tr>
					<td>
                        <img src="https://pandoraadvertising.files.wordpress.com/2016/09/1285_politicalemail_targeting_r1.png" width="600" height="" alt="Winning the Republican Vote with Pandora" border="0" align="center" style="width: 100%; max-width: 600px;">
					</td>
                </tr>
                <!-- Hero Image, Flush : END -->

                <!-- 1 Column Text + Button : BEGIN -->
                <tr width="100%">
                    <td>
                        <table cellspacing="0" cellpadding="0" border="0" width="100%">
                            <tr>
                                <td  style="padding:0% 10% 0% 10%; font-family: sans-serif; font-size: 2em; line-height:1.2em; mso-height-rule: exactly; text-align:center; font-weight: bold; color: #000000; background-color: #ffffff">
                                    Pandora Precision Targeting
                                </td>
                            </tr>
                            <tr>
                                <td  style="padding:2% 8% 5% 8%; font-family: sans-serif; font-size: 1.1em; text-align:center; mso-height-rule: exactly; line-height: 1.7em; color: #4b4b4b;">
                                    In the final stretch leading up to Election Day, every campaign impression counts. Smart targeting strategies can turn potential voters into enthusiastic supporters.<br /><br />
                                    
                                    With rich registration data, billions of daily data points from 84 million active monthly listeners,<sup>1</sup> and partnerships with leading data companies including i360, L2, TargetSmart, DataTrust, PDI, and Cambridge Analytica, campaigns on Pandora reach the right voter with the right message.<br /><br />
                                    
                                    <strong>1,300+ Targeting Segments on Pandora</strong>
                                </td>
                            </tr>

                            <tr>
                                <td>
                                    <img src="https://pandoraadvertising.files.wordpress.com/2016/09/1285_seg-categories_r1.png" width="600" height="" alt="Segment Categories: Location, Party Affiliation, Issue Advocasy, Ethnicity, Lifestage" border="0" align="center" style="width: 100%; max-width: 600px; padding: 4% 0% 5% 0%;">
                                </td>
                            </tr>
                            
<!--Segment Location-->
                            <tr>
                                <td>
                                    <img src="https://pandoraadvertising.files.wordpress.com/2016/09/1285_location-seg_r1.png" width="600" height="" alt="Location: State, DMA, MSA, County, Congressional District, State Senate District, Zip Code" border="0" align="center" style="width: 100%; max-width: 600px; padding: 4% 0% 5% 0%;">
                                </td>
                            </tr>
<!--Segment Party-->
                            <tr>
                                <td>
                                    <img src="https://pandoraadvertising.files.wordpress.com/2016/09/1285_party-seg_r1.png" width="600" height="" alt="Party Affiliation: Strong Republican, Lean Republican, Republican Primary Voter, Independent Voter, Strong Democrat, Lean Democrat, Democratic Primary Voter, High Prompensity Voter" border="0" align="center" style="width: 100%; max-width: 600px; padding: 4% 0% 5% 0%;">
                                </td>
                            </tr>
<!--Segment Issue-->
                            <tr>
                                <td>
                                    <img src="https://pandoraadvertising.files.wordpress.com/2016/09/1285_issue-seg_r2.png" width="600" height="" alt="Issue Advocacy: Pro Life/ Choice, Pro 2nd Ammendment, Support Stricter Gun Control, Pro Traditional Energy, Common Core, Immigration Reform, Charter School, Pro Fracking, Fiscally Conservative, Pro Green Energy, Minimum Wage, Marijuana Legalization" border="0" align="center" style="width: 100%; max-width: 600px; padding: 4% 0% 5% 0%;">
                                </td>
                            </tr>
<!--Segment Ethnicity-->
                            <tr>
                                <td>
                                    <img src="https://pandoraadvertising.files.wordpress.com/2016/09/1285_ethnicity-seg_r1.png" width="600" height="" alt="Ethnicity: Hispanic Spanish Prefered, Hispanic Bilingual, African-American, Asian-American" border="0" align="center" style="width: 100%; max-width: 600px; padding: 4% 0% 5% 0%;">
                                </td>
                            </tr>
<!--Segment Lifestage-->
                            <tr>
                                <td>
                                    <img src="https://pandoraadvertising.files.wordpress.com/2016/09/1285_lifestage-seg_r2.png" width="600" height="" alt="Lifestage: Working Class Moms, Millennials, Parents with Young Children, First-Time Eligible Voters, Music Genre Preferences, Hunting Enthusiasts, Outdoor Enthusiasts, Hybrid Car Owners" border="0" align="center" style="width: 100%; max-width: 600px; padding: 4% 0% 5% 0%;">
                                </td>
                            </tr>

<!--Closing Copy-->
                            <tr>
                                <td  style="padding:2% 8% 0% 8%; font-family: sans-serif; font-size: 1.1em; text-align:center; mso-height-rule: exactly; line-height: 1.7em; color: #333333;">
                                    Personalization sharpens political messaging, persuades voters and creates meaningful connections. Pandora’s 1,300+ targeting segments provide the accuracy necessary to help campaigns engage, inspire and mobilize voters to turnout on Election Day.
                                </td>
                            </tr>                      
                            
<!--CTA-->
                            <tr>
                                <td  style="padding: 5% 8% 5% 8%; font-family: sans-serif; font-size: 1.1em; font-weight: bold; text-align:center; mso-height-rule: exactly; line-height: 1.7em; color: #000000;">
                                    Email us today to learn more about our political targeting and advertising solutions, <a href="mailto:Election2016@pandora.com">Election2016@pandora.com</a>
                                </td>
                            </tr>
                            
                     <!--   <tr>
                    <td style="padding: 5% 8% 5% 8%; width: 100%;font-size: 1.3em; font-family: sans-serif; mso-height-rule: exactly; line-height:1.7em; text-align: left; color: #555555; background-color:#f9f9f9">
                    <img src="https://pandoraadvertising.files.wordpress.com/2016/05/pandora_logo_blue_rgb_150px.png">
                    </td>
                </tr>-->
<!--SOURCE-->
                    <tr width="100%"style="background-color:#ededed;">
                            <td align="left" style="padding: 3% 5% 3% 5%; font-family: sans-serif; font-size: .6em; mso-height-rule: exactly; line-height: 1.5em; color: #7A7A7A; width:50%">
                                1. comScore Media Metrix, July 2016
                            </td>
                    </tr>
                    </table>
        </td>
        </tr>
               
    </table>

        </div>
    </center>
</td></tr></table>
</body>
</html>