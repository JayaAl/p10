<?xml version="1.0" encoding="UTF-8"?>
<CustomObject xmlns="http://soap.sforce.com/2006/04/metadata">
    <actionOverrides>
        <actionName>Accept</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>CancelEdit</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>Clone</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>Delete</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>Edit</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>Follow</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>List</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>New</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>SaveEdit</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>Tab</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>View</actionName>
        <type>Default</type>
    </actionOverrides>
    <allowInChatterGroups>false</allowInChatterGroups>
    <compactLayoutAssignment>SYSTEM</compactLayoutAssignment>
    <deploymentStatus>Deployed</deploymentStatus>
    <description>Questions to ask employees when they completing a certification.</description>
    <enableActivities>false</enableActivities>
    <enableBulkApi>true</enableBulkApi>
    <enableFeeds>false</enableFeeds>
    <enableHistory>false</enableHistory>
    <enableReports>true</enableReports>
    <enableSearch>true</enableSearch>
    <enableSharing>true</enableSharing>
    <enableStreamingApi>true</enableStreamingApi>
    <fields>
        <fullName>Certification__c</fullName>
        <externalId>false</externalId>
        <label>Certification</label>
        <referenceTo>Cert_Tool_Certification__c</referenceTo>
        <relationshipLabel>Cert Tool Questions</relationshipLabel>
        <relationshipName>Cert_Tool_Questions</relationshipName>
        <relationshipOrder>0</relationshipOrder>
        <reparentableMasterDetail>false</reparentableMasterDetail>
        <trackTrending>false</trackTrending>
        <type>MasterDetail</type>
        <writeRequiresMasterRead>false</writeRequiresMasterRead>
    </fields>
    <fields>
        <fullName>Default_Answer__c</fullName>
        <defaultValue>&quot;John Smith&quot;</defaultValue>
        <externalId>false</externalId>
        <inlineHelpText>What is the default answer to display for the question? For Multiselect Picklists you may separate values using a semicolon (e.g. ;).</inlineHelpText>
        <label>Default Answer</label>
        <required>false</required>
        <trackTrending>false</trackTrending>
        <type>TextArea</type>
    </fields>
    <fields>
        <fullName>Picklist_Options__c</fullName>
        <externalId>false</externalId>
        <inlineHelpText>If the question type is Picklist or Multiselect Picklist please provide the list of values separated by a semicolon (e.g. ;).</inlineHelpText>
        <label>Picklist Options</label>
        <required>false</required>
        <trackTrending>false</trackTrending>
        <type>TextArea</type>
    </fields>
    <fields>
        <fullName>Question_Text__c</fullName>
        <defaultValue>&quot;What is your name?&quot;</defaultValue>
        <externalId>false</externalId>
        <inlineHelpText>The question we ask the employee.</inlineHelpText>
        <label>Question Text</label>
        <required>false</required>
        <trackTrending>false</trackTrending>
        <type>TextArea</type>
    </fields>
    <fields>
        <fullName>Question_Type__c</fullName>
        <externalId>false</externalId>
        <inlineHelpText>What format will the potential answers be displayed in?</inlineHelpText>
        <label>Question Type</label>
        <picklist>
            <picklistValues>
                <fullName>Free Text</fullName>
                <default>true</default>
            </picklistValues>
            <picklistValues>
                <fullName>Picklist</fullName>
                <default>false</default>
            </picklistValues>
            <picklistValues>
                <fullName>Multiselect picklist</fullName>
                <default>false</default>
            </picklistValues>
            <picklistValues>
                <fullName>Radio Button (yes/no)</fullName>
                <default>false</default>
            </picklistValues>
            <picklistValues>
                <fullName>Checkbox</fullName>
                <default>false</default>
            </picklistValues>
            <picklistValues>
                <fullName>Date</fullName>
                <default>false</default>
            </picklistValues>
            <picklistValues>
                <fullName>Date/Time</fullName>
                <default>false</default>
            </picklistValues>
            <picklistValues>
                <fullName>Email</fullName>
                <default>false</default>
            </picklistValues>
            <picklistValues>
                <fullName>Number</fullName>
                <default>false</default>
            </picklistValues>
            <sorted>false</sorted>
        </picklist>
        <required>false</required>
        <trackTrending>false</trackTrending>
        <type>Picklist</type>
    </fields>
    <fields>
        <fullName>Sorting_Order__c</fullName>
        <defaultValue>0</defaultValue>
        <externalId>false</externalId>
        <inlineHelpText>Determines the order questions are presented to the employee. Lower numbers are displayed first. In the case of a tie questions are ordered by their Question Number.</inlineHelpText>
        <label>Sorting Order</label>
        <precision>18</precision>
        <required>true</required>
        <scale>0</scale>
        <trackTrending>false</trackTrending>
        <type>Number</type>
        <unique>false</unique>
    </fields>
    <label>Cert Tool Question</label>
    <nameField>
        <displayFormat>CQ-{00000}</displayFormat>
        <label>Question Number</label>
        <type>AutoNumber</type>
    </nameField>
    <pluralLabel>Cert Tool Questions</pluralLabel>
    <searchLayouts/>
    <sharingModel>ControlledByParent</sharingModel>
</CustomObject>
