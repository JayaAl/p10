<?xml version="1.0" encoding="UTF-8"?>
<CustomObject xmlns="http://soap.sforce.com/2006/04/metadata">
    <actionOverrides>
        <actionName>Accept</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>CancelEdit</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>Clone</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>Delete</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>Edit</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>Follow</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>List</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>New</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>SaveEdit</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>Tab</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>View</actionName>
        <type>Default</type>
    </actionOverrides>
    <allowInChatterGroups>false</allowInChatterGroups>
    <compactLayoutAssignment>SYSTEM</compactLayoutAssignment>
    <deploymentStatus>Deployed</deploymentStatus>
    <description>Logical grouping of Oracle Roles. Used as part of the Role assignment / approval / review process.

Developed per ERP-618: User Access: On-boarding and Reporting</description>
    <enableActivities>false</enableActivities>
    <enableBulkApi>true</enableBulkApi>
    <enableFeeds>false</enableFeeds>
    <enableHistory>true</enableHistory>
    <enableReports>true</enableReports>
    <enableSearch>true</enableSearch>
    <enableSharing>true</enableSharing>
    <enableStreamingApi>true</enableStreamingApi>
    <fields>
        <fullName>Business_Owner_Email__c</fullName>
        <description>ERP-618
User Access: On-boarding and Reporting</description>
        <externalId>false</externalId>
        <formula>Business_Owner__r.User_Email_Address__c</formula>
        <formulaTreatBlanksAs>BlankAsZero</formulaTreatBlanksAs>
        <label>Business Owner Email</label>
        <required>false</required>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Text</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>Business_Owner__c</fullName>
        <deleteConstraint>SetNull</deleteConstraint>
        <externalId>false</externalId>
        <label>Business Owner</label>
        <lookupFilter>
            <active>true</active>
            <errorMessage>You may only select an ERP User that is associated to a Salesforce User to be the Business Owner.</errorMessage>
            <filterItems>
                <field>ERP_User__c.User__c</field>
                <operation>notEqual</operation>
                <value></value>
            </filterItems>
            <isOptional>false</isOptional>
        </lookupFilter>
        <referenceTo>ERP_User__c</referenceTo>
        <relationshipLabel>ERP Modules Managed</relationshipLabel>
        <relationshipName>ERP_Modules_Managed</relationshipName>
        <required>false</required>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Lookup</type>
    </fields>
    <fields>
        <fullName>IsActive__c</fullName>
        <defaultValue>false</defaultValue>
        <externalId>false</externalId>
        <label>IsActive</label>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Checkbox</type>
    </fields>
    <label>ERP Module</label>
    <listViews>
        <fullName>All</fullName>
        <columns>NAME</columns>
        <columns>Business_Owner__c</columns>
        <columns>Business_Owner_Email__c</columns>
        <columns>CREATEDBY_USER</columns>
        <columns>CREATED_DATE</columns>
        <filterScope>Everything</filterScope>
        <label>All</label>
        <language>en_US</language>
    </listViews>
    <listViews>
        <fullName>All1</fullName>
        <columns>NAME</columns>
        <columns>Business_Owner__c</columns>
        <columns>IsActive__c</columns>
        <filterScope>Everything</filterScope>
        <label>All</label>
        <language>en_US</language>
    </listViews>
    <nameField>
        <label>ERP Module Name</label>
        <trackHistory>false</trackHistory>
        <type>Text</type>
    </nameField>
    <pluralLabel>ERP Modules</pluralLabel>
    <searchLayouts>
        <customTabListAdditionalFields>Business_Owner__c</customTabListAdditionalFields>
        <customTabListAdditionalFields>Business_Owner_Email__c</customTabListAdditionalFields>
    </searchLayouts>
    <sharingModel>ReadWrite</sharingModel>
    <startsWith>Vowel</startsWith>
</CustomObject>
