<?xml version="1.0" encoding="UTF-8"?>
<CustomObject xmlns="http://soap.sforce.com/2006/04/metadata">
    <actionOverrides>
        <actionName>Accept</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>CancelEdit</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>Clone</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>Delete</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>Edit</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>Follow</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>List</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>New</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>SaveEdit</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>Tab</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>View</actionName>
        <type>Default</type>
    </actionOverrides>
    <allowInChatterGroups>false</allowInChatterGroups>
    <compactLayoutAssignment>SYSTEM</compactLayoutAssignment>
    <deploymentStatus>Deployed</deploymentStatus>
    <description>Used to assign ownership of records users via Apex</description>
    <enableActivities>false</enableActivities>
    <enableBulkApi>true</enableBulkApi>
    <enableFeeds>false</enableFeeds>
    <enableHistory>false</enableHistory>
    <enableReports>true</enableReports>
    <enableSearch>true</enableSearch>
    <enableSharing>true</enableSharing>
    <enableStreamingApi>true</enableStreamingApi>
    <fields>
        <fullName>Active__c</fullName>
        <description>If True, then include user in Round Robin assignments.</description>
        <externalId>false</externalId>
        <inlineHelpText>If True, then include user in Round Robin assignments.</inlineHelpText>
        <label>Active</label>
        <picklist>
            <picklistValues>
                <fullName>True</fullName>
                <default>true</default>
            </picklistValues>
            <picklistValues>
                <fullName>False</fullName>
                <default>false</default>
            </picklistValues>
            <sorted>false</sorted>
        </picklist>
        <required>false</required>
        <trackTrending>false</trackTrending>
        <type>Picklist</type>
    </fields>
    <fields>
        <fullName>Last_Assignment__c</fullName>
        <externalId>false</externalId>
        <label>Last Assignment</label>
        <required>false</required>
        <trackTrending>false</trackTrending>
        <type>DateTime</type>
    </fields>
    <fields>
        <fullName>Millisecond__c</fullName>
        <externalId>false</externalId>
        <label>Millisecond</label>
        <precision>3</precision>
        <required>false</required>
        <scale>0</scale>
        <trackTrending>false</trackTrending>
        <type>Number</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>Status__c</fullName>
        <externalId>false</externalId>
        <formula>IMAGE( 
IF(AND(User_Active__c , ISPICKVAL(Active__c,&quot;True&quot;)),&quot;/img/samples/light_green.gif&quot;,&quot;/img/samples/light_red.gif&quot;),
&quot;status color&quot;
)</formula>
        <formulaTreatBlanksAs>BlankAsZero</formulaTreatBlanksAs>
        <inlineHelpText>If Status and User Status are True</inlineHelpText>
        <label>Status</label>
        <required>false</required>
        <trackTrending>false</trackTrending>
        <type>Text</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>User_Active__c</fullName>
        <description>6/13/16 Identified that the lead routing is using this field and not the flag. Updated to include both.

ESS-28845</description>
        <externalId>false</externalId>
        <formula>IF(ISPICKVAL(Active__c,&quot;FALSE&quot;),FALSE,
User__r.IsActive)</formula>
        <formulaTreatBlanksAs>BlankAsZero</formulaTreatBlanksAs>
        <label>User Active</label>
        <trackTrending>false</trackTrending>
        <type>Checkbox</type>
    </fields>
    <fields>
        <fullName>User_ID__c</fullName>
        <externalId>false</externalId>
        <formula>Owner:User.Id</formula>
        <label>User ID</label>
        <required>false</required>
        <trackTrending>false</trackTrending>
        <type>Text</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>User__c</fullName>
        <deleteConstraint>SetNull</deleteConstraint>
        <externalId>false</externalId>
        <label>User</label>
        <referenceTo>User</referenceTo>
        <relationshipName>R00N30000002Yj6ZEAS</relationshipName>
        <required>false</required>
        <trackTrending>false</trackTrending>
        <type>Lookup</type>
    </fields>
    <label>Group Member</label>
    <listViews>
        <fullName>ALL</fullName>
        <columns>NAME</columns>
        <columns>Status__c</columns>
        <columns>Active__c</columns>
        <filterScope>Everything</filterScope>
        <label>ALL</label>
        <language>en_US</language>
    </listViews>
    <nameField>
        <label>Group Member Name</label>
        <type>Text</type>
    </nameField>
    <pluralLabel>Group Members</pluralLabel>
    <searchLayouts>
        <lookupDialogsAdditionalFields>User__c</lookupDialogsAdditionalFields>
        <searchFilterFields>User__c</searchFilterFields>
        <searchResultsAdditionalFields>User__c</searchResultsAdditionalFields>
    </searchLayouts>
    <sharingModel>ReadWrite</sharingModel>
    <validationRules>
        <fullName>Group_Name_must_equal_user_name</fullName>
        <active>true</active>
        <errorConditionFormula>Name !=  (User__r.FirstName + &quot; &quot; + User__r.LastName)</errorConditionFormula>
        <errorDisplayField>Name</errorDisplayField>
        <errorMessage>Group Member Name must Match User Name</errorMessage>
    </validationRules>
</CustomObject>
