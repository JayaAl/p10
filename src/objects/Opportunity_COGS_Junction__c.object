<?xml version="1.0" encoding="UTF-8"?>
<CustomObject xmlns="http://soap.sforce.com/2006/04/metadata">
    <actionOverrides>
        <actionName>Accept</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>CancelEdit</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>Clone</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>Delete</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>Edit</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>Follow</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>List</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>New</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>SaveEdit</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>Tab</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>View</actionName>
        <type>Default</type>
    </actionOverrides>
    <allowInChatterGroups>false</allowInChatterGroups>
    <compactLayoutAssignment>SYSTEM</compactLayoutAssignment>
    <deploymentStatus>Deployed</deploymentStatus>
    <enableActivities>true</enableActivities>
    <enableBulkApi>true</enableBulkApi>
    <enableFeeds>false</enableFeeds>
    <enableHistory>false</enableHistory>
    <enableReports>true</enableReports>
    <enableSearch>true</enableSearch>
    <enableSharing>true</enableSharing>
    <enableStreamingApi>true</enableStreamingApi>
    <fields>
        <fullName>COGS_Owner__c</fullName>
        <deleteConstraint>SetNull</deleteConstraint>
        <description>Read only copy of the COGS Owner</description>
        <externalId>false</externalId>
        <label>COGS Owner</label>
        <referenceTo>User</referenceTo>
        <relationshipName>COGS_Opportunity_Links</relationshipName>
        <required>false</required>
        <trackTrending>false</trackTrending>
        <type>Lookup</type>
    </fields>
    <fields>
        <fullName>COGS__c</fullName>
        <externalId>false</externalId>
        <label>COGS</label>
        <referenceTo>COGS_2_0__c</referenceTo>
        <relationshipName>Opportunity_COGS_Junctions</relationshipName>
        <relationshipOrder>1</relationshipOrder>
        <reparentableMasterDetail>false</reparentableMasterDetail>
        <trackTrending>false</trackTrending>
        <type>MasterDetail</type>
        <writeRequiresMasterRead>false</writeRequiresMasterRead>
    </fields>
    <fields>
        <fullName>Campaign_Spend__c</fullName>
        <externalId>false</externalId>
        <label>Campaign Spend</label>
        <precision>18</precision>
        <required>false</required>
        <scale>2</scale>
        <trackTrending>false</trackTrending>
        <type>Currency</type>
    </fields>
    <fields>
        <fullName>Duplicate_Opportunity__c</fullName>
        <caseSensitive>true</caseSensitive>
        <description>Field used to store a unique Opportunity ID value. Used to limit one COGS per Opportunity.</description>
        <externalId>false</externalId>
        <label>Duplicate Opportunity</label>
        <length>18</length>
        <required>false</required>
        <trackTrending>false</trackTrending>
        <type>Text</type>
        <unique>true</unique>
    </fields>
    <fields>
        <fullName>Opportunity_Account_Id__c</fullName>
        <description>Opportunity Account Id, used for the creation of YoY reporting.</description>
        <externalId>false</externalId>
        <formula>Opportunity__r.AccountId</formula>
        <formulaTreatBlanksAs>BlankAsZero</formulaTreatBlanksAs>
        <label>Opportunity Account Id</label>
        <required>false</required>
        <trackTrending>false</trackTrending>
        <type>Text</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>Opportunity_Account__c</fullName>
        <deleteConstraint>SetNull</deleteConstraint>
        <externalId>false</externalId>
        <label>Opportunity Account</label>
        <referenceTo>Account</referenceTo>
        <relationshipName>Opportunity_COGS_2_0_Junctions</relationshipName>
        <required>false</required>
        <trackTrending>false</trackTrending>
        <type>Lookup</type>
    </fields>
    <fields>
        <fullName>Opportunity_Owner__c</fullName>
        <deleteConstraint>SetNull</deleteConstraint>
        <externalId>false</externalId>
        <label>Opportunity Owner</label>
        <referenceTo>User</referenceTo>
        <relationshipName>Opportunity_COGS_2_0_Junctions2</relationshipName>
        <required>false</required>
        <trackTrending>false</trackTrending>
        <type>Lookup</type>
    </fields>
    <fields>
        <fullName>Opportunity_Probability__c</fullName>
        <externalId>false</externalId>
        <label>Opportunity Probability</label>
        <precision>5</precision>
        <required>false</required>
        <scale>2</scale>
        <trackTrending>false</trackTrending>
        <type>Number</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>Opportunity_Sales_Dev_LOOKUP__c</fullName>
        <deleteConstraint>SetNull</deleteConstraint>
        <externalId>false</externalId>
        <label>Opportunity Sales Dev</label>
        <referenceTo>User</referenceTo>
        <relationshipName>Opportunity_COGS_2_0_Junctions3</relationshipName>
        <required>false</required>
        <trackTrending>false</trackTrending>
        <type>Lookup</type>
    </fields>
    <fields>
        <fullName>Opportunity_Sales_Dev__c</fullName>
        <description>Opportunity Sales Dev field from the parent Opportunity, used to transfer values to the related COGS records.</description>
        <externalId>false</externalId>
        <label>Opportunity Sales Dev</label>
        <length>80</length>
        <required>false</required>
        <trackTrending>false</trackTrending>
        <type>Text</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>Opportunity_Sales_Planner__c</fullName>
        <deleteConstraint>SetNull</deleteConstraint>
        <description>Replicated Opportunity Sales Planner data from the Opportunity used to populate COGS Package values via Trigger.</description>
        <externalId>false</externalId>
        <label>Opportunity Sales Planner</label>
        <referenceTo>User</referenceTo>
        <relationshipName>Opportunity_COGS_2_0_Junctions1</relationshipName>
        <required>false</required>
        <trackTrending>false</trackTrending>
        <type>Lookup</type>
    </fields>
    <fields>
        <fullName>Opportunity_Seller__c</fullName>
        <deleteConstraint>SetNull</deleteConstraint>
        <description>Replicated Opportunity Owner value for use in automated Triggers to populate the COGS Package values.</description>
        <externalId>false</externalId>
        <label>Opportunity Seller</label>
        <referenceTo>User</referenceTo>
        <relationshipName>Opportunity_COGS_2_0_Junctions</relationshipName>
        <required>false</required>
        <trackTrending>false</trackTrending>
        <type>Lookup</type>
    </fields>
    <fields>
        <fullName>Opportunity_Upfront__c</fullName>
        <deleteConstraint>SetNull</deleteConstraint>
        <externalId>false</externalId>
        <label>Opportunity Upfront</label>
        <referenceTo>Upfront__c</referenceTo>
        <relationshipName>Opportunity_COGS_2_0_Junctions</relationshipName>
        <required>false</required>
        <trackTrending>false</trackTrending>
        <type>Lookup</type>
    </fields>
    <fields>
        <fullName>Opportunity__c</fullName>
        <externalId>false</externalId>
        <label>Opportunity</label>
        <referenceTo>Opportunity</referenceTo>
        <relationshipName>Opportunity_COGS_Junctions</relationshipName>
        <relationshipOrder>0</relationshipOrder>
        <reparentableMasterDetail>false</reparentableMasterDetail>
        <trackTrending>false</trackTrending>
        <type>MasterDetail</type>
        <writeRequiresMasterRead>false</writeRequiresMasterRead>
    </fields>
    <fields>
        <fullName>Package_Sold__c</fullName>
        <defaultValue>false</defaultValue>
        <externalId>false</externalId>
        <label>Package Sold</label>
        <trackTrending>false</trackTrending>
        <type>Checkbox</type>
    </fields>
    <fields>
        <fullName>Sold__c</fullName>
        <description>Is there a Sold Package on the related COGS?</description>
        <externalId>false</externalId>
        <formula>COGS__r.Package_Sold__c</formula>
        <formulaTreatBlanksAs>BlankAsZero</formulaTreatBlanksAs>
        <inlineHelpText>Is there a Sold Package on the related COGS?</inlineHelpText>
        <label>Sold?</label>
        <trackTrending>false</trackTrending>
        <type>Checkbox</type>
    </fields>
    <label>COGS Opportunity Link</label>
    <nameField>
        <displayFormat>OC_{0000000}</displayFormat>
        <label>COGS Opportunity Link</label>
        <type>AutoNumber</type>
    </nameField>
    <pluralLabel>COGS Opportunity Links</pluralLabel>
    <searchLayouts/>
    <sharingModel>ControlledByParent</sharingModel>
</CustomObject>
