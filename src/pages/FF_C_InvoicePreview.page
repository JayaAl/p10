<apex:page standardController="c2g__codaInvoice__c" renderas="pdf" showHeader="false" sidebar="false" standardStylesheets="false">

    <apex:stylesheet value="{!URLFOR($Resource.FF_C_SalesInvoicePreviewStyleSheet)}"/>
       <!-- RECORD TYPE -->
        
        <apex:variable var="taxType" value="{!c2g__codaInvoice__c.c2g__OwnerCompany__r.RecordType.Name}"/>        

        <!-- LABEL VARIABLES -->
    
        <!-- Invoice Title -->
        <apex:variable var="lblInvoice"                     value="INVOICE" />
        
        <!-- Invoice Header -->
        <apex:variable var="lblBillingAddress"              value="Billing Address" />
        <apex:variable var="lblShippingAddress"             value="Advertiser Address" />
        
        <apex:variable var="lblInvoiceNumber"               value="Invoice Number" />
        <apex:variable var="lblInvoiceDate"                 value="Invoice Date" />
        <apex:variable var="lblTerms"                       value="Payment Terms" />
        
        <apex:variable var="lblCampaign"                    value="Campaign:  " />
        
        <apex:variable var="lblCustomerReference"           value="Insertion Order #" />
        <apex:variable var="lblShippingMethod"              value="Flight Dates" />
        <apex:variable var="lblDueDate"                     value="Due Date" />
        <apex:variable var="lblAdvertiserName"              value="Advertiser Name" />
        <apex:variable var="lblBillingPeriod"               value="Billing Period" />
        
         <!-- Payment Schedule -->
        <apex:variable var="lblPaymentSchedules"            value="Payment Schedule" />
        <apex:variable var="lblBroadcastPeriod"             value="Broadcast Period" />
        <apex:variable var="lblInsertionOrder"              value="Insertion Order #" />

        <apex:variable var="lblCallLetters"                 value="Call Letters" />
        <apex:variable var="lblMarket"                      value="Market" />
        <apex:variable var="lblBuyer"                       value="Buyer" />
        <apex:variable var="lblClient"                      value="Client" />
        <apex:variable var="lblProduct"                     value="Product" />
        <apex:variable var="lblEstimate"                    value="Estimate" />
        
        <style>
            @page {
               
            @top-right {content:"Page " counter(page) " of " counter(pages);
                           font-family:Verdana, Arial, Helvetica, sans-serif;
                           font-size:8.0pt;
                           font-weight:bold;
                           padding-top:25px;
                           }
             @bottom-right {content:"Page " counter(page) " of " counter(pages);
                           font-family:Verdana, Arial, Helvetica, sans-serif;
                           font-size:8.0pt;
                           font-weight:bold;
                           padding-bottom;25px;
                           }
            }
        </style>
        
        
        <div class="email">
            <apex:Panelgrid width="100%" columns="1">
                <apex:panelGroup >
                <apex:Outputpanel style="vertical-align:top align:right"> 
                    <table>
                        <tr>
                            <td>
<apex:image id="Pandora_logo_Image" value="{!$Resource.PandoraLogoInvoiceImg}" />
</td>
                        </tr>
                    </table>
                    <table class="boxed boxedtop" border = "1px">
                        <table style="vertical-align:top align:right">
                            <td class="alignright" style="font-size:12px">
                                <apex:outputtext value="{!lblInvoice}" styleclass="strong align-right" style="align:right" /> 
                                <br/><br/>
                                    <table  class="boxed boxedtop" border="1px" padding="0px">
                                        <tr>
                                            <td class="strong alignleft bgcolor boxed boxedtop" >{!lblInvoiceNumber}</td>
                                            <td class="alignleft boxed boxedtop">{!c2g__codaInvoice__c.Name}</td>
                                        </tr>
                                        <tr>
                                            <td class="strong alignleft bgcolor boxed boxedtop">{!lblInvoiceDate}</td>
                                            <td class="alignleft boxed boxedtop">
                                                <apex:outputText value="{0,date,dd-MMM-yyyy}">
                                                    <apex:param value="{!c2g__codaInvoice__c.c2g__InvoiceDate__c}"/>
                                                </apex:outputText>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td class="strong alignleft bgcolor boxed boxedtop">{!lblTerms}</td>
                                            <td class="alignleft boxed boxedtop">
                                                <apex:outputText value="{!c2g__codaInvoice__c.Billing_Terms__c}">                                            
                                                </apex:outputText>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td class="strong alignleft bgcolor boxed boxedtop">{!lblDueDate}</td>
                                            <td class="alignleft boxed boxedtop">
                                                <apex:outputText value="{0,date,dd-MMM-yyyy}">
                                                    <apex:param value="{!c2g__codaInvoice__c.c2g__DueDate__c}"/>
                                                </apex:outputText>
                                            </td>
                                        </tr>
                                        <apex:panelGroup rendered="{!c2g__codaInvoice__c.c2g__Opportunity__r.Budget_Source__c=='Radio' && c2g__codaInvoice__c.c2g__Opportunity__r.Slingshot_Eligible__c}">
                                            <tr>
                                                <td class="strong alignleft bgcolor boxed boxedtop">{!lblBroadcastPeriod}</td>
                                                <td class="alignleft boxed boxedtop">
                                                    <apex:outputText value="{!c2g__codaInvoice__c.Billing_Period__c}">                                            
                                                    </apex:outputText>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td class="strong alignleft bgcolor boxed boxedtop">{!lblInsertionOrder}</td>
                                                <td class="alignleft boxed boxedtop">
                                                    <apex:outputText value="{!c2g__codaInvoice__c.IO_Number__c}">
                                                    </apex:outputText>
                                                </td>
                                            </tr>
                                        </apex:panelGroup>
                                    </table>
                            </td>
                        </table> 
                    </table>
                </apex:outputpanel>                 
                </apex:panelGroup>
                <!-- END INVOICE SUMMARY TAG  -->

            </apex:panelgrid>    
            <apex:outputPanel rendered="{!c2g__codaInvoice__c.c2g__Opportunity__r.Budget_Source__c=='Radio' && c2g__codaInvoice__c.c2g__Opportunity__r.Slingshot_Eligible__c}" layout="block">
                <table width="100%" cellpadding="0" cellspacing="0" border="0" style="margin-left: -6px;">
                    <tr>
                        <td width="40%" >
                            <table  class="boxed boxedtop" cellpadding="0" cellspacing="0" border="0" width="40%" >
                                <tr>
                                    <td class="strong bgcolor boxed boxedtop" width="40%">{!lblCallLetters}</td>
                                    <td class="boxed boxedtop">{!c2g__codaInvoice__c.Radio_Call_Letters__c}</td>
                                </tr>
                                <tr>
                                    <td class="strong bgcolor boxed boxedtop" >{!lblMarket}</td>
                                    <td class="boxed boxedtop">{!c2g__codaInvoice__c.Radio_Market__c}</td>
                                </tr>
                                <tr>
                                    <td class="strong bgcolor boxed boxedtop" >{!lblBuyer}</td>
                                    <td class="boxed boxedtop">{!c2g__codaInvoice__c.Radio_Buyer__c}</td>
                                </tr>
                                <tr>
                                    <td class="strong bgcolor boxed boxedtop" >{!lblClient}</td>
                                    <td class="boxed boxedtop">{!c2g__codaInvoice__c.Radio_Client__c}</td>
                                </tr>
                                <tr>
                                    <td class="strong bgcolor boxed boxedtop" >{!lblProduct}</td>
                                    <td class="boxed boxedtop">{!c2g__codaInvoice__c.Radio_Product__c}</td>
                                </tr>
                                <tr>
                                    <td class="strong bgcolor boxed boxedtop" >{!lblEstimate}</td>
                                    <td class="boxed boxedtop">{!c2g__codaInvoice__c.Radio_Estimate__c}</td>
                                </tr>
                            </table>
                        </td>
                        <td>&nbsp;</td>
                    </tr>
                </table>
            </apex:outputPanel>

            <table>
                <tr>
                    <th class="nobg textAlignLeft" style="font-size:10px;font-weight:bold">{!lblBillingAddress}</th>
                    <th class="nobg textAlignLeft" style="font-size:10px;font-weight:bold">{!lblShippingAddress}</th>
                    <td rowspan="1" class="alignBottom">                                             
                    </td>
                </tr>
                <tr>
                    <td class="alignTop" style="font-size:10px">
                        <p>
                            <apex:outputText rendered="{!c2g__codaInvoice__c.Billing_Contact_Invoice_Name__c!=''}" escape="false">{!c2g__codaInvoice__c.Billing_Contact_Invoice_Name__c}<br/></apex:outputText>                    
                            <apex:outputText rendered="{!c2g__codaInvoice__c.c2g__Account__r.Name!=''}" escape="false">{!c2g__codaInvoice__c.c2g__Account__r.Name}<br/></apex:outputText>
                            <apex:outputText rendered="{!c2g__codaInvoice__c.Billing_Contact_Address_1__c!=''}" escape="false">{!c2g__codaInvoice__c.Billing_Contact_Address_1__c}<br/></apex:outputText>
                            <apex:outputText rendered="{!c2g__codaInvoice__c.Billing_Contact_Address_2__c!=''}" escape="false">{!c2g__codaInvoice__c.Billing_Contact_Address_2__c}<br/></apex:outputText>
                            <apex:outputText value="{!c2g__codaInvoice__c.Billing_Contact_City__c},&nbsp;" rendered="{!c2g__codaInvoice__c.Billing_Contact_City__c!=''}" escape="false"/>
                            <apex:outputText value="{!c2g__codaInvoice__c.Billing_Contact_State__c} &nbsp;" rendered="{!c2g__codaInvoice__c.Billing_Contact_State__c!=''}" escape="false"/>
                            <apex:outputText value="{!c2g__codaInvoice__c.Billing_Contact_Postal_Code__c} &nbsp;" rendered="{!c2g__codaInvoice__c.Billing_Contact_Postal_Code__c!=''}" escape="false"/><br/>
                            <apex:outputText value="{!c2g__codaInvoice__c.Billing_Contact_Country__c} &nbsp;" rendered="{!c2g__codaInvoice__c.Billing_Contact_Country__c!=''}" escape="false"/>
                        </p>
                    </td>
                    <td class="alignTop" style="Font-Size:10px">
                        <p>
                            <apex:outputText rendered="{!c2g__codaInvoice__c.Advertiser__r.Name!=''}" escape="false">{!c2g__codaInvoice__c.Advertiser__r.Name}<br/></apex:outputText>                                
                            <apex:outputText rendered="{!c2g__codaInvoice__c.Advertiser__r.BillingStreet!=''}" escape="false">{!c2g__codaInvoice__c.Advertiser__r.BillingStreet}<br/></apex:outputText>
                            <apex:outputText value="{!c2g__codaInvoice__c.Advertiser__r.BillingCity}, &nbsp;" rendered="{!c2g__codaInvoice__c.Advertiser__r.BillingCity!=''}" escape="false"/>
                            <apex:outputText value="{!c2g__codaInvoice__c.Advertiser__r.BillingState}&nbsp;" rendered="{!c2g__codaInvoice__c.Advertiser__r.BillingState!=''}" escape="false"/>
                            <apex:outputText value="{!c2g__codaInvoice__c.Advertiser__r.BillingPostalCode} &nbsp;" rendered="{!c2g__codaInvoice__c.Advertiser__r.BillingPostalCode!=''}" escape="false"/><br/>
                            <apex:outputText value="{!c2g__codaInvoice__c.Advertiser__r.BillingCountry} &nbsp;" rendered="{!c2g__codaInvoice__c.Advertiser__r.BillingCountry!=''}" escape="false"/>                        
                        </p>
                    </td>
                </tr>
            </table>

            <td styleclass="alignCenter" style="text-align:center;Font-Size:15px">
                <apex:outputtext style="font-weight:bold" value="{!lblCampaign}" />
                <apex:outputText style="font-Weight:Bold" value="{!c2g__codaInvoice__c.c2g__Opportunity__r.Campaign_Name_Invoice__c}" rendered="{!c2g__codaInvoice__c.c2g__Opportunity__r.Campaign_Name_Invoice__c!=''}"/>
                <apex:outputText style="font-Weight:Bold" value="{!c2g__codaInvoice__c.Campaign_Name__c}" rendered="{!c2g__codaInvoice__c.Campaign_Name__c!='' && c2g__codaInvoice__c.c2g__Opportunity__r.Campaign_Name_Invoice__c==''}"/>
            </td><br/>
    
            <apex:Outputpanel rendered="{!NOT(c2g__codaInvoice__c.c2g__Opportunity__r.Budget_Source__c=='Radio' && c2g__codaInvoice__c.c2g__Opportunity__r.Slingshot_Eligible__c)}">
                <table class="boxedtable">
                    <tr>
                        <th class="boxed boxedtop aligncenter bgcolor">{!lblBillingPeriod}</th>
                        <th class="boxed boxedtop aligncenter bgcolor">{!lblShippingMethod}</th>
                        <th class="boxed boxedtop aligncenter bgcolor">{!lblAdvertiserName}</th>
                        <th class="boxed boxedtop aligncenter bgcolor">{!lblCustomerReference}</th>

                    </tr>
                   <tr>
                        <td class="boxed boxedtop aligncenter">{!c2g__codaInvoice__c.Billing_Period__c}</td> 
                        <td class="boxed boxedtop aligncenter">{!c2g__codaInvoice__c.ffdc_FlightDates__c}</td>
                        <td class="boxed boxedtop aligncenter">
                            <apex:outputText value="{!c2g__codaInvoice__c.Advertiser__r.Name}" rendered="{!c2g__codaInvoice__c.Advertiser__r.Name!=''}"/>                    
                            </td>
                        <td  class="boxed boxedtop aligncenter">{!c2g__codaInvoice__c.IO_Number__c}</td>                                            
                    </tr>
                </table><br/>
            </apex:outputPanel>

            
            <c:FF_C_SalesInvoiceEmailComponent salesinvoiceID="{!c2g__codaInvoice__c.ID}" invoicePage1Of1="{!c2g__codaInvoice__c.c2g__OwnerCompany__r.InvoicePage1Of1__c}"
                     invoicePage1OfX="{!c2g__codaInvoice__c.c2g__OwnerCompany__r.InvoicePage1OfX__c}" invoicePageXOfX="{!c2g__codaInvoice__c.c2g__OwnerCompany__r.InvoicePageXOfX__c}" 
                     invoiceLastPage="{!c2g__codaInvoice__c.c2g__OwnerCompany__r.InvoiceLastPage__c}"
                     PrintedText1Heading="{!c2g__codaInvoice__c.c2g__PrintedText1Heading__c}" PrintedText1Text="{!c2g__codaInvoice__c.c2g__PrintedText1Text__c}" 
                     PrintedText2Heading="{!c2g__codaInvoice__c.c2g__PrintedText2Heading__c}" PrintedText2Text="{!c2g__codaInvoice__c.c2g__PrintedText2Text__c}" 
                     PrintedText3Heading="{!c2g__codaInvoice__c.c2g__PrintedText3Heading__c}" PrintedText3Text="{!c2g__codaInvoice__c.c2g__PrintedText3Text__c}" 
                     PrintedText4Heading="{!c2g__codaInvoice__c.c2g__PrintedText4Heading__c}" PrintedText4Text="{!c2g__codaInvoice__c.c2g__PrintedText4Text__c}"
                     PrintedText5Heading="{!c2g__codaInvoice__c.c2g__PrintedText5Heading__c}" PrintedText5Text="{!c2g__codaInvoice__c.c2g__PrintedText5Text__c}"  
                     EditablePrintedTextHeading="{!c2g__codaInvoice__c.Editable_Printed_Text_Heading__c}" EditablePrintedText="{!c2g__codaInvoice__c.Editable_Printed_Text__c}"  

                     taxType="{c2g__codaInvoice__c.taxType}" isoCode="{!c2g__codaInvoice__c.c2g__InvoiceCurrency__r.Currency_Symbol__c}" netTotal="{!c2g__codaInvoice__c.c2g__NetTotal__c}" 
                     taxTotal="{!c2g__codaInvoice__c.c2g__TaxTotal__c}" invoiceTotal="{!c2g__codaInvoice__c.c2g__InvoiceTotal__c}"   />
             

        </div>

        <!--BEGIN REMIT TO INFORMATION -->
        <div style="page-break-inside:avoid">
            <td styleclass="alignLeft" style="text-align:left;Font-Size:15px">
            <apex:outputtext style="font-weight:bold" value="REMIT TO INFORMATION" />
            </td>
            <table class="boxedtable">
                <tr>
                    <td class="boxed boxedtop aligncenter strong bgcolor">Mail Checks To:  </td> 
                    <td class="boxed boxedtop aligncenter bgcolor strong">To send Wire/ACH payments please  use the information below:  </td>
                </tr>
               <tr>
                    <td  class="boxed boxedtop aligncenter"><apex:outputfield value="{!c2g__codaInvoice__c.c2g__OwnerCompany__r.ffdc_RemitTo1__c}" style="Align:Top;text-align:top"/></td> 
                    <td  class="boxed boxedtop aligncenter"><apex:outputfield value="{!c2g__codaInvoice__c.c2g__OwnerCompany__r.ffdc_RemitTo2__c}" style="Align:Top;text-align:top"/> </td>                                          
                </tr>
            </table>
        </div>
        <!--END REMIT TO INFORMATION -->

    <apex:Outputpanel rendered="{!c2g__codaInvoice__c.c2g__Opportunity__r.Budget_Source__c=='Radio' && c2g__codaInvoice__c.c2g__Opportunity__r.Slingshot_Eligible__c}">
        <div style="email">
            <c:FF_C_RadioSalesInvoiceEmailComponent salesinvoiceID="{!c2g__codaInvoice__c.ID}" invoicePage1Of1="15" invoicePage1OfX="21" invoicePageXOfX="21" 
                invoiceLastPage="5" Billing_Period="{!c2g__codaInvoice__c.Billing_Period__c}" Campaign_Name="{!c2g__codaInvoice__c.Campaign_Name__c}" />
        </div>
    </apex:Outputpanel>

</apex:page>