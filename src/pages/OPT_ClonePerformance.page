<!--
Version         : v1.1 
Modified By     : Jaya Alaparthi
Modified Purpose: ESS-40334
-->
<apex:page standardController="Opportunity" extensions="OPT_CloneOpportunityExt">

  <script>
        var isClicked = false;
        function checkDoubleSubmit(obj){
                    if (isClicked) {
                        return false;
                    }else {
                        isClicked = true;
                        obj.className = 'btnDisabled';
                        return true;
                    }
        }  
  </script>      
  
  <apex:sectionHeader title="Opportunity Clone" />
  
  <apex:form >
    <apex:pageMessages />
    <apex:pageBlock title="Opportunity Edit" mode="edit">
      
      <apex:pageBlockButtons location="top">
        
        <apex:commandButton value="Save" action="{!save}" onclick="return checkDoubleSubmit(this)" />
        
        
        <apex:commandButton value="Cancel" action="{!cancel}"/>
        
      </apex:pageBlockButtons>
      
      <apex:pageBlockButtons location="bottom">
        
        <apex:commandButton value="Save" action="{!save}" onclick="return checkDoubleSubmit(this)" />
        
        <apex:commandButton value="Cancel" action="{!cancel}"/>
        
      </apex:pageBlockButtons>
      
      <apex:pageBlockSection title="Opportunity Information" columns="2">
        
        <apex:outputField value="{!objOpp.OwnerId}" />
        
        <apex:outputField value="{!objOpp.Amount}" />
        
        <apex:inputField value="{!objOpp.Name}" required="true"/>
        
        <apex:inputField value="{!objOpp.StageName}" required="true"/>
        
        <apex:inputField value="{!objOpp.AccountId}" required="true"/>
        
        <apex:inputText value="{!objOpp.Probability}" required="false" disabled="true"/>
        
        <apex:inputField value="{!objOpp.Agency__c}" required="false"/>
        
        <apex:inputField value="{!objOpp.Reason_Lost__c}" required="true"/>
        
        <apex:inputField value="{!objOpp.Confirm_direct_relationship__c}" required="false"/>
        
        <apex:inputField value="{!objOpp.CloseDate}" required="true"/>
        
        <apex:inputField value="{!objOpp.Primary_Contact__c}" required="true"/>
        
        <apex:inputField value="{!objOpp.ContractStartDate__c}" required="false"/>
        
        <apex:inputField value="{!objOpp.Primary_Billing_Contact__c}" required="false"/>
        
        <apex:inputField value="{!objOpp.ContractEndDate__c}" required="false"/>
        
        <apex:inputField value="{!objOpp.Industry_Category__c}" required="true"/>
        
        <apex:inputField value="{!objOpp.Bill_on_Broadcast_Calendar2__c}" required="false"/>
        
        <apex:inputField value="{!objOpp.Industry_Sub_Category__c}" required="false"/>
        
        <apex:inputField value="{!objOpp.Initiative_Location__c}" required="true"/>
        
        <apex:inputField value="{!objOpp.CurrencyIsoCode}" required="true"/>
        
        <apex:inputField value="{!objOpp.Initiative_Type__c}" required="true"/>
        
        <apex:inputField value="{!objOpp.NextStep}" required="false"/>
        
      </apex:pageBlockSection>
      
      <apex:pageBlockSection title="Performance Reporting" columns="2">
        
        <apex:inputField value="{!objOpp.Cost_Type__c}" required="true"/>
        
        <apex:inputField value="{!objOpp.Budget_Cap__c}" required="true"/>
        
        <apex:inputField value="{!objOpp.Variable_CPA_CPI__c}" required="false"/>
        
        <apex:inputField value="{!objOpp.Rev_Share__c}" required="false"/>
        
        <apex:inputField value="{!objOpp.Audience_Extension_Included__c}" required="false"/>
        
        <apex:inputField value="{!objOpp.Vendor__c}" required="false"/>
        
        <apex:inputField value="{!objOpp.Performance_Action__c}" required="true"/>
        
        <apex:inputField value="{!objOpp.Network_Billing_Status__c}" required="false"/>
        
        <apex:inputField value="{!objOpp.Length_of_Lookback_Window_Days__c}" required="false"/>
        
        <apex:inputField value="{!objOpp.Bill_for_latent_transactions__c}" required="false"/>
        
        <apex:inputField value="{!objOpp.Contract_Type__c}" required="true"/>
        
        <apex:inputField value="{!objOpp.Test_Campaign__c}" required="true"/>
        
        <!-- v1.1 -->
        <apex:inputField value="{!objOpp.Is_Hybrid__c}"/>

        <apex:inputField value="{!objOpp.Acquisition_Equals__c}" required="false"/>
        
        <apex:inputField value="{!objOpp.Performance_Industry__c}" required="true"/>
        
        <apex:inputField value="{!objOpp.Objective__c}" required="false"/>
        
        <apex:inputField value="{!objOpp.Performance_Sub_Industry__c}" required="true"/>
        
        <apex:inputField value="{!objOpp.Data_Validation__c}" required="false"/>
        
        <apex:inputField value="{!objOpp.Client_Reporting__c}" required="false"/>
        
        <apex:inputField value="{!objOpp.Served__c}" required="false"/>
        
        <apex:inputField value="{!objOpp.Performance_Tier__c}" required="false"/>
        
        <apex:inputField value="{!objOpp.Fcap__c}" required="false"/>
        
        <apex:inputField value="{!objOpp.Pandora_History__c}" required="false"/>
        
        <apex:inputField value="{!objOpp.Bounty__c}" required="true"/>
        
        <apex:inputField value="{!objOpp.Schedule_Priority__c}" required="false"/>
        
        <apex:inputField value="{!objOpp.UI_Reporting_URL__c}" required="false"/>
        
        <apex:inputField value="{!objOpp.Addendum_Language__c}" required="false"/>
        
        <apex:inputField value="{!objOpp.UI_Reporting_Login_Name__c}" required="false"/>
        
        <apex:inputField value="{!objOpp.Reporting__c}" required="false"/>
        
        <apex:inputField value="{!objOpp.UI_Reporting_Login_Password__c}" required="false"/>
        
        <apex:inputField value="{!objOpp.Tracking_URL_s__c}" required="false"/>
        
        <apex:inputField value="{!objOpp.Impression_Level_Notes__c}" required="false"/>
        
      </apex:pageBlockSection>
      
      <apex:pageBlockSection title="Support Team" columns="2">
        
        <apex:inputField value="{!objOpp.Sales_Planner_NEW__c}" required="false"/>
        
        <apex:inputField value="{!objOpp.Referral__c}" required="false"/>
        
        <apex:inputField value="{!objOpp.Lead_Campaign_Manager__c}" required="true"/>
        
      </apex:pageBlockSection>
      
      <apex:pageBlockSection title="Sales Planning" columns="2">
        
        <apex:inputField value="{!objOpp.Media_Plan__c}" required="false"/>
        
        <apex:inputField value="{!objOpp.Campaign_Wrap__c}" required="false"/>
        
        <apex:inputField value="{!objOpp.Media_Plan_Completed__c}" required="false"/>
        
        <apex:inputField value="{!objOpp.Campaign_Wrap_Completed__c}" required="false"/>
        
        <apex:inputField value="{!objOpp.RFP_Deck_Planner_Only__c}" required="false"/>
        
        <apex:inputField value="{!objOpp.Highest_RFP_Amount__c}" required="false"/>
        
        <apex:inputField value="{!objOpp.RFP_Deck_Completed__c}" required="false"/>
        
        <apex:inputField value="{!objOpp.Insertion_Order__c}" required="false"/>
        
        <apex:inputField value="{!objOpp.Pre_Sales_Support_Notes__c}" required="false"/>
        
      </apex:pageBlockSection>
      
      <apex:pageBlockSection title="Accounts Receivable" columns="2">
        
        <apex:inputField value="{!objOpp.Collections_Contact__c}" required="false"/>
        
        <apex:inputField value="{!objOpp.PO_Number__c}" required="false"/>
        
        <apex:inputField value="{!objOpp.Billing_Coordinator__c}" required="false"/>
        
        <apex:inputField value="{!objOpp.Campaign_Name_Invoice__c}" required="false"/>
        
        <apex:inputField value="{!objOpp.Preferred_Invoicing_Method__c}" required="false"/>
        
        <apex:inputField value="{!objOpp.Invoice_Print_Page_2__c}" required="false"/>
        
        <apex:inputField value="{!objOpp.Performance_Page_2_Preference__c}" required="false"/>
        
        <apex:inputField value="{!objOpp.ffps_edipan__EDI_Service_Provider__c}" required="true"/>
        
      </apex:pageBlockSection>
      
      <apex:pageBlockSection title="International" columns="2">
        
        <apex:inputField value="{!objOpp.Delivery_Location__c}" required="false"/>
        
        <apex:inputField value="{!objOpp.Legal_Entity__c}" required="false"/>
        
      </apex:pageBlockSection>
      
      <apex:pageBlockSection title="Targeting & TTR" columns="2">
        
        <apex:inputField value="{!objOpp.Psychographic_Audience_Targets__c}" required="true"/>
        
        <apex:inputField value="{!objOpp.TTR_User_Data__c}" required="false"/>
        
        <apex:inputField value="{!objOpp.Target_Audience_Gender__c}" required="false"/>
        
        <apex:inputField value="{!objOpp.MSA_target_exists__c}" required="false"/>
        
        <apex:inputField value="{!objOpp.Age__c}" required="false"/>
        
        <apex:inputField value="{!objOpp.Adx_Deal_ID__c}" required="false"/>
        
        <apex:inputField value="{!objOpp.Request_TTR__c}" required="false"/>
        
        <apex:inputField value="{!objOpp.Programmatic_Buy__c}" required="false"/>
        
        <apex:inputField value="{!objOpp.TTR_URL__c}" required="false"/>
        
      </apex:pageBlockSection>
      
      <apex:pageBlockSection title="Order Management System" columns="2">
        <apex:outputField value="{!objOpp.RecordTypeId}"/>
        <apex:inputField value="{!objOpp.Slingshot_Eligible__c}" required="false"/>
        
      </apex:pageBlockSection>
      
    </apex:pageBlock>
    
  </apex:form>
  
</apex:page>