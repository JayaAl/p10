trigger ATG_QuoteLineTrigger on SBQQ__QuoteLine__c (after insert,after update) {

	 if(Trigger.isAfter && Trigger.isInsert) {
	    ATG_QuoteLineTriggerHelper.handleAfterInsert(Trigger.new);
	  }

	  if(Trigger.isAfter && Trigger.isUpdate) {
	    ATG_QuoteLineTriggerHelper.handleAfterUpdate(Trigger.new,trigger.oldMap);
	  }

}