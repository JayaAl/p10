trigger ATG_QuoteTrigger on SBQQ__Quote__c (after insert) {

  if(Trigger.isAfter && Trigger.isInsert) {
    ATG_QuoteTriggerHelper.handleAfterInsert(Trigger.new);
  }

}