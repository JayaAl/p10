trigger ATS_UpdateLineItems on OpportunityLineItem (before insert, before update) {
    system.debug('inside ATS_UpdateLineItems '+Trigger.New);
    if(UserInfo.getUserId() != Label.ADXSyncUser)
    	ATS_Util.updateOpptyLineItems(Trigger.New);
}