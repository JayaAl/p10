/**
 * @name: AT_GrantOwnershipOnApproval 
 * @desc: Used change ownership once the record is approved
 * @author: Lakshman(sfdcace@gmail.com)
 * @date: 6-8-2013
 */
trigger AT_GrantOwnershipOnApproval on Account_Ownership__c (after update) {
    Map<Id, Account_Ownership__c> mapOfAccountIdToOwner = new Map<Id, Account_Ownership__c>();
    Map<Id, User> mapOfUser = new Map<Id, User>();
    Set<Id> setOfUsers = new Set<Id>();
    for(Account_Ownership__c ao : Trigger.new) {
        if(Trigger.oldMap.get(ao.Id).Status__c != ao.Status__c && ao.Status__c == 'Approved') {
            mapOfAccountIdToOwner.put(ao.Account__c, ao);
            setOfUsers.add(ao.Requested_By__c);
            setOfUsers.add(ao.Requesters_Manager__c);
            setOfUsers.add(ao.Account_Owners_Manager__c);
        }
    }
    
    for(User u: [Select Id, Name, Email from User where Id =: setOfUsers]) {
        mapOfUser.put(u.Id, u);
    }
    
    if(! mapOfAccountIdToOwner.isEmpty()) {
        List<Account> listOfAccountToUpdate = new List<Account>();
        String sfdcBaseURL = URL.getSalesforceBaseUrl().toExternalForm();
        for(Account acc: [Select Id, OwnerId, Owner.Email, Owner.Name, Name from Account where Id In: mapOfAccountIdToOwner.keySet() ]) {
            if(acc.OwnerId != mapOfAccountIdToOwner.get(acc.Id).Requested_By__c) {
                AT_EmailUtil.to(new String[]{acc.Owner.Email})
                                    .saveAsActivity(false)
                                    .senderDisplayName('Salesforce.com')
                                    .subject(acc.Name + ' has been transferred' )
                                    .htmlBody(
                                    'Hello ' + acc.Owner.Name + ', <br/><br/> <a href="' + sfdcBaseURL +'/' + acc.Id + '">' + acc.Name + '</a> has been transferred to <b>' + mapOfUser.get(mapOfAccountIdToOwner.get(acc.Id).Requested_By__c).Name + '</b>.<br/><br/>It was approved by<br/><br/><b>' + mapOfUser.get(mapOfAccountIdToOwner.get(acc.Id).Requesters_Manager__c).Name + '<br/>' + mapOfUser.get(mapOfAccountIdToOwner.get(acc.Id).Account_Owners_Manager__c).Name + '</b>'
                                    )
                                    .useSignature(false)
                                    .replyTo(acc.Owner.email)
                                    .stashForBulk();
                acc.OwnerId = mapOfAccountIdToOwner.get(acc.Id).Requested_By__c;
                listOfAccountToUpdate.add(acc);
            }
        }
        if(! listOfAccountToUpdate.isEmpty()) {
            try{
                update listOfAccountToUpdate;
                AT_EmailUtil.sendBulkEmail();
            } catch(Exception ex) {
                system.debug('******ex' + ex);
            }
        }
    }
}