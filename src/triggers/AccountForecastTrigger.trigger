/**
* ─────────────────────────────────────────────────────────────────────────────────────────────────┐
* Trigger on AccountForecastTrigger.trigger
* ──────────────────────────────────────────────────────────────────────────────────────────────────
* @author         Jaya Alaparthi
* @maintainedBy   Jaya Alaparthi
* @version        1.0
* @created        2017-02-14
* @modified       YYYY-MM-DD
* @systemLayer    Trigger
* @see            ????
* @see            ????
* ──────────────────────────────────────────────────────────────────────────────────────────────────
* @changes
* vX.X            Jaya Alaparthi
* YYYY-MM-DD      Explanation of the change.  Multiple lines can be used to explain the change, but
*                 each line should be indented till left aligned with the previous description text.
* ─────────────────────────────────────────────────────────────────────────────────────────────────┘
*/
trigger AccountForecastTrigger on Account_Forecast__c (after insert) {

	AccountForecastTriggerHandler refHandler = new AccountForecastTriggerHandler();

	if (Trigger.isAfter && Trigger.isInsert) {
		
		refHandler.afterInsertAction(Trigger.new);		
	}
}