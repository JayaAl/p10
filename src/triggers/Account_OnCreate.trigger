trigger Account_OnCreate on Account (before insert) {
    //nsk: 10/23 Adding below logic to control trigger execution via custom label
    if(Label.Disable_Account_Triggers!='YES'){
        if(trigger.isBefore && trigger.isInsert){
            for(Account a : trigger.new){
                if(('Advertiser').equalsIgnoreCase(a.Type) || ('Ad Agency').equalsIgnoreCase(a.Type)){
                    a.c2g__CODAAccountsReceivableControl__c = Label.General_Ledger_Account;
                }
            }
        }
    }
}