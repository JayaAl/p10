/*
==================================================
Developer: Menaka Nanjappan <mnanjappan@pandora.com>
Created on : 12/2/2016
Jira :ES Support/ ESS-33830 - ADX Product Mapping is currently a custom setting. Jenny Xavier is requesting the ability to edit these records. 
Unable to provide edit access via permissions as this will allow access to edit all metadata.
Description: This trigger is used to update 'Adx Product Mapping' custom settings -whenever a business user creates or updates CustomAdxProductMapping Custom object.
====================================================
*/

trigger AdExProductMapping on CustomAdxProductMapping__c  (after insert, after update) {
    List<Adx_Product_Mapping__c>  adexProductList= new List<Adx_Product_Mapping__c>();
    // This trigger creates an record in custom settings 'Adx Product Mapping' after an user inserts any records on Custom object "Custom Adx Product Mapping".
    if(trigger.isInsert){
        for(CustomAdxProductMapping__c  prod:trigger.new)
        { 
            Adx_Product_Mapping__c adexProduct= new Adx_Product_Mapping__c ();
            adexProduct.Name = prod.Name;
            adexProduct.Ad_Unit_Size_Name__c= prod.Ad_Unit_Size_Name__c;
            adexProduct.Banner__c= prod.Banner__c;
            adexProduct.DFP_Ad_Unit_Id__c= prod.DFP_Ad_Unit_Id__c;
            adexProduct.Medium__c= prod.Medium__c;
            adexProduct.Offering_Type__c= prod.Offering_Type__c;
            adexProduct.Platform__c= prod.Platform__c;
            adexProduct.SFDC_Products__c=prod.SFDC_Products__c;
            adexProduct.Size__c=prod.Size__c;
            adexProduct.Sub_Platform__c=prod.Sub_Platform__c;
            adexProductList.add(adexProduct);
        }
        try{
            if(adexProductList != null && adexProductList.size() >0){
                insert adexProductList;
            }
        }catch(Exception ex){
            System.debug('Error Message AdExProductMapping Insert Trigger'+ex.getMessage());
        }
    }
    
    //This trigger updates an record in custom settings 'Adx Product Mapping' after an user inserts any records on Custom object "Custom Adx Product Mapping".
    if(trigger.isUpdate){
        set<String> uniqueName = new set<String>();
        map<String, CustomAdxProductMapping__c> mapAdExProd = new map<String, CustomAdxProductMapping__c>();
        List<Adx_Product_Mapping__c>  adexUpdateProductList= new List<Adx_Product_Mapping__c>();
        for(CustomAdxProductMapping__c  prod:trigger.new) {
            uniqueName.add(prod.Name);
            mapAdExProd.put(prod.Name, prod);
        }
        //Fetech all the Custom setting values from 'Adx Product Mapping'
        List<Adx_Product_Mapping__c> adexProdList= Adx_Product_Mapping__c.getall().values();
        List<Adx_Product_Mapping__c>  adxMapList= new List<Adx_Product_Mapping__c>();
        if(adexProdList != null && adexProdList.size() >0){
            for(Adx_Product_Mapping__c ad: adexProdList){
                if(mapAdExProd.containsKey(ad.Name)){
                    adxMapList.add(ad);
                }
            }
        }
        
        if(adxMapList != Null && adxMapList.size() > 0) {
            for(Adx_Product_Mapping__c product : adxMapList) {
                CustomAdxProductMapping__c adexProd= mapAdExProd.get(product.Name);
                product.Name = adexProd.name;
                product.Ad_Unit_Size_Name__c =adexProd.Ad_Unit_Size_Name__c;
                product.Banner__c = adexProd.Banner__c;
                product.DFP_Ad_Unit_Id__c =adexProd.DFP_Ad_Unit_Id__c;
                product.Medium__c = adexProd.Medium__c;
                product.Offering_Type__c = adexProd.Offering_Type__c;
                product.Platform__c =  adexProd.Platform__c;
                product.SFDC_Products__c =adexProd.SFDC_Products__c;
                product.Size__c = adexProd.Size__c;
                product.Sub_Platform__c = adexProd.Sub_Platform__c;
                adexUpdateProductList.add(product);                
            }
            try{
                 if(adexUpdateProductList != null && adexUpdateProductList.size() >0){
               		 update adexUpdateProductList;
                 }
            }catch(Exception ex){
                System.debug('Error Message AdExProductMapping Update Trigger'+ex.getMessage());
            }
            
        }
    }
    
}