trigger Auto_Assign_Biller_Opportunity on Opportunity (before insert) { //, before update
    // 
    // feed all Auto_Assign_Biller__c records into a Map for reference
    Map<String, Auto_Assign_Biller__c> mapBillers = Auto_Assign_Biller__c.getAll();
    
    // For each Oppty, if there is a Campaign_Manager__c, and that Campaign_Manager__c exists in mapBillers then populate the Billing_Coordinator__c
    for (Opportunity o : Trigger.new) {
        Id theCM = o.Lead_Campaign_Manager__c;
        if(theCM != null && mapBillers.containsKey(theCM)){
            Auto_Assign_Biller__c a = mapBillers.get(theCM);
            if(!a.Billing_Coordinator_on_Vacation__c){
                o.Billing_Coordinator__c = a.Billing_Coordinator_ID__c;
            } else {
                o.Billing_Coordinator__c = a.Temporary_Billing_Coordinator_Id__c;
            }
        }
    }
}