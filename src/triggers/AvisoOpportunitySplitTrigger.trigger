/**
* ─────────────────────────────────────────────────────────────────────────────────────────────────┐
* Trigger for Aviso Integration.
* ──────────────────────────────────────────────────────────────────────────────────────────────────
* @author         Jaya Alaparthi
* @maintainedBy   Jaya Alaparthi
* @created        2017-09-06
* @modified       
* @systemLayer    Trigger
* @see            ????
* @see            ????
* ──────────────────────────────────────────────────────────────────────────────────────────────────
* @changes
* v1.1            Jaya Alaparthi
* YYYY-MM-DD      
* ──────────────────────────────────────────────────────────────────────────────────────────────────
* ─────────────────────────────────────────────────────────────────────────────────────────────────┘
*/
trigger AvisoOpportunitySplitTrigger on Opportunity_Split__c (
											before insert, 
											before update, 
											before delete, 
											after insert, 
											after update, 
											after delete, 
											after undelete) {

		Gnana.AvisoController ac = new Gnana.AvisoController();
		if (Trigger.isInsert) {	   
	    	ac.handleInsertTrigger(Trigger.new,'Opportunity_Split__c'); 
		} else if (Trigger.isUpdate) { 
	    	ac.handleUpdateTrigger(Trigger.new,Trigger.oldMap,'Opportunity_Split__c');    
		} else if(Trigger.isDelete) {
			ac.handleDeleteTrigger(Trigger.oldMap,'Opportunity_Split__c'); 
		} else if(Trigger.isUndelete) {
			ac.handleUndeleteTrigger(Trigger.new,'Opportunity_Split__c'); 
		}
}