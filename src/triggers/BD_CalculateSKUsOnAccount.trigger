trigger BD_CalculateSKUsOnAccount on Project__c (after insert, after update, after delete) {
    Set<Id> accIds = new Set<Id>();
    if(trigger.isDelete){
        for(Project__c p : trigger.old){
            accIds.add(p.Account__c);
        }
    }else{
        for(Project__c p : trigger.new){
            if(trigger.isUpdate){ 
                if((p.Number_of_SKUs__c != trigger.oldMap.get(p.Id).Number_of_SKUs__c) || (p.Account__c != trigger.oldMap.get(p.Id).Account__c) ){
                    accIds.add(p.Account__c);
                    accIds.add(trigger.oldMap.get(p.Id).Account__c);
                }
            }else{
                accIds.add(p.Account__c);
            }
        }
    }
    if(accIds.size() > 0){
        BD_SKUCount.calculateSKUCount(accIds);
    }
}