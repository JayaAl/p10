trigger BD_CheckDuplicateAccountName on Account (before insert, before update) {
    String BIZ_DEV_RT = 'Business Development';

    //nsk: 10/23 Adding below logic to control trigger execution via custom label
    if(Label.Disable_Account_Triggers!='YES'){

        Id BD_RECORD_TYPE_ID = getBizDevRecordType();   //'012Q0000000Csvs';
    
        Set<String> accountNames = new Set<String>();
        Set<Id> accountIds = new Set<Id>();
    
        for (Account a : trigger.new) {
            if (a.RecordTypeId != BD_RECORD_TYPE_ID) continue;
            accountNames.add(a.name);
            if (trigger.isUpdate) {
                accountIds.add(a.Id);
            }
        }
        
        List<Account> projects = [Select Id, name from Account 
            where name in :accountNames And Id Not In :accountIds And RecordTypeId = :BD_RECORD_TYPE_ID];
        if (projects.isEmpty()) return;
    
        trigger.new.get(0).Name.addError('An account with the same name already exists');

    }
        
    public Id getBizDevRecordType() {
        Map<String, Schema.RecordTypeInfo> accountRT = Schema.SObjectType.Account.getRecordTypeInfosByName();
        Schema.RecordTypeInfo bizDevRT = accountRT.get(BIZ_DEV_RT);
        return bizDevRT.getRecordTypeId();
    }        
}