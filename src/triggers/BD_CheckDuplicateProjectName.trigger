trigger BD_CheckDuplicateProjectName on Project__c (before insert, before update) {
/* Deprecated code, re-written and added to TriggerOnProject
    Set<String> projectNames = new Set<String>();
    Set<Id> projectIds = new Set<Id>();
    for (Project__c p : trigger.new) {
        projectNames.add(p.name);
        if (trigger.isUpdate) {
            projectIds.add(p.Id);
        }
    }
    
    List<Project__c> projects = [Select Id, name from Project__c where name in :projectNames And Id Not In :projectIds];
    if (projects.isEmpty()) return;

    trigger.new.get(0).Name.addError('A project with the same name already exists');
*/        
}