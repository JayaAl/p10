trigger BD_SetProject on Agreement__c (before insert, after insert) {
	
	String ACTIVE_RT_NAME = 'Active';
	Id activeRT = getAgreementActiveRecordType();

    Map<Id, Id> projectAgreementIds = new Map<Id, Id>();
    List<Project_Agreement__c> projectAgreements = new List<Project_Agreement__c>();
    for (Agreement__c a : trigger.new) {
        if (trigger.isBefore) {
            a.RecordTypeId = activeRT;	//'012Q0000000Ctbj';
        } else {
            if (a.Project__c == null) continue;
            projectAgreementIds.put(a.Id, a.Project__c);
            //a.Project__c = null;
        }
        
    }
    
    if (projectAgreementIds.isEmpty()) return;
    
    for (Id agreementId : projectAgreementIds.keySet())
    {
        projectAgreements.add(new Project_Agreement__c(//Name = trigger.newmap.get(agreementId).name,
            Agreement__c = agreementId, 
            Project__c = projectAgreementIds.get(agreementId)));
    }
    
    if ( ! projectAgreements.isEmpty()) {
        insert projectAgreements;
    }
    
    public Id getAgreementActiveRecordType() {
		Map<String, Schema.RecordTypeInfo> agreementRT = Schema.SObjectType.Agreement__c.getRecordTypeInfosByName();
    	return agreementRT.get(ACTIVE_RT_NAME).getRecordTypeId();
    }        
}