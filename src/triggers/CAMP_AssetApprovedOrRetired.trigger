trigger CAMP_AssetApprovedOrRetired on Case (before update){
    
    Set<Id> caseId = new Set<Id>();
    Map<Id, Boolean> mapCaseApproval = new Map<Id, Boolean>();
    Map<Id, Boolean> mapCaseRetired = new Map<Id, Boolean>();
    
    for(Case c :trigger.new){
        caseId.add(c.Id);
    }
      
    List<Asset__c> assetList = new List<Asset__c>([Select id, Legally_Approved__c, 
                    Case__c, Asset_Retired__c from Asset__c where Case__c in :caseId]);
                        
    for(Asset__c a : assetList){
        
        if(a.Legally_Approved__c != null && a.Legally_Approved__c.equals('Yes')){
            if(mapCaseApproval != null && mapCaseApproval.containsKey(a.Case__c)){
                if(mapCaseApproval.get(a.Case__c)){
                    mapCaseApproval.put(a.Case__c, true); 
                }
            }else{
                mapCaseApproval.put(a.Case__c, true); 
            }
        }else{
            mapCaseApproval.put(a.Case__c, false);
        }  
        
        if(a.Asset_Retired__c != null && a.Asset_Retired__c.equals('Yes')){
            if(mapCaseRetired != null && mapCaseRetired.containsKey(a.Case__c)){
                if(mapCaseRetired.get(a.Case__c)){
                    mapCaseRetired.put(a.Case__c, true); 
                }
            }else{
                mapCaseRetired.put(a.Case__c, true); 
            }      
        }else{
            mapCaseRetired.put(a.Case__c, false);
        } 
    } 
    
    for(Case c : trigger.new){
        if(mapCaseApproval != null && mapCaseApproval.containsKey(c.id)){
            c.Legally_Approved_trigger__c = mapCaseApproval.get(c.id);
        }
        if(mapCaseRetired != null && mapCaseRetired.containsKey(c.id)){
            if(mapCaseRetired.get(c.id)){
                c.Status = 'Closed';
            }
        }
    }
  
}