/*
Name : CAMP_UpdateOwnerNamer
Author : ClearTask
Date : 13/11/2011
Usage : Trigger on Case to get the Owner Name on
        field Opportunity Owner by querying OwnerID
        from Opportunity.
*/
trigger CAMP_UpdateOwnerNamer on Case (before insert) {
     /* variable for record type Campaign Clearance */
    String RT_CC = 'Campaign Clearance';
    /* variable for Case Object */
    String OBJ_CASE = 'Case';
    /* set for Opportunity id */
    Set<Id> oppIds = new Set<Id>();
   
    
    /* query Case recordtype id for Campaign Clearance */
    List<RecordType> recordID = [select id from RecordType where 
    name = :RT_CC and sObjecttype = :OBJ_CASE limit 1];
    
    for(Case c :trigger.new){
        /* collects the ids of related Opportunity when case is inserted */
        if(c.Opportunity__c != null && c.RecordTypeId == recordId[0].id) {
            oppIds.add(c.Opportunity__c);     
        }
    }
    if(oppIds != null && oppIds.size()>0){
        Map<Id, Opportunity> oppMap = new Map<Id, Opportunity>([Select o.Owner.Name, 
        o.OwnerId From Opportunity o where Id in :oppIds]);
        if(oppMap == null){
            return;
        }else{
            for(Case c :trigger.new){
                if(c.Opportunity__c != null && c.RecordTypeId == recordId[0].id){
                    if(oppMap.containskey(c.Opportunity__c)){
                        Opportunity opp = new Opportunity();
                        opp  = oppMap.get(c.Opportunity__c);
                        c.Opportunity_Owner__c = opp.Owner.Name; 
                    }        
                }    
            }
        }
    }
    
    
}