/*
Name : CAMP_UpdateOwnerNamerOnCase
Author : ClearTask
Date : 18/11/2011
Usage : Trigger on Opportunity to update the Opportunity
        Owner Name field on Case when Opportunity Owner
        Name field on Opportunity is updated.
*/
trigger CAMP_UpdateOwnerNamerOnCase on Opportunity (after update) {
  if(UserInfo.getUserId() != Label.ADXSyncUser){//Code ADDED By Sri for Loading ADX
  
  
       Boolean flag;
       /* set for Opportunity id */
       Set<Id> oppIds = new Set<Id>();
       List<Opportunity> oppList = new List<Opportunity>();
       Set<Id> billingContactOppIds = new Set<Id>();  //updated by VG 12/04/2012 -- Billing Contact Opportunity list.
       List<Opportunity> billingContactOppList = new List<Opportunity>(); //updated by VG 12/04/2012 -- Billing Contact Opportunity list.
         Boolean billingContactChanged; //updated by VG 12/04/2012
          List<Id> primaryContactIDs = new List<Id>();  //updated by VG 12/04/2012
          Map<ID, ID> opptyContactMapIDs = new Map<ID, ID>();  //updated by VG 12/04/2012
        
        
        
       for(Opportunity  o :trigger.new){
           Opportunity oldRecord  = System.Trigger.oldMap.get(o.id);
           System.debug('oldRecord'+oldRecord );
           flag  = CAMP_Util.hasChanges('OwnerId', oldRecord, o);
          
           if(flag){
               oppIds.add(o.Id); 
               oppList.add(o); 
              
           } 
           
            billingContactChanged =  CAMP_Util.hasChanges('Primary_Billing_Contact__c', oldRecord, o); //updated by VG 12/04/2012
           //updated by VG 12/04/2012
            if(billingContactChanged){
               billingContactOppIds.add(o.Id); 
               billingContactOppList.add(o);  
                 primaryContactIDs.add(o.Primary_Billing_Contact__c);
               opptyContactMapIDs.put(o.Id, o.Primary_Billing_Contact__c);
           } 
           
       }
        
        
        
        //updated by VG 12/04/2012
        if(billingContactOppIds != null && billingContactOppIds.size()>0){
      
            IO_UpdatePaymentApprvlStatusOnCase paymentApprovalUpdateOnCase = new IO_UpdatePaymentApprvlStatusOnCase(billingContactOppIds);
            System.debug('----VG - Billing Contact Updated - In TRIGGER ' + 'billingContactOppIds size = ' + billingContactOppIds.size());
            paymentApprovalUpdateOnCase.updateCases(billingContactOppIds);
            
      //    primaryBillingContactUpdater.updateBillingContactOnIOCase(primaryContactIDs, opptyContactMapIDs);
            
        //  IO_UpdateBillingContactonIOCase primaryBillingContactUpdater = new IO_UpdateBillingContactonIOCase(billingContactOppList, billingContactOppIds);
      //      System.debug('----VG - Billing Contact Updated - In TRIGGER ' + primaryContactIDs.size() + 'opptyContactMapIDs size = ' + opptyContactMapIDs.size());   
      //    primaryBillingContactUpdater.updateBillingContactOnIOCase(primaryContactIDs, opptyContactMapIDs);
     
        }
        
        
       if(flag && oppIds != null && oppIds.size()>0){
           Map<Id, Opportunity> oppMap = new Map<Id, Opportunity>([Select (Select Id, 
                                       Record_Type__c, Opportunity_Owner__c From Cases__r where Record_Type__c =: CAMP_Util.RT_CC) , 
                                       o.Owner.Name, o.OwnerId From Opportunity o where id in :oppIds]);
                                   
           
           List<Case> caseUpdateList = new List<Case>();
           for(Opportunity  o :oppList){
               if(oppMap != null && oppMap.containsKey(o.Id)){
                   Opportunity  opp = new Opportunity ();
                   opp =  oppMap.get(o.Id);
                   List<Case> caseList =  oppMap.get(o.Id).Cases__r; 
                   for(Case c : caseList){
                       c.Opportunity_Owner__c = opp.Owner.Name;
                       caseUpdateList.add(c);                   
                   } 
               }
           } 
           if(caseUpdateList != null && caseUpdateList.size()>0){
               try{
                   update caseUpdateList; 
               }catch(Exception e){
                   System.debug('In Catch'+e.getmessage());    
               }
           }   
       }
    }
   
}