trigger CERT_TOOL_SendCertificationNotification on Cert_Tool_Employee_Reminder__c (after update) {

/*  
    Sends an email when the object is updated as a result of a WF rule field update.
    NOTE: Only send email when the Employee Cert status is in an open state (Open, Working)
*/

//  Get the status of the associated Employee Certs
//  -----------------------------------------------------------
    Map<Id, String> ecrTOcertstatus = new Map<Id, String>{};
    for(Cert_Tool_Employee_Reminder__c ecr : [select id, Employee_Certification__r.Status__c 
                                                from Cert_Tool_Employee_Reminder__c
                                                where id in: trigger.new]){
        ecrTOcertstatus.put(ecr.id, ecr.Employee_Certification__r.Status__c);
    }

//  Get the active Certification Reminders
//  -----------------------------------------------------------
    Set<Id> empSertIds = new Set<Id>();    
    Set<String> empTemplates = new Set<String>();
    List<Cert_Tool_Employee_Reminder__c> activeECRs = new List<Cert_Tool_Employee_Reminder__c>{};
    for(Cert_Tool_Employee_Reminder__c ecr :trigger.new){          
        if( ecr.Send_Notifications__c != null       && ecr.Send_Notifications__c == true    &&  
            ecr.Employee_Certification__c != null   && ecr.Email_Template__c != null        &&
            (ecrTOcertstatus.get(ecr.id) == 'Open'  || ecrTOcertstatus.get(ecr.id) == 'Working')){
            empSertIds.add(ecr.Employee_Certification__c);
            empTemplates.add(ecr.Email_Template__c);
            activeECRs.add(ecr);
        }
    }

//  Send the emails
//  -----------------------------------------------------------       
    if(!empSertIds.isEmpty()){
        Map<Id, Cert_Tool_Employee_Certification__c> empCertMap = new Map<Id, Cert_Tool_Employee_Certification__c>(
            [select Employee__r.Email__c from Cert_Tool_Employee_Certification__c where Id in :empSertIds]); 
        
        Map<String, EmailTemplate> emailTemplateMap = new Map<String, EmailTemplate>();
        for(EmailTemplate et :[select id, ownerid, Name from EmailTemplate where Name in :empTemplates]){
            emailTemplateMap.put(et.Name, et);    
        }
         
        List<Messaging.SingleEmailMessage> emailList = new List<Messaging.SingleEmailMessage>();  
        
        for(Cert_Tool_Employee_Reminder__c ecr :activeECRs){

            if(empCertMap.get(ecr.Employee_Certification__c).Employee__r.Email__c != null){
                List<String> toMail = new List<String>();
                toMail.add(empCertMap.get(ecr.Employee_Certification__c).Employee__r.Email__c);
                // system.assert(false,emailTemplateMap.get(ecr.Email_Template__c));
                Messaging.SingleEmailMessage emailObj = CERT_TOOL_CreateEmpCertification.sendEmailTemplate (
                    ''+ ecr.ownerid, 								// String strOwnerId, 
                    toMail, 										// List<String> toMail, 
                    emailTemplateMap.get(ecr.Email_Template__c), 	// EmailTemplate templateName, 
                    ecr.Employee_Certification__c 					// Id ec
                );
                if(emailObj != null)emailList.add(emailObj);               
            }   
                
        }
        
        if(emailList.size()>0)try{         
            Messaging.SendEmailResult [] r = Messaging.sendEmail(emailList);         
        }catch(Exception e){
            for(Integer i = 0; i < e.getNumDml(); i++)System.debug('Message on DML:' + e.getDmlMessage(i)); 
        }
        
    }
}