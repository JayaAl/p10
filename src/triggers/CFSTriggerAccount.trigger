trigger CFSTriggerAccount on Account(after insert, after update) {
    lmsilt.CFSAPI.fireLearningTriggers(Trigger.old, Trigger.new);
}