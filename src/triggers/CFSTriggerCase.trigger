trigger CFSTriggerCase on Case(after insert, after update) {
    lmsilt.CFSAPI.fireLearningTriggers(Trigger.old, Trigger.new);
}