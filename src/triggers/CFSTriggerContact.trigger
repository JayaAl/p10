trigger CFSTriggerContact on Contact(after insert, after update) {
    lmsilt.CFSAPI.fireLearningTriggers(Trigger.old, Trigger.new);
}