trigger CFSTriggerLead on Lead(after insert, after update) {
    lmsilt.CFSAPI.fireLearningTriggers(Trigger.old, Trigger.new);
}