trigger CFSTriggerOpportunity on Opportunity(after insert, after update) {
    lmsilt.CFSAPI.fireLearningTriggers(Trigger.old, Trigger.new);
}