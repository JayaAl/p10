trigger COGS_Header_trigger on COGS_2_0__c (after update) {


    COGS_Package_trigger_handler h = new COGS_Package_trigger_handler();
    
    if(!h.isRunning){ // ignore trigger entirely on recursive calculations
        h.isRunning = TRUE;
        h.COGSHeaderAfterUpdate(Trigger.newMap, Trigger.oldMap);
    }
}