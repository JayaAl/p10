trigger COGS_Junction_trigger on Opportunity_COGS_Junction__c (after insert, after update, after delete) {

    COGS_Package_trigger_handler h = new COGS_Package_trigger_handler();
    
    if(!h.isRunning){ // ignore trigger entirely on recursive calculations
        h.isRunning = TRUE;
        if(Trigger.isInsert){
            h.COGSJunctionBeforeInsert(Trigger.newMap);
        } else if (Trigger.isUpdate){
            // COGS_Package_trigger_handler 
            h.COGSJunctionBeforeUpdate(Trigger.newMap, Trigger.oldMap);
        } else if (Trigger.isDelete){
            h.COGSJunctionAfterDelete(Trigger.oldMap);
        }
        
    }
}