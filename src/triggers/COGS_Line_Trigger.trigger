trigger COGS_Line_Trigger on COGS_2_0_Line_Item__c (after insert, after update, after delete) {

    COGS_Package_trigger_handler h = new COGS_Package_trigger_handler();
    
    if(!h.isRunning){ // ignore trigger entirely on recursive calculations
        if(trigger.isupdate || trigger.isInsert){
            h.isRunning = TRUE;
        	h.updateSoldCOGS(Trigger.new);
        } else { // on deletions
            h.updateSoldCOGS(Trigger.old);
        }
    }
}