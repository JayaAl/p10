trigger COGS_Package_trigger on COGS_2_0_Package__c (before insert) {
	
    COGS_Package_trigger_handler h = new COGS_Package_trigger_handler();
    
    if(Trigger.isInsert){
        if(!h.isRunning){
            h.isRunning = TRUE;
            h.COGSPackageBeforeInsert( Trigger.new );
        }
    }
    /* else if (Trigger.isUpdate){
        // COGS_Package_trigger_handler 
        
    } */
}