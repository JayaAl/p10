/*
Name : COGS_UpdateOpportunityOwner
Author : ClearTask
Date : 18/11/2011
Usage : Trigger on Opportunity to update the Opportunity
        Owner Name field on Case when Opportunity Owner
        Name field on Opportunity is updated.
*/
trigger COGS_UpdateOpportunityOwner on Opportunity (after update) {
   
   if(UserInfo.getUserId() != Label.ADXSyncUser){//Code ADDED By Sri for Loading ADX
   
       Boolean flag;
       
       /* set for Opportunity id */
       Set<Id> oppIds = new Set<Id>();
       
       List<Opportunity> oppList = new List<Opportunity>();
       
           for(Opportunity  o :trigger.new){
               Opportunity oldRecord  = System.Trigger.oldMap.get(o.id);
               System.debug('oldRecord'+oldRecord);
               flag  = COGS_Util.hasChanges('OwnerId', oldRecord, o); 
               if(flag){
                   oppIds.add(o.Id); 
                   oppList.add(o);     
               } 
           }
       
       if(flag && oppIds != null && oppIds.size()>0){
           Map<Id, Opportunity> oppMap = new Map<Id, Opportunity>([Select (Select Id, 
                                       Record_Type__c, Opportunity_Owner__c From Cases__r where Record_Type__c =:COGS_Util.RT_CAC) , 
                                       o.Owner.Name, o.OwnerId From Opportunity o where id in :oppIds]);
           System.debug('oppMap '+oppMap );                             
           List<Case> caseUpdateList = new List<Case>();
           for(Opportunity  o :oppList){
               if(oppMap != null && oppMap.containsKey(o.Id)){
                   Opportunity  opp = new Opportunity ();
                   opp =  oppMap.get(o.Id);
                   System.debug('opp '+opp );
                   List<Case> caseList =  oppMap.get(o.Id).Cases__r; 
                   System.debug('caseList'+caseList);
                   for(Case c : caseList){
                       System.debug(' c.Opportunity_Owner__c'+ c.Opportunity_Owner__c);
                       c.Opportunity_Owner__c = opp.Owner.Name;
                       caseUpdateList.add(c);                   
                   } 
               }
           } 
            System.debug('caseUpdateList'+caseUpdateList);    
           if(caseUpdateList != null && caseUpdateList.size()>0){
               try{
                   update caseUpdateList; 
               }catch(Exception e){
                   System.debug('In Catch'+e.getmessage());    
               }
           }   
       }
    }
   
}