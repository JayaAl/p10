trigger COGS_updateCase on Case (before insert , before update) {
    if(Trigger.isBefore && Trigger.isInsert) {
        if(!vCaseTriggerHandler.COGS_BEFORE_INSERT) return;
        vCaseTriggerHandler.COGS_BEFORE_INSERT = false;
    } else if(Trigger.isBefore && Trigger.isUpdate) {
        if(!vCaseTriggerHandler.COGS_BEFORE_UPDATE) return;

        vCaseTriggerHandler.COGS_BEFORE_UPDATE = false;
    }

    String RT_CAC = 'COGS Approval Details';
    
    String OBJ_CASE = 'Case';
    Set<String> oppowner = new Set<String>();
    
    List<RecordType> recordID = new List<RecordType>([select id from RecordType where name = :RT_CAC and sObjecttype = :OBJ_CASE limit 1]);
   
    try{
        for(Case c : trigger.new){
            if(c.Opportunity_Owner__c != null && c.RecordTypeId == recordId[0].id){
                oppowner.add(c.Opportunity_Owner__c);
            }
            
            System.debug('----oppowner----' +oppowner);
        }
     
    
        List<User> userList = new List<User>([select Territory__c,  Name from User where Name in :oppowner]);
        Map<String, String> userMap = new Map<String , String>();
        
        for(User u : userList){
            
           userMap.put(u.Name, u.Territory__c);
        
        }
           System.debug('----userMap----' +userMap);
        for(Case cc : trigger.new){
        
            cc.Territory__c = userMap.get(cc.Opportunity_Owner__c);
        }    
    
    }Catch(Exception e){}
    
}