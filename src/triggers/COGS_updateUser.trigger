trigger COGS_updateUser on User (after update) {
    String RT_CAC = 'COGS Approval Details';
    
    String OBJ_CASE = 'Case';
    Set<String> uname = new Set<String>();
    Map<String, String> uMap = new Map<String , String>();
    List<RecordType> recordID = new List<RecordType>([select id from RecordType where name = :RT_CAC and sObjecttype = :OBJ_CASE limit 1]);
    
    for(User u : trigger.new){
       uname.add(u.Name); 
       User uOld = trigger.oldmap.get(u.id);
      
       System.debug('--uOld--' +uOld);
       
       if(uOld.Territory__c != null && uOld.Territory__c != u.Territory__c){
           uMap.put(u.Name, u.Territory__c);
       }
    }
    
    System.debug('----uname----' +uname); 
    System.debug('----uMap----' +uMap);
    
    List<Case> clist = new List<Case>([select id, Territory__c, Opportunity_Owner__c from Case where Opportunity_Owner__c in :uname and RecordTypeId = :recordID[0].id]);
    
    System.debug('----clist----' +clist);
    
    for(Case c : clist){
    
        
        if(uMap != null && uMap.containsKey(c.Opportunity_Owner__c)){
        
            c.Territory__c = uMap.get(c.Opportunity_Owner__c);
        }
    
    }
    try{ 
        if(clist != null && clist.size() > 0 ){
            
            upsert clist;
        }
    }catch(Exception e){}
    
}