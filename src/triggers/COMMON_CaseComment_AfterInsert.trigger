/*
Developer: Ralph Callaway <ralph@callawaycloudconsulting.com>
Description:
	Place all OBJECT OPERATION trigger actions in this file to provide
	to allow for easy deduction of execution order 
*/
trigger COMMON_CaseComment_AfterInsert on CaseComment (after insert) {

	// populate legal first response date and user if needed
	IOLGL_PopulateLegalFirstResponse firstResponseAdder = new IOLGL_PopulateLegalFirstResponse(trigger.new);
	firstResponseAdder.populate();

}