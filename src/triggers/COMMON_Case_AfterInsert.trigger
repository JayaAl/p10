/*
Developer: Ralph Callaway <ralph@callawaycloudconsulting.com>
Description:
    Place all OBJECT OPERATION trigger actions in this file to provide
    to allow for easy deduction of execution order 
*/
trigger COMMON_Case_AfterInsert on Case (after insert) {
	if(!VGeneralSettings.settings.EnableCaseTrigger__c) return;

    if(!vCaseTriggerHandler.EXECUTE_AFTER_INSERT) return;
    vCaseTriggerHandler.EXECUTE_AFTER_INSERT = false;
    // sync changes to approval fields back to opportunity
    IOAPR_SyncApprovalFields approvalSync = new IOAPR_SyncApprovalFields(trigger.new, trigger.oldMap);
    approvalSync.sync();

     
    
}