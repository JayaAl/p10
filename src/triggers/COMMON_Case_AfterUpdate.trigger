/*
Developer: Ralph Callaway <ralph@callawaycloudconsulting.com>
Description:
	Place all OBJECT OPERATION trigger actions in this file to provide
	to allow for easy deduction of execution order 
*/
trigger COMMON_Case_AfterUpdate on Case (after update) {
	if(!VGeneralSettings.settings.EnableCaseTrigger__c) return;

    if(!vCaseTriggerHandler.EXECUTE_AFTER_UPDATE) return;
    vCaseTriggerHandler.EXECUTE_AFTER_UPDATE = false;
    
	// sync changes to approval fields back to opportunity
	IOAPR_SyncApprovalFields approvalSync = new IOAPR_SyncApprovalFields(trigger.new, trigger.oldMap);
	approvalSync.sync();
    
       //updated by VG 12/05/2012 
    //Populate primary Contact from Opportunity, if needed
    //removed from the BEFOREupdate trigger and put it in the AFTERUpdate trigger. 
    //IOAPR_PopulateOppContactToCase primaryContactAdder = new IOAPR_PopulateOppContactToCase(trigger.new);
       //primaryContactAdder.populate();
   
 
    

}