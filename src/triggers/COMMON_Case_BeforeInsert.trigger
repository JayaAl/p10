/*
Developer: Ralph Callaway <ralph@callawaycloudconsulting.com>
Description:
	Place all OBJECT OPERATION trigger actions in this file to provide
	to allow for easy deduction of execution order 
*/
trigger COMMON_Case_BeforeInsert on Case (before insert) {
	if(!VGeneralSettings.settings.EnableCaseTrigger__c) return;

	if(!vCaseTriggerHandler.EXECUTE_BEFORE_INSERT) return;
	vCaseTriggerHandler.EXECUTE_BEFORE_INSERT = false;

    vCaseTriggerHandler handler = new vCaseTriggerHandler();
    handler.beforeInsert(Trigger.new);

	// Populate case account, if needed
	IOAPR_PopulateCaseAccount accountAdder = new IOAPR_PopulateCaseAccount(trigger.new);
	accountAdder.populate();
	
	// Populate case ad agency, if needed
	IOAPR_PopulateCaseAdAgency agencyAdder = new IOAPR_PopulateCaseAdAgency(trigger.new);
	agencyAdder.populate();
}