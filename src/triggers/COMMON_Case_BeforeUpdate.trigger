/*
Developer: Ralph Callaway <ralph@callawaycloudconsulting.com>
Description:
    Place all OBJECT OPERATION trigger actions in this file to provide
    to allow for easy deduction of execution order 
*/
trigger COMMON_Case_BeforeUpdate on Case (before update) {
    if(!VGeneralSettings.settings.EnableCaseTrigger__c) return;

    if(!vCaseTriggerHandler.EXECUTE_BEFORE_UPDATE) return;
    vCaseTriggerHandler.EXECUTE_BEFORE_UPDATE = false;
    // populate legal first response date and user if needed
    IOLGL_PopulateLegalFirstResponse firstResponseAdder = new IOLGL_PopulateLegalFirstResponse(trigger.new);
    firstResponseAdder.populate();

    
     //updated by VG 12/28/2012 - This class is deprecated. This is done via a formula field now.
    //Populate primary Contact from Opportunity, if needed
   // IOAPR_PopulateOppContactToCase primaryContactAdder = new IOAPR_PopulateOppContactToCase(trigger.new);
    
    //primaryContactAdder.populate();

    //Added by R.E. at Veltig on 12/18/2017, close milestones, if needed
    vCaseTriggerHandler.closeOpenMilestones(Trigger.NewMap,Trigger.OldMap);
    //Added by B.E.S at Veltig on 01/15/18
    vCaseTriggerHandler vHandler = new vCaseTriggerHandler();
    vHandler.beforeUpdate(Trigger.oldMap,Trigger.newMap);
    
}