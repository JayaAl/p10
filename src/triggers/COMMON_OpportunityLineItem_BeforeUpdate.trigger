/*
Developer: Ralph Callaway <ralph@callawaycloudconsulting.com>
Description:
    Place all OBJECT OPERATION trigger actions in this file to provide
    to allow for easy deduction of execution order 
*/
trigger COMMON_OpportunityLineItem_BeforeUpdate on OpportunityLineItem (before update) {


//VG - 04/02/2013 - Code Deprecated as Operative One is currently not in use.
    
//    OP1_SYNC_AutoSplitAudioEverywhere splitHelper = new OP1_SYNC_AutoSplitAudioEverywhere(trigger.new, trigger.oldMap);
 //   splitHelper.doSplit();
    
}