/*
 * Code modified by Lakshman on 18-8-2014 to consider Region, Territory and MSA along with DMA
 * Modified By: Jaya
 * Modified Date: 02/22/2017
 * Modified Purpose: RecordType query is giving 
 *                      Too many SOQL queries: 101. This query is could be avoided.
 * Modified Version: v1.1
 */
trigger DMA_UpdateOrAddDMATrigger on Account (before insert, before update) {
    //first check if the trigger is not fired from a Scheduler or custom code to perform this operation
    if(ACC_Static_Helper.runDMATrigger) {
    
        //Variables
        set <string> zipCodes = new set<string>();
        set <string> states= new set<string>();
        set <string> cities= new set<string>();
        set <string> recordTypeIds = new set<string>();
        map<string, DMA__c> zipDMA = new map<String, DMA__c>();
        map<string, DMA__c> cityStateDMA = new map<String, DMA__c>();
        map<string, string> recordTypeMap = new map<string, string>();
     
        //zip, state, city, and recordType sets
        for(Account a : Trigger.new){
            if(a.BillingPostalCode != null && a.BillingPostalCode.length() >= 5)
                zipCodes.add(a.BillingPostalCode.subString(0,5));
            if(a.BillingState != null)
                states.add(a.BillingState.toUpperCase());
            if(a.BillingCity!=null)
                cities.add(a.BillingCity.toUpperCase());
            if(a.RecordTypeId !=null)           
                recordTypeIds.add(a.RecordTypeId);
        }
        // v1.1
        // The purpose of this query is to get the 
        // recordtypId for default recordTypeId.
        Id defaultRecordTypeId = Schema.SObjectType.Account.getRecordTypeInfosByName().get('default').getRecordTypeId();
        // old-- 
        //get record type objects and make map
        /*list <RecordType> recordTypes = [Select SobjectType, Name, IsActive, Id From RecordType where id in :recordTypeIds];
        for (RecordType rt:recordTypes){
            recordTypeMap.put(rt.Id, rt.Name);      
        }*/
        
        //get list of DMAS
        list <DMA__c> dmaList = [Select Zip_Code__c, State__c, Name, DMA_Code__c, City_Name__c, Region__c, Territory__c, MSA__c, MSA_Threshold__c 
                                 From DMA__c 
                                 where Zip_Code__c in :zipCodes 
                                 or (City_Name__c in :cities and State__c in :states)] ;
        //make DMA maps
        for (DMA__c dma:dmaList){
            zipDMA.put(dma.Zip_Code__c, dma);
            cityStateDMA.put(dma.city_name__c+dma.state__c, dma);
            
        }
        
        //params needed in for loop
        Boolean isChangeOfAddress;
        DMA__c dmaMatch;
        Account oldAccount;
        String recordTypeName;
        
        for(Account a: Trigger.new){
            isChangeOfAddress = false;
            
            //check for update and if address changed
            if(Trigger.isUpdate){
                oldAccount = Trigger.oldMap.get(a.ID);
                if(a.BillingPostalCode !=null && a.BillingPostalCode.length() >= 5)
                    isChangeOfAddress = a.BillingPostalCode != oldAccount.BillingPostalCode;
                if(a.BillingPostalCode == null && a.BillingCity!=null && a.BillingState != null)
                    isChangeOfAddress = !((a.BillingCity+a.BillingState).equalsIgnoreCase(oldAccount.BillingCity+oldAccount.BillingState));
                //if((a.BillingPostalCode !=null ||(a.BillingCity!=null && a.BillingState != null)) && a.DMA_Code__c == null)
                if(a.DMA_Name__c == 'update')//this is for batch update
                    isChangeOfAddress = true;
            }
            
            dmaMatch = null;
            // v1.1 
            // old version--
            // recordTypeName = recordTypeMap.get(a.RecordTypeId);
            //update values if found in DMA maps
            // if((Trigger.isInsert || isChangeOfAddress) && (a.RecordTypeId ==null ||(recordTypeName != null && recordTypeName.equalsIgnoreCase('default')))){
            if((Trigger.isInsert || isChangeOfAddress) 
                && (a.RecordTypeId ==null 
                    ||(defaultRecordTypeId == a.RecordTypeId))) {
                // v1.1 
                // (defaultRecordTypeId == a.RecordTypeId) added in version 1.1
                a.DMA_Code__c = 0;
                a.DMA_Name__c = 'none';
                if(a.BillingPostalCode != null && a.BillingPostalCode.length() >= 5){
                    dmaMatch = zipDMA.get(a.BillingPostalCode.subString(0,5));
                }
                if(dmaMatch == null && a.BillingCity != null && a.BillingState != null){
                    dmaMatch = cityStateDMA.get(a.BillingCity.toUpperCase()+a.BillingState.toUpperCase());
                }
                if(dmaMatch != null){
                    a.DMA_Code__c = dmaMatch.DMA_Code__c;
                    a.DMA_Name__c = dmaMatch.Name;
                    a.Region__c = dmaMatch.Region__c;
                    a.Territory__c = dmaMatch.Territory__c;
                    a.MSA__c = dmaMatch.MSA__c;   
                    a.MSA_Threshold__c = dmaMatch.MSA_Threshold__c;  
                } 
                else{
                    a.DMA_Code__c = null;
                    a.DMA_Name__c = null;
                    a.Region__c = null;
                    a.Territory__c = null;
                    a.MSA__c = null;
                    a.MSA_Threshold__c = null;
                }
            }
        } 
    }   
}