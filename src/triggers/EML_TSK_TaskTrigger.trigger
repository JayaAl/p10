/*
Developer: Ralph Callaway <ralph.w.callaway@gmail.com>
Story: https://na2.salesforce.com/a0I40000004Juoi
Description:
	Changes the default status and due date for tasks with subjects that contain
	the name of an Email_Task_Process__c.  When found change the task's status and
	due date to match the value stored in the custom setting.
*/
trigger EML_TSK_TaskTrigger on Task (before insert) {

	List<Email_Task_Process__c> emailTaskProcesses = Email_Task_Process__c.getAll().values();
	
	for(Task task : trigger.new) {
		if(task.subject != null) {
			for(Email_Task_Process__c emailTaskProcess : emailTaskProcesses) {
				if(task.subject.contains(emailTaskProcess.name)) {
					task.status = emailTaskProcess.new_status__c;
					task.activityDate = system.today().addDays(emailTaskProcess.due_date_offset_days__c.intValue());
				}
			}
		}
	}
	
}