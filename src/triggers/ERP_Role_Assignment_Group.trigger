trigger ERP_Role_Assignment_Group on ERP_Role_Assignment_Group__c (after insert, after update) {

	// Send updated or inserted records to the trigger handler to determine if they need to be deleted.
	Cloud_App_Onboarding_Trigger_Handler handler = new Cloud_App_Onboarding_Trigger_Handler();
	handler.delete_ERP_Role_Assignment_Group(trigger.New);

}