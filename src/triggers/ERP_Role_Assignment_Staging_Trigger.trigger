trigger ERP_Role_Assignment_Staging_Trigger on ERP_Role_Assignment_Staging__c (
	before insert, 
	before update
) {
	Cloud_App_Onboarding_Trigger_Handler handler = new Cloud_App_Onboarding_Trigger_Handler();
	handler.updateRoleStaging(Trigger.new);
}