trigger ERP_Role_Assignment_Trigger on ERP_Role_Assignment__c (
    after insert, 
    after update,
    after delete, 
    after undelete
) {
    Set<Id> groupIds = new Set<Id>();
    
    if(Trigger.isInsert || Trigger.isUpdate){
        for(ERP_Role_Assignment__c a:Trigger.new){
            groupIds.add(a.ERP_Role_Assignment_Group__c);
        }
    }
    if(Trigger.isUpdate || Trigger.isDelete){
        for(ERP_Role_Assignment__c a:Trigger.old){
            groupIds.add(a.ERP_Role_Assignment_Group__c);
        }
    }
    
    Cloud_App_Onboarding_Trigger_Handler h = new Cloud_App_Onboarding_Trigger_Handler();
    h.populateGroupRoles(groupIds);
        
}