trigger Event_UpdateAccountTrigger on Event (before insert) {
    map<id,account> accounts = new map<id,account>();
    Set<Id> setIds = new Set<Id>();
    Set<Id> setOppIds = new Set<Id>();
    for(Event record:trigger.new) {
        if(record.whatid != null && record.whatid.getsobjecttype() == account.sobjecttype) {
            setIds.add(record.whatid);
        }
        if(record.whatid != null && record.whatid.getsobjecttype() == opportunity.sobjecttype) {
            setOppIds.add(record.whatid);
        }
    }
    if(!setOppIds.isEmpty()) {
        for(Opportunity opp: [Select Id, AccountId, Agency__c from Opportunity where Id =: setOppIds]) {
            if(opp.AccountId != null) {
                setIds.add(opp.AccountId);
            } 
            if(opp.Agency__c != null) {
                setIds.add(opp.Agency__c);
            }
        }
    }
    if(! setIds.isEmpty()) {
        for(Account acc: [Select Id, last_updated_activity__c from Account 
                             where Id =: setIds 
                             AND RecordType.Name != 'Vendor']) {
            acc.last_updated_activity__c=date.today();
            accounts.put(acc.Id, acc);
        }
        if(! accounts.isEmpty()) {
            update accounts.values();
        }
    }
}