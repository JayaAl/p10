//TO DO - Delete this class
/*
Developer: Ralph Callaway <ralph.w.callaway@gmail.com>
Description:
	Updates the "Credited to Date" field on the opportunity related to the credit notes.
	
Related Files:
	FF_BLNG_CreditAggregator.cls
*/
trigger FF_BLNG_SalesCreditNote on c2g__codaCreditNote__c (
	  after delete
	, after insert
	, after undelete
	, after update
) {
/*
	// update opportunity credit to date rollup
	FF_BLNG_CreditAggregator rollup = new FF_BLNG_CreditAggregator(
		  trigger.new
		, (trigger.isUpdate) ? trigger.oldMap : null 
	);
	rollup.updateCreditedToDate();
*/
}