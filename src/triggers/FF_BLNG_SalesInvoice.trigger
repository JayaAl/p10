//TO DO - Delete this class
/*
Developer: Ralph Callaway <ralph.w.callaway@gmail.com>
Description:
	Updates the "Invoiced to Date" field on the opportunity related to the invoice.
	
Related Files:
	FF_BLNG_InvoiceAggregator.cls
*/
trigger FF_BLNG_SalesInvoice on c2g__codaInvoice__c (
	  after delete
	, after insert
	, after undelete
	, after update
) {
/*
	// update opportunity invoiced to date rollup
	FF_BLNG_InvoiceAggregator rollup = new FF_BLNG_InvoiceAggregator(
		  trigger.new
		, (trigger.isUpdate) ? trigger.oldMap : null
	);
	rollup.updateInvoicedToDate();
*/
}