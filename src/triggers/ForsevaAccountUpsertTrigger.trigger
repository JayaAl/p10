trigger ForsevaAccountUpsertTrigger on Account (before insert, before update) {
    //nsk: 10/23 Adding below logic to control trigger execution via custom label
    if(Label.Disable_Account_Triggers!='YES'){
        List<forseva1__CollectionsPolicy__c> cops = [select Id, Name from forseva1__CollectionsPolicy__c];
        Map<String, Id> collPolMap = new Map<String, Id>();
        for (forseva1__CollectionsPolicy__c cp : cops) {
            collPolMap.put(cp.Name, cp.Id);
        }
        Id cpId;
        List<forseva1__CreditPolicy__c> crps = [select Id, Name from forseva1__CreditPolicy__c];
        Map<String, Id> crPolMap = new Map<String, Id>();
        for (forseva1__CreditPolicy__c crp : crps) {
            crPolMap.put(crp.Name, crp.Id);
        }
        for (Account acc : Trigger.new) {
            if (acc.Collections_Policy_Territory__c != null) {
                acc.forseva1__CollectionsPolicy__c = collPolMap.get(acc.Collections_Policy_Territory__c);
            }
            if (!acc.Override_Credit_Policy_Auto_Assignment__c) {
                if (acc.Type == 'Advertiser') {
                    acc.forseva1__Dunning_Enabled__c = true;
                    acc.forseva1__Credit_Policy__c = crPolMap.get('Advertiser');
                }
                else if (acc.Type == 'Ad Agency') {
                    acc.forseva1__Credit_Policy__c = crPolMap.get('Agency');
                }
            }
            if (acc.D_U_N_S__c != null) {
                acc.forseva1__DUNS_Number__c = acc.D_U_N_S__c;
            }
        }
    }//End of Disable_Account_Triggers        
}