trigger ForsevaCollectionsCaseUpdateTrigger on forseva1__CollectionsCase__c (before update) {
    forseva1__CollectionsCase__c ccOld;
    for (forseva1__CollectionsCase__c cc : Trigger.new) {
        ccOld = Trigger.oldMap.get(cc.Id);
        if (!System.isBatch()) {
        	if (cc.forseva1__Follow_Up_Date__c == null && cc.forseva1__ExpectedPaymentDate__c == null) {
                cc.addError(' The Follow-Up Date and Expected Payment Date can\'t be blank.  Please enter a date on those two fields.');
        	}
        	else if (cc.forseva1__Follow_Up_Date__c == null) {
                cc.addError(' The Follow-Up Date can\'t be blank.  Please enter a Follow-up Date.');
        	}
            else if (cc.forseva1__ExpectedPaymentDate__c == null) {
                cc.addError(' The Expected Payment Date can\'t be blank.  Please enter the Expected Payment Date.');
            }
        }
    }

}