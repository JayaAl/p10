trigger ForsevaContactUpsertTrigger on Contact (after insert, after update) {

    List<Id> contactIds = new List<Id>();
    for (Contact cntct : Trigger.new) {
        contactIds.add(cntct.Id);
    }
    List<AccountContactRole> acrsToUpsert = [select Id, AccountId, ContactId, Role 
                                             from   AccountContactRole
                                             where  ContactId in :contactIds
                                             and    Role = 'Billing Contact'];
    List<AccountContactRole> acrsToDelete = new List<AccountContactRole>();
    AccountContactRole accContactRole = null;
    for (Contact cntct : Trigger.new) {
        if (cntct.Role__c != null && (cntct.Role__c == 'Billing Contact' || cntct.Role__c == 'Collections Contact')) {
	        accContactRole = null;
	        for (AccountContactRole acr : acrsToUpsert) {
	            if (cntct.Id == acr.ContactId) {
	                accContactRole = acr;
	                break;
	            }
	        }
	        if (cntct.Role__c != null && (cntct.Role__c == 'Billing Contact' || cntct.Role__c == 'Collections Contact') && accContactRole == null) {
	            AccountContactRole acr = new AccountContactRole(AccountId = cntct.AccountId, ContactId = cntct.Id, Role = 'Billing Contact');
	            acrsToUpsert.add(acr);
	        }
        } 
        else if (cntct.Role__c == null) {
            AccountContactRole acr;
        	for (AccountContactRole acr1 : acrsToUpsert) {
        		if (acr1.AccountId == cntct.AccountId && acr1.ContactId == cntct.Id) {
        			acr = new AccountContactRole(Id = acr1.Id);
        			acrsToDelete.add(acr);
        		}
        	}
        }
    }
    upsert acrsToUpsert;
    delete acrsToDelete;

}