trigger ForsevaCorteraCPRInsertTrigger on forseva1__CorteraCPR__c (before insert) {

    for (forseva1__CorteraCPR__c cpr : Trigger.new) {
        if (cpr.CorporateFinancialSummaryLiabilities__c == null || 
            cpr.CorporateFinancialSummaryNetWorth__c == null || cpr.CorporateFinancialSummaryNetWorth__c ==  0) {
            cpr.Liabilities_to_Net_Worth_Ratio__c = null;
        }
        else {
            cpr.Liabilities_to_Net_Worth_Ratio__c = cpr.CorporateFinancialSummaryNetWorth__c/cpr.CorporateFinancialSummaryLiabilities__c;
        }
    }
    
}