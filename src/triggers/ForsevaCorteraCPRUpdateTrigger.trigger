trigger ForsevaCorteraCPRUpdateTrigger on forseva1__CorteraCPR__c (after update) {

    forseva1__CorteraCPR__c cprOld;
    List<Id> accIds = new List<Id>();
    for (forseva1__CorteraCPR__c cpr : Trigger.new) {
        accIds.add(cpr.forseva1__Account__c);
    }
    Map<Id, Account> accMap = new Map<Id, Account>([select Id, Credit_Verification2__c 
                                                    from   Account 
                                                    where  Id in :accIds]);
    List<Credit_Verification__c> cvsInserts = new List<Credit_Verification__c>();
    List<Credit_Verification__c> cvsUpdates = new List<Credit_Verification__c>();
    Map<Id, Account> accUpdateMap = new Map<Id, Account>();
    List<Account> accs = new List<Account>();
    Account acc;
    Credit_Verification__c cv;
    for (forseva1__CorteraCPR__c cpr : Trigger.new) {
        cprOld = Trigger.oldMap.get(cpr.Id);
        if (cpr.forseva1__F_Credit_Review_Status__c != null && cprOld.forseva1__F_Credit_Review_Status__c == null &&
            cpr.forseva1__F_Credit_Limit_Approved__c != null && cprOld.forseva1__F_Credit_Limit_Approved__c == null) {
            acc = accMap.get(cpr.forseva1__Account__c);
            if (acc.Credit_Verification2__c != null) {
                cv = new Credit_Verification__c(Id = acc.Credit_Verification2__c, CPR_Index_Rating__c = cpr.forseva1__CprIndexRating__c,
                                                CPR_Index_Segment__c = cpr.forseva1__CprIndexSegment__c, Risk_Rating__c = cpr.RiskScorecardRiskRating__c, 
                                                Approved_Amount__c = cpr.forseva1__F_Credit_Limit_Approved__c, Approved_By__c = 'System Integration with Cortera', 
                                                Status__c = cpr.forseva1__F_Credit_Review_Status__c != null && cpr.forseva1__F_Credit_Review_Status__c == 'Passed' ? 'Approved' : cpr.forseva1__F_Credit_Review_Status__c,
                                                Payment_Terms__c = cpr.Payment_Terms__c, Cortera_CPR__c = cpr.Id, DnB_Custom_Report__c = null);
                cvsUpdates.add(cv);
            }
            else {
                cv = new Credit_Verification__c(CPR_Index_Rating__c = cpr.forseva1__CprIndexRating__c,
                                                CPR_Index_Segment__c = cpr.forseva1__CprIndexSegment__c, Risk_Rating__c = cpr.RiskScorecardRiskRating__c, 
                                                Approved_Amount__c = cpr.forseva1__F_Credit_Limit_Approved__c, Approved_By__c = 'System Integration with Cortera', 
                                                Status__c = cpr.forseva1__F_Credit_Review_Status__c != null && cpr.forseva1__F_Credit_Review_Status__c == 'Passed' ? 'Approved' : cpr.forseva1__F_Credit_Review_Status__c,
                                                Payment_Terms__c = cpr.Payment_Terms__c, Cortera_CPR__c = cpr.Id, DnB_Custom_Report__c = null);
                cvsInserts.add(cv);
                acc = new Account(Id = cpr.forseva1__Account__c);
                accUpdateMap.put(cpr.Id, acc);
            }
        }
    }
    update cvsUpdates;
    insert cvsInserts;
    for (Credit_Verification__c cvi : cvsInserts) {
    	acc = accUpdateMap.get(cvi.Cortera_CPR__c);
    	acc.Credit_Verification2__c = cvi.Id; 
    }
    update accUpdateMap.values();

}