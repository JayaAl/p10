trigger ForsevaDnBCustomReportInsertTrigger on forseva1__DnBCustomReport__c (before insert) {

    for (forseva1__DnBCustomReport__c dnb : Trigger.new) {
        dnb.Highest_Credit_or_Zero__c = dnb.HIGH_CR__c != null ? dnb.HIGH_CR__c : 0; 
    }

}