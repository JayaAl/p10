trigger ForsevaDnBCustomReportUpdateTrigger on forseva1__DnBCustomReport__c (after update) {

    forseva1__DnBCustomReport__c dnbOld;
    List<Id> accIds = new List<Id>();
    for (forseva1__DnBCustomReport__c dnb : Trigger.new) {
        accIds.add(dnb.forseva1__Account__c);
    }
    Map<Id, Account> accMap = new Map<Id, Account>([select Id, Credit_Verification2__c 
                                                    from   Account 
                                                    where  Id in :accIds]);
    Account acc;
    Credit_Verification__c cv;
    List<Credit_Verification__c> cvsInserts = new List<Credit_Verification__c>();
    List<Credit_Verification__c> cvsUpdates = new List<Credit_Verification__c>();
    Map<Id, Account> accUpdateMap = new Map<Id, Account>();
    for (forseva1__DnBCustomReport__c dnb : Trigger.new) {
        dnbOld = Trigger.oldMap.get(dnb.Id);
        if (dnb.forseva1__F_Credit_Review_Status__c != null && dnbOld.forseva1__F_Credit_Review_Status__c == null &&
            dnb.forseva1__F_Credit_Limit_Approved__c != null && dnbOld.forseva1__F_Credit_Limit_Approved__c == null) {
            acc = accMap.get(dnb.forseva1__Account__c);
            if (acc.Credit_Verification2__c != null) {
                cv = new Credit_Verification__c(Id = acc.Credit_Verification2__c, Commercial_Credit_Score_Class__c = dnb.CMCL_CR_SCR_CLAS__c,
                                                DNB_Rating__c = dnb.USDS_DNB_RATG__c, Viability_Score__c = dnb.VBLTY_SCRCLAS_SCR__c, 
                                                D_B_Viability_Rating__c = dnb.DNB_VBLTY_RATG__c, Approved_Amount__c = dnb.forseva1__F_Credit_Limit_Approved__c, 
                                                Approved_By__c = 'System Integration with DnB', Status__c = dnb.forseva1__F_Credit_Review_Comments__c,
                                                Payment_Terms__c = dnb.Payment_Terms__c, DnB_Custom_Report__c = dnb.Id, Cortera_CPR__c = null);
                cvsUpdates.add(cv);
            }
            else {
                cv = new Credit_Verification__c(Commercial_Credit_Score_Class__c = dnb.CMCL_CR_SCR_CLAS__c, DNB_Rating__c = dnb.USDS_DNB_RATG__c, 
                                                Viability_Score__c = dnb.VBLTY_SCRCLAS_SCR__c, D_B_Viability_Rating__c = dnb.DNB_VBLTY_RATG__c, 
                                                Approved_Amount__c = dnb.forseva1__F_Credit_Limit_Approved__c, Approved_By__c = 'System Integration with DnB', 
                                                Status__c = dnb.forseva1__F_Credit_Review_Comments__c, Payment_Terms__c = dnb.Payment_Terms__c, 
                                                DnB_Custom_Report__c = dnb.Id, Cortera_CPR__c = null);
                cvsInserts.add(cv);
                acc = new Account(Id = dnb.forseva1__Account__c);
                accUpdateMap.put(dnb.Id, acc);
            }
        }
    }
    update cvsUpdates;
    insert cvsInserts;
    for (Credit_Verification__c cvi : cvsInserts) {
        acc = accUpdateMap.get(cvi.DNB_Custom_Report__c);
        acc.Credit_Verification2__c = cvi.Id; 
    }
    update accUpdateMap.values();

}