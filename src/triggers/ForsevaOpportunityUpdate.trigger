trigger ForsevaOpportunityUpdate on Opportunity (before update) {

    if (!System.isBatch() && UserInfo.getUserId() != '00540000001bMSuAAM' && UserInfo.getUserId() != '00540000001biqHAAQ') {
        //String str = 'rp: \n';
        EmailTemplate em75To99 = null, em100 = null;
        Map<Id, Id> accIdMap = new Map<Id, Id>();
        Map<Id, Id> oppIdMap = new Map<Id, Id>();
        Map<Id, Id> oppOwnerIdMap = new Map<Id, Id>();
        Opportunity oldOpp;
        for (Opportunity opp : Trigger.new) {
            oldOpp = Trigger.oldMap.get(opp.Id);
            if (opp.AccountId != null && oldOpp.Probability != null && opp.Probability != null && 
                ((opp.Probability >= 75 && opp.Probability <= 100 && oldOpp.Probability < 75) ||
                 (opp.Probability == 100 && oldOpp.Probability < 100) ||
                 (opp.Probability < 75 && oldOpp.Probability >= 75 && oldOpp.Probability <= 100))) {
                 // RP - 11/26/13: don't do calculations when the amount changes.
                 //(opp.Amount != null && opp.Amount != oldOpp.Amount))) {
                //str += 'Adding opp.id = ' + opp.Id + ' and name = ' + opp.Name + ' to list with Probability = ' + opp.Probability + ' and old Probability = ' + oldOpp.Probability + '\n';
                accIdMap.put(opp.AccountId, opp.AccountId);
                oppIdMap.put(opp.Id, opp.Id);
                oppOwnerIdMap.put(opp.OwnerId, opp.OwnerId);
            }
        }
        //Integer numberOfEmails = 0;
        Map<Id, Account> accsMap = null;
        if (oppIdMap.values().size() > 0) {
            Integer numberOfEmails = 0;
            Map<Id, Double> totalARMap = new Map<Id, Double>(); 
            List<Id> userIds = new List<Id>(), oppIds = new List<Id>(), emIds = new List<Id>();
            Map<Id, User> oppOwnerMap = new Map<Id, User>([select Id, FirstName, LastName, Email
                                                           from   User
                                                           where  Id in :oppOwnerIdMap.values()]);
            //str += 'About to query FF invoice \n';
/*            List<AggregateResult> ffInvs = [select Advertiser__c, sum(c2g__OutstandingValue__c)
                                            from   c2g__codaInvoice__c
                                            where  Advertiser__c in :accIdMap.values() 
                                            and    c2g__OutstandingValue__c <> 0
                                            and    c2g__InvoiceStatus__c = 'Complete'
                                            group by Advertiser__c];
            for (AggregateResult ar : ffInvs) {
                totalARMap.put((Id)ar.get('Advertiser__c'), (Double)ar.get('expr0'));
            }
*/

            List<forseva1__ARSummary__c> arsList = [Select forseva1__Account__r.id, forseva1__Total_AR_Balance__c 
                                from forseva1__ARSummary__c 
                                where forseva1__Account__r.id in :accIdMap.values()];
            
            for(forseva1__ARSummary__c ars : arsList){
                totalARMap.put(ars.forseva1__Account__r.id, ars.forseva1__Total_AR_Balance__c);
            }

            
            accsMap = new Map<Id, Account>([select Id, Name, Open_Balance__c, Credit_Approved_Amount__c, Pending_Approval__c
                                            from   Account 
                                            where  Id in :accIdMap.values()]);
            for (Account acc : accsMap.values()) {
                //str += 'About to set the Open Balance and Credit Limit for acc = ' + acc.Id + ' \n';
                acc.Open_Balance__c = totalARMap.get(acc.Id) != null ? (Double)totalARMap.get(acc.Id) : 0;
            }
            List<AggregateResult> otherOpps = [select AccountId, sum(Amount)
                                               from   Opportunity
                                               where  Id not in :oppIdMap.values()
                                               and    AccountId in :accIdMap.values()
                                               and    Probability >= 75
                                               and    Probability < 100
                                               group by AccountId];
            Map<Id, Double> otherOppsMap = new Map<Id, Double>();
            Double amount, billedToDate;
            //str += 'After query to get otherOpps, size = ' + otherOpps.size() + ' \n';
            for (AggregateResult ar : otherOpps) {
                amount = (Double)ar.get('expr0') == null ? 0 : (Double)ar.get('expr0');
                otherOppsMap.put((Id)ar.get('AccountId'), amount);
                //str += 'Sum of all opps for 75-99,  amount = ' + amount + '\n';
            }
            otherOpps = [select AccountId, sum(Amount), sum(Billed_To_Date__c)
                         from   Opportunity
                         where  Id not in :oppIdMap.values()
                         and    AccountId in :accIdMap.values()
                         and    Probability = 100
                         group by AccountId];
            for (AggregateResult ar : otherOpps) {
                amount = (Double)ar.get('expr0') == null ? 0 : (Double)ar.get('expr0');
                billedToDate = (Double)ar.get('expr1') == null ? 0 : (Double)ar.get('expr1');
                amount = amount - billedToDate;
                //str += 'Sum of all opps for 100,  amount = ' + amount + '\n';
                amount = (Double)otherOppsMap.get((Id)ar.get('AccountId')) == null ? amount : amount + (Double)otherOppsMap.get((Id)ar.get('AccountId'));  
                //str += 'Sum of all opps for 75-100,  amount = ' + amount + '\n';
                otherOppsMap.put((Id)ar.get('AccountId'), amount);
            }
            Double pendingApproval, oppAmount;
            Account acc;
            User oppOwner;
            Opportunity otherOpp;
            for (Opportunity opp : Trigger.new) {
                //str += 'Looping for each opportunity to check if notification is created... \n';
                if (oppIdMap.get(opp.Id) != null) {
                    acc = accsMap.get(opp.AccountId);
                    pendingApproval = otherOppsMap.get(opp.AccountId) != null ? (Double)otherOppsMap.get(opp.AccountId) : 0;
                    oppAmount = opp.Amount == null ? 0 : opp.Amount;
                    acc.Pending_Approval__c = opp.Probability >= 75 && opp.Probability <= 100 ? oppAmount + pendingApproval : pendingApproval;
                    acc.Available_Credit__c = acc.Credit_Approved_Amount__c != null ? acc.Credit_Approved_Amount__c - acc.Open_Balance__c - acc.Pending_Approval__c :
                                                                                      0.00 - acc.Open_Balance__c - acc.Pending_Approval__c;
                    accsMap.put(acc.Id, acc);
                    //str += 'For acc = ' + acc.Name + ', pending approval = ' + acc.Pending_Approval__c + ', Available Credit = ' + 
                    //       acc.Available_Credit__c + ', CL = ' + acc.Credit_Approved_Amount__c + ' & Open Balance = ' + acc.Open_Balance__c + ' \n';
                    //02/20/14: nsk - Emails needs to be sent only when the Oppty stage is "closed-won" and only once
                    if (acc.Available_Credit__c < 0 && opp.Probability == 100) {
                        
                        //if (em75To99 == null || em100 == null) {
                        if (em100 == null){
                            //em75To99 = [select Id, Name from EmailTemplate where Name = 'Credit Limit Exceeded for Opportunity 75%-99%'];
                            em100 = [select Id, Name from EmailTemplate where Name = 'Credit Limit Exceeded for Opportunity 100%'];
                        }
                        //str += 'Avalable credit < 0, about to queue email to oppOwnerId = ' + opp.OwnerId + ' & email = ' + opp.Owner.Email + 
                        //       ' for current userid = ' + UserInfo.getUserId() + ' and name = ' + UserInfo.getName() + ' and email = ' + userInfo.getUserEmail() + '\n';
                        userIds.add(opp.OwnerId);
                        oppIds.add(opp.Id);
                        
                        /*
                        if (opp.Probability == 100) {
                            emIds.add(em100.Id);
                        }
                        else {
                            emIds.add(em75To99.Id);
                        }
                        */
                        emIds.add(em100.Id);
                        numberOfEmails++;
                    }
                    if (numberOfEmails == 15 && ForsevaUtilities.isFirstRun()) {
                        ForsevaUtilities.sendEmailNotifications(userIds, oppIds, emIds, Label.Forseva_AR_Email_Address);
                        numberOfEmails = 0;
                        userIds.clear();
                        oppIds.clear();
                        emIds.clear();
                        ForsevaUtilities.setFirstRunFalse();
                    }
                }
            }
            //str += 'after processing all opps, numberOfEmails = ' + numberOfEmails + '\n';
            //String[] toMe = new String[] {'rpelaez@forseva.com'};
            //ForsevaUtilities.sendEmailNotification('Opportunity Trigger log', str, toMe);
            if (numberOfEmails > 0  && ForsevaUtilities.isFirstRun()) {
                ForsevaUtilities.sendEmailNotifications(userIds, oppIds, emIds, Label.Forseva_AR_Email_Address);
                ForsevaUtilities.setFirstRunFalse();
            }
        }
        if (accsMap != null) {
            upsert accsMap.values();
        }
    }
   
}