trigger GSA_EventTrigger on Event (after delete, after insert, after update) {
    private List<Event> newEventList = new List<Event>();
    private List<Event> p10EventList = new List<Event>();
    private List<Event> p1EventList = new List<Event>();
    private List<Event> oldEventList = trigger.old;
    CustomSetting_GSA_Public_Calendar__c prodCalendarID = CustomSetting_GSA_Public_Calendar__c.getValues('Production Deployment Calendar ID');
    if(Trigger.isInsert)
    { 
        for(Event evnt : Trigger.new)
        {
            if (evnt.OwnerId == prodCalendarID.Production_deployment_Calendar_ID__c && evnt.Location == 'P10') {
            	p10EventList.add(evnt);
           } else if (evnt.OwnerId == prodCalendarID.Production_deployment_Calendar_ID__c && evnt.Location == 'P1') {
           	  p1EventList.add(evnt);
           } else if (evnt.OwnerId == prodCalendarID.Production_deployment_Calendar_ID__c && evnt.Location == 'Prod') {
           	  newEventList.add(evnt);
           }
        }
        GSA_StoryDeployDate.updateDeployDate(p10EventList, 'P10');
        GSA_StoryDeployDate.updateDeployDate(p1EventList, 'P1');
        GSA_StoryDeployDate.updateDeployDate(newEventList, 'Prod');
    } else if (Trigger.isUpdate)
    { 
        List<Event> eventsUpdated = new List<Event>();
        List<Event> p10EventsUpdated = new List<Event>();
        List<Event> p1EventsUpdated = new List<Event>();
        for (integer x=0; x<trigger.new.size(); x++) {
            if (trigger.new[x].StartDateTime != Trigger.old[x].StartDateTime && 
            trigger.new[x].OwnerId == prodCalendarID.Production_deployment_Calendar_ID__c &&
            trigger.new[x].Location == 'P10'){
                p10EventsUpdated.add(trigger.new[x]);
            } else if(trigger.new[x].StartDateTime != Trigger.old[x].StartDateTime && 
            trigger.new[x].OwnerId == prodCalendarID.Production_deployment_Calendar_ID__c &&
            trigger.new[x].Location == 'P1'){
                p1EventsUpdated.add(trigger.new[x]);
            } else if(trigger.new[x].StartDateTime != Trigger.old[x].StartDateTime && 
            trigger.new[x].OwnerId == prodCalendarID.Production_deployment_Calendar_ID__c &&
            trigger.new[x].Location == 'Prod'){
                eventsUpdated.add(trigger.new[x]);
            }
        }
        //GSA_StoryDeployDate.updateStoryDeliveryDate(eventsUpdated);
        GSA_StoryDeployDate.updateDeployDate(p10EventsUpdated, 'P10');
        GSA_StoryDeployDate.updateDeployDate(p1EventsUpdated, 'P1');
        GSA_StoryDeployDate.updateDeployDate(eventsUpdated, 'Prod');
        
    } else if (Trigger.isDelete)
    {
        for(Event evnt : Trigger.old){
            if (evnt.OwnerId == prodCalendarID.Production_deployment_Calendar_ID__c && evnt.Location == 'Prod') {
               newEventList.add(evnt);
           } else if(evnt.OwnerId == prodCalendarID.Production_deployment_Calendar_ID__c && evnt.Location == 'P1'){
           	   p1EventList.add(evnt);
           } else if(evnt.OwnerId == prodCalendarID.Production_deployment_Calendar_ID__c && evnt.Location == 'P10'){
           	   p10EventList.add(evnt);
           }
        }
    GSA_StoryDeployDate.clearDeployEvent(newEventList, 'Prod');
    GSA_StoryDeployDate.clearDeployEvent(p1EventList, 'P1');
    GSA_StoryDeployDate.clearDeployEvent(p10EventList, 'P10');
    } 
}