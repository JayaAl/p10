/***************************************************************************************************
* Trigger : IOAPR_AutoPopulateAccount 
* This trigger populate the Case Ad_Agency__c from OpportunityPartner account Id
* Author : Cleartask
* Date : 05/11/2011
***************************************************************************************************/
trigger IOAPR_AutoPopulateAccount on Case (before insert, before update) {//
    
    /*recordTypeMap contains recordTypeId as key & RecordTypeName as value*/
    Map<Id, String> recordTypeMap = new Map<Id, String>();
    
    /*recordTypeName  set for Record Type Name*/
    Set<String> recordTypeName = new Set<String>{'Legal_IO_Approval_Request', 'Legal_IO_Approval_Case_Simple_Entry'};
    
    /*oppIds  is set of Opportunity Id*/
    Set<Id> oppIds = new Set<Id>();
    
    /*Static variables -start*/
    String AGENCYCLIENT = 'Agency Client';
    String ADAGENCY = 'Ad Agency';
    String AGENCY = 'Agency';
    String SOBJECT_TYPE = 'Case';
    String LEGALIO = 'Legal_IO_Approval_Request';//IO Approval Details
    String LEGALIOApproval = 'Legal_IO_Approval_Case_Simple_Entry'; //IO Approval Case
    /*Static variables -end*/ 
    
    List<RecordType> recordTypeList = [ select id, Name, DeveloperName from RecordType where Developername in :recordTypeName 
                                        and sObjecttype = :SOBJECT_TYPE ];
    /*loop for putting value in recordTypeMap*/
    for(RecordType r :recordTypeList){
        recordTypeMap.put(r.Id ,r.DeveloperName); 
    }
    System.debug('recordTypeMap=' + recordTypeMap);
    /*loop for adding Opportunity Id to set*/
    for(Case c :trigger.new){
        oppIds.add(c.Opportunity__c); 
    }
    
    /*partnerMap contains key as Opportunity Id & value as List of OpportunityPartner*/
    Map<Id, Set<OpportunityPartner>> partnerMap = new Map<Id, Set<OpportunityPartner>>();
    List<OpportunityPartner> oppPartnerList = [ Select o.Role,o.IsPrimary , o.OpportunityId, o.AccountTo.Type, o.AccountTo.Id, o.AccountToId
                                                From OpportunityPartner o where o.OpportunityId in :oppIds 
                                                and o.Role != :AGENCYCLIENT
                                                and o.Role != null
                                              and o.AccountTo.Type = :ADAGENCY];//and o.AccountTo.Type = :ADAGENCY
                                                
    System.debug('oppPartnerList ='+ oppPartnerList);
    
    
    /*loop for putting values in OpportunityPartner with key = opp id and value = partner list*/
    for(Case c :trigger.new){
        if (recordTypeMap.containsKey(c.RecordTypeId) ){//&& c.Ad_Agency__c == null
            for(OpportunityPartner oppPartner : oppPartnerList){
                Set<OpportunityPartner> opPartnerList = new Set<OpportunityPartner>();
                if (partnerMap != null && partnerMap.containsKey(c.Opportunity__c)){
                    opPartnerList = partnerMap.get(c.Opportunity__c);
                    opPartnerList.add(oppPartner);
                    partnerMap.put(c.Opportunity__c, opPartnerList);
               }else{
                   System.debug(' in else' + c);
                   opPartnerList.add(oppPartner);
                   partnerMap.put(c.Opportunity__c, opPartnerList);
             }
        }
     }       
    }
    System.debug('partnerMap=' + partnerMap);
    
    
    /*loop for populating value of Ad_Agency__c from OpportunityPartner AccountId*/
    Integer errorIndex = 0;
    for(Case c :trigger.new){
        System.debug('c.RecordTypeId=' + c.RecordTypeId);
        if(recordTypeMap.containsKey(c.RecordTypeId)){//&& c.Ad_Agency__c == null            
           Set<OpportunityPartner> accountPartnersSet  = partnerMap.get(c.Opportunity__c);
            if (accountPartnersSet == null) continue;
            List<OpportunityPartner> accountPartners = new List<OpportunityPartner>();
            accountPartners.addAll(accountPartnersSet);
            if(accountPartners != null && accountPartners.size() >0){
                /*If List has 1 record then we will populate first AccountToId*/
                if(accountPartners.size() == 1){
                    System.debug(' accountPartners' + accountPartners);
                    c.Ad_Agency__c = accountPartners[0].AccountToId;
                    System.debug('c.Ad_Agency__c'+c.Ad_Agency__c);
                
                }else{
                    List<Id>  agencyRolePartner = new List<Id>();
                    for(OpportunityPartner a :accountPartners){
                        if(a.Role.equals(AGENCY)){
                            agencyRolePartner.add(a.AccountToId);                            
                        }                        
                    } 
                    System.debug('agencyRolePartner'+agencyRolePartner);
                    /*If List has 1 record then we will populate partner on Opp which role is Agency*/
                    if(agencyRolePartner != null && agencyRolePartner.size() == 1){
                        c.Ad_Agency__c = agencyRolePartner[0]; 
                        System.debug('c.Ad_Agency__c'+c.Ad_Agency__c); 
                    } else if(agencyRolePartner != null && agencyRolePartner.size()> 1){
                        /*If List has more 1 record of partner on Opp which role is Agency then throw error*/
                        for(OpportunityPartner a :accountPartners){
                            if(a.IsPrimary && a.Role != null && a.Role.equals(AGENCY)){
                                 c.Ad_Agency__c = a.AccountToId;   
                            }
                            System.debug('c.Ad_Agency__c'+c.Ad_Agency__c);
                        }
                        //trigger.new[errorIndex].addError('More than one Agency found on the Opportunity Partner listing. Please update the Opportunity and create the case again.');
                    }
                }
            }   
        } 
        errorIndex ++;      
    }
}