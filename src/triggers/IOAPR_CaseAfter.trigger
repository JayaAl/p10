trigger IOAPR_CaseAfter on Case (after insert, after update) {
    List<Case> changedCases = new List<Case>();
    Set<String> approvalFields = new Set<String> {'Legal_approval_status__c', 'Revenue_IO_Approval__c','Sales_Operations_Approvals__c',
        'Billing_approval_status__c','Payment_approval__c', 'IO_Signature_Status__c'};
    
    Boolean run = (Label.Run_Case_Update != null && Label.Run_Case_Update.equalsIgnoreCase('yes'));
    if (Trigger.isUpdate) {
        for (Case c : trigger.new) {
            if (run || IOAPR_CaseUtil.hasChanges(approvalFields, trigger.oldMap.get(c.Id), c)) {
                changedCases.add(c);
            }
        }
    } else {
        changedCases = trigger.new;
    }
    IOAPR_CaseUtil.updateOpportunityFields(changedCases);
}