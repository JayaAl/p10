/***************************************************************************************************
* Trigger : IOAPR_PopulateAccntOfOpptOnCase 
* This trigger on case record, which will populate account of selected
* opportunity on creating or updating the Case.
* Author : Cleartask
* Date : 05/17/2011
***************************************************************************************************/
trigger IOAPR_PopulateAccntOfOpptOnCase on Case (before insert, before update) {
    
    /*oppIds  is set of Opportunity Id*/
    Set<Id> oppIds = new Set<Id>();
    
    /*loop for adding Opportunity Id to set*/
    for(Case c :trigger.new){
        if(c.Opportunity__c != null)
            oppIds.add(c.Opportunity__c); 
    }
    
    /*oppMap contains key as Opportunity Id & value as Opportunity */
    if(oppIds != null && oppIds.size()>0){
        Map<Id, Opportunity> oppMap = new Map<Id, Opportunity>([Select o.AccountId From Opportunity o where o.Id in :oppIds]);
        if(oppMap != null && oppMap.size()>0){            
            for(Case c :trigger.new){
                if(c.Opportunity__c != null && oppMap.containsKey(c.Opportunity__c)){
                    /*Update Case :Account with the related Account from the linked Opportunity. */
                    c.AccountId = (oppMap.get(c.Opportunity__c)).AccountId;                    
                }
            }
            
        }
    }
}