trigger IOAPR_SetPaymentApproved on Case (before insert, before update) {

    for (Case c : trigger.new) {
        String paperworkOrigin = c.Paperwork_Origin__c == null ? '' : c.Paperwork_Origin__c;
        System.debug('paperworkOrigin = ' + paperworkOrigin);
        if (Trigger.isInsert) {
            if (paperworkOrigin.equalsIgnoreCase('Pandora Paperwork - no changes')) {
                c.Original_agency_payment_term_requeste__c = 'N30';
                c.c_Original_WLI_term_requested__c = '90 days';
            }
        }
        
        if (Trigger.isUpdate) {
            String oldPaperworkOrigin = Trigger.oldMap.get(c.id).Paperwork_Origin__c == null ? '' : Trigger.oldMap.get(c.id).Paperwork_Origin__c;
            System.debug('oldPaperworkOrigin = ' + oldPaperworkOrigin);
            if (paperworkOrigin.equalsIgnoreCase('Pandora Paperwork - no changes') && (!oldPaperworkOrigin.equalsIgnoreCase('Pandora Paperwork - no changes'))) 
            {
                System.debug('Got in if: ' + oldPaperworkOrigin);
                c.Original_agency_payment_term_requeste__c = 'N30';
                c.c_Original_WLI_term_requested__c = '90 days';
            }
        }
    }
    
}