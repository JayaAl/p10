/***************************************************************************************************
* Trigger : IOAPR_UpdateCaseStatusByAutoClose 
* This trigger update Case status as
     1: if Case: Status_Trigger__c = 1, set Case: Status = Close  
     2: If Auto_Close_Tigger__c (Status_Trigger__c) = 0, set Status = Open  
* Author : Cleartask
* Date : 05/11/2011
***************************************************************************************************/
trigger IOAPR_UpdateCaseStatusByAutoClose on Case (before update) {
    /*Static variables -start*/    
    String SOBJECT_TYPE = 'Case';
    String STATUS_CLOSE = 'Closed';
    String STATUS_OPEN = 'Open';
    String PENDING_APPROVAL = 'Pending payment approval';
    String PENDING_WLI = 'Pending WLI';
    String PENDING_SALES = 'Pending Sales Operations';
    String PENDING_REVENUE = 'Pending Revenue';
    String PENDING_LEGAL = 'Pending legal';
    /*Static variables -end*/
     /*recordTypeMap contains recordTypeId as key & RecordTypeName as value*/
    Map<Id, String> recordTypeMap = new Map<Id, String>();
    
    /*recordTypeName  set for Record Type Name*/
    Set<String> recordTypeName = new Set<String>{'Legal_IO_Approval_Request', 'Legal_IO_Approval_Case_Simple_Entry'}; 
    
    List<RecordType> recordTypeList = [ select id, Name, DeveloperName from RecordType where Developername in :recordTypeName 
                                        and sObjecttype = :SOBJECT_TYPE ];
    /*loop for putting value in recordTypeMap*/
    for(RecordType r :recordTypeList){
        recordTypeMap.put(r.Id ,r.DeveloperName); 
    }
    
    
    for(Case c :trigger.new){
        if(recordTypeMap.containsKey(c.RecordTypeId) && c.Status_Trigger__c != null && c.Reason != null){
            System.debug('c.Reason'+c.Reason);
            Case beforeUpdate = trigger.oldMap.get(c.Id);
            if(beforeUpdate.Reason != c.Reason){
                c.Payment_approval__c = PENDING_APPROVAL; 
                c.Billing_approval_status__c = PENDING_WLI; 
                c.Legal_approval_status__c = PENDING_LEGAL; 
                c.Revenue_IO_Approval__c = PENDING_REVENUE; 
                c.Sales_Operations_Approvals__c = PENDING_SALES;    
            }       
        }     
    }
    /* loop for if Case: Status_Trigger__c = 1, set Case: Status = Close  
      If Status_Trigger__c = 0, set Status = Open
    */
    for(Case c :trigger.new){
         if(recordTypeMap.containsKey(c.RecordTypeId) ){        //&& c.Status_Trigger__c != null 
             if(c.Status_Trigger__c == 1){
                 c.Status =  STATUS_CLOSE;
             }else if (c.Status_Trigger__c == 0){
                 c.Status =  STATUS_OPEN;
             }    
         }
    }
}