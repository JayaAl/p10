/*
Name : IOLGL_CaseCommentLegalStatusUpdate
Author : Rohit Mehta, Apprivo
Date : 22/06/2011
Usage : Trigger on Case Comment to update fields of Case 
        depending on Case Legal Status.
        Updating the Trigger to fire only for IO Approval Details Cases
*/
/**
************************************************************************
Revision History
Author : Vinay Lakshmeesh
Date : 4/16/2012
Added Query to fetch Parent record's Record type.
Date : 4/25/2012
Modified Trigger on Case Comment to update fields of Case 
on any Approval Status
**/
trigger IOLGL_CaseCommentLegalStatusUpdate on CaseComment (before insert, before update) {
    
    Set<Id> caseId = new Set<Id>();
    String USER_ROLE = 'Legal';
    //String APPROVAL_STATUS = 'Pending legal';
    RecordType recordType = [Select id from RecordType where DeveloperName='Legal_IO_Approval_Request'];
    List<Case> updateCaseList = new List<Case>();
    Set<Id> ParentCaseIds = new Set<Id>();
    
    for(CaseComment c : trigger.new) {
        ParentCaseIds.add(c.ParentId);
    }
    
    Map<Id, Case> m = new Map<ID, Case>([select id, ownerId, RecordTypeId from case where Id IN :ParentCaseIds ]);
    
    for(CaseComment cm :trigger.new){
        if(m.get(cm.ParentId).RecordTypeId == recordType.Id)
            caseId.add(cm.ParentId);
    }
    
    List<Case> caseList = [Select Legal_approval_status__c, Legal_1st_Response_Date__c from Case where Id in :caseId]; 
    User usr = [Select u.UserRole.Name, u.UserRoleId From User u where Id = :UserInfo.getUserid()];
    
    for(Case c :caseList){
        //if(usr.UserRole.Name != null && usr.UserRole.Name.equals(USER_ROLE) && c.Legal_approval_status__c.equals(APPROVAL_STATUS) && c.Legal_1st_Response_Date__c == null){
        
          if(usr.UserRole.Name != null && usr.UserRole.Name.equals(USER_ROLE) && c.Legal_1st_Response_Date__c == null){
            c.Legal_1st_Response_Date__c = System.now();
            c.X1st_Response_User__c = usr.id;
        }
    }
    
    if(caseList != null && caseList.size() > 0){
        update caseList;
    }
  
}