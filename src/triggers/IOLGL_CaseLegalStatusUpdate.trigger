/*
Name : IOLGL_CaseLegalStatusUpdate
Author : Rohit Mehta, Apprivo
Date : 22/06/2011
Usage : Trigger on Case to update fields of Case 
        depending on Case Legal Status.
*/
trigger IOLGL_CaseLegalStatusUpdate on Case (before insert, before update) {
    Case beforeUpdate = new Case();
    String PENDING_STATUS = 'Pending legal';
    String APPROVED_STATUS = 'Final IO Approved';
    String PRE_APPROVED_STATUS ='Pre IO Approved';
    String USER_ROLE = 'Legal';
    User usr = [Select u.UserRole.Name, u.UserRoleId From User u where Id = :UserInfo.getUserid()];
    for(Case c :trigger.new){
        if(Trigger.isInsert){
            /* If Approval status is equal to Pending legal 
             *  && Legal_Pending_Date__c equal to null then 
             *  update Legal_Pending_Date__c to now date /time 
             */
            String paperwork = (c.Paperwork_Origin__c == null) ? '' : c.Paperwork_Origin__c;
            if(c.Legal_approval_status__c != null && 
               (!paperwork.equals('Pandora Paperwork - no changes')) &&
               c.Legal_approval_status__c.equals(PENDING_STATUS) && 
               c.Legal_Pending_Date__c == null){
               c.Legal_Pending_Date__c = System.now();
            }
            /* If Approval status is equal to Final IO Approved or Pre IO Approved
             *  && Legal_Approved_Date__c equal to null then 
             *  update Legal_Manual_Approval_date__c to now date /time 
             */
            if(c.Legal_approval_status__c != null && 
               (!paperwork.equals('Pandora Paperwork - no changes')) &&
               (c.Legal_approval_status__c.equals(APPROVED_STATUS) ||
               c.Legal_approval_status__c.equals(PRE_APPROVED_STATUS)) && 
               c.Legal_Approved_Date__c == null){
                   c.Legal_Manual_Approval_date__c = System.now();
            }
        }
        if(Trigger.isUpdate){
            beforeUpdate = System.Trigger.oldMap.get(c.Id);
            
            /* If Approval status is equal to Pending legal
             * && old Approval status is not equal to Pending legal
             * && Legal_Pending_Date__c equal to null then 
             * update Legal_Pending_Date__c to now date /time 
             */
         
            if(c.Legal_approval_status__c != null && 
              beforeUpdate.Legal_approval_status__c != null && 
              beforeUpdate.Legal_approval_status__c != c.Legal_approval_status__c && 
              c.Legal_approval_status__c.equals(PENDING_STATUS) && 
              c.Legal_Pending_Date__c == null){
                
                c.Legal_Pending_Date__c = System.now();
            }
        
            /* If Approval status is equal to Final IO Approved or Pre IO Approved
             * && old Approval status is not equal to Final IO Approved or Pre IO Approved
             * && Legal_Approved_Date__c equal to null then 
             * update Legal_Manual_Approval_date__c to now date /time 
             */
            String paperwork = (c.Paperwork_Origin__c == null) ? '' : c.Paperwork_Origin__c;
            if(c.Legal_approval_status__c != null && 
              beforeUpdate.Legal_approval_status__c != null && 
              beforeUpdate.Legal_approval_status__c != c.Legal_approval_status__c && 
              (!paperwork.equals('Pandora Paperwork - no changes')) &&
              (c.Legal_approval_status__c.equals(APPROVED_STATUS)||
              c.Legal_approval_status__c.equals(PRE_APPROVED_STATUS)) && 
              c.Legal_Approved_Date__c == null)
            {
                System.debug('In If Approve');
                c.Legal_Manual_Approval_date__c = System.now();
                c.Approval_Method__c = 'Manual';
            }
            
            /* If UserRole is equal to Legal
             * && new value of  Approval status is equal to Final IO Approved or Pre IO Approved
             * && old value of  Approval status is equal to Pending legal 
             * X1st_Response_User__c && Legal_1st_Response_Date__c equal to null
             * update Legal_Manual_Approval_date__c to now date /time 
             */
            if(c.Legal_approval_status__c != null && usr.UserRole.Name != null && usr.UserRole.Name.equals(USER_ROLE) && 
             (c.Legal_approval_status__c.equals(APPROVED_STATUS) ||  c.Legal_approval_status__c.equals(PRE_APPROVED_STATUS)) 
             && c.Legal_1st_Response_Date__c == null &&  c.X1st_Response_User__c == null && beforeUpdate.Legal_approval_status__c.equals(PENDING_STATUS)){
                c.Legal_1st_Response_Date__c = System.now();
                c.X1st_Response_User__c = usr.id;
            }
        }        
    }

}