trigger IO_Detail_History on IO_Detail__c (after insert, after update) {

    manageIOHistory mIOH = new manageIOHistory();
    
    // Gather the cache of previously inserted records
    Map<Id,Map<String,Set<String>>> mapRCache = manageIOHistoryHelper.queryCache();
    
    mIOH.mapRCache = mapRCache;
    
    if(Trigger.isUpdate){
        mIOH.ioUpdateTrigger(Trigger.new, Trigger.oldMap);
    } else if(Trigger.isInsert){
        //mIOH.ioInsertTrigger(Trigger.new);
    }

    // write the cache of inserted records back to the helper class for recursive reference
    manageIOHistoryHelper.setCache(mIOH.mapRCache);
    
}