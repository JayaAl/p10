trigger IntakeFormTrigger on Intake_Form__c (after update) {
    
    if(trigger.isUpdate && trigger.isAfter){
        IntakeFormTriggerHelper.AfterUpdate(trigger.new,trigger.oldMap,trigger.newMap);
    }
}