/*
Developer: Ralph Callaway <ralph.w.callaway@gmail.com>
Story: https://na2.salesforce.com/a0I40000003Y6zt
Description:
	For new Avail Request Tasks (subject = Label.LCL_AV_AvailRequestTaskSubject)
	 - Default status to Label.LCL_AV_AvailRequestDefaultStatus
	 - Default due date to tomorrow
*/
trigger LCL_AV_TaskTrigger on Task (before insert) {

	for(Task newTask : trigger.new) {
		if(newTask.subject != null && newTask.subject.contains(Label.LCL_AV_AvailRequestTaskSubject)) {
			newTask.status = Label.LCL_AV_AvailRequestDefaultStatus;
			newTask.activityDate = System.today().addDays(1);
		}
	}
	
}