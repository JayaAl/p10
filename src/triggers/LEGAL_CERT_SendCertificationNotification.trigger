trigger LEGAL_CERT_SendCertificationNotification on Emp_Certification_Reminder__c (after update) {

/*  
    Sends an email when the object is updated as a result of a WF rule field update.
    NOTE: Only send email when the Employee Cert status is in an open state (Open, Working)
*/

//  Get the status of the associated Employee Certs
//  -----------------------------------------------------------
    Map<Id, String> ecrTOcertstatus = new Map<Id, String>{};
    for(Emp_Certification_Reminder__c ecr : [select id, Employee_Certification__r.Status__c 
                                                from Emp_Certification_Reminder__c 
                                                where id in: trigger.new]){
        ecrTOcertstatus.put(ecr.id, ecr.Employee_Certification__r.Status__c);
    }

//  Get the active Certification Reminders
//  -----------------------------------------------------------
    Set<Id> empSertIds = new Set<Id>();    
    Set<String> empTemplates = new Set<String>();
    List<Emp_Certification_Reminder__c> activeECRs = new List<Emp_Certification_Reminder__c>{};
    for(Emp_Certification_Reminder__c ecr :trigger.new){          
        if( ecr.Send_Notifications__c != null       && ecr.Send_Notifications__c == true    &&  
            ecr.Employee_Certification__c != null   && ecr.Email_Template__c != null        &&
            (ecrTOcertstatus.get(ecr.id) == 'Open'  || ecrTOcertstatus.get(ecr.id) == 'Working')){
            empSertIds.add(ecr.Employee_Certification__c);
            empTemplates.add(ecr.Email_Template__c);
            activeECRs.add(ecr);
        }
    }

//  Send the emails
//  -----------------------------------------------------------       
    if(empSertIds != null && empSertIds.size()>0){
        Map<Id, Employee_Certification__c> empCertMap = new Map<Id, Employee_Certification__c>(
            [select Employee__r.Email__c from Employee_Certification__c where Id in :empSertIds]); 
        
        Map<String, EmailTemplate> emailTemplateMap = new Map<String, EmailTemplate>();
        for(EmailTemplate et :[select id, ownerid, Name from EmailTemplate where Name in :empTemplates]){
            emailTemplateMap.put(et.Name, et);    
        }
         
        List<Messaging.SingleEmailMessage> emailList = new List<Messaging.SingleEmailMessage>();  
        
        for(Emp_Certification_Reminder__c ecr :activeECRs){

            if(empCertMap.get(ecr.Employee_Certification__c).Employee__r.Email__c != null){
                List<String> toMail = new List<String>();
                toMail.add(empCertMap.get(ecr.Employee_Certification__c).Employee__r.Email__c);
                Messaging.SingleEmailMessage emailObj = LEGAL_CERT_CreateEmpCertification.sendEmailTemplate (
                                       ''+ ecr.ownerid, toMail, emailTemplateMap.get(ecr.Email_Template__c), ecr.Employee_Certification__c);
                if(emailObj != null)emailList.add(emailObj);               
            }   
                
        }
        
        if(emailList.size()>0)try{         
            Messaging.SendEmailResult [] r = Messaging.sendEmail(emailList);         
        }catch(Exception e){
            for(Integer i = 0; i < e.getNumDml(); i++)System.debug('Message on DML:' + e.getDmlMessage(i)); 
        }
        
    }
}