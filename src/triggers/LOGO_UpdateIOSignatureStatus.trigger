/*
Name : LOGO_UpdateIOSignatureStatus
Author : ClearTask
Date : Oct 7, 2011
Usage : Trigger on Case with Record Type = Logo Permission Request 
        to update the IO Signature Status field with the 
        same value in the IO Signature Status field on the Case with 
        record type = IO Approval Details that is also on the same related 
        Opportunity.
        
Revision: Nov 7, 2011 - Clear Task - Case with record type = 'Legal IO Approval Request' will also be updated 
                                      as Case with 'IO Approval Details'
*/
trigger LOGO_UpdateIOSignatureStatus on Case (before update, before insert) {
    
    /* variable for IO Approval Details record type */
    String IO_APPROVAL_CASE = 'IO Approval Details';
    
    /* variable for Logo Permission Request record type */
    String LOGO_PERMISSION_REQUEST = 'Logo Permission Request';
    
    /* variable for IO Approval Details record type */
    String LEGAL_IO_APPROVAL_REQUEST= 'Legal IO Approval Request';
    
    /* variable to store set of related opportunity id */
    Set<Id> oppId = new Set<Id>();
    
    /* map to store opportunity id and related case signature status */
    Map<Id, String> mapOppSignatureStatus = new Map<Id, String>();
    
    /* function to get the ids of Case record types -- Start */
    
    Schema.DescribeSObjectResult c = Schema.SObjectType.Case;
    Map<String,Schema.RecordTypeInfo> rtMapByName = c.getRecordTypeInfosByName();
    
    Schema.RecordTypeInfo ioApprovalCaseRT = rtMapByName.get(IO_APPROVAL_CASE);
    String ioApprovalCaseRTId = ioApprovalCaseRT.getRecordTypeId();
    
    Schema.RecordTypeInfo legalIoApprovalCaseRT = rtMapByName.get(LEGAL_IO_APPROVAL_REQUEST);
    String legalIoApprovalCaseRTId = legalIoApprovalCaseRT.getRecordTypeId();
    
    Schema.RecordTypeInfo logoPermissionRT = rtMapByName.get(LOGO_PERMISSION_REQUEST);
    String logoPermissionRTId = logoPermissionRT.getRecordTypeId();
    
    /* function to get the ids of Case record types -- End */
    
    /* create the set of opportunity ids for case 
        record type Logo Permission Request */
    for(Case ca :trigger.new){
        if(ca.RecordTypeId == logoPermissionRTId){
            oppId.add(ca.Opportunity__c);
        }
    }
    
    if(!oppId.isEmpty()){
        
        /* query the case related to opportunity of record type IO Approval Details */
        List<Case> oppList = new List<Case>([Select IO_Signature_Status__c, Opportunity__c From Case where Opportunity__c in :oppId and (RecordTypeId = :ioApprovalCaseRTId or RecordTypeId = :legalIoApprovalCaseRTId)]);
        
        /* create the map of opportunity id and case signature status value */
        for(Case o :oppList){
            mapOppSignatureStatus.put(o.Opportunity__c, o.IO_Signature_Status__c);
        }
        
        /* assign the signature status value to the case */
        for(Case o :trigger.new){
            if(mapOppSignatureStatus != null && mapOppSignatureStatus.containsKey(o.Opportunity__c)){
                o.IO_Signature_Status__c = mapOppSignatureStatus.get(o.Opportunity__c);
            }
        }
    }
}