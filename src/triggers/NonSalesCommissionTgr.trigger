trigger NonSalesCommissionTgr on Non_Sales_Commissions__c (after insert) {

    set<id> nonSalesCommissionsId=new set<Id>();
    for(Non_Sales_Commissions__c nsc: trigger.new)
        nonSalesCommissionsId.add(nsc.Id);
    
    
    if(trigger.isafter && trigger.isInsert && nonSalesCommissionsId.size()>0){
        //NonSalesCommissionHandler nsc = new NonSalesCommissionHandler();
        NonSalesCommissionHandler.EnableRecordAcessAtFuture(nonSalesCommissionsId);
    }
}