/*
Modified By: Ralph Callaway <ralph@callawaycloudconsulting.com>
*/
trigger OPT_PROD_UI_ProductScheduleTriggerBulk on OpportunityLineItem (after insert) { 

    final String REVENUE = 'Revenue';

    /*if(
        OPT_PROD_STATIC_HELPER.runForProds 
     && UserInfo.getUserId() != Label.OperativeDashboardUserId 
    )(UserInfo.getName()=='Salesforce Admin')|| 
    */
    system.debug(logginglevel.info,'username ' + UserInfo.getName());
    if((UserInfo.getUserId() == Label.ADXSyncUser)|| 
        (OPT_PROD_STATIC_HELPER.runForProds && 
     UserInfo.getUserId() != Label.OperativeDashboardUserId) // don't run when line items created by operative dashboard sync user
    // && UserInfo.getUserId() != Label.OP1_SYNC_OperativeSyncUser // don't run when line items created by operative one sync user //VG - 04/02/2013 - Code Deprecated as Operative One is currently not in use.
    ) 
    {
    
        OPT_PROD_STATIC_HELPER.runForProds = false;
        
        // Get prepped by retrieving the base information needed 
        Date currentDate; 
        Decimal numberPayments; 
        Decimal paymentAmount; 
        Decimal totalPaid;
        List<OpportunityLineItemSchedule> newScheduleObjects = new List<OpportunityLineItemSchedule>(); 
        
        // For every OpportunityLineItem record, add its associated pricebook entry 
        // to a set so there are no duplicates. 
        Set<Id> pbeIds = new Set<Id>(); 
        for (OpportunityLineItem oli : Trigger.new) {
            pbeIds.add(oli.pricebookentryid);
        } 
        
        // Query the PricebookEntries for their associated info and place the results 
        // in a map. 
        Map<Id, PricebookEntry> entries = new Map<Id, PricebookEntry>([
            select product2.Auto_Schedule__c 
            from pricebookentry where id in :pbeIds
        ]); 
        
        // For every OpportunityLineItem record, add its associated oppty 
        // to a set so there are no duplicates. 
        Set<Id> opptyIds = new Set<Id>(); 
        for (OpportunityLineItem oli : Trigger.new) {
            opptyIds.add(oli.OpportunityId);
        } 
        
        // Query the Opportunities for their associated info and place the results 
        // in a map. 
        Map<Id, Opportunity> Opptys = new Map<Id, Opportunity>([
            select CloseDate, LastModifiedById 
            from Opportunity where id in :opptyIds
        ]); 
        
        // Iterate through the changes 
        for (OpportunityLineItem item : trigger.new) { 
            if(
                entries.get(item.pricebookEntryID).product2.Auto_Schedule__c == true 
             && opptys.get(item.opportunityId).lastModifiedById != Label.OperativeDashboardUserId // don't run when opportunity was last modified by operative dashboard sync user ?? is this what we want??
             // && opptys.get(item.opportunityId).lastModifiedById != Label.OP1_SYNC_OperativeSyncUser // don't run when opportunity was last modified by operative one sync user ?? is this what we want?? //VG - 04/02/2013 - Code Deprecated as Operative One is currently not in use.
            ) {
                
                //Now, we have an item that needs to be Auto Scheduled 
                //Calculate the payment amount 
                paymentAmount = item.UnitPrice; 
                numberPayments = integer.valueOf(item.Duration__c); 
                paymentAmount = paymentAmount.divide(numberPayments,2); 
                
                // Determine which date to use as the start date. 
                if (item.ServiceDate == NULL) { 
                    currentDate = Opptys.get(item.OpportunityId).CloseDate; 
                } else { 
                    currentDate = item.ServiceDate; 
                } 
                totalPaid = 0; 
                
                // Loop though the payments 
                for (Integer i = 1;i < numberPayments;i++) { 
                    OpportunityLineItemSchedule s = new OpportunityLineItemSchedule(); 
                    s.Revenue = paymentAmount; 
                    s.ScheduleDate = currentDate; 
                    s.OpportunityLineItemId = item.id; 
                    s.Type = REVENUE; 
                    newScheduleObjects.add(s); 
                    totalPaid = totalPaid + paymentAmount;  
                    currentDate = currentDate.addDays(1); 
                } 
                
                //Now Calulate the last payment! 
                paymentAmount = item.UnitPrice - totalPaid ; 
                OpportunityLineItemSchedule s = new OpportunityLineItemSchedule(); 
                s.Revenue = paymentAmount; 
                s.ScheduleDate = currentDate; 
                s.OpportunityLineItemId = item.id; 
                s.Type = REVENUE; 
                newScheduleObjects.add(s); 
            } 
        } 
         
        if (newScheduleObjects.size() > 0) { 
            try { 
                insert newScheduleObjects; // XXX: Bad practice. If scheduling breaks we should fail the transactions so that we know it's broken and needs to be fixed!!
            } catch (System.DmlException e) { 
                for (Integer ei = 0; ei < e.getNumDml(); ei++) { 
                    System.debug(e.getDmlMessage(ei)); 
                }
                Logger.logMessage(
                    'trigger OPT_PROD_UI_ProductScheduleTriggerBulk',
                    'OpportunityLineItemSchedule insert',
                    'Insert of OpportunityLineItemSchedule failed - Err: ' + e,
                    'OpportunityLineItemSchedule',
                    '',
                    'Error'
                );
            } 
        }  
    }
}