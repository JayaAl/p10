trigger OPT_PRTNR_PartnerRoleOPPS on Opportunity (after insert,after update) {

    if(UserInfo.getUserId() != Label.ADXSyncUser){//Code ADDED By Sri for Loading ADX


        //Updated by Lakshman - Bulkified the code
        List<Partner> p = new List<Partner>();
        List<OpportunityContactRole> c = new List<OpportunityContactRole>();
        List<OpportunityContactRole> ocListtoAdd = new List<OpportunityContactRole>();
        List<OpportunityContactRole> ocListtoUpdate = new List<OpportunityContactRole>();
        List<OpportunityContactRole> billingContactOCListtoAdd = new List<OpportunityContactRole>();
        List<OpportunityContactRole> billingContactOCListtoUpdate = new List<OpportunityContactRole>();
        List<Opportunity> triggerOpp = new List<Opportunity>();
        Set<Id> oppIds = new Set<Id>();
        
        List<Partner> listPartnersToDelete = new List<Partner>();
        List<Partner> listPartnersToInsert = new List<Partner>();
        
        for(Opportunity opp: trigger.new)
        {
            oppIds.add(opp.Id);
        }
        if(oppIds.size()>0)
        {
            p = [SELECT AccountToId, Role, IsPrimary,OpportunityId FROM Partner where OpportunityId In :oppIds];
            c = [SELECT ContactId, Role, IsPrimary,OpportunityId FROM OpportunityContactRole where OpportunityId In :oppIds];
            triggerOpp = [SELECT Agency__c, Id, Agency__r.Type, Primary_Contact__c, Primary_Contact__r.Role__c, Primary_Billing_Contact__c FROM Opportunity where Id In :oppIds];
        }
        for(Opportunity opp: triggerOpp)
        {
            //perform seperate transactions for insert and update and insert only if Agency is changed
            if(Trigger.isInsert && opp.Agency__c != null)
            {
                for(Partner objP :p)
                {
                    if(objP.OpportunityId == opp.Id && objP.AccountToId == opp.Agency__c)
                    listPartnersToDelete.add(objP);
                }
                Partner op = new Partner (AccountToId = opp.Agency__c,
                                          IsPrimary = true,
                                          Role = opp.Agency__r.Type,
                                          OpportunityId = opp.Id);
                listPartnersToInsert.add(op);                                                           
                
            } else if(Trigger.isUpdate && opp.Agency__c != null && opp.Agency__c != Trigger.oldMap.get(opp.Id).Agency__c)
            {
                for(Partner objP :p)
                {
                    if(objP.OpportunityId == opp.Id && objP.AccountToId == opp.Agency__c)
                    listPartnersToDelete.add(objP);
                }
                Partner op = new Partner (AccountToId = opp.Agency__c,
                                          IsPrimary = true,
                                          Role = opp.Agency__r.Type,
                                          OpportunityId = opp.Id);
                listPartnersToInsert.add(op);   
            }
            
            //perform seperate transactions for insert and update and insert only if Primary Contact is changed
            if(Trigger.isInsert && opp.Primary_Contact__c != null)
            {
                Boolean contactFlag = false;            
                for(OpportunityContactRole objC :c)
                {
                    if(objC.ContactId == opp.Primary_Contact__c && objC.OpportunityId == opp.Id)
                    {
                     objC.IsPrimary = true;
                     objC.Role = opp.Primary_Contact__r.Role__c;
                     contactFlag = true;
                    }
                }
                if(contactFlag)
                ocListtoUpdate.addAll(c);
                else
                {
                    OpportunityContactRole oc = new OpportunityContactRole (ContactId = opp.Primary_Contact__c,
                                                                            IsPrimary = true,
                                                                            Role = opp.Primary_Contact__r.Role__c,
                                                                            OpportunityId = opp.Id);
                       ocListtoAdd.add(oc);                                                                 
                }
            } else if(Trigger.isUpdate && opp.Primary_Contact__c != null && opp.Primary_Contact__c != Trigger.oldMap.get(opp.Id).Primary_Contact__c)
            {
                Boolean contactFlag = false;            
                for(OpportunityContactRole objC :c)
                {
                    if(objC.ContactId == opp.Primary_Contact__c && objC.OpportunityId == opp.Id)
                    {
                     objC.IsPrimary = true;
                     objC.Role = opp.Primary_Contact__r.Role__c;
                     contactFlag = true;
                    }
                }
                if(contactFlag)
                ocListtoUpdate.addAll(c);
                else
                {
                    OpportunityContactRole oc = new OpportunityContactRole (ContactId = opp.Primary_Contact__c,
                                                                            IsPrimary = true,
                                                                            Role = opp.Primary_Contact__r.Role__c,
                                                                            OpportunityId = opp.Id);
                       ocListtoAdd.add(oc);                                                                 
                }
            }
            
            //updated by VG 11/28/2012 - Copy over the Primary Billing contact if not null
            //perform seperate transactions for insert and update and insert only if Primary Contact is changed
             if(Trigger.isInsert && opp.Primary_Billing_Contact__c != null)
            {
                Boolean contactFlag = false;            
                for(OpportunityContactRole objC :c)
                {
                    if(objC.ContactId == opp.Primary_Billing_Contact__c && objC.OpportunityId == opp.Id && objC.Role == 'Billing Contact')
                    {
                    // objC.IsPrimary = true;
                     objC.Role ='Billing Contact';
                     contactFlag = true;
                    }
                }
                if(contactFlag)
                billingContactOCListtoUpdate.addAll(c);
                else
                {
                    OpportunityContactRole oc = new OpportunityContactRole (ContactId = opp.Primary_Billing_Contact__c,
                                                                           // IsPrimary = true,
                                                                            Role = 'Billing Contact',
                                                                            OpportunityId = opp.Id);
                    billingContactOCListtoAdd.add(oc);                                                                   
                }
            } else if(Trigger.isUpdate && opp.Primary_Billing_Contact__c != null && opp.Primary_Billing_Contact__c != Trigger.oldMap.get(opp.Id).Primary_Billing_Contact__c)
            {
                Boolean contactFlag = false;            
                for(OpportunityContactRole objC :c)
                {
                    if(objC.ContactId == opp.Primary_Billing_Contact__c && objC.OpportunityId == opp.Id && objC.Role == 'Billing Contact')
                    {
                    // objC.IsPrimary = true;
                     objC.Role ='Billing Contact';
                     contactFlag = true;
                    }
                }
                if(contactFlag)
                billingContactOCListtoUpdate.addAll(c);
                else
                {
                    OpportunityContactRole oc = new OpportunityContactRole (ContactId = opp.Primary_Billing_Contact__c,
                                                                           // IsPrimary = true,
                                                                            Role = 'Billing Contact',
                                                                            OpportunityId = opp.Id);
                    billingContactOCListtoAdd.add(oc);                                                                   
                }
            }
        }
        if(! listPartnersToDelete.isEmpty()){
            try{
                  system.debug('listPartnersToDelete Size =  ' + listPartnersToDelete.size())   ;
                Database.delete(listPartnersToDelete, false);
            } catch(Exception ex){
                system.debug('Ex =>' + ex.getMessage() + 'listPartnersToDelete Size =  ' + listPartnersToDelete.size()) ;
            }
        }
        
        if(! listPartnersToInsert.isEmpty()){
            system.debug('listPartnersToInsert Size =  ' + listPartnersToInsert.size()) ;
            Database.insert(listPartnersToInsert, false);
        }
        if(! ocListtoUpdate.isEmpty()){
            try{
                system.debug('OC LIST Size =  ' + ocListtoUpdate.size())    ;
                Database.update(ocListtoUpdate, false);
            } catch(Exception ex){
                system.debug('Ex =>' + ex.getMessage() + 'OC LIST Size =  ' + ocListtoUpdate.size())    ;
            }
        }
        if(! ocListtoAdd.isEmpty()){
            try{
                system.debug('ocListtoAdd Size =  ' + ocListtoAdd.size())   ;
                Database.insert(ocListtoAdd, false);
            } catch(Exception ex){
                system.debug('Ex =>' + ex.getMessage() + 'ocListtoAdd Size =  ' + ocListtoAdd.size())   ;
            }
        }
        if(! billingContactOCListtoUpdate.isEmpty()){
            try{
                 system.debug('billingContactOCListtoUpdate Size =  ' + billingContactOCListtoUpdate.size())    ;
                Database.update(billingContactOCListtoUpdate, false);
            } catch(Exception ex){
                system.debug('Ex =>' + ex.getMessage() + 'billingContactOCListtoUpdate Size =  ' + billingContactOCListtoUpdate.size()) ;
            }
        }
        if(! billingContactOCListtoAdd.isEmpty()){
            try{
                  system.debug('billingContactOCListtoAdd Size =  ' + billingContactOCListtoAdd.size()) ;
                Database.insert(billingContactOCListtoAdd, false);
            } catch(Exception ex){
                system.debug('Ex =>' + ex.getMessage() + 'billingContactOCListtoAdd Size =  ' + billingContactOCListtoAdd.size())   ;
            }
        }
    }
}