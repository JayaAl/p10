/**
 * @name: OpportunityTrigger
 * @desc: Single Trigger for Opportunity
 * @author: Abhishek
 * @Description : Single Trigger Per Object : Combined al the Triggers into one common trigger 
 *                OpportunityTrigger -> OpptyTriggerHandler -> OpportunityTriggerHandlerHelper --> OpportunityTriggerUtil
 */
trigger OpportunityTrigger on Opportunity (before insert,after insert,before update,after update,after delete,after undelete) {

    OpptyTriggerHandler opportunityTriggerHandlerObj = new OpptyTriggerHandler(Trigger.isExecuting, Trigger.size);

    if(trigger.isInsert && trigger.isBefore ){      
        // Before insert Handler invocation
        opportunityTriggerHandlerObj.onBeforeInsert(Trigger.new);

    }else if(trigger.isInsert && trigger.isAfter ){
        // After insert Handler invocation
        opportunityTriggerHandlerObj.onAfterInsert(Trigger.new,Trigger.newMap);

    }
    else if(trigger.isUpdate && trigger.isBefore ){
        // Before Update Handler invocation
        opportunityTriggerHandlerObj.onBeforeUpdate(Trigger.new,Trigger.oldMap, Trigger.newMap);        
        
    }else if(trigger.isUpdate && trigger.isAfter){      
        // After Update Handler invocation       
        opportunityTriggerHandlerObj.onAfterUpdate(Trigger.new,Trigger.oldMap,Trigger.newMap);

    }else if(trigger.isDelete && trigger.isAfter){
        // After Delete Handler invocation
        opportunityTriggerHandlerObj.onAfterDelete(Trigger.oldMap); 

    }else if(trigger.isUnDelete && trigger.isAfter){
        // After Delete Handler invocation
        opportunityTriggerHandlerObj.onAfterUnDelete(Trigger.new);  
            
    }
}