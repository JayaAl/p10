trigger OpptySplitOwner on Opportunity_Split__c (before insert, before update) {
    /**
    * Updates the Owner field on Opportunity Splits to match the Salesperson User record.
    *
    * @author Casey Grooms cgroms@pandora.com
    * @version 1.0
    * @date 2014-03-14
    *
    */
    
    /*
    if insert we need to always perform the update
    If update check if there was an update to the Salesperson, if so then update the Owner
	only ever update the owner if the Salesperson is Active
    */
    
    Map<Id, List<Opportunity_Split__c>> mapOwnerToOSList = new Map<Id, List<Opportunity_Split__c>>();
    
    // Collect all Opportunity_Split__c records that need to be updated in a Map by the Salesperson__c Id
    for(Opportunity_Split__c os:Trigger.new){
        if(os.Salesperson__c != NULL && os.OwnerId != os.Salesperson__c){
            if(!mapOwnerToOSList.containsKey(os.Salesperson__c)){
                mapOwnerToOSList.put(os.Salesperson__c, new List<Opportunity_Split__c>());
            }
            mapOwnerToOSList.get(os.Salesperson__c).add(os);
        }
    }
    
    // Query the Salesperson__c Ids collected above, limited to only Active users,
    // For each User found update the Opportunity_Split__c records associated to them
    for(User u:[Select Id from User Where Isactive = TRUE And Id In :mapOwnerToOSList.keySet()]){
        for(Opportunity_Split__c o: mapOwnerToOSList.get(u.Id)){
            o.OwnerId = o.Salesperson__c;
        }
    }
	
    /* Permutations to test:
     * New Opportunity creation
     * Adding a new Split to an existing set
     * Updating an existing split
     * Deleting a Split
     * Changing the Owner of an Oppty that has only one Split
     */
}