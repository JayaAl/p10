trigger ReceiptJunctionTrigger on Receipt_Junction__c (after insert,after update,before insert,before update) {
    
    ReceiptJunctionTriggerHandler handler = new ReceiptJunctionTriggerHandler();
    if(Boolean.valueOf(system.label.execute_ReceiptJunctionLogic)){
        if(Trigger.isAfter && Trigger.isInsert){
            handler.onAfterInsert(Trigger.new);
        }
        else if(Trigger.isAfter && Trigger.isUpdate){
            handler.onAfterUpdate(Trigger.new,Trigger.oldMap);
        }
        else if(Trigger.isBefore && Trigger.isUpdate){
            handler.onBeforeUpdate(Trigger.new,Trigger.oldMap);
        }
        else if(Trigger.isBefore && Trigger.isInsert){
            handler.onBeforeInsert(Trigger.new);
        }
    }
}