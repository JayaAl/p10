/**
 * @name: SA_PerformSplitsOperations 
 * @desc: Used to perform insert, delete, update operations Opportunity_Splits__c object
 * @author: Lakshman(sfdcace@gmail.com)
 * @date: 03-06-2014
 */
 /*
  * Lakshman: Bug fix for Issue:ISS-9592 on date: 08/15/2014
  */
trigger SA_PerformSplitsOperations on Splits_Approval__c (after update) {
    Set<Id> setOfSplits = new Set<Id>();
    Set<Id> setSplitsRejected = new Set<Id>();
    for(Splits_Approval__c sa : Trigger.new) {
        if(Trigger.oldMap.get(sa.Id).Status__c != sa.Status__c && sa.Status__c == 'Approved') {
            setOfSplits.add(sa.Id);
        } else if(Trigger.oldMap.get(sa.Id).Status__c != sa.Status__c && sa.Status__c == 'Rejected') {
            setSplitsRejected.add(sa.Opportunity__c);
        }
    }
    
    if(! setOfSplits.isEmpty()) {
        List<Opportunity_Split__c> listOfSplitsToUpsert = new List<Opportunity_Split__c>();
        List<Opportunity_Split__c> listOfSplitsToDelete = new List<Opportunity_Split__c>();
        List<Opportunity> listOfOpportunityToUpdate = new List<Opportunity>();
        Set<Id> setOfOpportunityIds = new Set<Id>();
        for(Splits_Approval__c sa: [Select Id, Opportunity__c , 
                                            X2nd_Salesperson__c,//Queried SP fields by Lakshman on 04-09-2014
                                            X3rd_Salesperson__c,
                                            X4th_Salesperson__c,
                                            X5th_Salesperson__c,
                                            X6th_Salesperson__c,
                                            
                                            X1st_Salesperson_Split__c,
                                            X2nd_Salesperson_Split__c,
                                            X3rd_Salesperson_Split__c,
                                            X4th_Salesperson_Split__c,
                                            X5th_Salesperson_Split__c,
                                            X6th_Salesperson_Split__c,
                                            X1st_Salesperson_Role_Type__c,
                                            X2nd_Salesperson_Role_Type__c,
                                            X3rd_Salesperson_Role_Type__c,
                                            X4th_Salesperson_Role_Type__c,
                                            X5th_Salesperson_Role_Type__c,
                                            X6th_Salesperson_Role_Type__c,
                                            
                                            
                                        (Select Operation__c, 
                                         Opportunity_Owner__c, 
                                         Opportunity_Split__c,  
                                         Opportunity_Split__r.Salesperson__c,
                                         Salesperson__c, 
                                         Split__c,
                                         Role_Type__c, 
                                         Splits_Approval__r.Opportunity__c , 
                                         CurrencyIsoCode
                                    from Split_Changes__r Order By CreatedDate)
                                    from Splits_Approval__c
                                    where Id =: setOfSplits ]) {
            
            Opportunity opp = new Opportunity(Id = sa.Opportunity__c, 
                                              X2nd_Salesperson__c = sa.X2nd_Salesperson__c,//Setting SP fields by Lakshman on 04-09-2014
                                              X3rd_Salesperson__c = sa.X3rd_Salesperson__c,
                                              X4th_Salesperson__c = sa.X4th_Salesperson__c,
                                              X5th_Salesperson__c = sa.X5th_Salesperson__c,
                                              X6th_Salesperson__c = sa.X6th_Salesperson__c, 
                                              X1st_Salesperson_Split__c = sa.X1st_Salesperson_Split__c,
                                              X2nd_Salesperson_Split__c = sa.X2nd_Salesperson_Split__c,
                                              X3rd_Salesperson_Split__c = sa.X3rd_Salesperson_Split__c,
                                              X4th_Salesperson_Split__c = sa.X4th_Salesperson_Split__c,
                                              X5th_Salesperson_Split__c = sa.X5th_Salesperson_Split__c,
                                              X6th_Salesperson_Split__c = sa.X6th_Salesperson_Split__c,
                                              Splits_Locked__c = false);

            Integer idx = 0;
            for(Split_Changes__c sc: sa.Split_Changes__r) {
                if(sc.Operation__c == 'INSERT') {
                    listOfSplitsToUpsert.add(new Opportunity_Split__c(Opportunity__c = sc.Splits_Approval__r.Opportunity__c,
                                                                      Opportunity_Owner__c = sc.Opportunity_Owner__c,
                                                                      Salesperson__c = sc.Salesperson__c,
                                                                      Split__c = sc.Split__c,
                                                                      Role_Type__c = sc.Role_Type__c, 
                                                                      CurrencyIsoCode = sc.CurrencyIsoCode
                                                                      ));
                } else if(sc.Operation__c == 'UPDATE') {
                    listOfSplitsToUpsert.add(new Opportunity_Split__c(Id = sc.Opportunity_Split__c, 
                                                                      Opportunity__c = sc.Splits_Approval__r.Opportunity__c,
                                                                      Opportunity_Owner__c = sc.Opportunity_Owner__c,
                                                                      Salesperson__c = sc.Salesperson__c,
                                                                      Split__c = sc.Split__c,
                                                                      Role_Type__c = sc.Role_Type__c, 
                                                                      CurrencyIsoCode = sc.CurrencyIsoCode
                                                                      ));
                } else if(sc.Operation__c == 'DELETE') {//modified delete operation by Lakshman on 04-09-2014
                    listOfSplitsToDelete.add(new Opportunity_Split__c( Id = sc.Opportunity_Split__c));
                    /*code commented by Lakshman on 04-09-2014 as we copy Salesperson info directly in Splits Approval object
                    if(opp.X2nd_Salesperson__c == sc.Opportunity_Split__r.Salesperson__c) {
                        opp.X2nd_Salesperson__c = null;
                        opp.X2nd_Salesperson_Split__c = 0;
                    } else if(opp.X3rd_Salesperson__c == sc.Opportunity_Split__r.Salesperson__c) {
                        opp.X3rd_Salesperson__c = null;
                        opp.X3rd_Salesperson_Split__c = 0;
                    } else if(opp.X4th_Salesperson__c == sc.Opportunity_Split__r.Salesperson__c) {
                        opp.X4th_Salesperson__c = null;
                        opp.X4th_Salesperson_Split__c = 0;
                    }*/
                } else if(sc.Operation__c == 'OWNER CHANGE') {
                    opp.OwnerId = sc.Salesperson__c;
                }
                
                /* code commented by Lakshman on 04-09-2014 as we copy Salesperson info directly in Splits Approval object
                if(!sc.Opportunity_Owner__c && (sc.Operation__c == 'INSERT' || sc.Operation__c == 'UPDATE')) { 
                    if(idx == 0) {
                        opp.X2nd_Salesperson__c = sc.Salesperson__c;//ISS-9592 bug fix, need to set 2nd Salesperson at 0th index
                        opp.X2nd_Salesperson_Split__c = sc.Split__c;
                        opp.X3rd_Salesperson_Split__c = 0;
                        opp.X3rd_Salesperson__c = null;
                        opp.X4th_Salesperson_Split__c = 0;
                        opp.X4th_Salesperson__c = null; 
                        opp.Splits_Locked__c = false;
                        setOfOpportunityIds.add(sa.Opportunity__c);
                        opp.X1st_Salesperson_Split__c = sc.Split__c;
                    } else if(idx == 1) {
                        opp.X3rd_Salesperson__c = sc.Salesperson__c;
                        opp.X3rd_Salesperson_Split__c = sc.Split__c;
                    } else if(idx == 2) {
                        opp.X4th_Salesperson__c = sc.Salesperson__c;
                        opp.X4th_Salesperson_Split__c = sc.Split__c;
                    } 
                    idx ++;
                }*/
            } 
            listOfOpportunityToUpdate.add(opp);
            setOfOpportunityIds.add(opp.Id);//Lakshman: Added on 30/10/2014 as Evaluate Splits was never getting called
        }
    
        if( !listOfSplitsToUpsert.isEmpty()) {
            upsert listOfSplitsToUpsert;
        }
        if( !listOfSplitsToDelete.isEmpty()) {
            delete listOfSplitsToDelete;
        }
        if( !listOfOpportunityToUpdate.isEmpty()) {
            update listOfOpportunityToUpdate;
            if( !setOfOpportunityIds.isEmpty()) {
                SPLT_ManageSplitDetails.evaluateSplitTypes(setOfOpportunityIds);
            }
        }
    } else {
        List<Opportunity> listOfOpportunityToUpdate = new List<Opportunity>();
        for(Id id: setSplitsRejected) {
            listOfOpportunityToUpdate.add(new Opportunity(Id = id, Splits_Locked__c = false));
        }
        if(! listOfOpportunityToUpdate.isEmpty()) {
            update listOfOpportunityToUpdate;
        }
    }
}