trigger SCAL_populateDataValuesOnSponsorship on Sponsorship__c (before insert, before update) {

/*
     Set<String> sponsorshipTypeSet = new Set<String>();
     boolean isChanged = false;
     Sponsorship__c oldSponsorshipInfo = null;
     for(Sponsorship__c sponsorshipInfo : Trigger.new ) {
         isChanged = false;
         if(Trigger.isInsert) {
             isChanged = true;  
         } else if (Trigger.isUpdate) {
            oldSponsorshipInfo = trigger.oldMap.get(sponsorshipInfo.Id);
            if (SCAL_Utils.hasChanges('Type__c', oldSponsorshipInfo, sponsorshipInfo) || 
                SCAL_Utils.hasChanges('Status__c', oldSponsorshipInfo, sponsorshipInfo) ||
                SCAL_Utils.hasChanges('Date__c', oldSponsorshipInfo, sponsorshipInfo) || 
                SCAL_Utils.hasChanges('End_Date__c', oldSponsorshipInfo, sponsorshipInfo)) {
                isChanged = true;   
            }
         }  
         if (isChanged) { 
             sponsorshipTypeSet.add(sponsorshipInfo.Type__c); 
         }  
     } 
     
     if (sponsorshipTypeSet != null && sponsorshipTypeSet.size() > 0) {
         List<Sponsorship_Type__c>  sponsorshipTypeList =[select id,name,Type__c,Exclusive__c,Minimum_Gap__c , Max_per_Week__c ,Duration__c,Interval__c,Max_Per_Month__c from Sponsorship_Type__c where Type__c IN : sponsorshipTypeSet];
         if (sponsorshipTypeList != null && sponsorshipTypeList.size() > 0) {
            Map<String, Sponsorship_Type__c> sponsorshipByTypeMap = new Map<String, Sponsorship_Type__c>();
            for(Sponsorship_Type__c sponsorshipTypeInfo : sponsorshipTypeList) {
                sponsorshipByTypeMap.put(sponsorshipTypeInfo.Type__c, sponsorshipTypeInfo);
            }
            Sponsorship_Type__c currentSponsorshipTypeInfo = null;
            Integer year = 0;
            Integer month = 0;
            Integer currentMonth = System.today().month();
            Integer currentYear = System.today().year();
            Map<String,List<Sponsorship__c>> sponsorshipListByTypeMap = SCAL_Utils.getAllSponsorshipsByType(sponsorshipTypeSet); //VG - Commented code - 1/07/2013 - moved code to SCAL_sponsorshipController.
            
            Integer sponsorshipCount = 0;
            for(Sponsorship__c sponsorshipInfo : Trigger.new ) {
                currentSponsorshipTypeInfo = sponsorshipByTypeMap.get(sponsorshipInfo.Type__c);
                if (currentSponsorshipTypeInfo != null) {
                    if (sponsorshipInfo.Date__c != null) {
                        if(Trigger.isInsert) {
                            SCAL_Utils.populateEndDateOnSponsorship(currentSponsorshipTypeInfo, sponsorshipInfo);
                        }
                      
                  
                        sponsorshipCount = SCAL_Utils.getSponsorshipCountFallsInCurrentSponsorshipDate(sponsorshipInfo, sponsorshipListByTypeMap.get(sponsorshipInfo.Type__c) , Trigger.isUpdate); 
                        System.debug('sponsorshipCount----------->'+ sponsorshipCount);
                        if (currentSponsorshipTypeInfo.Exclusive__c && sponsorshipCount > 0) {
                            sponsorshipInfo.addError('There is '+sponsorshipInfo.Type__c +' scheduled for this period.');
                        }
                        

                        if (currentSponsorshipTypeInfo.Max_Per_Month__c != null && sponsorshipCount > currentSponsorshipTypeInfo.Max_Per_Month__c) {
                            sponsorshipInfo.addError('Max allowed  for this month '+ sponsorshipInfo.Type__c +' is already reached.');
                        }
                        
                       Integer sponsorshipWeekCount = 0;
                       Sponsorship__c oldValue = new Sponsorship__c();
                       if(Trigger.Isupdate){
                           oldValue = Trigger.oldMap.get(sponsorshipInfo.Id );
                       
                       }
                     
        if (currentSponsorshipTypeInfo.Max_per_Week__c != null ){ 
                           //if((Trigger.Isinsert) || (oldValue != null && oldValue.Date__c != sponsorshipInfo.Date__c))
                           System.debug('currentSponsorshipTypeInfo.Max_per_Week__c--'+currentSponsorshipTypeInfo.Max_per_Week__c);
                       
                           //Commented by VG - 07/18/2012
                           //code will be moved to the SCAL_Sponsorship_Controller
                           // sponsorshipWeekCount  =  SCAL_Utils.getAllSponsorshipsByTypeforWeek(sponsorshipInfo, Trigger.isUpdate); 
                           //System.debug('sponsorshipWeekCount--- '+sponsorshipWeekCount );
                           //if(sponsorshipWeekCount >= currentSponsorshipTypeInfo.Max_per_Week__c) 
                             //  sponsorshipInfo.addError('There can be a maximum of ' + currentSponsorshipTypeInfo.Max_per_Week__c  + ' '  + sponsorshipInfo.Type__c + ' Sponsorships per Week ');
                       }
                       
                       
                            //Commented by VG - 07/18/2012
                           //code will be moved to the SCAL_Sponsorship_Controller
                Integer sponsorshipMinCount = 0;
                       if(currentSponsorshipTypeInfo.Minimum_Gap__c != null){
                            //if((Trigger.Isinsert) || (oldValue != null && oldValue.Date__c != sponsorshipInfo.Date__c))
                            sponsorshipMinCount = SCAL_Utils.getSponsorshipCountFallsInMinGapCurrentSponsorshipDate(sponsorshipInfo, Trigger.isUpdate, currentSponsorshipTypeInfo);
                            System.debug('sponsorshipMinCount '+sponsorshipMinCount );
                            if(sponsorshipMinCount != 0)
                                sponsorshipInfo.addError('A minimum gap of ' + currentSponsorshipTypeInfo.Minimum_Gap__c + ' days is required between two ' +  sponsorshipInfo.Type__c + ' Sponsorships');      
                            
                       }
                                            
                    }
                }
            }
            
         }
     }
     
     */

     
}