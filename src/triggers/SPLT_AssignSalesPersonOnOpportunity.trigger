trigger SPLT_AssignSalesPersonOnOpportunity on Opportunity (before insert, before update) {
    if(Trigger.isInsert){
        // Setting split and associated Salesperson fields to 0 and null.
        SPLT_OpportunitySplit.initializeOpportunityFields(trigger.new);      
        SPLT_OpportunitySplit.Assign1stSalesperson(trigger.new);
    }
    
    //if(Trigger.isUpdate){//Code COMMENTED By Sri for Loading ADX
    if(Trigger.isUpdate && UserInfo.getUserId() != Label.ADXSyncUser){//Code ADDED By Sri for Loading ADX
        SPLT_OpportunitySplit.Assign1stSalesperson(trigger.new);  
    }
}

/*
trigger OpportunityBefore on Opportunity (before insert, before update) {
  OpportunitySplit.Assign1stSalesperson(trigger.new);
}
*/