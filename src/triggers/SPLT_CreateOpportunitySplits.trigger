/**
* This trigger creates and initializes a new Opportunity split object for every new opportunity.
* @Author: Bharath Kumar Gadiyaram.
*/
trigger SPLT_CreateOpportunitySplits on Opportunity (after insert) {
    
    if(UserInfo.getUserId() != Label.ADXSyncUser){//Code ADDED By Sri for Loading ADX


        Opportunity[] opportunityArray = Trigger.new;
        List<Opportunity_Split__c> opportunitySplitList = new List<Opportunity_Split__c>();
        for(Opportunity opportunity:opportunityArray){
            Opportunity_Split__c oSplit = new Opportunity_Split__c();
            oSplit.Split__c = 100;
            oSplit.Salesperson__c = opportunity.OwnerId;
            oSplit.Opportunity__c = opportunity.Id;
            oSplit.Opportunity_Owner__c = true;
            oSplit.CurrencyIsoCode = opportunity.CurrencyIsoCode;//Updated by Lakshman on 12-10-2013 for maintaining same currency
            opportunitySplitList.add(oSplit);   
        }
        insert opportunitySplitList;
    }
}