trigger SPLT_EvaluateSplitDetails_OppLineItem on OpportunityLineItem (after delete, after insert, after update) {
    List<OpportunityLineItem> oppLineItemList;
    if(Trigger.isDelete){
        oppLineItemList = Trigger.old;
    }else{
        oppLineItemList = Trigger.new;
    }
    Set<Id> opportunityIdSet = new Set<Id>();
    for (OpportunityLineItem oppLineItem: oppLineItemList){
        opportunityIdSet.add(oppLineItem.OpportunityId);
    }
    if(!System.isBatch() && !System.isFuture()){ // Added by Priyanka Ghanta to avoid the GovernorLimits When run BatchProcessingDealsToOpp  
        SPLT_ManageSplitDetails.evaluateSplitTypesAsync(opportunityIdSet);
    }
  
}