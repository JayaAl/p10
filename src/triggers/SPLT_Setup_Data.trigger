trigger SPLT_Setup_Data on Opportunity (after update) { 
    
    if(UserInfo.getUserId() != Label.ADXSyncUser){//Code ADDED By Sri for Loading ADX
    
        //check from Custom setting, create split is allow or not
        OpportunitySplit__c c=OpportunitySplit__c.getInstance() ;  
        //System.debug('#######Custom=='+c.Create_Splits__c);
        // if create split is true
        if(c<>null && c.Create_Splits__c){
            // split from updateSponserNameSplit method in UpdateOpportunitySponserForSplit class 
            SPLT_Setup_OppSplitData.updateSponserNameSplit(Trigger.new);
            SPLT_ManageSplitDetails.evaluateSplitTypesAsync(Trigger.newMap.keySet());
        }
    }
}