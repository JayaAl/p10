trigger SPLT_UpdateOpportunitySplits on Opportunity (after update) {
    if(UserInfo.getUserId() != Label.ADXSyncUser){//Code ADDED By Sri for Loading ADX
        SPLT_OpportunitySplit.checkOpportunityOwner(Trigger.oldMap,Trigger.newMap); 
        SPLT_OpportunitySplit.checkOpportunityCurrencyISO(Trigger.oldMap,Trigger.newMap); //Currency check by Lakshman(sfdcace@gmail.com) on 16-10-2013
    }
}