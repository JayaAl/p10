trigger SetAgreementRecipientAttachments on echosign_dev1__SIGN_Agreement__c (after insert) {
    List<Id> ioIds = new List<Id>();
    
    for( echosign_dev1__SIGN_Agreement__c agreement : Trigger.new ) {
        if( agreement.IO_Approval__c != null ) {
            ioIds.add( agreement.IO_Approval__c );
        }
    }

    if( ioIds.size() == 0 ) {
        return;
    }
    
    Map<Id,IO_Detail__c> ioDetails = new Map<Id,IO_Detail__c>([SELECT Id, Opportunity__c from IO_Detail__c where Id IN :ioIds and Opportunity__c != null]);
    
    List<Id> oppIds = new List<Id>();
    for( IO_Detail__c ioD : ioDetails.values() ) {
        oppIds.add( ioD.Opportunity__c );
    }
    
    List<OpportunityContactRole> roles = [SELECT OpportunityId, ContactId from OpportunityContactRole where OpportunityId IN :oppIds AND IsPrimary = true];
    Map<Id,Id> rolesMap = new Map<Id,Id>();
    for( OpportunityContactRole role : roles ) {
        rolesMap.put(role.OpportunityId, role.ContactId);
    }

    List<echosign_dev1__SIGN_Recipients__c> recipients = new List<echosign_dev1__SIGN_Recipients__c>();
    for( echosign_dev1__SIGN_Agreement__c agreement : Trigger.new ) {
        if( agreement.IO_Approval__c == null ) {
            continue;
        }
        
        IO_Detail__c ioD = ioDetails.get( agreement.IO_Approval__c );
        if( ioD == null ) {
            continue;
        }
        
        echosign_dev1__SIGN_Recipients__c recipient = new echosign_dev1__SIGN_Recipients__c();
        recipient.echosign_dev1__Agreement__c = agreement.Id;
        recipient.echosign_dev1__Recipient_Type__c = 'Contact';
        recipient.echosign_dev1__Order_Number__c = 1;
        recipient.echosign_dev1__Contact__c = rolesMap.get( ioD.Opportunity__c );
        
        recipients.add(recipient);
    }
    
    if(!recipients.isEmpty()){
        insert recipients;
    }
    
    List<ContentVersion> contents = [
        SELECT Id, VersionData, Description, PathOnClient, ContentDocumentId, Opportunity__c from ContentVersion 
        Where Folder__c = 'Insertion Orders' 
        AND Opportunity__c IN :oppIds
        AND IsLatest = TRUE
    ];
    
    List<Id> documentIds = new List<Id>();
    Map<Id,List<ContentVersion>> contentMap = new Map<Id,List<ContentVersion>>();
    for( ContentVersion contentVersion : contents ) {
        List<ContentVersion> curContent = contentMap.get( contentVersion.Opportunity__c );
        if( curContent == null ) {
            curContent = new List<ContentVersion>();
            contentMap.put(contentVersion.Opportunity__c, curContent);
        }
        curContent.add(contentVersion);
        documentIds.add(contentVersion.ContentDocumentId);
    }
    
    List<ContentWorkspaceDoc> docs = [SELECT ContentDocumentId, ContentWorkspaceId from ContentWorkspaceDoc 
        where ContentDocumentId IN :documentIds AND ContentWorkspaceId = '05840000000Cdp4'];
        
    List<Attachment> attachments = new List<Attachment>();
    for( echosign_dev1__SIGN_Agreement__c agreement : Trigger.new ) {
        if( agreement.IO_Approval__c == null ) {
            continue;
        }
        
        IO_Detail__c ioD = ioDetails.get( agreement.IO_Approval__c );
        if( ioD == null ) {
            continue;
        }
        
        List<ContentVersion> curContents = contentMap.get( ioD.Opportunity__c );
        
        if( curContents == null ) {
            continue;
        }
        
        for( ContentVersion content : curContents ) {
            Boolean isFound = false;
            for( ContentWorkspaceDoc doc : docs ) {
                if( doc.ContentDocumentId == content.ContentDocumentId ) {
                    isFound = true;
                    break;
                }
            }
            
            if( !isFound ) {
                continue;
            }
            
            Attachment attachment = new Attachment();
            attachment.Name = content.PathOnClient;
            attachment.Description = content.Id;
            attachment.Body = content.VersionData;
            attachment.ParentId = agreement.Id;
        
            attachments.add(attachment);
        }
    }
    if(!attachments.isEmpty()){
        insert attachments;
    }
}