trigger SetContractDate on echosign_dev1__SIGN_Agreement__c (after insert, after update) {
    
    // Prepare a collection to be updated
    List<IO_Detail__c> ioDetails = new List<IO_Detail__c>();
    
    // Create a map of Agreement --> IO_Detail__c statuses
    Map<String, String> mapAgreementStatus = new Map<String,String>{
        
        'Draft' => 'New no Signatures - esign',
        // '' => 'Signed by Pandora - esign',
        'Waiting for Counter-Signature' => 'Signed by Client - esign',
        'Signed' => 'Signed by Pandora/Client - esign'
            
        /* Old mapping values
        'Draft' => 'New - no signatures',
        'Waiting for Counter-Signature' => 'Signed by Client',
        'Signed' => 'Signed by Client / Pandora'
        */    
    };
    
    // Loop through all Agreements, only processing where there is an IO_Detail__c and the Status is changed
    for( echosign_dev1__SIGN_Agreement__c agreement : Trigger.new ) {
        if( agreement.IO_Approval__c == null ||
           ( Trigger.isUpdate && ( Trigger.oldMap.get(agreement.Id).echosign_dev1__Status__c == agreement.echosign_dev1__Status__c ) ) ) {
           continue;
        }
        
        // Only update those IO_Detail__c records where the status is defined
        IO_Detail__c thisIO = new IO_Detail__c(Id = agreement.IO_Approval__c);
        String thisStatus = agreement.echosign_dev1__Status__c;
        if(mapAgreementStatus.containsKey(thisStatus)){
            thisIO.IO_Signature_Status__c = mapAgreementStatus.get(thisStatus);
            ioDetails.add(thisIO);
        }
    }
    
    // Only perform DML if there are records to update
    if(!ioDetails.isEmpty()){
        update ioDetails;
    } 
}