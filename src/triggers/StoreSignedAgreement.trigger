/*
    If the attachment name does not include '- signed', or it does not have a ParentId it will be ignored
    otherwise we look at attachments already on the Agreement, find the Ids within the Description field, and add new versions to the content indicated
    if no content Ids are found we add a new Content document
    if more than one Id was found we update all Content
*/

trigger StoreSignedAgreement on Attachment (after insert) {

// declaring all vars here for clarity //
    // TODO: Move this Id to a setting or label for ease of management
    // Id of the 'Campaign Documentation' Content Library, where all new Content will be saved
    Id libraryId = '058400000004Ei6AAE';
    
    // Map<Agreement Id, List<Attachments>>, used to hold existing Attachments associated to the Agreements
    Map<Id,List<Attachment>> mapAgreementIdToAttachments = new Map<Id,List<Attachment>>();
    
    // Map<Attachment Id, Attachment>, used to hold all new Attachments that meet our critera
    Map<Id,Attachment> mapAttachments = new Map<Id,Attachment>();
    
    // collection of Agreements associated to the new Attachments
    Map<Id,echosign_dev1__SIGN_Agreement__c> mapAgreements = new Map<Id,echosign_dev1__SIGN_Agreement__c>();

    // Set to contain all Opportunities associated to the Agreements, used to query existing Content 
    Set<Id> setOppIds = new Set<Id>();    
    
    // Map<ContentVersion Id, ContentVersion>, collection of all existing Content associated to the Opportunities
    Map<Id,ContentVersion> contentVersionsMap = new Map<Id,ContentVersion>();
    
    // Map<Agreement Id, Set<Ids found within the Attachment Description>>, maps Agreements to the ContentVersion records identified as belonging to an attachment on the record
    Map<Id,Set<Id>> mapAgreementIdToPriorAttachment = new Map<Id,Set<Id>>();

    // List of ContentVersion records to be inserted
    List<ContentVersion> versions = new List<ContentVersion>();
    
// end declaring vars, begin logic //
    
    for( Attachment a : Trigger.new ) {
        //If the attachment parent id is not set, or does not have the post-fix '- signed' skip this attachment
        if( a.ParentId != null && a.Name.contains('- signed') ) {
            //Save the list of attachments associated with each agreement
            if(!mapAgreementIdToAttachments.containsKey(a.ParentId)){
                mapAgreementIdToAttachments.put(a.ParentId, new List<Attachment>());
            }
            mapAgreementIdToAttachments.get(a.ParentId).add(a);
            
            //Save attachments keyed off it id
            mapAttachments.put(a.Id,a);
        }
    }
    
    // if No attachments found meeting our criteria above, simply abort process as there is no need to continue
    System.debug('---> mapAttachments: '+mapAttachments);
    if( mapAttachments.isEmpty() ) {
        System.debug('---> mapAttachments empty');
        return;
    }
    
    // find all Agreements associated to the Attachments discovered above
    mapAgreements = new Map<Id,echosign_dev1__SIGN_Agreement__c>([SELECT Id, echosign_dev1__Opportunity__c, echosign_dev1__Recipient_User__c, echosign_dev1__Recipient_Addresses__c
                                                                  FROM echosign_dev1__SIGN_Agreement__c 
                                                                  WHERE Id IN :mapAgreementIdToAttachments.keySet()
                                                                 ]);
                                                                 
    // If no Agreements were found simply abort process as there is no need to continue
    System.debug('---> mapAgreements: '+mapAgreements);
    if( mapAgreements.isEmpty() ) {
        System.debug('---> mapAgreements empty');
        return;
    }
    
    // Compile a Set of Opportunity Ids
    for( echosign_dev1__SIGN_Agreement__c a : mapAgreements.values() ) {
        if(a.echosign_dev1__Opportunity__c==null) continue;
        setOppIds.add( a.echosign_dev1__Opportunity__c );
    }
    
    // If no Agreements were found simply abort process as there is no need to continue
    System.debug('---> setOppIds:'+setOppIds.isEmpty());
    if( setOppIds.isEmpty() ) {
        System.debug('---> setOppIds empty');
        return;
    }
    
    // Query the current ContentVersion records from the found Opportunities, filtering to only the latest versions, and those within the Campaign Documentation library indicated above
    contentVersionsMap = new Map<Id,ContentVersion>([SELECT Id, ContentDocumentId, PathOnClient
                                                     FROM ContentVersion 
                                                     WHERE Opportunity__c IN :setOppIds
                                                     ORDER BY LastModifiedDate Asc
                                                    ]);
    
    // find all Attachments already associated to the Agreemnets, excluding any Attachments included in this transaction
    for(Attachment a:[SELECT ParentId, Id, Description FROM Attachment WHERE ParentId IN:mapAgreementIdToAttachments.keySet() And Id NOT IN : Trigger.newMap.Keyset()]){
        // Wrap in a try/catch so that if casting the Description as an Id fails we simply move on to the next attachment
        try{
            if(!mapAgreementIdToPriorAttachment.containsKey(a.ParentId)){
                mapAgreementIdToPriorAttachment.put(a.ParentId, new Set<Id>());
            }
            Id thisId = (Id) a.description;
            mapAgreementIdToPriorAttachment.get(a.ParentId).add(thisId);
        } catch (Exception e){
            System.debug('---> Casting existing attachment Description failed: '+a);
            System.debug(e);
            continue;
        }
    }
    
    // loop through all new Attachments, creating the ContentVersion records as appropriate
    for( Attachment a : mapAttachments.values() ) {
        
        // Only create a ContentVersion record if the new Attachment is associated to an Agreement queried earlier
        if( mapAgreements.containsKey( a.ParentId ) ) {
            
            // get the Agreement for the new Attachment
            echosign_dev1__SIGN_Agreement__c thisAgreement = mapAgreements.get( a.ParentId );
            
            // Find any existing ContentVersion Ids associated to old Attachments
            Boolean attachedToExisting = false;
            //Generate string to replace "New signed" version comment
            String commentStr = 'Signed by - ' + thisAgreement.echosign_dev1__Recipient_User__c + ' (' + thisAgreement.echosign_dev1__Recipient_Addresses__c + ')';
            
            if(mapAgreementIdToPriorAttachment.containsKey(a.ParentId)){
                for(Id i:mapAgreementIdToPriorAttachment.get(a.ParentId)){
                    // system.assert(false,'---> Keyset: '+contentVersionsMap.keySet()+' --->PriorAttachment: '+i);
                    if(contentVersionsMap.containsKey(i)){
                        attachedToExisting = true;
                        ContentVersion oldVersion = contentVersionsMap.get(i);
                        ContentVersion v = new ContentVersion(
                            VersionData = a.Body,
                            ContentDocumentId = oldVersion.contentDocumentId,
                            //Replace version comment
                            //ReasonForChange = 'New signed',
                            ReasonForChange = commentStr,
                            PathOnClient = oldVersion.PathOnClient
                        );
                        versions.add(v);
                        System.debug('---> existing Content found, appending record:');
                        System.debug(v);
                    }
                }
            }
            
            if(!attachedToExisting){
                // If no prior ContentVersion Ids found then simply create a new record
                ContentVersion v = new ContentVersion(
                    Title = a.Name,
                    VersionData = a.Body,
                    Description = a.Description,
                    PathOnClient = a.Name,
                    Folder__c = 'Insertion Orders',
                    FirstPublishLocationId = libraryId,
                    Opportunity__c = thisAgreement.echosign_dev1__Opportunity__c
                );
                versions.add(v);
                System.debug('---> new Content record created:');
                System.debug(v);
            }
        }
    }
    
    // Insert the new ContentVersion records, capturing any errors
    if(!versions.isEmpty()){
        try {
            insert versions;
        } catch(Exception e) {
            System.debug('---> Failed to insert signed agreement content:');
            System.debug(e);
            // TODO: Capture errors for reporting
        }
    }
}