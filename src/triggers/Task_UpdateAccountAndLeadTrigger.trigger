trigger Task_UpdateAccountAndLeadTrigger on Task (after insert, after update) {
    map<id,lead> leads = new map<id,lead>();
    map<id,account> accounts = new map<id,account>();
    Set<Id> setIds = new Set<Id>();
    Set<Id> setContactIds = new Set<Id>();
    Set<Id> setOppIds = new Set<Id>();
    for(task record:trigger.new) {
        if(record.whoid != null && record.whoid.getsobjecttype() == lead.sobjecttype) {
            leads.put(record.whoid, new lead(id=record.whoid, last_updated_activity__c=date.today()));
        }
        if(record.whoid != null && record.whoid.getsobjecttype() == contact.sobjecttype) {
            setContactIds.add(record.whoid);
        }
        if(record.whatid != null && record.whatid.getsobjecttype() == account.sobjecttype) {
            setIds.add(record.whatid);
        }
        if(record.whatid != null && record.whatid.getsobjecttype() == opportunity.sobjecttype) {
            setOppIds.add(record.whatid);
        }
    }
    if(!setOppIds.isEmpty()) {
        for(Opportunity opp: [Select Id, AccountId, Agency__c from Opportunity where Id =: setOppIds]) {
            if(opp.AccountId != null) {
                setIds.add(opp.AccountId);
            } 
            if(opp.Agency__c != null) {
                setIds.add(opp.Agency__c);
            }
        }
    }
    if(! leads.isEmpty()) {
        update leads.values();
    }
    if(! setContactIds.isEmpty()) {
        for(Contact con: [Select AccountId from Contact where Id =: setContactIds AND AccountId != NULL]) {
            setIds.add(con.AccountId);
        }
    }
    if(! setIds.isEmpty()) {
        for(Account acc: [Select Id, last_updated_activity__c from Account 
                             where Id =: setIds 
                             AND RecordType.Name != 'Vendor']) {
            acc.last_updated_activity__c=date.today();
            accounts.put(acc.Id, acc);
        }
        if(! accounts.isEmpty()) {
            update accounts.values();
        }
    }
}