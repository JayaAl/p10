/**
 * @name: TriggerOnADPAssociatedAcc
 * @desc: Used to populate Total Employees on ADP_Associated_Accounts__c from Account__c
 * @author: Lakshman(sfdcace@gmail.com)
 * @date: 22-8-2014
 */
trigger TriggerOnADPAssociatedAcc on ADP_Associated_Accounts__c (before insert, before update, after insert, after update) {
    /* Logic to update Total Employees from Account to ADP_Associated_Accounts__c */
    /*
    if(Trigger.isBefore){
        Map<Id, Decimal> mapOfAccountIdToEmpCnt = new Map<Id, Decimal>();
        for(ADP_Associated_Accounts__c aaa: Trigger.new) {
            ADP_Associated_Accounts__c oldAAA = aaa.Id != null ? Trigger.oldMap.get(aaa.Id) : null;
            if((oldAAA == null && aaa.Account__c != null) || (oldAAA.Account__c != aaa.Account__c)) {
                mapOfAccountIdToEmpCnt.put(aaa.Account__c, 0);
            }
        }
        if(! mapOfAccountIdToEmpCnt.isEmpty()) {
            for(Account objAcc: [Select Id, Total_Employees__c from Account where Id =: mapOfAccountIdToEmpCnt.keySet()]) {
                if(objAcc.Total_Employees__c != null) {
                    mapOfAccountIdToEmpCnt.put(objAcc.Id, objAcc.Total_Employees__c);
                }
            }
            for(ADP_Associated_Accounts__c aaa: Trigger.new) {
                if(mapOfAccountIdToEmpCnt.containsKey(aaa.Account__c)) {
                    aaa.Total_Employees__c = mapOfAccountIdToEmpCnt.get(aaa.Account__c);
                }
            }
        }
    }
    */
    /*End of Logic*/
    /*Logic to Set Primary Account*/
    /*
    if(Trigger.isAfter) {
        Map<Id, Id> mapOfAAAToADP = new Map<Id, Id>();
        for(ADP_Associated_Accounts__c aaa: Trigger.new) {
            ADP_Associated_Accounts__c oldAAA = (Trigger.oldMap != null) ? Trigger.oldMap.get(aaa.Id) : null;
            if(Trigger.isInsert && aaa.Primary_Account__c != false) {
                mapOfAAAToADP.put(aaa.Id, aaa.Account_Development_Plan__c);
            } else if( (Trigger.isUpdate && oldAAA.Primary_Account__c != aaa.Primary_Account__c && aaa.Primary_Account__c)) {
                mapOfAAAToADP.put(aaa.Id, aaa.Account_Development_Plan__c);
            }
        }
        
        if(! mapOfAAAToADP.isEmpty()) {
            ADP_Associated_Accounts__c[] listAAA = [Select Id, Primary_Account__c from ADP_Associated_Accounts__c where Id != :mapOfAAAToADP.keySet() AND Account_Development_Plan__c =: mapOfAAAToADP.values()
                                                    AND Primary_Account__c = true];
            for(ADP_Associated_Accounts__c aaa: listAAA) {
                aaa.Primary_Account__c = false;
            }
            if(! listAAA.isEmpty()) {
                update listAAA;
            }
        }
    }
    */
}