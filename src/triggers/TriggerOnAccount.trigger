trigger TriggerOnAccount on Account (after update, after insert) {
    //nsk: 10/23 Adding below logic to control trigger execution via custom label
    if(Label.Disable_Account_Triggers!='YES'){
        AccountTriggerHandler accountTriggerHandlerObj = new AccountTriggerHandler(Trigger.isExecuting, Trigger.size);
        if(trigger.isAfter){
            if(trigger.isUpdate) {
                accountTriggerHandlerObj.updateOpportunitySplitOwner(Trigger.new,Trigger.oldMap, Trigger.newMap);
                accountTriggerHandlerObj.addAccountOwnerAsTeamMember(Trigger.new,Trigger.oldMap, Trigger.newMap);
                //accountTriggerHandlerObj.onAfterUpdate(Trigger.new,Trigger.oldMap, Trigger.newMap);
            } else if(trigger.isInsert) { 
                accountTriggerHandlerObj.addAccountOwnerAsTeamMember(Trigger.new,Trigger.oldMap, Trigger.newMap);
                //OpportunityTriggerHandler.OnAfterInsertAsync(Trigger.newMap.keySet());
            }  
        }
    }//End of Disable_Account_Triggers
}