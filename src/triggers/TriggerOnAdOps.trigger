/*
 * @name: TriggerOnAdOps
 * @author: Lakshman(sfdcace@gmail.com)
 * @desc: Auto populate the "Ad Ops lookup" on content object with the corresponding Opportunity's Ad Ops record value on change of Opportunity on Ad Ops
 * @date: 29-7-2014
 */
trigger TriggerOnAdOps on Ad_Operations__c (after update, after insert) {
    Map<Id, Ad_Operations__c> oldMap = Trigger.oldMap;//Map to store old map values
    Map<Id, Id> mapOfChangeAdOps = new Map<Id, Id>();//Map to store changed Opportunity Id's of Ad Ops object
    Map<Id, Id> mapOfBLANKAdOps = new Map<Id, Id>();//Map to store Opportunity Id's which are blanked out    
    for(Ad_Operations__c ao: Trigger.new){
        Ad_Operations__c oldAdOps = (oldMap != null) ? oldMap.get(ao.Id) : null;
        if(oldAdOps == null || oldAdOps.Opportunity__c != ao.Opportunity__c) {
            if(ao.Opportunity__c == null && oldAdOps != null) {
                mapOfBLANKAdOps.put(oldAdOps.Opportunity__c, ao.Id);
            } else {
                mapOfChangeAdOps.put(ao.Opportunity__c, ao.Id);
            }
        }
    }
    
    if(! mapOfBLANKAdOps.isEmpty() || !mapOfChangeAdOps.isEmpty()) {
        List<ContentVersion> listCV = [
                                       Select Ad_Ops__c, Opportunity__c from ContentVersion where 
                                       (Opportunity__c =: mapOfBLANKAdOps.keySet() OR Opportunity__c =: mapOfChangeAdOps.keySet() ) AND IsLatest = true
                                       ];//Fetch all contentversion with the changed or blanked opp ids
        for(ContentVersion cv: listCV) {
            if(mapOfBLANKAdOps.containsKey(cv.Opportunity__c)) {//blanked out opps have precedence
                cv.Ad_Ops__c = cv.Ad_Ops__c != null ? null : cv.Ad_Ops__c;
            }else if(mapOfChangeAdOps.containsKey(cv.Opportunity__c)) {
                cv.Ad_Ops__c = mapOfChangeAdOps.get(cv.Opportunity__c);
            }
        }
        update listCV;//Finally update the cv
    }
}