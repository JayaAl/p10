/*
Date: 06/03/2014
Name: TriggerOnContact 
Description: This Trigger will update the Account's Source field based on the Lead conversion process. When a Lead is converted, it can be either created as a new contact or an update to an existing 
one under again a new account or an existing one And also when edits are made to Pandora Promotion and Pandora B2B event on a contact, reflect any changes on the related Account.    
*******Change Log***********
Lakshman: The code now considers removal of any picklist value as well. So if a value is removed from Contact it will remove value from Account as well(if other contacts dont have tha value too).
Lakshman: Issue fixed, details -  ESS-23287
*/
trigger TriggerOnContact on Contact (after update, before insert,before update, after insert) {
    set<Id> accountIds = new set<Id>();
    Set<Id> contactsInContext = new Set<Id>();
    Set<Id> setContactIdRegionAssignment = new Set<Id>();
    
    
    
    for(Contact con : Trigger.New) {
        if(Trigger.isUpdate) {
            Contact oldCon = Trigger.oldMap.get(con.Id);
            if(
                (con.Hidden_Lead_Source__c != oldCon.Hidden_Lead_Source__c 
                   && String.isNotBlank(con.Hidden_Lead_Source__c)
                   )
                || (con.Pandora_Promotion__c != oldCon.Pandora_Promotion__c )
                || (con.How_would_you_describe_your_marketplace__c != oldCon.How_would_you_describe_your_marketplace__c )               
                || (con.Pandora_B2B_Event__c != oldCon.Pandora_B2B_Event__c ) 
                ){
                accountIds.add(con.AccountId);
                contactsInContext.add(con.Id);
            } 
            } else if(Trigger.isAfter && trigger.isInsert) {
                if(
                    String.isNotBlank(con.Hidden_Lead_Source__c) 
                    || String.isNotBlank(con.Pandora_Promotion__c)
                    || String.isNotBlank(con.Pandora_B2B_Event__c)
                    || String.isNotBlank(con.How_would_you_describe_your_marketplace__c) 
                    ) {
                    accountIds.add(con.AccountId);
                    contactsInContext.add(con.Id);
                }
                if(String.isBlank(con.Hidden_Lead_Source__c))
                {
                    setContactIdRegionAssignment.add(con.id);
                }
            }
        /*else if(Trigger.isBefore && trigger.isInsert) {
            
                if(String.isBlank(con.Hidden_Lead_Source__c))
                {
                    lstContactsRegionAssignment.add(con);
                }
               
                }*/

            }
            /*====================================================*/
            // This method trigger updates Inside_Sales_Region__C contact field if the mailingpostal code has value
            // Refer Jira ticket - ESS-34710 for more information
            if(Trigger.isBefore){

               Set<String> contactZipCodes = new Set<String>();
               if(Trigger.isInsert){

                  for(Contact cont : Trigger.New) {
                    cont.Inside_Sales_Region__C ='';
                    if(cont.MailingPostalCode   != Null){
                       contactZipCodes.add(cont.MailingPostalCode);
                   }
               }

               if(contactZipCodes.size() >0){
                Map<String,Group_Member_Zip_Junction__c> mapGroupName = new  Map<String,Group_Member_Zip_Junction__c>();
                mapGroupName= getGroupNameforZipcodes(contactZipCodes);
                if(mapGroupName.size() >0 ){
                  for(Contact insertCon : Trigger.New) {
                     //if(mapGroupName.get(insertCon.MailingPostalCode).Assignment_Group__r.Name != Null ){
                       
                       If(insertCon.MailingPostalCode!=null && mapGroupName.get(insertCon.MailingPostalCode)!=null && mapGroupName.get(insertCon.MailingPostalCode).Assignment_Group__c!=null && mapGroupName.get(insertCon.MailingPostalCode).Assignment_Group__r.Name != Null ){
                            insertCon.Inside_Sales_Region__C = mapGroupName.get(insertCon.MailingPostalCode).Assignment_Group__r.Name ;
                        } else{
                            insertCon.Inside_Sales_Region__C ='';
                        }
                    }
                }
            }
        }
        else if(Trigger.isUpdate){
           List<Contact>contactZipCodeList = new List<Contact>();
           //Verify if the new value and old value is same
           for(integer i=0; i<Trigger.new.size(); i++){
            if(Trigger.new[i].MailingPostalCode != Null && Trigger.old[i].MailingPostalCode != Trigger.new[i].MailingPostalCode){
                contactZipCodes.add(Trigger.New[i].MailingPostalCode);
                contactZipCodeList.add(Trigger.New[i]);
            }
        }

        if(contactZipCodes.size() >0){
            Map<String,Group_Member_Zip_Junction__c> mapGroupName = new  Map<String,Group_Member_Zip_Junction__c>();
            mapGroupName= getGroupNameforZipcodes(contactZipCodes);
            if(mapGroupName.size() >0 ){
                for(Contact contat : contactZipCodeList){
                   if(mapGroupName.get(contat.MailingPostalCode).Assignment_Group__r.Name != Null ){
                    contat.Inside_Sales_Region__C = mapGroupName.get(contat.MailingPostalCode).Assignment_Group__r.Name ;
                    }else{
                        contat.Inside_Sales_Region__C ='';
                    }
                }
            }
        }
    }
}



/*====================================================*/
//Pass the contact postalcode to get the appropriate Assignment Group Name
public  Map<String,Group_Member_Zip_Junction__c> getGroupNameforZipcodes(Set<String> contactZipCodes){
   Map<String,Group_Member_Zip_Junction__c> mapGroupName = new  Map<String,Group_Member_Zip_Junction__c>();
   List<Zip_Code__c>  zipList = [Select Name 
   from Zip_Code__c 
   where Name IN :contactZipCodes 
   AND Active__c =:'True' 
   AND Valid_Zip__c =: True];

   Set<String> conZipCodesList = new Set<String>();
   if(zipList.size() > 0){
       for(Zip_Code__c ZIP: zipList){
        conZipCodesList.add(ZIP.Name);
    }

    for(List<Group_Member_Zip_Junction__c> groupAssignZip : [Select Assignment_Group__r.Name,
        Name ,
        Zip_Code__r.Name 
        from Group_Member_Zip_Junction__c
        where Zip_Code__r.Name IN: conZipCodesList AND Assignment_Group__r.Type__C ='Lead']){
       if(groupAssignZip.size() > 0){
            for(Group_Member_Zip_Junction__c zipcode : groupAssignZip){
                mapGroupName.put(zipcode.Zip_Code__r.Name, zipcode);
            }
        }

    }
}
return mapGroupName;
}

system.debug('THIS IS THE CON'+ setContactIdRegionAssignment);
if(Trigger.isInsert && Trigger.isafter && setContactIdRegionAssignment.Size()>0){
    system.debug('THIS IS THE CON'+ setContactIdRegionAssignment);
    contactRegionTerritoryHandler cntHandler = new contactRegionTerritoryHandler();
    cntHandler.assignRegionTerritory(setContactIdRegionAssignment);
}
if(!accountIds.isEmpty()) {
    List<Account> listOfAccountToUpdate = new List<Account>();
    for(Account acc :[
        SELECT Id,Pandora_Promotion__c,Pandora_B2B_Event__c, AccountSource, How_would_you_describe_your_marketplace__c, 
        (
        SELECT Pandora_Promotion__c, Pandora_B2B_Event__c, Id, Hidden_Lead_Source__c, How_would_you_describe_your_marketplace__c 
        FROM Contacts 
        WHERE Pandora_Promotion__c != null 
        OR Pandora_B2B_Event__c != null 
        OR How_would_you_describe_your_marketplace__c != null
        ) 
        FROM Account 
        WHERE Id IN: accountIds
        ]) {
        String picklistStr='';
        String picklistStr1='';
        String picklistStr2='';
        for(Contact con: acc.Contacts) {
            if(contactsInContext.contains(con.Id)) {
                acc.AccountSource = con.Hidden_Lead_Source__c;
            }
            if(con.Pandora_Promotion__c != null && con.Pandora_Promotion__c != '') {
                picklistStr += con.Pandora_Promotion__c +';'; 
            }                
            if(con.Pandora_B2B_Event__c != null && con.Pandora_B2B_Event__c != '') {
                picklistStr1 += con.Pandora_B2B_Event__c +';'; 
            }
            if(con.How_would_you_describe_your_marketplace__c!= null && con.How_would_you_describe_your_marketplace__c != '') {
                picklistStr2 += con.How_would_you_describe_your_marketplace__c +';'; 
            }
        }
        if(picklistStr != '') {
                picklistStr.substring(0, picklistStr.length() - 1); // Strip the trailing semicolon from the string of values
                picklistStr = format(picklistStr); // remove duplicate values from the array
            }
            if(picklistStr1 != '') {
                picklistStr1.substring(0, picklistStr1.length() - 1);
                picklistStr1 = format(picklistStr1);
            }
            if(picklistStr2 != '') {
                picklistStr2.substring(0, picklistStr2.length() - 1);
                picklistStr2 = format(picklistStr2);
            }            
            listOfAccountToUpdate.add(
                new Account(
                    Id = acc.Id, 
                    Pandora_Promotion__c = picklistStr, 
                    Pandora_B2B_Event__c = picklistStr1, 
                    How_would_you_describe_your_marketplace__c= picklistStr2, 
                    AccountSource = acc.AccountSource
                    )
                );
        }
        if(! listOfAccountToUpdate.isEmpty()) {
            update listOfAccountToUpdate;
        }
    }
    public String format(String values){
        Set<string> picklistSet = new Set<String>();
        String st='';
        for(String str: values.split(';')){
            system.debug('======'+str);
            if(str !='null' && str !='')
            picklistSet.add(str);
        }
        for(String pickValue:picklistSet){
            st+= pickValue+';';
        }
        return st.subString(0,st.Length()-1);
    }   
}