/*
 * @name: TriggerOnContent
 * @author: Lakshman(sfdcace@gmail.com)
 * @desc: Auto populate the "Ad Ops lookup" on content object with the corresponding Opportunity's Ad Ops record value
 * @date: 29-7-2014
 */
trigger TriggerOnContent on ContentVersion (before update, before insert) {
    Map<Id, ContentVersion> oldMap = Trigger.oldMap;//Map to Store old CV values
    Map<Id, ContentVersion> mapOfOppIdToCV = new Map<Id, ContentVersion>();//Map to store Opportunity Id of CV to CV Id
    //Loop over Trigger to populate the mapOfOppIdToCV 
    for(ContentVersion cv: Trigger.new){
        ContentVersion oldContent = (oldMap != null) ? oldMap.get(cv.Id) : null;
        if(oldContent == null || oldContent.Opportunity__c != cv.Opportunity__c) {
            if(cv.Opportunity__c == null) {
                cv.Ad_Ops__c = cv.Ad_Ops__c != null ? null : cv.Ad_Ops__c;//Blank out Ad Ops if Opportunity becomes null
            } else {
                mapOfOppIdToCV.put(cv.Opportunity__c, cv);
            }
        }
    }
    if(! mapOfOppIdToCV.isEmpty()) {
        //Create a map to hold <Opportunity, Ad Ops record>
        Map<Id, Id> mapOfOppToAdOps = new Map<Id, Id>();
        
        for(Ad_Operations__c ao: [Select Id, Opportunity__c from Ad_Operations__c where Opportunity__c IN: mapOfOppIdToCV.keySet() ORDER By CreatedDate]) {//Latest comes first to get the oldest in map
            mapOfOppToAdOps.put(ao.Opportunity__c, ao.Id);
        }
        
        for(ContentVersion cv: mapOfOppIdToCV.values()) {
            cv.Ad_Ops__c = mapOfOppToAdOps.get(cv.Opportunity__c);
        }
    }
}