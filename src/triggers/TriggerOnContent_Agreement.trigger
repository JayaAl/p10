trigger TriggerOnContent_Agreement on Content_Agreement__c (before insert, before update) {

	Content_AgreementTriggerHandler handler = new Content_AgreementTriggerHandler();
	handler.recalculateRateCardData(Trigger.New);
}