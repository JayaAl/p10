/*
Developer: Lakshman Ace(sfdcace@gmail.com)
Description:
    Updates the "Credited to Date" field on the opportunity related to the credit notes.
    
Related Files:
    ERP_BLNG_CreditAggregator.cls
*/
trigger TriggerOnERPCreditNote on ERP_Credit_Note__c (
      after delete
    , after insert
    , after undelete
    , after update
) {

    // update opportunity credit to date rollup
    ERP_BLNG_CreditAggregator rollup = new ERP_BLNG_CreditAggregator(
          (trigger.isDelete) ? trigger.old : trigger.new
        , (trigger.isUpdate) ? trigger.oldMap : null 
    );
    try {
    rollup.updateCreditedToDate();
    } catch(Exception ex) {
        logger.logMessage('TriggerOnERPCreditNote', 'execute', ex.getMessage(), 'ERP_Credit_Note__c', '', 'Error');        
        system.debug('>>>ex' + ex);
    }

}