/*
Developer: Lakshman Ace(sfdcace@gmail.com)
Description:
    Updates the "Invoiced to Date" field on the opportunity related to the new ERP invoice.
    
Related Files:
    ERP_BLNG_InvoiceAggregator.cls
*/
trigger TriggerOnERPInvoice on ERP_Invoice__c (
    after delete
    , after insert
    , after undelete
    , after update
    , before update
) {
    
    if(trigger.isBefore){
        ERP_InvoiceTriggerHandler.onBeforeUpdate(trigger.newMap, trigger.oldMap);
    } else { // all After
    
    
        // update opportunity invoiced to date rollup
        ERP_BLNG_InvoiceAggregator rollup = new ERP_BLNG_InvoiceAggregator(
              (trigger.isdelete) ? trigger.old: trigger.new
            , (trigger.isUpdate) ? trigger.oldMap : null
        );
        try{
        rollup.updateInvoicedToDate();
        } catch(Exception ex) {
            logger.logMessage('TriggerOnERPInvoice', 'execute', ex.getMessage(), 'ERP_Invoice__c', '', 'Error'); 
            system.debug('>>>ex' + ex);
        }
    }
}