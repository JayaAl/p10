/**
 * @name : TriggerOnIOApproval
 * @desc : Trigger for following operations:
            - Copying CurrencyISOCode from Opportunity on every insert event.
 * @version : 1.0
 * @date : 10/10/2014
 * @author : Lakshman(sfdcace@gmail.com)
 * Changes# ESS-8457 - Adding logic to trigger Account credit verification
 */
trigger TriggerOnIOApproval on IO_Detail__c (before insert, before update) {

    //Get the list of corresponding opportunities
    Set<Id> opptyIds = new Set<Id>();
    List<opportunity> opportunities = new List<opportunity>();
    for(IO_Detail__c ioDetail : Trigger.new) {
        system.debug('ioDetail.Opportunity__c = '+ioDetail.Opportunity__c);
        system.debug('ioDetail.probability = '+ioDetail.probability__c);
        
        if(ioDetail.Opportunity__c != null && ioDetail.probability__c >= 90) {
             opptyIds.add(ioDetail.Opportunity__c);
         }
    }
    if(opptyIds.size()>0)
        opportunities = [Select Id, Account.Credit_Verification2__c, AccountId, probability, Account.RecordType.Name from Opportunity Where Id in :opptyIds ];
    
    OpportunityTriggerHandler opptyTriggerHandlerObj = new OpportunityTriggerHandler(null,null);

    if(Trigger.isBefore && Trigger.isInsert) {
        IOApprovalTriggerHandler.handleCurrencyForInsertOperation(Trigger.new);
        opptyTriggerHandlerObj.checkAndInitiateAccountCV(opportunities, null, null);
    }
    else if(Trigger.isBefore && Trigger.isUpdate) {
          if(!System.isBatch() && !System.isFuture()){
        opptyTriggerHandlerObj.checkAndInitiateAccountCV(opportunities, null, null);
        }
    }

}