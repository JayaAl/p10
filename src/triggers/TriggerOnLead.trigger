/******************************************************************************************************
*  Name                 : TriggerOnLead
*  Author               : Sridharan Subramanian
*  Last Modified Date   : 05/04/2016
*  Description          : Invoked on every time the Lead is Inserted and Edited. The control is 
*                         transferred to the Handler to manager the Lead Ownership
******************************************************************************************************/

trigger TriggerOnLead on Lead (before insert, before update, after insert, after update) {
    
    Map<String,Disable_Triggers__c>  support = Disable_Triggers__c.getAll();
    if(support!=null && support.ContainsKey('Lead') && support.get('Lead')!=null){
        
        if((boolean)support.get('Lead').Disabled__c ==true) 
            return;
    }
    system.debug('LeadTriggerHandler.assignAlreadyCalled' + LeadTriggerHandler.assignAlreadyCalled);
    if(LeadTriggerHandler.assignAlreadyCalled==FALSE)
    {
        LeadTriggerHandler handler = new LeadTriggerHandler();
    
        if(Trigger.isInsert && Trigger.isBefore) {
            system.debug(logginglevel.info,'BEFORE INSERT' + trigger.new);
            handler.OnBeforeInsert(Trigger.new);
        }
        if(Trigger.isUpdate && trigger.isBefore)
        {
            system.debug(logginglevel.info,'before update trigger called');
            handler.OnBeforeUpdate(Trigger.old, Trigger.new, Trigger.oldMap, Trigger.newMap);
        }
        else if(Trigger.isUpdate && Trigger.isAfter) { 
            system.debug(logginglevel.info,'after update trigger called');
            handler.OnAfterUpdate(Trigger.old, Trigger.new, Trigger.oldMap, Trigger.newMap);
            
            system.debug(logginglevel.info,'LeadTriggerHandler.assignAlreadyCalled=' + LeadTriggerHandler.assignAlreadyCalled);
            
                //LeadTriggerHandler.deletionQueueOwnerUpdate(Trigger.new,Trigger.oldMap);
        } 
    }
    
}