trigger TriggerOnOpportunity on Opportunity (before update, after insert, after update, after delete, after undelete) {
    OpportunityTriggerHandler opportunityTriggerHandlerObj = new OpportunityTriggerHandler(Trigger.isExecuting, Trigger.size);
    if(trigger.isBefore){
        if(trigger.isUpdate){
           if(!System.isBatch() && !System.isFuture()){ //Added by Priyanka on 09/22/2015 to avoid the error from Adx Batch(Error: Future method can not be called from a future)
            opportunityTriggerHandlerObj.onBeforeUpdate(Trigger.new,Trigger.oldMap, Trigger.newMap);
           }
        }
        else if(trigger.isInsert){
            opportunityTriggerHandlerObj.onBeforeInsert(Trigger.new);
        }
    }
    if(trigger.isAfter){
        //if(trigger.isUpdate){//Code Commented By Sri for Loading ADX
        if(trigger.isUpdate && (UserInfo.getUserId() != Label.ADXSyncUser)){//Code ADDED By Sri for Loading ADX
            //opportunityTriggerHandlerObj.onAfterUpdate(Trigger.new,Trigger.oldMap, Trigger.newMap);
            
            opportunityTriggerHandlerObj.updateCaseOnOpportunityAccountChange(Trigger.new,Trigger.oldMap, Trigger.newMap);
            opportunityTriggerHandlerObj.updateAccountOnOppClosed(Trigger.new,Trigger.oldMap, Trigger.newMap);
            //updated by VG 12/11/2012
            //notify Matterhorn if there are changes in the Opportunity. 
          //  MatterhornNotifyChangesOnOppty matterhornNotification = new MatterhornNotifyChangesOnOppty(Trigger.new,Trigger.oldMap, Trigger.newMap);
           // matterhornNotification.onAfterUpdate();
            
        } else if(trigger.isInsert){
            //OpportunityTriggerHandler.OnAfterInsertAsync(Trigger.newMap.keySet());
        }
    }
    
    // finally, mark all IO_Detail__c records associated to any Closed Opptys as "Cancelled"
    if(trigger.isInsert || trigger.isUpdate ){
        if((UserInfo.getUserId() != Label.ADXSyncUser)){//Code ADDED By Sri for Loading ADX
            opportunityTriggerHandlerObj.closeIODetailOnOpportunityClose(Trigger.newMap);
        }
    }
    
    /*Logic To Calculate Rollup on all after operations*/
    //if(trigger.isAfter) {//Code Commented By Sri for Loading ADX
      if(trigger.isAfter && (UserInfo.getUserId() != Label.ADXSyncUser)) {//Code ADDED By Sri for Loading ADX
        Map<Id, Opportunity> mapOfOpp = new Map<Id, Opportunity>();
        Map<Id, Id> mapOfAccountIdToAgency = new Map<Id, Id>();
        // modified objects whose parent records should be updated

        if (Trigger.isDelete) {
         for(Opportunity opp: Trigger.Old) {
            if(opp.Agency__c != null) {
                mapOfAccountIdToAgency.put(opp.AccountId, opp.Agency__c);
            }
            mapOfOpp.put(opp.AccountId, opp);
         }
        } else if(Trigger.isUpdate){
            for(Opportunity opp : Trigger.new) {
                if((opp.Agency__c != Trigger.oldMap.get(opp.Id).Agency__c) || 
               (opp.AccountId != Trigger.oldMap.get(opp.Id).AccountId)) {
                if(opp.Agency__c != null) {
                     mapOfAccountIdToAgency.put(opp.AccountId, opp.Agency__c);
                 }
                mapOfOpp.put(opp.AccountId, opp);
                }
            }
            
            //ISS-11369
            opportunityTriggerHandlerObj.onAfterUpdate(Trigger.new, Trigger.oldMap, Trigger.newMap);
             
        } else {
            for(Opportunity opp : Trigger.new) {
                mapOfOpp.put(opp.AccountId, opp);
                if(opp.Agency__c != null) {
                    mapOfAccountIdToAgency.put(opp.AccountId, opp.Agency__c);
                }
            } 
            //ISS-11369
            opportunityTriggerHandlerObj.onAfterInsert(Trigger.new);
        }
        if(!mapOfOpp.isEmpty()) {
            
            Map<Id, Account> accountsToUpdate = new Map<Id, Account>([Select Id, Last_Opportunity_Created_Date__c from Account 
                                                                      where (Id IN :mapOfOpp.keySet() OR Id IN :mapOfAccountIdToAgency.values()) AND RecordType.Name != 'Vendor']);
            
            // initilize Account
            for (Account a : accountsToUpdate.values()) {
                a.Last_Opportunity_Created_Date__c = null;
            }
            for(AggregateResult q : [select AccountId accId ,Max(CreatedDate) 
                                     from Opportunity where AccountId = :mapOfOpp.keySet() AND Account.RecordType.Name != 'Vendor' group by AccountId]){
              
              system.debug('***********q' + q);
              Account a = accountsToUpdate.get((Id)q.get('accId'));
              if(a != null) { 
                  DateTime dT = (DateTime)q.get('expr0');
                  Date lastCreatedDate = date.newinstance(dT.year(), dT.month(), dT.day());
                  a.Last_Opportunity_Created_Date__c = lastCreatedDate;
                  if(mapOfAccountIdToAgency.containsKey(a.Id)) {
                     Account agencies = accountsToUpdate.get(mapOfAccountIdToAgency.get(a.Id));
                     agencies.Last_Opportunity_Created_Date__c = a.Last_Opportunity_Created_Date__c;
                  }
              }
            }
          if(! accountsToUpdate.isEmpty()) {
              update accountsToUpdate.values();
          }
          
        }
     }
}