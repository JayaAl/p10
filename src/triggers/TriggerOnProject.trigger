trigger TriggerOnProject on Project__c (before insert, before update, after insert, after update, after delete) {
    /* Trigger to compile all Project__c trigger functionality */
    
    // instance the trigger helper
	TriggerOnProject controller = new TriggerOnProject();
	    
    if(Trigger.isBefore){ 
        system.debug('----> Start Trigger.isBefore');
/* Validate that there are no duplicate Project Names. Rewritten to capture duplicate names committed in the same action. */
        Map<String,Set<Project__c>> mapNamesToRecords = new Map<String,Set<Project__c>>();
        Set<Id> setIds = (Trigger.isUpdate) ? Trigger.newMap.keySet() : null; // if Update call gather the existing Ids for use later
        for (Project__c p : trigger.new) {
            if(!mapNamesToRecords.containsKey(p.Name)){
                mapNamesToRecords.put(p.Name, new Set<Project__c>());
            }
            mapNamesToRecords.get(p.name).add(p);
        }
        for(Project__c priorP:[ // Find any existing records that may have the same name.
            SELECT Id, name 
            FROM Project__c 
            WHERE name in :mapNamesToRecords.keySet() 
            AND Id Not In :setIds
        ]){
            if(mapNamesToRecords.containsKey(priorP.Name)){
                mapNamesToRecords.get(priorP.name).add(priorP);
            }
        }
        for (Project__c p : trigger.new) {
            // Go through newMap again, adding errors to any records where there are more than one found by name
            if(mapNamesToRecords.get(p.Name).size() > 1){
            	p.Name.addError('A project with the same name already exists, or duplicate Name');
            }
        }
        
/* Also update the Next_Renewal_Date__c */
        for (Project__c p : trigger.new) {
            controller.updateProjectDates_Before(Trigger.New);
        }
        
        system.debug('----> End Trigger.isBefore');
    } // End isBefore
    
    
    
    
}

/*
trigger BD_CalculateSKUsOnAccount on Project__c (after insert, after update, after delete) {
    Set<Id> accIds = new Set<Id>();
    if(trigger.isDelete){
        for(Project__c p : trigger.old){
            accIds.add(p.Account__c);
        }
    }else{
        for(Project__c p : trigger.new){
            if(trigger.isUpdate){ 
                if((p.Number_of_SKUs__c != trigger.oldMap.get(p.Id).Number_of_SKUs__c) || (p.Account__c != trigger.oldMap.get(p.Id).Account__c) ){
                    accIds.add(p.Account__c);
                    accIds.add(trigger.oldMap.get(p.Id).Account__c);
                }
            }else{
                accIds.add(p.Account__c);
            }
        }
    }
    if(accIds.size() > 0){
        BD_SKUCount.calculateSKUCount(accIds);
    }
}

*/