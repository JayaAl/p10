trigger TriggerOnUpfrontJunction on Upfront_Junction__c (after insert, after update) {
	UpfrontJunctionTriggerHandler upfrontJunctionTriggerHandler = new UpfrontJunctionTriggerHandler(Trigger.isExecuting, Trigger.size);
	if(trigger.isAfter){
		if(trigger.isInsert){
			upfrontJunctionTriggerHandler.onAfterInsert(Trigger.new);
		}else if(trigger.isUpdate){
			upfrontJunctionTriggerHandler.onAfterUpdate(Trigger.new,Trigger.oldMap, Trigger.newMap);
		}
	}
}