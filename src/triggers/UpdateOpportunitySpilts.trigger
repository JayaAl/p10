/*
Name : UpdateOpportunitySpilts
Author : Clear Task 
Date : 16-06-11
Usage : It deletes Oppornuity Spilt Record if the OwnerId is equal to SalesPerson   
*/
/* 
  Splits Approval functionality, added my Lakshman(sfdcace@gmail.com)
*/
trigger UpdateOpportunitySpilts on Opportunity (after update) {

    //if(!Splits_OPP_OwnerChangeHelper.hasOwnerChanged) {//Code COMMENTED By Sri for Loading ADX
    if(!Splits_OPP_OwnerChangeHelper.hasOwnerChanged && UserInfo.getUserId() != Label.ADXSyncUser) {//Code ADDED By Sri for Loading ADX
        /*ownerIds set for Opportunity Owners*/
        Set<Id>ownerIds = new Set<Id>();
        /*ownerIds set for Opportunity Owners*/
        Set<Id>oldOwnerIds = new Set<Id>();
        /*oppIds set for Opportunity Ids*/
        Set<Id>oppIds = new Set<Id>();
        Map<Id, Opportunity> oppSplitsForApproval = new Map<Id, Opportunity>();
        /*oppList List for whose Owner is change*/
        List<Opportunity> oppList = new List<Opportunity>();
        /*ownerChange variable if owner changes*/
        Boolean ownerChange;
        Map<Id, Opportunity> oldMap = new Map<Id, Opportunity>(Trigger.oldMap);
        Map<Id, Opportunity> newMap = new Map<Id, Opportunity>(Trigger.newMap);
        /*for collecting of Owner changes*/
        for(Opportunity opp :trigger.new){
            ownerChange = UpdateOpportunitySplit.hasChanges('OwnerId',trigger.oldMap.get(opp.Id),opp);
            Opportunity beforeUpdate = trigger.oldMap.get(opp.Id);
            if(ownerChange){
                if(opp.ContractStartDate__c < system.today() && opp.Probability == 100 && !opp.Splits_Locked__c && opp.Splits_Locked__c == beforeUpdate.Splits_Locked__c) {
                    oppSplitsForApproval.put(opp.Id, opp);
                    oldMap.remove(opp.Id);//remove from old and new map
                    newMap.remove(opp.Id);
                } else {
                    ownerIds.add(opp.OwnerId);
                    System.debug('***opp.OwnerId = '+opp.OwnerId);
                    oldOwnerIds.add(beforeUpdate.OwnerId);  
                    oppIds.add(opp.Id);
                    oppList.add(opp);
                }
            }   
        }
        Map<Id, User> userMap = new Map<Id, User>([SELECT Name, LastName FROM User WHERE Id IN :ownerIds]);
        System.debug('***ownerIds = '+ ownerIds);
        System.debug('***oldOwnerIds = '+ oldOwnerIds);
        /*If owner has not change then do nothing */
        if((ownerChange) && (!ownerIds.isEmpty())){
            /*mapOppSpilt as SalesPerson Id as key & List Opportunity_Split__c as value for new Owner*/
            Map<Id,List<Opportunity_Split__c>> mapOppSpilt = new Map<Id,List<Opportunity_Split__c>>();
            List<Opportunity_Split__c> lstOppSplit = new List<Opportunity_Split__c>();
             List<Splits_Approval__c> oppSpLAppList = new List<Splits_Approval__c>();
            
            mapOppSpilt = UpdateOpportunitySplit.getOppMap(oppIds,ownerIds,oppList);
            
            System.debug('mapOppSplit='+ mapOppSpilt);
            /*mapOppSpilt as SalesPerson Id as key & List Opportunity_Split__c as value for old owner*/
            Map<Id,List<Opportunity_Split__c>> mapOldOppSpilt = new Map<Id,List<Opportunity_Split__c>>();
            mapOldOppSpilt = UpdateOpportunitySplit.getOppMap(oppIds,oldOwnerIds,oppList);
            System.debug('mapOldOppSpilt='+ mapOldOppSpilt);
            //lstOppSplit = UpdateOpportunitySplit.splitList;
            Set<Opportunity_Split__c> oppSpNewOwnerSet = new Set<Opportunity_Split__c>();//Modified by Lakshman at 21/2/2013 to fix Duplicate Id issue
            Set<Opportunity_Split__c> oppSpOldOwnerSet = new Set<Opportunity_Split__c>();//Modified by Lakshman at 21/2/2013 to fix Duplicate Id issue
            
            System.debug('oppSpNewOwnerSet' + oppSpNewOwnerSet);
            System.debug('oppSpOldOwnerSet' + oppSpOldOwnerSet);
            /*for Owner is Change so assign Old Owner Spilt to New Owner*/
            Date firstDayOfMonth = System.today().toStartOfMonth();
            for(Opportunity opp :oppList){
                Opportunity beforeUpdate = trigger.oldMap.get(opp.Id);
                
                system.debug('beforeUpdate======='+beforeUpdate);
                //Split Approval logic
                Decimal changeVal;
                System.debug('lstOppSplit======='+lstOppSplit);
                for(Opportunity_Split__c oppSplit : lstOppSplit){
                    Splits_Approval__c spApproval = new Splits_Approval__c();
                    
                    spApproval.Split__c = string.valueOf(oppSplit.Split__c);
                    spApproval.Start_Date__c =opp.CreatedDate;
                    spApproval.Effective_Month__c = firstDayOfMonth ;
                    System.debug('====$$$$$$$'+spApproval.Effective_Month__c+'====='+firstDayOfMonth);
                    spApproval.End_Date__c =spApproval.Effective_Month__c;                  
                    spApproval.Opportunity__c = oppSplit.Opportunity__c ;
                    spApproval.Name__c = oppSplit.Salesperson_Name__c ;
                    
                    System.debug('#####'+oldOwnerIds.contains(oppSplit.Salesperson__c));
                    if(oldOwnerIds.contains(oppSplit.Salesperson__c)){
                                  
                        spApproval.Change_Value__c ='0%';
                        changeVal = oppSplit.Split__c;
                        spApproval.Change_Type__c = 'Opportunity Owner Change';
                      
                    }
                   
                    System.debug('spApproval======'+spApproval);
                    oppSpLAppList.add(spApproval);
                }
                Splits_Approval__c spApp = new Splits_Approval__c();
                //spApproval.Name ='c';Trigger.newMap.get(opp.Id).Name;
                spApp.Split__c = '';
                spApp.Effective_Month__c = firstDayOfMonth ;
                spApp.Start_Date__c = firstDayOfMonth+1 ;
                spApp.Change_Value__c =string.valueOf(changeVal);
                spApp.Change_Type__c ='Opportunity Owner Change';
               
                //spApp.End_Date__c = spApp.Effective_Month__c;
                spApp.Opportunity__c = opp.Id ;
                spApp.Name__c = userMap.get(opp.OwnerId).name;
                System.debug('spAppr======================================'+spApp);
                oppSpLAppList.add(spApp);
                            
            
            
                if(!mapOppSpilt.isEmpty()&& mapOppSpilt.containsKey(opp.OwnerId)){
                    oppSpNewOwnerSet.addAll(mapOppSpilt.get(opp.OwnerId)); 
                }
                if(!mapOldOppSpilt.isEmpty()&& mapOldOppSpilt.containsKey(beforeUpdate.OwnerId)){
                    oppSpOldOwnerSet.addAll(mapOldOppSpilt.get(beforeUpdate.OwnerId));   
                }   
                if(oppSpOldOwnerSet != null && oppSpOldOwnerSet.size()>0){
                    System.debug('In If');
                        for(Opportunity_Split__c opSpt :oppSpOldOwnerSet){
                            System.debug('opp.OwnerId' + opp.OwnerId);
                            opSpt.Salesperson__c =  opp.OwnerId;
                            System.debug('oppSpOldOwnerSet' + oppSpOldOwnerSet);
                        }
                }
              }
             System.debug('oppSpLAppList======='+oppSpLAppList);
            if(oppSpLAppList.size()>0){
                insert oppSpLAppList;
            }
            /*old Owner Id Record Added to new Owner Spilt*/
            for(Opportunity opp :oppList){
                Opportunity beforeUpdate = trigger.oldMap.get(opp.Id);
                if(!mapOppSpilt.isEmpty()&& mapOppSpilt.containsKey(opp.OwnerId)){
                    oppSpNewOwnerSet.addAll(mapOppSpilt.get(opp.OwnerId)); 
                }
                if(!mapOldOppSpilt.isEmpty()&& mapOldOppSpilt.containsKey(beforeUpdate.OwnerId)){
                    oppSpOldOwnerSet.addAll(mapOldOppSpilt.get(beforeUpdate.OwnerId));   
                }
                

                
                if(oppSpNewOwnerSet != null && oppSpNewOwnerSet.size()>0 && oppSpOldOwnerSet != null && oppSpOldOwnerSet.size()>0){
                  Decimal percent;
                  /*for(Opportunity_Split__c opSpt :oppSpOldOwnerSet){
                    percent = opSpt.Split__c;
                       
                  }*/
                  System.debug('percent' + percent);
                  if(oppSpOldOwnerSet != null && oppSpOldOwnerSet.size()>0){
                    for(Opportunity_Split__c opSpt :oppSpOldOwnerSet){
                      percent = opSpt.Split__c;
                      for(Opportunity_Split__c setSpt : oppSpNewOwnerSet){
                        setSpt.Split__c = setSpt.Split__c + percent;
                        break;
                      } 
                      break;//Added by Lakshman on 06-04-2014 to break from outer loop to fix issue # ISS 2821.
                    }
                  }
                }
            
            }
            
            
            try{
                if(oppSpNewOwnerSet != null && oppSpNewOwnerSet.size()>0 && oppSpOldOwnerSet != null && oppSpOldOwnerSet.size()>0){
                    if(oppSpOldOwnerSet != null && oppSpOldOwnerSet.size()>0){
                        List<Opportunity_Split__c> tempList = new List<Opportunity_Split__c>();
                        Set<Id> splitIds = new Set<Id>();
                        for(Opportunity_Split__c setSplit: oppSpOldOwnerSet){
                            if(splitIds.add(setSplit.Id)){
                                tempList.add(setSplit);
                            }
                        }
                        
                        
                        delete tempList;
                    }
                    if(oppSpNewOwnerSet != null && oppSpNewOwnerSet.size()>0){
                        List<Opportunity_Split__c> tempList = new List<Opportunity_Split__c>();
                        Set<Id> splitIds = new Set<Id>();
                        for(Opportunity_Split__c setSplit: oppSpNewOwnerSet){
                            if(splitIds.add(setSplit.Id)){
                                tempList.add(setSplit);
                            }
                        }
                        update tempList;
                    }       
                }else{
                    if(oppSpOldOwnerSet != null && oppSpOldOwnerSet.size()>0){
                        List<Opportunity_Split__c> tempList = new List<Opportunity_Split__c>();
                        Set<Id> splitIds = new Set<Id>();
                        for(Opportunity_Split__c setSplit: oppSpOldOwnerSet){
                            if(splitIds.add(setSplit.Id)){
                                tempList.add(setSplit);
                            }
                        }
                        update tempList;   
                    }   
                }
                
                
            }catch(Exception e){
                System.debug('In Catch' + e.getmessage());  
                Logger.logMessage(
                    'trigger UpdateOpportunitySpilts',
                    '',
                    'Trigger UpdateOpportunitySpilts failed - Err: ' + e,
                    'Opportunity_Split__c',
                    '',
                    'Error'
                );
            }
            
        } else if(! oppSplitsForApproval.isEmpty()) {//splits approval starts here - Bulkified code
            User loggedInUser = [Select Id, Territory__c from User where Id =: UserInfo.getUserId()];
            Map<String, Splits_Approvers__c> listApprovers = Splits_Approvers__c.getAll();
            Map<Id, Splits_Approval__c> approvalRequestMap = new Map<Id, Splits_Approval__c>();
            Map<Id, Split_Changes__c> approvalDetailMap = new Map<Id, Split_Changes__c>();
            for(Opportunity opportunity: oppSplitsForApproval.values()) { 
                if(!opportunity.Splits_Locked__c) {
                    String approverId = '';
                    if(loggedInUser.Territory__c == '' || loggedInUser.Territory__c == null || !listApprovers.containsKey(loggedInUser.Territory__c)) {
                        approverId = listApprovers.get('BLANK').Approver__c;
                    } else {
                        approverId = listApprovers.get(loggedInUser.Territory__c).Approver__c;
                    }
                    approvalRequestMap.put(opportunity.Id,  new Splits_Approval__c(Requested_By__c = loggedInUser.Id, Approver__c = approverId, Opportunity__c = opportunity.Id));
                    approvalDetailMap.put(opportunity.Id, new Split_Changes__c(Operation__c = 'OWNER CHANGE', CurrencyIsoCode = opportunity.CurrencyIsoCode,
                                                                                Salesperson__c = opportunity.OwnerId, Opportunity_Owner__c = true));
                }
            }
            if(! approvalRequestMap.isEmpty()) {
                Splits_OPP_OwnerChangeHelper.hasOwnerChanged = true;
                //List<Opportunity> oppToResetOwner = [Select Id, OwnerId, Owner.IsActive, Splits_Locked__c from Opportunity where Id =: oppSplitsForApproval.keySet()];
                
                Map<Id, Opportunity> oppToResetOwnerMap = new Map<Id, Opportunity>();//Add by Lakshman - ESS-13188 on 26/01/2015
                Map<Id, Boolean> mapOfUserIdToActive = new Map<Id, Boolean>();//Add by Lakshman - ESS-13188 on 26/01/2015
                
                for(Opportunity opp: oppSplitsForApproval.values()) {
                    mapOfUserIdToActive.put(Trigger.oldMap.get(opp.Id).OwnerId, true);
                }
                if(! mapOfUserIdToActive.isEmpty()) {//Add by Lakshman - ESS-13188 on 26/01/2015 to query if old Owner was active
                    for(User usr: [Select Id, IsActive from User where Id =: mapOfUserIdToActive.keySet()]) {
                        mapOfUserIdToActive.put(usr.Id,usr.IsActive);
                    }
                }
                for(Opportunity opp: oppSplitsForApproval.values()) {
                    if( mapOfUserIdToActive.containsKey(Trigger.oldMap.get(opp.Id).OwnerId) && mapOfUserIdToActive.get(Trigger.oldMap.get(opp.Id).OwnerId)) {//Add by Lakshman - ESS-13188 on 26/01/2015
                        
                        //opp.OwnerId = Trigger.oldMap.get(opp.Id).OwnerId;
                        //opp.Splits_Locked__c = true;
                        oppToResetOwnerMap.put(opp.Id, new Opportunity(Id = opp.Id, OwnerId = Trigger.oldMap.get(opp.Id).OwnerId, Splits_Locked__c = true));
                    }
                    
                }
                if(! oppToResetOwnerMap.isEmpty()) {
                    update oppToResetOwnerMap.values();//Add by Lakshman - ESS-13188 on 26/01/2015
                }
                List<Approval.ProcessSubmitRequest> requests = new List<Approval.ProcessSubmitRequest> ();
                for(Id oppId: approvalRequestMap.keySet()) {
                    if(! oppToResetOwnerMap.containsKey(oppId)) {
                        approvalRequestMap.get(oppId).Status__c = 'Approved';//Add by Lakshman - ESS-13188 on 26/01/2015
                    }
                }
                
                insert approvalRequestMap.values();
                for(Id oppId: approvalDetailMap.keySet()) {
                    approvalDetailMap.get(oppId).Splits_Approval__c = approvalRequestMap.get(oppId).Id;
                    if(oppToResetOwnerMap.containsKey(oppId)) {//Add by Lakshman - ESS-13188 on 26/01/2015
                        Approval.ProcessSubmitRequest req1 = new Approval.ProcessSubmitRequest();
                        req1.setComments('Submitting owner change request for approval.');
                        req1.setObjectId(approvalRequestMap.get(oppId).Id);
                        requests.add(req1);
                    }
                }
                if(! approvalDetailMap.isEmpty()) {
                    insert approvalDetailMap.values();
                }
                if(! requests.isEmpty()) {
                    Approval.ProcessResult[] processResults = null;
                    try {
                        processResults = Approval.process(requests, true);
                    }catch (System.DmlException e) {
                        System.debug('Exception Is ' + e.getMessage());
                        Logger.logMessage(
                            'trigger UpdateOpportunitySpilts',
                            '',
                            'Submitting owner change request for approval failed - Err: ' + e,
                            'Approval',
                            '',
                            'Error'
                        );
                    }
                }
            }
        }
        
       SPLT_OpportunitySplit.checkOpportunityOwner(oldMap,newMap); 
       //Recalculate Split Detail if map is not empty, Added by Lakshman
       if(! oppIds.isEmpty()) {
           SPLT_ManageSplitDetails.evaluateSplitTypesAsync(oppIds);
       }
   }
}