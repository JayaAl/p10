trigger Update_Sales_Planner_Manager_Email on Opportunity (before update, before insert) {

    if(UserInfo.getUserId() != Label.ADXSyncUser){//Code ADDED By Sri for Loading ADX
        // Create a Map of all Users => Manager email, where the user Id is in the Sales_Planner_NEW__c set created earlier
        Map<Id,String> mapPlannerIdToManagerEmail = new Map<Id,String>();
        for(User u:[Select u.Id, u.Manager.Email from User u Where u.ManagerId!=NULL]){
            mapPlannerIdToManagerEmail.put(u.Id, u.Manager.Email);
        }
        // system.assert(false,'---> ManagerId: '+mapPlannerIdToManagerEmail);
        for(Opportunity o:Trigger.new){        
            // only trigger when
            if( o.Preferred_Invoicing_Method__c != NULL 
                && o.Preferred_Invoicing_Method__c.trim().equalsIgnoreCase('Mail w/ Notarization')
                && o.Sales_Planner_NEW__c != NULL
                && mapPlannerIdToManagerEmail.containsKey(o.Sales_Planner_NEW__c)
                && ( Trigger.isInsert || o.Sales_Planner_Manager_Email__c == NULL || o.Sales_Planner_Manager_Email__c == '' || Trigger.oldMap.get(o.Id).Sales_Planner_NEW__c != o.Sales_Planner_NEW__c )
            ){
                // update the Sales_Planner_Manager_Email__c 
                o.Sales_Planner_Manager_Email__c=mapPlannerIdToManagerEmail.get(o.Sales_Planner_NEW__c);
            }
            /*  Desc: ESS-15087 -  update the Sales Planner field per criteria
                Logic: if Sales Planner = '' AND Campaign Manager != '' then Copy Campaign Manager to Sales Planner
            */
            if(o.Sales_Planner_NEW__c == null && o.Lead_Campaign_Manager__c != null){ 
                   o.Sales_Planner_NEW__c = o.Lead_Campaign_Manager__c;
             }
        }
        // Since this is a 'before' trigger no DML is necessary
    }
}