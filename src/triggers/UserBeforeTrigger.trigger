trigger UserBeforeTrigger on User (before insert, before update) {
    // Pass alluser edits thorough EnterpriseCapabilitiesController to add any applicable Enterprise Capabilities
    EnterpriseCapabilitiesController ecc = new EnterpriseCapabilitiesController();
    ecc.updateEnterpriseCapabilities(Trigger.new);
}