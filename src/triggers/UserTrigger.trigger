trigger UserTrigger on User (after delete, after insert, after undelete,
                             after update, before delete, before insert, before update){
    // Updated Trigger Handler, all functionality now processed through TriggerHandler_User.class
    
    TriggerHandler_User handler = new TriggerHandler_User(Trigger.isExecuting, Trigger.size);
    
    if(Trigger.isInsert && Trigger.isBefore){
        handler.OnBeforeInsert(Trigger.new);
    }
    else if(Trigger.isInsert && Trigger.isAfter){
        handler.OnAfterInsert(Trigger.new);
        TriggerHandler_User.OnAfterInsertAsync(Trigger.newMap.keySet());
        
    }
    
    else if(Trigger.isUpdate && Trigger.isBefore){
        handler.OnBeforeUpdate(Trigger.old, Trigger.new, Trigger.newMap);
    }
    else if(Trigger.isUpdate && Trigger.isAfter){
        handler.OnAfterUpdate(Trigger.old, Trigger.new, Trigger.newMap, Trigger.oldMap);
        TriggerHandler_User.OnAfterUpdateAsync(Trigger.newMap.keySet(),Trigger.oldMap.keySet());
        
    }

    /*  
      Venkata R Sabbella
      upated: 09/14.
      Users cannot be delted.
      
    */ 
    
 /*else if(Trigger.isDelete && Trigger.isBefore){
        handler.OnBeforeDelete(Trigger.old, Trigger    
                                 
   
   else if(Trigger.isDelete && Trigger.isAfter){
        handler.OnAfterDelete(Trigger.old, Trigger.oldMap);
        TriggerHandler_User.OnAfterDeleteAsync(Trigger.oldMap.keySet());
    }
    
    else if(Trigger.isUnDelete){
        handler.OnUndelete(Trigger.new);
    }*/
}