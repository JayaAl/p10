trigger ValidGMZipCombination on Group_Member_Zip_Junction__c (before insert, before update) {
    /*
     * Commenting all Trigger functionality as this can be accomplished more easily through the use of Field Uniqueness checking and Workflow Rules
     * Changes by Casey Grooms
     *
    Map<String, Group_Member_Zip_Junction__c> gmzMap = new Map<String, Group_Member_Zip_Junction__c>();
    for (Group_Member_Zip_Junction__c gmz : System.Trigger.new) {
        
        // Make sure we don't treat an email address that  
        // isn't changing during an update as a duplicate.  
        gmz.Group_Zip_Conbination__c = (String)gmz.Zip_Code__c + (String)gmz.Group_Member__c;
        if (System.Trigger.isInsert ||
              (gmz.Group_Zip_Conbination__c != 
                  System.Trigger.oldMap.get(gmz.Id).Group_Zip_Conbination__c )) {
        
            // Make sure another new gmz isn't also a duplicate  
    
            if (gmzMap.containsKey(gmz.Group_Zip_Conbination__c)) {
                gmz.addError('Another new Group Member Zip has the '
                                    + 'same Group member to Zip Combination.');
            } else {
                gmzMap.put(gmz.Group_Zip_Conbination__c, gmz);
            }
       }
    }
    
    // Using a single database query, find all the gmzs in  
    
    // the database that have the same email address as any  
    
    // of the gmzs being inserted or updated.  
    
    for (Group_Member_Zip_Junction__c gmz : [SELECT Group_Member__c, Zip_Code__c, Assignment_Group__r.Name, Assignment_Group__r.Id, Group_Zip_Conbination__c
                                             FROM Group_Member_Zip_Junction__c
                      WHERE Group_Zip_Conbination__c IN :gmzMap.KeySet()]) {
        Group_Member_Zip_Junction__c newGMZ = gmzMap.get(gmz.Group_Zip_Conbination__c);
        newGMZ.addError('A Group Member to Zio Combination already Exists for Assignment Group- ' + gmz.Assignment_Group__r.Name 
                        );
    }
     */
}