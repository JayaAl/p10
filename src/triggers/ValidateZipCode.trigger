trigger ValidateZipCode on Zip_Code__c (before insert, before update) {
    Set<String> leadZipCodes = new Set<String>();
    Map<String, String> zipStates = new Map<String, String>();
    if(trigger.isUpdate){
        Zip_Code__c oldRecord = Trigger.old[0];
        Zip_Code__c newRecord = Trigger.new[0];   

        if(oldRecord.name == newRecord.name)return;
        else leadZipCodes.add(newRecord.name);
    } else {
        leadZipCodes.add(Trigger.new[0].name);
    }
   
    
    

    
    //Find Zip Code matching on name
    for (Zip_Code__c agq : Trigger.new)
    {
        agq.Valid_Zip__c = true;
/*        if (zipStates.containsKey(agq.Name)) {
            agq.Valid_Zip__c = true;
        } else {
            //Error: Zip Code not found
            agq.Valid_Zip__c = false;
            agq.addError('Invalid Zip Code: Zip Code ' + agq.Name + ' cannot be found.');
        }   
*/
    }
}