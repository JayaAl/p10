trigger upfrontAfterTrigger on Upfront__c (after insert, after update) {

    List<Case> cList = new List<Case>();
    map<id,Double> mapFront = new map<Id,Double>();
    system.debug('************'+trigger.new);
        for(Upfront__c up:trigger.new){
            mapFront.put(up.Id,up.Amount_Spent__c);
            system.debug('****************'+mapFront);    
        }
        for(case cas: [select id,Amount_Spent__c,Upfront__c from case where Upfront__c IN:Trigger.new]) {
            cas.Amount_Spent__c =mapFront.get(cas.Upfront__c); 
            system.debug('********************'+cas);    
            cList.add(cas);
        }
        update cList;
}